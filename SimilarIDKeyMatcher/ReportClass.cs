using System;
using System.Collections.Generic;
using System.Text;
using BusinessObject;
using CarlosAg.ExcelXmlWriter;

namespace SimilarIDKeyMatcher
{
    class ReportClass
    {
        SimilarityData _reportData;

        internal ReportClass(SimilarityData reportData)
        {
            _reportData = reportData;
        }

        internal Workbook MakeReport(int depth)
        {
            IDTreeNode tree = _reportData.Tree;
            int maxCol = depth+2;
            List<string[]> matrix = new List<string[]>();
            const int execDepth = 0;
            const int initialGroupNum = 1;

            foreach (IDTreeNode execBasedNode in tree.ChildList)
            {
                List<string> execList = execBasedNode.GetExecList().ConvertAll<string>(
                    delegate(int i) { return "E" + i.ToString(); });
                string[] row = new string[maxCol];
                row[execDepth] = String.Join(" & ", execList.ToArray());
                FinishReport(matrix, execBasedNode, execDepth + 1, row, initialGroupNum);
            }
            List<string[]> missingIntronRows = MakeMissingIntronMatrix();
            int secondLevelCount = CountGroupsAt(2);

            return TurnMatrixToExcelXml(matrix, maxCol, missingIntronRows, secondLevelCount);
        }

        private List<string[]> MakeMissingIntronMatrix()
        {
            const int intronColumn = 1;
            int numIntronGroups = _reportData.CountMissingIntrons;
            int maxMissCol = numIntronGroups + intronColumn;
            List<string[]> missingIntronRows = new List<string[]>();

            for (int intronIndex = 0; intronIndex < numIntronGroups; intronIndex++)
            {
                List<string> intronList;
                List<ID> idList = _reportData.GetMissingIDs(intronIndex, out intronList);
                string[] row = new string[maxMissCol];

                row[intronIndex] = "No " + String.Join(",", intronList.ToArray());
                if (idList.Count == 0)
                {
                    missingIntronRows.Add(row);
                }
                else
                {
                    for (int idIndex = 0; idIndex < idList.Count; idIndex++)
                    {
                        row[maxMissCol - 1] = idList[idIndex].Header.ID;
                        missingIntronRows.Add(row);
                        row = new string[maxMissCol];
                    }
                }
            }
            return missingIntronRows;
        }

        private Workbook TurnMatrixToExcelXml(List<string[]> matrix, 
            int maxCol, List<string[]> missingIntronRows, int groupCount)
        {            
            const int muteColumn = 1;
            string lastExec = string.Empty;
            string lastMutegroup = string.Empty;
            int maxRow = matrix.Count;
            int nextRow;
            Workbook workbook = InitAWorkbookWithASheet();
            WorksheetTable table = workbook.Worksheets[0].Table;
            WorksheetRow currentRow = table.Rows.Add();

            currentRow.Cells.Add("Total of first group = " + groupCount.ToString());
            currentRow = table.Rows.Add();
            for (int row = 0; row < maxRow; row++)
            {
                lastExec = matrix[row][0];
                lastMutegroup = matrix[row][muteColumn];

                for (int col = 0; col < maxCol; col++)
                {
                    currentRow.Cells.Add(matrix[row][col]);
                }
                currentRow = table.Rows.Add();
                nextRow = row + 1;

                if (nextRow < maxRow && (lastExec != matrix[nextRow][0]))//!String.IsNullOrEmpty(matrix[nextRow][muteColumn]))
                {
                    currentRow = table.Rows.Add();
                }
                else if (nextRow < maxRow && (lastMutegroup != matrix[nextRow][muteColumn]))
                {
                    currentRow = table.Rows.Add();
                }

                
            }
            table.Rows.Add();
            foreach (string[] line in missingIntronRows)
            {
                currentRow = table.Rows.Add();
                currentRow.Cells.Add();
                for (int col = 0; col < line.Length; col++)
                {
                    currentRow.Cells.Add(line[col]);
                }
            }
            return workbook;
        }

        private Workbook InitAWorkbookWithASheet()
        {
            const string Title = "Similar ID Report";
            const string HeaderStyle = "HeaderStyle";
            Workbook book = new Workbook();

            book.Properties.Company = "UEI";
            book.Properties.Created = DateTime.Now;

            WorksheetStyle style = book.Styles.Add(HeaderStyle);
            style.Font.FontName = "Tahoma";
            style.Font.Size = 10;
            style.Font.Bold = true;
            style.Alignment.Horizontal = StyleHorizontalAlignment.Automatic;

            style = book.Styles.Add("Default");
            style.Font.FontName = "Tahoma";
            style.Font.Size = 10;
            style.Font.Color = "Black";
            style.Alignment.Horizontal = StyleHorizontalAlignment.Automatic;

            Worksheet sheet = book.Worksheets.Add(Title);
            WorksheetRow row1 = sheet.Table.Rows.Add();
            row1 = sheet.Table.Rows.Add();
            WorksheetCell titleCell = row1.Cells.Add(Title);
            titleCell.StyleID = HeaderStyle;
            WorksheetRow row2 = sheet.Table.Rows.Add();
            row2.Cells.Add("System Date and Time stamp");
            WorksheetRow row3 = sheet.Table.Rows.Add();
            row3.Cells.Add(DateTime.Now.ToString());
            WorksheetRow rowempty = sheet.Table.Rows.Add();
            rowempty = sheet.Table.Rows.Add();

            return book;
        }

        private int FinishReport(List<string[]> matrix, IDTreeNode parent,
            int depth, string[] row, int initialGroupNum)
        {
            string[] childRow = row.Clone() as string[];
            int mutegroup = 1;
            bool firstexecline = true;
            foreach(IDTreeNode mutenode in parent.ChildList)
            {
                bool firstmuteline = true;
                int startdepth = depth;
                childRow[startdepth++] = mutenode.Name + "#" + mutegroup;
                bool isValidMuteGroup = false;
                foreach(ID id in mutenode.IDList)
                {
                    if(IsIdMissingIntron(id.Header.ID.Trim(),parent.Name))
                    {
                        childRow = row.Clone() as string[];
                        startdepth = depth;
                        if (firstexecline)
                            childRow = row.Clone() as string[];
                        if(firstmuteline)
                            childRow[startdepth] = mutenode.Name + "#" + mutegroup;
                        continue;
                    }
                    startdepth = depth + 1;
                    isValidMuteGroup = true;
                    foreach(IDTreeNode intronnode in mutenode.ChildList)
                    {
                        int introngroup = 1;
                        
                        if(IsIdMissingIntron(id.Header.ID,intronnode.Name))
                        {
                            childRow[startdepth] = "No " + intronnode.Name;
                        }
                        else
                        {
                            foreach(IDTreeNode differentintron in intronnode.ChildList)
                            {
                                if (differentintron.IDList.Contains(id))
                                {
                                    if(firstmuteline)
                                        childRow[startdepth] = intronnode.Name + "#" + introngroup;
                                    else
                                        childRow[startdepth] = "#"+introngroup;
                                    break;
                                }
                                // dont increment Introngroups if they belong in missing introns list
                                bool isIdmissing = false;
                                foreach(ID mid in differentintron.IDList)
                                {
                                    if (IsIdMissingIntron(mid.Header.ID, intronnode.Name))
                                        isIdmissing = true;
                                }
                                if(!isIdmissing)
                                    introngroup++;
                            }
                        }
                        startdepth++;
                    }

                    childRow[startdepth] = id.Header.ID;
                    matrix.Add(childRow);
                    //childRow = new string[childRow.Length];
                    childRow = row.Clone() as string[];
                    startdepth = depth;
                    childRow[startdepth++] = mutenode.Name + "#" + mutegroup;
                    //firstmuteline = false;
                    firstexecline = false;
                }
                if(isValidMuteGroup)
                    mutegroup++;
            }
            return 1;
        }

        private bool IsIdMissingIntron(string id, string intronname)
        {
            // check if intron is missing
            const int IntronColumn = 1;
            int numIntronGroups = _reportData.CountMissingIntrons;
            bool ismissing = false;
            List<string[]> missingIntronRows = new List<string[]>();
            for (int intronIndex = 0; intronIndex < numIntronGroups; intronIndex++)
            {
                List<string> intronList;
                List<ID> idList = _reportData.GetMissingIDs(intronIndex, out intronList);
                foreach(ID missingid in idList)
                {
                    if (String.Compare(missingid.Header.ID.Trim(),id, true) == 0)
                    {
                        // check if it is the intron we are searching for 
                        if(String.Compare(intronname,String.Join(",", intronList.ToArray()),true) ==0)
                            ismissing = true;
                    }
                }
            }
            return ismissing;
        }

        private bool IsIdMissingIntron(string id, int intronIndex)
        {
            // check if intron is missing
            int numIntronGroups = _reportData.CountMissingIntrons;
            bool ismissing = false;
            List<string> intronList;
            List<ID> idList = _reportData.GetMissingIDs(intronIndex, out intronList);
            foreach(ID missingid in idList)
            {
                if (String.Compare(missingid.Header.ID.Trim(),id, true) == 0)
                    ismissing = true;
            }
            return ismissing;
        }

        /*private int FinishReportHelper(IEnumerable<IDTreeNode> childList, string[] childRow, 
            int depth, List<string[]> matrix, int groupNum)
        {
            const int groupNumResetDepth = 5;
            int validCount = 0;
            int initialChildGroupNum = 1;

            foreach (IDTreeNode node in childList)
            {
                bool missingIntron = CountChildrenWithAValidId(node) == 0 && 
                                     _reportData.IsANoDataID(node.IDListAsString[0], node.Name);
                childRow[depth] = (missingIntron ? "No " + node.Name : node.Name + "#" + groupNum);
                int leafCount = FinishReport(matrix, node, depth + 1, childRow, initialChildGroupNum);
                if (leafCount > 0)
                {
                    if(!missingIntron)
                        groupNum++;
                    validCount += leafCount;
                    childRow = new string[childRow.Length];
                }
                if (leafCount > 0 && depth > groupNumResetDepth)
                {
                    initialChildGroupNum += CountChildrenWithAValidId(node);
                }
            }
            return validCount;
        }*/

        /*private int CountChildrenWithAValidId(IDTreeNode parent)
        {
            int validFlag = 0;

            if (parent.ChildList.Count == 0)
            {
                foreach (string id in parent.IDListAsString)
                {
                    if (!_reportData.IsANoDataID(id))
                    {
                        validFlag = 1;
                        break;
                    }
                }
                return validFlag;
            }
            int childCount = 0;

            foreach (IDTreeNode child in parent.ChildList)
            {
                if (CountChildrenWithAValidId((child)) > 0)
                {
                    childCount++;
                }
            }
            return childCount;
        }*/

        private int CountGroupsAt(int maxDepth)
        {
            if (maxDepth < 2)
                return 0;

            int groupNum = 0;

            foreach (IDTreeNode execBasedNode in _reportData.Tree.ChildList)
            {
                groupNum += CountGroupHelper(execBasedNode, 1, maxDepth);
            }

            return groupNum;
        }

        private int CountGroupHelper(IDTreeNode parent, int depth, int maxDepth)
        {
            if (depth == maxDepth)
            {
                int validCount = 0;

                foreach (string id in parent.IDListAsString)
                {
                    if (!IsIdMissingIntron(id,0))
                    {                        
                        validCount++;
                        break;
                    }
                }
                return validCount;
            }
            int groupNum = 0;
            foreach (IDTreeNode node in parent.ChildList)
            {
                if (CountGroupHelper(node, depth + 1, maxDepth) > 0)
                    groupNum++;
            }
            return groupNum;
        }
    }
}
