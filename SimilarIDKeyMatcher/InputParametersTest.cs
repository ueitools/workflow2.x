using System;
using System.Collections.Generic;
using System.Text;

namespace SimilarIDKeyMatcher
{
    class InputParametersTest : InputParameters
    {
        override internal bool Get()
        {
            _IDList = new List<string>(new string[] { "T0000", "T0010" });
            _intronList = new List<List<string>>();
            _intronList.Add(new List<string>(new string[] { "SB.MU" }));
            _intronList.Add(new List<string>(new string[] { "SB.VL.+" }));
            _intronList.Add(new List<string>(new string[] { "SB.VL.-" }));
            _intronList.Add(new List<string>(new string[] { "PW", "PW.ST", "PW.OF" }));
            _intronList.Add(new List<string>(new string[] { "NM.0" }));
            _intronList.Add(new List<string>(new string[] { "IN" }));

            _workDir = "c:\\working\\similarID\\";
            _isTemporaryDir = false;

            return true;
        }
    }
}
