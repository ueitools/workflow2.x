using System;
using System.IO;
using System.Windows.Forms;

namespace CommonUI.Controls
{
    public partial class FileInput : UserControl
    {
        #region Private Data
        private string _filter = "Text Files (*.TXT)|*.TXT|(*.LST)|*.LST|All Files (*.*)|*.*";
        private bool _useSaveDialog = false;
        private string _defaultExt = "txt";
        private bool _fileExists = true;
        private bool _multiselect = false;
        private bool _pathVerified = false;
        #endregion

        public event EventHandler FilePathChanged;

        #region Public Accessors
        public string Filter
        {
            set { _filter = value; }
        }

        public bool UseSaveDialog {
            set { _useSaveDialog = value; }
        }

        public string DefaultExt
        {
            set { _defaultExt = value; }
        }

        public bool FileExists
        {
            set { _fileExists = value; }
        }

        public bool Multiselect
        {
            set { _multiselect = value; }
        }

        public string ButtonTitle
        {
            set { btn_Browse.Text = value; }
        }

        public string FilePath
        {
            get { return tb_FilePath.Text; }
            set { tb_FilePath.Text = value; }
        }

        public bool PathVerified {
            get { return _pathVerified; }
        }
        #endregion

        public FileInput()
        {
            InitializeComponent();
        }

        private void FileInput_Load(object sender, EventArgs e)
        {

        }

        private void btn_Browse_Click(object sender, EventArgs e)
        {
            FileDialog dlg;
            if (_useSaveDialog) {
                dlg = new SaveFileDialog();
            } else {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Multiselect = _multiselect;
                dlg = openFileDialog;
            }
            dlg.CheckFileExists = _fileExists;
            dlg.DefaultExt = _defaultExt;
            dlg.Filter = _filter;
            if (!string.IsNullOrEmpty(tb_FilePath.Text))
                dlg.InitialDirectory = Path.GetFullPath(tb_FilePath.Text);

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                tb_FilePath.Text = dlg.FileName;
                if (FilePathChanged != null) {
                    FilePathChanged(this, EventArgs.Empty);
                }
            }

            _pathVerified = true;
        }

        private void FileInput_EnabledChanged(object sender, EventArgs e) {
            tb_FilePath.Enabled = Enabled;
            btn_Browse.Enabled = Enabled;
        }

        private void tb_FilePath_TextChanged(object sender, EventArgs e) {
            _pathVerified = false;
        }
    }
}
