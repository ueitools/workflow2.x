namespace SimilarIDKeyMatcher
{
    partial class KeyInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.intronTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.OKButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.workDirTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.removeCheckBox = new System.Windows.Forms.CheckBox();
            this.Browsebutton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // intronTextBox
            // 
            this.intronTextBox.Location = new System.Drawing.Point(12, 139);
            this.intronTextBox.Name = "intronTextBox";
            this.intronTextBox.Size = new System.Drawing.Size(566, 20);
            this.intronTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(380, 120);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enter introns:\r\nUse semicolon (;) to separate keys\r\nUse comma (,) to make conditi" +
    "onal list\r\n\r\nExample:\r\nSB.MU;SB.VL.+;SB.VL.-;PW,PW.ST,PW.OF;NM.0;IN";
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(17, 285);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 2;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(124, 285);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // workDirTextBox
            // 
            this.workDirTextBox.Location = new System.Drawing.Point(12, 204);
            this.workDirTextBox.Name = "workDirTextBox";
            this.workDirTextBox.Size = new System.Drawing.Size(566, 20);
            this.workDirTextBox.TabIndex = 4;
            this.workDirTextBox.Text = "c:\\working\\";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 179);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(470, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Working directory (files will be deleted and new files created here):";
            // 
            // removeCheckBox
            // 
            this.removeCheckBox.AutoSize = true;
            this.removeCheckBox.Checked = true;
            this.removeCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.removeCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeCheckBox.Location = new System.Drawing.Point(17, 245);
            this.removeCheckBox.Name = "removeCheckBox";
            this.removeCheckBox.Size = new System.Drawing.Size(273, 24);
            this.removeCheckBox.TabIndex = 6;
            this.removeCheckBox.Text = "Delete cfi directories when finished";
            this.removeCheckBox.UseVisualStyleBackColor = true;
            // 
            // Browsebutton
            // 
            this.Browsebutton.Location = new System.Drawing.Point(628, 202);
            this.Browsebutton.Name = "Browsebutton";
            this.Browsebutton.Size = new System.Drawing.Size(75, 23);
            this.Browsebutton.TabIndex = 7;
            this.Browsebutton.Text = "Browse";
            this.Browsebutton.UseVisualStyleBackColor = true;
            this.Browsebutton.Click += new System.EventHandler(this.Browsebutton_Click);
            // 
            // KeyInput
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(769, 329);
            this.Controls.Add(this.Browsebutton);
            this.Controls.Add(this.removeCheckBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.workDirTextBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.intronTextBox);
            this.Name = "KeyInput";
            this.Text = "Additional Input";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox intronTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TextBox workDirTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox removeCheckBox;
        private System.Windows.Forms.Button Browsebutton;
    }
}

