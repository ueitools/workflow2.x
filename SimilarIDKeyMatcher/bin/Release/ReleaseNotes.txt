
UEI Workflow 2010 
---------------------------
Release Version : V2.0
Build Version   : V2.14.4738.21602
Update Date     : 21/12/2012
StarTeam Label  : WF.2.14.Beta2

Fixes:
1. Fixed Issue related to file opening in setup code format modifier utility in BSC report.
2. Fixed Issue related to setup code format changing in BSC report.

######################################################################################
Release Version : V2.0
Build Version   : V2.0.4730.30007
Update Date     : 14/12/2012
StarTeam Label  : WF2010.2.14.Beta2


Features:

BSC:
1. Added Base6 format in BSc report.
2. Added option to switch between base digits starting with 1 and 0.
3. Added Setupcode format modifier to previously generated BSC reports.
Fixes:
1. Fixed issue related to folder name in IRDBSearchEngine.

###############################################################################################
Release Version : V2.0
Build Version   : V2.0.4717.25059
Update Date     : 29/10/2012
StarTeam Label  : WF2010.2.14 Beta 1

Features: 
1. IRDBSearch Engine modified.
	 Added feature for input multiple folders.
2. Executor dlls updated.
#####################################################################################################
Release Version : V2.0
Build Version   : V2.0.4678.31410
Update Date     : 22/10/2012
StarTeam Label  : WF2010.2.13

Fixes:

1. ICompareID.cs is fixed for E402.

########################################################################################################
Release Version : V2.0
Build Version   : V2.0.4591.22880
Update Date      : 27/07/2012
StarTeam Label  : T31

Fixes:

EX/ID/TN Info report:

1. Getting Exceptional handling error in Ex/TN/ID Info  report while clicking on empty space in the TN list in UEI2.(CR:17625)


#########################################################################
Release Version : V2.0
Build Version   : V2.0.4590.18928
Update Date      : 26/07/2012
StarTeam Label  : T30

Fixes:

IRDBSearchEngine:

1.Issue regarding saving the result to report file with RPT extension is fixed.

##############################################################
Release Version : V2.0
Build Version   : V2.0.4580.28095
Update Date      : 16/07/2012
StarTeam Label  : T29

Fixes:

IRDBSearchEngine:
1) To handle display LOG file in Multiple TN input. 

###################################################################
Release Version : V2.0
Build Version   : V2.0.4577.21529
Update Date      : 13/07/2012
StarTeam Label  : T28

Fixes:

IRMaestro:- Custom code in RBK
-------------------------------------------------
Some executors generate custom code in the CFI files.  
These codes are added to the report (RBK file) for easy viewing.  
These custom codes are extracted from the CFI files.

 


######################################################################################
Release Version : V2.0
Build Version   : V2.0.4570.17232
Update Date      : 06/07/2012
StarTeam Label  : T27

Fixes:

IRDBSearchEngine:-

1. IntronMatch value format fix. Compare.cs code is modified.

Enhancements:

IRMaestro:- Readback compare Enhancements
---------------------------------------------

1    RPT File Saving
2    Report only unmatched
3    Save settings in report file
4    Save settings in working directory
5    Enable Cancel Button while Readback comparison is running
6    Warning executor is not implemented
7    Explicit labeling of data source in RPT file
8    Flatten report structure
9    Merge Compare results
10   Keep CFI Files
11   Handle Empty Readback .zip File



###################################################################################
Release Version : V2.0
Build Version   : V2.0.4559.31010
Update Date      : 25/06/2012
StarTeam Label  : T26

Updates:

IRDBSearchEngine:

1.Enable multiple capture file processing.
2. New functions and new signals feature added.

#############################################################################
Release Version : V2.0
Build Version   : V2.0.4538.30245
Update Date      : 04/06/2012
StarTeam Label  : T25

Updates:

1. Exec/Id Report is added to Workflow 2010.

IrDBSearch Engine
-------------------
1.To display multiple tns when the user inputs tn list as file.(ResultForm is modified in common forms project).


#############################################################################################
Release Version : V2.0
Build Version   : V2.0.4491.28477
Update Date      : 18/04/2012
StarTeam Label  : T24

Updates:

1.Executor DLLs updated

#########################################################################################
Release Version : V2.0
Build Version   : V2.0.4491.28477
Update Date      : 18/04/2012
StarTeam Label  : T24

Fixes:

IDView
-------
1.Remote picture viewer form reverted back to old version.
###############################################################################################
Release Version : V2.0
Build Version   : V2.0.4484.21133
Update Date      : 18/04/2012
StarTeam Label  : T23

Updates:

1. Executor dll update no : 551.

############################################################################################
Release Version : V2.0
Build Version   : V2.0.4484.21133
Build Date      : 11/04/2012
StarTeam Label  : T23

Features:

IrMaestro
-----------------

1. XML input feature is enabled in read back verification.

Note:  IrMaestro 2.0 uses the updated version of XML. Old versions of XMLs are not supported.

##########################################################################################
Release Version : V2.0
Build Version   : V2.0.4472.22895
Build Date      : 30/03/2012
StarTeam Label  :T22
Fixes:

PickAccessLayer
----------------
1. Updated GetPrefixItems_Test query in Pick2Dao.xml.

######################################################################################
Release Version : V2.0
Build Version   : V2.0.4455.23285
Build Date      : 13/03/2012

Features:

IRMaestro
------------------------------

1.)	Fixed Typo error in Fi Compare Settings form
2.)	Duty cycle checkbox is disabled by default in Fi Compare Settings form

###################################################################################################
Release Version : V2.0
Build Version   : V2.0.4436.20657
Build Date      : 23/02/2012

Features:

IRMaestro
-----------------

Timing Inspector
----------------------
  1. Enabled Cancel button
  2. Explicit labeling of data source in RPT file
  3. Save settings in working directory
  4. Enhancing Fi Compare Algorithm

Readback Verification
----------------------------
  1. Enabled Cancel button
  2. Save settings in working directory
  3. Custom code in RBK
  4. Explicit labeling of data source in RPT file


Fixes:

Label\Intron Search
------------------------------
  1. Rewriting issues found while saving the reports to excel and pdf is fixed. The report wont be
  saved if a file with same name is opened, instead it will through a message that file is in use.	

####################################################################################################
Release Version : V2.0
Build Version   : V2.0.4430.26739
Build Date      : 17/02/2012

Fixes:

1. CR no:16834 - Getting execption while navigating to IRDBSearchEngine after the generation of reports
using input files.

2. Issue with duplicate label or intron names in Label/Intron search. 

##############################################################################################
Release Version : V2.0
Build Version   : V2.0.4414.26283
Build Date      : 01/02/2012

Fixes:

Label\Intron Dictionary
----------------------------------
1. Category dropdown items are sorted.
2. Category dropdown type is changed to dropdownlist(blocked the user entry).
3. Fixed the problem found in check box selection.(checkbox not reflecting the change).

#################################################################################################
Release Version : V2.0
Build Version   : V2.0.4409.24310
Build Date      : 27/01/2012

Features:

ReportFilterForm
-----------------------------------
1.Provided Label\Intron dictionary in ReportFilterForm for Label\Intron search report.
####################################################################################################
Release Version : V2.0
Build Version   : V2.0.4370.28109
Build Date      : 19/12/2011

Fixes:

ReportFilterForm
------------------------
1.Changed the text "Load File" to "Browse" in modelinforeport. 
####################################################################################################
Release Version : V2.0
Build Version   : V2.0.4363.22204
Build Date      : 12/12/2011

Features:

1. Field Capture Dongle support has been integrated in IRMaestro.

Fixes:

1. Issues in loading IDView tool is fixed. Refer Change Request #16834.
2. Toolstrip buttons in ReportMainForm is aligned properly.

########################################################################################################
Release Version : V2.0
Build Version   : V2.0.4351.30869
Build Date      : 30/11/2011

Modifications:

ReportFilterForm
---------------------
1. Removed selection of all device types by default.

###########################################################################################################
Release Version : V2.0
Build Version   : V2.0.4351.25761
Build Date      : 30/11/2011

Features:

1)IDView
--------------------------------------
Updated in IDView to view IDs from 
1. Main Data Base: UEI2
2. Temp Data Base: UEI2Temp

2)Getting model count based on type of models(ie based on target model and remote model) in BSC report.

#############################################################################################################
Release Version : V2.0
Build Version   : V2.0.4351.19405
Build Date      : 29/11/2011

1.Getting model count based on type of models(ie based on target model and remote model) in reports
IDByRegion,IDByBrand and in BrandBasedSearch.

2. Modifications in paramater names in stored procedure(UEI2_Sp_GetIDList  ) for IDByRegion report is handled.


########################################################################################################
Release Version : V2.0
Build Version   : V2.0.4338.27350
Build Date      : 17/11/2011

Fixes:

IRDBSearch Engine (New Signal & New Function Update)
-----------------------------------------------------
1. Updated NewFunctionsNewSignals.cs in CommonForms to handle variable zip file name with special charecters.

###############################################################################################################
Release Version : V2.0
Build Version   : V2.0.4337.19347
Build Date      : 16/11/2011

Features:

ReportFilterForm
-------------------------
1. "Enter" key press support for text boxes Brand & Model sections displayed with ModelInfo report.
2. Multiple selection support for list box in Label/Intron search report.

#################################################################################################################
Build Version   : V2.0.4335.33333
Build Date      : 11/11/2011

Issues Fixed
-------------
1. Generation of BSC report in .XLS format with Sub Device Types is generating an empty report.
2. Generation of BSC report in .PDF format with Sub Device Types is not generating any report.

Changes made to fix it:
	Updated DeviceCategoryForm.cs form for handling With/Without Device Type option while generating text\excel\pdf report.

3. Error generated when user try to load device types and pressed OK button in ReportFilterForm.

Changes made to fix it:
	Updated ReportFilterForm.cs.
	
#################################################################################################################

Build Version   : V2.0.4318.19392
Build Date      : 28/10/2011

Features:

IRDBSearchEngine
-----------------
1. Added New Signal and new function feature. 
2. A log file by name 'NewSignalsNewFunctionsInfo' is created in the working directory.

ReportFilterForm
---------------------
1. All Data Sources will be selected by default.
2. All Device Types will be selected by default.

DeviceCategoryForm
---------------------
1. Updated DeviceCategoryForm for Generating Excel report for BSC from PIC With/Without Device Types

