using System;
using System.Collections.Generic;
using System.Text;
using BusinessObject;

namespace SimilarIDKeyMatcher
{
    [Serializable]
    public class IDTreeNode
    {
        List<IDTreeNode> _childList = new List<IDTreeNode>();
        List<ID> _idList = new List<ID>();        
        static Dictionary<string, ID> _deserializeCache = new Dictionary<string, ID>();
        string _name;

        internal IDTreeNode()
        {
        }

        internal IDTreeNode(string name)
        {
            _name = name;
        }

        internal IDTreeNode(string name, List<ID> idList)
        {
            _name = name;
            _idList.AddRange(idList);
        }

        internal IDTreeNode(string name, ID id)
        {
            _name = name;
            _idList.Add(id);
        }

        public List<IDTreeNode> ChildList
        {
            get { return _childList; }
            set { _childList = value; }
        }

        internal List<ID> IDList
        {
            get { return _idList; }
        }

        public string[] IDListAsString
        {
            get 
            { 
                return _idList.ConvertAll<string>(delegate(ID id) { return id.Header.ID; }).ToArray(); 
            }
            set
            {
                foreach (string id in value)
                {
                    _idList.Add(DeserializeCache(id));
                }
            }
        }

        private static ID DeserializeCache(string id)
        {
            if (_deserializeCache.ContainsKey(id))
                return _deserializeCache[id];
            else
            {
                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
                ID dbID = ProductDBFunctions.GetID(id);
                _deserializeCache[id] = dbID;
                return dbID;
            }

        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public List<int> GetExecList()
        {
            List<int> sortedList = new List<int>();

            foreach (ID id in _idList)
            {
                int exec = id.Header.Executor_Code;

                if (!sortedList.Contains(exec))
                    sortedList.Add(exec);
            }
            sortedList.Sort();

            return sortedList;
        }
    }
}
