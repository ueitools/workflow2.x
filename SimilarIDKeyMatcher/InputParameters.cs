using System;
using System.Collections.Generic;
using System.Text;

namespace SimilarIDKeyMatcher
{
    using IntronCollection = List<List<string>>;

    abstract class InputParameters
    {
        protected List<string> _IDList;
        protected IntronCollection _intronList;
        protected string _workDir;
        protected bool _isTemporaryDir;

        internal InputParameters()
        {
        }

        abstract internal bool Get();
            
        internal List<string> IDList
        {
            get { return _IDList; }
        }

        internal IntronCollection Introns
        {
            get { return _intronList; }
        }

        internal string WorkDir
        {
            get { return _workDir; }
        }

        internal bool IsTempDir
        {
            get { return _isTemporaryDir; }
        }

        protected IntronCollection ParseIntronList(string intronString)
        {
            char[] groupSeparator = new char[] { ';' };
            char[] conditionSeparator = new char[] { ',' };

            IntronCollection intronList = new IntronCollection();
            List<string> groups = new List<string>(intronString.Split(groupSeparator));
            foreach (string group in groups)
            {
                List<string> introns = new List<string>(group.Trim().Split(conditionSeparator));

                introns.RemoveAll(delegate(string s) { return String.IsNullOrEmpty(s.Trim()); });

                intronList.Add(introns);
            }
            
            return intronList;
        }
    }
}
