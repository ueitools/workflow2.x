using System;
using System.Collections.Generic;
using System.Text;
using CommonForms;
using System.Windows.Forms;
using BusinessObject;

namespace SimilarIDKeyMatcher
{
    class InputParametersGUI : InputParameters
    {
        override internal bool Get()
        {
            IDSelectionForm idSelect = new IDSelectionForm();

            if (idSelect.ShowDialog() != DialogResult.OK)
                return false;

            DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);

            StringCollection allIDList = DBFunctions.NonBlockingGetAllIDList();

            _IDList = idSelect.GetSelectedIDList(allIDList);

            KeyInput intronSelect = new KeyInput();

            if (intronSelect.ShowDialog() != DialogResult.OK)
                return false;

            _intronList = ParseIntronList(intronSelect.Introns);
            _workDir = intronSelect.WorkDirectory;
            _isTemporaryDir = intronSelect.IsTempDir;

            return true;
        }
    }
}
