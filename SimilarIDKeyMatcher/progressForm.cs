﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SimilarIDKeyMatcher
{
    public delegate void SetUpProgressDelegate(int min,int max);
    public delegate void UpateProgressDelegate(int index,string activity);

    public partial class progressForm : Form
    {

        public progressForm()
        {
            InitializeComponent();
        }

        public void SetupProgressBar(int min,int max)
        {
            if(this.InvokeRequired)
                this.BeginInvoke(new SetUpProgressDelegate(SetupProgressBar),new object[]{min,max});
            else
            {
                progressBar1.Maximum = max;
                progressBar1.Minimum = min;
                progressBar1.Value = 0;
                label2.Text = string.Empty;
            }
        }

        public void UpdateProgressBar(int index, string activity)
        {
             if(this.InvokeRequired)
                 this.BeginInvoke(new UpateProgressDelegate(UpdateProgressBar), new object[] { index, activity });
            else
            {
                progressBar1.Value = index;
                label2.Text = activity;
                Application.DoEvents();
            }
        }
    }
}
