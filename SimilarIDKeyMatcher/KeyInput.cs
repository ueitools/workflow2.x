using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SimilarIDKeyMatcher.Properties;
using CommonCode;
using System.IO;

namespace SimilarIDKeyMatcher
{
    public partial class KeyInput : Form
    {
        string _introns;
        string _workDir;
        bool _isTempDir;

        public KeyInput()
        {
            InitializeComponent();
            intronTextBox.Text=  Settings.Default.IntronString;
            workDirTextBox.Text = Settings.Default.WorkingDirectory;
            _isTempDir = Settings.Default.IsTempDir;
            removeCheckBox.Checked = _isTempDir;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            string introns = intronTextBox.Text.Trim();

            if (String.IsNullOrEmpty(introns))
            {
                MessageBox.Show("Please enter a list of introns, like mut;vol+;vol-");
                return;
            }

            _introns = introns;
            _workDir = workDirTextBox.Text.Trim();
            _isTempDir = removeCheckBox.Checked;

            if (!_workDir.EndsWith("\\"))
                _workDir += "\\";
            // make sure working directory exists
            if (!Directory.Exists(_workDir))
                Directory.CreateDirectory(_workDir);

            Settings.Default.WorkingDirectory = _workDir;
            Settings.Default.IntronString = _introns;
            Settings.Default.IsTempDir = _isTempDir;
            Settings.Default.Save();

            DialogResult = DialogResult.OK;
            Close();
        }

        internal string Introns
        {
            get { return _introns; }
        }

        internal string WorkDirectory
        {
            get { return _workDir; }
        }

        internal bool IsTempDir
        {
            get { return _isTempDir; }
        }

        private void Browsebutton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog frm = new FolderBrowserDialog();

            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                _workDir = frm.SelectedPath;
                workDirTextBox.Text = frm.SelectedPath;
            }
        }
    }
}