using System;
using System.Collections.Generic;
using System.Text;
using BusinessObject;
using CfiGenerator2=CfiGenerator.CfiGenerator;
using CommonCode;
using IDCompare;
using System.IO;
using CommonForms;

namespace SimilarIDKeyMatcher
{
    class IDMatcher
    {
        InputParameters _input;
        IntronListWithTagging _intronList;
        delegate bool AreSimilarIDs(ID id1, ID id2);
        delegate void OnNoMatch(ID id);
        Dictionary<string, IFilePathManager> _tempDirs = new Dictionary<string, IFilePathManager>();
        public event SetUpProgressDelegate setupProgress = null;
        public event UpateProgressDelegate updateProgress = null;

        internal IDMatcher(InputParameters input)
        {
            _input = input;
            _intronList = new IntronListWithTagging(input.Introns);

            DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);

            CommonCode.Log.Init(input.WorkDir + "log.txt", true);
        }

        internal SimilarityData ReportSimilarities()
        {
            if (!_input.WorkDir.EndsWith("\\"))
                throw new DirectoryNotFoundException();

            List<ID> idList = new List<ID>();
            if (setupProgress != null)
                setupProgress(0, _input.IDList.Count);
            int index = 0;
            foreach (string id in _input.IDList)
            {
                ID dbID = ProductDBFunctions.GetID(id);
                idList.Add(dbID);
                OutputCFIs(_input.IsTempDir, _intronList.GetList(), dbID);
                if(updateProgress!=null)
                {
                    string msg = string.Format("Generating CFI of {0}",id);
                    updateProgress(index++, msg);
                }
            }
            if (setupProgress != null)
                setupProgress(0, _input.IDList.Count);
            
            IDTreeNode root = new IDTreeNode("Similar IDs", idList);
            SimilarityData result = new SimilarityData(root);

            List<IDTreeNode> currentLevel = MakeExecBasedChildren(idList);
            root.ChildList.AddRange(currentLevel);
            List<IDTreeNode> nextLevel = new List<IDTreeNode>();
            List<ID> idsWithoutCFI = new List<ID>();

            if (setupProgress != null)
                setupProgress(0, currentLevel.Count);
            index = 0;
            foreach (IDTreeNode tn in currentLevel)
            {
                List<IDTreeNode> newChildren = MakeCFIBasedChildren(_input.Introns[0], tn.IDList, idsWithoutCFI);
                nextLevel.AddRange(newChildren);
                tn.ChildList.AddRange(newChildren);
                tn.Name = String.Join(",", _input.Introns[0].ToArray());
                if (updateProgress != null)
                {
                    string msg = string.Format("Processing CFI data of {0}", tn.Name);
                    updateProgress(index++, msg);
                }

            }
                
            currentLevel = nextLevel;
            result.AddMissingIDs(_input.Introns[0], idsWithoutCFI);

            if (setupProgress != null)
                setupProgress(0, currentLevel.Count);
            index = 0;

            foreach (IDTreeNode tn in currentLevel)
            {
                for (int i = 1; i < _input.Introns.Count;i++ )
                {
                    List<ID> idWithoutCFI = new List<ID>();
                    List<IDTreeNode> newChildren = MakeCFIBasedChildren(_input.Introns[i], tn.IDList, idWithoutCFI);
                    IDTreeNode introngroups = new IDTreeNode(String.Join(",", _input.Introns[i].ToArray()));
                    introngroups.ChildList.AddRange(newChildren);
                    tn.ChildList.Add(introngroups);
                    result.AddMissingIDs(_input.Introns[i], idWithoutCFI);
                }

                if (updateProgress != null)
                {
                    string msg = string.Format("Processing CFI data of {0}", tn.Name);
                    updateProgress(index++, msg);
                }
                
            }
            if (setupProgress != null)
                setupProgress(0, currentLevel.Count);
            return result;
        }

        private void CompareEach(List<ID> idList, AreSimilarIDs areSimilar, OnNoMatch onNoMatch)
        {
            foreach (ID candidateID in idList)
            {
                bool isFound = false;

                foreach (ID innerID in idList)
                {
                    if (candidateID == innerID)
                    {
                        continue;
                    }
                    if (areSimilar(candidateID, innerID))
                    {
                        isFound = true;
                        break;
                    }
                }
                if (!isFound)
                {
                    onNoMatch(candidateID);
                }
            }
        }

        private List<IDTreeNode> MakeExecBasedChildren(List<ID> idList)
        {
            List<List<int>> execList = CommonFunctions.MakeExecListWithAlternateEquivalent();
            List<IDTreeNode> execDivisions = new List<IDTreeNode>();
            AreSimilarIDs areSimilar = delegate(ID candidateID, ID innerID) 
            {
                if (IsExecSimilar(candidateID, innerID, execList))
                {
                    string candidate = "E" + candidateID.Header.Executor_Code.ToString();
                    string inner = "E" + innerID.Header.Executor_Code.ToString();

                    AddToSameGroup(execDivisions, candidate, inner, candidateID, innerID);
                    return true;
                }
                return false;
            };
            OnNoMatch onNoMatch = delegate(ID candidateID)
            {
                IDTreeNode tn = new IDTreeNode("E" + candidateID.Header.Executor_Code.ToString(), candidateID);
                execDivisions.Add(tn);
            };
            CompareEach(idList, areSimilar, onNoMatch);

            return execDivisions;
        }

        private List<IDTreeNode> MakeCFIBasedChildren(List<string> intronList, List<ID> idGroup,
            List<ID> idListWithoutCFI)
        {
            List<IDTreeNode> result = new List<IDTreeNode>();
            string intronName = String.Join(",", intronList.ToArray());
            AreSimilarIDs areSimilar = delegate(ID candidate, ID innerID) 
            {
                List<string> path1CheckList = MakeCFICheckList(intronList, candidate, idListWithoutCFI);
                List<string> path2CheckList = MakeCFICheckList(intronList, innerID, idListWithoutCFI);

                if (path1CheckList.Count == 0 || path2CheckList.Count == 0)
                    return false;

                ICompareID compareID = new ICompareID();
                if (CompareCFIsInDirectories(compareID, path1CheckList, path2CheckList))
                {
                    AddToSameGroup(result, intronName, intronName, candidate, innerID);
                    return true;
                }
                return false;
            };
            OnNoMatch onNoMatch = delegate(ID candidate)
            {
                IDTreeNode node = new IDTreeNode(intronName, candidate);
                result.Add(node);

                MakeCFICheckList(intronList, candidate, idListWithoutCFI);
            };
            CompareEach(idGroup, areSimilar, onNoMatch);

            return result;
        }

        private List<string> MakeCFICheckList(List<string> currentIntrons, ID idDB,
            List<ID> idListWithoutCFI)
        {
            string id = idDB.Header.ID.ToUpper().Trim();
            List<string> result = new List<string>();

            foreach (string intron in currentIntrons)
            {
                int intronIndex = _intronList.GetTagNum(intron);
                int count = 0;
                bool isFound = true;

                while (isFound)
                {
                    string path = CfiGenerator2.GetIntronTaggedFileName(_tempDirs[id], id, intronIndex, count);

                    isFound = File.Exists(path);
                    if (isFound)
                    {
                        result.Add(path);
                    }
                    count++;
                }
                if (result.Count > 0)
                {
                    break;
                }
            }
            if (result.Count == 0 && !idListWithoutCFI.Contains(idDB))
                idListWithoutCFI.Add(idDB);
            return result;
        }

        private void AddToSameGroup(List<IDTreeNode> groups, string name1, string name2, ID db1, ID db2)
        {
            bool hasOneOrTwo = false;

            foreach (IDTreeNode group in groups)
            {
                bool hasOne = group.IDList.Contains(db1);
                bool hasTwo = group.IDList.Contains(db2);

                if (hasOneOrTwo && (hasOne || hasTwo))
                    //throw new Exception("ID duplicate found in group");
                    break;

                if (!hasOneOrTwo && hasOne && !hasTwo)
                {
                    group.IDList.Add(db2);
                    hasOneOrTwo = true;
                }
                else if (!hasOneOrTwo && !hasOne && hasTwo)
                {
                    group.IDList.Add(db1);
                    hasOneOrTwo = true;
                }
                else if (!hasOneOrTwo && hasOne && hasTwo)
                {
                    hasOneOrTwo = true;
                }
            }
            if (!hasOneOrTwo)
            {
                string comboName = (name1 == name2) ? name1 : name1 + " & " + name2;

                IDTreeNode newNode = new IDTreeNode(comboName, db1);

                newNode.IDList.Add(db2);
                groups.Add(newNode);
            }
        }

        private void OutputCFIs(bool remove, List<string> intronList, ID id)
        {
            try
            {
                string idString = id.Header.ID;
                IFilePathManager tempPath = new TempFilePathOnlyParamDir(remove, _input.WorkDir + idString);

                tempPath.ClearFiles();
                _tempDirs[idString] = tempPath;
                CfiGenerator2 cfiGenerator = new CfiGenerator2(id);
                cfiGenerator.GenerateCfiFilesWithIntronTagNames(intronList, tempPath);
            }
            catch (Exception ex)
            {
                CommonCode.Log.WriteLine("Failed generate Cfi's for {0} {1}", (string)id.Header.ID, ex.Message);
            }
        }

        private static bool CompareCFIsInDirectories(ICompareID compareID, List<string> path1, List<string> path2)
        {
            int compResult = 0;
            string rbLabel, pickLabel;
            string RXData = "", TXData = "";
            CFIInfo RXCFIInfo = null, TXCFIInfo = null;
            List<string> errorMessages = null;
            foreach (string file1 in path1)
            {
                foreach (string file2 in path2)
                {
                    //compareID.DoCompare(file1, file2, out compResult);
                    compareID.DoCompare(file1,file2,out compResult,
                                     out rbLabel, out pickLabel,
                                     out RXData, out TXData, out RXCFIInfo, out TXCFIInfo,
                                     CFIInfo.MASK_TOGGLE_BITS, -1);

                    /*if (compResult != 0)
                    {
                        return true;
                    }*/
                    if (compResult > 0) {
                        if (compareID.CompareCFIInfo(RXCFIInfo, TXCFIInfo, out errorMessages)) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsExecSimilar(ID id1, ID id2, List<List<int>> execList)
        {
            int exec1 = id1.Header.Executor_Code;
            int exec2 = id2.Header.Executor_Code;

            if (exec1 == exec2)
                return true;

            foreach (List<int> similarExecs in execList)
            {
                if (similarExecs.Contains(exec1) &&
                    similarExecs.Contains(exec2))
                    return true;
            }

            return false;
        }
    }
}
