using System;
using System.Collections.Generic;
using System.Text;

namespace SimilarIDKeyMatcher
{
    class IntronListWithTagging
    {
        List<string> _allIntronList = new List<string>();

        internal IntronListWithTagging(List<List<string>> input)
        {
            foreach (List<string> priorityList in input)
            {
                _allIntronList.AddRange(priorityList);
            }
        }

        internal List<string> GetList()
        {
            return _allIntronList;
        }

        internal int GetTagNum(string intron)
        {
            int index = _allIntronList.IndexOf(intron);

            if (index < 0)
                throw new Exception("intron missing in intron list");

            return index;
        }
    }
}
