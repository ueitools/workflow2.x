// Needs ZipHelper.cs -> ref to ICSharpCode.SharpZipLib
// Needs Configuration.cs
//

using System;
using System.IO;
using CommonCode;

namespace SimilarIDKeyMatcher {
    public class TempFilePathOnlyParamDir : IFilePathManager {
        private string _currentWorkingPath;
        IMessageHandler _messageHandler;
        bool _isTempDir = true;

        public TempFilePathOnlyParamDir() {
            _currentWorkingPath = string.Empty;
        }

        public TempFilePathOnlyParamDir(bool removalOverride, string path) {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            _currentWorkingPath = path;
            _isTempDir = removalOverride;
        }

        ~TempFilePathOnlyParamDir()
        {
            if (_isTempDir)
                Dispose();
        }

        public void Dispose() {
            if (string.IsNullOrEmpty(_currentWorkingPath)) {
                return;
            }

            if (Directory.Exists(_currentWorkingPath)) {
                if (CanDelete(_currentWorkingPath)) {
                    try {
                        Directory.Delete(_currentWorkingPath, true);
                    } catch (Exception x) {
                        _messageHandler.ShowMessage(string.Format("Error removing {0}\n\n{1}", _currentWorkingPath, x.Message));
                    }
                }
                _currentWorkingPath = string.Empty;
            }

            GC.SuppressFinalize(this);
        }

        private bool CanDelete(string cdw) {
            return Directory.Exists(cdw) && cdw != GetWorkingDirectory();
        }

        public string CurrentWorkingPath {
            get {
                if (string.IsNullOrEmpty(_currentWorkingPath)) {
                    SetSubDirectory(null);
                }
                return _currentWorkingPath;
            }
        }

        public void SetSubDirectory(string subDirectory) {
            if (string.IsNullOrEmpty(_currentWorkingPath)) {
                string tempFilepath = GetWorkingDirectory();
                if (subDirectory != null) {
                    if (subDirectory == string.Empty) {
                        tempFilepath += Path.GetRandomFileName() + "\\";
                    } else {
                        tempFilepath += subDirectory + "\\";
                    }
                }
                _currentWorkingPath = tempFilepath;
            }

            if (CanDelete(_currentWorkingPath)) {
                Directory.Delete(_currentWorkingPath, true);
            }
            Directory.CreateDirectory(_currentWorkingPath);
        }

        public string GetWorkingDirectory() {
            string workpath = _currentWorkingPath;
            if (!string.IsNullOrEmpty(workpath) && !workpath.EndsWith("\\")) {
                workpath += "\\";
            }

            return workpath;
        }

        public string[] GetFiles(string searchPattern) {
            return Directory.GetFiles(CurrentWorkingPath, searchPattern);
        }

        public string CopyFileFrom(string sourcePath, string destinationFilename) {
            string destinationPath = CurrentWorkingPath + destinationFilename;
            File.Copy(sourcePath, destinationPath);
            return destinationPath;
        }

        public void CopyToWorking(string searchPattern) {
            string[] files = GetFiles(searchPattern);
            foreach (string fileName in files) {
                File.Copy(fileName, GetWorkingDirectory() + Path.GetFileName(fileName), true);
            }
        }

        public void DeleteFile(string file) {
            if (File.Exists(file)) {
                File.Delete(file);
            }
        }

        public bool ClearFiles() {
            if (string.IsNullOrEmpty(_currentWorkingPath)) {
                return true;
            }

            if (Directory.Exists(_currentWorkingPath) == false) {
                return true;
            }

            string[] files = Directory.GetFiles(_currentWorkingPath);
            foreach (string file in files) {
                DeleteFile(file);
            }

            return true;
        }

        public void ZipTo(string targetPath) {
            throw new Exception("Not implemented");
        }

        public bool Unzip(string zipPath) {
            throw new Exception("Not implemented");
        }

        public string GenerateTempFilename(string filename, string prefix, string suffix) {
            if (string.IsNullOrEmpty(filename)) {
                filename = Path.GetFileName(Path.GetTempFileName());
            }
            string baseFilename = Path.GetFileName(filename);
            string baseExtension = Path.GetExtension(filename);
            string tempFilename = string.Format("{0}{1}{2}{3}", prefix, Path.GetFileNameWithoutExtension(baseFilename), suffix,
                                                (baseExtension != string.Empty) ? baseExtension : ".tmp");

            tempFilename = EnsureValidFilename(tempFilename);

            return tempFilename;
        }

        private string EnsureValidFilename(string tempFilename) {
            foreach (char invalidFileNameChar in Path.GetInvalidFileNameChars()) {
                tempFilename = tempFilename.Replace(invalidFileNameChar, '_');
            }
            return tempFilename;
        }

        public string GetNewCaptureName(int tn, int capNum) 
        {
            throw new Exception("Not implemented");
        }

        public string GetNewCaptureName(string id, int capNum, bool oneMHz) 
        {
            throw new Exception("Not implemented");
        }

        public string MakeFullPath(string fileName) {
            return GetWorkingDirectory() + fileName;
        }

        public void SetMessageHandler(IMessageHandler messageHandler)
        {
            _messageHandler = messageHandler;
        }
    }
}
