using System;
using System.Collections.Generic;
using System.Text;
using BusinessObject;

namespace SimilarIDKeyMatcher
{
    using IntronIDList = KeyValuePair<List<string>, List<ID>>;

    class SimilarityData
    {
        IDTreeNode _tree;
        List<IntronIDList> _idsWithoutIntrons;

        internal SimilarityData(IDTreeNode root)
        {
            _tree = root;
            _idsWithoutIntrons = new List<IntronIDList>();
        }

        internal IDTreeNode Tree { get { return _tree; } }

        internal List<ID> GetMissingIDs(int index, out List<string> intron)
        {
            intron = _idsWithoutIntrons[index].Key;

            return _idsWithoutIntrons[index].Value;
        }

        internal int CountMissingIntrons
        {
            get { return _idsWithoutIntrons.Count; }
        }

        internal void AddMissingIDs(List<string> intron, List<ID> idList)
        {
            // check if intron already exists then just add the list to the intron pair
            bool found = false;
            foreach (IntronIDList iPair in _idsWithoutIntrons)
            {
                if (iPair.Key == intron)
                {
                    iPair.Value.AddRange(idList);
                    found = true;
                }
            }
            if(!found)
            {
                IntronIDList pair = new IntronIDList(intron, idList);
                _idsWithoutIntrons.Add(pair);
            }
        }

        internal int CountIDsWithData()
        {
            return _tree.IDList.Count - CountAllIDsWithoutIntrons();
        }

        internal bool IsANoDataID(string id)
        {
            string anyIntron = null;
            return IsANoDataID(id, anyIntron);
        }

        internal bool IsANoDataID(string targetParam, string intron)
        {
            bool isFound = false;
            string target = targetParam.Trim();
            bool ignoreCase = true;

            foreach (IntronIDList idList in _idsWithoutIntrons)
            {
                if (!IsMatchingIntron(idList.Key, intron))
                    continue;
                foreach (ID id in idList.Value)
                {
                    if (String.Compare(id.Header.ID.Trim(), target, ignoreCase) == 0)
                    {
                        isFound = true;
                        break;
                    }
                }
            }
            return isFound;
        }

        private bool IsMatchingIntron(IList<string> intronList, string intron)
        {
            return String.IsNullOrEmpty(intron) || intronList.Contains(intron);
        }

        private int CountAllIDsWithoutIntrons()
        {
            int count = 0;

            _idsWithoutIntrons.ForEach(delegate(IntronIDList idList)
            {
                count += idList.Value.Count;
            });
            return count;
        }
    }
}
