using System;
using System.Windows.Forms;
using System.Diagnostics;

namespace SimilarIDKeyMatcher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(ExceptionHandler);

            InputParameters input = new InputParametersGUI();

            if (!input.Get())
                return;

            string filename = OutputGUI.SelectSaveFile();
            if (String.IsNullOrEmpty(filename))
                return;

            IDMatcher matcher = new IDMatcher(input);
            progressForm Progress = new progressForm();
            Progress.Show();
            matcher.setupProgress += new SetUpProgressDelegate(Progress.SetupProgressBar);
            matcher.updateProgress += new UpateProgressDelegate(Progress.UpdateProgressBar);
            Stopwatch stopwatch = Stopwatch.StartNew();
            SimilarityData similarIDs = matcher.ReportSimilarities();
            Progress.SetupProgressBar(0, 1);
            Progress.UpdateProgressBar(0, "Generating Report");
            OutputGUI.OutputFile(similarIDs, filename, input.Introns.Count);
            Progress.Close();
            stopwatch.Stop();
            MessageBox.Show("Elapsed time: " + stopwatch.Elapsed,"SimilarIDKeyMatcher");
        }

        static void ExceptionHandler(object obj, UnhandledExceptionEventArgs eventArgs)
        {
            Exception e = (Exception)eventArgs.ExceptionObject;
            MessageBox.Show("Error with program:\n" + e.Message + 
                "\n\nDetails:" + e.InnerException + 
                "\n\nStack Trace:" + e.StackTrace);
        }
    }
}