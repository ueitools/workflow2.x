using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using CommonCode;
using CarlosAg.ExcelXmlWriter;
using System.Windows.Forms;

namespace SimilarIDKeyMatcher
{
    class OutputGUI
    {
        static public string SelectSaveFile()
        {            
            SaveFileDialog saveLogFileDialog = new SaveFileDialog();

            saveLogFileDialog.FileName = "report";
            saveLogFileDialog.Filter = "Excel File (*.xls) | *.xls";
            saveLogFileDialog.DefaultExt = "xls";
            saveLogFileDialog.AddExtension = true;
            DialogResult userChoice = saveLogFileDialog.ShowDialog();
            if (userChoice == DialogResult.OK)
            {
                bool isUsed;
                isUsed = FileInUse(saveLogFileDialog.FileName);
                if (isUsed)
                {
                    String strmsg = "File is in Use. Close " + saveLogFileDialog.FileName + " and try again";
                    MessageBox.Show(strmsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return string.Empty;
                }
                return saveLogFileDialog.FileName;
            }
            return string.Empty;
        }

        static internal void OutputFile(SimilarityData treeData, string saveFilePath, int maxDepth)
        {
            if (treeData == null)
                return;

            ReportClass report = new ReportClass(treeData);
            Workbook book = report.MakeReport(maxDepth);
            book.Save(saveFilePath);
        }

        static bool FileInUse(string path)
        {
            try
            {
                //Just opening the file as open/create
                using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    //If required we can check for read/write by using fs.CanRead or fs.CanWrite
                }
                return false;
            }
            catch (IOException)
            {
                return true;

            }
        }
    }
}
