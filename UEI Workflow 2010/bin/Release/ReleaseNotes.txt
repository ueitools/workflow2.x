UEI Workflow 2010
------------------------------------------
Release Version : V2.35
Build Version   : V2.35.5360.24480
Update Date     : 12/1/2015
StarTeam Label  : WF2010.2.35

Features:
1. ID verification UI issue fixed.
2. Fixed Label Intron dictionary Form Grid Row selection issue.

UEI Workflow 2010
------------------------------------------
Release Version : V2.34
Build Version   : V2.34.5360.24480
Update Date     : 4/12/2014
StarTeam Label  : WF2010.2.34

Features:
1. Uncomfotable UI view in different screen resolution issue is fixed.


UEI Workflow 2010
------------------------------------------
Release Version : V2.32
Build Version   : V2.32.5360.24480
Update Date     : 04/10/2014
StarTeam Label  : WF2010.2.32

Features:
1. Functions Loading in IR Maestro, ID Verification is fixed
2. Fixed DOT-DASH Key Capturing in IR Maestro

################################################################################
UEI Workflow 2010
------------------------------------------
Release Version : V2.31
Build Version   : V2.31.5311.29708
Update Date     : 17/07/2014
StarTeam Label  : WF2010 2.31

Features:
1. Functions Loading in IR Maestro, ID Verification is fixed
2. Fixed DOT-DASH Key Capturing in IR Maestro

################################################################################
Release Version : V2.3
Build Version   : V2.3.5270.30614
Update Date     : 06/06/2014
StarTeam Label  : WF2010 2.3

Features:
1. Issue fixed releated to "ID By Brand Report" for Unique Models based on Standard or Former Brand
2. Updated to New Data Base Server Settings
3. Fixed issues releated to Domain Changes
4. Added New Model Info Reports for QuickSet Project Support
5. Updated Label and Intron Search for Brand Support
6. New Tool is added to Support Importing EDID data

UEI Workflow 2010 (Unification implemented)
---------------------------
Release Version : V2.20
Build Version   : V2.20.4937.22931
Update Date      : 08/07/2013
StarTeam Label  : WF.2.2

Features:

1. Added alias brand and alias type to model info report.
2. Same is added to IDView.

################################################################################
Release Version : V2.20
Build Version   : V2.20.4807.20354
Update Date      : 28/02/2013
StarTeam Label  : WF.2.20.Beta3

Fixes:

1. Issue related to displaying of main devices in IDView.

New Features:

1.Vizio Primax encryption-decryption integrated with Workflow2010. Go to "Tools > Encrypt and Decrypt".

###########################################################################################
Release Version : V2.20
Build Version   : V2.20.4785.20767
Update Date      : 06/02/2013
StarTeam Label  : WF.2.20.Beta2

Fixes:

1. Issue related to CF in data sources is fixed.
#########################################################################################
Release Version : V2.20
Build Version   : V2.20.4737.25211
Update Date      : 20/12/2012
StarTeam Label  : WF.2.20.Beta1

Features :

1. Workfow 2010 V2.20 connects to UEI2 with unified location and devices implemented.
2. New GUI supporting unified location and devices is implemented.
3. Added Base6 - 5digit and Base6-6digit to setup code format list for BSC report.
4. Provided option to switch between base conversions starting with 0 and with 1.

