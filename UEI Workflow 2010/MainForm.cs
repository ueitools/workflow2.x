using System;
using System.Windows.Forms;
using System.Configuration;
using System.Diagnostics;
using BusinessObject;
using DBReports;
using CommonForms;
using UeiWorkflow2010Report = UEI.Workflow2010.Report;


namespace UEI_Workflow_2006
{
    /// <summary>
    ///
    /// </summary>
    public partial class MainForm : Form
    {
        Process process = null;

        private  const string IRMaestro_IDToFIKey = "ID_TO_FI";

        public MainForm() {
            InitializeComponent();

            string adminList = ConfigurationSettings.AppSettings["DBADMINISTRATOR"].ToUpper();
            string user = SystemInformation.UserName.ToUpper();

            string domain = SystemInformation.UserDomainName;
            if (domain != "UEIC") {
                checkInIDToolStripMenuItem.Enabled = false;
                batchIDCheckInToolStripMenuItem.Enabled = false;
                checkInTNToolStripMenuItem.Enabled = false;
                renameIDToolStripMenuItem.Enabled = false;
                importIDFromXMLToolStripMenuItem.Enabled = false;
                deleteIDFromMainDBToolStripMenuItem.Enabled = false;
                checkInAllIDsForExecutorToolStripMenuItem.Enabled = false;
                updateIDDataToolStripMenuItem.Enabled = false;
                updateIDIntronToolStripMenuItem.Enabled = false;
                updateIDLabelToolStripMenuItem.Enabled = false;
                releaseLockedIDToolStripMenuItem.Enabled = false;
                releaseLockedTNToolStripMenuItem.Enabled = false;
                importDeviceDataToolStripMenuItem.Enabled = false;
                synchronizeToolStripMenuItem.Enabled = false;
            }

            if (adminList.Contains(user))
                deleteIDFromMainDBToolStripMenuItem.Visible = true;

        }

        /// <summary>
        /// Editor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void eidtTNToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBEditor", "TN");
        }

        private void editIDToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBEditor", "ID");
        }

        /// <summary>
        /// Viewer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void iDToolStripMenuItem_Click(object sender, EventArgs e)
        {
           //process = Process.Start("IDView");
            //process = Process.Start(@"D:\UEIWorks\UEITools\Workflow2010\UEI Workflow 2010\bin\Release\IDView.exe");
            IDView.IDFormView idView = new IDView.IDFormView("", 0);
            idView.Show();
        }

        /// <summary>
        /// ID Level Edit (Data, Intron (Priority), Label)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateIDDataToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBEditor", "DATA");
        }

        private void updateIDIntronToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = process = Process.Start("DBEditor", "INTRON");
        }

        private void updateIDLabelToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBEditor", "LABEL");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void releaseLockedIDToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBEditor", "UNLOCK_ID");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void releaseLockedTNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("DBEditor", "UNLOCK_TN");
        }

        private void setWorkingDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CommonForms.Configuration.SetWorkingDirectory();
        }

        private void qAStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm qaStatus = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.QASTATUS,this);            
        }

        private void findDisconnectedTNsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm disconnectedtn = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.DISCONNECTED_TNS, this);
        }
        private void ambiguousIntronsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm duplicateIntrons = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.DUPLICATE_INTRONS, this);
        }

        private void iDCheckoutConflictToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportForm form = new ReportForm(ReportType.CHECKOUT_CONFLICT);
            form.Show();
        }

        private void synthesizerTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ReportForm form = new ReportForm(ReportType.SYNTHESIZER_TABLE);
            //form.Show();
            UEI.Workflow2010.Report.ReportMainForm reportForm = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.SYNTHESIZER_TABLE,this);
        }       
       
        private void uniqueFunctionIRCountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm uniquefuncir = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.UNIQUE_FUNC_IR,this);
        }

        private void brandSetupSRCFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm brandromtableReport = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.BRAND_TABLE_ROM, this);
        }

        /////////////////////////////////////////////////////////////////////
        ///
        /// Check In and Out
        ///
        private void exportIDToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBTransfer", "ID Export");
        }

        private void createIDToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBTransfer", "ID Create");
        }

        private void batchIDCheckInToolStripMenuItem_Click(
           object sender, EventArgs e)
        {
            process = Process.Start("DBTransfer", "ID BatchCheckIn");
        }

        private void batchIDCheckOutToolStripMenuItem_Click(
           object sender, EventArgs e)
        {
            process = Process.Start("DBTransfer", "ID BatchCheckOut");
        }

        private void checkOutIDToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBTransfer", "ID CheckOut");
        }

        private void checkInIDToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBTransfer", "ID CheckIn");
        }

        private void undoIDCheckoutToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBTransfer", "ID RevertEditsAndUnlock");
        }

        private void checkOutAllIDsForExecutorToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBTransfer", "Executor CheckOut");
        }

        private void checkInAllIDsForExecutorToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBTransfer", "Executor CheckIn");
        }

        private void checkOutTNToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBTransfer", "TN CheckOut");
        }

        private void checkInTNToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBTransfer", "TN CheckIn");
        }

        private void deleteIDFromMainDBToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBTransfer", "ID Delete");
        }

        private void synchronizeToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBTransfer", "Table Synchronize");
        }

        private void importIDFromXMLToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBTransfer", "XML ImportPartialID");
        }

        private void pickToFIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("IRMaestro", "PICK_TO_FI");
        }

        private void readBackVerificationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("IRMaestro", "READ_BACK_VERIFICATION");
        }

        private void readBackResultsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("IRMaestro", "READ_BACK_RESULT");
        }

        private void decodeToolStripMenuItem_Click(object sender, EventArgs e) {
            process = Process.Start("IRMaestro", "DECODE_IR");
        }

        private void iRVerificationToolStripMenuItem_Click(object sender, EventArgs e) {
            process = Process.Start("IRMaestro", "IR_VERIFICATION");
        }

        private void macroAnalyzerToolStripMenuItem_Click(object sender, EventArgs e) {
            process = Process.Start("IRMaestro", "MACRO_ANALYZER");
        }

        private void modelNumberSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("DBSearch", "Model Number Search");
        }

        private void labelSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.IntronFilterForm m_IntronFilterForm = new UEI.Workflow2010.Report.IntronFilterForm();
            m_IntronFilterForm.ShowDialog();
            if(m_IntronFilterForm.IncludeBrand == true)
            {
                UeiWorkflow2010Report.ReportMainForm reportMain = new UEI.Workflow2010.Report.ReportMainForm(UeiWorkflow2010Report.ReportType.LabelSearchWithBrand, this);
            }
            else
            {
                UeiWorkflow2010Report.ReportMainForm reportMain = new UEI.Workflow2010.Report.ReportMainForm(UeiWorkflow2010Report.ReportType.LabelSearch, this);
            }
        }

        private void intronSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.IntronFilterForm m_IntronFilterForm = new UEI.Workflow2010.Report.IntronFilterForm();
            m_IntronFilterForm.ShowDialog();
            if(m_IntronFilterForm.IncludeBrand == true)
            {
                UeiWorkflow2010Report.ReportMainForm reportMain = new UEI.Workflow2010.Report.ReportMainForm(UeiWorkflow2010Report.ReportType.IntronSearchWithBrand, this);
            }
            else
            {
                UeiWorkflow2010Report.ReportMainForm reportMain = new UEI.Workflow2010.Report.ReportMainForm(UeiWorkflow2010Report.ReportType.IntronSearch, this);
            }
        }

        private void bVDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccessRestrictForm AccessRestrict = new AccessRestrictForm();
            DialogResult result = AccessRestrict.ShowDialog();
            AccessRestrict.Close();
            if (result == DialogResult.OK)
            {
                process = Process.Start("BVDataImporter");
            }
            else if (result == DialogResult.No)
            {
                MessageBox.Show("Password Incorrect.",
                    "Password Error", MessageBoxButtons.OK);
            }

        }

        private void importDeviceDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccessRestrictForm AccessRestrict = new AccessRestrictForm();
            DialogResult result = AccessRestrict.ShowDialog();
            AccessRestrict.Close();
            if (result == DialogResult.OK)
            {
                process = Process.Start("BVDataImporter");
            }
            else if (result == DialogResult.No)
            {
                MessageBox.Show("Password Incorrect.",
                    "Password Error", MessageBoxButtons.OK);
            }
        }

        private void logNewRemote_Click(object sender, EventArgs e) {
            process = Process.Start("DBEditor", "ADD_REMOTE");
        }

        private void renameIDToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            process = Process.Start("DBEditor", "UPDATE_ID");
        }

        private void imageBuilderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("ImageCraft", "");
        }

        private void imageLoaderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("ImageLoader", "");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (process != null && !process.HasExited )
            {
                process.Kill();
            }
            this.Close();
        }

        private void iRDBSearchEngineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("IRDBSearchEngine", "");
        }

        private void imageBuilderBatchModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("ImageCraft", "BatchMode");
        }

        private void tNCompToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("IRDBSearchEngine", "ShowResult");
        }

        private void importUSBCaptureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("USBCaptureImport");
        }

        private void splitLongCapturesMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("SplitCap");
        }

        private void similarIDReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("SimilarIDKeyMatcher");
        }

        private void iDCompareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("IRMaestro", "ID_COMPARE");
        }

        private void iDToFIToolStripMenuItem_Click(object sender, EventArgs e)
        {
           process = Process.Start("IRMaestro", IRMaestro_IDToFIKey);
        }

        #region [DBMIntegration - 11/18/2010]
        
        private void iDSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm idSelectionRpt = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.IDList, this);
        }

        private void iDByBrandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm idBrandRpt = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.IDBrandList,this);
        }

        private void setupCodeInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm setupCodeRpt = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.BSC,this);
        }
        private void modelInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm modelInfoRpt = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.ModelInformation, this);

        }
        private void modelInfoArdKeyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm modelInfoRpt = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.ModelInfoWithArdKey, this);
        }       
        private void timingInspectorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("IRMaestro", "TIMING_INSP");
        }

        private void timingInspectorResultsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            process = Process.Start("IRMaestro", "TIMING_INS_REP");
        }

       
        #endregion

        private void eXIDTNInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm reportForm = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.EXIDTNInfo, this);
           
        }
        private void restrictedIDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm reportForm = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.RestrictedIDList, this);
        }
        private void nonemptyIDsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm reportForm = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.NONEMPTY_ID, this);            
        }

        private void emptyIDsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm reportForm = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.EMPTY_ID, this);
        }
        private void brandBasedSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm reportForm = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.BrandBasedSearch, this);
        }

        private void brandModelSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm reportForm = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.BrandModelBasedSearch, this);
        }

        private void abouttoolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox m_About = new AboutBox();
            m_About.Show();
        }

        private void toolBrandSearch_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm reportForm = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.BrandSearch, this);
        }

        private void miscReportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm reportForm = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.MiscReport, this);

        }

        private void encryptAndDecryptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.EncryptDecrypt.AutomateEncryptDecrypt m_EncDec = new UEI.EncryptDecrypt.AutomateEncryptDecrypt();
            m_EncDec.ShowDialog();
        }

        private void exIdMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm reportForm = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.ExecIdMap, this);
        }

        private void emptyDeviceTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm modelInfoRpt = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.ModelInformation_EmptyDevice, this);
        }

        private void exSamePrefixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm modelInfoRpt = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.ExecIdPrefixSame, this);
        }

        #region QuickSet Report
        private void quicksetReport_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm setupCodeRpt = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.QuickSet, this);
        }
        #endregion

        #region Key Functiion Report
        private void keyFunctionReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm modelInfoRpt = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.KeyFunction, this);
        }
        #endregion
       

        #region Supported Ids By Platform
        private void toolSupportedIdsByPlatform_Click(object sender, EventArgs e)
        {
            UEI.Workflow2010.Report.ReportMainForm modelInfoRpt = new UEI.Workflow2010.Report.ReportMainForm(UEI.Workflow2010.Report.ReportType.SupportedIDByPlatform, this);
        }
        #endregion

        #region Service Provider Viewer
        private void serviceProviderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UEI.ServiceProvider.MainForm m_ServiceProvider = new UEI.ServiceProvider.MainForm();
            m_ServiceProvider.Show();
        }
        #endregion       

        private void excludeLabelSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UeiWorkflow2010Report.ReportMainForm reportMain = new UEI.Workflow2010.Report.ReportMainForm(UeiWorkflow2010Report.ReportType.ExcludeLabelSearch, this);
        }

        private void excludeIntronSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UeiWorkflow2010Report.ReportMainForm reportMain = new UEI.Workflow2010.Report.ReportMainForm(UeiWorkflow2010Report.ReportType.ExcludeIntronSearch, this);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            UeiWorkflow2010Report.ReportMainForm reportMain = new UEI.Workflow2010.Report.ReportMainForm(UeiWorkflow2010Report.ReportType.LocationSearch, this);
        }
    }
}