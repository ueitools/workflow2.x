namespace UEI_Workflow_2006
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eidtTNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tNCompToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceProviderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkOutIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkInIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.batchIDCheckInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.batchIDCheckOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.checkOutTNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkInTNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.undoIDCheckoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.createIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importIDFromXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteIDFromMainDBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.checkOutAllIDsForExecutorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkInAllIDsForExecutorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.synchronizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateIDDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateIDIntronToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateIDLabelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.releaseLockedIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.releaseLockedTNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.importDeviceDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.logNewRemote = new System.Windows.Forms.ToolStripMenuItem();
            this.importUSBCapture = new System.Windows.Forms.ToolStripMenuItem();
            this.splitLongCapturesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.encryptAndDecryptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qAStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findDisconnectedTNsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ambiguousIntronsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iDCheckoutConflictToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.synthesizerTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restrictedIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolSupportedIdsByPlatform = new System.Windows.Forms.ToolStripMenuItem();
            this.emptyNonEmptyIDsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nonemptyIDsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emptyIDsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uniqueFunctionIRCountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brandSetupSRCFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iDSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iDByBrandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setupCodeInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quicksetReport = new System.Windows.Forms.ToolStripMenuItem();
            this.modelInfoArdKeyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modelInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emptyDeviceTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eXIDTNInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miscReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exIdMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exSamePrefixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.keyFunctionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setWorkingDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iRMaestroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pickToFIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readBackVerificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readBackResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iRVerificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.macroAnalyzerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.similarIDReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iDCompareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timingInspectorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timingInspectorResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iDToFIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modelNumberSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intronSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolBrandSearch = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.excludeLabelSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excludeIntronSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iRDBSearchEngineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brandBasedSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brandModelSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upgradeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageBuilderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageLoaderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageBuilderBatchModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abouttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.actionToolStripMenuItem,
            this.tableToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.iRMaestroToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.upgradeToolStripMenuItem,
            this.abouttoolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(634, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eidtTNToolStripMenuItem,
            this.editIDToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.editToolStripMenuItem.Enabled = false;
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // eidtTNToolStripMenuItem
            // 
            this.eidtTNToolStripMenuItem.Name = "eidtTNToolStripMenuItem";
            this.eidtTNToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.eidtTNToolStripMenuItem.Text = "TN";
            this.eidtTNToolStripMenuItem.Click += new System.EventHandler(this.eidtTNToolStripMenuItem_Click);
            // 
            // editIDToolStripMenuItem
            // 
            this.editIDToolStripMenuItem.Name = "editIDToolStripMenuItem";
            this.editIDToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.editIDToolStripMenuItem.Text = "Partial ID";
            this.editIDToolStripMenuItem.Click += new System.EventHandler(this.editIDToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iDToolStripMenuItem,
            this.tNCompToolStripMenuItem,
            this.serviceProviderToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // iDToolStripMenuItem
            // 
            this.iDToolStripMenuItem.Name = "iDToolStripMenuItem";
            this.iDToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.iDToolStripMenuItem.Text = "ID";
            this.iDToolStripMenuItem.Click += new System.EventHandler(this.iDToolStripMenuItem_Click);
            // 
            // tNCompToolStripMenuItem
            // 
            this.tNCompToolStripMenuItem.Name = "tNCompToolStripMenuItem";
            this.tNCompToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.tNCompToolStripMenuItem.Text = "TN Comp. Result";
            this.tNCompToolStripMenuItem.Click += new System.EventHandler(this.tNCompToolStripMenuItem_Click);
            // 
            // serviceProviderToolStripMenuItem
            // 
            this.serviceProviderToolStripMenuItem.Name = "serviceProviderToolStripMenuItem";
            this.serviceProviderToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.serviceProviderToolStripMenuItem.Text = "Service Providers";
            this.serviceProviderToolStripMenuItem.Click += new System.EventHandler(this.serviceProviderToolStripMenuItem_Click);
            // 
            // actionToolStripMenuItem
            // 
            this.actionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkOutIDToolStripMenuItem,
            this.checkInIDToolStripMenuItem,
            this.batchIDCheckInToolStripMenuItem,
            this.batchIDCheckOutToolStripMenuItem,
            this.toolStripSeparator2,
            this.checkOutTNToolStripMenuItem,
            this.checkInTNToolStripMenuItem,
            this.toolStripSeparator5,
            this.undoIDCheckoutToolStripMenuItem,
            this.toolStripSeparator4,
            this.createIDToolStripMenuItem,
            this.renameIDToolStripMenuItem,
            this.exportIDToolStripMenuItem,
            this.importIDFromXMLToolStripMenuItem,
            this.toolStripSeparator7,
            this.deleteIDFromMainDBToolStripMenuItem,
            this.toolStripSeparator3,
            this.checkOutAllIDsForExecutorToolStripMenuItem,
            this.checkInAllIDsForExecutorToolStripMenuItem});
            this.actionToolStripMenuItem.Enabled = false;
            this.actionToolStripMenuItem.Name = "actionToolStripMenuItem";
            this.actionToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.actionToolStripMenuItem.Text = "Action";
            // 
            // checkOutIDToolStripMenuItem
            // 
            this.checkOutIDToolStripMenuItem.Name = "checkOutIDToolStripMenuItem";
            this.checkOutIDToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.checkOutIDToolStripMenuItem.Text = "Check Out ID";
            this.checkOutIDToolStripMenuItem.Click += new System.EventHandler(this.checkOutIDToolStripMenuItem_Click);
            // 
            // checkInIDToolStripMenuItem
            // 
            this.checkInIDToolStripMenuItem.Name = "checkInIDToolStripMenuItem";
            this.checkInIDToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.checkInIDToolStripMenuItem.Text = "Check In ID";
            this.checkInIDToolStripMenuItem.Click += new System.EventHandler(this.checkInIDToolStripMenuItem_Click);
            // 
            // batchIDCheckInToolStripMenuItem
            // 
            this.batchIDCheckInToolStripMenuItem.Name = "batchIDCheckInToolStripMenuItem";
            this.batchIDCheckInToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.batchIDCheckInToolStripMenuItem.Text = "Batch ID Check In";
            this.batchIDCheckInToolStripMenuItem.Click += new System.EventHandler(this.batchIDCheckInToolStripMenuItem_Click);
            // 
            // batchIDCheckOutToolStripMenuItem
            // 
            this.batchIDCheckOutToolStripMenuItem.Name = "batchIDCheckOutToolStripMenuItem";
            this.batchIDCheckOutToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.batchIDCheckOutToolStripMenuItem.Text = "Batch ID Check Out";
            this.batchIDCheckOutToolStripMenuItem.Click += new System.EventHandler(this.batchIDCheckOutToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(231, 6);
            // 
            // checkOutTNToolStripMenuItem
            // 
            this.checkOutTNToolStripMenuItem.Name = "checkOutTNToolStripMenuItem";
            this.checkOutTNToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.checkOutTNToolStripMenuItem.Text = "Check Out TN";
            this.checkOutTNToolStripMenuItem.Click += new System.EventHandler(this.checkOutTNToolStripMenuItem_Click);
            // 
            // checkInTNToolStripMenuItem
            // 
            this.checkInTNToolStripMenuItem.Name = "checkInTNToolStripMenuItem";
            this.checkInTNToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.checkInTNToolStripMenuItem.Text = "Check In TN";
            this.checkInTNToolStripMenuItem.Click += new System.EventHandler(this.checkInTNToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(231, 6);
            // 
            // undoIDCheckoutToolStripMenuItem
            // 
            this.undoIDCheckoutToolStripMenuItem.Name = "undoIDCheckoutToolStripMenuItem";
            this.undoIDCheckoutToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.undoIDCheckoutToolStripMenuItem.Text = "Undo ID Checkout";
            this.undoIDCheckoutToolStripMenuItem.Click += new System.EventHandler(this.undoIDCheckoutToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(231, 6);
            // 
            // createIDToolStripMenuItem
            // 
            this.createIDToolStripMenuItem.Name = "createIDToolStripMenuItem";
            this.createIDToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.createIDToolStripMenuItem.Text = "Create ID";
            this.createIDToolStripMenuItem.Click += new System.EventHandler(this.createIDToolStripMenuItem_Click);
            // 
            // renameIDToolStripMenuItem
            // 
            this.renameIDToolStripMenuItem.Name = "renameIDToolStripMenuItem";
            this.renameIDToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.renameIDToolStripMenuItem.Text = "Rename ID";
            this.renameIDToolStripMenuItem.Click += new System.EventHandler(this.renameIDToolStripMenuItem_Click);
            // 
            // exportIDToolStripMenuItem
            // 
            this.exportIDToolStripMenuItem.Name = "exportIDToolStripMenuItem";
            this.exportIDToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.exportIDToolStripMenuItem.Text = "Export ID";
            this.exportIDToolStripMenuItem.Click += new System.EventHandler(this.exportIDToolStripMenuItem_Click);
            // 
            // importIDFromXMLToolStripMenuItem
            // 
            this.importIDFromXMLToolStripMenuItem.Name = "importIDFromXMLToolStripMenuItem";
            this.importIDFromXMLToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.importIDFromXMLToolStripMenuItem.Text = "Import ID From XML";
            this.importIDFromXMLToolStripMenuItem.Click += new System.EventHandler(this.importIDFromXMLToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(231, 6);
            // 
            // deleteIDFromMainDBToolStripMenuItem
            // 
            this.deleteIDFromMainDBToolStripMenuItem.Name = "deleteIDFromMainDBToolStripMenuItem";
            this.deleteIDFromMainDBToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.deleteIDFromMainDBToolStripMenuItem.Text = "Delete ID from Published DB";
            this.deleteIDFromMainDBToolStripMenuItem.Visible = false;
            this.deleteIDFromMainDBToolStripMenuItem.Click += new System.EventHandler(this.deleteIDFromMainDBToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(231, 6);
            // 
            // checkOutAllIDsForExecutorToolStripMenuItem
            // 
            this.checkOutAllIDsForExecutorToolStripMenuItem.Name = "checkOutAllIDsForExecutorToolStripMenuItem";
            this.checkOutAllIDsForExecutorToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.checkOutAllIDsForExecutorToolStripMenuItem.Text = "Check Out All IDs For Executor";
            this.checkOutAllIDsForExecutorToolStripMenuItem.Click += new System.EventHandler(this.checkOutAllIDsForExecutorToolStripMenuItem_Click);
            // 
            // checkInAllIDsForExecutorToolStripMenuItem
            // 
            this.checkInAllIDsForExecutorToolStripMenuItem.Name = "checkInAllIDsForExecutorToolStripMenuItem";
            this.checkInAllIDsForExecutorToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.checkInAllIDsForExecutorToolStripMenuItem.Text = "Check In All IDs For Executor";
            this.checkInAllIDsForExecutorToolStripMenuItem.Click += new System.EventHandler(this.checkInAllIDsForExecutorToolStripMenuItem_Click);
            // 
            // tableToolStripMenuItem
            // 
            this.tableToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.synchronizeToolStripMenuItem});
            this.tableToolStripMenuItem.Enabled = false;
            this.tableToolStripMenuItem.Name = "tableToolStripMenuItem";
            this.tableToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.tableToolStripMenuItem.Text = "Table";
            // 
            // synchronizeToolStripMenuItem
            // 
            this.synchronizeToolStripMenuItem.Name = "synchronizeToolStripMenuItem";
            this.synchronizeToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.synchronizeToolStripMenuItem.Text = "Synchronize";
            this.synchronizeToolStripMenuItem.Click += new System.EventHandler(this.synchronizeToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.updateIDDataToolStripMenuItem,
            this.updateIDIntronToolStripMenuItem,
            this.updateIDLabelToolStripMenuItem,
            this.toolStripSeparator1,
            this.releaseLockedIDToolStripMenuItem,
            this.releaseLockedTNToolStripMenuItem,
            this.toolStripSeparator6,
            this.importDeviceDataToolStripMenuItem,
            this.toolStripSeparator8,
            this.logNewRemote,
            this.importUSBCapture,
            this.splitLongCapturesMenuItem,
            this.toolStripMenuItem1,
            this.encryptAndDecryptToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // updateIDDataToolStripMenuItem
            // 
            this.updateIDDataToolStripMenuItem.Enabled = false;
            this.updateIDDataToolStripMenuItem.Name = "updateIDDataToolStripMenuItem";
            this.updateIDDataToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.updateIDDataToolStripMenuItem.Text = "Update ID Data";
            this.updateIDDataToolStripMenuItem.Click += new System.EventHandler(this.updateIDDataToolStripMenuItem_Click);
            // 
            // updateIDIntronToolStripMenuItem
            // 
            this.updateIDIntronToolStripMenuItem.Enabled = false;
            this.updateIDIntronToolStripMenuItem.Name = "updateIDIntronToolStripMenuItem";
            this.updateIDIntronToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.updateIDIntronToolStripMenuItem.Text = "Update ID Intron";
            this.updateIDIntronToolStripMenuItem.Click += new System.EventHandler(this.updateIDIntronToolStripMenuItem_Click);
            // 
            // updateIDLabelToolStripMenuItem
            // 
            this.updateIDLabelToolStripMenuItem.Enabled = false;
            this.updateIDLabelToolStripMenuItem.Name = "updateIDLabelToolStripMenuItem";
            this.updateIDLabelToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.updateIDLabelToolStripMenuItem.Text = "Update ID Label";
            this.updateIDLabelToolStripMenuItem.Click += new System.EventHandler(this.updateIDLabelToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(181, 6);
            // 
            // releaseLockedIDToolStripMenuItem
            // 
            this.releaseLockedIDToolStripMenuItem.Enabled = false;
            this.releaseLockedIDToolStripMenuItem.Name = "releaseLockedIDToolStripMenuItem";
            this.releaseLockedIDToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.releaseLockedIDToolStripMenuItem.Text = "Release Locked ID";
            this.releaseLockedIDToolStripMenuItem.Click += new System.EventHandler(this.releaseLockedIDToolStripMenuItem_Click);
            // 
            // releaseLockedTNToolStripMenuItem
            // 
            this.releaseLockedTNToolStripMenuItem.Enabled = false;
            this.releaseLockedTNToolStripMenuItem.Name = "releaseLockedTNToolStripMenuItem";
            this.releaseLockedTNToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.releaseLockedTNToolStripMenuItem.Text = "Release Locked TN";
            this.releaseLockedTNToolStripMenuItem.Click += new System.EventHandler(this.releaseLockedTNToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(181, 6);
            // 
            // importDeviceDataToolStripMenuItem
            // 
            this.importDeviceDataToolStripMenuItem.Enabled = false;
            this.importDeviceDataToolStripMenuItem.Name = "importDeviceDataToolStripMenuItem";
            this.importDeviceDataToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.importDeviceDataToolStripMenuItem.Text = "Import Device Data";
            this.importDeviceDataToolStripMenuItem.Click += new System.EventHandler(this.importDeviceDataToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(181, 6);
            // 
            // logNewRemote
            // 
            this.logNewRemote.Enabled = false;
            this.logNewRemote.Name = "logNewRemote";
            this.logNewRemote.Size = new System.Drawing.Size(184, 22);
            this.logNewRemote.Text = "Log New Remote";
            this.logNewRemote.Click += new System.EventHandler(this.logNewRemote_Click);
            // 
            // importUSBCapture
            // 
            this.importUSBCapture.Enabled = false;
            this.importUSBCapture.Name = "importUSBCapture";
            this.importUSBCapture.Size = new System.Drawing.Size(184, 22);
            this.importUSBCapture.Text = "Import USB Captures";
            this.importUSBCapture.Click += new System.EventHandler(this.importUSBCaptureToolStripMenuItem_Click);
            // 
            // splitLongCapturesMenuItem
            // 
            this.splitLongCapturesMenuItem.Enabled = false;
            this.splitLongCapturesMenuItem.Name = "splitLongCapturesMenuItem";
            this.splitLongCapturesMenuItem.Size = new System.Drawing.Size(184, 22);
            this.splitLongCapturesMenuItem.Text = "Split Long Captures";
            this.splitLongCapturesMenuItem.Click += new System.EventHandler(this.splitLongCapturesMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(181, 6);
            // 
            // encryptAndDecryptToolStripMenuItem
            // 
            this.encryptAndDecryptToolStripMenuItem.Name = "encryptAndDecryptToolStripMenuItem";
            this.encryptAndDecryptToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.encryptAndDecryptToolStripMenuItem.Text = "Encrypt and Decrypt";
            this.encryptAndDecryptToolStripMenuItem.Click += new System.EventHandler(this.encryptAndDecryptToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.qAStatusToolStripMenuItem,
            this.findDisconnectedTNsToolStripMenuItem,
            this.ambiguousIntronsToolStripMenuItem,
            this.iDCheckoutConflictToolStripMenuItem,
            this.synthesizerTableToolStripMenuItem,
            this.restrictedIDToolStripMenuItem,
            this.toolSupportedIdsByPlatform,
            this.emptyNonEmptyIDsToolStripMenuItem,
            this.uniqueFunctionIRCountToolStripMenuItem,
            this.brandSetupSRCFileToolStripMenuItem,
            this.iDSelectionToolStripMenuItem,
            this.iDByBrandToolStripMenuItem,
            this.setupCodeInfoToolStripMenuItem,
            this.quicksetReport,
            this.modelInfoArdKeyToolStripMenuItem,
            this.modelInfoToolStripMenuItem,
            this.emptyDeviceTypeToolStripMenuItem,
            this.eXIDTNInfoToolStripMenuItem,
            this.miscReportsToolStripMenuItem,
            this.exIdMapToolStripMenuItem,
            this.exSamePrefixToolStripMenuItem,
            this.keyFunctionReportToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // qAStatusToolStripMenuItem
            // 
            this.qAStatusToolStripMenuItem.Name = "qAStatusToolStripMenuItem";
            this.qAStatusToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.qAStatusToolStripMenuItem.Text = "QA Status";
            this.qAStatusToolStripMenuItem.Click += new System.EventHandler(this.qAStatusToolStripMenuItem_Click);
            // 
            // findDisconnectedTNsToolStripMenuItem
            // 
            this.findDisconnectedTNsToolStripMenuItem.Name = "findDisconnectedTNsToolStripMenuItem";
            this.findDisconnectedTNsToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.findDisconnectedTNsToolStripMenuItem.Text = "Find Disconnected TNs";
            this.findDisconnectedTNsToolStripMenuItem.Click += new System.EventHandler(this.findDisconnectedTNsToolStripMenuItem_Click);
            // 
            // ambiguousIntronsToolStripMenuItem
            // 
            this.ambiguousIntronsToolStripMenuItem.Name = "ambiguousIntronsToolStripMenuItem";
            this.ambiguousIntronsToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.ambiguousIntronsToolStripMenuItem.Text = "Ambiguous Introns";
            this.ambiguousIntronsToolStripMenuItem.Click += new System.EventHandler(this.ambiguousIntronsToolStripMenuItem_Click);
            // 
            // iDCheckoutConflictToolStripMenuItem
            // 
            this.iDCheckoutConflictToolStripMenuItem.Name = "iDCheckoutConflictToolStripMenuItem";
            this.iDCheckoutConflictToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.iDCheckoutConflictToolStripMenuItem.Text = "ID Checkout Conflict";
            this.iDCheckoutConflictToolStripMenuItem.Click += new System.EventHandler(this.iDCheckoutConflictToolStripMenuItem_Click);
            // 
            // synthesizerTableToolStripMenuItem
            // 
            this.synthesizerTableToolStripMenuItem.Name = "synthesizerTableToolStripMenuItem";
            this.synthesizerTableToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.synthesizerTableToolStripMenuItem.Text = "Synthesizer Table";
            this.synthesizerTableToolStripMenuItem.Click += new System.EventHandler(this.synthesizerTableToolStripMenuItem_Click);
            // 
            // restrictedIDToolStripMenuItem
            // 
            this.restrictedIDToolStripMenuItem.Name = "restrictedIDToolStripMenuItem";
            this.restrictedIDToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.restrictedIDToolStripMenuItem.Text = "Restricted ID";
            this.restrictedIDToolStripMenuItem.Click += new System.EventHandler(this.restrictedIDToolStripMenuItem_Click);
            // 
            // toolSupportedIdsByPlatform
            // 
            this.toolSupportedIdsByPlatform.Name = "toolSupportedIdsByPlatform";
            this.toolSupportedIdsByPlatform.Size = new System.Drawing.Size(213, 22);
            this.toolSupportedIdsByPlatform.Text = "Supported Ids By Platform";
            this.toolSupportedIdsByPlatform.Click += new System.EventHandler(this.toolSupportedIdsByPlatform_Click);
            // 
            // emptyNonEmptyIDsToolStripMenuItem
            // 
            this.emptyNonEmptyIDsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nonemptyIDsToolStripMenuItem,
            this.emptyIDsToolStripMenuItem});
            this.emptyNonEmptyIDsToolStripMenuItem.Name = "emptyNonEmptyIDsToolStripMenuItem";
            this.emptyNonEmptyIDsToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.emptyNonEmptyIDsToolStripMenuItem.Text = "Empty/Non-Empty IDs";
            // 
            // nonemptyIDsToolStripMenuItem
            // 
            this.nonemptyIDsToolStripMenuItem.Name = "nonemptyIDsToolStripMenuItem";
            this.nonemptyIDsToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.nonemptyIDsToolStripMenuItem.Text = "Non-empty IDs";
            this.nonemptyIDsToolStripMenuItem.Click += new System.EventHandler(this.nonemptyIDsToolStripMenuItem_Click);
            // 
            // emptyIDsToolStripMenuItem
            // 
            this.emptyIDsToolStripMenuItem.Name = "emptyIDsToolStripMenuItem";
            this.emptyIDsToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.emptyIDsToolStripMenuItem.Text = "Empty IDs";
            this.emptyIDsToolStripMenuItem.Click += new System.EventHandler(this.emptyIDsToolStripMenuItem_Click);
            // 
            // uniqueFunctionIRCountToolStripMenuItem
            // 
            this.uniqueFunctionIRCountToolStripMenuItem.Name = "uniqueFunctionIRCountToolStripMenuItem";
            this.uniqueFunctionIRCountToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.uniqueFunctionIRCountToolStripMenuItem.Text = "Unique Function/IR Count";
            this.uniqueFunctionIRCountToolStripMenuItem.Click += new System.EventHandler(this.uniqueFunctionIRCountToolStripMenuItem_Click);
            // 
            // brandSetupSRCFileToolStripMenuItem
            // 
            this.brandSetupSRCFileToolStripMenuItem.Name = "brandSetupSRCFileToolStripMenuItem";
            this.brandSetupSRCFileToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.brandSetupSRCFileToolStripMenuItem.Text = "Brand Setup .SRC file";
            this.brandSetupSRCFileToolStripMenuItem.Click += new System.EventHandler(this.brandSetupSRCFileToolStripMenuItem_Click);
            // 
            // iDSelectionToolStripMenuItem
            // 
            this.iDSelectionToolStripMenuItem.Name = "iDSelectionToolStripMenuItem";
            this.iDSelectionToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.iDSelectionToolStripMenuItem.Text = "ID Selection";
            this.iDSelectionToolStripMenuItem.Click += new System.EventHandler(this.iDSelectionToolStripMenuItem_Click);
            // 
            // iDByBrandToolStripMenuItem
            // 
            this.iDByBrandToolStripMenuItem.Name = "iDByBrandToolStripMenuItem";
            this.iDByBrandToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.iDByBrandToolStripMenuItem.Text = "ID By Brand";
            this.iDByBrandToolStripMenuItem.Click += new System.EventHandler(this.iDByBrandToolStripMenuItem_Click);
            // 
            // setupCodeInfoToolStripMenuItem
            // 
            this.setupCodeInfoToolStripMenuItem.Name = "setupCodeInfoToolStripMenuItem";
            this.setupCodeInfoToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.setupCodeInfoToolStripMenuItem.Text = "Brand Setup Code";
            this.setupCodeInfoToolStripMenuItem.Click += new System.EventHandler(this.setupCodeInfoToolStripMenuItem_Click);
            // 
            // quicksetReport
            // 
            this.quicksetReport.Name = "quicksetReport";
            this.quicksetReport.Size = new System.Drawing.Size(213, 22);
            this.quicksetReport.Text = "Quick Set";
            this.quicksetReport.Click += new System.EventHandler(this.quicksetReport_Click);
            // 
            // modelInfoArdKeyToolStripMenuItem
            // 
            this.modelInfoArdKeyToolStripMenuItem.Name = "modelInfoArdKeyToolStripMenuItem";
            this.modelInfoArdKeyToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.modelInfoArdKeyToolStripMenuItem.Text = "Model Info(With Ard Key)";
            this.modelInfoArdKeyToolStripMenuItem.Click += new System.EventHandler(this.modelInfoArdKeyToolStripMenuItem_Click);
            // 
            // modelInfoToolStripMenuItem
            // 
            this.modelInfoToolStripMenuItem.Name = "modelInfoToolStripMenuItem";
            this.modelInfoToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.modelInfoToolStripMenuItem.Text = "Model Info";
            this.modelInfoToolStripMenuItem.Click += new System.EventHandler(this.modelInfoToolStripMenuItem_Click);
            // 
            // emptyDeviceTypeToolStripMenuItem
            // 
            this.emptyDeviceTypeToolStripMenuItem.Name = "emptyDeviceTypeToolStripMenuItem";
            this.emptyDeviceTypeToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.emptyDeviceTypeToolStripMenuItem.Text = "Empty Device";
            this.emptyDeviceTypeToolStripMenuItem.Click += new System.EventHandler(this.emptyDeviceTypeToolStripMenuItem_Click);
            // 
            // eXIDTNInfoToolStripMenuItem
            // 
            this.eXIDTNInfoToolStripMenuItem.Name = "eXIDTNInfoToolStripMenuItem";
            this.eXIDTNInfoToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.eXIDTNInfoToolStripMenuItem.Text = "EX/ID/TN Info";
            this.eXIDTNInfoToolStripMenuItem.Click += new System.EventHandler(this.eXIDTNInfoToolStripMenuItem_Click);
            // 
            // miscReportsToolStripMenuItem
            // 
            this.miscReportsToolStripMenuItem.Name = "miscReportsToolStripMenuItem";
            this.miscReportsToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.miscReportsToolStripMenuItem.Text = "Misc Reports";
            this.miscReportsToolStripMenuItem.Click += new System.EventHandler(this.miscReportsToolStripMenuItem_Click);
            // 
            // exIdMapToolStripMenuItem
            // 
            this.exIdMapToolStripMenuItem.Name = "exIdMapToolStripMenuItem";
            this.exIdMapToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.exIdMapToolStripMenuItem.Text = "Ex/Id Map";
            this.exIdMapToolStripMenuItem.Click += new System.EventHandler(this.exIdMapToolStripMenuItem_Click);
            // 
            // exSamePrefixToolStripMenuItem
            // 
            this.exSamePrefixToolStripMenuItem.Name = "exSamePrefixToolStripMenuItem";
            this.exSamePrefixToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.exSamePrefixToolStripMenuItem.Text = "Ex Same Prefix";
            this.exSamePrefixToolStripMenuItem.Click += new System.EventHandler(this.exSamePrefixToolStripMenuItem_Click);
            // 
            // keyFunctionReportToolStripMenuItem
            // 
            this.keyFunctionReportToolStripMenuItem.Name = "keyFunctionReportToolStripMenuItem";
            this.keyFunctionReportToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.keyFunctionReportToolStripMenuItem.Text = "Key Function Report";
            this.keyFunctionReportToolStripMenuItem.Visible = false;
            this.keyFunctionReportToolStripMenuItem.Click += new System.EventHandler(this.keyFunctionReportToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setWorkingDirectoryToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // setWorkingDirectoryToolStripMenuItem
            // 
            this.setWorkingDirectoryToolStripMenuItem.Name = "setWorkingDirectoryToolStripMenuItem";
            this.setWorkingDirectoryToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.setWorkingDirectoryToolStripMenuItem.Text = "Set Working Directory";
            this.setWorkingDirectoryToolStripMenuItem.Click += new System.EventHandler(this.setWorkingDirectoryToolStripMenuItem_Click);
            // 
            // iRMaestroToolStripMenuItem
            // 
            this.iRMaestroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pickToFIToolStripMenuItem,
            this.readBackVerificationToolStripMenuItem,
            this.readBackResultsToolStripMenuItem,
            this.decodeToolStripMenuItem,
            this.iRVerificationToolStripMenuItem,
            this.macroAnalyzerToolStripMenuItem,
            this.similarIDReportToolStripMenuItem,
            this.iDCompareToolStripMenuItem,
            this.timingInspectorToolStripMenuItem,
            this.timingInspectorResultsToolStripMenuItem,
            this.iDToFIToolStripMenuItem});
            this.iRMaestroToolStripMenuItem.Name = "iRMaestroToolStripMenuItem";
            this.iRMaestroToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.iRMaestroToolStripMenuItem.Text = "IRMaestro";
            // 
            // pickToFIToolStripMenuItem
            // 
            this.pickToFIToolStripMenuItem.Name = "pickToFIToolStripMenuItem";
            this.pickToFIToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.pickToFIToolStripMenuItem.Text = "Pick To FI";
            this.pickToFIToolStripMenuItem.Click += new System.EventHandler(this.pickToFIToolStripMenuItem_Click);
            // 
            // readBackVerificationToolStripMenuItem
            // 
            this.readBackVerificationToolStripMenuItem.Name = "readBackVerificationToolStripMenuItem";
            this.readBackVerificationToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.readBackVerificationToolStripMenuItem.Text = "Read Back Verification";
            this.readBackVerificationToolStripMenuItem.Click += new System.EventHandler(this.readBackVerificationToolStripMenuItem_Click);
            // 
            // readBackResultsToolStripMenuItem
            // 
            this.readBackResultsToolStripMenuItem.Name = "readBackResultsToolStripMenuItem";
            this.readBackResultsToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.readBackResultsToolStripMenuItem.Text = "ReadBack Results";
            this.readBackResultsToolStripMenuItem.Click += new System.EventHandler(this.readBackResultsToolStripMenuItem_Click);
            // 
            // decodeToolStripMenuItem
            // 
            this.decodeToolStripMenuItem.Name = "decodeToolStripMenuItem";
            this.decodeToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.decodeToolStripMenuItem.Text = "Decode";
            this.decodeToolStripMenuItem.Click += new System.EventHandler(this.decodeToolStripMenuItem_Click);
            // 
            // iRVerificationToolStripMenuItem
            // 
            this.iRVerificationToolStripMenuItem.Name = "iRVerificationToolStripMenuItem";
            this.iRVerificationToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.iRVerificationToolStripMenuItem.Text = "IR Verification";
            this.iRVerificationToolStripMenuItem.Click += new System.EventHandler(this.iRVerificationToolStripMenuItem_Click);
            // 
            // macroAnalyzerToolStripMenuItem
            // 
            this.macroAnalyzerToolStripMenuItem.Name = "macroAnalyzerToolStripMenuItem";
            this.macroAnalyzerToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.macroAnalyzerToolStripMenuItem.Text = "Macro Analyzer";
            this.macroAnalyzerToolStripMenuItem.Click += new System.EventHandler(this.macroAnalyzerToolStripMenuItem_Click);
            // 
            // similarIDReportToolStripMenuItem
            // 
            this.similarIDReportToolStripMenuItem.Enabled = false;
            this.similarIDReportToolStripMenuItem.Name = "similarIDReportToolStripMenuItem";
            this.similarIDReportToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.similarIDReportToolStripMenuItem.Text = "Similar ID Report";
            this.similarIDReportToolStripMenuItem.Click += new System.EventHandler(this.similarIDReportToolStripMenuItem_Click);
            // 
            // iDCompareToolStripMenuItem
            // 
            this.iDCompareToolStripMenuItem.Name = "iDCompareToolStripMenuItem";
            this.iDCompareToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.iDCompareToolStripMenuItem.Text = "ID Compare";
            this.iDCompareToolStripMenuItem.Click += new System.EventHandler(this.iDCompareToolStripMenuItem_Click);
            // 
            // timingInspectorToolStripMenuItem
            // 
            this.timingInspectorToolStripMenuItem.Name = "timingInspectorToolStripMenuItem";
            this.timingInspectorToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.timingInspectorToolStripMenuItem.Text = "Timing Inspector";
            this.timingInspectorToolStripMenuItem.Click += new System.EventHandler(this.timingInspectorToolStripMenuItem_Click);
            // 
            // timingInspectorResultsToolStripMenuItem
            // 
            this.timingInspectorResultsToolStripMenuItem.Name = "timingInspectorResultsToolStripMenuItem";
            this.timingInspectorResultsToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.timingInspectorResultsToolStripMenuItem.Text = "Timing Inspector Results";
            this.timingInspectorResultsToolStripMenuItem.Click += new System.EventHandler(this.timingInspectorResultsToolStripMenuItem_Click);
            // 
            // iDToFIToolStripMenuItem
            // 
            this.iDToFIToolStripMenuItem.Name = "iDToFIToolStripMenuItem";
            this.iDToFIToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.iDToFIToolStripMenuItem.Text = "ID To FI";
            this.iDToFIToolStripMenuItem.Click += new System.EventHandler(this.iDToFIToolStripMenuItem_Click);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Checked = true;
            this.searchToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.searchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modelNumberSearchToolStripMenuItem,
            this.labelSearchToolStripMenuItem,
            this.intronSearchToolStripMenuItem,
            this.toolBrandSearch,
            this.toolStripMenuItem2,
            this.excludeLabelSearchToolStripMenuItem,
            this.excludeIntronSearchToolStripMenuItem,
            this.iRDBSearchEngineToolStripMenuItem,
            this.brandBasedSearchToolStripMenuItem,
            this.brandModelSearchToolStripMenuItem});
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.searchToolStripMenuItem.Text = "Search";
            // 
            // modelNumberSearchToolStripMenuItem
            // 
            this.modelNumberSearchToolStripMenuItem.Name = "modelNumberSearchToolStripMenuItem";
            this.modelNumberSearchToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.modelNumberSearchToolStripMenuItem.Text = "Model Number Search";
            this.modelNumberSearchToolStripMenuItem.Click += new System.EventHandler(this.modelNumberSearchToolStripMenuItem_Click);
            // 
            // labelSearchToolStripMenuItem
            // 
            this.labelSearchToolStripMenuItem.Name = "labelSearchToolStripMenuItem";
            this.labelSearchToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.labelSearchToolStripMenuItem.Text = "Label Search";
            this.labelSearchToolStripMenuItem.Click += new System.EventHandler(this.labelSearchToolStripMenuItem_Click);
            // 
            // intronSearchToolStripMenuItem
            // 
            this.intronSearchToolStripMenuItem.Name = "intronSearchToolStripMenuItem";
            this.intronSearchToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.intronSearchToolStripMenuItem.Text = "Intron Search";
            this.intronSearchToolStripMenuItem.Click += new System.EventHandler(this.intronSearchToolStripMenuItem_Click);
            // 
            // toolBrandSearch
            // 
            this.toolBrandSearch.Name = "toolBrandSearch";
            this.toolBrandSearch.Size = new System.Drawing.Size(193, 22);
            this.toolBrandSearch.Text = "Brand Search";
            this.toolBrandSearch.Click += new System.EventHandler(this.toolBrandSearch_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem2.Text = "Location Search";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // excludeLabelSearchToolStripMenuItem
            // 
            this.excludeLabelSearchToolStripMenuItem.Name = "excludeLabelSearchToolStripMenuItem";
            this.excludeLabelSearchToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.excludeLabelSearchToolStripMenuItem.Text = "Exclude Label Search";
            this.excludeLabelSearchToolStripMenuItem.Click += new System.EventHandler(this.excludeLabelSearchToolStripMenuItem_Click);
            // 
            // excludeIntronSearchToolStripMenuItem
            // 
            this.excludeIntronSearchToolStripMenuItem.Name = "excludeIntronSearchToolStripMenuItem";
            this.excludeIntronSearchToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.excludeIntronSearchToolStripMenuItem.Text = "Exclude Intron Search";
            this.excludeIntronSearchToolStripMenuItem.Click += new System.EventHandler(this.excludeIntronSearchToolStripMenuItem_Click);
            // 
            // iRDBSearchEngineToolStripMenuItem
            // 
            this.iRDBSearchEngineToolStripMenuItem.Name = "iRDBSearchEngineToolStripMenuItem";
            this.iRDBSearchEngineToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.iRDBSearchEngineToolStripMenuItem.Text = "IR DB Search Engine";
            this.iRDBSearchEngineToolStripMenuItem.Click += new System.EventHandler(this.iRDBSearchEngineToolStripMenuItem_Click);
            // 
            // brandBasedSearchToolStripMenuItem
            // 
            this.brandBasedSearchToolStripMenuItem.Enabled = false;
            this.brandBasedSearchToolStripMenuItem.Name = "brandBasedSearchToolStripMenuItem";
            this.brandBasedSearchToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.brandBasedSearchToolStripMenuItem.Text = "Brand Based Search";
            this.brandBasedSearchToolStripMenuItem.Visible = false;
            this.brandBasedSearchToolStripMenuItem.Click += new System.EventHandler(this.brandBasedSearchToolStripMenuItem_Click);
            // 
            // brandModelSearchToolStripMenuItem
            // 
            this.brandModelSearchToolStripMenuItem.Enabled = false;
            this.brandModelSearchToolStripMenuItem.Name = "brandModelSearchToolStripMenuItem";
            this.brandModelSearchToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.brandModelSearchToolStripMenuItem.Text = "Brand/Model Search";
            this.brandModelSearchToolStripMenuItem.Click += new System.EventHandler(this.brandModelSearchToolStripMenuItem_Click);
            // 
            // upgradeToolStripMenuItem
            // 
            this.upgradeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imageBuilderToolStripMenuItem,
            this.imageLoaderToolStripMenuItem,
            this.imageBuilderBatchModeToolStripMenuItem});
            this.upgradeToolStripMenuItem.Enabled = false;
            this.upgradeToolStripMenuItem.Name = "upgradeToolStripMenuItem";
            this.upgradeToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.upgradeToolStripMenuItem.Text = "Upgrade";
            // 
            // imageBuilderToolStripMenuItem
            // 
            this.imageBuilderToolStripMenuItem.Name = "imageBuilderToolStripMenuItem";
            this.imageBuilderToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.imageBuilderToolStripMenuItem.Text = "Image Builder";
            this.imageBuilderToolStripMenuItem.Click += new System.EventHandler(this.imageBuilderToolStripMenuItem_Click);
            // 
            // imageLoaderToolStripMenuItem
            // 
            this.imageLoaderToolStripMenuItem.Name = "imageLoaderToolStripMenuItem";
            this.imageLoaderToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.imageLoaderToolStripMenuItem.Text = "Image Loader";
            this.imageLoaderToolStripMenuItem.Click += new System.EventHandler(this.imageLoaderToolStripMenuItem_Click);
            // 
            // imageBuilderBatchModeToolStripMenuItem
            // 
            this.imageBuilderBatchModeToolStripMenuItem.Name = "imageBuilderBatchModeToolStripMenuItem";
            this.imageBuilderBatchModeToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.imageBuilderBatchModeToolStripMenuItem.Text = "Image Builder(Batch Mode)";
            this.imageBuilderBatchModeToolStripMenuItem.Click += new System.EventHandler(this.imageBuilderBatchModeToolStripMenuItem_Click);
            // 
            // abouttoolStripMenuItem
            // 
            this.abouttoolStripMenuItem.Name = "abouttoolStripMenuItem";
            this.abouttoolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.abouttoolStripMenuItem.Text = "About";
            this.abouttoolStripMenuItem.Click += new System.EventHandler(this.abouttoolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(634, 362);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "UEI WorkFlow 2010";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eidtTNToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateIDDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateIDIntronToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateIDLabelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qAStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkOutIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkInIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem releaseLockedIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem releaseLockedTNToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem createIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem batchIDCheckInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setWorkingDirectoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem checkOutAllIDsForExecutorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkInAllIDsForExecutorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findDisconnectedTNsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoIDCheckoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem checkOutTNToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem checkInTNToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ambiguousIntronsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iDCheckoutConflictToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem batchIDCheckOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem synthesizerTableToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem deleteIDFromMainDBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem synchronizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restrictedIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importIDFromXMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iRMaestroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pickToFIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modelNumberSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importDeviceDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem emptyNonEmptyIDsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nonemptyIDsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emptyIDsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uniqueFunctionIRCountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem upgradeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageBuilderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageLoaderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem labelSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem intronSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iRDBSearchEngineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageBuilderBatchModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tNCompToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brandSetupSRCFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readBackVerificationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importUSBCapture;
        private System.Windows.Forms.ToolStripMenuItem readBackResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem splitLongCapturesMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iRVerificationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem macroAnalyzerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem decodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logNewRemote;
        private System.Windows.Forms.ToolStripMenuItem similarIDReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iDCompareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iDSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iDByBrandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setupCodeInfoToolStripMenuItem;
 private System.Windows.Forms.ToolStripMenuItem timingInspectorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timingInspectorResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modelInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eXIDTNInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brandBasedSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brandModelSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abouttoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolBrandSearch;
        private System.Windows.Forms.ToolStripMenuItem miscReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem encryptAndDecryptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exIdMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emptyDeviceTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exSamePrefixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quicksetReport;
        private System.Windows.Forms.ToolStripMenuItem keyFunctionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iDToFIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolSupportedIdsByPlatform;
        private System.Windows.Forms.ToolStripMenuItem serviceProviderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem excludeLabelSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem excludeIntronSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem modelInfoArdKeyToolStripMenuItem;

    }
}