using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using CommonForms;
using BusinessObject;

namespace IRDBSearchEngine
{
    class SearchFunctions
    {
        const int QUERY_IDLIST_SIZE = 20;

        public string WORKDIR = "";
        public bool canceledByUser;
        public StringCollection IDListForDataQuery = new StringCollection();
        public HashtableCollection tempIDData;

        public string TNFiles;
        public string U1Label;
        public string Executor;
        public string GenericCfgPath;
        public string Brands;
        public bool KeepAllCFIFiles;
        private BackgroundWorker backgroundWorker;
        //New Functions and New Signals
        private FunctionsSignals m_FunctionsSignals = null;       
        #region public DBSearch Functions

        public SearchFunctions()
        {
            m_FunctionsSignals = FunctionsSignals.GetInstance();
            backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += backgroundWorker_DoWork;
        }

        public void SetCfiTollerance(int devFreq, int devPulse, int devDelay,
                                    int devCodeGap, int devDelayPulse)
        {
            CFIInfo.SET_TOLERANCE(devFreq, devPulse, devDelay, 
                devCodeGap, devDelayPulse);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="processingForm"></param>
        /// <returns></returns>
        public List<CompareResults> BatchSearch(
            ProcessingForm processingForm)
        {
            StreamReader sr = File.OpenText(TNFiles);
            List<CompareResults> results = new List<CompareResults>();

            string path = TNFiles.Substring(0,
                TNFiles.LastIndexOf('\\') + 1);

            string buf;

            while ((buf = sr.ReadLine()) != null)
            {
                string[] strs = buf.Split(new char[] { ' ' },
                    StringSplitOptions.RemoveEmptyEntries);
                if (strs.Length == 0)
                    continue;
                if (strs.Length < 2)
                    continue;

                string fileName = strs[0].Trim() + ".ZIP";
                string modes = strs[1].Trim();
                modes = modes.ToUpper();

                List<CompareResults> list
                    = DBSearch(false, path + fileName, modes,
                    processingForm);

                if (list.Count > 0)
                {
                    foreach (CompareResults res in list)
                    {
                        int i = 0;

                        while (i < results.Count &&
                            String.Compare(results[i].captureFile,
                            res.captureFile) <= 0)
                            i++;

                        results.Insert(i, res);
                    }
                }
                else
                {
                    CompareResults res = new CompareResults();
                    res.captureFile = fileName;
                    res.dataMatch = 0;

                    int i = 0;

                    while (i < results.Count &&
                        String.Compare(results[i].captureFile,
                        res.captureFile) <= 0)
                        i++;

                    results.Insert(i, res);
                }

                if (canceledByUser)
                {
                    sr.Close();
                    return results;
                }
            }

            sr.Close();
            return results;
        }

        #region BatchSearch with SubFolder
        //Added new Method for Sub Folder Zip File Selection
        public List<CompareResults> BatchSearch(
            ProcessingForm processingForm, bool isSubFolder)
        {
            
          
            List<CompareResults> results = new List<CompareResults>();
            try
            {

                StreamReader sr = File.OpenText(TNFiles);
                string path = TNFiles.Substring(0,
                    TNFiles.LastIndexOf('\\') + 1);

                string buf;

                while ((buf = sr.ReadLine()) != null)
                {
                    string[] strs = buf.Split(new char[] { '|' },
                        StringSplitOptions.RemoveEmptyEntries);
                    if (strs.Length == 0)
                        continue;
                    if (strs.Length < 2)
                        continue;

                    string fileName = strs[0].Trim() + ".ZIP";
                    string modes = strs[1].Trim();
                    modes = modes.ToUpper();

                    //List<CompareResults> list
                    //    = DBSearch(false, path + fileName, modes,
                    //    processingForm);

                    List<CompareResults> list
                           = DBSearch(false, fileName, modes,
                           processingForm);

                    if (list.Count > 0)
                    {
                        foreach (CompareResults res in list)
                        {
                            int i = 0;

                            while (i < results.Count &&
                                String.Compare(results[i].captureFile,
                                res.captureFile) <= 0)
                                i++;

                            results.Insert(i, res);
                        }
                    }
                    else
                    {
                        CompareResults res = new CompareResults();
                        res.captureFile = fileName;
                        res.dataMatch = 0;

                        int i = 0;

                        while (i < results.Count &&
                            String.Compare(results[i].captureFile,
                            res.captureFile) <= 0)
                            i++;

                        results.Insert(i, res);
                    }

                    if (canceledByUser)
                    {
                        sr.Close();
                        return results;
                    }
                }

                sr.Close();
            }
            catch (IOException iEx)
            {
                MessageBox.Show("Specified File/s Not Found in the Given Folder");
                throw iEx;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.Message);
                throw ex;
            }
            return results;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="singleU1"></param>
        /// <param name="inputFile"></param>
        /// <param name="modes"></param>
        /// <param name="processingForm"></param>
        /// <returns></returns>
        public List<CompareResults> DBSearch(bool singleU1,
                                            string inputFile, 
                                            string modes,
                                            ProcessingForm processingForm)
        {
            if (processingForm != null)
            {
                processingForm.SetMessage("Identifying " + Path.GetFileName(inputFile));
                processingForm.SetProgress(0);
                processingForm.Update();
            }

            FunctionList.InitIntronTable();

            List<CompareResults> results = new List<CompareResults>();

            StringBuilder logFileContents = new StringBuilder();
            DateTime startTime = DateTime.Now;
            DateTime endTime;

            logFileContents.AppendFormat("Start time for ID:  {0}\r\n\r\n", startTime);

            string[] captureFiles;
            string tempFileDir;
            string capturePath;

            if (singleU1)
            {
                capturePath = TNFiles;
                
                tempFileDir = WORKDIR + Path.GetFileName(inputFile);
                if (!Directory.Exists(tempFileDir)) {
                    Directory.CreateDirectory(tempFileDir);
                }

                captureFiles = new string[1];
                captureFiles[0] = tempFileDir + "\\" + Path.GetFileName(inputFile);
                File.Copy(TNFiles.Trim(), captureFiles[0], true);
            }
            else
            {
                // Unzip TN file in to their individual U1 capture files
                logFileContents.AppendFormat("Unzip file: {0}\r\n", inputFile);
                capturePath = unzipTNZipFile(inputFile);

                logFileContents.AppendFormat("File: {0} successfully\r\n\r\n", inputFile);
                tempFileDir = capturePath.Substring(0, capturePath.LastIndexOf('\\'));
                captureFiles = Directory.GetFiles(capturePath);
            }

            // If there are no files to process, then log that we're complete and end the search process
            if (captureFiles == null || captureFiles.Length == 0)
            {
                endTime = DateTime.Now;

                logFileContents.AppendFormat("Finish Comp. at:  {0}\r\n", endTime);
                logFileContents.AppendFormat("Total time used for Comp.: {0}\r\n",
                    FunctionList.GetTimeDifference(startTime, endTime));

                File.WriteAllText(WORKDIR + Path.GetFileName(inputFile).Replace(".ZIP", ".LOG"), logFileContents.ToString());
                RemoveTempFiles(tempFileDir);

                return results;
            }

            
            Hashtable labelList = new Hashtable();
            if (singleU1) {
                // Get the capture label if one was specified
                if (U1Label != null && U1Label.Trim() != "") {
                    string fileName = Path.GetFileName(inputFile);
                    fileName = fileName.Replace(".u1", ".U1");
                    fileName = fileName.Replace(".u2", ".U2");
                    labelList[fileName] = U1Label.Trim();
                }
            } else {
                // Get capture labels from the PRJ file from the TN archive
                for (int i = 0; i < captureFiles.Length; i++) {
                    if (captureFiles[i].Contains(".prj") || captureFiles[i].Contains(".PRJ")) {
                        labelList = FunctionList.GetLabels(captureFiles[i]);
                    }
                }
            }

            DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);
            string logMessage = "Seaching in database:\r\n\r\n";

            // Get the list of the possible Execs we can use for this search
            List<int> execList = null;
            if (Executor != null) {
                execList = new List<int>();

                int parsedExec;
                if (int.TryParse(Executor.Trim(), out parsedExec)) {
                    execList.Add(parsedExec);
                }
            } 
            if (execList == null || execList.Count == 0) {
                execList = FunctionList.GetExecutorList(capturePath, singleU1, ref logMessage, labelList, processingForm);
            }

            if (canceledByUser) {
                MessageBox.Show("User canceled the progress!", "WARNING");

                ProcessResults(inputFile, execList, logFileContents, startTime, tempFileDir,
                    ref logMessage, string.Empty, string.Empty, string.Empty, string.Empty,
                    null, Path.GetTempFileName(), new StringCollection(), new Hashtable());

                return results;
            }

            logMessage += "Possible executors identified by U1 files:";

            if (execList.Count == 0)
                logMessage += "\r\n      No possible executor found!!";
            else {
                for (int i = 0; i < execList.Count; i++) {
                    if (i % 10 == 0) {
                        logMessage += "\r\n  ";
                    }
                    logMessage += String.Format("    {0,-3}", execList[i]);
                }
            }

            logMessage += "\r\n\r\nExecutor/Id List:";

            List<string> skippedFiles = null;

            StringCollection captureCfiFiles = null;
            if (execList.Contains(90))
                captureCfiFiles = FunctionList.GetCfiFiles(captureFiles, 90, labelList, ref logMessage, out skippedFiles);

            StringCollection excludedIDList;
            StringCollection brands = GetBrandes();
            StringCollection prefixExcludedIDList = new StringCollection();
            StringCollection QAIDList = GetIdList(execList, modes, brands, captureCfiFiles, out excludedIDList, null, processingForm);

            if (excludedIDList.Count > 0) {
                foreach (string id in excludedIDList) {
                    AddToList(id, ListID.QA, prefixExcludedIDList);
                }
            }

            StringCollection QAIDList_India = new StringCollection();
            if (SystemInformation.UserDomainName == "UEIC") {
                DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP_INDIA);
                QAIDList_India = GetIdList(execList, modes, brands, captureCfiFiles, out excludedIDList, QAIDList, processingForm);
                if (excludedIDList.Count > 0) {
                    foreach (string id in excludedIDList) {
                        AddToList(id, ListID.QAI, prefixExcludedIDList);
                    }
                }
            }

            StringCollection qaList = new StringCollection();
            qaList.Append(QAIDList);
            qaList.Append(QAIDList_India);
            DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
            StringCollection UEIIDList = GetIdList(execList, modes, brands, captureCfiFiles, out excludedIDList, qaList, processingForm);

            if (excludedIDList.Count > 0) {
                foreach (string id in excludedIDList) {
                    AddToList(id, ListID.UEI, prefixExcludedIDList);
                }
            }

            StringCollection tmpIDList = new StringCollection();
            Hashtable ExecIDTable = new Hashtable();
            IDHeader IDHeaderData = null;

            string QAComplogMessage = string.Empty;
            string QAComplogMessage_India = string.Empty;
            string UEIComplogMessage = string.Empty;
            string ENDlogMessage = string.Empty;

            endTime = DateTime.Now;
            ENDlogMessage += String.Format("\r\n\r\n\r\nFinish ID at:  {0}\r\n", endTime);
            ENDlogMessage += String.Format("Total time used for ID: {0}\r\n\r\n", FunctionList.GetTimeDifference(startTime, endTime));
            startTime = DateTime.Now;
            ENDlogMessage += String.Format("Start time for Comp.:  {0}", startTime);

            DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);

            int prevExecCode = -1;
            Hashtable RXDataTable = new Hashtable();
            Hashtable RXCFIInfoTable = new Hashtable();
            HashtableCollection IDheader = null;
            int idChecked = 0;
            bool matched;
            HashtableCollection IDdata = null;
            int iter = 0;

            string tempFileName = Path.GetTempFileName();
            IDListForDataQuery.Clear();
            if (QAIDList.Count > 0) {
                for (int i = 0; i < QAIDList.Count && IDListForDataQuery.Count < QUERY_IDLIST_SIZE; i++) {
                    IDListForDataQuery.Add(QAIDList[i]);
                }
                RunDataQuery();
            }

            //New Functions and New Signals
            NewFunctionsNewSignals m_NewFunctionsNewSignals = new NewFunctionsNewSignals();
            m_NewFunctionsNewSignals.CaptureFiles = captureFiles;
            String m_Modes = String.Empty;
            foreach (Char ch in modes)
            {
                m_Modes += ch.ToString() + ",";
            }
            if (m_Modes.Length > 0)
                m_Modes = m_Modes.Substring(0, m_Modes.Length - 1);
            m_NewFunctionsNewSignals.Modes = m_Modes;
            m_NewFunctionsNewSignals.LabelList = labelList;
            m_NewFunctionsNewSignals.InputFile = inputFile;
            m_FunctionsSignals.Add(m_NewFunctionsNewSignals);
            m_NewFunctionsNewSignals = null;

            Compare comp = new Compare();
            comp.WORKDIR = WORKDIR;
            comp.KeepAllCFIFiles = KeepAllCFIFiles;

            #region QA Search
            foreach (string id in QAIDList)
            {
                idChecked++;
                #region UpdateProgressBarForm
                if (processingForm != null)
                {
                    processingForm.SetMessage(string.Format("(Q){0} to {1} comp.", id, Path.GetFileName(inputFile)));
                    processingForm.SetProgress(GetPercentage(idChecked, QAIDList.Count + QAIDList_India.Count + UEIIDList.Count));
                    processingForm.Update();
                }
                #endregion
                IDheader = FunctionList.GetDBdata(id, "AdHoc.GetIDHeader"); // retireve order is important
                int Executor_Code = 0;
                if (!string.IsNullOrEmpty(GenericCfgPath)) {
                    Executor_Code = ExecLib.ExecsInC.GENERIC_EXEC_NUM;
                } else if (IDheader.Count == 0) {
                    IDHeaderData = DAOFactory.IDHeader().SelectHeaderOnly(id);
                    Executor_Code = IDHeaderData.Executor_Code;
                } else {
                    IDHeaderData = null;
                    Executor_Code = (int)IDheader[0]["Executor_Code"];
                }

                if (Executor_Code != prevExecCode)
                {
                    captureCfiFiles = FunctionList.GetCfiFiles(captureFiles, Executor_Code, GenericCfgPath, labelList,
                                                        ref logMessage,out skippedFiles);

                    RXDataTable.Clear();
                    RXCFIInfoTable.Clear();
                    prevExecCode = Executor_Code;
                }
                #region WaitForQuery StartNewQuery
                if (iter % QUERY_IDLIST_SIZE == 0)
                {
                    while (backgroundWorker.IsBusy)
                        Application.DoEvents();

                    IDListForDataQuery.Clear();

                    for (int i = QAIDList.IndexOf(id) + QUERY_IDLIST_SIZE;
                        i < QAIDList.Count
                        && IDListForDataQuery.Count < QUERY_IDLIST_SIZE;
                        i++)
                    {
                        IDListForDataQuery.Add(QAIDList[i]);
                    }

                    IDdata = tempIDData;
                    if (IDListForDataQuery.Count > 0)
                        RunDataQuery();
                }
                #endregion
                iter++;

                comp.DoCompare(IDHeaderData, IDdata, IDheader, results, id, labelList,
                    inputFile, captureCfiFiles, ref RXDataTable, ref RXCFIInfoTable, ListID.QA,
                    tempFileName, ref QAComplogMessage, skippedFiles,
                    out matched);

                if (matched)
                {
                    tmpIDList.Add(id);
                    Add(Executor_Code, id, ExecIDTable, ListID.QA);
                } else if (QAIDList_India.Contains(id) == false || UEIIDList.Contains(id) == false)
                    Add(Executor_Code, id, ExecIDTable, ListID.QA);

                #region If Canceled 
                if (canceledByUser)
                {
                    MessageBox.Show("User caceled the progress!", "WARNING");

                    for (int i = QAIDList.IndexOf(id) + 1; i < QAIDList.Count; i++)
                    {
                        if (QAIDList_India.Contains(QAIDList[i]) || UEIIDList.Contains(QAIDList[i]))
                            continue;

                        IDheader = FunctionList.GetDBdata(QAIDList[i], "AdHoc.GetIDHeader"); // retireve order is important
                        if (IDheader.Count == 0)
                        {
                            IDHeaderData = DAOFactory.IDHeader().SelectHeaderOnly(QAIDList[i]);
                            Executor_Code = IDHeaderData.Executor_Code;
                        }
                        else
                        {
                            IDHeaderData = null;
                            Executor_Code = (int)IDheader[0]["Executor_Code"];
                        }
                        Add(Executor_Code, QAIDList[i], ExecIDTable, ListID.QA);
                    }

                    DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP_INDIA);
                    for (int i = 0; i < QAIDList_India.Count; i++)
                    {
                        if (tmpIDList.Contains(QAIDList_India[i]))
                            continue;

                        IDheader = FunctionList.GetDBdata(QAIDList_India[i], "AdHoc.GetIDHeader"); // retireve order is important
                        if (IDheader.Count == 0)
                        {
                            IDHeaderData = DAOFactory.IDHeader().SelectHeaderOnly(QAIDList_India[i]);
                            Executor_Code = IDHeaderData.Executor_Code;
                        }
                        else
                        {
                            IDHeaderData = null;
                            Executor_Code = (int)IDheader[0]["Executor_Code"];
                        }
                        Add(Executor_Code, QAIDList_India[i], ExecIDTable, ListID.QAI);
                    }

                    DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
                    for (int i = 0; i < UEIIDList.Count; i++)
                    {
                        if (tmpIDList.Contains(UEIIDList[i]))
                            continue;

                        IDheader = FunctionList.GetDBdata(UEIIDList[i], "AdHoc.GetIDHeader"); // retireve order is important
                        if (IDheader.Count == 0)
                        {
                            IDHeaderData = DAOFactory.IDHeader().SelectHeaderOnly(UEIIDList[i]);
                            Executor_Code = IDHeaderData.Executor_Code;
                        }
                        else
                        {
                            IDHeaderData = null;
                            Executor_Code = (int)IDheader[0]["Executor_Code"];
                        }
                        Add(Executor_Code, UEIIDList[i], ExecIDTable, ListID.UEI);
                    }

                    ProcessResults(inputFile, execList, logFileContents, startTime, tempFileDir,
                        ref logMessage, QAComplogMessage, QAComplogMessage_India, UEIComplogMessage,
                        ENDlogMessage, captureCfiFiles, tempFileName, prefixExcludedIDList, ExecIDTable);

                    return results;
                }
                #endregion
            }
            #endregion

            if (QAIDList_India.Count > 0) {
                prevExecCode = -1;
                captureCfiFiles = null;
                iter = 0;

                DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP_INDIA);

                IDListForDataQuery.Clear();
                if (QAIDList_India.Count > 0) {
                    for (int i = 0; i < QAIDList_India.Count && IDListForDataQuery.Count < QUERY_IDLIST_SIZE; i++) {
                        if (!tmpIDList.Contains(QAIDList_India[i]))
                            IDListForDataQuery.Add(QAIDList_India[i]);
                    }
                    RunDataQuery();
                }

                #region India QA Search
                foreach (string id in QAIDList_India) {
                    idChecked++;
                    if (tmpIDList.Contains(id))
                        continue;

                    if (processingForm != null) {
                        processingForm.SetMessage(string.Format("(I){0} to {1} comp.", id, Path.GetFileName(inputFile)));
                        processingForm.SetProgress(GetPercentage(idChecked, QAIDList.Count + QAIDList_India.Count + UEIIDList.Count));
                        processingForm.Update();
                    }
                    IDheader = FunctionList.GetDBdata(id, "AdHoc.GetIDHeader"); // retireve order is important
                    int Executor_Code = 0;
                    if (!string.IsNullOrEmpty(GenericCfgPath)) {
                        Executor_Code = ExecLib.ExecsInC.GENERIC_EXEC_NUM;
                    } else if (IDheader.Count == 0) {
                        IDHeaderData = DAOFactory.IDHeader().SelectHeaderOnly(id);
                        Executor_Code = IDHeaderData.Executor_Code;
                    } else {
                        IDHeaderData = null;
                        Executor_Code = (int)IDheader[0]["Executor_Code"];
                    }

                    if (Executor_Code != prevExecCode) {
                        // Remove capture CFI files.  Only remove them if we are planning on keeping the other
                        // CFI files b/c if we are not keeping any CFI files, they will all get deleted anyway.
                        if (KeepAllCFIFiles && captureCfiFiles != null) {
                            if (captureCfiFiles.Count > 0) {
                                foreach (string file in captureCfiFiles) {
                                    try {
                                        File.Delete(file);
                                    } catch {}
                                }

                                string s = captureCfiFiles[0].Substring(0, captureCfiFiles[0].LastIndexOf('\\'));
                                string[] dirs = Directory.GetDirectories(s);

                                if (dirs == null || dirs.Length == 0) {
                                    try {
                                        Directory.Delete(s);
                                    } catch {}
                                }
                            }
                        }

                        captureCfiFiles = FunctionList.GetCfiFiles(captureFiles, Executor_Code, GenericCfgPath, labelList,
                                                                   ref logMessage, out skippedFiles);

                        RXDataTable.Clear();
                        RXCFIInfoTable.Clear();
                        prevExecCode = Executor_Code;
                    }

                    if (iter % QUERY_IDLIST_SIZE == 0) {
                        while (backgroundWorker.IsBusy)
                            Application.DoEvents();

                        IDListForDataQuery.Clear();

                        int i = QAIDList_India.IndexOf(id), j = 0;

                        while (i < QAIDList_India.Count && j < QUERY_IDLIST_SIZE) {
                            if (!tmpIDList.Contains(QAIDList_India[i]))
                                j++;
                            i++;
                        }

                        for (;
                            i < QAIDList_India.Count
                            && IDListForDataQuery.Count < QUERY_IDLIST_SIZE;
                            i++) {
                            if (!tmpIDList.Contains(QAIDList_India[i]))
                                IDListForDataQuery.Add(QAIDList_India[i]);
                        }

                        IDdata = tempIDData;
                        if (IDListForDataQuery.Count > 0)
                            RunDataQuery();
                    }

                    iter++;

                    comp.DoCompare(IDHeaderData, IDdata, IDheader, results, id, labelList,
                                   inputFile, captureCfiFiles, ref RXDataTable, ref RXCFIInfoTable, ListID.QAI,
                                   tempFileName, ref QAComplogMessage_India, skippedFiles,
                                   out matched);

                    if (matched) {
                        tmpIDList.Add(id);
                        Add(Executor_Code, id, ExecIDTable, ListID.QAI);
                    } else if (QAIDList.Contains(id) == false || UEIIDList.Contains(id) == false)
                        Add(Executor_Code, id, ExecIDTable, ListID.QAI);

                    if (canceledByUser) {
                        MessageBox.Show("User caceled the progress!", "WARNING");
                        for (int i = UEIIDList.IndexOf(id) + 1; i < UEIIDList.Count; i++) {
                            if (tmpIDList.Contains(UEIIDList[i]))
                                continue;

                            IDheader = FunctionList.GetDBdata(UEIIDList[i], "AdHoc.GetIDHeader"); // retireve order is important
                            if (IDheader.Count == 0) {
                                IDHeaderData = DAOFactory.IDHeader().SelectHeaderOnly(UEIIDList[i]);
                                Executor_Code = IDHeaderData.Executor_Code;
                            } else {
                                IDHeaderData = null;
                                Executor_Code = (int)IDheader[0]["Executor_Code"];
                            }
                            Add(Executor_Code, UEIIDList[i], ExecIDTable, ListID.UEI);
                        }

                        ProcessResults(inputFile, execList, logFileContents, startTime, tempFileDir,
                                       ref logMessage, QAComplogMessage, QAComplogMessage_India, UEIComplogMessage,
                                       ENDlogMessage, captureCfiFiles, tempFileName, prefixExcludedIDList, ExecIDTable);

                        return results;
                    }
                }
                #endregion
            }

            prevExecCode = -1;
            captureCfiFiles = null;
            iter = 0;

            DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);

            IDListForDataQuery.Clear();
            if (UEIIDList.Count > 0) {
                for (int i = 0; i < UEIIDList.Count && IDListForDataQuery.Count < QUERY_IDLIST_SIZE; i++) {
                    if (!tmpIDList.Contains(UEIIDList[i]))
                        IDListForDataQuery.Add(UEIIDList[i]);
                }
                RunDataQuery();
            }


            #region Main DB Search
            foreach (string id in UEIIDList)
            {
                idChecked++;
                if (tmpIDList.Contains(id))
                    continue;

                if (processingForm != null)
                {
                    processingForm.SetMessage(string.Format("(U){0} to {1} comp.", id, Path.GetFileName(inputFile)));
                    processingForm.SetProgress(GetPercentage(idChecked, QAIDList.Count + QAIDList_India.Count + UEIIDList.Count));
                    processingForm.Update();
                }
                IDheader = FunctionList.GetDBdata(id, "AdHoc.GetIDHeader"); // retireve order is important
                int Executor_Code = 0;
                if (!string.IsNullOrEmpty(GenericCfgPath)) {
                    Executor_Code = ExecLib.ExecsInC.GENERIC_EXEC_NUM;
                } else if (IDheader.Count == 0) {
                    IDHeaderData = DAOFactory.IDHeader().SelectHeaderOnly(id);
                    Executor_Code = IDHeaderData.Executor_Code;
                } else {
                    IDHeaderData = null;
                    Executor_Code = (int)IDheader[0]["Executor_Code"];
                }

                if (Executor_Code != prevExecCode)
                {
                    // Remove capture CFI files.  Only remove them if we are planning on keeping the other
                    // CFI files b/c if we are not keeping any CFI files, they will all get deleted anyway.
                    if (KeepAllCFIFiles && captureCfiFiles != null) {
                        if (captureCfiFiles.Count > 0) {
                            foreach (string file in captureCfiFiles) {
                                try {
                                    File.Delete(file);
                                } catch { }
                            }

                            string s = captureCfiFiles[0].Substring(0, captureCfiFiles[0].LastIndexOf('\\'));
                            string[] dirs = Directory.GetDirectories(s);

                            if (dirs == null || dirs.Length == 0) {
                                try {
                                    Directory.Delete(s);
                                } catch { }
                            }
                        }
                    }

                    captureCfiFiles = FunctionList.GetCfiFiles(captureFiles, Executor_Code, GenericCfgPath, labelList, ref logMessage, out skippedFiles);

                    RXDataTable.Clear();
                    RXCFIInfoTable.Clear();
                    prevExecCode = Executor_Code;
                }

                if (iter % QUERY_IDLIST_SIZE == 0)
                {
                    while (backgroundWorker.IsBusy)
                        Application.DoEvents();

                    IDListForDataQuery.Clear();

                    int i = UEIIDList.IndexOf(id), j = 0;

                    while (i < UEIIDList.Count && j < QUERY_IDLIST_SIZE)
                    {
                        if (!tmpIDList.Contains(UEIIDList[i]))
                            j++;
                        i++;
                    }

                    for (; i < UEIIDList.Count
                        && IDListForDataQuery.Count < QUERY_IDLIST_SIZE;
                        i++)
                    {
                        if (!tmpIDList.Contains(UEIIDList[i]))
                            IDListForDataQuery.Add(UEIIDList[i]);
                    }

                    IDdata = tempIDData;
                    if (IDListForDataQuery.Count > 0)
                        RunDataQuery();
                }

                iter++;

                comp.DoCompare(IDHeaderData, IDdata, IDheader, results, id, labelList,
                    inputFile, captureCfiFiles, ref RXDataTable, ref RXCFIInfoTable, ListID.UEI,
                    tempFileName, ref UEIComplogMessage, skippedFiles,
                    out matched);

                if (matched || !QAIDList.Contains(id) || !QAIDList_India.Contains(id))
                {
                    Add(Executor_Code, id, ExecIDTable, ListID.UEI);
                } else if (QAIDList.Contains(id) == false || QAIDList_India.Contains(id) == false)
                    Add(Executor_Code, id, ExecIDTable, ListID.QA);

                if (canceledByUser)
                {
                    MessageBox.Show("User caceled the progress!", "WARNING");
                    for (int i = UEIIDList.IndexOf(id) + 1; i < UEIIDList.Count; i++)
                    {
                        if (tmpIDList.Contains(UEIIDList[i]))
                            continue;

                        IDheader = FunctionList.GetDBdata(UEIIDList[i], "AdHoc.GetIDHeader"); // retireve order is important
                        if (IDheader.Count == 0)
                        {
                            IDHeaderData = DAOFactory.IDHeader().SelectHeaderOnly(UEIIDList[i]);
                            Executor_Code = IDHeaderData.Executor_Code;
                        }
                        else
                        {
                            IDHeaderData = null;
                            Executor_Code = (int)IDheader[0]["Executor_Code"];
                        }
                        Add(Executor_Code, UEIIDList[i], ExecIDTable, ListID.UEI);
                    }

                    ProcessResults(inputFile, execList, logFileContents, startTime, tempFileDir,
                        ref logMessage, QAComplogMessage, QAComplogMessage_India, UEIComplogMessage,
                        ENDlogMessage, captureCfiFiles, tempFileName, prefixExcludedIDList, ExecIDTable);

                    return results;
                }
            }
            #endregion

            ProcessResults(inputFile, execList, logFileContents, startTime, tempFileDir,
                ref logMessage, QAComplogMessage, QAComplogMessage_India, UEIComplogMessage,
                ENDlogMessage, captureCfiFiles, tempFileName, prefixExcludedIDList, ExecIDTable);
         
            return results;
        }

        private StringCollection GetIdList(List<int> execList, string modes, StringCollection brands, StringCollection captureCfiFiles, out StringCollection excludedIDList, StringCollection QAIDList, ProcessingForm processingForm) {
            const int MAX_RETRIES = 5;
            const int RETRY_DELAY_IN_SECONDS = 30;

            StringCollection idList = new StringCollection();
            excludedIDList = new StringCollection();

            int retryCount = 0;
            bool retry = true;
            while (retry) {
                try {
                    idList = FunctionList.GetIDList(execList, modes, brands, captureCfiFiles, out excludedIDList, QAIDList);
                    retry = false;
                } catch (Exception exception) {
                    if (retryCount < MAX_RETRIES) {
                        RetryForm retryForm = new RetryForm();
                        retryForm.CurrentRetryCount = retryCount + 1;
                        retryForm.MaxRetries = MAX_RETRIES;
                        retryForm.RetryDelayInSeconds = RETRY_DELAY_IN_SECONDS;
                        retryForm.SetMessage(string.Format("Error occured getting list of ID's\n\n{0}", exception.Message));
                        DialogResult errorResponse = retryForm.ShowDialog();

                        if (errorResponse == DialogResult.Abort) {
                            throw;
                        }

                        if (errorResponse == DialogResult.Ignore) {
                            retry = false;
                        }
                    } else {
                        const string message = "Error occured getting list of ID's.\n\t{0}\n\n" + 
                                               "Would you like to continue the search with the remainder of the databases?";
                        DialogResult errorResponse =
                            MessageBox.Show(string.Format(message, exception.Message), "Error",
                                            MessageBoxButtons.YesNo);

                        if (errorResponse == DialogResult.No) {
                            throw;
                        }

                        break;
                    }
                }
                retryCount++;
            }
            return idList;
        }

        public void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            tempIDData =
                FunctionList.GetGroupDBdata(IDListForDataQuery, "AdHoc.GetGroupIDData");
            e.Cancel = true;
        }
        #endregion

        #region Private Functions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="execList"></param>
        /// <param name="logFileContents"></param>
        /// <param name="startTime"></param>
        /// <param name="tempFileDir"></param>
        /// <param name="logMessage"></param>
        /// <param name="QAComplogMessage"></param>
        /// <param name="UEIComplogMessage"></param>
        /// <param name="ENDlogMessage"></param>
        /// <param name="cfiFiles"></param>
        /// <param name="tempFileName"></param>
        /// <param name="prefixExculdedIDList"></param>
        /// <param name="ExecIDTable"></param>
        private void ProcessResults(string inputFile, List<int> execList, StringBuilder logFileContents,
            DateTime startTime, string tempFileDir, ref string logMessage,
            string QAComplogMessage, string QAComplogMessage_India, string UEIComplogMessage, string ENDlogMessage,
            StringCollection cfiFiles, string tempFileName, StringCollection prefixExculdedIDList, Hashtable ExecIDTable)
        {
            // remove all temporary files and old working files in the directories we will be using
            try {
                if (File.Exists(tempFileName))
                    File.Delete(tempFileName);
            }
            catch { }

            if (KeepAllCFIFiles && cfiFiles != null) {
                if (cfiFiles.Count > 0) {
                    foreach (string file in cfiFiles) {
                        try {
                            File.Delete(file);
                        } catch { }
                    }

                    string s = cfiFiles[0].Substring(0, cfiFiles[0].LastIndexOf('\\'));
                    string[] dirs = Directory.GetDirectories(s);

                    if (dirs == null || dirs.Length == 0) {
                        try {
                            Directory.Delete(s);
                        } catch { }
                    }
                }
            }
            RemoveTempFiles(tempFileDir);

            // Write log of all messages that have been accumulated from the process
            logMessage += GetExecIDLogMessage(ExecIDTable, execList, prefixExculdedIDList);
            logFileContents.Append(logMessage + ENDlogMessage + QAComplogMessage + QAComplogMessage_India + UEIComplogMessage);

            DateTime endTime = DateTime.Now;
            logFileContents.AppendFormat("\r\n\r\nFinish Comp. at:  {0}\r\n", endTime);
            logFileContents.AppendFormat("Total time used for Comp.: {0}\r\n", FunctionList.GetTimeDifference(startTime, endTime));

            File.WriteAllText(WORKDIR + Path.GetFileNameWithoutExtension(inputFile) + ".LOG", logFileContents.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        private void RunDataQuery()
        {
            tempIDData = null;
            this.backgroundWorker.RunWorkerAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ExecIDTable"></param>
        /// <param name="execList"></param>
        /// <param name="prefixExculdedIDList"></param>
        private string GetExecIDLogMessage(Hashtable ExecIDTable, List<int> execList, StringCollection prefixExculdedIDList) {
            if (ExecIDTable.Count == 0) {
                return "      \r\nNone\r\n\r\n";
            }

            string logMessage = "\r\n";
            for (int i = 0; i < execList.Count; i++) {
                logMessage += String.Format("\r\nE{0:000}:", execList[i]);

                if (ExecIDTable.ContainsKey(execList[i])) {
                    StringCollection idList = (StringCollection)ExecIDTable[execList[i]];

                    for (int j = 0; j < idList.Count; j++) {
                        if (j % 8 == 0)
                            logMessage += "\r\n ";
                        logMessage += "   " + idList[j];
                    }

                    if (execList[i] == 90 && prefixExculdedIDList.Count > 0) {
                        logMessage += "\r\n\r\n    The following IDs are screened out by prefix comparisons:\r\n";
                        for (int j = 0; j < prefixExculdedIDList.Count; j++) {
                            if (j % 8 == 0)
                                logMessage += "\r\n ";
                            logMessage += "   " + prefixExculdedIDList[j];
                        }
                    }
                } else
                    logMessage += "\r\n";
            }

            return logMessage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="executor"></param>
        /// <param name="id"></param>
        /// <param name="ExecIDTable"></param>
        /// <param name="QA"></param>
        private void Add(int executor, string id,
            Hashtable ExecIDTable, ListID listID)
        {
            if (ExecIDTable.ContainsKey(executor))
            {
                StringCollection idList
                    = (StringCollection)ExecIDTable[executor];
                AddToList(id, listID, idList);
                ExecIDTable[executor] = idList;
            }
            else
            {
                StringCollection idList = new StringCollection();
                idList.Add(listID.Prefix + id);
                ExecIDTable.Add(executor, idList);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="QA"></param>
        /// <param name="idList"></param>
        private void AddToList(string id, ListID listID, StringCollection idList)
        {
            int i = 0;

            for (; i < idList.Count; i++)
            {
                string[] strs = idList[i].Split(new char[] { ')' },
                    StringSplitOptions.RemoveEmptyEntries);
                string s = strs[1].Trim();

                if (String.Compare(id, s) < 0)
                    break;
            }

            idList.Insert(i, listID.Prefix + id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="num"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        private int GetPercentage(int num, int total)
        {
            int percentage = 1000 * num / total;

            int mod = percentage % 10;

            if (mod >= 5)
                percentage += 10;

            return (percentage - mod) / 10;
        }

        #endregion

        #region Processing Files

        private string unzipTNZipFile(string zipFilePath)
        {
            string filePath = WORKDIR;

            int i = zipFilePath.Length - 1;
            int j;

            while (zipFilePath[i] != '\\' && i >= 0)
                i--;

            j = i + 1;

            while (zipFilePath[j] != '.' && j < zipFilePath.Length - 1)
                j++;

            filePath += zipFilePath.Substring(i + 1, j - i - 1) + "\\";

            if (Directory.Exists(filePath))
                Directory.Delete(filePath, true);
            Directory.CreateDirectory(filePath);

            ZipHelper.ExtractAll(zipFilePath, filePath);
            return filePath;
        }

        private void RemoveTempFiles(string tempFileDir)
        {
            if (tempFileDir == "")
                return;

            if (KeepAllCFIFiles)
            {
                string[] files = Directory.GetFiles(tempFileDir);

                if (files == null || files.Length == 0)
                    return;

                for (int i = 0; i < files.Length; i++)
                {
                    if (!files[i].Contains(".cfi")
                        && !files[i].Contains("._cfi"))
                    {
                        try
                        {
                            File.Delete(files[i]);
                        }
                        catch { }
                    }
                }
            }
            else
            {
                if (Directory.Exists(tempFileDir))
                {
                    try
                    {
                        Directory.Delete(tempFileDir, true);
                    }
                    catch { }
                }
            }
        }

        #endregion

        #region TN Source Input
        private StringCollection GetBrandes()
        {
            StringCollection brands = new StringCollection();
            string s = Brands;
            string[] strs = s.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (strs.Length == 0)
                return brands;

            for (int i = 0; i < strs.Length; i++) {
                brands.Add(strs[i].Trim());
            }

            return brands;
        }
        #endregion
     
        public class ListID {
            private readonly string _prefix;
            private readonly string _dbName;
            private readonly string _extension;

            public static readonly ListID QA = new ListID("(Q)", "QADB", ".QA");
            public static readonly ListID UEI = new ListID("(U)", "UEIDB", ".UEI");
            public static readonly ListID QAI = new ListID("(I)", "QAIDB", ".IQA");

            private ListID(string prefix, string dbName, string extension) {
                _prefix = prefix;
                _dbName = dbName;
                _extension = extension;
            }

            public string Prefix {
                get { return _prefix; }
            }

            public string DBName {
                get { return _dbName; }
            }

            public string Extension {
                get {
                    return _extension;
                }
            }
        }
    }
}
