using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;
using CommonForms;
using ExecLib;
using BusinessObject;
using IDCompare;
using Microsoft.Win32;

namespace IRDBSearchEngine
{   
    public partial class DBSearchEngine : Form
    {
        public ReportStyle reportStyle = ReportStyle.ALL;

        #region Private Fields

        const int QUERY_IDLIST_SIZE = 20;

        const string _UEITOOLSROOT =
            "HKEY_CURRENT_USER\\Software\\UEITools\\";
        const string _WORKINGDIR = "WorkingDirectory";

		private int devPulse = 10;
		private int devDelay = 10;
		private int devDelayPulse = 10;
		private int devFreq = 10;
		private int devCodeGap = 10;

        private string genericCfgPath = null;

        List<CompareResults> results = null;
        ResultsForm resultsForm = null;

        //private StringCollection IDListForDataQuery 
        //    = new StringCollection();
        SearchFunctions Search = null;

        #endregion

        #region Initialization

        public DBSearchEngine()
        {
            InitializeComponent();
        }

        private void DBSearchEngine_Load(object sender, EventArgs e)
        {
            this.workDirBox.Text = Configuration.GetWorkingDirectory();
            List<int> executorList = FunctionList.GetAllExecutorList();

            foreach (int i in executorList)
            {
                ExecutorList.Items.Add("" + i);
            }
        }

        #endregion

        #region Menue Click
        private void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OK_Click(null, null);
        }

        private void viewResultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (results == null)
            {
                resultsForm = new ResultsForm();
                resultsForm.Show();
                return;
            }

            if (resultsForm != null && !resultsForm.IsDisposed)
            {
                resultsForm.Show();
                resultsForm.BringToFront();
                return;
            }

            resultsForm = new ResultsForm(this.reportStyle, results);
            resultsForm.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cancel_Click(null, null);
        }

        private void keepCFIFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            KeepAllCFIFiles.Checked = !KeepAllCFIFiles.Checked;
            keepCFIFilesToolStripMenuItem.Checked = KeepAllCFIFiles.Checked;
        }

        private void onlyTop5IDsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportOnlyTop5IDs.Checked = !ReportOnlyTop5IDs.Checked;
            onlyTop5IDsToolStripMenuItem.Checked = ReportOnlyTop5IDs.Checked;
        }

        private void onlyTop10IDsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReportTheTop10IDs.Checked = !ReportTheTop10IDs.Checked;
            onlyTop10IDsToolStripMenuItem.Checked = ReportTheTop10IDs.Checked;
        }

        #endregion

        #region Buttons Click

        private void OK_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(workDirBox.Text))
            {
                MessageBox.Show("Please set up the working directory!",
                    this.Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            if (String.IsNullOrEmpty(TNFiles.Text))
            {
                MessageBox.Show("No file name entered. Try again!",
                    this.Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            string modes = GetModes();

            if (!TNListFile.Checked)
            {
                if (modes == "")
                {
                    MessageBox.Show("Please select modes for search!!",
                        this.Text, MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }
            }
            
            this.Hide();
            ProcessingForm processingForm = new ProcessingForm();
            processingForm.FormClosing += ProcessingForm_FormClosing; 
            processingForm.Show();

            try
            {
                Search = new SearchFunctions();

                Search.WORKDIR          = workDirBox.Text;
                Search.TNFiles          = TNFiles.Text;
                Search.U1Label          = U1Label.Text;
                Search.Executor         = Executor.Text;
                Search.GenericCfgPath   = genericCfgPath;
                Search.Brands           = BrandesTextBox.Text;
                Search.KeepAllCFIFiles  = KeepAllCFIFiles.Checked;
                Search.canceledByUser   = false;

                Search.SetCfiTollerance(devFreq, devPulse, devDelay,
                    devCodeGap, devDelayPulse);

                if (SingleTNFile.Checked)
                {
                    results = Search.DBSearch(false, TNFiles.Text.ToUpper(), 
                        modes, processingForm);
                }
                else if (SingleU1.Checked)
                {
                    results = Search.DBSearch(true, TNFiles.Text.ToUpper(), 
                        modes, processingForm);
                }
                else
                {
                    results = Search.BatchSearch(processingForm);
                }
                //Added for New Functions and New Signals
                FunctionsSignals m_FunctionsSignals = FunctionsSignals.GetInstance();
                resultsForm = new ResultsForm(this.reportStyle, results);                
                this.Show();
                resultsForm.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Show();
            }
            processingForm.Close();
        }

        private void ProcessingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Search.canceledByUser = true;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void View_Click(object sender, EventArgs e)
        {
            viewResultToolStripMenuItem_Click(null, null);
        }

        #endregion

        #region TN Source Input

        private void SingleU1_CheckedChanged(object sender, EventArgs e)
        {
            if (SingleU1.Checked)
                U1Label.Enabled = true;
        }

        private void SingleTNFile_CheckedChanged(object sender, EventArgs e)
        {
            if (SingleTNFile.Checked)
                U1Label.Enabled = false;
        }

        private void TNListFile_CheckedChanged(object sender, EventArgs e)
        {
            if (TNListFile.Checked)
                U1Label.Enabled = false;
        }

        private void workDirBrowse_Click(object sender, EventArgs e)
        {
            Configuration.SetWorkingDirectory();
            this.workDirBox.Text = Configuration.GetWorkingDirectory();
        }

        private void Browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.RestoreDirectory = true;

            if (SingleTNFile.Checked)
            {
                dlg.Filter = "Zip Files (*.zip)|*.zip|All Files (*.*)|*.*";
            }
            else if (TNListFile.Checked)
            {
                dlg.Filter = "Text Files (*.TXT)|*.TXT|(*.LST)|*.LST|All Files (*.*)|*.*";
            }
            else if (SingleU1.Checked)
            {
                dlg.Filter = "Capture Files (*.U1;*.U2)|*.U1;*.U2|All Files (*.*)|*.*";
            }

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                this.TNFiles.Text = dlg.FileName;
            }
        }

        private string GetModes()
        {
            if (All.Checked)
                return "ALL";

            string modes = "";

            foreach (Control control in Modes.Controls)
            {
                if (((CheckBox)control).Checked)
                    modes += ((CheckBox)control).Text;
            }

            return modes;
        }

        #endregion

        #region Timing
        private void KeepAllCFIFiles_CheckedChanged(object sender, EventArgs e)
        {
            keepCFIFilesToolStripMenuItem.Checked = KeepAllCFIFiles.Checked;
        }

        #endregion

        #region Report Type Selection

        private void ReportOnlyTop5IDs_CheckedChanged(object sender, EventArgs e)
        {
            if (ReportOnlyTop5IDs.Checked)
                reportStyle = ReportStyle.TOP5;
            onlyTop5IDsToolStripMenuItem.Checked
                = ReportOnlyTop5IDs.Checked;
        }

        private void ReportTheTop_CheckedChanged(object sender, EventArgs e)
        {
            if (ReportTheTop10IDs.Checked)
            {
                reportStyle = ReportStyle.TOP10;
            }
            onlyTop10IDsToolStripMenuItem.Checked
                = ReportTheTop10IDs.Checked;
        }

        private void ReportAllIDs_CheckedChanged(object sender, EventArgs e)
        {
            if (ReportAllIDs.Checked)
                reportStyle = ReportStyle.ALL;
        }

        #endregion

        #region Executor

        private void ExecutorList_ItemSelectionChanged(object sender,
            ListViewItemSelectionChangedEventArgs e)
        {
            Executor.Text = e.Item.Text;
        }

        private void Executor_Validating(object sender, CancelEventArgs e)
        {
            int exec = 0;

            Executor.Text = Executor.Text.Trim();

            if (string.IsNullOrEmpty(Executor.Text))
            {
                genericCfgPath = null;
                return;
            }

            if (!int.TryParse(Executor.Text, out exec))
            {
                genericCfgPath = null;
                Executor.Text = "";
            }
        }

        private void Executor_TextChanged(object sender, EventArgs e)
        {
            string s = Executor.Text.Trim();

            if (string.IsNullOrEmpty(s))
            {
                LoadGenericExecutor.Enabled = false;
                genericCfgPath = null;
                return;
            }

            LoadGenericExecutor.Enabled = true;
        }

        private void LoadGenericExecutor_CheckedChanged(object sender, EventArgs e)
        {
            genericCfgPath = null;

            if (LoadGenericExecutor.Checked) {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.RestoreDirectory = true;

                dlg.Filter = "cfg Files (*.cfg)|*.cfg|All Files (*.*)|*.*";

                if (dlg.ShowDialog() == DialogResult.OK) {
                    genericCfgPath = dlg.FileName;
                }
            }
        }

        #endregion

        #region Validating functions

        private void PulseDurationTextBox_ValueChanged(object sender, EventArgs e)
        {
            devPulse = (int)NudPulseDuration.Value;
        }

        private void NudDelayDuration_ValueChanged(object sender, EventArgs e)
        {
            devDelay = (int)NudDelayDuration.Value;
        }

        private void NudPulseDelayDuration_ValueChanged(object sender, EventArgs e)
        {
            devDelayPulse = (int)NudPulseDelayDuration.Value;
        }

        private void NudFrequency_ValueChanged(object sender, EventArgs e)
        {
            devFreq = (int)NudFrequency.Value;
        }

        private void NudCodeGap_ValueChanged(object sender, EventArgs e)
        {
            devCodeGap = (int)NudCodeGap.Value;
        }

        private void TNFiles_Validating(object sender, CancelEventArgs e)
        {
            string fileName = TNFiles.Text;
            fileName = fileName.Trim();
            TNFiles.Text = fileName;

            if (string.IsNullOrEmpty(TNFiles.Text))
                return;

            if (!File.Exists(fileName))
            {
                MessageBox.Show("File " + fileName + " does not exist!");
                TNFiles.Text = "";
            }
        }

        private void workDirBox_Validating(object sender, CancelEventArgs e)
        {
            string dirName = workDirBox.Text;
            dirName = dirName.Trim();
            if (string.IsNullOrEmpty(dirName))
            {
                MessageBox.Show("The path of working directory is empty!");
                workDirBox.Text = "";
                return;
            }

            if (!Directory.Exists(dirName))
            {
                MessageBox.Show("Folder " + dirName + " does not exist!");
                workDirBox.Text = "";
            }

            if (!dirName.EndsWith("\\"))
                dirName += "\\";

            workDirBox.Text = dirName;

            Registry.SetValue(_UEITOOLSROOT, _WORKINGDIR,
                workDirBox.Text, RegistryValueKind.String);
        }

        #endregion

        #region Mode Check Boxes Click

        private void ModeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;

            if (checkBox.Name != "All")
            {
                if (!checkBox.Checked)
                {
                    All.CheckedChanged -= ModeCheckBox_CheckedChanged;
                    All.Checked = false;
                    All.CheckedChanged += ModeCheckBox_CheckedChanged;
                }
                else
                {
                    All.CheckedChanged -= ModeCheckBox_CheckedChanged;
                    All.Checked = true;
                    foreach (Control control in Modes.Controls)
                    {
                        CheckBox box = (CheckBox)control;

                        if (!box.Checked)
                        {
                            All.Checked = false;
                            break;
                        }
                    }
                    All.CheckedChanged += ModeCheckBox_CheckedChanged;
                }
            }
            else
            {
                foreach (Control control in Modes.Controls)
                {
                    CheckBox box = (CheckBox)control;
                    box.CheckedChanged -= ModeCheckBox_CheckedChanged;
                    box.Checked = checkBox.Checked;
                    box.CheckedChanged += ModeCheckBox_CheckedChanged;
                }
            }
        }

        #endregion
    }
}
