/*--------------------------------------------------------------
   db_QUERY 2.20 - Small working database definition

   The maximum number of columns is currently set up as 48.
   The maximum number of order/group by fields is 24.

   The number of fields defined in the two record and compound
   key declarations determines the maximum number of allowed fields
   in a selection list and sort/group by list.  If you need more
   feel free to add them to the ends of their respective declarations
   using the given naming convention.  The sizes only matter for 
   calculating the total size of the compound keys.

   Be sure that the total length of the compound keys, however,
   is exactly 240 bytes.  (This is based the max. key length of 246 
   bytes, and the need for q_open to allocate internal key tables to 
   accomodate worst case b-trees.)

   If necessary the page sizes of this database can be increased.
   However, due the requirement of the compound keys to be 240 bytes,
   the page sizes can NOT be made smaller than 1024.

   DO NOT CHANGE THE NAMES OR ORDER OF ANY OF THE FILES, RECORDS,
   OR FIELDS.

   Copyright (c) 1999, Centura Software Corporation, All Rights Reserved.
--------------------------------------------------------------*/
database db_query [1024] 
{
   data file sort_data = "dbquery.qsd" contains qsortrec;
   key  file sort_key  = "dbquery.qsk" contains qsortkey;
   data file grup_data = "dbquery.qgd" contains qgruprec;
   key  file grup_key  = "dbquery.qgk" contains qgrupkey;

   record qsortrec 
   {   
      /* first field must be named `qsortf00' */
      char qsortf00[10]; char qsortf01[10]; char qsortf02[10];
      char qsortf03[10]; char qsortf04[10]; char qsortf05[10];
      char qsortf06[10]; char qsortf07[10]; char qsortf08[10];
      char qsortf09[10]; char qsortf10[10]; char qsortf11[10];
      char qsortf12[10]; char qsortf13[10]; char qsortf14[10];
      char qsortf15[10]; char qsortf16[10]; char qsortf17[10];
      char qsortf18[10]; char qsortf19[10]; char qsortf20[10];
      char qsortf21[10]; char qsortf22[10]; char qsortf23[10];
      char qsortf024; char qsortf025; char qsortf026; char qsortf027;
      char qsortf028; char qsortf029; char qsortf030; char qsortf031;
      char qsortf032; char qsortf033; char qsortf034; char qsortf035;
      char qsortf036; char qsortf037; char qsortf038; char qsortf039;
      char qsortf040; char qsortf041; char qsortf042; char qsortf043;
      char qsortf044; char qsortf045; char qsortf046; char qsortf047;
      compound key qsortkey 
      {
         qsortf00; qsortf01; qsortf02; qsortf03;
         qsortf04; qsortf05; qsortf06; qsortf07;
         qsortf08; qsortf09; qsortf10; qsortf11;
         qsortf12; qsortf13; qsortf14; qsortf15;
         qsortf16; qsortf17; qsortf18; qsortf19;
         qsortf20; qsortf21; qsortf22; qsortf23;
      }
   }
   record qgruprec 
   {   
      /* first field must be named `qgrupf00' */
      char qgrupf00[10]; char qgrupf01[10]; char qgrupf02[10];
      char qgrupf03[10]; char qgrupf04[10]; char qgrupf05[10];
      char qgrupf06[10]; char qgrupf07[10]; char qgrupf08[10];
      char qgrupf09[10]; char qgrupf10[10]; char qgrupf11[10];
      char qgrupf12[10]; char qgrupf13[10]; char qgrupf14[10];
      char qgrupf15[10]; char qgrupf16[10]; char qgrupf17[10];
      char qgrupf18[10]; char qgrupf19[10]; char qgrupf20[10];
      char qgrupf21[10]; char qgrupf22[10]; char qgrupf23[10];
      char qgrupf024; char qgrupf025; char qgrupf026; char qgrupf027;
      char qgrupf028; char qgrupf029; char qgrupf030; char qgrupf031;
      char qgrupf032; char qgrupf033; char qgrupf034; char qgrupf035;
      char qgrupf036; char qgrupf037; char qgrupf038; char qgrupf039;
      char qgrupf040; char qgrupf041; char qgrupf042; char qgrupf043;
      char qgrupf044; char qgrupf045; char qgrupf046; char qgrupf047;
      compound key qgrupkey 
      {
         qgrupf00; qgrupf01; qgrupf02; qgrupf03;
         qgrupf04; qgrupf05; qgrupf06; qgrupf07;
         qgrupf08; qgrupf09; qgrupf10; qgrupf11;
         qgrupf12; qgrupf13; qgrupf14; qgrupf15;
         qgrupf16; qgrupf17; qgrupf18; qgrupf19;
         qgrupf20; qgrupf21; qgrupf22; qgrupf23;
      }
   }
}

/* $Revision:   1.3  $ */

