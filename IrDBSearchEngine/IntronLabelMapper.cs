using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;
using System.IO;
using BusinessObject;
//Purpose: This class perform the following tasks
//1. Finding conflict, description for Intron and Label
//2. Validate Label present / Not for the given ID
//3. Providing additional details to Search Report
//4. This call is being called from Compare class whenever required
namespace IRDBSearchEngine
{
    public class IntronLabelMapper
    {
        protected DataSet dsMap = null;
        protected DataTable functionTable = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intron"></param>
        /// <param name="label"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<string> IntronLabelRelation(string intron, string label, string id, bool fromError, bool outPutFmt)
        {
            string strTable = string.Empty;
            StringBuilder strHeader = new StringBuilder();
            StringBuilder strBody = new StringBuilder();
            string strFooter = string.Empty;
            string strStatus = string.Empty;
            string strLabelStmt = "SELECT DISTINCT Intron,Label FROM [function] WHERE Intron IS NOT NULL AND Label = '" + label + "' AND LEFT(ID,1) = '" + id.Substring(0, 1) + "'";
            string strIntronStmt = "SELECT DISTINCT Intron,Label FROM [function] WHERE Label IS NOT NULL AND Intron = '" + intron + "' AND LEFT(ID,1) = '" + id.Substring(0, 1) + "'";
            const string strUnion = " UNION ";
            string strSQLStmt = string.Empty;
            bool isFound = false;
            
            string isPresent = string.Empty;

            List<string> returnValue = new List<string>();
            SqlConnection conn = new SqlConnection(DBConnectionString.UEITEMP);
            try
            {
              if (string.IsNullOrEmpty(label))
                {
                    strSQLStmt = strIntronStmt;
                    strStatus = "Label is Empty. Intron Value : " + intron + "\r\n";

                }

                if (string.IsNullOrEmpty(intron))
                {
                    strSQLStmt = strLabelStmt;
                    if (!fromError)
                    {
                        strStatus = "Intron is Empty. Label value : " + label + "\r\n";
                    }
                    else
                    {
                        strStatus = "Label value : " + label + "\r\n";
                    }
                    List<string> results = AvailinID(id, label);
                    strStatus += results[1];
                }
                if (!string.IsNullOrEmpty(label) && !string.IsNullOrEmpty(intron))
                {
                     strSQLStmt = strLabelStmt + strUnion + strIntronStmt;
                    strStatus = " Both have value Intron: " + intron + " Label: " + label;
                }

                SqlCommand sqlCmd = new SqlCommand(strSQLStmt, conn);
                SqlDataAdapter sDA = new SqlDataAdapter(strSQLStmt, conn);
                dsMap = new DataSet();
                sDA.Fill(dsMap);
                DataTable resultTable = dsMap.Tables[0];

                if (outPutFmt)
                {

                    strHeader.Append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n");
                    strHeader.Append(strStatus);
                    strHeader.Append("\r\n");
                    strHeader.Append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n");
                    strHeader.Append(string.Format("{0,-30}", "Label"));
                    strHeader.Append(string.Format("{0,-90}", "Description"));
                    strHeader.Append("\r\n");
                    strHeader.Append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n");
                    strHeader.Append(string.Format("{0,-30}", label));
                    strHeader.Append(string.Format("{0,-90}", functionDesc(label, string.Empty)));
                    strHeader.Append("\r\n");
                    strHeader.Append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n");
                    strHeader.Append("\r\n");
                    strHeader.Append(string.Format("{0,-30}", "Label"));
                    strHeader.Append(string.Format("{0,-15}", "Intron"));
                    strHeader.Append(string.Format("{0,-10}", "Present"));
                    strHeader.Append(string.Format("{0,-90}", "Description"));
                    strHeader.Append("\r\n");
                    strHeader.Append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n");


                   foreach (DataRow dr in resultTable.Rows)
                    {
                        strBody.Append(string.Format("{0,-30}", dr["Label"].ToString()));
                        strBody.Append(string.Format("{0,-15}", dr["Intron"].ToString()));
                        isPresent = checkPresent(dr["Label"].ToString(), dr["Intron"].ToString(), id);
                        strBody.Append(string.Format("{0,-10}", isPresent));
                        strBody.Append(string.Format("{0,-90}", functionDesc(string.Empty, dr["Intron"].ToString())));
                        strBody.Append("\r\n");

                        if (isPresent == "YES")
                        {
                            isFound = true;
                        }

                    }
                    if (resultTable.Rows.Count <= 0)
                   
                    {
                        strBody.Append(string.Format("{0,-15}", "No Record Found in Database"));
                        strBody.Append("\r\n");
                    }
                    strFooter = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n";
                    strHeader.Append(strBody.ToString());
                    strHeader.Append(strFooter);

                    strTable = strHeader.ToString();
                }
                returnValue.Add(strTable);

                if (isFound)
                {
                    returnValue.Add("FOUND");
                }
                else
                {
                    returnValue.Add("NOTFOUND");
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
            }
            return returnValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="label"></param>
        /// <param name="intron"></param>
        /// <returns></returns>
        private string functionDesc(string label, string intron)
        {
            string functiondescription = "<<No Function Description Defined>>";
            SqlConnection conn = new SqlConnection(DBConnectionString.UEITEMP);
            try
            {


                #region  DB
                if (functionTable == null)
                {
                    
                    conn.Open();
                    string sqlStmt = @"SELECT DISTINCT LTRIM(RTRIM(KL.Label)) AS [Label],LTRIM(RTRIM(IT.Intron)) AS 				   
	[Intron],LTRIM(RTRIM(ID.Description)) AS [FunctionBriefDescription] FROM IntronDictionary ID 
	INNER JOIN KeyLabels KL
	ON ID.FK_KeyLabel_RID = KL.KeyLabel_RID 
	JOIN Introns IT 
	ON ID.FK_Intron_RID = IT.Intron_RID
	WHERE ID.Description IS NOT NULL";
                    SqlDataAdapter sDA = new SqlDataAdapter(sqlStmt, conn);

                    functionTable = new DataTable();
                    sDA.Fill(functionTable);

                }
                #endregion


                if (!string.IsNullOrEmpty(label))
                {
                    DataRow[] rowsResult = functionTable.Select("Label = '" + label + "'");
                    foreach (DataRow item in rowsResult)
                    {
                        if (!string.IsNullOrEmpty(item["FunctionBriefDescription"].ToString()))
                        {
                            functiondescription = formatDesc(item["FunctionBriefDescription"].ToString(), 29, "LBL");
                            break;
                        }
                    }

                }
                if (!string.IsNullOrEmpty(intron))
                {
                    DataRow[] rowsResult = functionTable.Select("Intron = '" + intron + "'");
                    foreach (DataRow item in rowsResult)
                    {
                        if (!string.IsNullOrEmpty(item["FunctionBriefDescription"].ToString()))
                        {
                            functiondescription = formatDesc(item["FunctionBriefDescription"].ToString(), 54, "IN");
                            break;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
            }

            return functiondescription;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="desc"></param>
        /// <param name="chrLen"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private string formatDesc(string desc, int chrLen, string key)
        {
            string disp = string.Empty;

            int res = desc.Length / 60;
            if (res >= 1)
            {
                for (int i = 1; i <= res; i++)
                {
                    if (i == 1) disp += desc.Substring(0, 50);
                    if (i > 1)
                    {
                        string fill = new string(' ', chrLen);
                        disp += fill + desc.Substring(0, 50);
                    }
                    desc = desc.Remove(0, 50);
                    disp += "\r\n";
                }
            }
            else
            {
                disp = desc;
            }
            if (res > 0 && desc.Length > 0)
            {
                string fill = new string(' ', chrLen + 1);
                disp += fill + desc;
                if (key == "IN") disp += "\r\n";
            }
            return disp;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="label"></param>
        /// <param name="intron"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private string checkPresent(string label, string intron, string id)
        {
            string presentStatus = "NO";
            bool status = ValidateLabel(label, intron, id);
            if (status) presentStatus = "YES";
            return presentStatus;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        public List<string> AvailinID(string id, string label)
        {
            string isFound = string.Empty;
            List<string> results = new List<string>();
            string resultData = string.Empty;
            StringBuilder resultDataX = new StringBuilder();
            string sqlStmt = "SELECT DISTINCT Data, Label, Intron FROM [function] WHERE ID = '" + id + "' AND Label = '" + label + "'";
            SqlConnection conn = new SqlConnection(DBConnectionString.UEITEMP);
            try
            {

                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(sqlStmt, conn);
                sqlCmd.CommandType = CommandType.Text;
                SqlDataReader item = sqlCmd.ExecuteReader();

                if (item.HasRows)

                { isFound = "FOUND"; }
                else
                {
                    isFound = "NOT FOUND";
                    resultDataX.Append(string.Format("{0}{1}{2}{3}{4}", "Label : ", label, " Not Found in the Id '", id, "'.\r\n\r\n"));

                }
                while (item.Read())

                {
                    resultDataX.Append(string.Format("{0}{1}{2}{3}{4}", "Label : ", label, " Found in the Id '", id, "'.\r\n\r\n"));
                    resultDataX.Append(string.Format("{0}{1}{2}", "Data  : ", item["Data"].ToString(), ".\r\n\r\n"));
                    resultDataX.Append(string.Format("{0}{1}{2}", "Label  : ", item["Label"].ToString(), ".\r\n\r\n"));
                    resultDataX.Append(string.Format("{0}{1}{2}", "Intron  : ", item["Intron"].ToString(), ".\r\n\r\n"));
                }



                results.Add(isFound);
                results.Add(resultDataX.ToString());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();

            }
            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="label"></param>
        /// <param name="intron"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ValidateLabel(string label, string intron, string id)
        {
            bool valid = false;

            string strValidationStmt = "SELECT DISTINCT Intron,Label FROM [function] WHERE Label IS NOT NULL AND Intron IS NOT NULL AND Label = '" + label + "' AND Intron = '" + intron + "' AND ID = '" + id + "'";
            SqlConnection conn = new SqlConnection(DBConnectionString.UEITEMP);
            try
            {
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(strValidationStmt, conn);
                sqlCmd.CommandType = CommandType.Text;
                SqlDataReader result = sqlCmd.ExecuteReader();
                if (result.HasRows)
                {
                    valid = true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
            }

            return valid;
        }

    }
}
