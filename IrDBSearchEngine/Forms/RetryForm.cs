using System;
using System.Windows.Forms;

namespace IRDBSearchEngine {
    public partial class RetryForm : Form {
        private int _currentRetryCount;
        private int _maxRetries;
        private int _retryDelayCount;
        private int _retryDelayInSeconds;
        private string _message;
        private readonly Timer _retryTimer;

        public RetryForm() {
            InitializeComponent();

            _retryTimer = new Timer();
            _retryTimer.Interval = 1000;
            _retryTimer.Tick += RetryTimer_Tick;
        }
        
        private void RetryTimer_Tick(object sender, EventArgs e) {
            _retryDelayCount--;

            if (_retryDelayCount <= 0) {
                DialogResult = DialogResult.Retry;
                _retryTimer.Stop();

                Close();
            } else {
                SetMessageLabel();
            }
        }

        public int CurrentRetryCount {
            get { return _currentRetryCount; }
            set { _currentRetryCount = value; }
        }

        public int MaxRetries {
            get { return _maxRetries; }
            set { _maxRetries = value; }
        }

        public int RetryDelayInSeconds {
            get { return _retryDelayInSeconds; }
            set {
                _retryDelayInSeconds = value;
                _retryDelayCount = _retryDelayInSeconds;
            }
        }

        protected override void OnShown(EventArgs e) {
            Text = string.Format("Error in process ({0} of {1} retries)", _currentRetryCount, _maxRetries);
            _retryTimer.Start();
            SetMessageLabel();
        }

        public void SetMessage(string message) {
            _message = message + "\n\nAnother attempt will be made in";
        }

        private void SetMessageLabel() {
            MessageLabel.Text = _message + string.Format(" {0} sec(s).", _retryDelayCount);
        }
    }
}