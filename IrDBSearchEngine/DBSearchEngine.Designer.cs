namespace IRDBSearchEngine
{
    partial class DBSearchEngine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.View = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.OK = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.workingDir = new System.Windows.Forms.GroupBox();
            this.workDirBrowse = new System.Windows.Forms.Button();
            this.workDirBox = new System.Windows.Forms.TextBox();
            this.BrandGroupBox = new System.Windows.Forms.GroupBox();
            this.BrandesTextBox = new System.Windows.Forms.TextBox();
            this.BrandesLabel = new System.Windows.Forms.Label();
            this.Modes = new System.Windows.Forms.GroupBox();
            this.All = new System.Windows.Forms.CheckBox();
            this.Z = new System.Windows.Forms.CheckBox();
            this.Y = new System.Windows.Forms.CheckBox();
            this.X = new System.Windows.Forms.CheckBox();
            this.W = new System.Windows.Forms.CheckBox();
            this.V = new System.Windows.Forms.CheckBox();
            this.U = new System.Windows.Forms.CheckBox();
            this.T = new System.Windows.Forms.CheckBox();
            this.S = new System.Windows.Forms.CheckBox();
            this.R = new System.Windows.Forms.CheckBox();
            this.Q = new System.Windows.Forms.CheckBox();
            this.P = new System.Windows.Forms.CheckBox();
            this.O = new System.Windows.Forms.CheckBox();
            this.N = new System.Windows.Forms.CheckBox();
            this.M = new System.Windows.Forms.CheckBox();
            this.L = new System.Windows.Forms.CheckBox();
            this.K = new System.Windows.Forms.CheckBox();
            this.J = new System.Windows.Forms.CheckBox();
            this.I = new System.Windows.Forms.CheckBox();
            this.H = new System.Windows.Forms.CheckBox();
            this.G = new System.Windows.Forms.CheckBox();
            this.F = new System.Windows.Forms.CheckBox();
            this.E = new System.Windows.Forms.CheckBox();
            this.D = new System.Windows.Forms.CheckBox();
            this.C = new System.Windows.Forms.CheckBox();
            this.B = new System.Windows.Forms.CheckBox();
            this.A = new System.Windows.Forms.CheckBox();
            this.TNSourceFile = new System.Windows.Forms.GroupBox();
            this.SingleTNFile = new System.Windows.Forms.RadioButton();
            this.U1Label = new System.Windows.Forms.TextBox();
            this.TNListFile = new System.Windows.Forms.RadioButton();
            this.SingleU1Label = new System.Windows.Forms.Label();
            this.TNFiles = new System.Windows.Forms.TextBox();
            this.SingleU1 = new System.Windows.Forms.RadioButton();
            this.Browse = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ManualCaptureFile = new System.Windows.Forms.CheckBox();
            this.KeepAllCFIFiles = new System.Windows.Forms.CheckBox();
            this.TimingGroupBox = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.NudCodeGap = new System.Windows.Forms.NumericUpDown();
            this.NudPulseDelayDuration = new System.Windows.Forms.NumericUpDown();
            this.NudDelayDuration = new System.Windows.Forms.NumericUpDown();
            this.NudPulseDuration = new System.Windows.Forms.NumericUpDown();
            this.NudFrequency = new System.Windows.Forms.NumericUpDown();
            this.CodeGap = new System.Windows.Forms.CheckBox();
            this.Frequency = new System.Windows.Forms.CheckBox();
            this.PulseDelayDuration = new System.Windows.Forms.CheckBox();
            this.DelayDuration = new System.Windows.Forms.CheckBox();
            this.PulseDuration = new System.Windows.Forms.CheckBox();
            this.IncludeExtensionFunctions = new System.Windows.Forms.CheckBox();
            this.Report = new System.Windows.Forms.GroupBox();
            this.ReportAllIDs = new System.Windows.Forms.RadioButton();
            this.ReportTheTop10IDs = new System.Windows.Forms.RadioButton();
            this.ReportOnlyTop5IDs = new System.Windows.Forms.RadioButton();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.ExecutorList = new System.Windows.Forms.ListView();
            this.LoadGenericExecutor = new System.Windows.Forms.CheckBox();
            this.Executor = new System.Windows.Forms.TextBox();
            this.ExecutorLabel = new System.Windows.Forms.Label();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.keepCFIFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.onlyTop5IDsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.onlyTop10IDsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.workingDir.SuspendLayout();
            this.BrandGroupBox.SuspendLayout();
            this.Modes.SuspendLayout();
            this.TNSourceFile.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.TimingGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NudCodeGap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudPulseDelayDuration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudDelayDuration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudPulseDuration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudFrequency)).BeginInit();
            this.Report.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // View
            // 
            this.View.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.View.Location = new System.Drawing.Point(270, 426);
            this.View.Name = "View";
            this.View.Size = new System.Drawing.Size(75, 23);
            this.View.TabIndex = 0;
            this.View.Text = "View Result";
            this.View.UseVisualStyleBackColor = true;
            this.View.Click += new System.EventHandler(this.View_Click);
            // 
            // Cancel
            // 
            this.Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Cancel.Location = new System.Drawing.Point(163, 426);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 2;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // OK
            // 
            this.OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.OK.Location = new System.Drawing.Point(56, 426);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(75, 23);
            this.OK.TabIndex = 3;
            this.OK.Text = "Run";
            this.OK.UseVisualStyleBackColor = true;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Controls.Add(this.tabPage4);
            this.tabControl.Location = new System.Drawing.Point(0, 24);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(400, 390);
            this.tabControl.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.workingDir);
            this.tabPage1.Controls.Add(this.BrandGroupBox);
            this.tabPage1.Controls.Add(this.Modes);
            this.tabPage1.Controls.Add(this.TNSourceFile);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(392, 364);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Input/Set Up";
            // 
            // workingDir
            // 
            this.workingDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.workingDir.Controls.Add(this.workDirBrowse);
            this.workingDir.Controls.Add(this.workDirBox);
            this.workingDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workingDir.Location = new System.Drawing.Point(3, 6);
            this.workingDir.Name = "workingDir";
            this.workingDir.Size = new System.Drawing.Size(383, 53);
            this.workingDir.TabIndex = 11;
            this.workingDir.TabStop = false;
            this.workingDir.Text = "Working Directory";
            // 
            // workDirBrowse
            // 
            this.workDirBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workDirBrowse.Location = new System.Drawing.Point(285, 22);
            this.workDirBrowse.Name = "workDirBrowse";
            this.workDirBrowse.Size = new System.Drawing.Size(75, 23);
            this.workDirBrowse.TabIndex = 1;
            this.workDirBrowse.Text = "Browse";
            this.workDirBrowse.UseVisualStyleBackColor = true;
            this.workDirBrowse.Click += new System.EventHandler(this.workDirBrowse_Click);
            // 
            // workDirBox
            // 
            this.workDirBox.Location = new System.Drawing.Point(13, 25);
            this.workDirBox.Name = "workDirBox";
            this.workDirBox.Size = new System.Drawing.Size(266, 20);
            this.workDirBox.TabIndex = 0;
            this.workDirBox.Validating += new System.ComponentModel.CancelEventHandler(this.workDirBox_Validating);
            // 
            // BrandGroupBox
            // 
            this.BrandGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.BrandGroupBox.Controls.Add(this.BrandesTextBox);
            this.BrandGroupBox.Controls.Add(this.BrandesLabel);
            this.BrandGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BrandGroupBox.Location = new System.Drawing.Point(6, 300);
            this.BrandGroupBox.Name = "BrandGroupBox";
            this.BrandGroupBox.Size = new System.Drawing.Size(380, 54);
            this.BrandGroupBox.TabIndex = 10;
            this.BrandGroupBox.TabStop = false;
            this.BrandGroupBox.Text = "Brands(Optional)";
            // 
            // BrandesTextBox
            // 
            this.BrandesTextBox.Location = new System.Drawing.Point(69, 22);
            this.BrandesTextBox.Name = "BrandesTextBox";
            this.BrandesTextBox.Size = new System.Drawing.Size(294, 20);
            this.BrandesTextBox.TabIndex = 1;
            // 
            // BrandesLabel
            // 
            this.BrandesLabel.AutoSize = true;
            this.BrandesLabel.Location = new System.Drawing.Point(10, 25);
            this.BrandesLabel.Name = "BrandesLabel";
            this.BrandesLabel.Size = new System.Drawing.Size(43, 13);
            this.BrandesLabel.TabIndex = 0;
            this.BrandesLabel.Text = "Brands:";
            // 
            // Modes
            // 
            this.Modes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Modes.Controls.Add(this.All);
            this.Modes.Controls.Add(this.Z);
            this.Modes.Controls.Add(this.Y);
            this.Modes.Controls.Add(this.X);
            this.Modes.Controls.Add(this.W);
            this.Modes.Controls.Add(this.V);
            this.Modes.Controls.Add(this.U);
            this.Modes.Controls.Add(this.T);
            this.Modes.Controls.Add(this.S);
            this.Modes.Controls.Add(this.R);
            this.Modes.Controls.Add(this.Q);
            this.Modes.Controls.Add(this.P);
            this.Modes.Controls.Add(this.O);
            this.Modes.Controls.Add(this.N);
            this.Modes.Controls.Add(this.M);
            this.Modes.Controls.Add(this.L);
            this.Modes.Controls.Add(this.K);
            this.Modes.Controls.Add(this.J);
            this.Modes.Controls.Add(this.I);
            this.Modes.Controls.Add(this.H);
            this.Modes.Controls.Add(this.G);
            this.Modes.Controls.Add(this.F);
            this.Modes.Controls.Add(this.E);
            this.Modes.Controls.Add(this.D);
            this.Modes.Controls.Add(this.C);
            this.Modes.Controls.Add(this.B);
            this.Modes.Controls.Add(this.A);
            this.Modes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modes.Location = new System.Drawing.Point(6, 189);
            this.Modes.Name = "Modes";
            this.Modes.Size = new System.Drawing.Size(380, 108);
            this.Modes.TabIndex = 9;
            this.Modes.TabStop = false;
            this.Modes.Text = "Modes";
            // 
            // All
            // 
            this.All.AutoSize = true;
            this.All.Location = new System.Drawing.Point(22, 19);
            this.All.Name = "All";
            this.All.Size = new System.Drawing.Size(37, 17);
            this.All.TabIndex = 26;
            this.All.Text = "All";
            this.All.UseVisualStyleBackColor = true;
            this.All.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // Z
            // 
            this.Z.AutoSize = true;
            this.Z.Location = new System.Drawing.Point(282, 88);
            this.Z.Name = "Z";
            this.Z.Size = new System.Drawing.Size(33, 17);
            this.Z.TabIndex = 25;
            this.Z.Text = "Z";
            this.Z.UseVisualStyleBackColor = true;
            this.Z.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // Y
            // 
            this.Y.AutoSize = true;
            this.Y.Location = new System.Drawing.Point(230, 88);
            this.Y.Name = "Y";
            this.Y.Size = new System.Drawing.Size(33, 17);
            this.Y.TabIndex = 24;
            this.Y.Text = "Y";
            this.Y.UseVisualStyleBackColor = true;
            this.Y.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // X
            // 
            this.X.AutoSize = true;
            this.X.Location = new System.Drawing.Point(178, 88);
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(33, 17);
            this.X.TabIndex = 23;
            this.X.Text = "X";
            this.X.UseVisualStyleBackColor = true;
            this.X.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // W
            // 
            this.W.AutoSize = true;
            this.W.Location = new System.Drawing.Point(126, 88);
            this.W.Name = "W";
            this.W.Size = new System.Drawing.Size(37, 17);
            this.W.TabIndex = 22;
            this.W.Text = "W";
            this.W.UseVisualStyleBackColor = true;
            this.W.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // V
            // 
            this.V.AutoSize = true;
            this.V.Location = new System.Drawing.Point(74, 88);
            this.V.Name = "V";
            this.V.Size = new System.Drawing.Size(33, 17);
            this.V.TabIndex = 21;
            this.V.Text = "V";
            this.V.UseVisualStyleBackColor = true;
            this.V.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // U
            // 
            this.U.AutoSize = true;
            this.U.Location = new System.Drawing.Point(22, 88);
            this.U.Name = "U";
            this.U.Size = new System.Drawing.Size(34, 17);
            this.U.TabIndex = 20;
            this.U.Text = "U";
            this.U.UseVisualStyleBackColor = true;
            this.U.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // T
            // 
            this.T.AutoSize = true;
            this.T.Location = new System.Drawing.Point(334, 65);
            this.T.Name = "T";
            this.T.Size = new System.Drawing.Size(33, 17);
            this.T.TabIndex = 19;
            this.T.Text = "T";
            this.T.UseVisualStyleBackColor = true;
            this.T.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // S
            // 
            this.S.AutoSize = true;
            this.S.Location = new System.Drawing.Point(282, 65);
            this.S.Name = "S";
            this.S.Size = new System.Drawing.Size(33, 17);
            this.S.TabIndex = 18;
            this.S.Text = "S";
            this.S.UseVisualStyleBackColor = true;
            this.S.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // R
            // 
            this.R.AutoSize = true;
            this.R.Location = new System.Drawing.Point(230, 65);
            this.R.Name = "R";
            this.R.Size = new System.Drawing.Size(34, 17);
            this.R.TabIndex = 17;
            this.R.Text = "R";
            this.R.UseVisualStyleBackColor = true;
            this.R.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // Q
            // 
            this.Q.AutoSize = true;
            this.Q.Location = new System.Drawing.Point(178, 65);
            this.Q.Name = "Q";
            this.Q.Size = new System.Drawing.Size(34, 17);
            this.Q.TabIndex = 16;
            this.Q.Text = "Q";
            this.Q.UseVisualStyleBackColor = true;
            this.Q.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // P
            // 
            this.P.AutoSize = true;
            this.P.Location = new System.Drawing.Point(126, 65);
            this.P.Name = "P";
            this.P.Size = new System.Drawing.Size(33, 17);
            this.P.TabIndex = 15;
            this.P.Text = "P";
            this.P.UseVisualStyleBackColor = true;
            this.P.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // O
            // 
            this.O.AutoSize = true;
            this.O.Location = new System.Drawing.Point(74, 65);
            this.O.Name = "O";
            this.O.Size = new System.Drawing.Size(34, 17);
            this.O.TabIndex = 14;
            this.O.Text = "O";
            this.O.UseVisualStyleBackColor = true;
            this.O.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // N
            // 
            this.N.AutoSize = true;
            this.N.Location = new System.Drawing.Point(22, 65);
            this.N.Name = "N";
            this.N.Size = new System.Drawing.Size(34, 17);
            this.N.TabIndex = 13;
            this.N.Text = "N";
            this.N.UseVisualStyleBackColor = true;
            this.N.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // M
            // 
            this.M.AutoSize = true;
            this.M.Location = new System.Drawing.Point(334, 42);
            this.M.Name = "M";
            this.M.Size = new System.Drawing.Size(35, 17);
            this.M.TabIndex = 12;
            this.M.Text = "M";
            this.M.UseVisualStyleBackColor = true;
            this.M.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // L
            // 
            this.L.AutoSize = true;
            this.L.Location = new System.Drawing.Point(282, 42);
            this.L.Name = "L";
            this.L.Size = new System.Drawing.Size(32, 17);
            this.L.TabIndex = 11;
            this.L.Text = "L";
            this.L.UseVisualStyleBackColor = true;
            this.L.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // K
            // 
            this.K.AutoSize = true;
            this.K.Location = new System.Drawing.Point(230, 42);
            this.K.Name = "K";
            this.K.Size = new System.Drawing.Size(33, 17);
            this.K.TabIndex = 10;
            this.K.Text = "K";
            this.K.UseVisualStyleBackColor = true;
            this.K.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // J
            // 
            this.J.AutoSize = true;
            this.J.Location = new System.Drawing.Point(178, 42);
            this.J.Name = "J";
            this.J.Size = new System.Drawing.Size(31, 17);
            this.J.TabIndex = 9;
            this.J.Text = "J";
            this.J.UseVisualStyleBackColor = true;
            this.J.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // I
            // 
            this.I.AutoSize = true;
            this.I.Location = new System.Drawing.Point(126, 42);
            this.I.Name = "I";
            this.I.Size = new System.Drawing.Size(29, 17);
            this.I.TabIndex = 8;
            this.I.Text = "I";
            this.I.UseVisualStyleBackColor = true;
            this.I.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // H
            // 
            this.H.AutoSize = true;
            this.H.Location = new System.Drawing.Point(74, 42);
            this.H.Name = "H";
            this.H.Size = new System.Drawing.Size(34, 17);
            this.H.TabIndex = 7;
            this.H.Text = "H";
            this.H.UseVisualStyleBackColor = true;
            this.H.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // G
            // 
            this.G.AutoSize = true;
            this.G.Location = new System.Drawing.Point(22, 42);
            this.G.Name = "G";
            this.G.Size = new System.Drawing.Size(34, 17);
            this.G.TabIndex = 6;
            this.G.Text = "G";
            this.G.UseVisualStyleBackColor = true;
            this.G.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // F
            // 
            this.F.AutoSize = true;
            this.F.Location = new System.Drawing.Point(334, 19);
            this.F.Name = "F";
            this.F.Size = new System.Drawing.Size(32, 17);
            this.F.TabIndex = 5;
            this.F.Text = "F";
            this.F.UseVisualStyleBackColor = true;
            this.F.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // E
            // 
            this.E.AutoSize = true;
            this.E.Location = new System.Drawing.Point(282, 19);
            this.E.Name = "E";
            this.E.Size = new System.Drawing.Size(33, 17);
            this.E.TabIndex = 4;
            this.E.Text = "E";
            this.E.UseVisualStyleBackColor = true;
            this.E.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // D
            // 
            this.D.AutoSize = true;
            this.D.Location = new System.Drawing.Point(230, 19);
            this.D.Name = "D";
            this.D.Size = new System.Drawing.Size(34, 17);
            this.D.TabIndex = 3;
            this.D.Text = "D";
            this.D.UseVisualStyleBackColor = true;
            this.D.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // C
            // 
            this.C.AutoSize = true;
            this.C.Location = new System.Drawing.Point(178, 19);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(33, 17);
            this.C.TabIndex = 2;
            this.C.Text = "C";
            this.C.UseVisualStyleBackColor = true;
            this.C.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // B
            // 
            this.B.AutoSize = true;
            this.B.Location = new System.Drawing.Point(126, 19);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(33, 17);
            this.B.TabIndex = 1;
            this.B.Text = "B";
            this.B.UseVisualStyleBackColor = true;
            this.B.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // A
            // 
            this.A.AutoSize = true;
            this.A.Location = new System.Drawing.Point(74, 19);
            this.A.Name = "A";
            this.A.Size = new System.Drawing.Size(33, 17);
            this.A.TabIndex = 0;
            this.A.Text = "A";
            this.A.UseVisualStyleBackColor = true;
            this.A.CheckedChanged += new System.EventHandler(this.ModeCheckBox_CheckedChanged);
            // 
            // TNSourceFile
            // 
            this.TNSourceFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TNSourceFile.Controls.Add(this.SingleTNFile);
            this.TNSourceFile.Controls.Add(this.U1Label);
            this.TNSourceFile.Controls.Add(this.TNListFile);
            this.TNSourceFile.Controls.Add(this.SingleU1Label);
            this.TNSourceFile.Controls.Add(this.TNFiles);
            this.TNSourceFile.Controls.Add(this.SingleU1);
            this.TNSourceFile.Controls.Add(this.Browse);
            this.TNSourceFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TNSourceFile.Location = new System.Drawing.Point(3, 65);
            this.TNSourceFile.Name = "TNSourceFile";
            this.TNSourceFile.Size = new System.Drawing.Size(384, 121);
            this.TNSourceFile.TabIndex = 8;
            this.TNSourceFile.TabStop = false;
            this.TNSourceFile.Text = "TN Source File";
            // 
            // SingleTNFile
            // 
            this.SingleTNFile.AutoSize = true;
            this.SingleTNFile.Checked = true;
            this.SingleTNFile.Location = new System.Drawing.Point(22, 19);
            this.SingleTNFile.Name = "SingleTNFile";
            this.SingleTNFile.Size = new System.Drawing.Size(91, 17);
            this.SingleTNFile.TabIndex = 1;
            this.SingleTNFile.TabStop = true;
            this.SingleTNFile.Text = "Single TN File";
            this.SingleTNFile.UseVisualStyleBackColor = true;
            this.SingleTNFile.CheckedChanged += new System.EventHandler(this.SingleTNFile_CheckedChanged);
            // 
            // U1Label
            // 
            this.U1Label.Enabled = false;
            this.U1Label.Location = new System.Drawing.Point(142, 90);
            this.U1Label.Name = "U1Label";
            this.U1Label.Size = new System.Drawing.Size(100, 20);
            this.U1Label.TabIndex = 7;
            // 
            // TNListFile
            // 
            this.TNListFile.AutoSize = true;
            this.TNListFile.Location = new System.Drawing.Point(153, 19);
            this.TNListFile.Name = "TNListFile";
            this.TNListFile.Size = new System.Drawing.Size(78, 17);
            this.TNListFile.TabIndex = 2;
            this.TNListFile.Text = "TN List File";
            this.TNListFile.UseVisualStyleBackColor = true;
            this.TNListFile.CheckedChanged += new System.EventHandler(this.TNListFile_CheckedChanged);
            // 
            // SingleU1Label
            // 
            this.SingleU1Label.AutoSize = true;
            this.SingleU1Label.Location = new System.Drawing.Point(31, 93);
            this.SingleU1Label.Name = "SingleU1Label";
            this.SingleU1Label.Size = new System.Drawing.Size(105, 13);
            this.SingleU1Label.TabIndex = 6;
            this.SingleU1Label.Text = "Single Capture Label";
            // 
            // TNFiles
            // 
            this.TNFiles.Location = new System.Drawing.Point(13, 42);
            this.TNFiles.Name = "TNFiles";
            this.TNFiles.Size = new System.Drawing.Size(266, 20);
            this.TNFiles.TabIndex = 3;
            this.TNFiles.Validating += new System.ComponentModel.CancelEventHandler(this.TNFiles_Validating);
            // 
            // SingleU1
            // 
            this.SingleU1.AutoSize = true;
            this.SingleU1.Location = new System.Drawing.Point(22, 68);
            this.SingleU1.Name = "SingleU1";
            this.SingleU1.Size = new System.Drawing.Size(113, 17);
            this.SingleU1.TabIndex = 5;
            this.SingleU1.Text = "Single Capture File";
            this.SingleU1.UseVisualStyleBackColor = true;
            this.SingleU1.CheckedChanged += new System.EventHandler(this.SingleU1_CheckedChanged);
            // 
            // Browse
            // 
            this.Browse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Browse.Location = new System.Drawing.Point(285, 39);
            this.Browse.Name = "Browse";
            this.Browse.Size = new System.Drawing.Size(75, 23);
            this.Browse.TabIndex = 4;
            this.Browse.Text = "Browse";
            this.Browse.UseVisualStyleBackColor = true;
            this.Browse.Click += new System.EventHandler(this.Browse_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.ManualCaptureFile);
            this.tabPage3.Controls.Add(this.KeepAllCFIFiles);
            this.tabPage3.Controls.Add(this.TimingGroupBox);
            this.tabPage3.Controls.Add(this.Report);
            this.tabPage3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(392, 364);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Algorithm";
            // 
            // ManualCaptureFile
            // 
            this.ManualCaptureFile.AutoSize = true;
            this.ManualCaptureFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ManualCaptureFile.Location = new System.Drawing.Point(156, 332);
            this.ManualCaptureFile.Name = "ManualCaptureFile";
            this.ManualCaptureFile.Size = new System.Drawing.Size(123, 17);
            this.ManualCaptureFile.TabIndex = 5;
            this.ManualCaptureFile.Text = "Manual Capture File ";
            this.ManualCaptureFile.UseVisualStyleBackColor = true;
            // 
            // KeepAllCFIFiles
            // 
            this.KeepAllCFIFiles.AutoSize = true;
            this.KeepAllCFIFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeepAllCFIFiles.Location = new System.Drawing.Point(11, 332);
            this.KeepAllCFIFiles.Name = "KeepAllCFIFiles";
            this.KeepAllCFIFiles.Size = new System.Drawing.Size(108, 17);
            this.KeepAllCFIFiles.TabIndex = 4;
            this.KeepAllCFIFiles.Text = "Keep All CFI Files";
            this.KeepAllCFIFiles.UseVisualStyleBackColor = true;
            this.KeepAllCFIFiles.CheckedChanged += new System.EventHandler(this.KeepAllCFIFiles_CheckedChanged);
            // 
            // TimingGroupBox
            // 
            this.TimingGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TimingGroupBox.Controls.Add(this.label5);
            this.TimingGroupBox.Controls.Add(this.label4);
            this.TimingGroupBox.Controls.Add(this.label3);
            this.TimingGroupBox.Controls.Add(this.label2);
            this.TimingGroupBox.Controls.Add(this.label1);
            this.TimingGroupBox.Controls.Add(this.NudCodeGap);
            this.TimingGroupBox.Controls.Add(this.NudPulseDelayDuration);
            this.TimingGroupBox.Controls.Add(this.NudDelayDuration);
            this.TimingGroupBox.Controls.Add(this.NudPulseDuration);
            this.TimingGroupBox.Controls.Add(this.NudFrequency);
            this.TimingGroupBox.Controls.Add(this.CodeGap);
            this.TimingGroupBox.Controls.Add(this.Frequency);
            this.TimingGroupBox.Controls.Add(this.PulseDelayDuration);
            this.TimingGroupBox.Controls.Add(this.DelayDuration);
            this.TimingGroupBox.Controls.Add(this.PulseDuration);
            this.TimingGroupBox.Controls.Add(this.IncludeExtensionFunctions);
            this.TimingGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimingGroupBox.Location = new System.Drawing.Point(11, 117);
            this.TimingGroupBox.Name = "TimingGroupBox";
            this.TimingGroupBox.Size = new System.Drawing.Size(345, 200);
            this.TimingGroupBox.TabIndex = 3;
            this.TimingGroupBox.TabStop = false;
            this.TimingGroupBox.Text = "Timing";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(193, 164);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "dev(%)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(193, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "dev(%)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(193, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "dev(%)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(193, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "dev(%)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(193, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "dev(%)";
            // 
            // NudCodeGap
            // 
            this.NudCodeGap.Location = new System.Drawing.Point(238, 160);
            this.NudCodeGap.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NudCodeGap.Name = "NudCodeGap";
            this.NudCodeGap.Size = new System.Drawing.Size(39, 20);
            this.NudCodeGap.TabIndex = 14;
            this.NudCodeGap.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NudCodeGap.ValueChanged += new System.EventHandler(this.NudCodeGap_ValueChanged);
            // 
            // NudPulseDelayDuration
            // 
            this.NudPulseDelayDuration.Location = new System.Drawing.Point(238, 106);
            this.NudPulseDelayDuration.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NudPulseDelayDuration.Name = "NudPulseDelayDuration";
            this.NudPulseDelayDuration.Size = new System.Drawing.Size(39, 20);
            this.NudPulseDelayDuration.TabIndex = 13;
            this.NudPulseDelayDuration.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NudPulseDelayDuration.ValueChanged += new System.EventHandler(this.NudPulseDelayDuration_ValueChanged);
            // 
            // NudDelayDuration
            // 
            this.NudDelayDuration.Location = new System.Drawing.Point(238, 79);
            this.NudDelayDuration.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NudDelayDuration.Name = "NudDelayDuration";
            this.NudDelayDuration.Size = new System.Drawing.Size(39, 20);
            this.NudDelayDuration.TabIndex = 12;
            this.NudDelayDuration.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NudDelayDuration.ValueChanged += new System.EventHandler(this.NudDelayDuration_ValueChanged);
            // 
            // NudPulseDuration
            // 
            this.NudPulseDuration.Location = new System.Drawing.Point(238, 52);
            this.NudPulseDuration.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NudPulseDuration.Name = "NudPulseDuration";
            this.NudPulseDuration.Size = new System.Drawing.Size(39, 20);
            this.NudPulseDuration.TabIndex = 11;
            this.NudPulseDuration.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NudPulseDuration.ValueChanged += new System.EventHandler(this.PulseDurationTextBox_ValueChanged);
            // 
            // NudFrequency
            // 
            this.NudFrequency.Location = new System.Drawing.Point(238, 133);
            this.NudFrequency.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NudFrequency.Name = "NudFrequency";
            this.NudFrequency.Size = new System.Drawing.Size(39, 20);
            this.NudFrequency.TabIndex = 10;
            this.NudFrequency.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NudFrequency.ValueChanged += new System.EventHandler(this.NudFrequency_ValueChanged);
            // 
            // CodeGap
            // 
            this.CodeGap.AutoSize = true;
            this.CodeGap.Checked = true;
            this.CodeGap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CodeGap.Location = new System.Drawing.Point(16, 162);
            this.CodeGap.Name = "CodeGap";
            this.CodeGap.Size = new System.Drawing.Size(74, 17);
            this.CodeGap.TabIndex = 5;
            this.CodeGap.Text = "Code Gap";
            this.CodeGap.UseVisualStyleBackColor = true;
            // 
            // Frequency
            // 
            this.Frequency.AutoSize = true;
            this.Frequency.Checked = true;
            this.Frequency.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Frequency.Location = new System.Drawing.Point(16, 135);
            this.Frequency.Name = "Frequency";
            this.Frequency.Size = new System.Drawing.Size(82, 17);
            this.Frequency.TabIndex = 4;
            this.Frequency.Text = "Frequencey";
            this.Frequency.UseVisualStyleBackColor = true;
            // 
            // PulseDelayDuration
            // 
            this.PulseDelayDuration.AutoSize = true;
            this.PulseDelayDuration.Checked = true;
            this.PulseDelayDuration.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PulseDelayDuration.Location = new System.Drawing.Point(16, 108);
            this.PulseDelayDuration.Name = "PulseDelayDuration";
            this.PulseDelayDuration.Size = new System.Drawing.Size(127, 17);
            this.PulseDelayDuration.TabIndex = 3;
            this.PulseDelayDuration.Text = "Pulse/Delay Duration";
            this.PulseDelayDuration.UseVisualStyleBackColor = true;
            // 
            // DelayDuration
            // 
            this.DelayDuration.AutoSize = true;
            this.DelayDuration.Checked = true;
            this.DelayDuration.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DelayDuration.Location = new System.Drawing.Point(16, 81);
            this.DelayDuration.Name = "DelayDuration";
            this.DelayDuration.Size = new System.Drawing.Size(96, 17);
            this.DelayDuration.TabIndex = 2;
            this.DelayDuration.Text = "Delay Duration";
            this.DelayDuration.UseVisualStyleBackColor = true;
            // 
            // PulseDuration
            // 
            this.PulseDuration.AutoSize = true;
            this.PulseDuration.Checked = true;
            this.PulseDuration.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PulseDuration.Location = new System.Drawing.Point(16, 54);
            this.PulseDuration.Name = "PulseDuration";
            this.PulseDuration.Size = new System.Drawing.Size(95, 17);
            this.PulseDuration.TabIndex = 1;
            this.PulseDuration.Text = "Pulse Duration";
            this.PulseDuration.UseVisualStyleBackColor = true;
            // 
            // IncludeExtensionFunctions
            // 
            this.IncludeExtensionFunctions.AutoSize = true;
            this.IncludeExtensionFunctions.Location = new System.Drawing.Point(16, 27);
            this.IncludeExtensionFunctions.Name = "IncludeExtensionFunctions";
            this.IncludeExtensionFunctions.Size = new System.Drawing.Size(159, 17);
            this.IncludeExtensionFunctions.TabIndex = 0;
            this.IncludeExtensionFunctions.Text = "Include Extension Functions";
            this.IncludeExtensionFunctions.UseVisualStyleBackColor = true;
            // 
            // Report
            // 
            this.Report.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Report.Controls.Add(this.ReportAllIDs);
            this.Report.Controls.Add(this.ReportTheTop10IDs);
            this.Report.Controls.Add(this.ReportOnlyTop5IDs);
            this.Report.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Report.Location = new System.Drawing.Point(11, 15);
            this.Report.Name = "Report";
            this.Report.Size = new System.Drawing.Size(345, 87);
            this.Report.TabIndex = 1;
            this.Report.TabStop = false;
            this.Report.Text = "Report";
            // 
            // ReportAllIDs
            // 
            this.ReportAllIDs.AutoSize = true;
            this.ReportAllIDs.Checked = true;
            this.ReportAllIDs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportAllIDs.Location = new System.Drawing.Point(16, 65);
            this.ReportAllIDs.Name = "ReportAllIDs";
            this.ReportAllIDs.Size = new System.Drawing.Size(90, 17);
            this.ReportAllIDs.TabIndex = 4;
            this.ReportAllIDs.TabStop = true;
            this.ReportAllIDs.Text = "Report All IDs";
            this.ReportAllIDs.UseVisualStyleBackColor = true;
            this.ReportAllIDs.CheckedChanged += new System.EventHandler(this.ReportAllIDs_CheckedChanged);
            // 
            // ReportTheTop10IDs
            // 
            this.ReportTheTop10IDs.AutoSize = true;
            this.ReportTheTop10IDs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportTheTop10IDs.Location = new System.Drawing.Point(16, 42);
            this.ReportTheTop10IDs.Name = "ReportTheTop10IDs";
            this.ReportTheTop10IDs.Size = new System.Drawing.Size(135, 17);
            this.ReportTheTop10IDs.TabIndex = 3;
            this.ReportTheTop10IDs.Text = "Report The Top 10 IDs";
            this.ReportTheTop10IDs.UseVisualStyleBackColor = true;
            this.ReportTheTop10IDs.CheckedChanged += new System.EventHandler(this.ReportTheTop_CheckedChanged);
            // 
            // ReportOnlyTop5IDs
            // 
            this.ReportOnlyTop5IDs.AutoSize = true;
            this.ReportOnlyTop5IDs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportOnlyTop5IDs.Location = new System.Drawing.Point(16, 19);
            this.ReportOnlyTop5IDs.Name = "ReportOnlyTop5IDs";
            this.ReportOnlyTop5IDs.Size = new System.Drawing.Size(129, 17);
            this.ReportOnlyTop5IDs.TabIndex = 2;
            this.ReportOnlyTop5IDs.Text = "Report The Top 5 IDs";
            this.ReportOnlyTop5IDs.UseVisualStyleBackColor = true;
            this.ReportOnlyTop5IDs.CheckedChanged += new System.EventHandler(this.ReportOnlyTop5IDs_CheckedChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage4.Controls.Add(this.ExecutorList);
            this.tabPage4.Controls.Add(this.LoadGenericExecutor);
            this.tabPage4.Controls.Add(this.Executor);
            this.tabPage4.Controls.Add(this.ExecutorLabel);
            this.tabPage4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(392, 364);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Exec/Generic";
            // 
            // ExecutorList
            // 
            this.ExecutorList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ExecutorList.Location = new System.Drawing.Point(6, 42);
            this.ExecutorList.MultiSelect = false;
            this.ExecutorList.Name = "ExecutorList";
            this.ExecutorList.Size = new System.Drawing.Size(380, 290);
            this.ExecutorList.TabIndex = 4;
            this.ExecutorList.UseCompatibleStateImageBehavior = false;
            this.ExecutorList.View = System.Windows.Forms.View.List;
            this.ExecutorList.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.ExecutorList_ItemSelectionChanged);
            // 
            // LoadGenericExecutor
            // 
            this.LoadGenericExecutor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.LoadGenericExecutor.AutoSize = true;
            this.LoadGenericExecutor.Enabled = false;
            this.LoadGenericExecutor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadGenericExecutor.Location = new System.Drawing.Point(35, 341);
            this.LoadGenericExecutor.Name = "LoadGenericExecutor";
            this.LoadGenericExecutor.Size = new System.Drawing.Size(135, 17);
            this.LoadGenericExecutor.TabIndex = 2;
            this.LoadGenericExecutor.Text = "Load Generic Executor";
            this.LoadGenericExecutor.UseVisualStyleBackColor = true;
            this.LoadGenericExecutor.CheckedChanged += new System.EventHandler(this.LoadGenericExecutor_CheckedChanged);
            // 
            // Executor
            // 
            this.Executor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Executor.Location = new System.Drawing.Point(146, 10);
            this.Executor.MaxLength = 4;
            this.Executor.Name = "Executor";
            this.Executor.Size = new System.Drawing.Size(100, 20);
            this.Executor.TabIndex = 1;
            this.Executor.TextChanged += new System.EventHandler(this.Executor_TextChanged);
            this.Executor.Validating += new System.ComponentModel.CancelEventHandler(this.Executor_Validating);
            // 
            // ExecutorLabel
            // 
            this.ExecutorLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ExecutorLabel.AutoSize = true;
            this.ExecutorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExecutorLabel.Location = new System.Drawing.Point(88, 13);
            this.ExecutorLabel.Name = "ExecutorLabel";
            this.ExecutorLabel.Size = new System.Drawing.Size(52, 13);
            this.ExecutorLabel.TabIndex = 0;
            this.ExecutorLabel.Text = "Executor:";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.settingsToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(400, 24);
            this.menuStrip.TabIndex = 6;
            this.menuStrip.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runToolStripMenuItem,
            this.viewResultToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.runToolStripMenuItem.Text = "Run";
            this.runToolStripMenuItem.Click += new System.EventHandler(this.runToolStripMenuItem_Click);
            // 
            // viewResultToolStripMenuItem
            // 
            this.viewResultToolStripMenuItem.Name = "viewResultToolStripMenuItem";
            this.viewResultToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.viewResultToolStripMenuItem.Text = "View Result";
            this.viewResultToolStripMenuItem.Click += new System.EventHandler(this.viewResultToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.keepCFIFilesToolStripMenuItem,
            this.onlyTop5IDsToolStripMenuItem,
            this.onlyTop10IDsToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // keepCFIFilesToolStripMenuItem
            // 
            this.keepCFIFilesToolStripMenuItem.Name = "keepCFIFilesToolStripMenuItem";
            this.keepCFIFilesToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.keepCFIFilesToolStripMenuItem.Text = "Keep CFI files";
            this.keepCFIFilesToolStripMenuItem.Click += new System.EventHandler(this.keepCFIFilesToolStripMenuItem_Click);
            // 
            // onlyTop5IDsToolStripMenuItem
            // 
            this.onlyTop5IDsToolStripMenuItem.Name = "onlyTop5IDsToolStripMenuItem";
            this.onlyTop5IDsToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.onlyTop5IDsToolStripMenuItem.Text = "Only Top 5 IDs";
            this.onlyTop5IDsToolStripMenuItem.Click += new System.EventHandler(this.onlyTop5IDsToolStripMenuItem_Click);
            // 
            // onlyTop10IDsToolStripMenuItem
            // 
            this.onlyTop10IDsToolStripMenuItem.Name = "onlyTop10IDsToolStripMenuItem";
            this.onlyTop10IDsToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.onlyTop10IDsToolStripMenuItem.Text = "Only Top 10 IDs";
            this.onlyTop10IDsToolStripMenuItem.Click += new System.EventHandler(this.onlyTop10IDsToolStripMenuItem_Click);
            // 
            // DBSearchEngine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 461);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.OK);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.View);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "DBSearchEngine";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IR DB SearchEngine Options";
            this.Load += new System.EventHandler(this.DBSearchEngine_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.workingDir.ResumeLayout(false);
            this.workingDir.PerformLayout();
            this.BrandGroupBox.ResumeLayout(false);
            this.BrandGroupBox.PerformLayout();
            this.Modes.ResumeLayout(false);
            this.Modes.PerformLayout();
            this.TNSourceFile.ResumeLayout(false);
            this.TNSourceFile.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.TimingGroupBox.ResumeLayout(false);
            this.TimingGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NudCodeGap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudPulseDelayDuration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudDelayDuration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudPulseDuration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudFrequency)).EndInit();
            this.Report.ResumeLayout(false);
            this.Report.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button View;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.RadioButton TNListFile;
        private System.Windows.Forms.RadioButton SingleTNFile;
        private System.Windows.Forms.TextBox TNFiles;
        private System.Windows.Forms.Button Browse;
        private System.Windows.Forms.RadioButton SingleU1;
        private System.Windows.Forms.Label SingleU1Label;
        private System.Windows.Forms.GroupBox TNSourceFile;
        private System.Windows.Forms.TextBox U1Label;
        private System.Windows.Forms.GroupBox Modes;
        private System.Windows.Forms.CheckBox E;
        private System.Windows.Forms.CheckBox D;
        private System.Windows.Forms.CheckBox C;
        private System.Windows.Forms.CheckBox B;
        private System.Windows.Forms.CheckBox A;
        private System.Windows.Forms.CheckBox J;
        private System.Windows.Forms.CheckBox I;
        private System.Windows.Forms.CheckBox H;
        private System.Windows.Forms.CheckBox G;
        private System.Windows.Forms.CheckBox F;
        private System.Windows.Forms.CheckBox T;
        private System.Windows.Forms.CheckBox S;
        private System.Windows.Forms.CheckBox R;
        private System.Windows.Forms.CheckBox Q;
        private System.Windows.Forms.CheckBox P;
        private System.Windows.Forms.CheckBox O;
        private System.Windows.Forms.CheckBox N;
        private System.Windows.Forms.CheckBox M;
        private System.Windows.Forms.CheckBox L;
        private System.Windows.Forms.CheckBox K;
        private System.Windows.Forms.CheckBox Y;
        private System.Windows.Forms.CheckBox X;
        private System.Windows.Forms.CheckBox W;
        private System.Windows.Forms.CheckBox V;
        private System.Windows.Forms.CheckBox U;
        private System.Windows.Forms.CheckBox All;
        private System.Windows.Forms.CheckBox Z;
        private System.Windows.Forms.TextBox Executor;
        private System.Windows.Forms.Label ExecutorLabel;
        private System.Windows.Forms.CheckBox LoadGenericExecutor;
        private System.Windows.Forms.ListView ExecutorList;
        private System.Windows.Forms.GroupBox BrandGroupBox;
        private System.Windows.Forms.TextBox BrandesTextBox;
        private System.Windows.Forms.Label BrandesLabel;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox Report;
        private System.Windows.Forms.RadioButton ReportAllIDs;
        private System.Windows.Forms.RadioButton ReportTheTop10IDs;
        private System.Windows.Forms.RadioButton ReportOnlyTop5IDs;
        private System.Windows.Forms.CheckBox ManualCaptureFile;
        private System.Windows.Forms.CheckBox KeepAllCFIFiles;
        private System.Windows.Forms.GroupBox TimingGroupBox;
        private System.Windows.Forms.CheckBox CodeGap;
        private System.Windows.Forms.CheckBox Frequency;
        private System.Windows.Forms.CheckBox PulseDelayDuration;
        private System.Windows.Forms.CheckBox DelayDuration;
        private System.Windows.Forms.CheckBox PulseDuration;
        private System.Windows.Forms.CheckBox IncludeExtensionFunctions;
        private System.Windows.Forms.GroupBox workingDir;
        private System.Windows.Forms.TextBox workDirBox;
        private System.Windows.Forms.Button workDirBrowse;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem keepCFIFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem onlyTop5IDsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem onlyTop10IDsToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown NudFrequency;
        private System.Windows.Forms.NumericUpDown NudCodeGap;
        private System.Windows.Forms.NumericUpDown NudPulseDelayDuration;
        private System.Windows.Forms.NumericUpDown NudDelayDuration;
        private System.Windows.Forms.NumericUpDown NudPulseDuration;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}

