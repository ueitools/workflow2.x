using System;
using System.Windows.Forms;
using BusinessObject;

namespace IRDBSearchEngine
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args.Length > 0 && args[0] == "ShowResult")
            {
                Application.Run(new CommonForms.ResultsForm());
            }
            else Application.Run(new DBSearchEngine());
        }
    }
}