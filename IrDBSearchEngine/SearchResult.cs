using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace IRDBSearchEngine
{
    [Serializable]
    public class SearchResult
    {
        private string _trackingNumber;
        private int _exec;
        private string _captureFile;
        private string _id;
        private string _database;
        private string _readStatus;
        private ResultItem.List _resultItems;
        private List<string> _skippedFileMessages;

        public SearchResult()
        {
            _resultItems = new ResultItem.List();
            _conflictResults = new ResultItem.ConflictList();
        }

        public string TrackingNumber
        {
            get { return _trackingNumber; }
            set { _trackingNumber = value; }
        }

        public int Exec
        {
            get { return _exec; }
            set { _exec = value; }
        }

        public string CaptureFile
        {
            get { return _captureFile; }
            set { _captureFile = value; }
        }

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Database
        {
            get { return _database; }
            set { _database = value; }
        }

        public string ReadStatus
        {
            get { return _readStatus; }
            set { _readStatus = value; }
        }

        public ResultItem.List ResultItems
        {
            get { return _resultItems; }
            set { _resultItems = value; }
        }


        //Added by Venki for Intron Enhancement - Start
        private ResultItem.ConflictList _conflictResults;
        public ResultItem.ConflictList ConflictResults
        {
            get { return _conflictResults; }
            set { _conflictResults = value; }
        }
        //Added by Venki for Intron Enhancement - End
        [XmlArrayItem("Message")]
        public List<string> SkippedFileMessages
        {
            get { return _skippedFileMessages; }
            set { _skippedFileMessages = value; }
        }

        public ResultItem AddResult(string name, string data, string label, string intron,string priority,bool exactMatch)
        {
            ResultItem result = new ResultItem(name, data, label, intron, priority,exactMatch);
            ResultItems.Add(result);
            return result;
        }
        ////Added by Venki for Intron Enhancement - Start
        public void AddConflictMatch(ConflictResultItem conflictItem)
        {
            //ConflictResultItem match = new ConflictResultItem();
            ConflictResults.Add(conflictItem);
        }
        //Added by Venki for Intron Enhancement - End
        public void WriteToFile(string filename)
        {
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(GetType());
            TextWriter writer = new StreamWriter(filename);
            x.Serialize(writer, this);
            writer.Close();
        }

        public static SearchResult ReadFromFile(string filename)
        {
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(SearchResult));
            TextReader reader = new StreamReader(filename);
            SearchResult retValue = (SearchResult)x.Deserialize(reader);
            reader.Close();
            return retValue;

        }
    }

    [Serializable]
    public class ResultItem
    {
        private string _name;
        private string _data;
        private string _label;
        private string _intron;
        private string _priority;
        private bool _exactMatch;
        private List _matches;
        private ConflictList _conflictMatchs;

        public ResultItem()
        {
            _matches = new List();
            _conflictMatchs = new ConflictList();
        }

        public ResultItem(string name, string data, string label, string intron, string priority,bool exactMatch)
        {
            _name = name;
            _data = data;
            _label = label;
            _intron = intron;
            _priority = priority;
            _exactMatch = exactMatch;
            _matches = new List();
            _conflictMatchs = new ConflictList();
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }

        public string Intron
        {
            get { return _intron; }
            set { _intron = value; }
        }

        public string IntronPriority
        {
            get { return _priority; }
            set { _priority = value; }
        }

        public List Matches
        {
            get { return _matches; }
            set { _matches = value; }
        }
        
        public bool ExactMatch
        {
            get { return _exactMatch; }
            set { _exactMatch = value; }
        }
        
        public ConflictList ConflictMatches
        {
            get { return _conflictMatchs; }
            set { _conflictMatchs = value; }
        }
        
        public ResultItem AddMatch(string name, string data, string label, string intron,string priority,bool exactMatch)
        {
            ResultItem match = new ResultItem(name, data, label, intron, priority,exactMatch);
            Matches.Add(match);
            return match;
        }
        
        public void AddConflictMatch(ConflictResultItem conflictItem)
        {
            ConflictMatches.Add(conflictItem);
        }
        
        public string WriteFormatted()
        {
            return String.Format("{0,-15}{1,-2}{2}{3}{4,-30}{5}", _name, ":", _data, "              ", _label, _intron);
        }

        [Serializable]
        public class List : List<ResultItem> { }

        [Serializable]
        public class ConflictList : List<ConflictResultItem> { }
    }


    //Added by Venki for Intron Enhancement - Start
    [Serializable]
    public class ConflictResultItem
    {
        private string _id;
        private string _label;
        private string _intron;
        private string _dbLabel;
        private string _dbIntron;
        private bool _isFromError;
        private bool _isFormat;
        private string _assignedIntron;

        private string _conflictDetails;

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string CapLabel
        {
            get { return _label; }
            set { _label = value; }
        }

        public string CapIntron
        {
            get { return _intron; }
            set { _intron = value; }
        }

        public string DBLabel
        {
            get { return _dbLabel; }
            set { _dbLabel = value; }
        }

        public string DBIntron
        {
            get { return _dbIntron; }
            set { _dbIntron = value; }
        }

        public bool IsFromError
        {
            get { return _isFromError; }
            set { _isFromError = value; }
        }

        public bool IsFormat
        {
            get { return _isFormat; }
            set { _isFormat = value; }
        }
       
        public string AssignedIntron
        {
            get { return _assignedIntron; }
            set { _assignedIntron = value; }
        }

        public string ConflictDetails
        {
            get { return _conflictDetails; }
            set { _conflictDetails = value; }
        }

        //public ConflictList ConflictMatches
        //{
        //    get { return _conflictMatches; }
        //    set { _conflictMatches = value; }
        //}
    }
    //Added by Venki for Intron Enhancement - End

}





