using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections;
using CommonForms;
using ExecLib;
using BusinessObject;
using IDCompare;

namespace IRDBSearchEngine
{
    class Compare
    {
        public string WORKDIR = "";
        public bool KeepAllCFIFiles = false;
        FunctionsSignals m_FunctionsSignals = FunctionsSignals.GetInstance();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="IDHeaderData"></param>
        /// <param name="IDdata"></param>
        /// <param name="IDheader"></param>
        /// <param name="results"></param>
        /// <param name="id"></param>
        /// <param name="labelList"></param>
        /// <param name="inputFile"></param>
        /// <param name="cfiFiles"></param>
        /// <param name="RXDataTable"></param>
        /// <param name="RXCFIInfoTable"></param>
        /// <param name="listID"></param>
        /// <param name="OutFile2"></param>
        /// <param name="logFileContents"></param>
        /// <param name="skippedFiles"></param>
        /// <param name="matched"></param>
        public void DoCompare(IDHeader IDHeaderData, HashtableCollection IDdata,
            HashtableCollection IDheader, List<CompareResults> results,
            string id, Hashtable labelList, string inputFile,
            StringCollection cfiFiles, ref Hashtable RXDataTable,
            ref Hashtable RXCFIInfoTable, SearchFunctions.ListID listID, string OutFile2,
            ref string logFileContents, List<string> skippedFiles,
            out bool matched)
        {
            matched = false;

            logFileContents += string.Format("\r\n\r\nComparing ({0}){1} to {2}\r\n",
                                    listID.Prefix, id, Path.GetFileName(inputFile));

            logFileContents += "Reading CFI files from capture file: " + inputFile + "\r\n";
            logFileContents += String.Format("      Total {0} CFI files read.\r\n", cfiFiles.Count);

            int Executor_Code = 0;
            if (IDheader.Count > 0)
                Executor_Code = (int)IDheader[0]["Executor_Code"];
            else
            {
                if (IDHeaderData == null)
                {
                    return;
                }
                Executor_Code = IDHeaderData.Executor_Code;
            }

            SearchResult searchResult = new SearchResult();
            searchResult.TrackingNumber = Path.GetFileName(inputFile);
            searchResult.Exec = Executor_Code;
            searchResult.CaptureFile = inputFile;
            searchResult.ID = id;
            searchResult.Database = listID.DBName;

            ICompareID CfiCmp = new ICompareID();
            List<int> Prefixes = FunctionList.CollectPrefixes(id, IDheader);

            double passed = 0.0, failed = 0.0, intronPassed = 0.0, intronFailed = 0.0;
            string captureFileName = Path.GetFileName(inputFile);

            Hashtable intronTable = new Hashtable();
            Hashtable intronPriorityTable = new Hashtable();
            Hashtable labelTable = new Hashtable();

            string RXData = "", TXData = "";
            CFIInfo RXCFIInfo = null, TXCFIInfo = null;
            string failedData = "";
            string passedData = "";

            int index = -1, count = -1;
            GetIndexAndCount(IDdata, id, out index, out count);

            logFileContents +=
                string.Format("Reading CFI files from ID: ({0}){1}\r\n", listID.Prefix, id);

            logFileContents += String.Format("      Total {0} CFI files read.\r\n", count);
            logFileContents += "Start comparing CFI files:\r\n";

            if (count <= 0)
            {
                logFileContents +=
                    string.Format("      No match found in ID: ({0}){1}", listID.Prefix, id);
                return;
            }


            GetLabelIntrons(IDdata, index, id, ref intronTable,
                ref labelTable, ref intronPriorityTable);

            Hashtable TXDataTable = new Hashtable();
            Hashtable TXCFIInfoTable = new Hashtable();

            #region Loop for each OutFile1 in cfiFiles
            foreach (string OutFile1 in cfiFiles)
            {
                int DoCompResult = 0;
                bool found = false;
                string intron = null;
                string data = "";
                RXData = (string)RXDataTable[OutFile1];
                RXCFIInfo = (CFIInfo)RXCFIInfoTable[OutFile1];
                int i = index;
                bool txCalled = false;

                for (; i < (index + count); i++)
                {
                    data = (string)IDdata[i]["Data"];
                    if (string.IsNullOrEmpty(data))
                        continue;

                    if (Executor_Code == 90 && RXData != null)
                    {
                        string s = data + "B "
                            + GetComplement(data);

                        if (!RXData.Contains(s))
                            continue;
                    }

                    TXData = (string)TXDataTable[data];
                    TXCFIInfo = (CFIInfo)TXCFIInfoTable[data];

                    if (TXData == null)
                    {
                        txCalled = true;

                        try
                        {
                            if (IDheader.Count > 0)
                                FunctionList.CreateTxCfiFile(IDheader, Prefixes,
                                    IDdata[i], OutFile2);
                            else FunctionList.CreateTxCfiFile(IDHeaderData, Prefixes,
                                IDdata[i], OutFile2);
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                    }
                    #region CfiCmp.DoCompare
                    if (RXData == null && TXData == null)
                    {
                        CfiCmp.DoCompare(OutFile1, OutFile2, out DoCompResult,
                            out RXData, out TXData, out RXCFIInfo, out TXCFIInfo);
                        RXDataTable[OutFile1] = RXData;
                        RXCFIInfoTable[OutFile1] = RXCFIInfo;
                        TXDataTable[data] = TXData;
                        TXCFIInfoTable[data] = TXCFIInfo;
                    }
                    else if (RXData == null)
                    {
                        CfiCmp.DoCompare(OutFile1, out DoCompResult,
                            out RXData, TXData, out RXCFIInfo);
                        RXDataTable[OutFile1] = RXData;
                        RXCFIInfoTable[OutFile1] = RXCFIInfo;
                    }
                    else if (TXData == null)
                    {
                        CfiCmp.DoCompare(OutFile2, out DoCompResult,
                            RXData, out TXData, out TXCFIInfo);
                        TXDataTable[data] = TXData;
                        TXCFIInfoTable[data] = TXCFIInfo;
                    }
                    else
                    {
                        CfiCmp.DoCompare(out DoCompResult, RXData, TXData);
                    }
                    #endregion
                    if (DoCompResult > 0)
                    {
                        matched = true;
                        passed = passed + 1.0;

                        intron = IntronMatch(OutFile1, id.Substring(0, 1),
                            labelList, intronTable[data] as StringCollection);

                        if (intron != null)
                            intronPassed = intronPassed + 1.0;
                        else intronFailed = intronFailed + 1.0;

                        found = true;
                        break;
                    }
                }
                #region if Keep All CFI Files
                if (KeepAllCFIFiles)
                {
                    try
                    {
                        string path = OutFile1.Substring(0,
                            OutFile1.LastIndexOf("\\") + 1)
                            + id;

                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        string destFileName = OutFile1.ToUpper()
                            .Replace(".U1.CFI", "#.cfi");
                        destFileName = destFileName.ToUpper()
                            .Replace(".U2.CFI", "#.cfi");
                        destFileName = Path.GetFileName(destFileName);
                        destFileName = path + "\\" + destFileName;

                        File.Copy(OutFile1, destFileName, true);
                    }
                    catch { }
                }
                #endregion

                string rxData = string.Empty;
                if (string.IsNullOrEmpty(RXData) == false)
                {
                    rxData = RXData.Replace("FD", "");
                    rxData = rxData.Replace("B", "").Trim();
                }

                string fileName = Path.GetFileName(OutFile1);
                fileName = fileName.Replace(".cfi", "");

                if (!found)
                {
                    failed = failed + 1.0;
                    intronFailed = intronFailed + 1.0;
                    string u1File = Path.GetFileName(OutFile1);
                    u1File = u1File.Replace(".U1", "");
                    u1File = u1File.Replace(".U2", "");
                    failedData += String.Format(
                        "{0, -21}{1}       {2}\r\n\r\n",
                        u1File, rxData, (string)labelList[fileName]);
                    ResultItem unMatechedRresultItem = searchResult.AddResult(u1File, rxData, (string)labelList[fileName], string.Empty, string.Empty, false);
                    //Added by Venki for Intron Enhancement - Start
                    //if (passed > 0.0 && intronPassed > 0.0)
                    //{
                    ConflictResultItem cResult = new ConflictResultItem();
                    cResult.ID = id;
                    cResult.CapIntron = string.Empty;
                    cResult.CapLabel = (string)labelList[fileName];
                    cResult.IsFromError = true;
                    cResult.IsFormat = true;
                    unMatechedRresultItem.AddConflictMatch(cResult);
                    //}
                    //Added by Venki for Intron Enhancement - End


                }
                else
                {
                    #region if Keep All CFI Files
                    if (KeepAllCFIFiles)
                    {
                        try
                        {
                            if (!txCalled)
                            {
                                if (IDheader.Count > 0)
                                    FunctionList.CreateTxCfiFile(
                                        IDheader, Prefixes,
                                        IDdata[i], OutFile2);
                                else FunctionList.CreateTxCfiFile(
                                    IDHeaderData, Prefixes,
                                    IDdata[i], OutFile2);
                            }

                            string path = OutFile1.Substring(0,
                                OutFile1.LastIndexOf("\\") + 1)
                                + id;

                            string destFileName = Path.GetFileName(OutFile1);
                            destFileName = "!" + id
                                + destFileName.Substring(6, 2) + ".cfi";
                            destFileName = path + "\\" + destFileName;
                            File.Copy(OutFile2, destFileName, true);
                        }
                        catch { }
                    }
                    #endregion

                    //added for new functions and new signals                     
                    m_FunctionsSignals.SetNewSignalFlag(inputFile, Path.GetFileName(fileName), false);

                    string txData = TXData.Replace("FD", "");
                    txData = txData.Replace("B", "").Trim();

                    passedData += "--------------------------"
                        + "----------------------------------"
                        + "------------------------------\r\n\r\n";

                    StringCollection introns
                        = (StringCollection)intronTable[data];
                    StringCollection intronPrioritys
                        = (StringCollection)intronPriorityTable[data];

                    StringCollection labels
                        = (StringCollection)labelTable[data];

                    if (introns == null || labels == null)
                        continue;

                    string rxLabel = (string)labelList[fileName];
                    string rxIntron = string.Empty;
                    if (intron != null)
                        rxIntron = intron;
                    else if (labelList[fileName] != null)
                    {
                        string label = (string)labelList[fileName];

                        if (!label.Contains("|"))
                            rxIntron += FunctionList.GetIntronData(id.Substring(0, 1), label);
                        else
                        {
                            string[] strs = label.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                            if (strs != null && strs.Length > 0)
                            {
                                for (int k = 0; k < strs.Length; k++)
                                {
                                    string s = FunctionList.GetIntronData(id.Substring(0, 1), strs[k].Trim());
                                    if (s != null)
                                    {
                                        rxIntron = s;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(rxLabel))
                        rxLabel = string.Empty;

                    ResultItem resultItem = searchResult.AddResult(fileName, rxData, rxLabel, rxIntron, string.Empty, false);
                    //Added by Venki for Intron Enhancement - Start
                    bool isMatch = false;
                    bool isCapIntronEmpty = false;

                    string capLabel = string.Empty;
                    string capIntron = string.Empty;
                    string dbLabel = string.Empty;
                    string dbIntron = string.Empty;
                    string assignedIntron = string.Empty;
                    string intronPriority = string.Empty;
                    string matchEx = string.Empty;
                    bool exMatch = false;
                    //Added by Venki for Intron Enhancement - End
                    for (int j = 0; j < labels.Count; j++)
                    {
                        passedData += resultItem.WriteFormatted() + "\r\n";

                        string matchedIntron = introns[j];

                        if (intronPrioritys[j] != null && intronPrioritys[j].Trim() != "")
                        {
                            intronPriority = "(p" + intronPrioritys[j].Trim() + ")";
                            matchedIntron += intronPriority;
                        }
                        //bool exMatch = false;
                        //if (labels[j].ToUpper().Trim() == rxLabel.ToUpper().Trim() && introns[j] == rxIntron) exMatch = true;
                        if (string.IsNullOrEmpty(matchEx))
                        {
                            exMatch = getExactMatch(labels, introns, rxLabel, rxIntron, j);
                            matchEx = exMatch.ToString();
                        }

                        ResultItem match = resultItem.AddMatch(listID.DBName, txData, labels[j], matchedIntron, intronPriority, exMatch);
                        //Added by Venki for Intron Enhancement - Start
                        if (intron != null) capIntron = intron;

                        //if (labels[j].ToUpper() == rxLabel.ToUpper() && introns[j] == rxIntron)
                        if (string.IsNullOrEmpty(introns[j]) && labels[j].ToUpper().Trim() == rxLabel.ToUpper().Trim())
                        {
                            isMatch = true;
                        }

                        if (!string.IsNullOrEmpty(introns[j]) && introns[j] == rxIntron)
                        {
                            isMatch = true;
                            //continue;
                        }

                        if (string.IsNullOrEmpty(rxIntron) && labels[j] == rxLabel)
                        {
                            isCapIntronEmpty = true;
                        }
                        assignedIntron = rxIntron;
                        if (string.IsNullOrEmpty(dbIntron))
                        {
                            dbIntron = introns[j];
                        }
                        else
                        {
                            //if(!dbIntron.Contains(introns[j]))
                            dbIntron += "," + introns[j];
                        }
                        //dbIntron = introns[j];
                        if (string.IsNullOrEmpty(dbLabel))
                        {
                            dbLabel = labels[j];
                        }
                        else
                        {
                            //if (!dbLabel.Contains(labels[j]))
                            dbLabel += "," + labels[j];
                        }
                        //dbLabel = labels[j];
                        //Added by Venki for Intron Enhancement - End
                        passedData += match.WriteFormatted() + "\r\n\r\n";
                    }

                    //Added by Venki for Intron Enhancement - Start
                    if (!isMatch && !isCapIntronEmpty)
                    {

                        ConflictResultItem cResult = new ConflictResultItem();
                        cResult.ID = id;
                        cResult.CapIntron = capIntron;
                        cResult.CapLabel = rxLabel;
                        cResult.DBIntron = dbIntron;
                        cResult.DBLabel = dbLabel;
                        cResult.AssignedIntron = assignedIntron;
                        cResult.IsFromError = false;
                        cResult.IsFormat = true;// !(dbIntron.Trim() == capIntron.Trim());
                        resultItem.AddConflictMatch(cResult);
                    }
                    if (isCapIntronEmpty)
                    {
                        if (intronFailed > 0) intronFailed = intronFailed - 1.0;
                        ConflictResultItem cResult = new ConflictResultItem();
                        cResult.ID = id;
                        cResult.CapIntron = capIntron;
                        cResult.CapLabel = rxLabel;
                        cResult.DBIntron = dbIntron;
                        cResult.DBLabel = dbLabel;
                        cResult.AssignedIntron = assignedIntron;
                        cResult.IsFromError = false;
                        cResult.IsFormat = true;
                        resultItem.AddConflictMatch(cResult);
                    }
                    //Added by Venki for Intron Enhancement - End
                    List<string> errorMesages;

                    if (!CfiCmp.CompareCFIInfo(RXCFIInfo, TXCFIInfo, out errorMesages))
                    {
                        foreach (string message in errorMesages)
                        {
                            passedData += message;
                            passedData += "\r\n";
                        }

                        passedData += "\r\n";
                    }
                }
            }
            #endregion

            if (passed == 0.0)
            {
                logFileContents += "      No match found in ID: " + listID.Prefix + id;
                return;
            }

            string comparisonFilePath =
                WORKDIR + Path.GetFileNameWithoutExtension(inputFile);

            comparisonFilePath += "_" + id + listID.Extension;

            StreamWriter comparisonFile = File.CreateText(comparisonFilePath);

            logFileContents += "      Possible match found in ID: " +
                listID.Prefix + id + "\r\n";

            logFileContents += String.Format("      {0} has been created!",
                comparisonFilePath);

            if (failed > 0)
                failedData = "---------------------------"
                    + "----------------------------------"
                    + "------------------------------\r\n\r\n"
                    + "Unmatched CFI files:\r\n\r\n" + failedData;
            else
                failedData = "---------------------------"
                    + "----------------------------------"
                    + "------------------------------\r\n\r\n";

            failedData += "Total " + failed + " unmatched CFI files.";

            if (skippedFiles != null && skippedFiles.Count > 0)
            {
                failedData = "\r\n\r\n---------------------------"
                    + "----------------------------------"
                    + "------------------------------\r\n\r\n";
                failedData += "\r\n" + string.Join("\r\n", skippedFiles.ToArray()) + "\r\n";
            }

            comparisonFile.WriteLine("Tracking Number = "
                + Path.GetFileName(inputFile));
            comparisonFile.WriteLine("Executor Number = "
                + Executor_Code);
            comparisonFile.WriteLine("Comparing Capture File: "
                + inputFile + " to ID: "
                + id);
            comparisonFile.WriteLine("DataBase type: " + listID.DBName);

            comparisonFile.Write("\r\n" + passedData);
            comparisonFile.Write(failedData);

            string searchResultFilePath = WORKDIR + Path.GetFileNameWithoutExtension(inputFile) + "_" + id +
                                          listID.Extension + ".xml";
            searchResult.SkippedFileMessages = skippedFiles;
            searchResult.WriteToFile(searchResultFilePath);

            CompareResults res = new CompareResults();

            res.captureFile = captureFileName;
            res.exec = String.Format("Exec{0:000}",
                Executor_Code);

            res.ID = listID.Prefix + id;
            res.dataMatch = (int)((passed / (passed + failed)) * 100);

            res.intronMatch = 0;
            //
            double tempIntronMatch = intronPassed + intronFailed;
            if (tempIntronMatch > 0.0)
            {
                res.intronMatch = (int)((intronPassed / (tempIntronMatch)) * 100);
            }
            
                
            res.outputFilePath = comparisonFilePath;

            //if (res.dataMatch > 50)
            //{
            AddResult(results, res);
            //}
            comparisonFile.Close();

            return;
        }


        /// <summary>
        /// Convert mode and label (from labelList) into an intron
        /// and try to find it in intronTable
        /// </summary>
        /// <param name="file">name of CFI file to index into labelList</param>
        /// <param name="mode">for (mode, label) to intron conversion</param>
        /// <param name="labelList"></param>
        /// <param name="introns">intronTable[data] subset</param>
        /// <returns>intron if found, null if not</returns>
        private string IntronMatch(string file, string mode,
                                    Hashtable labelList, StringCollection introns)
        {
            if (file == null || introns == null)
                return null;

            string fileName = Path.GetFileName(file);
            fileName = fileName.Replace(".cfi", "");

            string label = (string)labelList[fileName];
            //
            if (string.IsNullOrEmpty(label))
                return null;

            if (label == null)
                return null;

            string intron = FunctionList.GetIntronData(mode, label);

            if (introns.Contains(intron))
                return intron;

            if (label.Contains("|"))
            {
                string[] strs = label.Split(new string[] { "|" },
                    StringSplitOptions.RemoveEmptyEntries);
                if (strs == null || strs.Length == 0)
                    return null;
                for (int i = 0; i < strs.Length; i++)
                {
                    intron = FunctionList.GetIntronData(mode, strs[i].Trim());
                    if (introns.Contains(intron))
                        return intron;
                }
            }

            return null;
        }

        /// <summary>
        /// For the given ID and each data item in DataSets collect:
        /// {introns into intronTabel[data]}
        /// {Labels into labelTable[data]}
        /// {IntronPriority into intronPriorityTable[data]}
        /// </summary>
        /// <param name="DataSets">Collection containing ID data sets</param>
        /// <param name="index">Start index of ID in DataSets</param>
        /// <param name="id">ID Name</param>
        /// <param name="intronTable">table to store introns</param>
        /// <param name="labelTable">table to store labels</param>
        /// <param name="intronPriorityTable">table to store pority</param>
        private void GetLabelIntrons(HashtableCollection DataSets, int index,
            string id, ref Hashtable intronTable, ref Hashtable labelTable,
            ref  Hashtable intronPriorityTable)
        {
            for (int i = index; i < DataSets.Count && (string)DataSets[i]["ID"] == id; i++)
            {
                string data = (string)DataSets[i]["Data"];

                if (string.IsNullOrEmpty(data))
                    continue;

                if (intronTable.Contains(data))
                {
                    ((StringCollection)intronTable[data]).Add((string)DataSets[i]["Intron"]);
                    ((StringCollection)labelTable[data]).Add((string)DataSets[i]["Label"]);
                    ((StringCollection)intronPriorityTable[data]).Add((string)DataSets[i]["IntronPriority"]);
                }
                else
                {
                    StringCollection intronList = new StringCollection();
                    StringCollection labelList = new StringCollection();
                    StringCollection intronPriorityList = new StringCollection();

                    intronList.Add((string)DataSets[i]["Intron"]);
                    labelList.Add((string)DataSets[i]["Label"]);
                    intronPriorityList.Add((string)DataSets[i]["IntronPriority"]);

                    intronTable[data] = intronList;
                    labelTable[data] = labelList;
                    intronPriorityTable[data] = intronPriorityList;
                }
            }
        }

        /// <summary>
        /// Insert a line of results in sorted order
        /// </summary>
        /// <param name="list"></param>
        /// <param name="res"></param>
        private void AddResult(List<CompareResults> list, CompareResults res)
        {
            if (list == null)
                return;

            if (list.Count == 0)
            {
                list.Add(res);
                return;
            }

            int index = 0;

            while (index < list.Count
                && (res.dataMatch < list[index].dataMatch
                || (res.dataMatch == list[index].dataMatch
                && res.intronMatch < list[index].intronMatch)))
                index++;
            list.Insert(index, res);
        }

        /// <summary>
        /// Generic Binary search of a Hashtable Collection
        /// Find the start [index] of ID in the DataSets 
        /// Then find the [count] of ID entries in the DataSets
        /// </summary>
        /// <param name="DataSets">Collection containing ID data sets</param>
        /// <param name="id">Search value</param>
        /// <param name="index">returned ID set start index</param>
        /// <param name="count">returned ID set length</param>
        private void GetIndexAndCount(HashtableCollection DataSets,
            string id, out int index, out int count)
        {
            if (DataSets.Count == 0)
            {
                index = -1;
                count = 0;
                return;
            }

            int start = 0, end = DataSets.Count - 1, middle = -1;

            if (String.Compare((string)DataSets[end]["ID"], id) < 0 ||
                String.Compare((string)DataSets[start]["ID"], id) > 0)
            {
                index = -1;
                count = 0;
                return;
            }

            if ((string)DataSets[start]["ID"] == id)
            {
                middle = start;
            }
            else if ((string)DataSets[end]["ID"] == id)
            {
                middle = end;
            }

            if (middle >= 0)
            {
                GetRangeCount(DataSets, id, middle,
                    out index, out count);
                return;
            }

            while (start < end)
            {
                middle = (start + end) / 2;

                if (middle == start)
                    break;

                if ((string)DataSets[middle]["ID"] == id)
                    break;

                if (String.Compare((string)
                    DataSets[middle]["ID"], id) < 0)
                {
                    start = middle;
                }
                else end = middle;
            }

            if (id != (string)DataSets[middle]["ID"])
            {
                index = -1;
                count = 0;
                return;
            }

            GetRangeCount(DataSets, id, middle, out index, out count);

            return;
        }

        /// <summary>
        /// Given some point in the table collection that is part of the [id] set
        /// Find the start [index] and the total [count] of [id] entries.
        /// </summary>
        /// <param name="DataSets">HashtableCollection</param>
        /// <param name="id">Search value</param>
        /// <param name="middle">an index within ID set</param>
        /// <param name="index">returned ID set start index</param>
        /// <param name="count">returned ID set length</param>
        private void GetRangeCount(HashtableCollection DataSets, string id,
            int middle, out int index, out int count)
        {
            int start, end;
            start = end = middle;

            while (start >= 0 && id == (string)DataSets[start]["ID"])
                start--;
            start++;

            while (end < DataSets.Count && id == (string)DataSets[end]["ID"])
                end++;
            end--;

            index = start;
            count = end - start + 1;
        }

        /// <summary>
        /// copy every '1' as a '0' and '0' as '1' from any given string
        /// ignore other characters.
        /// </summary>
        /// <param name="data">string to modify</param>
        /// <returns>Modified string</returns>
        private string GetComplement(string data)
        {
            string s = "";
            for (int i = 0; i < data.Length; i++)
                if (data[i] == '0')
                    s = s + '1';
                else if (data[i] == '1')
                    s = s + '0';
            return s;
        }

        /// <summary>
        /// Added to find exact match for the lable and intron to set the priority. Added as part of intron search enhancement.
        /// </summary>
        /// <param name="dbLabels"></param>
        /// <param name="dbIntrons"></param>
        /// <param name="capLabel"></param>
        /// <param name="capIntron"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private bool getExactMatch(StringCollection dbLabels, StringCollection dbIntrons, string capLabel, string capIntron, int index)
        {
            bool result = false;

            if (dbLabels.Count > 1)
            {
                for (int i = index; i < dbLabels.Count; i++)
                {
                    if (dbIntrons[i] == capIntron && dbLabels[i].ToUpper().Trim() == capLabel.ToUpper().Trim())
                    {
                        result = true;
                    }
                }
            }
            else
            {
                if (dbIntrons[index] == capIntron && dbLabels[index].ToUpper().Trim() == capLabel.ToUpper().Trim())
                {
                    result = true;
                }
            }

            return result;
        }
    }
}
