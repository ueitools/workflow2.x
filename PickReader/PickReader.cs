using System;
using System.Collections.Generic;
using ToolsData;
using IRow = System.Collections.Generic.IDictionary<string, string>;
using RaimaWrapperLib;

namespace UEI.Legacy
{
    /// <summary>
    /// id need to convert to legacy format (3 or 4 digit) for input
    /// and converted from legacy for output
    /// </summary>
    public class PickReader : IPickReader
    {
        public abstract class PickIdFields {
            public const string Sequence = "Sequence";
            public const string ID = "ID";
            public const string SourceID = "SourceID";
            public const string Flags = "Flags";
            public const string Version = "Version";
            public const string Time = "Time";
            public const string Executor_Code = "Executor_Code";
            public const string Name = "Name";
            public const string Text = "Text";
            public const string IsInversedData = "IsInversedData";
            public const string IsExternalPrefix = "IsExternalPrefix";
            public const string IsRestricted = "IsRestricted";
            public const string IsFrequencyData = "IsFrequencyData";
            public const string IsInversedPrefix = "IsInversedPrefix";
        }

        public abstract class PickItemFields {
            public const string ID = "ID";
            public const string DATA = "DATA";
            public const string Flags = "Flags";
            public const string Label = "Label";
            public const string Intron = "Intron";
            public const string Outron = "Outron";
            public const string Text = "Text";
        }

        public abstract class PrefixItemFields {
            public const string ID = "ID";
            public const string DATA = "DATA";
            public const string Text = "Text";
        }

        string _dbPath;
        string _dbNameWithoutExt;

        public PickReader(string dbPath, string dbNameWithoutExt)
        {
            _dbPath = dbPath;
            _dbNameWithoutExt = dbNameWithoutExt;
        }

        /////////////////////////////////////////////////////////////////////
        ////
        //// explicit interface implementation
        ////

        /// <summary>
        /// sequence, id, pickFromID, flags,
        /// version, time, exec, name, comment
        /// </summary>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        IRow IPickReader.GetPickID(string id, int version)
        {
            IRaimaQuery rq = new RaimaQuery();
            rq.Setup(_dbPath, _dbNameWithoutExt);
            id = LegacyParser.ToLegacyIDFormat(id);

            Array strList = rq.ExecuteQuery(
                "SELECT * FROM pickID " +
                "WHERE id = '" + id + "' and version = " + version);

            string[] columnList = {"Sequence", "ID", "SourceID", "Flags", 
                "Version", "Time", "Executor_Code", "Name", "Text"};

            IList<IRow> rowList = IRowHelper.GetRowList(strList, columnList);
            if (rowList.Count > 0)
            {
                IRow row = rowList[0];
                return TransformID(row, IsInversedPrefix(id, version));
            }
            else
                return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        IList<IRow> IPickReader.GetPickItem(string id, int version)
        {
            IRaimaQuery rq = new RaimaQuery();
            rq.Setup(_dbPath, _dbNameWithoutExt);
            id = LegacyParser.ToLegacyIDFormat(id);

            Array strList = rq.ExecuteQuery(
                "SELECT id, db, bitCnt, flags, label, " +
                "       intron, outron, pickItemRec.comment " +
                "FROM   pickItem " +
                "WHERE id = '" + id + "' and version = " + version);

            string[] columnList = {"ID", "DB", "BitCount", "Flags", "Label", 
                "Intron", "Outron", "Text"};

            IList<IRow> rowList = IRowHelper.GetRowList(strList, columnList);
            foreach (IRow row in rowList)
            {
                TransformData(row);
            }

            return rowList;
        }

        /// <summary>
        /// RaimaQuery is unstable -- sometimes you need to specify 
        /// table within view
        /// 
        /// e.g.) pickPrefixRec.db
        /// </summary>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        IList<IRow> IPickReader.GetPrefixItem(string id, int version)
        {
            IRaimaQuery rq = new RaimaQuery();
            rq.Setup(_dbPath, _dbNameWithoutExt);
            id = LegacyParser.ToLegacyIDFormat(id);

            Array strList = rq.ExecuteQuery(
                "SELECT id, pickPrefixRec.db, pickPrefixRec.bitCnt, " +
                "       pickPrefixRec.comment " +
                "FROM   pickPrefix " +
                "WHERE id = '" + id + "' and version = " + version);

            string[] columnList = { "ID", "DB", "BitCount", "Text" };
            IList<IRow> rowList = IRowHelper.GetRowList(strList, columnList);
            foreach (IRow row in rowList)
            {
                TransformData(row);
            }

            return rowList;
        }

        /// <summary>
        /// convert the prefix list to List of int 
        /// </summary>
        /// <param name="PrefixList"></param>
        /// <returns></returns>
        List<int> IPickReader.ConvertPrefixList(IList<IRow> PrefixList)
        {
            List<int> iPrefixList = new List<int>();
            foreach (Dictionary<string, string> item in PrefixList)
            {
                if (item["DATA"] != null && item["DATA"] != "")
                    iPrefixList.Add(Convert.ToInt32(item["DATA"], 2));
            }
            return iPrefixList;
        }

        /// <summary>
        /// Assume there is at lease one record is returned
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int IPickReader.GetMaxVersion(string id)
        {
            IRaimaQuery rq = new RaimaQuery();
            rq.Setup(_dbPath, _dbNameWithoutExt);
            id = LegacyParser.ToLegacyIDFormat(id);

            Array strList = rq.ExecuteQuery(
                "SELECT max(version) FROM pickID " +
                "WHERE id = '" + id + "'");

            string version = IRowHelper.FromBase64Str(
                strList.GetValue(0).ToString());
            return Int32.Parse(version);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<string> IPickReader.GetAllIDList()
        {
            IRaimaQuery rq = new RaimaQuery();
            rq.Setup(_dbPath, _dbNameWithoutExt);
            
            Array strList = rq.ExecuteQuery(
                "SELECT DISTINCT id FROM pickRec WHERE id <> ''");

            IList<string> idList = new List<string>();
            foreach (string str in strList) {
                string id = IRowHelper.FromBase64Str(str);
                idList.Add(LegacyParser.FromLegacyIDFormat(id));
            }

            return idList;
        }

        Id.List IPickReader.GetAllIDList(IdFilterConditions idFilterConditions) {
            switch (idFilterConditions.FilterType) {
                case IdFilterConditions.FilterByType.Mode:
                    return GetAllIDList_Modes(idFilterConditions.FilterModes);
                case IdFilterConditions.FilterByType.Range:
                    return GetAllIDList_Range(idFilterConditions.FilterRange);
                case IdFilterConditions.FilterByType.File:
                    return idFilterConditions.FilterIdsFromFile;
                default:
                    IList<string> idList = ((IPickReader)this).GetAllIDList();
                    return new Id.List(idList);
            }
        }

        private Id.List GetAllIDList_Modes(List<char> modes) {
            IList<string> allIDList = ((IPickReader)this).GetAllIDList();

            Id.List idList = new Id.List();
            foreach (string id in allIDList) {
                foreach (char mode in modes) {
                    if (id.ToUpper()[0] == char.ToUpper(mode)) {
                        idList.Add(new Id(id));
                    }
                }
            }
            return idList;
        }

        private Id.List GetAllIDList_Range(IdFilterConditions.IdFilterRange filterRange) {
            IList<string> allIDList = ((IPickReader)this).GetAllIDList();
            
            Id.List idList = new Id.List();
            foreach (string id in allIDList) {
                Id pickId = new Id(id);
                if (filterRange.IsInRange(pickId)) {
                    idList.Add(pickId);
                }
            }
            return idList;
        }

        /////////////////////////////////////////////////////////////////////
        ////
        //// private
        ////

        const int INVFCN = 0x04;

        /// <summary>
        /// return "Y" or "N"
        /// </summary>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        string IsInversedPrefix(string id, int version)
        {
            IRaimaQuery rq = new RaimaQuery();
            rq.Setup(_dbPath, _dbNameWithoutExt);

            Array strList = rq.ExecuteQuery(
                "SELECT pickPrefixRec.flags FROM pickPrefix " +
                "WHERE id = '" + id + "' AND version = " + version);

            string[] columnList = {"Flags"};
            IList<IRow> rowList = IRowHelper.GetRowList(strList, columnList);
   
            if(rowList.Count > 0)
            {
                int flag = LegacyParser.StringFlagToInt(
                    rowList[0]["Flags"]);

                if ((flag & INVFCN) != 0)
                    return "Y";
            }
                
            return "N";
        }

        /// <summary>
        /// parsing
        /// 1. replace flags with IsInversedData, IsExternalPrefix, 
        ///    IsRestricted, IsFrequencyData
        /// 2. add IsInversedPrefix
        /// 3. time to datetime
        /// 4. id to fixed 4 digit format
        /// </summary>
        /// <param name="row"></param>
        /// <param name="isInversedPrefix"></param>
        /// <returns></returns>
        IDictionary<string, string> TransformID(
            IRow row, string isInversedPrefix)
        {
            string strFlags = row["Flags"];
            IRow flagRow = LegacyParser.ParseIDFlag(
                LegacyParser.StringFlagToInt(strFlags));

            row.Add("IsInversedData", flagRow["IsInversedData"]);
            row.Add("IsExternalPrefix", flagRow["IsExternalPrefix"]);
            row.Add("IsRestricted", flagRow["IsRestricted"]);
            row.Add("IsFrequencyData", flagRow["IsFrequencyData"]);
            row.Add("IsInversedPrefix", isInversedPrefix);

            uint unixtime = UInt32.Parse(row["Time"]);
            row["Time"] = LegacyParser.DateTimeFromUnixTime(unixtime);
            row["ID"] = LegacyParser.FromLegacyIDFormat(row["ID"]);
            row.Remove("Flags");

            return row;
        }

        /// <summary>
        /// db, bitCnt to string data
        /// e.g.) 128 to 10000000 
        /// </summary>
        /// <param name="row"></param>
        void TransformData(IRow row)
        {
            string data = LegacyParser.ParseData(
                row["DB"], row["BitCount"]);

            row.Add("DATA", data);
            row["ID"] = LegacyParser.FromLegacyIDFormat(row["ID"]);

            row.Remove("DB");
            row.Remove("BitCount");
        }
    }
}
