using System;
using System.Collections.Generic;
using System.Text;
using IRow = System.Collections.Generic.IDictionary<string, string>;

namespace UEI.Legacy
{
    static class IRowHelper
    {
        const string DELIMITER = "|";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strList"></param>
        /// <param name="columnList"></param>
        /// <returns></returns>
        public static IList<IRow> GetRowList(
            Array strList, IList<string> columnList)
        {
            List<IRow> rowList = new List<IRow>();

            foreach (string delimitedStr in strList)
            {
                string[] valueList = delimitedStr.Split(
                    DELIMITER.ToCharArray());

                rowList.Add(GetRow(valueList, columnList));
            }

            return rowList;
        }

        /// <summary>
        /// 
        /// order of two list assumed to be su=ynchronized
        /// 
        /// columnList: 'ID', 'Data'
        /// valueList:  'A0011', '00001111'
        /// 
        /// </summary>
        /// <param name="valueList"></param>
        /// <param name="columnList"></param>
        /// <returns></returns>
        public static IRow GetRow(IList<string> valueList, 
            IList<string> columnList)
        {
            if (valueList.Count != columnList.Count)
            {
                throw new ApplicationException(
                    "value and column count not matched");
            }

            IRow row = new Dictionary<string, string>();
            for (int i = 0; i < columnList.Count; i++)
            {
                row.Add(columnList[i], FromBase64Str(valueList[i]));
            }

            return row;
        }

        public static string FromBase64Str(string str)
        {
            byte[] byteArray = Convert.FromBase64String(str);
            ASCIIEncoding enc = new ASCIIEncoding();
            return enc.GetString(byteArray);
        }
    }
}
