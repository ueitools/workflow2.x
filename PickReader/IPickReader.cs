///
/// 
///
using System.Collections.Generic;
using ToolsData;
using IRow = System.Collections.Generic.IDictionary<string, string>;

namespace UEI.Legacy
{
    /// <summary>
    /// 
    /// IList<Row> rowList = GetPickItem('A0011', 0);
    /// foreach(Row row in rowList)
    /// {
    ///     string id = row["ID"];
    /// }
    /// 
    /// </summary>
    public interface IPickReader
    {
        /// <summary>
        /// 
        /// ID, sourceID, Executor_Code, Sequence,  
        /// IsInversedData, IsExternalPrefix, IsRestricted, 
        /// IsFrequencyData, IsInversedPrefix, IsHexFormat, 
        /// Time, Name, Text
        /// 
        /// 'A0011', 'A0013', 90, 128, 
        /// 'N', 'N', 'N', 'N', 'Y', 'N'
        /// '2006-02-24 10:09:00.000', 'bchoi', 'Test'
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        IRow GetPickID(string id, int version);

        /// <summary>
        /// 
        /// Picked item has non-empty data string (IsPicked flag is removed)
        /// 
        /// ID, Data, Label, Intron, Outron, Text
        /// 
        /// 'A0011', '00001111', 'Power', 'PW', 'POW', 'Test'
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        IList<IRow> GetPickItem(string id, int version);

        /// <summary>
        /// 
        /// ID, Data, Text
        ///
        /// 'A0011', '10001111', 'Test'
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        IList<IRow> GetPrefixItem(string id, int version);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PrefixList"></param>
        /// <returns></returns>
        List<int> ConvertPrefixList(IList<IRow> PrefixList);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        int GetMaxVersion(string id);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<string> GetAllIDList();
        Id.List GetAllIDList(IdFilterConditions idFilterConditions);
    }
}