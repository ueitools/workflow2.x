using System;
using System.Collections.Generic;
using System.Text;
using IRow = System.Collections.Generic.IDictionary<string, string>;

namespace UEI.Legacy
{
    static class LegacyParser
    {
        const int INVDATA = 0x01;
        const int EXTPREFIX = 0x02;
        const int RESTRICTED = 0x08;
        const int FREQDATA = 0x10;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flag"></param>
        /// <returns></returns>
        public static IRow ParseIDFlag(int flag)
        {
            IRow row = new Dictionary<string, string>();

            if ((flag & INVDATA) != 0)
                row.Add("IsInversedData", "Y");
            else
                row.Add("IsInversedData", "N");

            if ((flag & EXTPREFIX) != 0)
                row.Add("IsExternalPrefix", "Y");
            else
                row.Add("IsExternalPrefix", "N");

            if ((flag & RESTRICTED) != 0)
                row.Add("IsRestricted", "Y");
            else
                row.Add("IsRestricted", "N");

            if ((flag & FREQDATA) != 0)
                row.Add("IsFrequencyData", "Y");
            else
                row.Add("IsFrequencyData", "N");

            return row;
        }

        static DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0);
        const string DATETIME_FORMAT = "yyyy-MM-ddTHH:mm:ss";
        public static string DateTimeFromUnixTime(uint timeT)
        {
            DateTime dt = origin + new TimeSpan(
                timeT * TimeSpan.TicksPerSecond);

            return dt.ToString(DATETIME_FORMAT);
        }

        /// <summary>
        /// original value was char
        /// </summary>
        /// <param name="strFlag"></param>
        /// <returns></returns>
        public static int StringFlagToInt(string strFlag)
        {
            if (strFlag.Length > 0)
            {
                return (char)strFlag[0];
            }
            else
                return 0;
        }

        const char BINARYLPAD = '0';
        const int FREQUENCYDATA_WIDTH = 2;

        /// <summary>
        /// if width is empty return empty data
        /// 
        /// Frequency Data: 2980 --> 29.80
        /// Normal Data: 255, 16 --> 0000000011111111
        /// </summary>
        /// <param name="data"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        public static string ParseData(string db, string bitCnt)
        {
            if (bitCnt.Length == 0)
                return "";

            int data = Int32.Parse(db);
            int width = (char)bitCnt[0];

            if (width == FREQUENCYDATA_WIDTH)
            {
                double d = (double)data / (double)100;
                return d.ToString("##.00");
            }
            else
            {
                string str = Convert.ToString(data, 2);
                return str.PadLeft(width, BINARYLPAD);
            }
        }

        /// <summary>
        /// fixed 4 digit format to 
        /// variable length legacy format (3 or 4 digit)
        /// 
        /// if first digit after mode is 0 remove
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string ToLegacyIDFormat(string id)
        {
            if (id[1] == '0')
                id = id.Remove(1, 1);

            return id;
        }

        /// <summary>
        /// 3 digit to 4 digit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string FromLegacyIDFormat(string id)
        {
            if (id.Length == 4)
                id = id.Insert(1, "0");

            return id;
        }
    }
}
