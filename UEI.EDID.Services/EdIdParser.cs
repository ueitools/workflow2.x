﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UEI.EDID
{
    public class EDIDParser
    {
        private EDIDData EDIDObj = new EDIDData();
        public EDIDData ParseEDID(Byte[] EDID)
        {
            ValidateEDID(EDID);
            if (EDIDObj.IsEDIDValid())
            {
                Extract_0_19(EDID);
                Extract_128_255(EDID);
                return EDIDObj;
            }
            else
                return null;
        }
        void Extract_128_255(Byte[] EDID)
        {
            if (EDID.Length > 128)
                ExtractHDMIAddress(EDID);
        }
        private void ValidateEDID(Byte[] EDID)
        {
            // First Rule. Number of bytes should be greater than or equal to 128
            if (EDID.Length < 128)
            {
                EDIDObj.SetEDIDValidity(false);
                EDIDObj.SetReasonForValidity(1); // Number of bytes
            }
            else
            {
                //Checksum byte = (256-(S%256))%256
                // Where:  
                // S is the sum of the first 127 bytes
                // % is modulus operator

                // Check Check sum
                int SumOf127Bytes = 0;
                for (int i = 0; i < 127; i++)
                {
                    SumOf127Bytes += EDID[i];
                }
                int CalculatedCheckSum = 0xFF;
                CalculatedCheckSum = (256 - (SumOf127Bytes % 256)) % 256;
                EDIDObj.SetCalculatedCheckSum(CalculatedCheckSum);
                EDIDObj.SetCheckSumByteInEDID(EDID[127]);
                if (CalculatedCheckSum != EDID[127])
                {
                    EDIDObj.SetEDIDValidity(false);
                    EDIDObj.SetReasonForValidity(2); // Check sum error
                }
                else
                {
                    EDIDObj.SetEDIDValidity(true);
                }
            }
        }
        void Extract_0_19(Byte[] EDID)
        {
            // Extract Header bytes and Validate
            //00FFFFFFFFFFFF00
            string HeaderDefaultString = "00FFFFFFFFFFFF00";
            string HeaderString = "";
            for (int bi = 0; bi < 8; bi++)
            {
                if (EDID[bi] == 0)
                    HeaderString += "00";
                else
                    HeaderString += EDID[bi].ToString("X");
            }
            if (HeaderString == HeaderDefaultString)
            {
                EDIDObj.SetHeader(HeaderString);
            }
            else
            {
                EDIDObj.SetHeader("INVALID");
            }

            // Extract Manufacturer ID
            byte[] ManIDBytes = new byte[2];
            ManIDBytes[0] = EDID[8];
            ManIDBytes[1] = EDID[9];
            string ManIDStr = DecodeManufactureIDByte(ManIDBytes);
            EDIDObj.SetManufacturerID(ManIDStr);

            //Extract Manufacturer ID Hex Code
            StringBuilder mfgHexCode = new StringBuilder();
            if (ManIDBytes != null)
            {
                String hex = BitConverter.ToString(ManIDBytes);
                hex = hex.Replace("-", " ");
                mfgHexCode.Append(hex);
            }
            if (!String.IsNullOrEmpty(mfgHexCode.ToString()))
            {
                EDIDObj.SetManufacturerIDHexCode(mfgHexCode.ToString().ToUpper());
            }            

            // Extract Product ID, Little endian
            string ProductID = "Text(";
            if (EDID[11] == 0)
            {
                ProductID += "00";
            }
            else if (EDID[11] > 0 && EDID[11] <= 0x0F)
            {
                ProductID += "0" + EDID[11].ToString("X");
            }
            else
                ProductID += EDID[11].ToString("X");

            if (EDID[10] == 0)
            {
                ProductID += "00";
            }
            else if (EDID[10] > 0 && EDID[10] <= 0x0F)
            {
                ProductID += "0" + EDID[10].ToString("X");
            }
            else
                ProductID += EDID[10].ToString("X");
            ProductID += ")";

            EDIDObj.SetProductID(ProductID);

            // Serial Number
            bool NonZero_SerialNumber = false;
            string SerialNumber = ""; // little endian
            for (int si = 12; si <= 15; si++)
            {
                if (EDID[si] > 0)
                {
                    NonZero_SerialNumber = true;
                    break;
                }
            }
            UInt32 Int32SerialNumber = 0;
            bool NonAlphaNumeric = false;
            if (NonZero_SerialNumber)
            {
                /*for (int si = 15; si >= 12; si--)
                {
                    Char SNChar = Convert.ToChar(EDID[si]);
                    if (char.IsLetterOrDigit(SNChar))
                        SerialNumber += SNChar;
                    else
                    {
                        NonAlphaNumeric = true;
                        break;
                    }

                }*/
                UInt32 serialNumberByte = EDID[12];
                Int32SerialNumber |= serialNumberByte;
                serialNumberByte = EDID[13];
                serialNumberByte = (serialNumberByte << 8);
                Int32SerialNumber |= serialNumberByte;

                serialNumberByte = EDID[14];
                serialNumberByte = (serialNumberByte << 16);
                Int32SerialNumber |= serialNumberByte;

                serialNumberByte = EDID[15];
                serialNumberByte = (serialNumberByte << 24);
                Int32SerialNumber |= serialNumberByte;
                SerialNumber = Int32SerialNumber.ToString();
            }
            else
                SerialNumber = "0";

            //if (NonAlphaNumeric)
            //SerialNumber = "N/A";

            EDIDObj.SetSerialNumber(SerialNumber);
            EDIDObj.SetNonZeroSerialNumber(NonZero_SerialNumber);

            // Week of Manufactur
            EDIDObj.SetWeekOfManufactur(EDID[16]);

            // Year of Manufacture
            string strYearOFMan = "";
            strYearOFMan += (1990 + EDID[17]).ToString();
            EDIDObj.SetYearOfManufacture(strYearOFMan);

            // EDID Version
            string strEDIDVersion = "";
            EDIDObj.SetVersionNumber(EDID[18]);
            EDIDObj.SetRevisionNumber(EDID[19]);
            strEDIDVersion += EDID[18].ToString() + "." + EDID[19].ToString();
            EDIDObj.SetEDIDVersion(strEDIDVersion);
            if (EDID.Length < 20)
                return;
            // Bytes 20-24
            // Video Input definition
            // Bit 7 = 1 = Digital
            byte VideoDefinitionByte = EDID[20];
            byte VIBit = (byte)(VideoDefinitionByte & 0x80);

            if (VIBit > 0)
            {
                // Set Video Input Type
                EDIDObj.SetVideoInputType("DIGITAL");
                // Bits 6 to Bit 1 must be 0, else it is error
                byte ReservedBits = (byte)(VideoDefinitionByte & 0x7E);
                if (ReservedBits != 0)
                {
                    EDIDObj.SetVideoInputDefintionByte("INVALID");
                }
                else
                {
                    EDIDObj.SetVideoInputDefintionByte("VALID");
                }

                byte ColorBitDefinitionNibble = 0x00;
                string StrColorBitDefinition = "";
                ColorBitDefinitionNibble = (byte)(VideoDefinitionByte & 0xF0);
                EDIDObj.SetColorBitDepth(ColorBitDefinitionNibble);
                switch (ColorBitDefinitionNibble)
                {
                    case 0x80: // Color Bit Depth undefined
                        StrColorBitDefinition = "UnDefined";
                        break;
                    case 0x90: // 6 Bits per Primary Color
                        StrColorBitDefinition = "6 Bits per Primary Color";
                        break;
                    case 0xA0: // 8 bits per Primary Color
                        StrColorBitDefinition = "8 Bits per Primary Color";
                        break;
                    case 0xB0: // 10 bits per Primary Color
                        StrColorBitDefinition = "10 Bits per Primary Color";
                        break;
                    case 0xC0: // 12 bits per primary Color
                        StrColorBitDefinition = "12 Bits per Primary Color";
                        break;
                    case 0xD0: // 14 bits per primary color
                        StrColorBitDefinition = "14 Bits per Primary Color";
                        break;
                    case 0xE0: // 16 bits per primary color
                        StrColorBitDefinition = "16 Bits per Primary Color";
                        break;
                    case 0xF0: // Reserved
                        StrColorBitDefinition = "Reserved";
                        break;
                }
                EDIDObj.SetstrColorBitDefinitionString(StrColorBitDefinition);
                byte DVIStandardSupportNible = 0x00;
                string StrDVISupported = "";
                DVIStandardSupportNible = (byte)(VideoDefinitionByte & 0x0F);
                switch (DVIStandardSupportNible)
                {
                    case 0x00: // DVI Support Not defined
                        StrDVISupported = "DVI is not defined";
                        break;
                    case 0x01: // DVI is Supported
                        StrDVISupported = "DVI is supported";
                        break;
                    case 0x02: // HDMI-a is supported
                        StrDVISupported = "HDMI-a is supported";
                        break;
                    case 0x03: // HDMI-b is supported
                        StrDVISupported = "HDMI-b is supported";
                        break;
                    case 0x04: // MDDI is supported
                        StrDVISupported = "MDDI is supported";
                        break;
                    case 0x05: // Display Port is supported
                        StrDVISupported = "Display Port is supported";
                        break;
                    default:
                        StrDVISupported = "Reserved";
                        break;
                }
                EDIDObj.SetDVISupportString(StrDVISupported);
            }
            else
            {
                EDIDObj.SetVideoInputType("ANALOG");
                //Bits 6–5	Video white and sync levels, relative to blank: 

                // 01=+0.714/−0.286 V; 0x0010 0000
                // 10=+1.0/−0.4 V; 0x0100 0000
                // 11=+0.7/0 V; 0x0110 0000
                string strWhiteAndSyncLevel = "";
                byte WhiteAndSYncLevels = VideoDefinitionByte;

                switch (WhiteAndSYncLevels)
                {
                    case 0:// 00=+0.7/−0.3 V; 0x0000 0000
                        strWhiteAndSyncLevel = "+0.7/−0.3 V";
                        break;
                    case 0x20:
                        strWhiteAndSyncLevel = "+0.714/−0.286 V";
                        break;
                    case 0x40:
                        strWhiteAndSyncLevel = "+1.0/−0.4 V";
                        break;
                    case 0x60:
                        strWhiteAndSyncLevel = "+0.7/0 V";
                        break;
                }
                EDIDObj.SetWhiteAndSyncLevel(strWhiteAndSyncLevel);
                if ((VideoDefinitionByte & 0x10) > 0)
                {
                    EDIDObj.SetBlankToBlackSetup(true);
                }
                else
                    EDIDObj.SetBlankToBlackSetup(false);


            }

            if (EDID.Length < 21)
                return;
            // Byte 21-22
            int HorScreenSize = 0;
            int VerScreenSize = 0;
            double AspectRatioPortrait = 0.0;
            double AspectRatioLandscape = 0.0;
            string HorVerScreenSizeByte = "";

            if (strEDIDVersion == "1.4")
            {
                if (EDID[21] == 0 && EDID[22] == 0)
                {
                    HorVerScreenSizeByte = "Unknown";
                }
                else if (EDID[21] != 0 && EDID[22] == 0)
                {
                    AspectRatioLandscape = (double)((EDID[21] + 99) / 100);
                }
                else if (EDID[21] == 0 && EDID[22] != 0)
                {
                    AspectRatioPortrait = (double)(100 / (EDID[22] + 99));
                }
                else
                {
                    HorScreenSize = EDID[21];
                    VerScreenSize = EDID[22];
                }
            }
            else
            {
                string DisplayIsProjector = "";
                string MaxHorImageSize = EDID[21].ToString();// +" Cm";
                EDIDObj.SetMaxHorImageSize(MaxHorImageSize);
                if (EDID[21] == 0 && EDID[22] == 0)
                {
                    DisplayIsProjector = "Projector";
                }
                else
                    DisplayIsProjector = "Non-Projector";
                EDIDObj.SetDisplayIsProjector(DisplayIsProjector);

                string MaxVerImageSize = EDID[22].ToString();// +" Cm";
                EDIDObj.SetMaxVerImageSize(MaxVerImageSize);
            }
            // byte 23
            // Display Gamma, datavalue = (gamma*100)-100 (range 1.00–3.54)
            string DisplayGamma = ((EDID[23] + 100) / 100).ToString();
            EDIDObj.SetDisplayGamma(DisplayGamma);

            // Byte 24, DMPS
            // 72-89
            //Bit 7 DPMS standby supported, 0x80
            //Bit 6	DPMS suspend supported, 
            //Bit 5	DPMS active-off supported


            byte DPMSBits = 0;
            DPMSBits = (byte)(EDID[24] & 0xE0);

            string DMPSStandBy = "";
            string DPMSSuspend = "";
            string DPMSActiveOff = "";
            if ((DPMSBits & 0x80) == 0)
            {
                DMPSStandBy = "NO";
            }
            else
            {
                DMPSStandBy = "YES";
            }
            EDIDObj.SetDMPSStandBy(DMPSStandBy);
            if ((DPMSBits & 0x40) == 0)
            {
                DPMSSuspend = "NO";
            }
            else
            {
                DPMSSuspend = "YES";
            }
            EDIDObj.SetDPMSSuspend(DPMSSuspend);

            if ((DPMSBits & 0x20) == 0)
            {
                DPMSActiveOff = "NO";
            }
            else
            {
                DPMSActiveOff = "YES";
            }
            EDIDObj.SetDPMSActiveOff(DPMSActiveOff);

            if (strEDIDVersion == "1.4")
            {
                if ((VideoDefinitionByte & 0x80) > 0)
                {
                    byte ColorEncodingFormats = (byte)(EDID[24] & 0x18);
                    string strColorEncodingFormat = "";
                    switch (ColorEncodingFormats)
                    {
                        case 0x00:
                            // RGB 4:4:4
                            strColorEncodingFormat = "RGB 4:4:4";
                            break;
                        case 0x08:
                            // RGB 4:4:4 and YCrCb 4:4:4
                            strColorEncodingFormat = "RGB 4:4:4 and YCrCb 4:4:4";
                            break;
                        case 0x10:
                            // RGB 4:4:4 and YCrCb 4:2:2
                            strColorEncodingFormat = "RGB 4:4:4 and YCrCb 4:2:2";
                            break;
                        case 0x18:
                            // RGB 4:4:4 and YCrCb 4:4:4 and YCrCb 4:2:2
                            strColorEncodingFormat = "RGB 4:4:4 and YCrCb 4:4:4 and YCrCb 4:2:2";
                            break;
                    }
                    EDIDObj.SetColorEncodingFormats(ColorEncodingFormats);
                    EDIDObj.SetstrColorEncodingFormat(strColorEncodingFormat);
                }
                else
                {
                    string strDisplayColorType = "";
                    byte DisplayColorType = (byte)(EDID[24] & 0x18);

                    switch (DisplayColorType)
                    {
                        case 0x00:
                            // Monochrome or Grayscale display
                            strDisplayColorType = "Monochrome or Grayscale display";
                            break;
                        case 0x08:
                            // RGB Color Display
                            strDisplayColorType = "RGB Color Display";
                            break;
                        case 0x10:
                            // Non RGB Color Display
                            strDisplayColorType = "Non RGB Color Display";
                            break;
                        case 0x18:
                            // Display Color Type is Undefined
                            strDisplayColorType = "Display Color Type is Undefined";
                            break;
                    }
                    EDIDObj.SetDisplayColorType(DisplayColorType);
                    EDIDObj.SetstrDisplayColorType(strDisplayColorType);
                }

                byte sRGB = (byte)(EDID[24] & 0x04);
                bool sRGBDefault = false;
                string strSRGB = "";
                if (sRGB > 0)
                {
                    strSRGB = "sRGB standard is Default color space";
                    sRGBDefault = true;

                }
                else
                {
                    strSRGB = "sRGB standard is not Default color space";
                    sRGBDefault = false;
                }
                EDIDObj.SetsRGBDefault(sRGBDefault);

                byte PreferredTimingInfoByte = (byte)(EDID[24] & 0x02);
                bool NPFandRRInlcuded = false;
                if (PreferredTimingInfoByte > 0)
                {
                    NPFandRRInlcuded = true;
                }
                else
                {
                    NPFandRRInlcuded = false;
                }
                EDIDObj.SetNPFandRRInlcuded(NPFandRRInlcuded);

                byte DCF = (byte)(EDID[24] & 0x01);
                bool boolDCF = false;
                if (DCF > 0)
                {
                    boolDCF = true;
                }
                else
                {
                    boolDCF = false;
                }
                EDIDObj.SetDCF(boolDCF);
            }
            else
            {
                string strDisplayColorType = "";
                byte DisplayColorType = (byte)(VideoDefinitionByte & 0x18);

                switch (DisplayColorType)
                {
                    case 0x00:
                        // Monochrome or Grayscale display
                        strDisplayColorType = "Monochrome or Grayscale display";
                        break;
                    case 0x08:
                        // RGB Color Display
                        strDisplayColorType = "RGB Color Display";
                        break;
                    case 0x10:
                        // Non RGB Color Display
                        strDisplayColorType = "Non RGB Color Display";
                        break;
                    case 0x18:
                        // Display Color Type is Undefined
                        strDisplayColorType = "Display Color Type is Undefined";
                        break;
                }
                EDIDObj.SetDisplayColorType(DisplayColorType);
                EDIDObj.SetstrDisplayColorType(strDisplayColorType);

            }
            if (EDID.Length < 25)
                return;
            ExtractChromacityBytes(EDID);

            if (EDID.Length < 35)
                return;
            // bytes 35-37
            ExtractEstablishedTimingBitmap(EDID);

            string EstablishedTimingBitmap = "";
            for (int ti = 35; ti <= 37; ti++)
            {
                EstablishedTimingBitmap += ByteToHex(EDID[ti]);
            }
            EDIDObj.SetEstablishedTimingBitmap(EstablishedTimingBitmap);

            if (EDID.Length < 38)
                return;
            // bytes 38-53
            // standard display modes
            int identifier = 0;
            for (int hr = 38; hr <= 53; hr += 2)
            {
                int HorizontalRes = (EDID[hr] + 31) * 8;
                EDIDObj.SetHorizontalActivePixels(identifier++, HorizontalRes);
            }
            identifier = 0;
            for (int hr = 39; hr <= 53; hr += 2)
            {
                EDIDObj.SetImageAspectRatio(identifier++, GetAspectRatio(EDID[hr]));
            }
            identifier = 0;
            for (int hr = 39; hr <= 53; hr += 2)
            {
                EDIDObj.SetRefreshRate(identifier++, GetRefreshRate(EDID[hr]));
            }

            string StandardDisplayModes = "";
            for (int di = 38; di <= 53; di++)
            {
                StandardDisplayModes += ByteToHex(EDID[di]);
            }
            EDIDObj.SetStandardDisplayModes(StandardDisplayModes);


            byte PrefferedTimingBit = 0;
            PrefferedTimingBit = (byte)(EDID[24] & 0x02);

            double floatEDIDVersion = Convert.ToDouble(strEDIDVersion);
            if (floatEDIDVersion >= 1.3)
            {
                // preferred timing mode includes native pixel format and refresh rate
            }
            else
            {
                if (PrefferedTimingBit > 0)
                {
                    // Descriptor 1 always contains Preferred Timing
                }
            }

            if (EDID.Length < 54 || EDID.Length < 71)
                return;
            string Descriptor1IsPreferredTiming = "";
            string Descriptor1 = "";

            List<Byte> MonitorByteDescriptor1 = new List<Byte>();
            List<Byte> MonitorRangeLimitByteDescriptor1 = new List<Byte>();
            List<Byte> DetailedTimingByteDescriptor1 = new List<Byte>();

            string MonitorName = "";
            string Descriptor1_Type = "";
            string MonitorSerialNumber = "";
            string ASCIIText = "";
            // 54-71
            if (EDID[54] == 0 && EDID[55] == 0)
            {
                Descriptor1IsPreferredTiming = "NO";
                if (EDID[56] == 0)
                {
                    switch (EDID[56])
                    {
                        case 0xFC: 
                            MonitorName = ExtractMonitorName(EDID, 54);
                            MonitorByteDescriptor1 = ExtractMonitorByteData(EDID, 54);
                            break;
                        case 0xFF:
                            MonitorSerialNumber = ExtractMonitorSerialNumber(EDID, 54);
                            break;
                        case 0xFE:
                            ASCIIText = ExtractMonitorName(EDID, 54);
                            break;
                        case 0xFA:

                        case 0xFB:

                        case 0xFD:
                            MonitorRangeLimitByteDescriptor1 = ExtractMonitorRangeLimitByteData(EDID, 54);
                            for (int i = 54; i <= 71; i++)
                            {
                                Descriptor1 += ByteToHex(EDID[i]);                                
                            }
                                                        
                            break;
                    }
                    switch (EDID[56])
                    {
                        case 0xFC:
                            Descriptor1_Type = "Monitor Name";
                            break;
                        case 0xFA:
                            Descriptor1_Type = "Additional Timing Identifiers";
                            break;
                        case 0xFB:
                            Descriptor1_Type = "Additional white point data";
                            break;
                        case 0xFD:
                            Descriptor1_Type = "Monitor range limits";
                            break;
                        case 0xFE:
                            Descriptor1_Type = "Unspecified text";
                            break;
                        case 0xFF:
                            Descriptor1_Type = "Monitor serial number";
                            break;
                    }
                }
                EDIDObj.SetDescriptor1IsPreferredTiming(Descriptor1IsPreferredTiming);
                EDIDObj.SetDescriptor1_Type(Descriptor1_Type);
            }
            else
            {
                DecodeDetailedTimingDescriptor(EDID, 54);
                if (PrefferedTimingBit > 0)
                {
                    Descriptor1IsPreferredTiming = "YES";
                    for (int i = 54; i <= 71; i++)
                    {
                        DetailedTimingByteDescriptor1.Add(EDID[i]);
                        Descriptor1 += ByteToHex(EDID[i]);                        
                    }
                    EDIDObj.SetDescriptor1IsPreferredTiming(Descriptor1IsPreferredTiming);
                }
                else
                {
                    Descriptor1IsPreferredTiming = "NO";
                    EDIDObj.SetDescriptor1IsPreferredTiming(Descriptor1IsPreferredTiming);
                }
                Descriptor1_Type = "Detailed Timing";
            }

            EDIDObj.SetMonitorByteDescriptor1(MonitorByteDescriptor1);
            EDIDObj.SetMonitorRangeLimitByteDescriptor1(MonitorRangeLimitByteDescriptor1);
            EDIDObj.SetDetailedTimingByteDescriptor1(DetailedTimingByteDescriptor1);

            EDIDObj.SetDescriptor1(Descriptor1);
            EDIDObj.SetDescriptor1_Type(Descriptor1_Type);

            if (EDID.Length < 72 || EDID.Length < 89)
                return;

            List<Byte> MonitorByteDescriptor2 = new List<Byte>();
            List<Byte> MonitorRangeLimitByteDescriptor2 = new List<Byte>();
            List<Byte> DetailedTimingByteDescriptor2 = new List<Byte>();

            string Descriptor2 = "";
            string Descriptor2_Type = "";

            if (EDID[72] == 0 && EDID[73] == 0)
            {
                if (EDID[74] == 0)
                {
                    switch (EDID[75])
                    {
                        case 0xFC: 
                            MonitorName = ExtractMonitorName(EDID, 72);
                            MonitorByteDescriptor2 = ExtractMonitorByteData(EDID, 72);                           
                            break;
                        case 0xFF:
                            MonitorSerialNumber = ExtractMonitorSerialNumber(EDID, 72);
                            break;
                        case 0xFE:
                            ASCIIText = ExtractMonitorName(EDID, 72);
                            break;
                        case 0xFA:

                        case 0xFB:

                        case 0xFD:
                            MonitorRangeLimitByteDescriptor2 = ExtractMonitorRangeLimitByteData(EDID, 72);
                            for (int i = 72; i <= 89; i++)
                            {
                                Descriptor2 += ByteToHex(EDID[i]);                                
                            }                            
                            break;
                    }
                    if (EDID[75] >= 0x00 && EDID[75] <= 0x0F)
                        Descriptor2_Type = "Vendor Defined";
                    switch (EDID[75])
                    {
                        case 0xFC:
                            Descriptor2_Type = "Monitor Name";
                            break;
                        case 0xFA:
                            Descriptor2_Type = "Additional Timing Identifiers";
                            break;
                        case 0xFB:
                            Descriptor2_Type = "Additional white point data";
                            break;
                        case 0xFD:
                            Descriptor2_Type = "Monitor range limits";
                            break;
                        case 0xFE:
                            Descriptor2_Type = "Unspecified text";
                            break;
                        case 0xFF:
                            Descriptor2_Type = "Monitor serial number";
                            break;
                    }
                }
            }
            else
            {
                DecodeDetailedTimingDescriptor(EDID, 72);
                //Descriptor1IsPreferredTiming = "YES";
                for (int i = 72; i <= 89; i++)
                {
                    DetailedTimingByteDescriptor2.Add(EDID[i]);
                    Descriptor2 += ByteToHex(EDID[i]);                    
                }
                Descriptor2_Type = "Detailed Timing";
            }

            EDIDObj.SetMonitorByteDescriptor2(MonitorByteDescriptor2);
            EDIDObj.SetMonitorRangeLimitByteDescriptor2(MonitorRangeLimitByteDescriptor2);
            EDIDObj.SetDetailedTimingByteDescriptor2(DetailedTimingByteDescriptor2);

            EDIDObj.SetDescriptor2(Descriptor2);
            EDIDObj.SetDescriptor2_Type(Descriptor2_Type);


            List<Byte> MonitorByteDescriptor3 = new List<Byte>();
            List<Byte> MonitorRangeLimitByteDescriptor3 = new List<Byte>();
            List<Byte> DetailedTimingByteDescriptor3 = new List<Byte>();

            string Descriptor3 = "";
            string Descriptor3_Type = "";
            if (EDID.Length < 90 || EDID.Length < 107)
                return;
            // 90-107
            if (EDID[90] == 0 && EDID[91] == 0)
            {
                if (EDID[92] == 0)
                {
                    switch (EDID[93])
                    {
                        case 0xFC: 
                            MonitorName = ExtractMonitorName(EDID, 90);
                            MonitorByteDescriptor3 = ExtractMonitorByteData(EDID, 90);                           
                            break;
                        case 0xFF:
                            MonitorSerialNumber = ExtractMonitorSerialNumber(EDID, 90);
                            break;
                        case 0xFE:
                            ASCIIText = ExtractMonitorName(EDID, 90);
                            break;
                        case 0xFA:

                        case 0xFB:

                        case 0xFD:
                            MonitorRangeLimitByteDescriptor3 = ExtractMonitorRangeLimitByteData(EDID, 90);
                            for (int i = 90; i <= 107; i++)
                            {
                                Descriptor3 += ByteToHex(EDID[i]);                                
                            }                            
                            break;
                    }
                    if (EDID[93] >= 0x00 && EDID[93] <= 0x0F)
                        Descriptor2_Type = "Vendor Defined";
                    switch (EDID[93])
                    {
                        case 0xFC:
                            Descriptor3_Type = "Monitor Name";
                            break;
                        case 0xFA:
                            Descriptor3_Type = "Additional Timing Identifiers";
                            break;
                        case 0xFB:
                            Descriptor3_Type = "Additional white point data";
                            break;
                        case 0xFD:
                            Descriptor3_Type = "Monitor range limits";
                            break;
                        case 0xFE:
                            Descriptor3_Type = "Unspecified text";
                            break;
                        case 0xFF:
                            Descriptor3_Type = "Monitor serial number";
                            break;
                    }
                }
            }
            else
            {
                DecodeDetailedTimingDescriptor(EDID, 90);
                //Descriptor1IsPreferredTiming = "YES";
                for (int i = 90; i <= 107; i++)
                {
                    DetailedTimingByteDescriptor3.Add(EDID[i]);
                    Descriptor2 += ByteToHex(EDID[i]);                    
                }
                Descriptor3_Type = "Detailed Timing";
            }

            EDIDObj.SetMonitorByteDescriptor3(MonitorByteDescriptor3);
            EDIDObj.SetMonitorRangeLimitByteDescriptor3(MonitorRangeLimitByteDescriptor3);
            EDIDObj.SetDetailedTimingByteDescriptor3(DetailedTimingByteDescriptor3);

            EDIDObj.SetDescriptor3(Descriptor3);
            EDIDObj.SetDescriptor3_Type(Descriptor3_Type);

            if (EDID.Length < 108 || EDID.Length < 125)
                return;


            List<Byte> MonitorByteDescriptor4 = new List<Byte>();
            List<Byte> MonitorRangeLimitByteDescriptor4 = new List<Byte>();
            List<Byte> DetailedTimingByteDescriptor4 = new List<Byte>();

            string Descriptor4 = "";
            string Descriptor4_Type = "";
            // 108-125
            if (EDID[108] == 0 && EDID[109] == 0)
            {
                if (EDID[110] == 0)
                {
                    switch (EDID[111])
                    {
                        case 0xFC: 
                            MonitorName = ExtractMonitorName(EDID, 108);
                            MonitorByteDescriptor4 = ExtractMonitorByteData(EDID, 108);                          
                            break;
                        case 0xFF:
                            MonitorSerialNumber = ExtractMonitorSerialNumber(EDID, 108);
                            break;
                        case 0xFE:
                            ASCIIText = ExtractMonitorName(EDID, 108);
                            break;
                        case 0xFA:

                        case 0xFB:

                        case 0xFD:
                            MonitorRangeLimitByteDescriptor4 = ExtractMonitorRangeLimitByteData(EDID, 108);
                            for (int i = 108; i <= 125; i++)
                            {
                                Descriptor4 += ByteToHex(EDID[i]);                                
                            }                            
                            break;
                    }
                    if (EDID[111] >= 0x00 && EDID[111] <= 0x0F)
                        Descriptor2_Type = "Vendor Defined";
                    switch (EDID[111])
                    {
                        case 0xFC:
                            Descriptor4_Type = "Monitor Name";
                            break;
                        case 0xFA:
                            Descriptor4_Type = "Additional Timing Identifiers";
                            break;
                        case 0xFB:
                            Descriptor4_Type = "Additional white point data";
                            break;
                        case 0xFD:
                            Descriptor4_Type = "Monitor range limits";
                            break;
                        case 0xFE:
                            Descriptor4_Type = "Unspecified text";
                            break;
                        case 0xFF:
                            Descriptor4_Type = "Monitor serial number";
                            break;
                    }
                }
            }
            else
            {
                DecodeDetailedTimingDescriptor(EDID, 108);
                //Descriptor1IsPreferredTiming = "YES";
                for (int i = 108; i <= 125; i++)
                {
                    DetailedTimingByteDescriptor4.Add(EDID[i]);
                    Descriptor2 += ByteToHex(EDID[i]);                    
                }
                Descriptor4_Type = "Detailed Timing";
            }
            EDIDObj.SetMonitorName(MonitorName);
            EDIDObj.SetMonitorSerialNumber(MonitorSerialNumber);

            EDIDObj.SetMonitorByteDescriptor4(MonitorByteDescriptor4);
            EDIDObj.SetMonitorRangeLimitByteDescriptor4(MonitorRangeLimitByteDescriptor4);
            EDIDObj.SetDetailedTimingByteDescriptor4(DetailedTimingByteDescriptor4);

            EDIDObj.SetDescriptor4(Descriptor4);
            EDIDObj.SetDescriptor4_Type(Descriptor4_Type);
            EDIDObj.SetASCIIText(ASCIIText);
            if (EDID.Length < 126)
                return;
            // Number of Extensions
            // Byte 126
            EDIDObj.SetNumberOfExtensions(EDID[126]);

        }
        void ExtractHDMIAddress(Byte[] EDID)
        {
            List<Byte> physicaladdressBytes = new List<Byte>();
            //Check if Extension Block Exists
            //Extended data block must start with 0x02.
            //CEA extension version must be 0x03.
            if (EDID[0x7e] != 0)
            {
                EDIDObj.SetExtendedDataBlock(true);
            }
            if ((EDID[0x7e] != 0) && (EDID[0x80] == 0x02) && (EDID[0x81] == 0x03))
            {                
                for (int block_start = 0x84; block_start < 256; )
                {
                    int block_length = (EDID[block_start] & 0x1f) + 1;
                    switch ((EDID[block_start] >> 5) & 7)
                    {
                        case 3:	/* HDMI VSDB */
                            try
                            {
                                if ((EDID[block_start + 1] == 0x03) && (EDID[block_start + 2] == 0x0c) && (EDID[block_start + 3] == 0x00))
                                {
                                    /* HDMI IEEE Registration Identifier */
                                    String strPhysicalAddress = String.Format("{0}.{1}.{2}.{3}", (EDID[block_start + 4] >> 4) & 0xF,
                                    (EDID[block_start + 4] >> 0) & 0xF, (EDID[block_start + 5] >> 4) & 0xF, (EDID[block_start + 5] >> 0) & 0xF);
                                    EDIDObj.SetPhysicalAddress(strPhysicalAddress);
                                    physicaladdressBytes.Add(EDID[block_start + 4]);
                                    physicaladdressBytes.Add(EDID[block_start + 5]);
                                    EDIDObj.SetPhysicalAddressByteData(physicaladdressBytes);
                                    return;
                                }
                                else
                                {
                                    EDIDObj.SetPhysicalAddress(String.Empty);
                                    EDIDObj.SetPhysicalAddressByteData(physicaladdressBytes);
                                    return;
                                }
                            }
                            catch 
                            {
                                EDIDObj.SetPhysicalAddress(String.Empty);
                                EDIDObj.SetPhysicalAddressByteData(physicaladdressBytes);
                                return;
                            }
                            break;
                        default:
                            block_start += block_length;
                            break;
                    }
                }
            }            
        }        
        void ExtractChromacityBytes(byte[] EDID)
        {
            // bytes 25-34
            double Rx = 0, Ry = 0, Gx = 0, Gy = 0, Bx = 0, By = 0, Wx = 0, Wy = 0;

            byte maskValue = 0x80;

            for (byte bi = 0; bi < 8; bi++)
            {

                if ((EDID[27] & maskValue) > 0)
                {
                    Rx += 1 / Math.Pow(2, (bi + 1));
                }
                maskValue = (byte)(maskValue >> 1);
            }
            if ((EDID[25] & 0x80) > 0)
            {
                Rx += 1 / Math.Pow(2, 9);
            }
            if ((EDID[25] & 0x40) > 0)
            {
                Rx += 1 / Math.Pow(2, 10);
            }
            EDIDObj.SetRx(Rx);

            maskValue = 0x80;

            for (byte bi = 0; bi < 8; bi++)
            {

                if ((EDID[28] & maskValue) > 0)
                {
                    Ry += 1 / Math.Pow(2, (bi + 1));
                }
                maskValue = (byte)(maskValue >> 1);
            }
            if ((EDID[25] & 0x20) > 0)
            {
                Ry += 1 / Math.Pow(2, 9);
            }
            if ((EDID[25] & 0x10) > 0)
            {
                Ry += 1 / Math.Pow(2, 10);
            }
            EDIDObj.SetRy(Ry);

            maskValue = 0x80;

            for (byte bi = 0; bi < 8; bi++)
            {

                if ((EDID[29] & maskValue) > 0)
                {
                    Gx += 1 / Math.Pow(2, (bi + 1));
                }
                maskValue = (byte)(maskValue >> 1);
            }
            if ((EDID[25] & 0x08) > 0)
            {
                Gx += 1 / Math.Pow(2, 9);
            }
            if ((EDID[25] & 0x04) > 0)
            {
                Gx += 1 / Math.Pow(2, 10);
            }
            EDIDObj.SetGx(Gx);

            maskValue = 0x80;

            for (byte bi = 0; bi < 8; bi++)
            {

                if ((EDID[30] & maskValue) > 0)
                {
                    Gy += 1 / Math.Pow(2, (bi + 1));
                }
                maskValue = (byte)(maskValue >> 1);
            }
            if ((EDID[25] & 0x02) > 0)
            {
                Gy += 1 / Math.Pow(2, 9);
            }
            if ((EDID[25] & 0x01) > 0)
            {
                Gy += 1 / Math.Pow(2, 10);
            }
            EDIDObj.SetGy(Gy);
            maskValue = 0x80;

            for (byte bi = 0; bi < 8; bi++)
            {

                if ((EDID[31] & maskValue) > 0)
                {
                    Bx += 1 / Math.Pow(2, (bi + 1));
                }
                maskValue = (byte)(maskValue >> 1);
            }
            if ((EDID[26] & 0x80) > 0)
            {
                Bx += 1 / Math.Pow(2, 9);
            }
            if ((EDID[26] & 0x40) > 0)
            {
                Bx += 1 / Math.Pow(2, 10);
            }
            EDIDObj.SetBx(Bx);

            maskValue = 0x80;

            for (byte bi = 0; bi < 8; bi++)
            {

                if ((EDID[32] & maskValue) > 0)
                {
                    By += 1 / Math.Pow(2, (bi + 1));
                }
                maskValue = (byte)(maskValue >> 1);
            }
            if ((EDID[26] & 0x20) > 0)
            {
                By += 1 / Math.Pow(2, 9);
            }
            if ((EDID[26] & 0x10) > 0)
            {
                By += 1 / Math.Pow(2, 10);
            }
            EDIDObj.SetBy(By);

            maskValue = 0x80;

            for (byte bi = 0; bi < 8; bi++)
            {

                if ((EDID[33] & maskValue) > 0)
                {
                    Wx += 1 / Math.Pow(2, (bi + 1));
                }
                maskValue = (byte)(maskValue >> 1);
            }
            if ((EDID[26] & 0x08) > 0)
            {
                Wx += 1 / Math.Pow(2, 9);
            }
            if ((EDID[26] & 0x04) > 0)
            {
                Wx += 1 / Math.Pow(2, 10);
            }
            EDIDObj.SetWx(Wx);

            maskValue = 0x80;

            for (byte bi = 0; bi < 8; bi++)
            {

                if ((EDID[34] & maskValue) > 0)
                {
                    Wy += 1 / Math.Pow(2, (bi + 1));
                }
                maskValue = (byte)(maskValue >> 1);
            }
            if ((EDID[26] & 0x02) > 0)
            {
                Wy += 1 / Math.Pow(2, 9);
            }
            if ((EDID[26] & 0x01) > 0)
            {
                Wy += 1 / Math.Pow(2, 10);
            }
            EDIDObj.SetWy(Wy);

            //temp = (short)(temp << 2);
            //bRx = (short)((EDID[25] & 0xC0) >> 6) || temp;
            string ChromacityCoordinates = "";
            for (int ci = 25; ci <= 34; ci++)
            {
                ChromacityCoordinates += ByteToHex(EDID[ci]);
            }
            EDIDObj.SetChromacityCoordinates(ChromacityCoordinates);
        }
        void ExtractEstablishedTimingBitmap(byte[] EDID)
        {
            /*Bit 7	720×400 @ 70 Hz
            Bit 6	720×400 @ 88 Hz
            Bit 5	640×480 @ 60 Hz
            Bit 4	640×480 @ 67 Hz
            Bit 3	640×480 @ 72 Hz
            Bit 2	640×480 @ 75 Hz
            Bit 1	800×600 @ 56 Hz
            Bit 0	800×600 @ 60 Hz
            36	Bit 7	800×600 @ 72 Hz
            Bit 6	800×600 @ 75 Hz
            Bit 5	832×624 @ 75 Hz
            Bit 4	1024×768 @ 87 Hz, interlaced (1024×768i)
            Bit 3	1024×768 @ 60 Hz
            Bit 2	1024×768 @ 72 Hz
            Bit 1	1024×768 @ 75 Hz
            Bit 0	1280×1024 @ 75 Hz
             * 
            37	Bit 7	1152x870 @ 75 Hz (Apple Macintosh II)
            Bits 6–0	Other manufacturer-specific display modes*/

            if ((EDID[35] & 0x80) > 0)
            {
                EDIDObj.Set_Timing_720_400_70('Y');
            }
            else
                EDIDObj.Set_Timing_720_400_70('N');

            if ((EDID[35] & 0x40) > 0)
            {
                EDIDObj.Set_Timing_720_400_88('Y');
            }
            else
            {
                EDIDObj.Set_Timing_720_400_88('N');
            }

            if ((EDID[35] & 0x20) > 0)
            {

                EDIDObj.Set_Timing_640_480_60('Y');
            }
            else
            {
                EDIDObj.Set_Timing_640_480_60('N');
            }

            if ((EDID[35] & 0x10) > 0)
            {
                EDIDObj.Set_Timing_640_480_67('Y');
            }
            else
            {
                EDIDObj.Set_Timing_640_480_67('N');
            }

            if ((EDID[35] & 0x08) > 0)
            {
                EDIDObj.Set_Timing_640_480_72('Y');
            }
            else
            {
                EDIDObj.Set_Timing_640_480_72('N');
            }

            if ((EDID[35] & 0x04) > 0)
            {
                EDIDObj.Set_Timing_640_480_75('Y');
            }
            else
            {
                EDIDObj.Set_Timing_640_480_75('N');
            }

            if ((EDID[35] & 0x02) > 0)
            {
                EDIDObj.Set_Timing_800_600_56('Y');
            }
            else
            {
                EDIDObj.Set_Timing_800_600_56('N');
            }

            if ((EDID[35] & 0x01) > 0)
            {
                EDIDObj.Set_Timing_800_600_60('Y');
            }
            else
            {
                EDIDObj.Set_Timing_800_600_60('N');
            }

            if ((EDID[36] & 0x80) > 0)
            {
                EDIDObj.Set_Timing_800_600_60('Y');
            }
            else
            {
                EDIDObj.Set_Timing_800_600_60('N');
            }

            if ((EDID[36] & 0x40) > 0)
            {
                EDIDObj.Set_Timing_800_600_75Hz('Y');
            }
            else
            {
                EDIDObj.Set_Timing_800_600_75Hz('N');
            }

            if ((EDID[36] & 0x20) > 0)
            {
                EDIDObj.Set_Timing_832_624_75Hz('Y');
            }
            else
            {
                EDIDObj.Set_Timing_832_624_75Hz('N');
            }

            if ((EDID[36] & 0x10) > 0)
            {
                EDIDObj.Set_Timing_1024_768_87Hz('Y');
            }
            else
            {
                EDIDObj.Set_Timing_1024_768_87Hz('N');
            }

            if ((EDID[36] & 0x08) > 0)
            {
                EDIDObj.Set_Timing_1024_768_60Hz('Y');
            }
            else
            {
                EDIDObj.Set_Timing_1024_768_60Hz('N');
            }

            if ((EDID[36] & 0x40) > 0)
            {
                EDIDObj.Set_Timing_1024_768_72Hz('Y');
            }
            else
            {
                EDIDObj.Set_Timing_1024_768_72Hz('N');
            }

            if ((EDID[36] & 0x20) > 0)
            {
                EDIDObj.Set_Timing_1024_768_75Hz('Y');
            }
            else
            {
                EDIDObj.Set_Timing_1024_768_75Hz('N');
            }

            if ((EDID[36] & 0x01) > 0)
            {
                EDIDObj.Set_Timing_1280_1024_75Hz('Y');
            }
            else
            {
                EDIDObj.Set_Timing_1280_1024_75Hz('N');
            }

            if ((EDID[37] & 0x80) > 0)
            {
                EDIDObj.Set_Timing_1280_1024_75Hz('Y');
            }
            else
            {
                EDIDObj.Set_Timing_1280_1024_75Hz('N');
            }
        }
        string GetAspectRatio(byte ARByte)
        {
            byte ARVal = (byte)(ARByte & 0xC0);
            ARVal = (byte)(ARVal >> 6);
            switch (ARVal)
            {
                case 0: return "16:10";
                case 1:
                    return "4:3";
                case 2:
                    return "5:4";
                case 3:
                    return "16:9";
            }
            return "";
        }
        int GetRefreshRate(byte RRByte)
        {
            int refreshRate = RRByte & 0x3F;
            return refreshRate;
        }
        string ExtractMonitorName(byte[] EDID, int byteNo)
        {
            string MonitorName = "";
            int bi = byteNo + 5;
            for (int i = bi; i < bi + 14; i++)
            {
                if (EDID[i] == 0x0A)
                    break;
                else
                {
                    MonitorName += Convert.ToChar(EDID[i]);
                }
            }
            return MonitorName;
        }
        List<Byte> ExtractMonitorByteData(byte[] EDID, int byteNo)
        {
            List<Byte> byteData = new List<Byte>();
            int bi = byteNo + 5;
            if ((EDID[byteNo] == 0x00) && (EDID[byteNo + 1] == 0x00) && (EDID[byteNo + 2] == 0x00) && (EDID[byteNo + 3] == 0xFC) && (EDID[byteNo+4] == 0x00))
            {
                for (int i = bi; i < bi + 13; i++)
                {
                    if (EDID[i] == '\n')
                        break;
                    else
                    {
                        byteData.Add(EDID[i]);
                    }
                }
            }
            //for (int i = bi; i < bi + 13; i++)
            //{
            //    if (EDID[i] == 0x0A)                
            //        break;
            //    else
            //    {
            //        byteData.Add(EDID[i]);
            //    }
            //}
            return byteData;
        }
        List<Byte> ExtractMonitorRangeLimitByteData(byte[] EDID, int byteNo)
        {
            List<Byte> byteData = new List<Byte>();            
            int bi = byteNo + 5;           
            try
            {
                if ((EDID[18] == 0x01) && (EDID[19] == 0x04))
                {
                    if ((EDID[byteNo] == 0x00) && (EDID[byteNo + 1] == 0x00) && (EDID[byteNo + 2] == 0x00) && (EDID[byteNo + 3] == 0xFD) && ((EDID[byteNo + 4] == 0x00) || (EDID[byteNo + 4] == 0x02) || (EDID[byteNo + 4] == 0x03) || (EDID[byteNo + 4] == 0x08) || (EDID[byteNo + 4] == 0x0A) || (EDID[byteNo + 4] == 0x0B) || (EDID[byteNo + 4] == 0x0C) || (EDID[byteNo + 4] == 0x0E) || (EDID[byteNo + 4] == 0x0F)))
                    {
                        if ((EDID[byteNo + 10] == 2) || (EDID[byteNo + 10] == 4))
                        {
                            for (int i = bi; i < bi + 13; i++)
                            {
                                byteData.Add(EDID[i]);
                            }
                        }
                        else
                        {
                            for (int i = bi; i < bi + 6; i++)
                            {
                                byteData.Add(EDID[i]);
                            }
                        }
                    }
                }
                else
                {
                    if ((EDID[byteNo] == 0x00) && (EDID[byteNo + 1] == 0x00) && (EDID[byteNo + 2] == 0x00) && (EDID[byteNo + 3] == 0xFD) && (EDID[byteNo + 4] == 0x00))
                    {
                        if (EDID[byteNo + 10] == 0x02)
                        {
                            for (int i = bi; i < bi + 13; i++)
                            {
                                byteData.Add(EDID[i]);
                            }
                        }
                        else
                        {
                            for (int i = bi; i < bi + 6; i++)
                            {
                                byteData.Add(EDID[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                String s = ex.Message;
            }
            return byteData;
        }
        string ExtractMonitorSerialNumber(byte[] EDID, int byteNo)
        {
            string MonitorSeriaNumber = "";
            int bi = byteNo + 5;
            for (int i = bi; i < bi + 14; i++)
            {
                if (EDID[i] == 0x0A)
                    break;
                else
                {
                    MonitorSeriaNumber += Convert.ToChar(EDID[i]);
                }
            }
            return MonitorSeriaNumber;
        }
        void Extract_20_24(Byte[] EDID)
        {
        }
        void Extract_25_34(Byte[] EDID)
        {
        }
        void Extract_35_37(Byte[] EDID)
        {
        }
        void Extract_38_53(Byte[] EDID)
        {
        }
        private string ByteToHex(byte b)
        {
            string HexString = "";
            if (b == 0)
            {
                HexString += "00";
            }
            else if (b > 0 && b <= 0x0F)
            {
                HexString += "0" + b.ToString("X");
            }
            else
                HexString += b.ToString("X");
            return HexString;
        }
        private char GetChar(byte b)
        {
            switch (b)
            {
                case 1:
                    return 'A';
                case 2:
                    return 'B';
                case 3:
                    return 'C';
                case 4:
                    return 'D';
                case 5:
                    return 'E';
                case 6:
                    return 'F';
                case 7:
                    return 'G';
                case 8:
                    return 'H';
                case 9:
                    return 'I';
                case 10:
                    return 'J';
                case 11:
                    return 'K';
                case 12:
                    return 'L';
                case 13:
                    return 'M';
                case 14:
                    return 'N';
                case 15:
                    return 'O';
                case 16:
                    return 'P';
                case 17:
                    return 'Q';
                case 18:
                    return 'R';
                case 19:
                    return 'S';
                case 20:
                    return 'T';
                case 21:
                    return 'U';
                case 22:
                    return 'V';
                case 23:
                    return 'W';
                case 24:
                    return 'X';
                case 25:
                    return 'Y';
                case 26:
                    return 'Z';

            }
            return '1';
        }
        private void DecodeDetailedTimingDescriptor(byte[] EDID, byte StartIndex)
        {

            UInt16 pixelClock = 0;
            UInt16 HorizontalActivePixels = 0;
            UInt16 HorizontalBlankingPixels = 0;
            UInt16 VerticalActivePixels = 0;
            UInt16 VerticalBlankingPixels = 0;
            UInt16 HorizontalSyncOffset = 0;
            UInt16 HorizontalSyncPulseWidth = 0;
            UInt16 VerticalSyncOffset = 0;
            UInt16 VerticalSyncPulseWidth = 0;
            UInt16 HorizontalDisplaySize = 0;
            UInt16 VerticalDisplaySize = 0;
            byte HorizontalBorderPixels = 0;
            byte VerticalBorderPixels = 0;
            bool Interlaced = false;
            byte SteroModeBits = 0;


            pixelClock = EDID[StartIndex];
            pixelClock |= (byte)(EDID[StartIndex + 1] << 8);
            EDIDObj.SetpixelClock(pixelClock);

            UInt16 HAP_Msb = 0;
            HorizontalActivePixels |= (byte)(EDID[StartIndex + 2]);
            HAP_Msb = (UInt16)(EDID[StartIndex + 4] & 0xF0);
            HAP_Msb = (UInt16)(HAP_Msb << 4);
            HorizontalActivePixels |= HAP_Msb;
            EDIDObj.SetHorizontalActivePixelsOfDescriptor(HorizontalActivePixels);

            UInt16 HBP_Msb = 0;
            HorizontalBlankingPixels |= (byte)(EDID[StartIndex + 3]);
            HBP_Msb = (UInt16)(EDID[StartIndex + 4] & 0x0F);
            HBP_Msb = (UInt16)(HBP_Msb << 8);
            HorizontalBlankingPixels |= HBP_Msb;
            EDIDObj.SetHorizontalBlankingPixels(HorizontalBlankingPixels);

            UInt16 VAP_Msb = 0;
            VerticalActivePixels |= (byte)(EDID[StartIndex + 5]);
            VAP_Msb = (UInt16)(EDID[StartIndex + 7] & 0xF0);
            VAP_Msb = (UInt16)(VAP_Msb << 4);
            VerticalActivePixels |= VAP_Msb;
            EDIDObj.SetVerticalActivePixels(VerticalActivePixels);

            UInt16 VBP_Msb = 0;
            VerticalBlankingPixels |= (byte)(EDID[StartIndex + 6]);
            VBP_Msb = (UInt16)(EDID[StartIndex + 7] & 0x0F);
            VBP_Msb = (UInt16)(VBP_Msb << 8);
            VerticalBlankingPixels |= VBP_Msb;
            EDIDObj.SetVerticalBlankingPixels(VerticalBlankingPixels);

            HorizontalSyncOffset |= (byte)(EDID[StartIndex + 8]);
            UInt16 HSO_msb = 0;
            HSO_msb |= (UInt16)((EDID[StartIndex + 11]) & 0xC0);
            HSO_msb = (UInt16)(HSO_msb << 2);
            HorizontalSyncOffset |= HSO_msb;
            EDIDObj.SetHorizontalSyncOffset(HorizontalSyncOffset);

            HorizontalSyncPulseWidth |= (byte)(EDID[StartIndex + 9]);
            UInt16 HSP_msb = 0;
            HSP_msb |= (UInt16)((EDID[StartIndex + 11]) & 0x30);
            HSP_msb = (UInt16)(HSO_msb << 4);
            HorizontalSyncPulseWidth |= HSP_msb;
            EDIDObj.SetHorizontalSyncPulseWidth(HorizontalSyncPulseWidth);


            VerticalSyncOffset |= (UInt16)((EDID[StartIndex + 10] & 0xF0) >> 4);
            UInt16 VSO_msb = 0;
            VSO_msb |= (UInt16)(((EDID[StartIndex + 11]) & 0x0C) << 2);
            VerticalSyncOffset |= VSO_msb;
            EDIDObj.SetVerticalSyncOffset(VerticalSyncOffset);

            VerticalSyncPulseWidth |= (UInt16)(EDID[StartIndex + 10] & 0x0F);
            UInt16 VSP_msb = 0;
            VSP_msb |= (UInt16)(((EDID[StartIndex + 11]) & 0x03) << 4);
            VerticalSyncPulseWidth |= VSP_msb;
            EDIDObj.SetVerticalSyncPulseWidth(VerticalSyncPulseWidth);

            UInt16 HDS_Msb = 0;
            HorizontalDisplaySize |= (byte)(EDID[StartIndex + 12]);
            HDS_Msb = (UInt16)(EDID[StartIndex + 14] & 0xF0);
            HDS_Msb = (UInt16)(HAP_Msb << 4);
            HorizontalDisplaySize |= HDS_Msb;
            EDIDObj.SetHorizontalDisplaySize(HorizontalDisplaySize);

            UInt16 VDS_Msb = 0;
            VerticalDisplaySize |= (byte)(EDID[StartIndex + 13]);
            VDS_Msb = (UInt16)(EDID[StartIndex + 14] & 0x0F);
            VDS_Msb = (UInt16)(HAP_Msb << 8);
            VerticalDisplaySize |= VDS_Msb;
            EDIDObj.SetVerticalDisplaySize(VerticalDisplaySize);

            HorizontalBorderPixels = (EDID[StartIndex + 15]);
            EDIDObj.SetHorizontalBorderPixels(HorizontalBorderPixels);

            VerticalBorderPixels = (EDID[StartIndex + 16]);
            EDIDObj.SetVerticalBorderPixels(VerticalBorderPixels);
            // Flags

            if ((EDID[StartIndex + 17] & 0x80) > 0)
            {
                Interlaced = true;
            }
            else
                Interlaced = false;
            EDIDObj.SetInterlaced(Interlaced);

            byte Bit0 = (byte)(EDID[StartIndex + 17] & 0x01);
            SteroModeBits = (byte)(SteroModeBits | Bit0);
            byte Bit65 = (byte)(EDID[StartIndex + 17] & 0x30);
            Bit65 = (byte)(Bit65 >> 4);
            SteroModeBits |= Bit0;
            SteroModeBits |= Bit65;
            EDIDObj.SetSteroModeBits(SteroModeBits);

        }
        private string DecodeManufactureIDByte(byte[] ManInfo)
        {
            byte FirstByte = ManInfo[0];
            byte secondByte = ManInfo[1];
            // Bit 7 of first byte must be 0 always
            // first character is in bits 6 - 2, 0x01111100 - 7C
            byte SeventBit = (byte)(FirstByte & 0x80);
            if (SeventBit > 0)
            {
                return "INV";
            }
            else
            {
                string ManfIDString = "";
                byte FirstCharBitMask = 0x7C;
                byte DecodedFirstChar = (byte)(FirstByte & FirstCharBitMask);
                DecodedFirstChar = (byte)(DecodedFirstChar >> 2);
                ManfIDString += GetChar(DecodedFirstChar);

                byte secondCharBits = (Byte)(FirstByte & 0x03); // bit 1 and bit 0
                secondCharBits = (byte)(secondCharBits << 3);

                byte temp;
                temp = (byte)(secondByte & 0xE0);
                temp = (byte)(temp >> 5);
                secondCharBits = (byte)(secondCharBits | temp);
                ManfIDString += GetChar(secondCharBits);

                byte ThirdCharBits = (byte)(secondByte & 0x1F);
                ManfIDString += GetChar(ThirdCharBits);

                return ManfIDString;
            }
        }
    }
}
