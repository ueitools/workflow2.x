﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace UEI.EDID
{
    public class ErrorLogging
    {
        private static String logFile               = String.Empty;
        private static String errorlogFile          = String.Empty;        
        private const String _UEITOOLSROOT          = "HKEY_CURRENT_USER\\Software\\UEITools\\";
        private const String _WORKINGDIR            = "WorkingDirectory";
        private const String _DEFAULT_WORKINGDIR    = "C:\\Working";

        static ErrorLogging()
        {
            logFile = GetSettings("Log");
            errorlogFile = GetSettings("ErrorLog");           
            //Check if the Log file Directory is exist
            //if not create
            IsDirectoryPresent(StripDirectoryName(logFile), true);

            //Check if the Error Log file Directory is exist
            //if not create
            IsDirectoryPresent(StripDirectoryName(errorlogFile), true);
        }
        private static String GetfilefromPath(String path)
        {
            try
            {
                String formattedDate = String.Empty;
                String file = String.Empty;
                Int32 indexOfPeriod;
                formattedDate = "(" + DateTime.Now.ToString("dd - MM - yyyy") + ")";
                indexOfPeriod = path.LastIndexOf(".");
                file = path.Insert(indexOfPeriod, formattedDate);
                return file;
            }
            catch (Exception ex)
            {
                //Write To Error Log
                WriteToErrorLogFile(ex);
                return "";
            }
            finally
            {
            }
        }
        private static String GetSettings(String val)
        {
            try
            {
                String path = String.Empty;
                switch (val)
                {
                    case "Log":
                        path = GetfilefromPath((String)Microsoft.Win32.Registry.GetValue(
                               _UEITOOLSROOT, _WORKINGDIR, _DEFAULT_WORKINGDIR) + "\\EDID\\Log.txt");
                        break;
                    case "ErrorLog":
                        path = GetfilefromPath((String)Microsoft.Win32.Registry.GetValue(
                               _UEITOOLSROOT, _WORKINGDIR, _DEFAULT_WORKINGDIR) + "\\EDID\\ErrorLog.txt");
                        break;                    
                }
                return path;
            }
            catch (Exception ex)
            {
                //Write To Error Log
                WriteToErrorLogFile(ex);
                return "";
            }
            finally
            {
            }
        }

        #region Write To File
        public static void WriteToLogFile(String message, Boolean tm)
        {
            String _logFileName = String.Empty;
            if (IsDirectoryPresent(StripDirectoryName(logFile), true))
            {
                FileStream fs = null;
                StreamWriter sw = null;
                try
                {
                    fs = new FileStream(logFile, FileMode.Append, FileAccess.Write);
                    sw = new StreamWriter(fs);
                    if (tm == true)
                    {
                        //sw.WriteLine("===================================================");
                        sw.WriteLine(message + "\t" + DateTime.Now.ToString());
                        //sw.WriteLine("===================================================");
                    }
                    else
                    {
                        // sw.WriteLine("===================================================");
                        sw.WriteLine(message);
                        //sw.WriteLine("===================================================");
                    }
                    //sw.WriteLine("");
                }
                catch (Exception ex)
                {
                    WriteToErrorLogFile(ex);
                }
                finally
                {
                    if (sw != null)
                    {
                        sw.Close();
                    }
                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }
        }

        public static void WriteToErrorLogFile(Exception sourceException)
        {
            if (IsDirectoryPresent(StripDirectoryName(errorlogFile), true))
            {
                FileStream fs = null;
                StreamWriter sw = null;
                try
                {
                    fs = new FileStream(errorlogFile, FileMode.Append, FileAccess.Write);
                    sw = new StreamWriter(fs);
                    sw.WriteLine("==================================================================");
                    sw.WriteLine("ERROR OCCOURED IN:" + sourceException.Source);
                    sw.WriteLine("ERROR DESCRPTION:" + sourceException.Message);
                    sw.WriteLine("ERROR DESCRPTION:" + sourceException.StackTrace);
                    sw.WriteLine("ERROR DATE TIME:" + System.DateTime.Now.ToString());
                    sw.WriteLine("==================================================================");
                    sw.WriteLine("");
                }
                finally
                {
                    if (sw != null)
                    {
                        sw.Close();
                    }

                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }
        }
        public static void WriteToErrorLogFile(Exception sourceException, BusinessObject.IDSearchParameter param)
        {
            if (IsDirectoryPresent(StripDirectoryName(errorlogFile), true))
            {
                FileStream fs = null;
                StreamWriter sw = null;
                try
                {
                    fs = new FileStream(errorlogFile, FileMode.Append, FileAccess.Write);
                    sw = new StreamWriter(fs);
                    sw.WriteLine("==================================================================");
                    sw.WriteLine("PARAMETERS:-");
                    sw.WriteLine("ID :" + param.Ids);
                    sw.WriteLine("REGION :" + param.Regions);
                    sw.WriteLine("SUB REGION :" + param.SubRegions);
                    sw.WriteLine("COUNTRY :" + param.Countries);
                    sw.WriteLine("LOCATION XML:" + param.XMLLocations);
                    sw.WriteLine("DATASOURCE :" + param.DataSources);
                    sw.WriteLine("DEVICETYPES :" + param.DeviceTypes);
                    sw.WriteLine("SUBDEVICETYPES :" + param.SubDeviceTypes);
                    sw.WriteLine("COMPONENTS :" + param.DeviceComponents);
                    sw.WriteLine("DEVICE XML :" + param.XMLDevices);
                    sw.WriteLine("ERROR OCCOURED IN:" + sourceException.Source);
                    sw.WriteLine("ERROR DESCRPTION:" + sourceException.Message);
                    sw.WriteLine("ERROR DESCRPTION:" + sourceException.StackTrace);
                    sw.WriteLine("ERROR DATE TIME:" + System.DateTime.Now.ToString());
                    sw.WriteLine("==================================================================");
                    sw.WriteLine("");
                }
                finally
                {
                    if (sw != null)
                    {
                        sw.Close();
                    }

                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }
        }
        public static Boolean IsDirectoryPresent(String directory, Boolean create)
        {
            try
            {
                if (!Directory.Exists(directory))
                {
                    if (create == true)
                    {
                        Directory.CreateDirectory(directory);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                //Write to Error Log
                WriteToErrorLogFile(ex);
                return false;
            }
            finally
            {
            }
        }
        public static String StripDirectoryName(String path)
        {
            String direcoryPath = @"";
            int indexOfLastSlash = 0;

            try
            {
                indexOfLastSlash = path.LastIndexOf(@"\");
                direcoryPath = path.Substring(0, indexOfLastSlash);
                return direcoryPath;
            }
            catch (Exception ex)
            {
                //Write to Error Log file
                WriteToErrorLogFile(ex);
                return "";
            }
            finally
            {
            }
        }
        #endregion
    }
}