﻿using BusinessObject;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Text;
using UEI.EDID.DataAccess;
using UEI.Workflow2010.Report.DataAccess;

namespace UEI.EDID
{    
    public class EdIdServices
    {
        #region Variables
        private EDIDDataAccess m_DBAccess = null;
        private DBOperations objDb = null;
        #endregion     
   
        #region Properties
        public String ErrorMessage { get; set; }
        private BrandTranslationCollection m_BrandTranslation = null;
        public BrandTranslationCollection BrandTranslation
        {
            get { return m_BrandTranslation; }
            set { m_BrandTranslation = value; }
        }
        
        #endregion

        #region Constrcutor
        public EdIdServices()
        {
            m_DBAccess = new EDIDDataAccess();
        }
        public EdIdServices(String sqlconnectionString)
        {            
            objDb = new DBOperations(sqlconnectionString);
        }
        #endregion

        #region Methods from EDID(Direct Captured EDID + Telemetry EDID)

        #region Get all Devices
        public List<String> GetAllXBoxConsoleDevices(List<String> selectedDataSources)
        {
            this.ErrorMessage = String.Empty;
            List<String> l_DeviceTypes = null;
            try
            {
                String strDataSources = String.Empty;
                if(selectedDataSources != null)
                    foreach (String item in selectedDataSources)
	                {
                        strDataSources += item + ",";
	                }
                if(strDataSources.Length>0)
                    strDataSources = strDataSources.Remove(strDataSources.Length-1,1);

                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                //objCmd.CommandText = @"SELECT DISTINCT * FROM dbo.ConsoleDeviceClasses";
                objCmd.CommandText = "dbo.EDID_Sp_GetDeviceTypes";
                objCmd.Parameters.Add(objDb.CreateParameter("@vcharDataSources", DbType.String, ParameterDirection.Input, strDataSources.Length, strDataSources));
                DataTable dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult != null)
                {
                    foreach (DataRow item in dtResult.Rows)
                    {
                        if (l_DeviceTypes == null)
                            l_DeviceTypes = new List<String>();
                        if (!String.IsNullOrEmpty(item["MainDevice"].ToString()))
                        {
                            if (!l_DeviceTypes.Contains(item["MainDevice"].ToString()))
                            l_DeviceTypes.Add(item["MainDevice"].ToString());
                        }
                    }
                    if (l_DeviceTypes.Count > 0)
                        l_DeviceTypes.Sort();
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return l_DeviceTypes;
        }
        #endregion

        #region Get all XBox User entered Brands
        public List<String> GetAllXBoxConsoleBrands()
        {
            this.ErrorMessage = String.Empty;
            List<String> l_Brands = null;
            try
            {
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "dbo.EDID_Sp_GetBrands";
                DataTable dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult != null)
                {
                    foreach (DataRow item in dtResult.Rows)
                    {
                        if (l_Brands == null)
                            l_Brands = new List<String>();
                        if (!String.IsNullOrEmpty(item["BrandName"].ToString()))
                        {
                            if (!l_Brands.Contains(item["BrandName"].ToString()))
                                l_Brands.Add(item["BrandName"].ToString());
                        }
                    }
                    if (l_Brands.Count > 0)
                        l_Brands.Sort();
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return l_Brands;
        }
        #endregion

        #region Get all Countries
        public List<String> GetAllXBoxConsoleCountries()
        {
            this.ErrorMessage = String.Empty;
            List<String> l_Locations = null;
            try
            {
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "dbo.EDID_Sp_GetLocations";
                DataTable dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult != null)
                {
                    foreach (DataRow item in dtResult.Rows)
                    {
                        if (l_Locations == null)
                            l_Locations = new List<String>();
                        if (!String.IsNullOrEmpty(item["Country"].ToString()))
                        {
                            if (!l_Locations.Contains(item["Country"].ToString()))
                                l_Locations.Add(item["Country"].ToString());
                        }
                    }
                    if (l_Locations.Count > 0)
                        l_Locations.Sort();
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return l_Locations;
        }
        #endregion

        #region Get Data Sources
        public List<String> GetAllDataSources()
        {
            this.ErrorMessage = String.Empty;
            List<String> l_DataSources = null;
            try
            {
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.Text;
                objCmd.CommandText = @"select distinct Name from dbo.DataSources where SystemFlags = 0 order by Name";
                DataTable dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult != null)
                {
                    foreach (DataRow item in dtResult.Rows)
                    {
                        if (l_DataSources == null)
                            l_DataSources = new List<String>();
                        if (!String.IsNullOrEmpty(item["Name"].ToString())) l_DataSources.Add(item["Name"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return l_DataSources;
        }
        #endregion       
        
        #region Get XBox EDID
        public List<EDIDInfo> GetXBoxEDIDReport(List<String> devicetypes, List<String> brands, List<String> models, List<String> datasources, List<String> countries, ref String m_Message)
        {
            List<EDIDInfo> EdIdData = null;
            this.ErrorMessage = String.Empty;
            
            try
            {
                DataTable dtDevices = new DataTable("DeviceType");
                if (devicetypes.Count > 0)
                {
                    dtDevices.Columns.Add("MainDevice_RID");
                    dtDevices.Columns.Add("MainDevice");
                    dtDevices.Columns.Add("SubDevice_RID");
                    dtDevices.Columns.Add("SubDevice");
                    dtDevices.AcceptChanges();
                    DataRow dr = null;
                    foreach (String item in devicetypes)
                    {
                        dr = dtDevices.NewRow();
                        dr["MainDevice_RID"] = String.Empty;
                        dr["MainDevice"] = item;
                        dr["SubDevice_RID"] = String.Empty;
                        dr["SubDevice"] = String.Empty;
                        dtDevices.Rows.Add(dr);
                        dr = null;
                    }
                    dtDevices.AcceptChanges();
                }
                String strBrands = String.Empty;
                if (brands != null)
                {
                    foreach (String item in brands)
                    {
                        strBrands += item + ",";
                    }
                    if (strBrands.Length > 0)
                        strBrands = strBrands.Remove(strBrands.Length - 1, 1);
                }
                String strModels = String.Empty;
                if (models != null)
                {
                    foreach (String item in models)
                    {
                        strModels +=  item + ",";
                    }
                    if (strModels.Length > 0)
                        strModels = strModels.Remove(strModels.Length - 1, 1);
                }
                String strDataSources = String.Empty;
                if (datasources.Count > 0)
                {
                    foreach (String item in datasources)
                    {
                        strDataSources += item + ",";
                    }
                    if (strDataSources.Length > 0)
                        strDataSources = strDataSources.Remove(strDataSources.Length - 1, 1);
                }
                String strCountries = String.Empty;
                if (countries != null)
                {
                    foreach (String item in countries)
                    {
                        strCountries += item + ",";
                    }
                    if (strDataSources.Length > 0)
                        strCountries = strCountries.Remove(strCountries.Length - 1, 1);
                }

                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "[dbo].[EDID_Sp_GetEDIDData]";
                objCmd.Parameters.Add(objDb.CreateParameter("@InsertDeviceTypes", SqlDbType.Structured, ParameterDirection.Input, dtDevices));
                objCmd.Parameters.Add(objDb.CreateParameter("@nFlag", DbType.Int32, ParameterDirection.Input, 4, 0));
                objCmd.Parameters.Add(objDb.CreateParameter("@nPageNumber", DbType.Int32, ParameterDirection.Input, 1, 1));
                objCmd.Parameters.Add(objDb.CreateParameter("@nPageSize", DbType.Int32, ParameterDirection.Input, 4, Int64.Parse(ConfigurationManager.AppSettings["TotalRecord"].ToString())));
                if (!String.IsNullOrEmpty(strCountries))
                    objCmd.Parameters.Add(objDb.CreateParameter("@vcharCountry", DbType.String, ParameterDirection.Input, strCountries.Length, strCountries));
                objCmd.Parameters.Add(objDb.CreateParameter("@vcharDataSources", DbType.String, ParameterDirection.Input, strDataSources.Length, strDataSources));
                if (!String.IsNullOrEmpty(strBrands))
                    objCmd.Parameters.Add(objDb.CreateParameter("@vcharBrand", DbType.String, ParameterDirection.Input, strBrands.Length, strBrands));
                objCmd.CommandTimeout = 960;
                m_Message = "Loading EDID from data base";
                DataTable dtTable = objDb.ExecuteDataTable(objCmd);                                                
                if (dtTable.Rows.Count > 0)
                {                    
                    EdIdData = new List<EDIDInfo>();
                    Int32 intRowCount = 1;                    
                    foreach (DataRow item in dtTable.Rows)
                    {
                        m_Message = "Reading EDID " + intRowCount.ToString() + " of " + dtTable.Rows.Count.ToString();
                        intRowCount = intRowCount + 1;
                        if (!CheckEmptyRow(item))
                        {
                            EDIDInfo m_newediddata = new EDIDInfo();
                            if (dtTable.Columns.Contains("EDID_UEI2Bin"))
                            {
                                if (!String.IsNullOrEmpty(item["EDID_UEI2Bin"].ToString()))
                                {
                                    Byte[] _binData = (Byte[])item["EDID_UEI2Bin"];
                                    m_newediddata = ReadBinFile(_binData);                                    
                                }
                            }
                            if (dtTable.Columns.Contains("MainDevice"))
                            {
                                if (!String.IsNullOrEmpty(item["MainDevice"].ToString()))
                                {
                                    m_newediddata.MainDevice = item["MainDevice"].ToString();
                                }
                            }
                            if (dtTable.Columns.Contains("SubDevice"))
                            {
                                if (!String.IsNullOrEmpty(item["SubDevice"].ToString()))
                                {
                                    m_newediddata.SubDevice = item["SubDevice"].ToString();
                                }
                            }
                            if (dtTable.Columns.Contains("Component"))
                            {
                                if (!String.IsNullOrEmpty(item["Component"].ToString()))
                                {
                                    m_newediddata.Component = item["Component"].ToString();
                                }
                            }
                            if (dtTable.Columns.Contains("Country"))
                            {
                                if (!String.IsNullOrEmpty(item["Country"].ToString()))
                                {
                                    m_newediddata.Region = item["Country"].ToString();
                                }
                            }
                            if (dtTable.Columns.Contains("Brand"))
                            {
                                if (!String.IsNullOrEmpty(item["Brand"].ToString()))
                                {
                                    m_newediddata.Brand = item["Brand"].ToString();
                                }
                            }
                            if (dtTable.Columns.Contains("UEModel"))
                            {
                                if (!String.IsNullOrEmpty(item["UEModel"].ToString()))
                                {
                                    m_newediddata.Model = item["UEModel"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("UECodeSet"))
                            {
                                if (!String.IsNullOrEmpty(item["UECodeSet"].ToString()))
                                {
                                    m_newediddata.Codeset = item["UECodeSet"].ToString().Trim();
                                }
                            }                           
                            if (dtTable.Columns.Contains("DataSource"))
                            {
                                if (!String.IsNullOrEmpty(item["DataSource"].ToString()))
                                {
                                    m_newediddata.DataSource = item["DataSource"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("ConsoleCount"))
                            {
                                if (!String.IsNullOrEmpty(item["ConsoleCount"].ToString()))
                                {
                                    m_newediddata.ConsoleCount = Int64.Parse(item["ConsoleCount"].ToString().Trim());
                                }
                            }
                            if (dtTable.Columns.Contains("UEI2ID"))
                            {
                                if (!String.IsNullOrEmpty(Convert.ToString(item["UEI2ID"])))
                                {
                                    m_newediddata.SuggestedID = item["UEI2ID"].ToString().Trim();
                                }
                            }                            
                            EdIdData.Add(m_newediddata);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return EdIdData;
        }
        public List<EDIDGroupData> GetXBoxEDIDClusterReport(List<String> devicetypes, List<String> brands, List<String> models, List<String> datasources, List<String> countries, ref String m_Message)
        {
            List<EDIDGroupData> EdIdData = null;
            this.ErrorMessage = String.Empty;

            try
            {
                DataTable dtDevices = new DataTable("DeviceType");
                if (devicetypes.Count > 0)
                {
                    dtDevices.Columns.Add("MainDevice_RID");
                    dtDevices.Columns.Add("MainDevice");
                    dtDevices.Columns.Add("SubDevice_RID");
                    dtDevices.Columns.Add("SubDevice");
                    dtDevices.AcceptChanges();
                    DataRow dr = null;
                    foreach (String item in devicetypes)
                    {
                        dr = dtDevices.NewRow();
                        dr["MainDevice_RID"] = String.Empty;
                        dr["MainDevice"] = item;
                        dr["SubDevice_RID"] = String.Empty;
                        dr["SubDevice"] = String.Empty;
                        dtDevices.Rows.Add(dr);
                        dr = null;
                    }
                    dtDevices.AcceptChanges();
                }
                String strBrands = String.Empty;
                if (brands != null)
                {
                    foreach (String item in brands)
                    {
                        strBrands += item + ",";
                    }
                    if (strBrands.Length > 0)
                        strBrands = strBrands.Remove(strBrands.Length - 1, 1);
                }
                String strModels = String.Empty;
                if (models != null)
                {
                    foreach (String item in models)
                    {
                        strModels += item + ",";
                    }
                    if (strModels.Length > 0)
                        strModels = strModels.Remove(strModels.Length - 1, 1);
                }
                String strDataSources = String.Empty;
                if (datasources.Count > 0)
                {
                    foreach (String item in datasources)
                    {
                        strDataSources += item + ",";
                    }
                    if (strDataSources.Length > 0)
                        strDataSources = strDataSources.Remove(strDataSources.Length - 1, 1);
                }
                String strCountries = String.Empty;
                if (countries != null)
                {
                    foreach (String item in countries)
                    {
                        strCountries += item + ",";
                    }
                    if (strDataSources.Length > 0)
                        strCountries = strCountries.Remove(strCountries.Length - 1, 1);
                }

                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "[dbo].[EDID_Sp_GetEDIDData]";
                objCmd.Parameters.Add(objDb.CreateParameter("@InsertDeviceTypes", SqlDbType.Structured, ParameterDirection.Input, dtDevices));
                objCmd.Parameters.Add(objDb.CreateParameter("@nFlag", DbType.Int32, ParameterDirection.Input, 4, 0));
                objCmd.Parameters.Add(objDb.CreateParameter("@nPageNumber", DbType.Int32, ParameterDirection.Input, 1, 1));
                objCmd.Parameters.Add(objDb.CreateParameter("@nPageSize", DbType.Int32, ParameterDirection.Input, 4, Int64.Parse(ConfigurationManager.AppSettings["TotalRecord"].ToString())));
                if (!String.IsNullOrEmpty(strCountries))
                    objCmd.Parameters.Add(objDb.CreateParameter("@vcharCountry", DbType.String, ParameterDirection.Input, strCountries.Length, strCountries));
                objCmd.Parameters.Add(objDb.CreateParameter("@vcharDataSources", DbType.String, ParameterDirection.Input, strDataSources.Length, strDataSources));
                if (!String.IsNullOrEmpty(strBrands))
                    objCmd.Parameters.Add(objDb.CreateParameter("@vcharBrand", DbType.String, ParameterDirection.Input, strBrands.Length, strBrands));
                objCmd.CommandTimeout = 960;
                m_Message = "Loading EDID from data base";
                DataTable dtTable = objDb.ExecuteDataTable(objCmd);
                if (dtTable.Rows.Count > 0)
                {
                    EdIdData = new List<EDIDGroupData>();
                    Int32 intRowCount = 1;
                    foreach (DataRow item in dtTable.Rows)
                    {
                        m_Message = "Reading EDID " + intRowCount.ToString() + " of " + dtTable.Rows.Count.ToString();
                        intRowCount = intRowCount + 1;
                        if (!CheckEmptyRow(item))
                        {
                            EDIDGroupData m_newediddata = new EDIDGroupData();
                            
                            if (dtTable.Columns.Contains("EDID_UEI2Bin"))
                            {
                                if (!String.IsNullOrEmpty(item["EDID_UEI2Bin"].ToString()))
                                {
                                    Byte[] _binData = (Byte[])item["EDID_UEI2Bin"];
                                    EDIDInfo m_tempediddata = ReadBinFile(_binData);
                                    m_newediddata.CustomFP = m_tempediddata.CustomFP;
                                    m_newediddata.CustomFPType0x00 = m_tempediddata.CustomFPType0x00;
                                    m_newediddata.CustomFPType0x01 = m_tempediddata.CustomFPType0x01;
                                    m_newediddata.CustomFPType0x02 = m_tempediddata.CustomFPType0x02;
                                    m_newediddata.CustomFPType0x03 = m_tempediddata.CustomFPType0x03;
                                    m_newediddata.ModelWeek = m_tempediddata.Week;
                                    m_newediddata.ModelYear = m_tempediddata.Year;
                                    m_newediddata.MonitorName = m_tempediddata.ManufacturerName;
                                    m_newediddata.EDIDBlock = m_tempediddata.EdIdRawData;
                                    m_newediddata.HorizontalScreenSize = m_tempediddata.HorizontalScreenSize;
                                    m_newediddata.VerticalScreenSize = m_tempediddata.VerticalScreenSize;
                                    m_newediddata.ProductCode = m_tempediddata.ProductCode;
                                    m_newediddata.PhysicalAddress = m_tempediddata.PhysicalAddress;
                                    m_newediddata.Only128FP = m_tempediddata.Only128FP;                                    
                                }
                            }
                            if (dtTable.Columns.Contains("MainDevice"))
                            {
                                if (!String.IsNullOrEmpty(item["MainDevice"].ToString()))
                                {
                                    m_newediddata.MainDevice = item["MainDevice"].ToString();
                                }
                            }
                            if (dtTable.Columns.Contains("SubDevice"))
                            {
                                if (!String.IsNullOrEmpty(item["SubDevice"].ToString()))
                                {
                                    m_newediddata.SubDevice = item["SubDevice"].ToString();
                                }
                            }
                            //if (dtTable.Columns.Contains("Component"))
                            //{
                            //    if (!String.IsNullOrEmpty(item["Component"].ToString()))
                            //    {
                            //        m_newediddata.Component = item["Component"].ToString();
                            //    }
                            //}
                            if (dtTable.Columns.Contains("Country"))
                            {
                                if (!String.IsNullOrEmpty(item["Country"].ToString()))
                                {
                                    m_newediddata.Region = item["Country"].ToString();
                                }
                            }
                            if (dtTable.Columns.Contains("Brand"))
                            {
                                if (!String.IsNullOrEmpty(item["Brand"].ToString()))
                                {
                                    m_newediddata.Brand = item["Brand"].ToString();
                                }
                            }
                            if (dtTable.Columns.Contains("UEModel"))
                            {
                                if (!String.IsNullOrEmpty(item["UEModel"].ToString()))
                                {
                                    m_newediddata.UserEnteredModel = item["UEModel"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("UECodeSet"))
                            {
                                if (!String.IsNullOrEmpty(item["UECodeSet"].ToString()))
                                {
                                    m_newediddata.UserEnteredCodeset = item["UECodeSet"].ToString().Trim();
                                }
                            }
                            //if (dtTable.Columns.Contains("DataSource"))
                            //{
                            //    if (!String.IsNullOrEmpty(item["DataSource"].ToString()))
                            //    {
                            //        m_newediddata.DataSource = item["DataSource"].ToString().Trim();
                            //    }
                            //}
                            if (dtTable.Columns.Contains("ConsoleCount"))
                            {
                                if (!String.IsNullOrEmpty(item["ConsoleCount"].ToString()))
                                {
                                    m_newediddata.CounsoleCount = Int64.Parse(item["ConsoleCount"].ToString().Trim());
                                }
                            }
                            if (dtTable.Columns.Contains("UEI2ID"))
                            {
                                if (!String.IsNullOrEmpty(Convert.ToString(item["UEI2ID"])))
                                {
                                    m_newediddata.SuggestedID = item["UEI2ID"].ToString().Trim();
                                }
                            }                            
                            EdIdData.Add(m_newediddata);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return EdIdData;
        }
        #endregion

        #region Grouping
        public List<EDIDGroupData> GetEDIDGroupDataWithConsoleCount(List<String> devicetypes, List<String> brands, List<String> models, List<String> datasources, List<String> countries, ref String m_Message)
        {
            List<EDIDGroupData> EdIdData = null;
            this.ErrorMessage = String.Empty;
            try
            {
                String strDevices = String.Empty;
                if (devicetypes.Count > 0)
                {
                    foreach (String item in devicetypes)
                    {
                        strDevices += "'" + item + "',";
                    }
                    if (strDevices.Length > 0)
                        strDevices = strDevices.Remove(strDevices.Length - 1, 1);
                }
                String strBrands = String.Empty;
                if (brands.Count > 0)
                {
                    foreach (String item in brands)
                    {
                        strBrands += "'" + item + "',";
                    }
                    if (strBrands.Length > 0)
                        strBrands = strBrands.Remove(strBrands.Length - 1, 1);
                }
                String strModels = String.Empty;
                if (models != null)
                {
                    foreach (String item in models)
                    {
                        strModels += "'" + item + "',";
                    }
                    if (strModels.Length > 0)
                        strModels = strModels.Remove(strModels.Length - 1, 1);
                }
                String strDataSources = String.Empty;
                if (datasources.Count > 0)
                {
                    foreach (String item in datasources)
                    {
                        strDataSources += "'" + item + "',";
                    }
                    if (strDataSources.Length > 0)
                        strDataSources = strDataSources.Remove(strDataSources.Length - 1, 1);
                }
                String strCountries = String.Empty;
                if (countries != null)
                {
                    foreach (String item in countries)
                    {
                        strCountries += "'" + item + "',";
                    }
                    if (strDataSources.Length > 0)
                        strCountries = strCountries.Remove(strCountries.Length - 1, 1);
                }
                String strQuery = String.Empty;
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.Text;
                if (!String.IsNullOrEmpty(strCountries))
                {
                    if (!String.IsNullOrEmpty(strModels))
                    {
                        if (strDevices.Contains("TV") && strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                                [Custom FP]				= T2.CustomFingerPrint,
		                                                [128 FP]				= T2.[128FP],
                                                        [MainDevice]			= 
                                                        CASE
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                        END,
                                                        [Brand]					= T8.Name,
		                                                [User Entered Model]	= T3.Model,
		                                                [User Entered Codeset]  = T4.ID,	
                                                        [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                                [Monitor Name]			= T5.MonitorName,	
		                                                [Product Code]			= 
		                                                CASE
			                                                WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                                END,	
		                                                [Year]					= 
		                                                CASE 
			                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                                ELSE T5.YearOfManufacture
		                                                END,
		                                                [Week]					= T5.WeekOfManufacture,	
                                                        [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                        [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                                [Console Count]			= COUNT(T6.Console_ID),
                                                        [EDID]                  = T1.EDID_Uei2Bin 
		                                                from dbo.EDID T1
                                                        inner join dbo.DataSources T9
		                                                on T1.FK_Source_RID = T9.Source_RID
		                                                inner join dbo.FingerPrintHistory T2
		                                                on T1.EDID_RID = T2.FK_EDID_RID
		                                                inner join dbo.ConsoleDevices T3
		                                                on T1.EDID_RID = T3.FK_EDID_RID
		                                                left join dbo.UserCodesets T4
		                                                on T3.FK_ID = T4.ID
		                                                inner join dbo.DecodedEDIDData T5
		                                                on T1.EDID_RID = T5.FK_EDID_RID
		                                                inner join dbo.ConsoleID T6
		                                                on T3.FK_Console_RID = T6.Console_RID
		                                                INNER JOIN dbo.UPnP T7
		                                                ON T5.IDManufacturerName = T7.UPnP_Code
		                                                INNER JOIN dbo.Brands T8
		                                                ON T7.FK_Brand_RID = T8.Brand_RID
		                                                INNER JOIN dbo.ConsoleDevices_ConsoleLocations T10
		                                                on T3.ConsoleDevice_RID = T10.FK_ConsoleDevice_RID
		                                                INNER JOIN dbo.ConsoleLocations T11
		                                                on T10.FK_ConsoleLocation_RID = T11.ConsoleLocation_RID
		                                                INNER JOIN dbo.LocationTranslation T12
		                                                on T11.ConsoleLocation_RID = T12.FK_ConsoleLocation_RID
		                                                INNER JOIN dbo.Locations T13
		                                                on T12.FK_Location_RID = T13.Location_RID
		                                                INNER JOIN dbo.LocationTypes T14
		                                                on T13.FK_LocationType_RID = T14.LocationType_RID and T14.Name = 'Country'
		                                                group by 				 
				                                                    T5.IDProductCode,
				                                                    CASE 
					                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                                ELSE T5.YearOfManufacture
				                                                END,
				                                                    T5.WeekOfManufacture,	
                                                                    T2.CustomFingerPrint,
				                                                    T2.[128FP],
				                                                    T4.ID,
				                                                    T3.Model,				 
				                                                    T8.Name,				 				                                         
				                                                    T3.SystemFlags,
                                                                    T9.Name,
                                                                    T1.EDID_Uei2Bin,
                                                                    T5.IDManufacturerName,
                                                                    T5.MonitorName,
                                                                    T5.HorizontalScreenSize,	
                                                                    T5.VerticalScreenSize,
					                                                T13.Name				 				 
		                                                having T8.Name in ({0})
                                                        and T9.Name in ({1})
		                                                and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))
                                                        or ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
                                                        and (T2.[128FP] is not null or T2.[128FP] not like '')
                                                        and T3.Model in ({2})
		                                                and T13.Name in ({3})
		                                                order by [Console Count] DESC,
				                                                    [User Entered Model] desc,					 
				                                                    [User Entered Codeset] desc", strBrands, strDataSources, strModels, strCountries);
                        }
                        else if (strDevices.Contains("TV"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                                [Custom FP]				= T2.CustomFingerPrint,
		                                                [128 FP]				= T2.[128FP],
                                                        [MainDevice]			= 
                                                        CASE
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                        END,
		                                                [Brand]					= T8.Name,
		                                                [User Entered Model]	= T3.Model,
		                                                [User Entered Codeset]  = T4.ID,	
                                                        [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                                [Monitor Name]			= T5.MonitorName,	
		                                                [Product Code]			= 
		                                                CASE
			                                                WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                                END,	
		                                                [Year]					= 
		                                                CASE 
			                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                                ELSE T5.YearOfManufacture
		                                                END,
		                                                [Week]					= T5.WeekOfManufacture,	
                                                        [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                        [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                                [Console Count]			= COUNT(T6.Console_ID),
                                                        [EDID]                  = T1.EDID_Uei2Bin 
		                                                from dbo.EDID T1
                                                        inner join dbo.DataSources T9
		                                                on T1.FK_Source_RID = T9.Source_RID
		                                                inner join dbo.FingerPrintHistory T2
		                                                on T1.EDID_RID = T2.FK_EDID_RID
		                                                inner join dbo.ConsoleDevices T3
		                                                on T1.EDID_RID = T3.FK_EDID_RID
		                                                left join dbo.UserCodesets T4
		                                                on T3.FK_ID = T4.ID
		                                                inner join dbo.DecodedEDIDData T5
		                                                on T1.EDID_RID = T5.FK_EDID_RID
		                                                inner join dbo.ConsoleID T6
		                                                on T3.FK_Console_RID = T6.Console_RID
		                                                INNER JOIN dbo.UPnP T7
		                                                ON T5.IDManufacturerName = T7.UPnP_Code
		                                                INNER JOIN dbo.Brands T8
		                                                ON T7.FK_Brand_RID = T8.Brand_RID
		                                                INNER JOIN dbo.ConsoleDevices_ConsoleLocations T10
		                                                on T3.ConsoleDevice_RID = T10.FK_ConsoleDevice_RID
		                                                INNER JOIN dbo.ConsoleLocations T11
		                                                on T10.FK_ConsoleLocation_RID = T11.ConsoleLocation_RID
		                                                INNER JOIN dbo.LocationTranslation T12
		                                                on T11.ConsoleLocation_RID = T12.FK_ConsoleLocation_RID
		                                                INNER JOIN dbo.Locations T13
		                                                on T12.FK_Location_RID = T13.Location_RID
		                                                INNER JOIN dbo.LocationTypes T14
		                                                on T13.FK_LocationType_RID = T14.LocationType_RID and T14.Name = 'Country'
		                                                group by 				 
				                                                    T5.IDProductCode,
				                                                    CASE 
					                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                                ELSE T5.YearOfManufacture
				                                                END,
				                                                    T5.WeekOfManufacture,	
                                                                    T2.CustomFingerPrint,
				                                                    T2.[128FP],
				                                                    T4.ID,
				                                                    T3.Model,				 
				                                                    T8.Name,				 				                                         
				                                                    T3.SystemFlags,
                                                                    T9.Name,
                                                                    T1.EDID_Uei2Bin,
                                                                    T5.IDManufacturerName,
                                                                    T5.MonitorName,
                                                                    T5.HorizontalScreenSize,	
                                                                    T5.VerticalScreenSize,
					                                                T13.Name				 				 
		                                                having T8.Name in ({0})
                                                        and T9.Name in ({1})
		                                                and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))		                                                
                                                        and (T2.[128FP] is not null or T2.[128FP] not like '')
                                                        and T3.Model in ({2})
		                                                and T13.Name in ({3})
		                                                order by [Console Count] DESC,
				                                                    [User Entered Model] desc,					 
				                                                    [User Entered Codeset] desc", strBrands, strDataSources, strModels, strCountries);
                        }
                        else if (strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                                [Custom FP]				= T2.CustomFingerPrint,
		                                                [128 FP]				= T2.[128FP],
                                                        [MainDevice]			= 
                                                        CASE
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                        END,
		                                                [Brand]					= T8.Name,
		                                                [User Entered Model]	= T3.Model,
		                                                [User Entered Codeset]  = T4.ID,	
                                                        [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                                [Monitor Name]			= T5.MonitorName,	
		                                                [Product Code]			= 
		                                                CASE
			                                                WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                                END,	
		                                                [Year]					= 
		                                                CASE 
			                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                                ELSE T5.YearOfManufacture
		                                                END,
		                                                [Week]					= T5.WeekOfManufacture,	
                                                        [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                        [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                                [Console Count]			= COUNT(T6.Console_ID),
                                                        [EDID]                  = T1.EDID_Uei2Bin 
		                                                from dbo.EDID T1
                                                        inner join dbo.DataSources T9
		                                                on T1.FK_Source_RID = T9.Source_RID
		                                                inner join dbo.FingerPrintHistory T2
		                                                on T1.EDID_RID = T2.FK_EDID_RID
		                                                inner join dbo.ConsoleDevices T3
		                                                on T1.EDID_RID = T3.FK_EDID_RID
		                                                left join dbo.UserCodesets T4
		                                                on T3.FK_ID = T4.ID
		                                                inner join dbo.DecodedEDIDData T5
		                                                on T1.EDID_RID = T5.FK_EDID_RID
		                                                inner join dbo.ConsoleID T6
		                                                on T3.FK_Console_RID = T6.Console_RID
		                                                INNER JOIN dbo.UPnP T7
		                                                ON T5.IDManufacturerName = T7.UPnP_Code
		                                                INNER JOIN dbo.Brands T8
		                                                ON T7.FK_Brand_RID = T8.Brand_RID
		                                                INNER JOIN dbo.ConsoleDevices_ConsoleLocations T10
		                                                on T3.ConsoleDevice_RID = T10.FK_ConsoleDevice_RID
		                                                INNER JOIN dbo.ConsoleLocations T11
		                                                on T10.FK_ConsoleLocation_RID = T11.ConsoleLocation_RID
		                                                INNER JOIN dbo.LocationTranslation T12
		                                                on T11.ConsoleLocation_RID = T12.FK_ConsoleLocation_RID
		                                                INNER JOIN dbo.Locations T13
		                                                on T12.FK_Location_RID = T13.Location_RID
		                                                INNER JOIN dbo.LocationTypes T14
		                                                on T13.FK_LocationType_RID = T14.LocationType_RID and T14.Name = 'Country'
		                                                group by 				 
				                                                    T5.IDProductCode,
				                                                    CASE 
					                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                                ELSE T5.YearOfManufacture
				                                                END,
				                                                    T5.WeekOfManufacture,	
                                                                    T2.CustomFingerPrint,
				                                                    T2.[128FP],
				                                                    T4.ID,
				                                                    T3.Model,				 
				                                                    T8.Name,				 				                                         
				                                                    T3.SystemFlags,
                                                                    T9.Name,
                                                                    T1.EDID_Uei2Bin,
                                                                    T5.IDManufacturerName,
                                                                    T5.MonitorName,
                                                                    T5.HorizontalScreenSize,	
                                                                    T5.VerticalScreenSize,
					                                                T13.Name				 				 
		                                                having T8.Name in ({0})
                                                        and T9.Name in ({1})
		                                                and ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
                                                        and (T2.[128FP] is not null or T2.[128FP] not like '')
                                                        and T3.Model in ({2})
		                                                and T13.Name in ({3})
		                                                order by [Console Count] DESC,
				                                                    [User Entered Model] desc,					 
				                                                    [User Entered Codeset] desc", strBrands, strDataSources, strModels, strCountries);
                        }
                    }
                    else
                    {
                        if (strDevices.Contains("TV") && strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                                [Custom FP]				= T2.CustomFingerPrint,
		                                                [128 FP]				= T2.[128FP],
                                                        [MainDevice]			= 
                                                        CASE
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                        END,
		                                                [Brand]					= T8.Name,
		                                                [User Entered Model]	= T3.Model,
		                                                [User Entered Codeset]  = T4.ID,	
                                                        [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                                [Monitor Name]			= T5.MonitorName,	
		                                                [Product Code]			= 
		                                                CASE
			                                                WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                                END,	
		                                                [Year]					= 
		                                                CASE 
			                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                                ELSE T5.YearOfManufacture
		                                                END,
		                                                [Week]					= T5.WeekOfManufacture,	
                                                        [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                        [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                                [Console Count]			= COUNT(T6.Console_ID),
                                                        [EDID]                  = T1.EDID_Uei2Bin 
		                                                from dbo.EDID T1
                                                        inner join dbo.DataSources T9
		                                                on T1.FK_Source_RID = T9.Source_RID
		                                                inner join dbo.FingerPrintHistory T2
		                                                on T1.EDID_RID = T2.FK_EDID_RID
		                                                inner join dbo.ConsoleDevices T3
		                                                on T1.EDID_RID = T3.FK_EDID_RID
		                                                left join dbo.UserCodesets T4
		                                                on T3.FK_ID = T4.ID
		                                                inner join dbo.DecodedEDIDData T5
		                                                on T1.EDID_RID = T5.FK_EDID_RID
		                                                inner join dbo.ConsoleID T6
		                                                on T3.FK_Console_RID = T6.Console_RID
		                                                INNER JOIN dbo.UPnP T7
		                                                ON T5.IDManufacturerName = T7.UPnP_Code
		                                                INNER JOIN dbo.Brands T8
		                                                ON T7.FK_Brand_RID = T8.Brand_RID
		                                                INNER JOIN dbo.ConsoleDevices_ConsoleLocations T10
		                                                on T3.ConsoleDevice_RID = T10.FK_ConsoleDevice_RID
		                                                INNER JOIN dbo.ConsoleLocations T11
		                                                on T10.FK_ConsoleLocation_RID = T11.ConsoleLocation_RID
		                                                INNER JOIN dbo.LocationTranslation T12
		                                                on T11.ConsoleLocation_RID = T12.FK_ConsoleLocation_RID
		                                                INNER JOIN dbo.Locations T13
		                                                on T12.FK_Location_RID = T13.Location_RID
		                                                INNER JOIN dbo.LocationTypes T14
		                                                on T13.FK_LocationType_RID = T14.LocationType_RID and T14.Name = 'Country'
		                                                group by 				 
				                                                    T5.IDProductCode,
				                                                    CASE 
					                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                                ELSE T5.YearOfManufacture
				                                                END,
				                                                    T5.WeekOfManufacture,	
                                                                    T2.CustomFingerPrint,
				                                                    T2.[128FP],
				                                                    T4.ID,
				                                                    T3.Model,				 
				                                                    T8.Name,				 				                                         
				                                                    T3.SystemFlags,
                                                                    T9.Name,
                                                                    T1.EDID_Uei2Bin,
                                                                    T5.IDManufacturerName,
                                                                    T5.MonitorName,
                                                                    T5.HorizontalScreenSize,	
                                                                    T5.VerticalScreenSize,
					                                                T13.Name				 				 
		                                                having T8.Name in ({0})
                                                        and T9.Name in ({1})
		                                                and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))
                                                        or ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
                                                        and (T2.[128FP] is not null or T2.[128FP] not like '')                                                        
		                                                and T13.Name in ({2})
		                                                order by [Console Count] DESC,
				                                                    [User Entered Model] desc,					 
				                                                    [User Entered Codeset] desc", strBrands, strDataSources, strCountries);
                        }
                        else if (strDevices.Contains("TV"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                                [Custom FP]				= T2.CustomFingerPrint,
		                                                [128 FP]				= T2.[128FP],
                                                        [MainDevice]			= 
                                                        CASE
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                        END,
		                                                [Brand]					= T8.Name,
		                                                [User Entered Model]	= T3.Model,
		                                                [User Entered Codeset]  = T4.ID,	
                                                        [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                                [Monitor Name]			= T5.MonitorName,	
		                                                [Product Code]			= 
		                                                CASE
			                                                WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                                END,	
		                                                [Year]					= 
		                                                CASE 
			                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                                ELSE T5.YearOfManufacture
		                                                END,
		                                                [Week]					= T5.WeekOfManufacture,	
                                                        [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                        [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                                [Console Count]			= COUNT(T6.Console_ID),
                                                        [EDID]                  = T1.EDID_Uei2Bin 
		                                                from dbo.EDID T1
                                                        inner join dbo.DataSources T9
		                                                on T1.FK_Source_RID = T9.Source_RID
		                                                inner join dbo.FingerPrintHistory T2
		                                                on T1.EDID_RID = T2.FK_EDID_RID
		                                                inner join dbo.ConsoleDevices T3
		                                                on T1.EDID_RID = T3.FK_EDID_RID
		                                                left join dbo.UserCodesets T4
		                                                on T3.FK_ID = T4.ID
		                                                inner join dbo.DecodedEDIDData T5
		                                                on T1.EDID_RID = T5.FK_EDID_RID
		                                                inner join dbo.ConsoleID T6
		                                                on T3.FK_Console_RID = T6.Console_RID
		                                                INNER JOIN dbo.UPnP T7
		                                                ON T5.IDManufacturerName = T7.UPnP_Code
		                                                INNER JOIN dbo.Brands T8
		                                                ON T7.FK_Brand_RID = T8.Brand_RID
		                                                INNER JOIN dbo.ConsoleDevices_ConsoleLocations T10
		                                                on T3.ConsoleDevice_RID = T10.FK_ConsoleDevice_RID
		                                                INNER JOIN dbo.ConsoleLocations T11
		                                                on T10.FK_ConsoleLocation_RID = T11.ConsoleLocation_RID
		                                                INNER JOIN dbo.LocationTranslation T12
		                                                on T11.ConsoleLocation_RID = T12.FK_ConsoleLocation_RID
		                                                INNER JOIN dbo.Locations T13
		                                                on T12.FK_Location_RID = T13.Location_RID
		                                                INNER JOIN dbo.LocationTypes T14
		                                                on T13.FK_LocationType_RID = T14.LocationType_RID and T14.Name = 'Country'
		                                                group by 				 
				                                                    T5.IDProductCode,
				                                                    CASE 
					                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                                ELSE T5.YearOfManufacture
				                                                END,
				                                                    T5.WeekOfManufacture,	
                                                                    T2.CustomFingerPrint,
				                                                    T2.[128FP],
				                                                    T4.ID,
				                                                    T3.Model,				 
				                                                    T8.Name,				 				                                         
				                                                    T3.SystemFlags,
                                                                    T9.Name,
                                                                    T1.EDID_Uei2Bin,
                                                                    T5.IDManufacturerName,
                                                                    T5.MonitorName,
                                                                    T5.HorizontalScreenSize,	
                                                                    T5.VerticalScreenSize,
					                                                T13.Name				 				 
		                                                having T8.Name in ({0})
                                                        and T9.Name in ({1})
		                                                and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))                                                        
                                                        and (T2.[128FP] is not null or T2.[128FP] not like '')                                                        
		                                                and T13.Name in ({2})
		                                                order by [Console Count] DESC,
				                                                    [User Entered Model] desc,					 
				                                                    [User Entered Codeset] desc", strBrands, strDataSources, strCountries);
                        }
                        else if (strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                                [Custom FP]				= T2.CustomFingerPrint,
		                                                [128 FP]				= T2.[128FP],
                                                        [MainDevice]			= 
                                                        CASE
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                        END,
		                                                [Brand]					= T8.Name,
		                                                [User Entered Model]	= T3.Model,
		                                                [User Entered Codeset]  = T4.ID,	
                                                        [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                                [Monitor Name]			= T5.MonitorName,	
		                                                [Product Code]			= 
		                                                CASE
			                                                WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                                END,	
		                                                [Year]					= 
		                                                CASE 
			                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                                ELSE T5.YearOfManufacture
		                                                END,
		                                                [Week]					= T5.WeekOfManufacture,	
                                                        [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                        [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                                [Console Count]			= COUNT(T6.Console_ID),
                                                        [EDID]                  = T1.EDID_Uei2Bin 
		                                                from dbo.EDID T1
                                                        inner join dbo.DataSources T9
		                                                on T1.FK_Source_RID = T9.Source_RID
		                                                inner join dbo.FingerPrintHistory T2
		                                                on T1.EDID_RID = T2.FK_EDID_RID
		                                                inner join dbo.ConsoleDevices T3
		                                                on T1.EDID_RID = T3.FK_EDID_RID
		                                                left join dbo.UserCodesets T4
		                                                on T3.FK_ID = T4.ID
		                                                inner join dbo.DecodedEDIDData T5
		                                                on T1.EDID_RID = T5.FK_EDID_RID
		                                                inner join dbo.ConsoleID T6
		                                                on T3.FK_Console_RID = T6.Console_RID
		                                                INNER JOIN dbo.UPnP T7
		                                                ON T5.IDManufacturerName = T7.UPnP_Code
		                                                INNER JOIN dbo.Brands T8
		                                                ON T7.FK_Brand_RID = T8.Brand_RID
		                                                INNER JOIN dbo.ConsoleDevices_ConsoleLocations T10
		                                                on T3.ConsoleDevice_RID = T10.FK_ConsoleDevice_RID
		                                                INNER JOIN dbo.ConsoleLocations T11
		                                                on T10.FK_ConsoleLocation_RID = T11.ConsoleLocation_RID
		                                                INNER JOIN dbo.LocationTranslation T12
		                                                on T11.ConsoleLocation_RID = T12.FK_ConsoleLocation_RID
		                                                INNER JOIN dbo.Locations T13
		                                                on T12.FK_Location_RID = T13.Location_RID
		                                                INNER JOIN dbo.LocationTypes T14
		                                                on T13.FK_LocationType_RID = T14.LocationType_RID and T14.Name = 'Country'
		                                                group by 				 
				                                                    T5.IDProductCode,
				                                                    CASE 
					                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                                ELSE T5.YearOfManufacture
				                                                END,
				                                                    T5.WeekOfManufacture,	
                                                                    T2.CustomFingerPrint,
				                                                    T2.[128FP],
				                                                    T4.ID,
				                                                    T3.Model,				 
				                                                    T8.Name,				 				                                         
				                                                    T3.SystemFlags,
                                                                    T9.Name,
                                                                    T1.EDID_Uei2Bin,
                                                                    T5.IDManufacturerName,
                                                                    T5.MonitorName,
                                                                    T5.HorizontalScreenSize,	
                                                                    T5.VerticalScreenSize,
					                                                T13.Name				 				 
		                                                having T8.Name in ({0})
                                                        and T9.Name in ({1})
		                                                and ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
                                                        and (T2.[128FP] is not null or T2.[128FP] not like '')                                                      
		                                                and T13.Name in ({2})
		                                                order by [Console Count] DESC,
				                                                    [User Entered Model] desc,					 
				                                                    [User Entered Codeset] desc", strBrands, strDataSources, strCountries);
                        }
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(strModels))
                    {
                        if (strDevices.Contains("TV") && strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                        [Custom FP]				= T2.CustomFingerPrint,
		                                        [128 FP]				= T2.[128FP],
                                                [MainDevice]			= 
                                                CASE
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                END,
		                                        [Brand]					= T8.Name,
		                                        [User Entered Model]	= T3.Model,
		                                        [User Entered Codeset]  = T4.ID,	
                                                [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                        [Monitor Name]			= T5.MonitorName,	
		                                        [Product Code]			= 
		                                        CASE
			                                        WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                        END,	
		                                        [Year]					= 
		                                        CASE 
			                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                        ELSE T5.YearOfManufacture
		                                        END,
		                                        [Week]					= T5.WeekOfManufacture,	
                                                [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                        [Console Count]			= COUNT(T6.Console_ID),
                                                [EDID]                  = T1.EDID_Uei2Bin 
		                                        from dbo.EDID T1
                                                inner join dbo.DataSources T9
		                                        on T1.FK_Source_RID = T9.Source_RID
		                                        inner join dbo.FingerPrintHistory T2
		                                        on T1.EDID_RID = T2.FK_EDID_RID
		                                        inner join dbo.ConsoleDevices T3
		                                        on T1.EDID_RID = T3.FK_EDID_RID
		                                        left join dbo.UserCodesets T4
		                                        on T3.FK_ID = T4.ID
		                                        inner join dbo.DecodedEDIDData T5
		                                        on T1.EDID_RID = T5.FK_EDID_RID
		                                        inner join dbo.ConsoleID T6
		                                        on T3.FK_Console_RID = T6.Console_RID
		                                        inner join dbo.UPnP T7
		                                        ON T5.IDManufacturerName = T7.UPnP_Code
		                                        inner JOIN dbo.Brands T8
		                                        ON T7.FK_Brand_RID = T8.Brand_RID
		                                        group by 				 
				                                         T5.IDProductCode,
				                                         CASE 
					                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                        ELSE T5.YearOfManufacture
				                                        END,
				                                         T5.WeekOfManufacture,	
                                                         T2.CustomFingerPrint,
				                                         T2.[128FP],
				                                         T4.ID,
				                                         T3.Model,				 
				                                         T8.Name,				 				                                         
				                                         T3.SystemFlags,
                                                         T9.Name,
                                                         T1.EDID_Uei2Bin,
                                                         T5.IDManufacturerName,
                                                         T5.MonitorName,
                                                         T5.HorizontalScreenSize,	
                                                         T5.VerticalScreenSize				 				 
		                                        having T8.Name in ({0})
                                                and T9.Name in ({1})
		                                        and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))
                                                or ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
                                                and (T2.[128FP] is not null or T2.[128FP] not like '')
                                                and T3.Model in ({2})
		                                        order by [Console Count] DESC,
				                                         [User Entered Model] desc,					 
				                                         [User Entered Codeset] desc", strBrands, strDataSources, strModels);
                        }
                        else if (strDevices.Contains("TV"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                        [Custom FP]				= T2.CustomFingerPrint,
		                                        [128 FP]				= T2.[128FP],
		                                        [Brand]					= T8.Name,
                                                [MainDevice]			= 
                                                CASE
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                END,
		                                        [User Entered Model]	= T3.Model,
		                                        [User Entered Codeset]  = T4.ID,	
                                                [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                        [Monitor Name]			= T5.MonitorName,	
		                                        [Product Code]			= 
		                                        CASE
			                                        WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                        END,	
		                                        [Year]					= 
		                                        CASE 
			                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                        ELSE T5.YearOfManufacture
		                                        END,
		                                        [Week]					= T5.WeekOfManufacture,	
                                                [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                        [Console Count]			= COUNT(T6.Console_ID),
                                                [EDID]                  = T1.EDID_Uei2Bin 
		                                        from dbo.EDID T1
                                                inner join dbo.DataSources T9
		                                        on T1.FK_Source_RID = T9.Source_RID
		                                        inner join dbo.FingerPrintHistory T2
		                                        on T1.EDID_RID = T2.FK_EDID_RID
		                                        inner join dbo.ConsoleDevices T3
		                                        on T1.EDID_RID = T3.FK_EDID_RID
		                                        left join dbo.UserCodesets T4
		                                        on T3.FK_ID = T4.ID
		                                        inner join dbo.DecodedEDIDData T5
		                                        on T1.EDID_RID = T5.FK_EDID_RID
		                                        inner join dbo.ConsoleID T6
		                                        on T3.FK_Console_RID = T6.Console_RID
		                                        INNER JOIN dbo.UPnP T7
		                                        ON T5.IDManufacturerName = T7.UPnP_Code
		                                        INNER JOIN dbo.Brands T8
		                                        ON T7.FK_Brand_RID = T8.Brand_RID
		                                        group by 				 
				                                         T5.IDProductCode,
				                                         CASE 
					                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                        ELSE T5.YearOfManufacture
				                                        END,
				                                         T5.WeekOfManufacture,	
                                                         T2.CustomFingerPrint,
				                                         T2.[128FP],
				                                         T4.ID,
				                                         T3.Model,				 
				                                         T8.Name,				 				                                         
				                                         T3.SystemFlags,
                                                         T9.Name,
                                                         T1.EDID_Uei2Bin,
                                                         T5.IDManufacturerName,
                                                         T5.MonitorName,
                                                         T5.HorizontalScreenSize,	
                                                         T5.VerticalScreenSize						 				 
		                                        having T8.Name in ({0})
                                                and T9.Name in ({1})
                                                and T3.Model in ({2})
                                                and (T2.[128FP] is not null or T2.[128FP] not like '')
		                                        and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))	                                       
		                                        order by [Console Count] DESC,
				                                         [User Entered Model] desc,					 
				                                         [User Entered Codeset] desc", strBrands, strDataSources, strModels);
                        }
                        else if (strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                        [Custom FP]				= T2.CustomFingerPrint,
		                                        [128 FP]				= T2.[128FP],
		                                        [Brand]					= T8.Name,
                                                [MainDevice]			= 
                                                CASE
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                END,
		                                        [User Entered Model]	= T3.Model,
		                                        [User Entered Codeset]  = T4.ID,	
                                                [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                        [Monitor Name]			= T5.MonitorName,	
		                                        [Product Code]			= 
		                                        CASE
			                                        WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                        END,	
		                                        [Year]					= 
		                                        CASE 
			                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                        ELSE T5.YearOfManufacture
		                                        END,
		                                        [Week]					= T5.WeekOfManufacture,	
                                                [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                        [Console Count]			= COUNT(T6.Console_ID),
                                                [EDID]                  = T1.EDID_Uei2Bin 
		                                        from dbo.EDID T1
                                                inner join dbo.DataSources T9
		                                        on T1.FK_Source_RID = T9.Source_RID
		                                        inner join dbo.FingerPrintHistory T2
		                                        on T1.EDID_RID = T2.FK_EDID_RID
		                                        inner join dbo.ConsoleDevices T3
		                                        on T1.EDID_RID = T3.FK_EDID_RID
		                                        left join dbo.UserCodesets T4
		                                        on T3.FK_ID = T4.ID
		                                        inner join dbo.DecodedEDIDData T5
		                                        on T1.EDID_RID = T5.FK_EDID_RID
		                                        inner join dbo.ConsoleID T6
		                                        on T3.FK_Console_RID = T6.Console_RID
		                                        INNER JOIN dbo.UPnP T7
		                                        ON T5.IDManufacturerName = T7.UPnP_Code
		                                        INNER JOIN dbo.Brands T8
		                                        ON T7.FK_Brand_RID = T8.Brand_RID
		                                        group by 				 
				                                         T5.IDProductCode,
				                                         CASE 
					                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                        ELSE T5.YearOfManufacture
				                                        END,
				                                         T5.WeekOfManufacture,	
                                                         T2.CustomFingerPrint,
				                                         T2.[128FP],
				                                         T4.ID,
				                                         T3.Model,				 
				                                         T8.Name,				 				                                         
				                                         T3.SystemFlags,
                                                         T9.Name,
                                                         T1.EDID_Uei2Bin,
                                                         T5.IDManufacturerName,
                                                         T5.MonitorName,
                                                         T5.HorizontalScreenSize,	
                                                         T5.VerticalScreenSize					 				 
		                                        having T8.Name in ({0})
                                                and T9.Name in ({1})
                                                and T3.Model in ({2})
                                                and (T2.[128FP] is not null or T2.[128FP] not like '')
		                                        and ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
		                                        order by [Console Count] DESC,
				                                         [User Entered Model] desc,					 
				                                         [User Entered Codeset] desc", strBrands, strDataSources, strModels);
                        }
                    }
                    else
                    {
                        if (strDevices.Contains("TV") && strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                        [Custom FP]				= T2.CustomFingerPrint,
		                                        [128 FP]				= T2.[128FP],
		                                        [Brand]					= T8.Name,
                                                [MainDevice]			= 
                                                CASE
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                END,
		                                        [User Entered Model]	= T3.Model,
		                                        [User Entered Codeset]  = T4.ID,	
                                                [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                        [Monitor Name]			= T5.MonitorName,	
		                                        [Product Code]			= 
		                                        CASE
			                                        WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                        END,	
		                                        [Year]					= 
		                                        CASE 
			                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                        ELSE T5.YearOfManufacture
		                                        END,
		                                        [Week]					= T5.WeekOfManufacture,	
                                                [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                        [Console Count]			= COUNT(T6.Console_ID),
                                                [EDID]                  = T1.EDID_Uei2Bin 
		                                        from dbo.EDID T1
                                                inner join dbo.DataSources T9
		                                        on T1.FK_Source_RID = T9.Source_RID
		                                        inner join dbo.FingerPrintHistory T2
		                                        on T1.EDID_RID = T2.FK_EDID_RID
		                                        inner join dbo.ConsoleDevices T3
		                                        on T1.EDID_RID = T3.FK_EDID_RID
		                                        left join dbo.UserCodesets T4
		                                        on T3.FK_ID = T4.ID
		                                        inner join dbo.DecodedEDIDData T5
		                                        on T1.EDID_RID = T5.FK_EDID_RID
		                                        inner join dbo.ConsoleID T6
		                                        on T3.FK_Console_RID = T6.Console_RID
		                                        INNER JOIN dbo.UPnP T7
		                                        ON T5.IDManufacturerName = T7.UPnP_Code
		                                        INNER JOIN dbo.Brands T8
		                                        ON T7.FK_Brand_RID = T8.Brand_RID
		                                        group by 				 
				                                         T5.IDProductCode,
				                                         CASE 
					                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                        ELSE T5.YearOfManufacture
				                                        END,
				                                         T5.WeekOfManufacture,	
                                                         T2.CustomFingerPrint,
				                                         T2.[128FP],
				                                         T4.ID,
				                                         T3.Model,				 
				                                         T8.Name,				 				                                         
				                                         T3.SystemFlags,
                                                         T9.Name,
                                                         T1.EDID_Uei2Bin,
                                                         T5.IDManufacturerName,
                                                         T5.MonitorName,
                                                         T5.HorizontalScreenSize,	
                                                         T5.VerticalScreenSize				 				 
		                                        having T8.Name in ({0})
                                                and T9.Name in ({1})
                                                and (T2.[128FP] is not null or T2.[128FP] not like '')
		                                        and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))
                                                or ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
		                                        order by [Console Count] DESC,
				                                         [User Entered Model] desc,					 
				                                         [User Entered Codeset] desc", strBrands, strDataSources);
                        }
                        else if (strDevices.Contains("TV"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                        [Custom FP]				= T2.CustomFingerPrint,
		                                        [128 FP]				= T2.[128FP],
		                                        [Brand]					= T8.Name,
                                                [MainDevice]			= 
                                                CASE
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                END,
		                                        [User Entered Model]	= T3.Model,
		                                        [User Entered Codeset]  = T4.ID,	
                                                [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                        [Monitor Name]			= T5.MonitorName,	
		                                        [Product Code]			= 
		                                        CASE
			                                        WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                        END,	
		                                        [Year]					= 
		                                        CASE 
			                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                        ELSE T5.YearOfManufacture
		                                        END,
		                                        [Week]					= T5.WeekOfManufacture,	
                                                [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                        [Console Count]			= COUNT(T6.Console_ID),
                                                [EDID]                  = T1.EDID_Uei2Bin 
		                                        from dbo.EDID T1
                                                inner join dbo.DataSources T9
		                                        on T1.FK_Source_RID = T9.Source_RID
		                                        inner join dbo.FingerPrintHistory T2
		                                        on T1.EDID_RID = T2.FK_EDID_RID
		                                        inner join dbo.ConsoleDevices T3
		                                        on T1.EDID_RID = T3.FK_EDID_RID
		                                        left join dbo.UserCodesets T4
		                                        on T3.FK_ID = T4.ID
		                                        inner join dbo.DecodedEDIDData T5
		                                        on T1.EDID_RID = T5.FK_EDID_RID
		                                        inner join dbo.ConsoleID T6
		                                        on T3.FK_Console_RID = T6.Console_RID
		                                        INNER JOIN dbo.UPnP T7
		                                        ON T5.IDManufacturerName = T7.UPnP_Code
		                                        INNER JOIN dbo.Brands T8
		                                        ON T7.FK_Brand_RID = T8.Brand_RID
		                                        group by 				 
				                                         T5.IDProductCode,
				                                         CASE 
					                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                        ELSE T5.YearOfManufacture
				                                        END,
				                                         T5.WeekOfManufacture,	
                                                         T2.CustomFingerPrint,
				                                         T2.[128FP],
				                                         T4.ID,
				                                         T3.Model,				 
				                                         T8.Name,				 				                                         
				                                         T3.SystemFlags,
                                                         T9.Name,
                                                         T1.EDID_Uei2Bin,
                                                         T5.IDManufacturerName,
                                                         T5.MonitorName,
                                                         T5.HorizontalScreenSize,	
                                                         T5.VerticalScreenSize						 				 
		                                        having T8.Name in ({0})
                                                and T9.Name in ({1})
                                                and (T2.[128FP] is not null or T2.[128FP] not like '')
		                                        and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))		                                       
		                                        order by [Console Count] DESC,
				                                         [User Entered Model] desc,					 
				                                         [User Entered Codeset] desc", strBrands, strDataSources);
                        }
                        else if (strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                        [Custom FP]				= T2.CustomFingerPrint,
		                                        [128 FP]				= T2.[128FP],
                                               [MainDevice]			= 
                                                CASE
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                END,
		                                        [Brand]					= T8.Name,
		                                        [User Entered Model]	= T3.Model,
		                                        [User Entered Codeset]  = T4.ID,	
                                                [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                        [Monitor Name]			= T5.MonitorName,	
		                                        [Product Code]			= 
		                                        CASE
			                                        WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                        END,	
		                                        [Year]					= 
		                                        CASE 
			                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                        ELSE T5.YearOfManufacture
		                                        END,
		                                        [Week]					= T5.WeekOfManufacture,	
                                                [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                        [Console Count]			= COUNT(T6.Console_ID),
                                                [EDID]                  = T1.EDID_Uei2Bin 
		                                        from dbo.EDID T1
                                                inner join dbo.DataSources T9
		                                        on T1.FK_Source_RID = T9.Source_RID
		                                        inner join dbo.FingerPrintHistory T2
		                                        on T1.EDID_RID = T2.FK_EDID_RID
		                                        inner join dbo.ConsoleDevices T3
		                                        on T1.EDID_RID = T3.FK_EDID_RID
		                                        left join dbo.UserCodesets T4
		                                        on T3.FK_ID = T4.ID
		                                        inner join dbo.DecodedEDIDData T5
		                                        on T1.EDID_RID = T5.FK_EDID_RID
		                                        inner join dbo.ConsoleID T6
		                                        on T3.FK_Console_RID = T6.Console_RID
		                                        INNER JOIN dbo.UPnP T7
		                                        ON T5.IDManufacturerName = T7.UPnP_Code
		                                        INNER JOIN dbo.Brands T8
		                                        ON T7.FK_Brand_RID = T8.Brand_RID
		                                        group by 				 
				                                         T5.IDProductCode,
				                                         CASE 
					                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                        ELSE T5.YearOfManufacture
				                                        END,
				                                         T5.WeekOfManufacture,	
                                                         T2.CustomFingerPrint,
				                                         T2.[128FP],
				                                         T4.ID,
				                                         T3.Model,				 
				                                         T8.Name,				 				                                         
				                                         T3.SystemFlags,
                                                         T9.Name,
                                                         T1.EDID_Uei2Bin,
                                                         T5.IDManufacturerName,
                                                         T5.MonitorName,
                                                         T5.HorizontalScreenSize,	
                                                         T5.VerticalScreenSize					 				 
		                                        having T8.Name in ({0})
                                                and T9.Name in ({1})
                                                and (T2.[128FP] is not null or T2.[128FP] not like '')
		                                        and ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
		                                        order by [Console Count] DESC,
				                                         [User Entered Model] desc,					 
				                                         [User Entered Codeset] desc", strBrands, strDataSources);
                        }
                    }
                }

                objCmd.CommandText = strQuery;
                objCmd.CommandTimeout = 960;
                m_Message = "Loading EDID with Cosnole Count from data base";
                DataTable dtTable = objDb.ExecuteDataTable(objCmd);
                if (dtTable.Rows.Count > 0)
                {
                    EdIdData = new List<EDIDGroupData>();
                    Int32 intRowCount = 1;
                    foreach (DataRow item in dtTable.Rows)
                    {
                        m_Message = "Reading EDID " + intRowCount.ToString() + " of " + dtTable.Rows.Count.ToString();
                        if (!CheckEmptyRow(item))
                        {
                            EDIDGroupData m_newediddata = new EDIDGroupData();
                            m_newediddata.SlNo = intRowCount;
                            if (dtTable.Columns.Contains("EDID"))
                            {
                                if (!String.IsNullOrEmpty(item["EDID"].ToString()))
                                {

                                    Byte[] _binData = (Byte[])item["EDID"];
                                    EDIDInfo m_edid = ReadBinFile(_binData);
                                    m_newediddata.EDIDBlock = m_edid.EdIdRawData;
                                    m_newediddata.CustomFPType0x01 = m_edid.CustomFPType0x00;
                                    m_newediddata.CustomFPType0x01 = m_edid.CustomFPType0x01;
                                    m_newediddata.CustomFPType0x02 = m_edid.CustomFPType0x02;
                                    m_newediddata.CustomFPType0x03 = m_edid.CustomFPType0x03;
                                    m_newediddata.PhysicalAddress = m_edid.PhysicalAddress;
                                }
                            }
                            if (dtTable.Columns.Contains("Custom FP"))
                            {
                                if (!String.IsNullOrEmpty(item["Custom FP"].ToString()))
                                {
                                    m_newediddata.CustomFP = item["Custom FP"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("128 FP"))
                            {
                                if (!String.IsNullOrEmpty(item["128 FP"].ToString()))
                                {
                                    m_newediddata.Only128FP = item["128 FP"].ToString().Trim();
                                }
                            }
                            //MainDevice
                            if (dtTable.Columns.Contains("MainDevice"))
                            {
                                if (!String.IsNullOrEmpty(item["MainDevice"].ToString()))
                                {
                                    m_newediddata.MainDevice = item["MainDevice"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("Brand"))
                            {
                                if (!String.IsNullOrEmpty(item["Brand"].ToString()))
                                {
                                    m_newediddata.Brand = item["Brand"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("User Entered Model"))
                            {
                                if (!String.IsNullOrEmpty(item["User Entered Model"].ToString()))
                                {
                                    m_newediddata.UserEnteredModel = item["User Entered Model"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("User Entered Codeset"))
                            {
                                if (!String.IsNullOrEmpty(item["User Entered Codeset"].ToString()))
                                {
                                    m_newediddata.UserEnteredCodeset = item["User Entered Codeset"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("Product Code"))
                            {
                                if (!String.IsNullOrEmpty(item["Product Code"].ToString()))
                                {
                                    m_newediddata.ProductCode = item["Product Code"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("Year"))
                            {
                                if (!String.IsNullOrEmpty(item["Year"].ToString()))
                                {
                                    m_newediddata.ModelYear = Int32.Parse(item["Year"].ToString().Trim());
                                }
                            }
                            if (dtTable.Columns.Contains("Week"))
                            {
                                if (!String.IsNullOrEmpty(item["Week"].ToString()))
                                {
                                    m_newediddata.ModelWeek = Int32.Parse(item["Week"].ToString().Trim());
                                }
                            }
                            if (dtTable.Columns.Contains("Console Count"))
                            {
                                if (!String.IsNullOrEmpty(item["Console Count"].ToString()))
                                {
                                    m_newediddata.CounsoleCount = Int64.Parse(item["Console Count"].ToString().Trim());
                                }
                            }
                            if (dtTable.Columns.Contains("Manufacturer (3 char) Code"))
                            {
                                if (!String.IsNullOrEmpty(item["Manufacturer (3 char) Code"].ToString()))
                                {
                                    m_newediddata.ManufacturerName = item["Manufacturer (3 char) Code"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("Monitor Name"))
                            {
                                if (!String.IsNullOrEmpty(item["Monitor Name"].ToString()))
                                {
                                    m_newediddata.MonitorName = item["Monitor Name"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("Horizontal Screen Size"))
                            {
                                if (!String.IsNullOrEmpty(item["Horizontal Screen Size"].ToString()))
                                {
                                    m_newediddata.HorizontalScreenSize = Int32.Parse(item["Horizontal Screen Size"].ToString().Trim());
                                }
                            }
                            if (dtTable.Columns.Contains("Vertical Screen Size"))
                            {
                                if (!String.IsNullOrEmpty(item["Vertical Screen Size"].ToString()))
                                {
                                    m_newediddata.VerticalScreenSize = Int32.Parse(item["Vertical Screen Size"].ToString().Trim());
                                }
                            }
                            EdIdData.Add(m_newediddata);
                        }
                        intRowCount = intRowCount + 1;
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return EdIdData;
        }
        public List<EDIDGroupData> GetEDIDGroupDataWithConsoleCountNewFPRule(List<String> devicetypes, List<String> brands, List<String> models, List<String> datasources, List<String> countries, ref String m_Message)
        {
            List<EDIDGroupData> EdIdData = null;
            this.ErrorMessage = String.Empty;
            try
            {
                String strDevices = String.Empty;
                if (devicetypes.Count > 0)
                {
                    foreach (String item in devicetypes)
                    {
                        strDevices += "'" + item + "',";
                    }
                    if (strDevices.Length > 0)
                        strDevices = strDevices.Remove(strDevices.Length - 1, 1);
                }
                String strBrands = String.Empty;
                if (brands.Count > 0)
                {
                    foreach (String item in brands)
                    {
                        strBrands += "'" + item + "',";
                    }
                    if (strBrands.Length > 0)
                        strBrands = strBrands.Remove(strBrands.Length - 1, 1);
                }
                String strModels = String.Empty;
                if (models != null)
                {
                    foreach (String item in models)
                    {
                        strModels += "'" + item + "',";
                    }
                    if (strModels.Length > 0)
                        strModels = strModels.Remove(strModels.Length - 1, 1);
                }
                String strDataSources = String.Empty;
                if (datasources.Count > 0)
                {
                    foreach (String item in datasources)
                    {
                        strDataSources += "'" + item + "',";
                    }
                    if (strDataSources.Length > 0)
                        strDataSources = strDataSources.Remove(strDataSources.Length - 1, 1);
                }
                String strCountries = String.Empty;
                if (countries != null)
                {
                    foreach (String item in countries)
                    {
                        strCountries += "'" + item + "',";
                    }
                    if (strDataSources.Length > 0)
                        strCountries = strCountries.Remove(strCountries.Length - 1, 1);
                }
                String strQuery = String.Empty;
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.Text;
                if (!String.IsNullOrEmpty(strCountries))
                {
                    if (!String.IsNullOrEmpty(strModels))
                    {
                        if (strDevices.Contains("TV") && strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                                [Custom FP]				= T2.CustomFingerPrint,
		                                                [128 FP]				= T2.[128FP],
                                                        [MainDevice]			= 
                                                        CASE
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                        END,
                                                        [Brand]					= T8.Name,
		                                                [User Entered Model]	= T3.Model,
		                                                [User Entered Codeset]  = T4.ID,	
                                                        [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                                [Monitor Name]			= T5.MonitorName,	
		                                                [Product Code]			= 
		                                                CASE
			                                                WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                                END,	
		                                                [Year]					= 
		                                                CASE 
			                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                                ELSE T5.YearOfManufacture
		                                                END,
		                                                [Week]					= T5.WeekOfManufacture,	
                                                        [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                        [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                                [Console Count]			= COUNT(T6.Console_ID),
                                                        [EDID]                  = T1.EDID_Uei2Bin 
		                                                from dbo.EDID T1
                                                        inner join dbo.DataSources T9
		                                                on T1.FK_Source_RID = T9.Source_RID
		                                                inner join dbo.FingerPrintHistory T2
		                                                on T1.EDID_RID = T2.FK_EDID_RID
		                                                inner join dbo.ConsoleDevices T3
		                                                on T1.EDID_RID = T3.FK_EDID_RID
		                                                left join dbo.UserCodesets T4
		                                                on T3.FK_ID = T4.ID
		                                                inner join dbo.DecodedEDIDData T5
		                                                on T1.EDID_RID = T5.FK_EDID_RID
		                                                inner join dbo.ConsoleID T6
		                                                on T3.FK_Console_RID = T6.Console_RID
		                                                INNER JOIN dbo.UPnP T7
		                                                ON T5.IDManufacturerName = T7.UPnP_Code
		                                                INNER JOIN dbo.Brands T8
		                                                ON T7.FK_Brand_RID = T8.Brand_RID
		                                                INNER JOIN dbo.ConsoleDevices_ConsoleLocations T10
		                                                on T3.ConsoleDevice_RID = T10.FK_ConsoleDevice_RID
		                                                INNER JOIN dbo.ConsoleLocations T11
		                                                on T10.FK_ConsoleLocation_RID = T11.ConsoleLocation_RID
		                                                INNER JOIN dbo.LocationTranslation T12
		                                                on T11.ConsoleLocation_RID = T12.FK_ConsoleLocation_RID
		                                                INNER JOIN dbo.Locations T13
		                                                on T12.FK_Location_RID = T13.Location_RID
		                                                INNER JOIN dbo.LocationTypes T14
		                                                on T13.FK_LocationType_RID = T14.LocationType_RID and T14.Name = 'Country'
		                                                group by 				 
				                                                    T5.IDProductCode,
				                                                    CASE 
					                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                                ELSE T5.YearOfManufacture
				                                                END,
				                                                    T5.WeekOfManufacture,	
                                                                    T2.CustomFingerPrint,
				                                                    T2.[128FP],
				                                                    T4.ID,
				                                                    T3.Model,				 
				                                                    T8.Name,				 				                                         
				                                                    T3.SystemFlags,
                                                                    T9.Name,
                                                                    T1.EDID_Uei2Bin,
                                                                    T5.IDManufacturerName,
                                                                    T5.MonitorName,
                                                                    T5.HorizontalScreenSize,	
                                                                    T5.VerticalScreenSize,
					                                                T13.Name				 				 
		                                                having T8.Name in ({0})
                                                        and T9.Name in ({1})
		                                                and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))
                                                        or ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
                                                        and (T2.[128FP] is not null or T2.[128FP] not like '')
                                                        and T3.Model in ({2})
		                                                and T13.Name in ({3})
		                                                order by [Console Count] DESC,
				                                                    [User Entered Model] desc,					 
				                                                    [User Entered Codeset] desc", strBrands, strDataSources, strModels, strCountries);
                        }
                        else if (strDevices.Contains("TV"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                                [Custom FP]				= T2.CustomFingerPrint,
		                                                [128 FP]				= T2.[128FP],
                                                        [MainDevice]			= 
                                                        CASE
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                        END,
		                                                [Brand]					= T8.Name,
		                                                [User Entered Model]	= T3.Model,
		                                                [User Entered Codeset]  = T4.ID,	
                                                        [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                                [Monitor Name]			= T5.MonitorName,	
		                                                [Product Code]			= 
		                                                CASE
			                                                WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                                END,	
		                                                [Year]					= 
		                                                CASE 
			                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                                ELSE T5.YearOfManufacture
		                                                END,
		                                                [Week]					= T5.WeekOfManufacture,	
                                                        [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                        [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                                [Console Count]			= COUNT(T6.Console_ID),
                                                        [EDID]                  = T1.EDID_Uei2Bin 
		                                                from dbo.EDID T1
                                                        inner join dbo.DataSources T9
		                                                on T1.FK_Source_RID = T9.Source_RID
		                                                inner join dbo.FingerPrintHistory T2
		                                                on T1.EDID_RID = T2.FK_EDID_RID
		                                                inner join dbo.ConsoleDevices T3
		                                                on T1.EDID_RID = T3.FK_EDID_RID
		                                                left join dbo.UserCodesets T4
		                                                on T3.FK_ID = T4.ID
		                                                inner join dbo.DecodedEDIDData T5
		                                                on T1.EDID_RID = T5.FK_EDID_RID
		                                                inner join dbo.ConsoleID T6
		                                                on T3.FK_Console_RID = T6.Console_RID
		                                                INNER JOIN dbo.UPnP T7
		                                                ON T5.IDManufacturerName = T7.UPnP_Code
		                                                INNER JOIN dbo.Brands T8
		                                                ON T7.FK_Brand_RID = T8.Brand_RID
		                                                INNER JOIN dbo.ConsoleDevices_ConsoleLocations T10
		                                                on T3.ConsoleDevice_RID = T10.FK_ConsoleDevice_RID
		                                                INNER JOIN dbo.ConsoleLocations T11
		                                                on T10.FK_ConsoleLocation_RID = T11.ConsoleLocation_RID
		                                                INNER JOIN dbo.LocationTranslation T12
		                                                on T11.ConsoleLocation_RID = T12.FK_ConsoleLocation_RID
		                                                INNER JOIN dbo.Locations T13
		                                                on T12.FK_Location_RID = T13.Location_RID
		                                                INNER JOIN dbo.LocationTypes T14
		                                                on T13.FK_LocationType_RID = T14.LocationType_RID and T14.Name = 'Country'
		                                                group by 				 
				                                                    T5.IDProductCode,
				                                                    CASE 
					                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                                ELSE T5.YearOfManufacture
				                                                END,
				                                                    T5.WeekOfManufacture,	
                                                                    T2.CustomFingerPrint,
				                                                    T2.[128FP],
				                                                    T4.ID,
				                                                    T3.Model,				 
				                                                    T8.Name,				 				                                         
				                                                    T3.SystemFlags,
                                                                    T9.Name,
                                                                    T1.EDID_Uei2Bin,
                                                                    T5.IDManufacturerName,
                                                                    T5.MonitorName,
                                                                    T5.HorizontalScreenSize,	
                                                                    T5.VerticalScreenSize,
					                                                T13.Name				 				 
		                                                having T8.Name in ({0})
                                                        and T9.Name in ({1})
		                                                and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))		                                                
                                                        and (T2.[128FP] is not null or T2.[128FP] not like '')
                                                        and T3.Model in ({2})
		                                                and T13.Name in ({3})
		                                                order by [Console Count] DESC,
				                                                    [User Entered Model] desc,					 
				                                                    [User Entered Codeset] desc", strBrands, strDataSources, strModels, strCountries);
                        }
                        else if (strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                                [Custom FP]				= T2.CustomFingerPrint,
		                                                [128 FP]				= T2.[128FP],
                                                        [MainDevice]			= 
                                                        CASE
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                        END,
		                                                [Brand]					= T8.Name,
		                                                [User Entered Model]	= T3.Model,
		                                                [User Entered Codeset]  = T4.ID,	
                                                        [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                                [Monitor Name]			= T5.MonitorName,	
		                                                [Product Code]			= 
		                                                CASE
			                                                WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                                END,	
		                                                [Year]					= 
		                                                CASE 
			                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                                ELSE T5.YearOfManufacture
		                                                END,
		                                                [Week]					= T5.WeekOfManufacture,	
                                                        [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                        [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                                [Console Count]			= COUNT(T6.Console_ID),
                                                        [EDID]                  = T1.EDID_Uei2Bin 
		                                                from dbo.EDID T1
                                                        inner join dbo.DataSources T9
		                                                on T1.FK_Source_RID = T9.Source_RID
		                                                inner join dbo.FingerPrintHistory T2
		                                                on T1.EDID_RID = T2.FK_EDID_RID
		                                                inner join dbo.ConsoleDevices T3
		                                                on T1.EDID_RID = T3.FK_EDID_RID
		                                                left join dbo.UserCodesets T4
		                                                on T3.FK_ID = T4.ID
		                                                inner join dbo.DecodedEDIDData T5
		                                                on T1.EDID_RID = T5.FK_EDID_RID
		                                                inner join dbo.ConsoleID T6
		                                                on T3.FK_Console_RID = T6.Console_RID
		                                                INNER JOIN dbo.UPnP T7
		                                                ON T5.IDManufacturerName = T7.UPnP_Code
		                                                INNER JOIN dbo.Brands T8
		                                                ON T7.FK_Brand_RID = T8.Brand_RID
		                                                INNER JOIN dbo.ConsoleDevices_ConsoleLocations T10
		                                                on T3.ConsoleDevice_RID = T10.FK_ConsoleDevice_RID
		                                                INNER JOIN dbo.ConsoleLocations T11
		                                                on T10.FK_ConsoleLocation_RID = T11.ConsoleLocation_RID
		                                                INNER JOIN dbo.LocationTranslation T12
		                                                on T11.ConsoleLocation_RID = T12.FK_ConsoleLocation_RID
		                                                INNER JOIN dbo.Locations T13
		                                                on T12.FK_Location_RID = T13.Location_RID
		                                                INNER JOIN dbo.LocationTypes T14
		                                                on T13.FK_LocationType_RID = T14.LocationType_RID and T14.Name = 'Country'
		                                                group by 				 
				                                                    T5.IDProductCode,
				                                                    CASE 
					                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                                ELSE T5.YearOfManufacture
				                                                END,
				                                                    T5.WeekOfManufacture,	
                                                                    T2.CustomFingerPrint,
				                                                    T2.[128FP],
				                                                    T4.ID,
				                                                    T3.Model,				 
				                                                    T8.Name,				 				                                         
				                                                    T3.SystemFlags,
                                                                    T9.Name,
                                                                    T1.EDID_Uei2Bin,
                                                                    T5.IDManufacturerName,
                                                                    T5.MonitorName,
                                                                    T5.HorizontalScreenSize,	
                                                                    T5.VerticalScreenSize,
					                                                T13.Name				 				 
		                                                having T8.Name in ({0})
                                                        and T9.Name in ({1})
		                                                and ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
                                                        and (T2.[128FP] is not null or T2.[128FP] not like '')
                                                        and T3.Model in ({2})
		                                                and T13.Name in ({3})
		                                                order by [Console Count] DESC,
				                                                    [User Entered Model] desc,					 
				                                                    [User Entered Codeset] desc", strBrands, strDataSources, strModels, strCountries);
                        }
                    }
                    else
                    {
                        if (strDevices.Contains("TV") && strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                                [Custom FP]				= T2.CustomFingerPrint,
		                                                [128 FP]				= T2.[128FP],
                                                        [MainDevice]			= 
                                                        CASE
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                        END,
		                                                [Brand]					= T8.Name,
		                                                [User Entered Model]	= T3.Model,
		                                                [User Entered Codeset]  = T4.ID,	
                                                        [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                                [Monitor Name]			= T5.MonitorName,	
		                                                [Product Code]			= 
		                                                CASE
			                                                WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                                END,	
		                                                [Year]					= 
		                                                CASE 
			                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                                ELSE T5.YearOfManufacture
		                                                END,
		                                                [Week]					= T5.WeekOfManufacture,	
                                                        [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                        [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                                [Console Count]			= COUNT(T6.Console_ID),
                                                        [EDID]                  = T1.EDID_Uei2Bin 
		                                                from dbo.EDID T1
                                                        inner join dbo.DataSources T9
		                                                on T1.FK_Source_RID = T9.Source_RID
		                                                inner join dbo.FingerPrintHistory T2
		                                                on T1.EDID_RID = T2.FK_EDID_RID
		                                                inner join dbo.ConsoleDevices T3
		                                                on T1.EDID_RID = T3.FK_EDID_RID
		                                                left join dbo.UserCodesets T4
		                                                on T3.FK_ID = T4.ID
		                                                inner join dbo.DecodedEDIDData T5
		                                                on T1.EDID_RID = T5.FK_EDID_RID
		                                                inner join dbo.ConsoleID T6
		                                                on T3.FK_Console_RID = T6.Console_RID
		                                                INNER JOIN dbo.UPnP T7
		                                                ON T5.IDManufacturerName = T7.UPnP_Code
		                                                INNER JOIN dbo.Brands T8
		                                                ON T7.FK_Brand_RID = T8.Brand_RID
		                                                INNER JOIN dbo.ConsoleDevices_ConsoleLocations T10
		                                                on T3.ConsoleDevice_RID = T10.FK_ConsoleDevice_RID
		                                                INNER JOIN dbo.ConsoleLocations T11
		                                                on T10.FK_ConsoleLocation_RID = T11.ConsoleLocation_RID
		                                                INNER JOIN dbo.LocationTranslation T12
		                                                on T11.ConsoleLocation_RID = T12.FK_ConsoleLocation_RID
		                                                INNER JOIN dbo.Locations T13
		                                                on T12.FK_Location_RID = T13.Location_RID
		                                                INNER JOIN dbo.LocationTypes T14
		                                                on T13.FK_LocationType_RID = T14.LocationType_RID and T14.Name = 'Country'
		                                                group by 				 
				                                                    T5.IDProductCode,
				                                                    CASE 
					                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                                ELSE T5.YearOfManufacture
				                                                END,
				                                                    T5.WeekOfManufacture,	
                                                                    T2.CustomFingerPrint,
				                                                    T2.[128FP],
				                                                    T4.ID,
				                                                    T3.Model,				 
				                                                    T8.Name,				 				                                         
				                                                    T3.SystemFlags,
                                                                    T9.Name,
                                                                    T1.EDID_Uei2Bin,
                                                                    T5.IDManufacturerName,
                                                                    T5.MonitorName,
                                                                    T5.HorizontalScreenSize,	
                                                                    T5.VerticalScreenSize,
					                                                T13.Name				 				 
		                                                having T8.Name in ({0})
                                                        and T9.Name in ({1})
		                                                and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))
                                                        or ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
                                                        and (T2.[128FP] is not null or T2.[128FP] not like '')                                                        
		                                                and T13.Name in ({2})
		                                                order by [Console Count] DESC,
				                                                    [User Entered Model] desc,					 
				                                                    [User Entered Codeset] desc", strBrands, strDataSources, strCountries);
                        }
                        else if (strDevices.Contains("TV"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                                [Custom FP]				= T2.CustomFingerPrint,
		                                                [128 FP]				= T2.[128FP],
                                                        [MainDevice]			= 
                                                        CASE
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                        END,
		                                                [Brand]					= T8.Name,
		                                                [User Entered Model]	= T3.Model,
		                                                [User Entered Codeset]  = T4.ID,	
                                                        [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                                [Monitor Name]			= T5.MonitorName,	
		                                                [Product Code]			= 
		                                                CASE
			                                                WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                                END,	
		                                                [Year]					= 
		                                                CASE 
			                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                                ELSE T5.YearOfManufacture
		                                                END,
		                                                [Week]					= T5.WeekOfManufacture,	
                                                        [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                        [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                                [Console Count]			= COUNT(T6.Console_ID),
                                                        [EDID]                  = T1.EDID_Uei2Bin 
		                                                from dbo.EDID T1
                                                        inner join dbo.DataSources T9
		                                                on T1.FK_Source_RID = T9.Source_RID
		                                                inner join dbo.FingerPrintHistory T2
		                                                on T1.EDID_RID = T2.FK_EDID_RID
		                                                inner join dbo.ConsoleDevices T3
		                                                on T1.EDID_RID = T3.FK_EDID_RID
		                                                left join dbo.UserCodesets T4
		                                                on T3.FK_ID = T4.ID
		                                                inner join dbo.DecodedEDIDData T5
		                                                on T1.EDID_RID = T5.FK_EDID_RID
		                                                inner join dbo.ConsoleID T6
		                                                on T3.FK_Console_RID = T6.Console_RID
		                                                INNER JOIN dbo.UPnP T7
		                                                ON T5.IDManufacturerName = T7.UPnP_Code
		                                                INNER JOIN dbo.Brands T8
		                                                ON T7.FK_Brand_RID = T8.Brand_RID
		                                                INNER JOIN dbo.ConsoleDevices_ConsoleLocations T10
		                                                on T3.ConsoleDevice_RID = T10.FK_ConsoleDevice_RID
		                                                INNER JOIN dbo.ConsoleLocations T11
		                                                on T10.FK_ConsoleLocation_RID = T11.ConsoleLocation_RID
		                                                INNER JOIN dbo.LocationTranslation T12
		                                                on T11.ConsoleLocation_RID = T12.FK_ConsoleLocation_RID
		                                                INNER JOIN dbo.Locations T13
		                                                on T12.FK_Location_RID = T13.Location_RID
		                                                INNER JOIN dbo.LocationTypes T14
		                                                on T13.FK_LocationType_RID = T14.LocationType_RID and T14.Name = 'Country'
		                                                group by 				 
				                                                    T5.IDProductCode,
				                                                    CASE 
					                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                                ELSE T5.YearOfManufacture
				                                                END,
				                                                    T5.WeekOfManufacture,	
                                                                    T2.CustomFingerPrint,
				                                                    T2.[128FP],
				                                                    T4.ID,
				                                                    T3.Model,				 
				                                                    T8.Name,				 				                                         
				                                                    T3.SystemFlags,
                                                                    T9.Name,
                                                                    T1.EDID_Uei2Bin,
                                                                    T5.IDManufacturerName,
                                                                    T5.MonitorName,
                                                                    T5.HorizontalScreenSize,	
                                                                    T5.VerticalScreenSize,
					                                                T13.Name				 				 
		                                                having T8.Name in ({0})
                                                        and T9.Name in ({1})
		                                                and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))                                                        
                                                        and (T2.[128FP] is not null or T2.[128FP] not like '')                                                        
		                                                and T13.Name in ({2})
		                                                order by [Console Count] DESC,
				                                                    [User Entered Model] desc,					 
				                                                    [User Entered Codeset] desc", strBrands, strDataSources, strCountries);
                        }
                        else if (strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                                [Custom FP]				= T2.CustomFingerPrint,
		                                                [128 FP]				= T2.[128FP],
                                                        [MainDevice]			= 
                                                        CASE
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                        WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                        END,
		                                                [Brand]					= T8.Name,
		                                                [User Entered Model]	= T3.Model,
		                                                [User Entered Codeset]  = T4.ID,	
                                                        [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                                [Monitor Name]			= T5.MonitorName,	
		                                                [Product Code]			= 
		                                                CASE
			                                                WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                                END,	
		                                                [Year]					= 
		                                                CASE 
			                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                                ELSE T5.YearOfManufacture
		                                                END,
		                                                [Week]					= T5.WeekOfManufacture,	
                                                        [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                        [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                                [Console Count]			= COUNT(T6.Console_ID),
                                                        [EDID]                  = T1.EDID_Uei2Bin 
		                                                from dbo.EDID T1
                                                        inner join dbo.DataSources T9
		                                                on T1.FK_Source_RID = T9.Source_RID
		                                                inner join dbo.FingerPrintHistory T2
		                                                on T1.EDID_RID = T2.FK_EDID_RID
		                                                inner join dbo.ConsoleDevices T3
		                                                on T1.EDID_RID = T3.FK_EDID_RID
		                                                left join dbo.UserCodesets T4
		                                                on T3.FK_ID = T4.ID
		                                                inner join dbo.DecodedEDIDData T5
		                                                on T1.EDID_RID = T5.FK_EDID_RID
		                                                inner join dbo.ConsoleID T6
		                                                on T3.FK_Console_RID = T6.Console_RID
		                                                INNER JOIN dbo.UPnP T7
		                                                ON T5.IDManufacturerName = T7.UPnP_Code
		                                                INNER JOIN dbo.Brands T8
		                                                ON T7.FK_Brand_RID = T8.Brand_RID
		                                                INNER JOIN dbo.ConsoleDevices_ConsoleLocations T10
		                                                on T3.ConsoleDevice_RID = T10.FK_ConsoleDevice_RID
		                                                INNER JOIN dbo.ConsoleLocations T11
		                                                on T10.FK_ConsoleLocation_RID = T11.ConsoleLocation_RID
		                                                INNER JOIN dbo.LocationTranslation T12
		                                                on T11.ConsoleLocation_RID = T12.FK_ConsoleLocation_RID
		                                                INNER JOIN dbo.Locations T13
		                                                on T12.FK_Location_RID = T13.Location_RID
		                                                INNER JOIN dbo.LocationTypes T14
		                                                on T13.FK_LocationType_RID = T14.LocationType_RID and T14.Name = 'Country'
		                                                group by 				 
				                                                    T5.IDProductCode,
				                                                    CASE 
					                                                WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                                ELSE T5.YearOfManufacture
				                                                END,
				                                                    T5.WeekOfManufacture,	
                                                                    T2.CustomFingerPrint,
				                                                    T2.[128FP],
				                                                    T4.ID,
				                                                    T3.Model,				 
				                                                    T8.Name,				 				                                         
				                                                    T3.SystemFlags,
                                                                    T9.Name,
                                                                    T1.EDID_Uei2Bin,
                                                                    T5.IDManufacturerName,
                                                                    T5.MonitorName,
                                                                    T5.HorizontalScreenSize,	
                                                                    T5.VerticalScreenSize,
					                                                T13.Name				 				 
		                                                having T8.Name in ({0})
                                                        and T9.Name in ({1})
		                                                and ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
                                                        and (T2.[128FP] is not null or T2.[128FP] not like '')                                                      
		                                                and T13.Name in ({2})
		                                                order by [Console Count] DESC,
				                                                    [User Entered Model] desc,					 
				                                                    [User Entered Codeset] desc", strBrands, strDataSources, strCountries);
                        }
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(strModels))
                    {
                        if (strDevices.Contains("TV") && strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                        [Custom FP]				= T2.CustomFingerPrint,
		                                        [128 FP]				= T2.[128FP],
                                                [MainDevice]			= 
                                                CASE
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                END,
		                                        [Brand]					= T8.Name,
		                                        [User Entered Model]	= T3.Model,
		                                        [User Entered Codeset]  = T4.ID,	
                                                [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                        [Monitor Name]			= T5.MonitorName,	
		                                        [Product Code]			= 
		                                        CASE
			                                        WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                        END,	
		                                        [Year]					= 
		                                        CASE 
			                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                        ELSE T5.YearOfManufacture
		                                        END,
		                                        [Week]					= T5.WeekOfManufacture,	
                                                [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                        [Console Count]			= COUNT(T6.Console_ID),
                                                [EDID]                  = T1.EDID_Uei2Bin 
		                                        from dbo.EDID T1
                                                inner join dbo.DataSources T9
		                                        on T1.FK_Source_RID = T9.Source_RID
		                                        inner join dbo.FingerPrintHistory T2
		                                        on T1.EDID_RID = T2.FK_EDID_RID
		                                        inner join dbo.ConsoleDevices T3
		                                        on T1.EDID_RID = T3.FK_EDID_RID
		                                        left join dbo.UserCodesets T4
		                                        on T3.FK_ID = T4.ID
		                                        inner join dbo.DecodedEDIDData T5
		                                        on T1.EDID_RID = T5.FK_EDID_RID
		                                        inner join dbo.ConsoleID T6
		                                        on T3.FK_Console_RID = T6.Console_RID
		                                        inner join dbo.UPnP T7
		                                        ON T5.IDManufacturerName = T7.UPnP_Code
		                                        inner JOIN dbo.Brands T8
		                                        ON T7.FK_Brand_RID = T8.Brand_RID
		                                        group by 				 
				                                         T5.IDProductCode,
				                                         CASE 
					                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                        ELSE T5.YearOfManufacture
				                                        END,
				                                         T5.WeekOfManufacture,	
                                                         T2.CustomFingerPrint,
				                                         T2.[128FP],
				                                         T4.ID,
				                                         T3.Model,				 
				                                         T8.Name,				 				                                         
				                                         T3.SystemFlags,
                                                         T9.Name,
                                                         T1.EDID_Uei2Bin,
                                                         T5.IDManufacturerName,
                                                         T5.MonitorName,
                                                         T5.HorizontalScreenSize,	
                                                         T5.VerticalScreenSize				 				 
		                                        having T8.Name in ({0})
                                                and T9.Name in ({1})
		                                        and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))
                                                or ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
                                                and (T2.[128FP] is not null or T2.[128FP] not like '')
                                                and T3.Model in ({2})
		                                        order by [Console Count] DESC,
				                                         [User Entered Model] desc,					 
				                                         [User Entered Codeset] desc", strBrands, strDataSources, strModels);
                        }
                        else if (strDevices.Contains("TV"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                        [Custom FP]				= T2.CustomFingerPrint,
		                                        [128 FP]				= T2.[128FP],
		                                        [Brand]					= T8.Name,
                                                [MainDevice]			= 
                                                CASE
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                END,
		                                        [User Entered Model]	= T3.Model,
		                                        [User Entered Codeset]  = T4.ID,	
                                                [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                        [Monitor Name]			= T5.MonitorName,	
		                                        [Product Code]			= 
		                                        CASE
			                                        WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                        END,	
		                                        [Year]					= 
		                                        CASE 
			                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                        ELSE T5.YearOfManufacture
		                                        END,
		                                        [Week]					= T5.WeekOfManufacture,	
                                                [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                        [Console Count]			= COUNT(T6.Console_ID),
                                                [EDID]                  = T1.EDID_Uei2Bin 
		                                        from dbo.EDID T1
                                                inner join dbo.DataSources T9
		                                        on T1.FK_Source_RID = T9.Source_RID
		                                        inner join dbo.FingerPrintHistory T2
		                                        on T1.EDID_RID = T2.FK_EDID_RID
		                                        inner join dbo.ConsoleDevices T3
		                                        on T1.EDID_RID = T3.FK_EDID_RID
		                                        left join dbo.UserCodesets T4
		                                        on T3.FK_ID = T4.ID
		                                        inner join dbo.DecodedEDIDData T5
		                                        on T1.EDID_RID = T5.FK_EDID_RID
		                                        inner join dbo.ConsoleID T6
		                                        on T3.FK_Console_RID = T6.Console_RID
		                                        INNER JOIN dbo.UPnP T7
		                                        ON T5.IDManufacturerName = T7.UPnP_Code
		                                        INNER JOIN dbo.Brands T8
		                                        ON T7.FK_Brand_RID = T8.Brand_RID
		                                        group by 				 
				                                         T5.IDProductCode,
				                                         CASE 
					                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                        ELSE T5.YearOfManufacture
				                                        END,
				                                         T5.WeekOfManufacture,	
                                                         T2.CustomFingerPrint,
				                                         T2.[128FP],
				                                         T4.ID,
				                                         T3.Model,				 
				                                         T8.Name,				 				                                         
				                                         T3.SystemFlags,
                                                         T9.Name,
                                                         T1.EDID_Uei2Bin,
                                                         T5.IDManufacturerName,
                                                         T5.MonitorName,
                                                         T5.HorizontalScreenSize,	
                                                         T5.VerticalScreenSize						 				 
		                                        having T8.Name in ({0})
                                                and T9.Name in ({1})
                                                and T3.Model in ({2})
                                                and (T2.[128FP] is not null or T2.[128FP] not like '')
		                                        and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))	                                       
		                                        order by [Console Count] DESC,
				                                         [User Entered Model] desc,					 
				                                         [User Entered Codeset] desc", strBrands, strDataSources, strModels);
                        }
                        else if (strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                        [Custom FP]				= T2.CustomFingerPrint,
		                                        [128 FP]				= T2.[128FP],
		                                        [Brand]					= T8.Name,
                                                [MainDevice]			= 
                                                CASE
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                END,
		                                        [User Entered Model]	= T3.Model,
		                                        [User Entered Codeset]  = T4.ID,	
                                                [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                        [Monitor Name]			= T5.MonitorName,	
		                                        [Product Code]			= 
		                                        CASE
			                                        WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                        END,	
		                                        [Year]					= 
		                                        CASE 
			                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                        ELSE T5.YearOfManufacture
		                                        END,
		                                        [Week]					= T5.WeekOfManufacture,	
                                                [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                        [Console Count]			= COUNT(T6.Console_ID),
                                                [EDID]                  = T1.EDID_Uei2Bin 
		                                        from dbo.EDID T1
                                                inner join dbo.DataSources T9
		                                        on T1.FK_Source_RID = T9.Source_RID
		                                        inner join dbo.FingerPrintHistory T2
		                                        on T1.EDID_RID = T2.FK_EDID_RID
		                                        inner join dbo.ConsoleDevices T3
		                                        on T1.EDID_RID = T3.FK_EDID_RID
		                                        left join dbo.UserCodesets T4
		                                        on T3.FK_ID = T4.ID
		                                        inner join dbo.DecodedEDIDData T5
		                                        on T1.EDID_RID = T5.FK_EDID_RID
		                                        inner join dbo.ConsoleID T6
		                                        on T3.FK_Console_RID = T6.Console_RID
		                                        INNER JOIN dbo.UPnP T7
		                                        ON T5.IDManufacturerName = T7.UPnP_Code
		                                        INNER JOIN dbo.Brands T8
		                                        ON T7.FK_Brand_RID = T8.Brand_RID
		                                        group by 				 
				                                         T5.IDProductCode,
				                                         CASE 
					                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                        ELSE T5.YearOfManufacture
				                                        END,
				                                         T5.WeekOfManufacture,	
                                                         T2.CustomFingerPrint,
				                                         T2.[128FP],
				                                         T4.ID,
				                                         T3.Model,				 
				                                         T8.Name,				 				                                         
				                                         T3.SystemFlags,
                                                         T9.Name,
                                                         T1.EDID_Uei2Bin,
                                                         T5.IDManufacturerName,
                                                         T5.MonitorName,
                                                         T5.HorizontalScreenSize,	
                                                         T5.VerticalScreenSize					 				 
		                                        having T8.Name in ({0})
                                                and T9.Name in ({1})
                                                and T3.Model in ({2})
                                                and (T2.[128FP] is not null or T2.[128FP] not like '')
		                                        and ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
		                                        order by [Console Count] DESC,
				                                         [User Entered Model] desc,					 
				                                         [User Entered Codeset] desc", strBrands, strDataSources, strModels);
                        }
                    }
                    else
                    {
                        if (strDevices.Contains("TV") && strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                        [Custom FP]				= T2.CustomFingerPrint,
		                                        [128 FP]				= T2.[128FP],
		                                        [Brand]					= T8.Name,
                                                [MainDevice]			= 
                                                CASE
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                END,
		                                        [User Entered Model]	= T3.Model,
		                                        [User Entered Codeset]  = T4.ID,	
                                                [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                        [Monitor Name]			= T5.MonitorName,	
		                                        [Product Code]			= 
		                                        CASE
			                                        WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                        END,	
		                                        [Year]					= 
		                                        CASE 
			                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                        ELSE T5.YearOfManufacture
		                                        END,
		                                        [Week]					= T5.WeekOfManufacture,	
                                                [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                        [Console Count]			= COUNT(T6.Console_ID),
                                                [EDID]                  = T1.EDID_Uei2Bin 
		                                        from dbo.EDID T1
                                                inner join dbo.DataSources T9
		                                        on T1.FK_Source_RID = T9.Source_RID
		                                        inner join dbo.FingerPrintHistory T2
		                                        on T1.EDID_RID = T2.FK_EDID_RID
		                                        inner join dbo.ConsoleDevices T3
		                                        on T1.EDID_RID = T3.FK_EDID_RID
		                                        left join dbo.UserCodesets T4
		                                        on T3.FK_ID = T4.ID
		                                        inner join dbo.DecodedEDIDData T5
		                                        on T1.EDID_RID = T5.FK_EDID_RID
		                                        inner join dbo.ConsoleID T6
		                                        on T3.FK_Console_RID = T6.Console_RID
		                                        INNER JOIN dbo.UPnP T7
		                                        ON T5.IDManufacturerName = T7.UPnP_Code
		                                        INNER JOIN dbo.Brands T8
		                                        ON T7.FK_Brand_RID = T8.Brand_RID
		                                        group by 				 
				                                         T5.IDProductCode,
				                                         CASE 
					                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                        ELSE T5.YearOfManufacture
				                                        END,
				                                         T5.WeekOfManufacture,	
                                                         T2.CustomFingerPrint,
				                                         T2.[128FP],
				                                         T4.ID,
				                                         T3.Model,				 
				                                         T8.Name,				 				                                         
				                                         T3.SystemFlags,
                                                         T9.Name,
                                                         T1.EDID_Uei2Bin,
                                                         T5.IDManufacturerName,
                                                         T5.MonitorName,
                                                         T5.HorizontalScreenSize,	
                                                         T5.VerticalScreenSize				 				 
		                                        having T8.Name in ({0})
                                                and T9.Name in ({1})
                                                and (T2.[128FP] is not null or T2.[128FP] not like '')
		                                        and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))
                                                or ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
		                                        order by [Console Count] DESC,
				                                         [User Entered Model] desc,					 
				                                         [User Entered Codeset] desc", strBrands, strDataSources);
                        }
                        else if (strDevices.Contains("TV"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                        [Custom FP]				= T2.CustomFingerPrint,
		                                        [128 FP]				= T2.[128FP],
		                                        [Brand]					= T8.Name,
                                                [MainDevice]			= 
                                                CASE
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                END,
		                                        [User Entered Model]	= T3.Model,
		                                        [User Entered Codeset]  = T4.ID,	
                                                [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                        [Monitor Name]			= T5.MonitorName,	
		                                        [Product Code]			= 
		                                        CASE
			                                        WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                        END,	
		                                        [Year]					= 
		                                        CASE 
			                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                        ELSE T5.YearOfManufacture
		                                        END,
		                                        [Week]					= T5.WeekOfManufacture,	
                                                [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                        [Console Count]			= COUNT(T6.Console_ID),
                                                [EDID]                  = T1.EDID_Uei2Bin 
		                                        from dbo.EDID T1
                                                inner join dbo.DataSources T9
		                                        on T1.FK_Source_RID = T9.Source_RID
		                                        inner join dbo.FingerPrintHistory T2
		                                        on T1.EDID_RID = T2.FK_EDID_RID
		                                        inner join dbo.ConsoleDevices T3
		                                        on T1.EDID_RID = T3.FK_EDID_RID
		                                        left join dbo.UserCodesets T4
		                                        on T3.FK_ID = T4.ID
		                                        inner join dbo.DecodedEDIDData T5
		                                        on T1.EDID_RID = T5.FK_EDID_RID
		                                        inner join dbo.ConsoleID T6
		                                        on T3.FK_Console_RID = T6.Console_RID
		                                        INNER JOIN dbo.UPnP T7
		                                        ON T5.IDManufacturerName = T7.UPnP_Code
		                                        INNER JOIN dbo.Brands T8
		                                        ON T7.FK_Brand_RID = T8.Brand_RID
		                                        group by 				 
				                                         T5.IDProductCode,
				                                         CASE 
					                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                        ELSE T5.YearOfManufacture
				                                        END,
				                                         T5.WeekOfManufacture,	
                                                         T2.CustomFingerPrint,
				                                         T2.[128FP],
				                                         T4.ID,
				                                         T3.Model,				 
				                                         T8.Name,				 				                                         
				                                         T3.SystemFlags,
                                                         T9.Name,
                                                         T1.EDID_Uei2Bin,
                                                         T5.IDManufacturerName,
                                                         T5.MonitorName,
                                                         T5.HorizontalScreenSize,	
                                                         T5.VerticalScreenSize						 				 
		                                        having T8.Name in ({0})
                                                and T9.Name in ({1})
                                                and (T2.[128FP] is not null or T2.[128FP] not like '')
		                                        and ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40))		                                       
		                                        order by [Console Count] DESC,
				                                         [User Entered Model] desc,					 
				                                         [User Entered Codeset] desc", strBrands, strDataSources);
                        }
                        else if (strDevices.Contains("Audio"))
                        {
                            strQuery = String.Format(@"select  distinct	
		                                        [Custom FP]				= T2.CustomFingerPrint,
		                                        [128 FP]				= T2.[128FP],
                                               [MainDevice]			= 
                                                CASE
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20)) THEN 'Audio'
	                                                WHEN ((T3.SystemFlags = T3.SystemFlags | 0x2) or (T3.SystemFlags = T3.SystemFlags | 0x8) or (T3.SystemFlags = T3.SystemFlags | 0x40)) THEN 'TV'
                                                END,
		                                        [Brand]					= T8.Name,
		                                        [User Entered Model]	= T3.Model,
		                                        [User Entered Codeset]  = T4.ID,	
                                                [Manufacturer (3 char) Code]=T5.IDManufacturerName,
		                                        [Monitor Name]			= T5.MonitorName,	
		                                        [Product Code]			= 
		                                        CASE
			                                        WHEN ((T5.IDProductCode IS NOT NULL) and (T5.IDProductCode NOT LIKE '')) THEN SUBSTRING(T5.IDProductCode,6,4)
		                                        END,	
		                                        [Year]					= 
		                                        CASE 
			                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
			                                        ELSE T5.YearOfManufacture
		                                        END,
		                                        [Week]					= T5.WeekOfManufacture,	
                                                [Horizontal Screen Size]	= T5.HorizontalScreenSize,	
                                                [Vertical Screen Size]	= T5.VerticalScreenSize,	
		                                        [Console Count]			= COUNT(T6.Console_ID),
                                                [EDID]                  = T1.EDID_Uei2Bin 
		                                        from dbo.EDID T1
                                                inner join dbo.DataSources T9
		                                        on T1.FK_Source_RID = T9.Source_RID
		                                        inner join dbo.FingerPrintHistory T2
		                                        on T1.EDID_RID = T2.FK_EDID_RID
		                                        inner join dbo.ConsoleDevices T3
		                                        on T1.EDID_RID = T3.FK_EDID_RID
		                                        left join dbo.UserCodesets T4
		                                        on T3.FK_ID = T4.ID
		                                        inner join dbo.DecodedEDIDData T5
		                                        on T1.EDID_RID = T5.FK_EDID_RID
		                                        inner join dbo.ConsoleID T6
		                                        on T3.FK_Console_RID = T6.Console_RID
		                                        INNER JOIN dbo.UPnP T7
		                                        ON T5.IDManufacturerName = T7.UPnP_Code
		                                        INNER JOIN dbo.Brands T8
		                                        ON T7.FK_Brand_RID = T8.Brand_RID
		                                        group by 				 
				                                         T5.IDProductCode,
				                                         CASE 
					                                        WHEN T5.YearOfManufacture = 0 THEN T5.ModelYear
					                                        ELSE T5.YearOfManufacture
				                                        END,
				                                         T5.WeekOfManufacture,	
                                                         T2.CustomFingerPrint,
				                                         T2.[128FP],
				                                         T4.ID,
				                                         T3.Model,				 
				                                         T8.Name,				 				                                         
				                                         T3.SystemFlags,
                                                         T9.Name,
                                                         T1.EDID_Uei2Bin,
                                                         T5.IDManufacturerName,
                                                         T5.MonitorName,
                                                         T5.HorizontalScreenSize,	
                                                         T5.VerticalScreenSize					 				 
		                                        having T8.Name in ({0})
                                                and T9.Name in ({1})
                                                and (T2.[128FP] is not null or T2.[128FP] not like '')
		                                        and ((T3.SystemFlags = T3.SystemFlags | 0x4) or (T3.SystemFlags = T3.SystemFlags | 0x10) or (T3.SystemFlags = T3.SystemFlags | 0x20))
		                                        order by [Console Count] DESC,
				                                         [User Entered Model] desc,					 
				                                         [User Entered Codeset] desc", strBrands, strDataSources);
                        }
                    }
                }

                objCmd.CommandText = strQuery;
                objCmd.CommandTimeout = 960;
                m_Message = "Loading EDID with Cosnole Count from data base";
                DataTable dtTable = objDb.ExecuteDataTable(objCmd);
                if (dtTable.Rows.Count > 0)
                {
                    EdIdData = new List<EDIDGroupData>();
                    Int32 intRowCount = 1;
                    foreach (DataRow item in dtTable.Rows)
                    {
                        m_Message = "Reading EDID " + intRowCount.ToString() + " of " + dtTable.Rows.Count.ToString();
                        if (!CheckEmptyRow(item))
                        {
                            EDIDGroupData m_newediddata = new EDIDGroupData();
                            m_newediddata.SlNo = intRowCount;
                            if (dtTable.Columns.Contains("EDID"))
                            {
                                if (!String.IsNullOrEmpty(item["EDID"].ToString()))
                                {

                                    Byte[] _binData = (Byte[])item["EDID"];
                                    EDIDInfo m_edid = ReadBinFile(_binData);
                                    m_newediddata.EDIDBlock = m_edid.EdIdRawData;
                                    m_newediddata.CustomFPType0x00 = m_edid.CustomFPType0x00;
                                    m_newediddata.CustomFPType0x01 = m_edid.CustomFPType0x01;
                                    m_newediddata.CustomFPType0x02 = m_edid.CustomFPType0x02;
                                    m_newediddata.CustomFPType0x03 = m_edid.CustomFPType0x03;
                                    m_newediddata.PhysicalAddress = m_edid.PhysicalAddress;
                                }
                            }
                            if (dtTable.Columns.Contains("Custom FP"))
                            {
                                if (!String.IsNullOrEmpty(item["Custom FP"].ToString()))
                                {
                                    m_newediddata.CustomFP = item["Custom FP"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("128 FP"))
                            {
                                if (!String.IsNullOrEmpty(item["128 FP"].ToString()))
                                {
                                    m_newediddata.Only128FP = item["128 FP"].ToString().Trim();
                                }
                            }
                            //MainDevice
                            if (dtTable.Columns.Contains("MainDevice"))
                            {
                                if (!String.IsNullOrEmpty(item["MainDevice"].ToString()))
                                {
                                    m_newediddata.MainDevice = item["MainDevice"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("Brand"))
                            {
                                if (!String.IsNullOrEmpty(item["Brand"].ToString()))
                                {
                                    m_newediddata.Brand = item["Brand"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("User Entered Model"))
                            {
                                if (!String.IsNullOrEmpty(item["User Entered Model"].ToString()))
                                {
                                    m_newediddata.UserEnteredModel = item["User Entered Model"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("User Entered Codeset"))
                            {
                                if (!String.IsNullOrEmpty(item["User Entered Codeset"].ToString()))
                                {
                                    m_newediddata.UserEnteredCodeset = item["User Entered Codeset"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("Product Code"))
                            {
                                if (!String.IsNullOrEmpty(item["Product Code"].ToString()))
                                {
                                    m_newediddata.ProductCode = item["Product Code"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("Year"))
                            {
                                if (!String.IsNullOrEmpty(item["Year"].ToString()))
                                {
                                    m_newediddata.ModelYear = Int32.Parse(item["Year"].ToString().Trim());
                                }
                            }
                            if (dtTable.Columns.Contains("Week"))
                            {
                                if (!String.IsNullOrEmpty(item["Week"].ToString()))
                                {
                                    m_newediddata.ModelWeek = Int32.Parse(item["Week"].ToString().Trim());
                                }
                            }
                            if (dtTable.Columns.Contains("Console Count"))
                            {
                                if (!String.IsNullOrEmpty(item["Console Count"].ToString()))
                                {
                                    m_newediddata.CounsoleCount = Int64.Parse(item["Console Count"].ToString().Trim());
                                }
                            }
                            if (dtTable.Columns.Contains("Manufacturer (3 char) Code"))
                            {
                                if (!String.IsNullOrEmpty(item["Manufacturer (3 char) Code"].ToString()))
                                {
                                    m_newediddata.ManufacturerName = item["Manufacturer (3 char) Code"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("Monitor Name"))
                            {
                                if (!String.IsNullOrEmpty(item["Monitor Name"].ToString()))
                                {
                                    m_newediddata.MonitorName = item["Monitor Name"].ToString().Trim();
                                }
                            }
                            if (dtTable.Columns.Contains("Horizontal Screen Size"))
                            {
                                if (!String.IsNullOrEmpty(item["Horizontal Screen Size"].ToString()))
                                {
                                    m_newediddata.HorizontalScreenSize = Int32.Parse(item["Horizontal Screen Size"].ToString().Trim());
                                }
                            }
                            if (dtTable.Columns.Contains("Vertical Screen Size"))
                            {
                                if (!String.IsNullOrEmpty(item["Vertical Screen Size"].ToString()))
                                {
                                    m_newediddata.VerticalScreenSize = Int32.Parse(item["Vertical Screen Size"].ToString().Trim());
                                }
                            }
                            EdIdData.Add(m_newediddata);
                        }
                        intRowCount = intRowCount + 1;
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return EdIdData;
        }
        public Boolean CheckEmptyRow(DataRow m_Row)
        {
            String valuesarr = String.Empty;
            try
            {
                foreach (Object item in m_Row.ItemArray)
                {
                    if (item != null)
                        valuesarr += item.ToString();
                }
            }
            catch { valuesarr = String.Empty; }
            if (String.IsNullOrEmpty(valuesarr))
                return true;
            else
                return false;
        }
        public EDIDGroup GetGroupEdId(List<EDIDGroupData> EdIdData, ref String m_Message, ref String m_Message1)
        {
            EDIDGroup EdIdGrouped = null;
            if (EdIdData != null)
            {
                EdIdGrouped = new EDIDGroup();
                EdIdGrouped.EdIdGroups = new Dictionary<String, Dictionary<String, List<EDIDGroupData>>>();
                String strProductCodeGroup = "PCGroup_"; Int32 intProductCodeGroupCount = 1;
                String strModelYearGroup = "MYGroup_"; Int32 intModelYearGroupCount = 1;
                String strModelWeekGroup = "MWGroup_"; Int32 intModelWeekGroupCount = 1;
                String strMixedGroup = "MixedGroup";
                Int32 intRowCount = 1;
                foreach (EDIDGroupData item in EdIdData)
                {
                    m_Message = "Processing EDID " + intRowCount.ToString() + " of " + EdIdData.Count.ToString();
                    intRowCount = intRowCount + 1;
                    Boolean dataFound = false;
                    String strCustomPlus128FP = item.CustomFP;// +"," + item.Only128FP;
                    if ((String.IsNullOrEmpty(strCustomPlus128FP)) && (!String.IsNullOrEmpty(item.Only128FP)))
                    {
                        strCustomPlus128FP = item.Only128FP;
                    }

                    //Product Code Changing, Year and Week remains Same
                    m_Message1 = "Finding Product Code Changing";
                    EDIDGroupData changingProductCode = EdIdGrouped.GetChangingProductCode(item);
                    if (changingProductCode != null)
                    {
                        //Search in existing Product Code Grouping
                        m_Message1 = "Adding to Product Code Group";
                        String productcodeGroupKey = EdIdGrouped.GetProductCodeGroupKey(item);
                        if (!String.IsNullOrEmpty(productcodeGroupKey))
                        {
                            item.BuildRule = "4";
                            Dictionary<String, List<EDIDGroupData>> m_Data = EdIdGrouped.EdIdGroups[productcodeGroupKey];
                            if (m_Data.ContainsKey(strCustomPlus128FP))
                            {
                                EDIDGroupData m_UpdateConsoleCount = EdIdGrouped.GetEdIdGroupData(m_Data[strCustomPlus128FP], item);
                                if (m_UpdateConsoleCount != null)
                                {
                                    m_UpdateConsoleCount.CounsoleCount = m_UpdateConsoleCount.CounsoleCount + item.CounsoleCount;
                                }
                                else
                                    m_Data[strCustomPlus128FP].Add(item);                                
                            }
                            else
                            {
                                m_Data.Add(strCustomPlus128FP, new List<EDIDGroupData>());
                                m_Data[strCustomPlus128FP].Add(item);
                            }
                        }
                        else
                        {
                            item.BuildRule = "4";
                            Dictionary<String, List<EDIDGroupData>> m_Data = new Dictionary<String, List<EDIDGroupData>>();
                            String strPrevCustomPlus128FP = changingProductCode.CustomFP;
                            if ((String.IsNullOrEmpty(strPrevCustomPlus128FP)) && (!String.IsNullOrEmpty(changingProductCode.Only128FP)))
                            {
                                strPrevCustomPlus128FP = item.Only128FP;
                            }
                            if (strPrevCustomPlus128FP == strCustomPlus128FP)
                            {
                                changingProductCode.BuildRule = "4";
                                m_Data.Add(strCustomPlus128FP, new List<EDIDGroupData>());
                                m_Data[strCustomPlus128FP].Add(changingProductCode);
                                m_Data[strCustomPlus128FP].Add(item);
                            }
                            else
                            {
                                changingProductCode.BuildRule = "4";
                                m_Data.Add(strPrevCustomPlus128FP, new List<EDIDGroupData>());
                                m_Data[strPrevCustomPlus128FP].Add(changingProductCode);
                                m_Data.Add(strCustomPlus128FP, new List<EDIDGroupData>());
                                m_Data[strCustomPlus128FP].Add(item);
                            }
                            EdIdGrouped.EdIdGroups.Add(strProductCodeGroup + intProductCodeGroupCount.ToString(), m_Data);
                            intProductCodeGroupCount = intProductCodeGroupCount + 1;
                        }
                        dataFound = true;
                    }
                    //Model Year Changing Product Code and Model Week remains Same
                    if (dataFound == false)
                    {
                        m_Message1 = "Finding Model Year Changing";
                        EDIDGroupData changingModelYear = EdIdGrouped.GetChangingModelYear(item);
                        if (changingModelYear != null)
                        {
                            //Search in existing Model Year Grouping
                            m_Message1 = "Adding to Model Year Group";
                            String modelYearGroupKey = EdIdGrouped.GetModelYearGroupKey(item);
                            if (!String.IsNullOrEmpty(modelYearGroupKey))
                            {
                                item.BuildRule = "4";
                                Dictionary<String, List<EDIDGroupData>> m_Data = EdIdGrouped.EdIdGroups[modelYearGroupKey];
                                if (m_Data.ContainsKey(strCustomPlus128FP))
                                {
                                    //m_Data[strCustomPlus128FP].Add(item);
                                    EDIDGroupData m_UpdateConsoleCount = EdIdGrouped.GetEdIdGroupData(m_Data[strCustomPlus128FP], item);
                                    if (m_UpdateConsoleCount != null)
                                    {
                                        m_UpdateConsoleCount.CounsoleCount = m_UpdateConsoleCount.CounsoleCount + item.CounsoleCount;
                                    }
                                    else
                                        m_Data[strCustomPlus128FP].Add(item);
                                }
                                else
                                {
                                    m_Data.Add(strCustomPlus128FP, new List<EDIDGroupData>());
                                    m_Data[strCustomPlus128FP].Add(item);
                                }
                            }
                            else
                            {
                                item.BuildRule = "4";
                                Dictionary<String, List<EDIDGroupData>> m_Data = new Dictionary<String, List<EDIDGroupData>>();
                                String strPrevCustomPlus128FP = changingModelYear.CustomFP;
                                if ((String.IsNullOrEmpty(strPrevCustomPlus128FP)) && (!String.IsNullOrEmpty(changingModelYear.Only128FP)))
                                {
                                    strPrevCustomPlus128FP = item.Only128FP;
                                }
                                if (strPrevCustomPlus128FP == strCustomPlus128FP)
                                {
                                    changingModelYear.BuildRule = "4";
                                    m_Data.Add(strCustomPlus128FP, new List<EDIDGroupData>());
                                    m_Data[strCustomPlus128FP].Add(changingModelYear);
                                    m_Data[strCustomPlus128FP].Add(item);
                                }
                                else
                                {
                                    changingModelYear.BuildRule = "4";
                                    m_Data.Add(strPrevCustomPlus128FP, new List<EDIDGroupData>());
                                    m_Data[strPrevCustomPlus128FP].Add(changingModelYear);
                                    m_Data.Add(strCustomPlus128FP, new List<EDIDGroupData>());
                                    m_Data[strCustomPlus128FP].Add(item);
                                }
                                EdIdGrouped.EdIdGroups.Add(strModelYearGroup + intModelYearGroupCount.ToString(), m_Data);
                                intModelYearGroupCount = intModelYearGroupCount + 1;
                            }
                            dataFound = true;
                        }
                    }
                    //Model Week changing Product Code, Model Year remains Same
                    if (dataFound == false)
                    {
                        m_Message1 = "Finding Model Week Changing";
                        EDIDGroupData changingModelWeek = EdIdGrouped.GetChangingModelWeek(item);
                        if (changingModelWeek != null)
                        {
                            //Search in existing Model Week Grouping
                            m_Message1 = "Adding to Model Week Group";
                            String modelWeekGroupKey = EdIdGrouped.GetModelWeekGroupKey(item);
                            if (!String.IsNullOrEmpty(modelWeekGroupKey))
                            {
                                item.BuildRule = "4";
                                Dictionary<String, List<EDIDGroupData>> m_Data = EdIdGrouped.EdIdGroups[modelWeekGroupKey];
                                if (m_Data.ContainsKey(strCustomPlus128FP))
                                {
                                    //m_Data[strCustomPlus128FP].Add(item);
                                    EDIDGroupData m_UpdateConsoleCount = EdIdGrouped.GetEdIdGroupData(m_Data[strCustomPlus128FP], item);
                                    if (m_UpdateConsoleCount != null)
                                    {
                                        m_UpdateConsoleCount.CounsoleCount = m_UpdateConsoleCount.CounsoleCount + item.CounsoleCount;
                                    }
                                    else
                                        m_Data[strCustomPlus128FP].Add(item);
                                }
                                else
                                {
                                    m_Data.Add(strCustomPlus128FP, new List<EDIDGroupData>());
                                    m_Data[strCustomPlus128FP].Add(item);
                                }
                            }
                            else
                            {
                                item.BuildRule = "4";
                                Dictionary<String, List<EDIDGroupData>> m_Data = new Dictionary<String, List<EDIDGroupData>>();
                                String strPrevCustomPlus128FP = changingModelWeek.CustomFP;
                                if ((String.IsNullOrEmpty(strPrevCustomPlus128FP)) && (!String.IsNullOrEmpty(changingModelWeek.Only128FP)))
                                {
                                    strPrevCustomPlus128FP = item.Only128FP;
                                }
                                if (strPrevCustomPlus128FP == strCustomPlus128FP)
                                {
                                    changingModelWeek.BuildRule = "4";
                                    m_Data.Add(strCustomPlus128FP, new List<EDIDGroupData>());
                                    m_Data[strCustomPlus128FP].Add(changingModelWeek);
                                    m_Data[strCustomPlus128FP].Add(item);
                                }
                                else
                                {
                                    changingModelWeek.BuildRule = "4";
                                    m_Data.Add(strPrevCustomPlus128FP, new List<EDIDGroupData>());
                                    m_Data[strPrevCustomPlus128FP].Add(changingModelWeek);
                                    m_Data.Add(strCustomPlus128FP, new List<EDIDGroupData>());
                                    m_Data[strCustomPlus128FP].Add(item);
                                }
                                EdIdGrouped.EdIdGroups.Add(strModelWeekGroup + intModelWeekGroupCount.ToString(), m_Data);
                                intModelWeekGroupCount = intModelWeekGroupCount + 1;
                            }
                            dataFound = true;
                        }
                    }
                    if (dataFound == false)
                    {
                        m_Message1 = "Adding to Mixed Group";
                        if (EdIdGrouped.EdIdGroups.ContainsKey(strMixedGroup))
                        {
                            item.BuildRule = "Mixed";
                            Dictionary<String, List<EDIDGroupData>> m_Data = EdIdGrouped.EdIdGroups[strMixedGroup];
                            if (m_Data.ContainsKey(strCustomPlus128FP))
                            {
                                m_Data[strCustomPlus128FP].Add(item);
                            }
                            else
                            {
                                m_Data.Add(strCustomPlus128FP, new List<EDIDGroupData>());
                                m_Data[strCustomPlus128FP].Add(item);
                            }
                        }
                        else
                        {
                            item.BuildRule = "Mixed";
                            Dictionary<String, List<EDIDGroupData>> m_Data = new Dictionary<String, List<EDIDGroupData>>();
                            m_Data.Add(strCustomPlus128FP, new List<EDIDGroupData>());
                            m_Data[strCustomPlus128FP].Add(item);
                            EdIdGrouped.EdIdGroups.Add(strMixedGroup, m_Data);
                        }
                    }
                }
            }
            return EdIdGrouped;
        }
        #endregion

        #region Get Discrete Profile and Discrete ID
        /// <summary>
        /// Get Discrete Profile from QuickSet3.0 DB based on Device, Brand and Model (Monitor Name or user enter Model)
        /// </summary>
        /// <param name="edid"></param>
        /// <returns></returns>
        public DataTable GetDiscreteProfile(EDIDInfo edid)
        {
            DataTable dtResult = new DataTable();
            this.ErrorMessage = String.Empty;
            try
            {
                objDb = new DBOperations(DBConnectionString.QuickSet);
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandTimeout = 960;
                objCmd.CommandText = "[dbo].[MBD_Sp_ViewDiscreteBrandProfile]";
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add(objDb.CreateParameter("@vDevice", DbType.String, ParameterDirection.Input, edid.MainDevice.Length, edid.MainDevice));
                objCmd.Parameters.Add(objDb.CreateParameter("@vBrand", DbType.String, ParameterDirection.Input, edid.Brand.Length, edid.Brand));
                objCmd.Parameters.Add(objDb.CreateParameter("@vModel", DbType.String, ParameterDirection.Input, edid.ManufacturerName.Length, edid.ManufacturerName));
                DataTable dt = objDb.ExecuteDataTable(objCmd);
                if (dt.Rows.Count > 0)
                {
                    String[] strColumns = { "ProfileName", "DiscreteID" };
                    dtResult = dt.DefaultView.ToTable(true, strColumns);
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dtResult;
        }
        #endregion

        #region Get UEI2 ID for User Enter Model and User Enter ID
        public DataTable GetUEI2IDByUserEnteredModelCodeSet(EDIDInfo edid)
        {
            DataTable dtTable = null;
            this.ErrorMessage = String.Empty;
            try
            {
                String strQuery = String.Empty;
                strQuery = String.Format(@"SELECT DISTINCT T1.ID
                                            FROM [dbo].[dr_id_tn] T1
                                            INNER JOIN [dbo].[drdevicetype] T2
                                            ON T1.rid = T2.DR_RID
                                            INNER JOIN [dbo].[Vw_DeviceUnification] T3
                                            on T2.DeviceType = T3.PresentSubDevice
                                            WHERE T1.Model LIKE '{0}'
                                            AND T1.Brand LIKE '{1}'
                                            AND T3.MainDevice LIKE '{2}'
                                            AND T1.ID LIKE '{3}' 
                                            AND T1.ID is not null", edid.Model, 
                                                                  edid.Brand, 
                                                                  edid.MainDevice, 
                                                                  edid.Codeset);
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.Text;
                objCmd.CommandText = strQuery;
                objCmd.CommandTimeout = 960;
                dtTable = objDb.ExecuteDataTable(objCmd);
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dtTable;
        }
        public DataTable GetUEI2IDByMonitorName(EDIDInfo edid)
        {
            DataTable dtTable = null;
            this.ErrorMessage = String.Empty;
            try
            {                
                String strQuery = String.Empty;
                strQuery = String.Format(@"SELECT DISTINCT T1.ID
                                            FROM [dbo].[dr_id_tn] T1
                                            INNER JOIN [dbo].[drdevicetype] T2
                                            ON T1.rid = T2.DR_RID
                                            INNER JOIN [dbo].[Vw_DeviceUnification] T3
                                            on T2.DeviceType = T3.PresentSubDevice
                                            WHERE T1.Model LIKE '{0}'
                                            AND T1.Brand LIKE '{1}'
                                            AND T3.MainDevice LIKE '{2}'
                                            AND T1.ID is not null", edid.ManufacturerName, 
                                                                  edid.Brand, 
                                                                  edid.MainDevice);
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.Text;
                objCmd.CommandText = strQuery;
                objCmd.CommandTimeout = 960;
                dtTable = objDb.ExecuteDataTable(objCmd);
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dtTable;
        }
        private DataTable GetUEI2IDByUserEnteredModelCodeSet(EDIDGroupData edidgroupdata)
        {
            DataTable dtTable = null;
            this.ErrorMessage = String.Empty;
            try
            {
                String strQuery = String.Empty;
                strQuery = String.Format(@"SELECT DISTINCT T1.ID
                                            FROM [dbo].[dr_id_tn] T1
                                            INNER JOIN [dbo].[drdevicetype] T2
                                            ON T1.rid = T2.DR_RID
                                            INNER JOIN [dbo].[Vw_DeviceUnification] T3
                                            on T2.DeviceType = T3.PresentSubDevice
                                            WHERE T1.Model LIKE '{0}'
                                            AND T1.Brand LIKE '{1}'
                                            AND T3.MainDevice LIKE '{2}'
                                            AND T1.ID LIKE '{3}'
                                            AND T1.ID is not null", edidgroupdata.UserEnteredModel, 
                                                                  edidgroupdata.Brand, 
                                                                  edidgroupdata.MainDevice, 
                                                                  edidgroupdata.UserEnteredCodeset);
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.Text;
                objCmd.CommandText = strQuery;
                objCmd.CommandTimeout = 960;               
                dtTable = objDb.ExecuteDataTable(objCmd);
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dtTable;
        }
        private DataTable GetUEI2IDByMonitorName(EDIDGroupData edidgroupdata)
        {
            DataTable dtTable = null;
            this.ErrorMessage = String.Empty;
            try
            {
                String strQuery = String.Empty;
                strQuery = String.Format(@"SELECT DISTINCT T1.ID
                                            FROM [dbo].[dr_id_tn] T1
                                            INNER JOIN [dbo].[drdevicetype] T2
                                            ON T1.rid = T2.DR_RID
                                            INNER JOIN [dbo].[Vw_DeviceUnification] T3
                                            on T2.DeviceType = T3.PresentSubDevice
                                            WHERE T1.Model LIKE '{0}'
                                            AND T1.Brand LIKE '{1}'
                                            AND T3.MainDevice LIKE '{2}'
                                            AND T1.ID is not null", edidgroupdata.MonitorName, 
                                                                  edidgroupdata.Brand, 
                                                                  edidgroupdata.MainDevice);
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.Text;
                objCmd.CommandText = strQuery;
                objCmd.CommandTimeout = 960;                
                dtTable = objDb.ExecuteDataTable(objCmd);
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dtTable;
        }
        #endregion

        #region Set Linked Id
        public Dictionary<String, EDIDGroup> SetLinkedIdInGroupedEdId(EDIDGroup edidGroups, FPIDLinkCollection LinkedIds,FingerPrintTypes fpType, ref String m_Message, ref String m_Message1)
        {
            Dictionary<String, EDIDGroup> resultEdIdGroup = null;
            EDIDGroup PCEdIdGrouped = null;
            EDIDGroup MYEdIdGrouped = null;
            EDIDGroup MWEdIdGrouped = null;
            EDIDGroup MixedEdIdGrouped = null;
            try
            {
                if (edidGroups != null)
                {
                    PCEdIdGrouped = new EDIDGroup();
                    MYEdIdGrouped = new EDIDGroup();
                    MWEdIdGrouped = new EDIDGroup();
                    MixedEdIdGrouped = new EDIDGroup();
                    Int64 intRowCount = 1;
                    foreach (String itemKey in edidGroups.EdIdGroups.Keys)
                    {
                        m_Message = "Processing Linked ID for Cluster EDID Group " + intRowCount.ToString() + " of " + edidGroups.EdIdGroups.Keys.Count.ToString();
                        intRowCount++;
                        if (edidGroups.EdIdGroups[itemKey] != null)
                        {
                            foreach (String groupItem in edidGroups.EdIdGroups[itemKey].Keys)
                            {
                                if (edidGroups.EdIdGroups[itemKey][groupItem] != null)
                                {
                                    if (edidGroups.EdIdGroups[itemKey][groupItem].Count > 0)
                                    {
                                        Int64 edidCount = 1;
                                        foreach (EDIDGroupData itemEdId in edidGroups.EdIdGroups[itemKey][groupItem])
                                        {
                                            m_Message1 = "Finding Linked ID for EDID " + edidCount.ToString() + " of " + edidGroups.EdIdGroups[itemKey][groupItem].Count.ToString();
                                            if (LinkedIds != null)
                                            {
                                                if (!String.IsNullOrEmpty(itemEdId.CustomFP.Trim()))
                                                {
                                                    Dictionary<String, String> linkidncprrule = LinkedIds.GetIDs(itemEdId,fpType);
                                                    if (!String.IsNullOrEmpty(Convert.ToString(linkidncprrule["LinkedID"])))
                                                    {
                                                        itemEdId.DACPublishedID = linkidncprrule["LinkedID"];
                                                        itemEdId.CPRRule = linkidncprrule["CPRRule"];
                                                        itemEdId.DiscreteProfile = linkidncprrule["DiscreteProfile"];
                                                        itemEdId.DiscreteID = linkidncprrule["DiscreteID"];
                                                        //switch (fpType)
                                                        //{
                                                        //    case FingerPrintTypes.CustomFPType0x00:
                                                        //        itemEdId.BuildRule = "Custom FP Type0x00,User Entered ID, DB ID Matches, Console Count High";
                                                        //        break;
                                                        //    case FingerPrintTypes.CustomFPType0x01:
                                                        //        itemEdId.BuildRule = "Custom FP Type0x01,User Entered ID, DB ID Matches, Console Count High";
                                                        //        break;
                                                        //    case FingerPrintTypes.CustomFPType0x02:
                                                        //        itemEdId.BuildRule = "Custom FP Type0x02,User Entered ID, DB ID Matches, Console Count High";
                                                        //        break;
                                                        //    case FingerPrintTypes.CustomFPType0x03:
                                                        //        itemEdId.BuildRule = "Custom FP Type0x03,User Entered ID, DB ID Matches, Console Count High";
                                                        //        break;
                                                        //    case FingerPrintTypes.CustomFP:
                                                        //        itemEdId.BuildRule = "Custom FP,User Entered ID, DB ID Matches, Console Count High";
                                                        //        break;
                                                        //    case FingerPrintTypes.Only128FP:
                                                        //        itemEdId.BuildRule = "128 FP,User Entered ID, DB ID Matches, Console Count High";
                                                        //        break;                                                            
                                                        //}
                                                        if (!String.IsNullOrEmpty(itemEdId.DACPublishedID))
                                                        {
                                                            itemEdId.BuildRule = "2";
                                                            itemEdId.TrustLevel = "100%";
                                                        }
                                                        else
                                                        {
                                                            itemEdId.BuildRule = "4";
                                                            itemEdId.TrustLevel = "90%";
                                                        }
                                                    }
                                                }                                                
                                            }
                                            
                                            //if (String.IsNullOrEmpty(itemEdId.LinkedID))
                                            //{                    
                                            //    Dictionary<String, String> mappedidncprrule = LinkedIds.GetIDs(itemEdId, itemKey);
                                            //    itemEdId.MappedID = mappedidncprrule["LinkedID"];
                                            //    itemEdId.CPRRule = mappedidncprrule["CPRRule"];
                                            //    if (!String.IsNullOrEmpty(itemEdId.MappedID.Trim()))
                                            //    {
                                            //        itemEdId.TrustLevel = "70%";                                                    
                                            //    }
                                            //}                                            
                                            edidCount++;
                                        }
                                    }
                                }
                            }
                            if (itemKey.Contains("PCGroup_"))
                            {
                                PCEdIdGrouped.EdIdGroups.Add(itemKey, edidGroups.EdIdGroups[itemKey]);
                            }
                            if (itemKey.Contains("MYGroup_"))
                            {
                                MYEdIdGrouped.EdIdGroups.Add(itemKey, edidGroups.EdIdGroups[itemKey]);
                            }
                            if (itemKey.Contains("MWGroup_"))
                            {
                                MWEdIdGrouped.EdIdGroups.Add(itemKey, edidGroups.EdIdGroups[itemKey]);
                            }
                            if (itemKey.Contains("MixedGroup"))
                            {
                                MixedEdIdGrouped.EdIdGroups.Add(itemKey, edidGroups.EdIdGroups[itemKey]);
                            }
                        }
                    }
                    resultEdIdGroup = new Dictionary<String, EDIDGroup>();

                    if (PCEdIdGrouped.EdIdGroups.Count > 0)
                        resultEdIdGroup.Add("PCGroup_", PCEdIdGrouped);
                    else
                        resultEdIdGroup.Remove("PCGroup_");

                    if (MYEdIdGrouped.EdIdGroups.Count > 0)
                        resultEdIdGroup.Add("MYGroup_", MYEdIdGrouped);
                    else
                        resultEdIdGroup.Remove("MYGroup_");

                    if (MWEdIdGrouped.EdIdGroups.Count > 0)
                        resultEdIdGroup.Add("MWGroup_", MWEdIdGrouped);
                    else
                        resultEdIdGroup.Remove("MWGroup_");

                    if (MixedEdIdGrouped.EdIdGroups.Count > 0)
                        resultEdIdGroup.Add("MixedGroup", MixedEdIdGrouped);
                    else
                        resultEdIdGroup.Remove("MixedGroup");
                }
            }
            catch { }
            return resultEdIdGroup;
        }
        #endregion

        #region Search for Mapped ID
        public Dictionary<String, EDIDGroup> SetMappedIdInGroupedEdId(Dictionary<String, EDIDGroup> ClusterededidGroups, ref String m_Message, ref String m_Message1)
        {
            //Cosider only Same Cluster Group to get Mapped ID
            //e.g PCGroup_, MYGroup_, MWGroup_,MixedGroup 
            //Rule 3.1 to 3.4
            try
            {
                if (ClusterededidGroups != null)
                {
                    foreach (String clusteredGroup in ClusterededidGroups.Keys)
                    {
                        Int64 intRowCount = 1;
                        foreach (String itemKey in ClusterededidGroups[clusteredGroup].EdIdGroups.Keys)
                        {
                            m_Message = "Processing Mapped ID for Cluster EDID Group " + intRowCount.ToString() + " of " + ClusterededidGroups[clusteredGroup].EdIdGroups.Keys.Count.ToString();
                            intRowCount++;
                            if (ClusterededidGroups[clusteredGroup].EdIdGroups[itemKey] != null)
                            {
                                foreach (String groupItem in ClusterededidGroups[clusteredGroup].EdIdGroups[itemKey].Keys)
                                {
                                    if (ClusterededidGroups[clusteredGroup].EdIdGroups[itemKey][groupItem] != null)
                                    {
                                        if (ClusterededidGroups[clusteredGroup].EdIdGroups[itemKey][groupItem].Count > 0)
                                        {
                                            Int64 edidCount = 1;
                                            foreach (EDIDGroupData itemEdId in ClusterededidGroups[clusteredGroup].EdIdGroups[itemKey][groupItem])
                                            {
                                                m_Message1 = "Finding Mapped ID for EDID " + edidCount.ToString() + " of " + ClusterededidGroups[clusteredGroup].EdIdGroups[itemKey][groupItem].Count.ToString();
                                                if (String.IsNullOrEmpty(itemEdId.DACPublishedID))
                                                {
                                                    EDIDGroupData tempItemEdId = ClusterededidGroups[clusteredGroup].GetMappedIdInOtherGroupOfSameType(ClusterededidGroups[clusteredGroup], itemEdId, clusteredGroup);
                                                    if (tempItemEdId != null)
                                                    {
                                                        if (!String.IsNullOrEmpty(tempItemEdId.DACPublishedID))
                                                        {
                                                            itemEdId.SuggestedID = tempItemEdId.DACPublishedID;
                                                            itemEdId.CPRRule = tempItemEdId.CPRRule;
                                                            itemEdId.DiscreteProfile = tempItemEdId.DiscreteProfile;
                                                            itemEdId.DiscreteID = tempItemEdId.DiscreteID;
                                                            itemEdId.BuildRule = "4";
                                                            itemEdId.TrustLevel = "70%";
                                                        }                     
                                                    }
                                                    if (String.IsNullOrEmpty(itemEdId.DACPublishedID) && !String.IsNullOrEmpty(itemEdId.UserEnteredModel) && !String.IsNullOrEmpty(itemEdId.UserEnteredCodeset))
                                                    {
                                                        try
                                                        {
                                                            DataTable dtID = GetUEI2IDByUserEnteredModelCodeSet(itemEdId);
                                                            if (dtID.Rows.Count > 0)
                                                            {
                                                                itemEdId.SuggestedID = dtID.Rows[0]["ID"].ToString();
                                                                itemEdId.CPRRule = "CPR - 60";
                                                                //itemEdId.BuildRule = "Device, Brand, User Entered Model, User Entered ID Matches Matches in DB";

                                                                itemEdId.BuildRule = "3";
                                                                itemEdId.TrustLevel = "70%";
                                                            }
                                                        }
                                                        catch (Exception ex1)
                                                        {
                                                            String s2 = ex1.Message;
                                                        }
                                                    }
                                                    if (String.IsNullOrEmpty(itemEdId.DACPublishedID) && !String.IsNullOrEmpty(itemEdId.MonitorName))
                                                    {
                                                        try
                                                        {
                                                            DataTable dtID = GetUEI2IDByMonitorName(itemEdId);
                                                            if (dtID.Rows.Count > 0)
                                                            {
                                                                itemEdId.SuggestedID = dtID.Rows[0]["ID"].ToString();
                                                                itemEdId.CPRRule = "CPR - 60";

                                                                itemEdId.BuildRule = "1";
                                                                itemEdId.TrustLevel = "70%";
                                                            }
                                                        }
                                                        catch (Exception ex2)
                                                        {
                                                            String s2 = ex2.Message;
                                                        }
                                                    }
                                                }
                                                edidCount++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch { }
            return ClusterededidGroups;
        }
        #endregion

        #endregion

        #region Methods from Unified UEI2
        public String GetLocationForEDIDModel(String strMainDevice, String strSubDevice,String strBrand, String strModel)
        {
            this.ErrorMessage = String.Empty;
            String strQuery = String.Empty;
            String l_Countries = null;
            try
            {
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.Text;
                if (!String.IsNullOrEmpty(strMainDevice) && !String.IsNullOrEmpty(strSubDevice) && !String.IsNullOrEmpty(strBrand) && !String.IsNullOrEmpty(strModel))
                {
                    strQuery = String.Format(@"SELECT	DISTINCT		
		                                    --[MainDevice]	= T4.MainDevice,
		                                    --[SubDevice]		= T4.SubDevice, 
		                                    --[Brand]			= T2.Name, 
		                                    --[Model]			= T1.Model,
		                                    --[Region]		= T6.Region,
		                                    [Country]		= T6.Country
                                    FROM dbo.Devices T1
                                    INNER JOIN dbo.Brands T2
                                    ON T1.FK_Brand_RID = T2.Brand_RID
                                    INNER JOIN dbo.DeviceTypes T3
                                    ON T1.FK_DeviceType_RID = T3.DeviceType_RID
                                    INNER JOIN dbo.Vw_DeviceUnification T4
                                    ON T3.Name = T4.PresentSubDevice
                                    INNER JOIN dbo.LocalDevices T5
                                    ON T1.Device_RID = T5.FK_Device_RID
                                    INNER JOIN dbo.Vw_RegionCountry T6
                                    ON T5.FK_Location_RID = T6.Location_RID			
                                    WHERE T4.MainDevice = '{0}'
                                    AND T4.SubDevice='{1}'
                                    AND T2.Name ='{2}'
                                    AND T1.Model='{3}'
                                    AND T6.Country NOT LIKE ''
                                    AND T6.Country IS NOT NULL", strMainDevice, strSubDevice, strBrand, strModel);
                }                
                objCmd.CommandText = strQuery;
                DataTable dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult.Rows.Count>0)
                {
                    foreach (DataRow item in dtResult.Rows)
                    {
                        if (!String.IsNullOrEmpty(item["Country"].ToString()))
                        {
                            l_Countries += item["Country"].ToString() + ",";
                        }
                    }
                    if (l_Countries.Length > 0)
                        l_Countries = l_Countries.Remove(l_Countries.Length - 1, 1);
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return l_Countries;
        }
        public List<XBox> GetXBoxEdIDData(ref String m_Message, ref String m_Message1)
        {
            List<XBox> edidData = null;

            TimeSpan totalExecTime = TimeSpan.Zero;
            ErrorLogging.WriteToLogFile("--Function Name: GetXBoxEdIdData--", false);
            ErrorLogging.WriteToLogFile("Xbox_SP_SelectEDID Input Parameters", false);
            ErrorLogging.WriteToLogFile("##########################################################", false);
            ErrorLogging.WriteToLogFile("####################### Input ############################", false);
            ErrorLogging.WriteToLogFile("##########################################################", false);            

            DateTime spStart = DateTime.Now;
            ErrorLogging.WriteToLogFile("Xbox_SP_SelectEDID execution time", false);
            ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

            
            EDIDDataAccess m_DBAccess = new EDIDDataAccess();
            //IList<Hashtable> resultCollection = m_DBAccess.GetXBoxEdIDData();
            IList<Hashtable> resultCollection = m_DBAccess.GetXBoxEdIDDataEx();
            if (resultCollection != null)
                edidData = GetEdIdInfo(resultCollection, ref m_Message, ref m_Message1);
            DateTime spEnd = DateTime.Now;
            ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

            totalExecTime = spEnd - spStart;
            ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);
            return edidData;
        }
        public String UpdateEdIdXBoxData(Int32 edidRID, Byte[] m_FileStructure, String customFingerPrint, String customFPPlusOSD, String str128FP, String str128FPPlusOSD, String osdData, String osdFP, String bytePosition, String strCustomFingerPrint0, String strCustomFingerPrint1, String strCustomFingerPrint2, String strCustomFingerPrint3, String strPhysicalAddress)
        {
            String m_Message = String.Empty;
            try
            {
                m_Message = m_DBAccess.UpdateEdIdXBoxData(edidRID, m_FileStructure, customFingerPrint, customFPPlusOSD, str128FP, str128FPPlusOSD, osdData, osdFP, bytePosition,strCustomFingerPrint0,strCustomFingerPrint1,strCustomFingerPrint2,strCustomFingerPrint3,strPhysicalAddress);
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
            }
            return m_Message;
        }
        public String UpdateEDIDData(String m_MainDevice, String m_SubDevice, String m_Component, String m_Brand, String m_Model, String m_Countries, Byte[] m_FileStructure, String m_Description, Int32 m_Flag, String Base64EDID, String CustomFingerPrint, String CustomFPPlusOSD, String Only128FP, String Only128FPPlusOSD, String OSDData, String OSDFP, String BytePosition, String strCustomFingerPrint0, String strCustomFingerPrint1, String strCustomFingerPrint2, String strCustomFingerPrint3)
        {
            String m_Message = String.Empty;
            try
            {               
                if(String.IsNullOrEmpty(Base64EDID))
                    Base64EDID = String.Empty;
                if(String.IsNullOrEmpty(CustomFingerPrint))
                    CustomFingerPrint = String.Empty;
                if(String.IsNullOrEmpty(CustomFPPlusOSD))
                    CustomFPPlusOSD = String.Empty;
                if(String.IsNullOrEmpty(Only128FP))
                    Only128FP = String.Empty;
                if(String.IsNullOrEmpty(Only128FPPlusOSD))
                    Only128FPPlusOSD = String.Empty;
                if(String.IsNullOrEmpty(OSDData))
                    OSDData = String.Empty;
                if(String.IsNullOrEmpty(OSDFP))
                    OSDFP = String.Empty;
                if(String.IsNullOrEmpty(BytePosition))
                    BytePosition = String.Empty;

                if(String.IsNullOrEmpty(strCustomFingerPrint0))
                    strCustomFingerPrint0 = String.Empty;
                if(String.IsNullOrEmpty(strCustomFingerPrint1))
                    strCustomFingerPrint1 = String.Empty;
                if(String.IsNullOrEmpty(strCustomFingerPrint2))
                    strCustomFingerPrint2 = String.Empty;
                if(String.IsNullOrEmpty(strCustomFingerPrint3))
                    strCustomFingerPrint3 = String.Empty;
                m_Message = m_DBAccess.UpdateEDIDData(m_MainDevice, m_SubDevice, m_Component, m_Brand, m_Model, m_Countries, m_FileStructure, m_Description, m_Flag, Base64EDID, CustomFingerPrint, CustomFPPlusOSD, Only128FP, Only128FPPlusOSD, OSDData, OSDFP, BytePosition,strCustomFingerPrint0,strCustomFingerPrint1,strCustomFingerPrint2,strCustomFingerPrint3);
            }
            catch (Exception ex)
            {
                m_Message = ex.Message;
                ErrorLogging.WriteToErrorLogFile(ex);
            }
            return m_Message;
        }
        public List<EDIDInfo> GetEdIdData(IList<UEI.Workflow2010.Report.Model.Region> locations, List<String> selectedModes, IList<UEI.Workflow2010.Report.Model.DeviceType> devices, IList<UEI.Workflow2010.Report.Model.DataSources> dataSources, UEI.Workflow2010.Report.Model.IdSetupCodeInfo idSetupCodeInfoParam, UEI.Workflow2010.Report.Model.BrandFilter brands, UEI.Workflow2010.Report.Model.ModelFilter models,ref DataTable dtResult, ref String  m_Message)
        {
            this.ErrorMessage = String.Empty;
            IDSearchParameter searchParam = null;
            List<EDIDInfo> edidData = null;
            //add locations xml
            String strLocations = String.Empty;
            StringBuilder paramBuilder = new StringBuilder();
            XMLOperations _XML = null;
            try
            {
                if (locations != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "LocationSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach (UEI.Workflow2010.Report.Model.Region r in locations)
                    {
                        _XML.ParentNode = "Region";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = r.Name;
                        _XML.ParentAttributeName = "Weight";
                        _XML.ParentAttributeValue = "0";
                        _XML.ParentAttributeName = "SystemFlags";
                        if (idSetupCodeInfoParam.IncludeNonCountryLinkedRecords == true)
                        {
                            _XML.ParentAttributeValue = "2";
                        }
                        else
                        {
                            _XML.ParentAttributeValue = "0";
                        }
                        if (r.SubRegions != null)
                        {
                            foreach (UEI.Workflow2010.Report.Model.Region subregion in r.SubRegions)
                            {
                                _XML.ChildNode = "SubRegion";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = subregion.Name;
                                _XML.ChildAttributeName = "Weight";
                                _XML.ChildAttributeValue = "0";
                                _XML.ChildAttributeName = "SystemFlags";
                                _XML.ChildAttributeValue = "0";
                                if (r.Countries != null)
                                {
                                    foreach (UEI.Workflow2010.Report.Model.Country country in r.Countries)
                                    {
                                        if (subregion.Name == country.SubRegion)
                                        {
                                            _XML.ChildNode = "Country";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = country.Name;
                                            _XML.ChildAttributeName = "Weight";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        if (r.Countries != null)
                        {
                            foreach (UEI.Workflow2010.Report.Model.Country country in r.Countries)
                            {
                                if (country.SubRegion == String.Empty)
                                {
                                    _XML.ChildNode = "Country";
                                    _XML.ChildAttributeName = "Name";
                                    _XML.ChildAttributeValue = country.Name;
                                    _XML.ChildAttributeName = "Weight";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.ChildAttributeName = "SystemFlags";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.AppendChild();
                                }
                            }
                        }
                        _XML.AppendParent();
                    }
                }
                strLocations = _XML.XMLGenerator();
                _XML = null;

                //add device xml
                String strDevices = String.Empty;
                if (devices != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "DeviceSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach (String mode in selectedModes)
                    {
                        _XML.ParentNode = "DeviceMode";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = mode;
                        _XML.ParentAttributeName = "SystemFlags";
                        //if (idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                        //    _XML.ParentAttributeValue = "3";
                        //else if (idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                        //    _XML.ParentAttributeValue = "1";
                        if (idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            _XML.ParentAttributeValue = "7";
                        else if (idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            _XML.ParentAttributeValue = "4";
                        else if (idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            _XML.ParentAttributeValue = "3";
                        else if (idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            _XML.ParentAttributeValue = "1";
                        foreach (UEI.Workflow2010.Report.Model.DeviceType device in devices)
                        {
                            if (device.Mode == mode)
                            {
                                _XML.ChildNode = "MainDevices";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = device.Name;
                                _XML.ChildAttributeName = "SystemFlags";
                                _XML.ChildAttributeValue = "1";
                                if (device.SubDevices != null)
                                {
                                    foreach (UEI.Workflow2010.Report.Model.DeviceType subdevice in device.SubDevices)
                                    {
                                        _XML.ChildNode = "SubDevices";
                                        _XML.ChildAttributeName = "Name";
                                        _XML.ChildAttributeValue = subdevice.Name;
                                        _XML.ChildAttributeName = "SystemFlags";
                                        _XML.ChildAttributeValue = "1";
                                        if (device.Components != null)
                                        {
                                            foreach (UEI.Workflow2010.Report.Model.Component component in device.Components)
                                            {
                                                if (subdevice.Name == component.SubDeviceType)
                                                {
                                                    _XML.ChildNode = "Component";
                                                    _XML.ChildAttributeName = "Name";
                                                    _XML.ChildAttributeValue = component.Name;
                                                    _XML.ChildAttributeName = "SystemFlags";
                                                    _XML.ChildAttributeValue = "0";
                                                    _XML.ChildAttributeName = "SystemFlagsExt";
                                                    _XML.ChildAttributeValue = "1";
                                                    _XML.AppendChild();
                                                }
                                            }
                                        }
                                        _XML.AppendChild();
                                    }
                                }
                                if (device.Components != null)
                                {
                                    foreach (UEI.Workflow2010.Report.Model.Component component in device.Components)
                                    {
                                        if (component.SubDeviceType == String.Empty)
                                        {
                                            _XML.ChildNode = "Component";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = component.Name;
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlagsExt";
                                            _XML.ChildAttributeValue = "1";
                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        _XML.AppendParent();
                    }
                }
                strDevices = _XML.XMLGenerator();
                _XML = null;

                String strBrands = String.Empty;
                if (brands != null)
                {
                    paramBuilder = new StringBuilder();
                    foreach (String brand in brands.SelectedBrands)
                        paramBuilder.Append(brand + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    strBrands = paramBuilder.ToString();
                }

                String strModels = String.Empty;
                if (models != null)
                {
                    paramBuilder = new StringBuilder();
                    foreach (String model in models.SelectedModels)
                        paramBuilder.Append(model + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    strModels = paramBuilder.ToString();
                }

                paramBuilder = new StringBuilder();
                String dataSourceStr = String.Empty;
                if (dataSources != null)
                {
                    foreach (UEI.Workflow2010.Report.Model.DataSources l in dataSources)
                        paramBuilder.Append(l.Name + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    dataSourceStr = paramBuilder.ToString();
                }

                searchParam = new IDSearchParameter();
                searchParam.XMLLocations = strLocations;
                searchParam.XMLDevices = strDevices;
                searchParam.Brands = strBrands;
                searchParam.Models = strModels;
                searchParam.DataSources = dataSourceStr;

                TimeSpan totalExecTime = TimeSpan.Zero;
                ErrorLogging.WriteToLogFile("--Function Name: GetEdIdData--", false);
                ErrorLogging.WriteToLogFile("EDID_Sp_GetEDIDDataEx Input Parameters", false);
                ErrorLogging.WriteToLogFile("##########################################################", false);
                ErrorLogging.WriteToLogFile("####################### Input ############################", false);
                ErrorLogging.WriteToLogFile("##########################################################", false);
                ErrorLogging.WriteToLogFile("LOCATION XML   :" + strLocations, false);
                ErrorLogging.WriteToLogFile("DEVICE XML     :" + strDevices, false);
                ErrorLogging.WriteToLogFile("Brands         :" + strBrands, false);
                ErrorLogging.WriteToLogFile("Models         :" + strModels, false);
                ErrorLogging.WriteToLogFile("##########################################################", false);

                DateTime spStart = DateTime.Now;
                //ErrorLogging.WriteToLogFile("UEI2_Sp_GetAttributesDataEx execution time", false);
                ErrorLogging.WriteToLogFile("EDID_Sp_GetEDIDDataEx execution time", false);
                ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

                EDIDDataAccess m_DBAccess = new EDIDDataAccess();
                HashtableCollection resultCollection = m_DBAccess.GetIDIDData(searchParam);
                if (resultCollection != null)
                {
                    edidData = GetEdIdInfo(resultCollection,ref dtResult, ref m_Message);
                }
                DateTime spEnd = DateTime.Now;
                ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                totalExecTime = spEnd - spStart;
                ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return edidData;
        }        
        private List<XBox> GetEdIdInfo(IList<Hashtable> resultCollection, ref String m_Message, ref String m_Message1)
        {
            List<XBox> edidResultCollection = null;
            Int32 l_Count = 0;
            foreach (Hashtable item in resultCollection)
            {
                l_Count++;

                m_Message = "Reading XBox EDID {" + l_Count.ToString() + "} of {" + resultCollection.Count.ToString() + "}";
                m_Message1 = "Reading RID {" + item["RID"].ToString() + "}";

                XBox edid = new XBox();
                if (item.ContainsKey("MainDevice"))
                {
                    if (!String.IsNullOrEmpty(Convert.ToString(item["MainDevice"])))
                        edid.MainDevice = item["MainDevice"].ToString();
                    else
                        edid.MainDevice = String.Empty;
                }
                else
                    edid.MainDevice = String.Empty;
                if (item.ContainsKey("Brand"))
                {
                    if (!String.IsNullOrEmpty(Convert.ToString(item["Brand"])))
                        edid.Brand = item["Brand"].ToString();
                    else
                        edid.Brand = String.Empty;
                }
                else
                    edid.Brand = String.Empty;
                String _edid = String.Empty;
                if (item.ContainsKey("EDID"))
                {                    
                    _edid = item["EDID"].ToString().TrimEnd();
                    //|0,|1,
                    if (_edid.EndsWith("|0") || _edid.EndsWith("|1"))
                    {
                        _edid = _edid.Remove(_edid.Length - 2, 2);
                    }
                    edid.EdIdRawData = _edid;
                }
                
                try
                {
                    Byte[] _bindata = Convert.FromBase64String(_edid);
                    if (_bindata != null)
                    {
                        List<Byte> xboxEDIDData = new List<Byte>();                        
                        foreach (Byte byteData in _bindata)
                        {
                            xboxEDIDData.Add(byteData);
                        }
                        //Convert to Our bin file Strcuture for EDID Reader
                        Byte[] _bindatatemp = ConvertEdidToBin(xboxEDIDData);
                        if (_bindatatemp != null)
                        {
                            EDIDInfo edidTemp = ReadBinFile(_bindatatemp);
                            edid.EdId = new List<Byte>();
                            edid.EdId = edidTemp.EdId;
                        }                        
                    }
                }
                catch (Exception ex)
                {
                    String str = ex.Message;
                    m_Message = str;
                }
                edid.RID = Int32.Parse(item["RID"].ToString());
                if (edidResultCollection == null)
                    edidResultCollection = new List<XBox>();
                edidResultCollection.Add(edid);
                edid = null;
            }
            return edidResultCollection;
        }
        public String ConvertByteStreamToString(List<Byte> data)
        {
            String _data = String.Empty;
            foreach (Byte b in data)
            {
                String _temp = b.ToString("X");
                _data += _temp + " ";
            }
            return _data;
        }
        public void MigrateandUpdateCapturedEDID()
        {
            this.ErrorMessage = String.Empty;
            try
            {
                DataTable dtCapturedEdId = m_DBAccess.GetCapturedEDID();
                if (dtCapturedEdId != null)
                {
                    foreach (DataRow item in dtCapturedEdId.Rows)
                    {
                        if (item["FileStructure"] != null)
                        {
                            EDIDInfo edidData = new EDIDInfo();

                            Byte[] _binData = (Byte[])item["FileStructure"];
                            EDIDInfo m_edid = ReadBinFile(_binData);
                                                       
                            edidData.EdId = m_edid.EdId;
                            
                            edidData.EdIdRawData = m_edid.EdIdRawData;
                            edidData.OSD = m_edid.OSD;
                            edidData.OSDRawData = m_edid.OSDRawData;
                            edidData.FPBytePosition = m_edid.FPBytePosition;
                            edidData.CustomFPType0x00 = m_edid.CustomFPType0x00;
                            edidData.CustomFPType0x01 = m_edid.CustomFPType0x01;
                            edidData.CustomFPType0x02 = m_edid.CustomFPType0x02;
                            edidData.CustomFPType0x03 = m_edid.CustomFPType0x03;
                            edidData.PhysicalAddress = m_edid.PhysicalAddress;
                                                         
                            String base64String = String.Empty;
                            try
                            {
                                base64String = ConvertEDIDByteStreamToBase64String(m_edid);// System.Convert.ToBase64String(binaryData, 0, binaryData.Length);
                                m_DBAccess.UpdateCapturedEDID(item["RID"].ToString(), base64String, edidData.OSDRawData, edidData.CustomFP, edidData.CustomFPOSD, edidData.Only128FP, edidData.Only128FPOSD, edidData.OnlyOSDFP, edidData.FPBytePosition, edidData.CustomFPType0x00, edidData.CustomFPType0x01, edidData.CustomFPType0x02, edidData.CustomFPType0x03, edidData.PhysicalAddress);
                            }
                            catch { }
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
        }
        public String ConvertEDIDByteStreamToBase64String(EDIDInfo edidData)
        {
            String base64String = String.Empty;
            try
            {
                if (edidData != null)
                {
                    if (edidData.EdId != null)
                    {
                        if (edidData.EdId.Count > 0)
                        {
                            Byte[] binaryData = edidData.EdId.ToArray();
                            base64String = System.Convert.ToBase64String(binaryData, 0, binaryData.Length);
                        }
                    }
                }
            }
            catch { }
            return base64String;
        }
        private List<EDIDInfo> GetEdIdInfo(HashtableCollection resultCollection,ref DataTable dtResult, ref String m_Message)
        {
            List<EDIDInfo> edidResultCollection = null;
            Int32 resultCount = 1;
            m_Message = "Loading EDID";
            DataRow dr = null;
            dtResult = null;
            foreach (Hashtable item in resultCollection)
            {
                try
                {
                    if (dtResult == null)
                    {
                        dtResult = new DataTable("EDID");
                        dtResult.Columns.Add("SlNo");
                        dtResult.Columns.Add("FileStructure", typeof(Byte[]));
                        dtResult.AcceptChanges();
                    }

                    m_Message = "Loading EDID " + resultCount.ToString() + " of " + resultCollection.Count.ToString();
                    EDIDInfo edidData = new EDIDInfo();
                    edidData.SlNo = resultCount++;
                    if (item.ContainsKey("MainDevice"))
                    {
                        if (item["MainDevice"] != null)
                            edidData.MainDevice = item["MainDevice"].ToString();
                        else
                            edidData.MainDevice = String.Empty;
                    }
                    if (item.ContainsKey("SubDevice"))
                    {
                        if (item["SubDevice"] != null)
                            edidData.SubDevice = item["SubDevice"].ToString();
                        else
                            edidData.SubDevice = String.Empty;
                    }
                    if (item.ContainsKey("DeviceComponent"))
                    {
                        if (item["DeviceComponent"] != null)
                            edidData.Component = item["DeviceComponent"].ToString();
                        else
                            edidData.Component = String.Empty;
                    }
                    if (item.ContainsKey("Brand"))
                    {
                        if (item["Brand"] != null)
                            edidData.Brand = item["Brand"].ToString();
                        else
                            edidData.Brand = String.Empty;
                    }
                    if (item.ContainsKey("Model"))
                    {
                        if (item["Model"] != null)
                            edidData.Model = item["Model"].ToString();
                        else
                            edidData.Model = String.Empty;
                    }
                    if (item.ContainsKey("Country"))
                    {
                        if (item["Country"] != null)
                            edidData.Region = item["Country"].ToString();
                        else
                            edidData.Region = String.Empty;
                    }
                    if (item.ContainsKey("DataSource"))
                    {
                        if (item["DataSource"] != null)
                            edidData.DataSource = item["DataSource"].ToString();
                        else
                            edidData.Region = String.Empty;
                    }
                    if (item.ContainsKey("LinkedID"))
                    {
                        if (item["LinkedID"] != null)
                            edidData.DACPublishedID = item["LinkedID"].ToString();
                        else
                            edidData.DACPublishedID = String.Empty;
                    }
                    if (item.ContainsKey("TrustLevel"))
                    {
                        if (!String.IsNullOrEmpty(item["TrustLevel"].ToString()))
                        {
                            String strTrustLevel = item["TrustLevel"].ToString().Trim().Replace("%", "");
                            edidData.TrustLevel = Int32.Parse(strTrustLevel);
                        }
                        else
                            edidData.TrustLevel = 0;
                    }
                    if (item.ContainsKey("FileStructure"))
                    {
                        if (item["FileStructure"] != null)
                        {
                            Byte[] _binData = (Byte[])item["FileStructure"];
                            EDIDInfo m_edid = ReadBinFile(_binData);
                            if (m_edid != null)
                            {
                                edidData.ManufacturerHexCode = m_edid.ManufacturerHexCode;
                                edidData.ManufacturerCode = m_edid.ManufacturerCode;
                                edidData.ManufacturerName = m_edid.ManufacturerName;
                                edidData.HorizontalScreenSize = m_edid.HorizontalScreenSize;
                                edidData.VerticalScreenSize = m_edid.VerticalScreenSize;
                                edidData.ProductCode = m_edid.ProductCode;
                                edidData.SerialNumber = m_edid.SerialNumber;
                                edidData.Week = m_edid.Week;
                                edidData.Year = m_edid.Year;
                                edidData.VideoInputDefinition = m_edid.VideoInputDefinition;
                                edidData.CustomFPType0x00 = m_edid.CustomFPType0x00;
                                edidData.CustomFPType0x01 = m_edid.CustomFPType0x01;
                                edidData.CustomFPType0x02 = m_edid.CustomFPType0x02;
                                edidData.CustomFPType0x03 = m_edid.CustomFPType0x03;
                                edidData.CustomFP = m_edid.CustomFP;
                                edidData.Only128FP = m_edid.Only128FP;
                                edidData.CustomFPOSD = m_edid.CustomFPOSD;
                                edidData.Only128FPOSD = m_edid.Only128FPOSD;
                                edidData.OSD = m_edid.OSD;
                                edidData.OnlyOSDFP = m_edid.OnlyOSDFP;
                                edidData.EdId = m_edid.EdId;
                                edidData.EdIdRawData = m_edid.EdIdRawData;                                
                                edidData.OSDRawData = m_edid.OSDRawData;
                                edidData.FPBytePosition = m_edid.FPBytePosition;
                                edidData.FPAvailable = m_edid.FPAvailable;
                                edidData.BytePositionforFingerPrint = m_edid.BytePositionforFingerPrint;
                                edidData.PhysicalAddress = m_edid.PhysicalAddress;
                            }
                                                        
                        }
                        else
                        {
                            edidData.EdId = new List<Byte>();
                            edidData.EdIdRawData = String.Empty;
                            edidData.OSD = new List<Byte>();
                            edidData.OSDRawData = String.Empty;
                            edidData.CustomFPType0x00 = String.Empty;
                            edidData.CustomFPType0x01 = String.Empty;
                            edidData.CustomFPType0x02 = String.Empty;
                            edidData.CustomFPType0x03 = String.Empty;
                            edidData.CustomFP = String.Empty;
                            edidData.CustomFPOSD = String.Empty;
                            edidData.Only128FP = String.Empty;
                            edidData.Only128FPOSD = String.Empty;
                            edidData.OnlyOSDFP = String.Empty;
                            edidData.FPAvailable = "N";
                            edidData.FPBytePosition = String.Empty;
                            edidData.BytePositionforFingerPrint = null;
                            edidData.ManufacturerCode = String.Empty;
                            edidData.ManufacturerHexCode = String.Empty;
                            edidData.ManufacturerName = String.Empty;
                            edidData.ProductCode = String.Empty;
                            edidData.SerialNumber = String.Empty;
                            edidData.VideoInputDefinition = String.Empty;
                        }
                    }
                    //if (item.ContainsKey("Description"))
                    //{
                    //    String m_Description = String.Empty;
                    //    if (item["Description"] != null)
                    //        m_Description = item["Description"].ToString();
                    //    if (!String.IsNullOrEmpty(m_Description))
                    //    {                        
                    //        String[] data = m_Description.Split(',');
                    //        if (data.Length > 0)
                    //        {
                    //            if (!String.IsNullOrEmpty(data[0]))
                    //                edidData.ManufacturerCode = data[0];
                    //            if (!String.IsNullOrEmpty(data[1]))
                    //                edidData.ManufacturerName = data[1];
                    //            if (!String.IsNullOrEmpty(data[2]))
                    //            {
                    //                Int32 pc = 0;
                    //                if (Int32.TryParse(data[2], out pc))
                    //                    edidData.ProductCode = String.Format("{0:0000}", pc);
                    //                else
                    //                    edidData.ProductCode = data[2];
                    //            }
                    //            if (!String.IsNullOrEmpty(data[3]))
                    //                edidData.SerialNumber = data[3];
                    //            if (!String.IsNullOrEmpty(data[4]))
                    //            {
                    //                Int32 m_Result = 0;
                    //                if (Int32.TryParse(data[4], out m_Result))
                    //                    edidData.Week = m_Result;
                    //            }
                    //            if (!String.IsNullOrEmpty(data[5]))
                    //            {
                    //                Int32 m_Result = 0;
                    //                if (Int32.TryParse(data[5], out m_Result))
                    //                    edidData.Year = m_Result;
                    //            }
                    //            if (data.Length > 6)
                    //            {
                    //                if (!String.IsNullOrEmpty(data[6]))
                    //                {
                    //                    edidData.VideoInputDefinition = data[6];
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    if (edidResultCollection == null)
                        edidResultCollection = new List<EDIDInfo>();
                    edidResultCollection.Add(edidData);

                    dr = dtResult.NewRow();
                    dr[0] = edidData.SlNo;
                    dr[1] = item["FileStructure"];
                    dtResult.Rows.Add(dr);
                    dr = null;
                    edidData = null;
                }
                catch (Exception ex)
                {
                    this.ErrorMessage = ex.Message;
                }
            }      
            return edidResultCollection;
        }
        public List<Int32> FPPosition { get; set; }
        private Boolean ValidateBytePosition(String BytePosition)
        {
            Boolean isValid = false;
            FPPosition = new List<Int32>();
            String[] sep = { ",", ";", " ", "\t" };
            String[] data = BytePosition.Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
            if (data.Length > 0)
            {
                for (Int32 i = 0; i < data.Length; i++)
                {
                    try
                    {
                        FPPosition.Add(Int32.Parse(data[i]));
                        isValid = true;
                    }
                    catch
                    {
                        isValid = false;
                        break;
                    }
                }
            }
            return isValid;
        }
        #endregion

        #region Load Linked ID (ID from Direct Capture EDID)
        public FPIDLinkCollection LoadLinkedID()
        {
            String[] sep = { " " };
            FPIDLinkCollection LinkedIds = null;
            try
            {
                OleDbConnection m_ExcelConnection = null;
                m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "\\LinkedID.xlsx;Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
                OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [LinkedID$]", m_ExcelConnection);
                DataTable dtTable = new DataTable();
                m_ExcelAdapter.Fill(dtTable);
                m_ExcelConnection.Close();
                if (dtTable != null)
                {
                    LinkedIds = new FPIDLinkCollection();
                    foreach (DataRow item in dtTable.Rows)
                    {
                        FPIDLinkData newlinkedid = new FPIDLinkData();
                        //EDID Block
                        if (dtTable.Columns.Contains("EDID Block"))
                        {
                            String tempEDIDData = String.Empty;
                            if (!String.IsNullOrEmpty(item["EDID Block"].ToString()))
                            {
                                tempEDIDData = item["EDID Block"].ToString().Trim();
                            }                           
                            tempEDIDData = tempEDIDData.Trim();
                            if (tempEDIDData.EndsWith("\n"))
                            {
                                tempEDIDData = tempEDIDData.Replace("\n", "");
                            }
                            EDIDInfo newEDIDInfo = new EDIDInfo();
                            newEDIDInfo.EdIdRawData = tempEDIDData;

                            newEDIDInfo.EdId = new List<Byte>();
                            String[] data = tempEDIDData.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                            if (data.Length > 1)
                            {
                                foreach (String bytedata in data)
                                {
                                    newEDIDInfo.EdId.Add((Byte)Convert.ToInt16(bytedata.Trim(), 16));
                                }
                            }
                            Byte[] m_fileStructure = CreateBinFile(newEDIDInfo);
                            EDIDInfo m_edid = ReadBinFile(m_fileStructure);
                            newlinkedid.CustomFPType0x00        = m_edid.CustomFPType0x00;
                            newlinkedid.CustomFPType0x01        = m_edid.CustomFPType0x01;
                            newlinkedid.CustomFPType0x02        = m_edid.CustomFPType0x02;
                            newlinkedid.CustomFPType0x03        = m_edid.CustomFPType0x03;
                            newlinkedid.PhysicalAddress         = m_edid.PhysicalAddress;
                            newlinkedid.ProductCode             = m_edid.ProductCode;
                            newlinkedid.ModelWeek               = m_edid.Week;
                            newlinkedid.ModelYear               = m_edid.Year;
                            newlinkedid.HorizontalScreenSize    = m_edid.HorizontalScreenSize;
                            newlinkedid.VerticalScreenSize      = m_edid.VerticalScreenSize;
                        }
                        if (dtTable.Columns.Contains("CPRRules"))
                        {
                            if (!String.IsNullOrEmpty(item["CPRRules"].ToString()))
                            {
                                newlinkedid.CPRRule = item["CPRRules"].ToString().Trim();
                            }
                        }
                        if (dtTable.Columns.Contains("Control"))
                        {
                            if (!String.IsNullOrEmpty(item["Control"].ToString()))
                            {
                                newlinkedid.Control = item["Control"].ToString().Trim();
                            }
                        }
                        if (dtTable.Columns.Contains("Main Device"))
                        {
                            if (!String.IsNullOrEmpty(item["Main Device"].ToString()))
                            {
                                newlinkedid.MainDevice = item["Main Device"].ToString().Trim();
                            }
                        }
                        if (dtTable.Columns.Contains("Sub Device"))
                        {
                            if (!String.IsNullOrEmpty(item["Sub Device"].ToString()))
                            {
                                newlinkedid.SubDevice = item["Sub Device"].ToString().Trim();
                            }
                        }
                        if (dtTable.Columns.Contains("Brand"))
                        {
                            if (!String.IsNullOrEmpty(item["Brand"].ToString()))
                            {
                                newlinkedid.Brand = item["Brand"].ToString().Trim();
                            }
                        }
                        if (dtTable.Columns.Contains("Model"))
                        {
                            if (!String.IsNullOrEmpty(item["Model"].ToString()))
                            {
                                newlinkedid.Model = item["Model"].ToString().Trim();
                            }
                        }
                        if (newlinkedid.Brand.Contains("Vizio"))
                        {
                            if (dtTable.Columns.Contains("128 FP"))
                            {
                                newlinkedid.Only128FP = item["128 FP"].ToString().Trim();
                            }
                        }
                        else
                        {
                            if (dtTable.Columns.Contains("Custom FP"))
                            {
                                newlinkedid.CustomFP = item["Custom FP"].ToString().Trim();
                            }
                        }
                        if (dtTable.Columns.Contains("UEI2ID"))
                        {
                            if (!String.IsNullOrEmpty(item["UEI2ID"].ToString()))
                            {
                                newlinkedid.UEIID = item["UEI2ID"].ToString().Trim();
                            }
                        }
                        if (dtTable.Columns.Contains("LinkedID"))
                        {
                            if (!String.IsNullOrEmpty(item["LinkedID"].ToString()))
                            {
                                newlinkedid.LinkedID = item["LinkedID"].ToString().Trim();
                            }
                        }
                        if (dtTable.Columns.Contains("Discrete ID"))
                        {
                            if (!String.IsNullOrEmpty(item["Discrete ID"].ToString()))
                            {
                                newlinkedid.DiscreteID = item["Discrete ID"].ToString().Trim();
                            }
                        }
                        if (dtTable.Columns.Contains("Discrete Profile"))
                        {
                            if (!String.IsNullOrEmpty(item["Discrete Profile"].ToString()))
                            {
                                newlinkedid.DiscreteProfile = item["Discrete Profile"].ToString().Trim();
                            }
                        }
                        LinkedIds.Add(newlinkedid);
                    }
                }
            }
            catch { }
            return LinkedIds;
        }
        #endregion

        #region Load Brand Translation(Brand + Manufacturer Code)
        public BrandTranslationCollection LoadBrandTranslation()
        {           
            try
            {
                OleDbConnection m_ExcelConnection = null;
                m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "\\BrandTranslation.xlsx;Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
                OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [BrandMapping$]", m_ExcelConnection);
                DataTable dtTable = new DataTable();
                m_ExcelAdapter.Fill(dtTable);
                m_ExcelConnection.Close();
                if (dtTable != null)
                {
                    this.BrandTranslation = new BrandTranslationCollection();
                    foreach (DataRow item in dtTable.Rows)
                    {
                        BrandTranslationData newbrandtranslation = new BrandTranslationData();                        
                        if (dtTable.Columns.Contains("MainDevice"))
                        {
                            if (!String.IsNullOrEmpty(item["MainDevice"].ToString()))
                            {
                                newbrandtranslation.MainDevice = item["MainDevice"].ToString().Trim();
                            }
                        }
                        if (dtTable.Columns.Contains("SubDevice"))
                        {
                            if (!String.IsNullOrEmpty(item["SubDevice"].ToString()))
                            {

                                newbrandtranslation.SubDevice = item["SubDevice"].ToString().Trim();                               
                            }
                        }
                        if (dtTable.Columns.Contains("Brand"))
                        {
                            if (!String.IsNullOrEmpty(item["Brand"].ToString()))
                            {
                                newbrandtranslation.Brand = item["Brand"].ToString().Trim();
                            }
                        }
                        if (dtTable.Columns.Contains("Manufacturer (3 char) code"))
                        {
                            if (!String.IsNullOrEmpty(item["Manufacturer (3 char) code"].ToString()))
                            {
                                newbrandtranslation.ManufacturerCode = item["Manufacturer (3 char) code"].ToString().Trim();
                            }
                        }                       
                        this.BrandTranslation.Add(newbrandtranslation);
                        newbrandtranslation = null;
                    }
                }
            }
            catch { }
            return this.BrandTranslation;
        }
        #endregion

        #region Read Bin File
        public EDIDInfo ReadBinFile(Byte[] m_FileStructure)
        {
            //if(this.BrandTranslation == null)
            //    LoadBrandTranslation();
            EDIDInfo m_EdId = new EDIDInfo();
            try
            {                
                String[] sep = { " " };                
                EDIDReader m_EdIdReader = new EDIDReader();
                m_EdIdReader.ReadBinFile(m_FileStructure);
                m_EdId.EdIdRawData = m_EdIdReader.GetEdidData();                
                m_EdId.OSDRawData = m_EdIdReader.GetOSDData();
                String[] data = m_EdId.EdIdRawData.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                foreach (String bytedata in data)
                {
                    if (m_EdId.EdId == null)
                        m_EdId.EdId = new List<Byte>();
                    m_EdId.EdId.Add((Byte)Convert.ToInt32(bytedata, 16));
                }
                data = m_EdId.OSDRawData.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                foreach (String bytedata in data)
                {
                    if (m_EdId.OSD == null)
                        m_EdId.OSD = new List<Byte>();
                    m_EdId.OSD.Add((Byte)Convert.ToInt32(bytedata, 16));
                }
                m_EdIdReader.GetEDIDChecksum();
                m_EdIdReader.GetOSDChecksum();
                m_EdIdReader.GetCombinedChecksum();     
                

                m_EdId.FPBytePosition = m_EdIdReader.GetCustomFPBitPositions();
                if (m_EdId.FPBytePosition != null)
                {
                    if (m_EdId.FPBytePosition.ToString().Trim() != String.Empty)
                    {
                        m_EdId.FPAvailable = "Y";
                        ValidateBytePosition(m_EdId);
                    }
                }
                m_EdId.CustomFP = GetFingerPintCheckSum(m_EdId, false);
                if (m_EdId.OSD != null)
                    if (m_EdId.OSD.Count > 0)
                        m_EdId.CustomFPOSD = GetFingerPintCheckSum(m_EdId, true);
                m_EdId.Only128FP = GetFingerPrint(m_EdIdReader.GetEDIDChecksum());
                m_EdId.Only128FPOSD = GetFingerPrint(m_EdIdReader.GetCombinedChecksum());
                m_EdId.OnlyOSDFP = GetFingerPrint(m_EdIdReader.GetOSDChecksum());

                try
                {
                    EDIDParser m_EDIDDecoder = new EDIDParser();
                    EDIDData m_EDIDObject = m_EDIDDecoder.ParseEDID(m_EdId.EdId.ToArray());
                    DecodedEDIDData m_EDID = GetDecodedEDIDDataFromEDIDObject(m_EDIDObject);
                    if (m_EDID != null)
                    {
                        List<Byte> bytePositionforType0 = new List<Byte>();
                        List<Byte> bytePositionforType1 = new List<Byte>();
                        List<Byte> bytePositionforType2 = new List<Byte>();
                        //List<Byte> bytePositionforType3 = new List<Byte>();
                        String[] byteDataforType3 = new String[2];

                        EDIDInfo m_newfpruleedid = new EDIDInfo();
                        m_newfpruleedid.EdId = m_EdId.EdId;
                        m_newfpruleedid.EdIdRawData = m_EdId.EdIdRawData;

                        m_EdId.ManufacturerCode = m_EDID.IDManufacturerName;
                        m_EdId.ManufacturerHexCode = m_EDID.IDManufacturerHexCode;
                        m_EdId.SerialNumber = m_EDID.IDSerialNumber;
                        m_newfpruleedid.ManufacturerCode = m_EDID.IDManufacturerName; ;
                        m_newfpruleedid.ManufacturerHexCode = m_EDID.IDManufacturerHexCode;
                        m_newfpruleedid.PhysicalAddress = m_EDID.PhysicalAddress;
                        m_EdId.PhysicalAddress = m_EDID.PhysicalAddress;


                        //Product Code
                        String temp = m_EDID.IDProductCode.Substring(5, 4);
                        Int32 pc = 0;
                        if (Int32.TryParse(temp, out pc))
                            m_newfpruleedid.ProductCode = String.Format("{0:0000}", pc);
                        else
                            m_newfpruleedid.ProductCode = temp;

                        m_EdId.ProductCode = m_newfpruleedid.ProductCode;

                        //Year of Manufacture
                        if (m_EDID.YearOfManufacture == 0)
                            m_newfpruleedid.Year = (int)m_EDID.ModelYear;
                        else
                            m_newfpruleedid.Year = (int)m_EDID.YearOfManufacture;

                        m_EdId.Year = m_newfpruleedid.Year;

                        m_EdId.ManufacturerName = m_EDID.MonitorName;
                        m_newfpruleedid.ManufacturerName = m_EDID.MonitorName;

                        if (m_EDID.AnalogVideoSignalInterfaceSupport == 'Y')
                        {
                            m_newfpruleedid.VideoInputDefinition = "Analog";
                        }
                        else if (m_EDID.DigitalVideoSignalInterfaceSupport == 'Y')
                        {
                            m_newfpruleedid.VideoInputDefinition = "Digital";
                        }
                        m_EdId.VideoInputDefinition = m_newfpruleedid.VideoInputDefinition;

                        m_newfpruleedid.VerticalScreenSize = (int)m_EDID.VerticalScreenSize;
                        m_EdId.VerticalScreenSize = m_newfpruleedid.VerticalScreenSize;

                        m_newfpruleedid.HorizontalScreenSize = (int)m_EDID.HorizontalScreenSize;
                        m_EdId.HorizontalScreenSize = m_newfpruleedid.HorizontalScreenSize;

                        //Manufacturer ID (8-9), Product Code (10-11), Year of Manufacture (17)
                        bytePositionforType0.Add(m_newfpruleedid.EdId[8]);
                        bytePositionforType0.Add(m_newfpruleedid.EdId[9]);
                        bytePositionforType0.Add(m_newfpruleedid.EdId[10]);
                        bytePositionforType0.Add(m_newfpruleedid.EdId[11]);
                        bytePositionforType0.Add(m_newfpruleedid.EdId[17]);

                        bytePositionforType1.Add(m_newfpruleedid.EdId[8]);
                        bytePositionforType1.Add(m_newfpruleedid.EdId[9]);
                        bytePositionforType1.Add(m_newfpruleedid.EdId[10]);
                        bytePositionforType1.Add(m_newfpruleedid.EdId[11]);
                        bytePositionforType1.Add(m_newfpruleedid.EdId[17]);

                        bytePositionforType2.Add(m_newfpruleedid.EdId[8]);
                        bytePositionforType2.Add(m_newfpruleedid.EdId[9]);
                        bytePositionforType2.Add(m_newfpruleedid.EdId[10]);
                        bytePositionforType2.Add(m_newfpruleedid.EdId[11]);
                        bytePositionforType2.Add(m_newfpruleedid.EdId[17]);

                        //bytePositionforType3.Add(m_newfpruleedid.EdId[8]);
                        //bytePositionforType3.Add(m_newfpruleedid.EdId[9]);                        

                        //Monitor Name(s) 
                        //Parsed from monitor descriptor fields at positions 54-71, 72-89, 90-105 and 106-125.
                        //Monitor Range Limits Descriptor(s)
                        //Parsed from monitor descriptor fields at positions 54-71, 72-89, 90-105 and 106-125.
                        if (!String.IsNullOrEmpty(m_EDID.MonitorName))
                        {
                            if (m_EDIDObject.GetDescriptor1_Type() == "Monitor Name")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorByteDescriptor1();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType1.Add(descriptor[i]);
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                            if (m_EDIDObject.GetDescriptor2_Type() == "Monitor Name")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorByteDescriptor2();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType1.Add(descriptor[i]);
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                            if (m_EDIDObject.GetDescriptor3_Type() == "Monitor Name")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorByteDescriptor3();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType1.Add(descriptor[i]);
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                            if (m_EDIDObject.GetDescriptor4_Type() == "Monitor Name")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorByteDescriptor4();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType1.Add(descriptor[i]);
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                        }
                        else
                        {
                            bytePositionforType1.Add(0xA);
                            bytePositionforType2.Add(0xA);
                        }

                        //Basic display Parameters (20-24) : 5
                        for (int i = 20; i <= 24; i++)
                        {
                            bytePositionforType2.Add(m_newfpruleedid.EdId[i]);
                        }
                        //Color characteristics (25-34) : 10
                        for (int i = 25; i <= 34; i++)
                        {
                            bytePositionforType2.Add(m_newfpruleedid.EdId[i]);
                        }
                        //Established timings (35-37)   : 3
                        for (int i = 35; i <= 37; i++)
                        {
                            bytePositionforType2.Add(m_newfpruleedid.EdId[i]);
                        }
                        //Standard timings (38-53)  : 16
                        for (int i = 38; i <= 53; i++)
                        {
                            bytePositionforType2.Add(m_newfpruleedid.EdId[i]);
                        }
                        if ((m_EDIDObject.GetDescriptor1_Type() != "Monitor range limits") && (m_EDIDObject.GetDescriptor2_Type() != "Monitor range limits") && (m_EDIDObject.GetDescriptor3_Type() != "Monitor range limits") && (m_EDIDObject.GetDescriptor4_Type() != "Monitor range limits"))
                        {
                            bytePositionforType2.Add(0xA);
                        }
                        else
                        {
                            if (m_EDIDObject.GetDescriptor1_Type() == "Monitor range limits")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorRangeLimitByteDescriptor1();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                            if (m_EDIDObject.GetDescriptor2_Type() == "Monitor range limits")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorRangeLimitByteDescriptor2();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                            if (m_EDIDObject.GetDescriptor3_Type() == "Monitor range limits")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorRangeLimitByteDescriptor3();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                            if (m_EDIDObject.GetDescriptor4_Type() == "Monitor range limits")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorRangeLimitByteDescriptor4();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                        }

                        if (m_EDIDObject.GetExtendedDataBlock())
                        {
                            //N1.0.0.0,     with N1 ≠ 0	            00
                            //N1.N2.x.x,    with N1,2 ≠ 0	        80
                            //0.0.0.0 or    invalid address	        80
                            //              No extended data block	00

                            if (!String.IsNullOrEmpty(m_EdId.PhysicalAddress))
                            {
                                String[] parts = m_EdId.PhysicalAddress.Split('.');
                                if (parts.Length == 4)
                                {
                                    if (parts[0] == "0" && parts[1] == "0" && parts[2] == "0" && parts[3] == "0")
                                    {
                                        byteDataforType3[0] = "80";
                                        //bytePositionforType3.Add(0x80);
                                    }
                                    else if (parts[0] != "0" && parts[1] == "0" && parts[2] == "0" && parts[3] == "0")
                                    {
                                        byteDataforType3[0] = "00";
                                        //bytePositionforType3.Add(0X00);
                                    }
                                    else if (parts[0] != "0" && parts[1] != "0" && parts[2] == "0" && parts[3] == "0")
                                    {
                                        byteDataforType3[0] = "80";
                                        //bytePositionforType3.Add(0x80);
                                    }
                                    else if (parts[0] != "0" && parts[1] != "0" && parts[2] != "0" && parts[3] == "0")
                                    {
                                        byteDataforType3[0] = "80";
                                        //bytePositionforType3.Add(0x80);
                                    }
                                    else if (parts[0] != "0" && parts[1] != "0" && parts[2] != "0" && parts[3] != "0")
                                    {
                                        byteDataforType3[0] = "80";
                                        //bytePositionforType3.Add(0x80);
                                    }
                                }
                            }
                            else
                            {
                                //Extended DataBlock exists but Block1 doesn't exists
                                //No extended data block "00"                                
                                //bytePositionforType3.Add(0X00);                                
                                byteDataforType3[0] = "00";
                            }
                            //bytePositionforType3.Add(0X00);
                            byteDataforType3[1] = "00";
                        }
                        else
                        {
                            //No extended data block "00"
                            //bytePositionforType3.Add(0X00);
                            //bytePositionforType3.Add(0X00);                            
                            byteDataforType3[0] = "00";
                            byteDataforType3[1] = "00";
                        }

                        //Type 0        
                        if (bytePositionforType0.Count > 0)
                        {
                            try
                            {
                                ICSharpCode.SharpZipLib.Checksums.Crc32 crc_32 = new ICSharpCode.SharpZipLib.Checksums.Crc32();
                                crc_32.Update(bytePositionforType0.ToArray(), 0, bytePositionforType0.Count);
                                m_EdId.CustomFPType0x00 = "0X" + crc_32.Value.ToString("X8");
                            }
                            catch { }
                        }

                        //Type 1   
                        if (bytePositionforType1.Count > 0)
                        {
                            try
                            {
                                ICSharpCode.SharpZipLib.Checksums.Crc32 crc_32 = new ICSharpCode.SharpZipLib.Checksums.Crc32();
                                crc_32.Update(bytePositionforType1.ToArray(), 0, bytePositionforType1.Count);
                                m_EdId.CustomFPType0x01 = "0X" + crc_32.Value.ToString("X8");
                            }
                            catch { }
                        }

                        //Type 2
                        if (bytePositionforType2.Count > 0)
                        {
                            try
                            {
                                ICSharpCode.SharpZipLib.Checksums.Crc32 crc_32 = new ICSharpCode.SharpZipLib.Checksums.Crc32();
                                crc_32.Update(bytePositionforType2.ToArray(), 0, bytePositionforType2.Count);
                                m_EdId.CustomFPType0x02 = "0X" + crc_32.Value.ToString("X8");
                            }
                            catch { }
                        }
                        //Type 3
                        //if (bytePositionforType3.Count > 0)
                        //{
                        try
                        {
                            //ICSharpCode.SharpZipLib.Checksums.Crc32 crc_32 = new ICSharpCode.SharpZipLib.Checksums.Crc32();
                            //crc_32.Update(bytePositionforType3.ToArray(), 0, bytePositionforType3.Count);
                            //m_EdId.CustomFPType0x03 = "0X" + crc_32.Value.ToString("X8");
                            if (!String.IsNullOrEmpty(m_EdId.ManufacturerHexCode))
                            {
                                String[] strMfgCode = m_EdId.ManufacturerHexCode.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                foreach (String item in strMfgCode)
                                {
                                    m_EdId.CustomFPType0x03 += item;
                                }
                                foreach (String item in byteDataforType3)
                                {
                                    m_EdId.CustomFPType0x03 += item;
                                }
                                try
                                {
                                    m_EdId.CustomFPType0x03 = "0X" + m_EdId.CustomFPType0x03;
                                }
                                catch (Exception ex)
                                {
                                    String s1 = ex.Message;
                                }
                                
                            }
                        }
                        catch (Exception ex) { String str = ex.Message; }
                        //}
                    }                    
                }
                catch { }

            }
            catch { }
            return m_EdId;
        }        
        #endregion

        #region Validate Position
        public Boolean ValidateBytePosition(EDIDInfo m_EdId)
        {
            Boolean isValid = false;
            m_EdId.BytePositionforFingerPrint = new List<Int32>();
            String[] sep = { ",", ";", " ", "\t" };
            String[] data = m_EdId.FPBytePosition.Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
            if (data.Length > 0)
            {
                for (Int32 i = 0; i < data.Length; i++)
                {
                    try
                    {
                        m_EdId.BytePositionforFingerPrint.Add(Int32.Parse(data[i]));
                        isValid = true;
                    }
                    catch
                    {
                        isValid = false;
                        break;
                    }
                }
            }
            return isValid;
        }
        #endregion

        #region Get Custom FP Bytes
        public List<Byte> GetCustomFPBytes(EDIDInfo m_EdId, Boolean withOSD)
        {
            List<Byte> _customfpdata = new List<Byte>();
            if (m_EdId.BytePositionforFingerPrint != null && m_EdId.EdId != null)
            {
                if (m_EdId.BytePositionforFingerPrint.Count > 0)
                {
                    foreach (Int32 by in m_EdId.BytePositionforFingerPrint)
                    {
                        _customfpdata.Add(m_EdId.EdId[by]);
                    }
                }
            }
            if (m_EdId.OSD != null && withOSD == true)
            {
                foreach (Byte by in m_EdId.OSD)
                {
                    _customfpdata.Add(by);
                }
            }
            return _customfpdata;
        }
        #endregion

        #region Convert Edid To Bin
        public Byte[] ConvertEdidToBin(List<Byte> byteStream)
        {
            EDIDWriter m_EdIdWriter = new EDIDWriter();
            m_EdIdWriter._combinedchecksum = false;
            m_EdIdWriter._isCustomFPAvailable = false;
            m_EdIdWriter.SetHeader("EDOSD", 1, 0);
            m_EdIdWriter.SetTocHeader("TC");
            m_EdIdWriter.SetTocCollection("EDID");
            m_EdIdWriter.SetTocCollection("EDCHKSM");
            //if (m_EdId.OSD != null)
            //    if (m_EdId.OSD.Count > 0)
            //    {
            //        m_EdIdWriter.SetTocCollection("OSD");
            //        m_EdIdWriter.SetTocCollection("OSDCHKSM");
            //    }
            //if (m_EdId.EdId != null && m_EdId.OSD != null)
            //    if (m_EdId.EdId.Count > 0 && m_EdId.OSD.Count > 0)
            //    {
            //        m_EdIdWriter._combinedchecksum = true;
            //        m_EdIdWriter.SetTocCollection("CHKSM");
            //    }
            //if (m_EdId.FPAvailable == "Y" || m_EdId.FPAvailable == "y")
            //{
            //    m_EdIdWriter._isCustomFPAvailable = true;
            //    m_EdIdWriter.SetTocCollection("FPBYDT");
            //    m_EdIdWriter.SetTocCollection("FP");
            //}

            m_EdIdWriter.SetEdidTocHeader("EDID");
            m_EdIdWriter.SetEdidTocCollection("HI", 20);
            m_EdIdWriter.SetEdidTocCollection("BD", 5);
            m_EdIdWriter.SetEdidTocCollection("CC", 10);
            m_EdIdWriter.SetEdidTocCollection("ET", 3);
            m_EdIdWriter.SetEdidTocCollection("ST", 16);
            m_EdIdWriter.SetEdidTocCollection("D1", 18);
            m_EdIdWriter.SetEdidTocCollection("D2", 18);
            m_EdIdWriter.SetEdidTocCollection("D3", 18);
            m_EdIdWriter.SetEdidTocCollection("D4", 18);
            m_EdIdWriter.SetEdidTocCollection("EX", 1);
            m_EdIdWriter.SetEdidTocCollection("CS", 1);

            m_EdIdWriter.SetEdidChecksumHeader("EDCHKSM");
            m_EdIdWriter.SetEdidRawData(byteStream);

            List<Byte> m_cchecksm = new List<Byte>();
            if (byteStream != null)
            {
                //Take only 128 Byte
                Int32 count128ByteOnly = 0;
                foreach (Byte by in byteStream)
                {
                    m_cchecksm.Add(by);                    
                    count128ByteOnly++;
                    if (count128ByteOnly == 128)
                        break;
                }
                //m_EdIdWriter.CalculateEdidChecksum(byteStream);
                m_EdIdWriter.CalculateEdidChecksum(m_cchecksm);
            }
            //if (m_EdId.OSD != null)
            //    if (m_EdId.OSD.Count > 0)
            //    {
            //        m_EdIdWriter.SetOSDTocHeader("OSD");
            //        m_EdIdWriter.SetOSDChecksumHeader("OSDCHKSM");
            //        m_EdIdWriter.SetOSDRawData(m_EdId.OSD);
            //        foreach (Byte by in m_EdId.OSD)
            //        {
            //            m_cchecksm.Add(by);
            //        }
            //        m_EdIdWriter.CalculateOSDChecksum(m_EdId.OSD);
            //    }
            //if (m_EdId.EdId != null && m_EdId.OSD != null)
            //    if (m_EdId.EdId.Count > 0 && m_EdId.OSD.Count > 0)
            //    {
            //        m_EdIdWriter.SetCombinedChecksumHeader("CHKSM");
            //        m_EdIdWriter.CalculateCombinedChecksum(m_cchecksm);
            //    }
            //if (m_EdId.FPAvailable == "Y" || m_EdId.FPAvailable == "y")
            //{
            //    m_EdIdWriter.SetCustomFPBitHeader("FPBYDT", m_EdId.FPBytePosition);
            //    m_EdIdWriter.SetCustomFPHeader("FP");
            //    m_EdIdWriter.CalculateCustomFP(GetCustomFPBytes(m_EdId));
            //}
            m_EdIdWriter.CalculateAddress();
            List<Byte> m_bytes = m_EdIdWriter.GetBytes();

            //System.IO.File.WriteAllBytes(@"C:\Working\EDIDBin.bin", m_bytes.ToArray());

            return m_bytes.ToArray();
        }
        #endregion

        #region Create Bin File
        public Byte[] CreateBinFile(EDIDInfo m_EdId)
        {
            EDIDWriter m_EdIdWriter = new EDIDWriter();
            m_EdIdWriter._combinedchecksum = false;
            m_EdIdWriter._isCustomFPAvailable = false;
            m_EdIdWriter.SetHeader("EDOSD", 1, 0);
            m_EdIdWriter.SetTocHeader("TC");
            m_EdIdWriter.SetTocCollection("EDID");
            m_EdIdWriter.SetTocCollection("EDCHKSM");
            if (m_EdId.OSD != null)
                if (m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter.SetTocCollection("OSD");
                    m_EdIdWriter.SetTocCollection("OSDCHKSM");
                }
            if (m_EdId.EdId != null && m_EdId.OSD != null)
                if (m_EdId.EdId.Count > 0 && m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter._combinedchecksum = true;
                    m_EdIdWriter.SetTocCollection("CHKSM");
                }
            if (m_EdId.FPAvailable == "Y" || m_EdId.FPAvailable == "y")
            {
                m_EdIdWriter._isCustomFPAvailable = true;
                m_EdIdWriter.SetTocCollection("FPBYDT");
                m_EdIdWriter.SetTocCollection("FP");
            }

            m_EdIdWriter.SetEdidTocHeader("EDID");
            m_EdIdWriter.SetEdidTocCollection("HI", 20);
            m_EdIdWriter.SetEdidTocCollection("BD", 5);
            m_EdIdWriter.SetEdidTocCollection("CC", 10);
            m_EdIdWriter.SetEdidTocCollection("ET", 3);
            m_EdIdWriter.SetEdidTocCollection("ST", 16);
            m_EdIdWriter.SetEdidTocCollection("D1", 18);
            m_EdIdWriter.SetEdidTocCollection("D2", 18);
            m_EdIdWriter.SetEdidTocCollection("D3", 18);
            m_EdIdWriter.SetEdidTocCollection("D4", 18);
            m_EdIdWriter.SetEdidTocCollection("EX", 1);
            m_EdIdWriter.SetEdidTocCollection("CS", 1);

            m_EdIdWriter.SetEdidChecksumHeader("EDCHKSM");
            m_EdIdWriter.SetEdidRawData(m_EdId.EdId);

            List<Byte> m_cchecksm = new List<Byte>();
            if (m_EdId.EdId != null)
            {
                Int32 count128ByteOnly = 0;
                foreach (Byte by in m_EdId.EdId)
                {
                    m_cchecksm.Add(by);                    
                    count128ByteOnly++;
                    if (count128ByteOnly == 128)
                        break;
                }
                m_EdIdWriter.CalculateEdidChecksum(m_cchecksm);
            }
            if (m_EdId.OSD != null)
                if (m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter.SetOSDTocHeader("OSD");
                    m_EdIdWriter.SetOSDChecksumHeader("OSDCHKSM");
                    m_EdIdWriter.SetOSDRawData(m_EdId.OSD);
                    foreach (Byte by in m_EdId.OSD)
                    {
                        m_cchecksm.Add(by);
                    }
                    m_EdIdWriter.CalculateOSDChecksum(m_EdId.OSD);
                }
            if (m_EdId.EdId != null && m_EdId.OSD != null)
                if (m_EdId.EdId.Count > 0 && m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter.SetCombinedChecksumHeader("CHKSM");
                    m_EdIdWriter.CalculateCombinedChecksum(m_cchecksm);
                }
            if (m_EdId.FPAvailable == "Y" || m_EdId.FPAvailable == "y")
            {
                m_EdIdWriter.SetCustomFPBitHeader("FPBYDT", m_EdId.FPBytePosition);
                m_EdIdWriter.SetCustomFPHeader("FP");
                m_EdIdWriter.CalculateCustomFP(GetCustomFPBytes(m_EdId,true));
            }
            m_EdIdWriter.CalculateAddress();
            List<Byte> m_bytes = m_EdIdWriter.GetBytes();

            //System.IO.File.WriteAllBytes(@"C:\Working\EDIDBin.bin", m_bytes.ToArray());

            return m_bytes.ToArray();
        }
        public Byte[] CreateBinFile(XBox m_EdId)
        {
            EDIDWriter m_EdIdWriter = new EDIDWriter();
            m_EdIdWriter._combinedchecksum = false;
            m_EdIdWriter._isCustomFPAvailable = false;
            m_EdIdWriter.SetHeader("EDOSD", 1, 0);
            m_EdIdWriter.SetTocHeader("TC");
            m_EdIdWriter.SetTocCollection("EDID");
            m_EdIdWriter.SetTocCollection("EDCHKSM");

            m_EdIdWriter.SetEdidTocHeader("EDID");
            m_EdIdWriter.SetEdidTocCollection("HI", 20);
            m_EdIdWriter.SetEdidTocCollection("BD", 5);
            m_EdIdWriter.SetEdidTocCollection("CC", 10);
            m_EdIdWriter.SetEdidTocCollection("ET", 3);
            m_EdIdWriter.SetEdidTocCollection("ST", 16);
            m_EdIdWriter.SetEdidTocCollection("D1", 18);
            m_EdIdWriter.SetEdidTocCollection("D2", 18);
            m_EdIdWriter.SetEdidTocCollection("D3", 18);
            m_EdIdWriter.SetEdidTocCollection("D4", 18);
            m_EdIdWriter.SetEdidTocCollection("EX", 1);
            m_EdIdWriter.SetEdidTocCollection("CS", 1);

            m_EdIdWriter.SetEdidChecksumHeader("EDCHKSM");
            m_EdIdWriter.SetEdidRawData(m_EdId.EdId);

            List<Byte> m_cchecksm = new List<Byte>();
            if (m_EdId.EdId != null)
            {
                Int32 count128ByteOnly = 0;
                foreach (Byte by in m_EdId.EdId)
                {
                    m_cchecksm.Add(by);
                    count128ByteOnly++;
                    if (count128ByteOnly == 128)
                        break;
                }
                m_EdIdWriter.CalculateEdidChecksum(m_cchecksm);
            }
            m_EdIdWriter.CalculateAddress();
            List<Byte> m_bytes = m_EdIdWriter.GetBytes();

            //System.IO.File.WriteAllBytes(@"C:\Working\EDIDBin.bin", m_bytes.ToArray());

            return m_bytes.ToArray();
        }        
        #endregion

        #region Get Fingerprint
        public Dictionary<String,Dictionary<String,String>> DeviceBrandBytePosition { get; set; }
        public void LoadDeviceBrandBasedBytePositions()
        {
            this.ErrorMessage = String.Empty;
            try
            {
                if (this.DeviceBrandBytePosition == null)
                    this.DeviceBrandBytePosition = new Dictionary<String, Dictionary<String, String>>();
                OleDbConnection m_ExcelConnection = null;
                m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "\\DeviceBrandBasedBytePosition.xlsx;Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
                OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [BytePosition$]", m_ExcelConnection);
                DataTable dtTable = new DataTable();
                m_ExcelAdapter.Fill(dtTable);
                m_ExcelConnection.Close();
                if (dtTable != null)
                {
                    foreach (DataRow item in dtTable.Rows)
                    {
                        if (!this.DeviceBrandBytePosition.ContainsKey(item["MainDevice"].ToString().Trim().ToLower()))
                        {
                            this.DeviceBrandBytePosition.Add(item["MainDevice"].ToString().Trim().ToLower(), new Dictionary<String, String>());
                        }
                        if (!this.DeviceBrandBytePosition[item["MainDevice"].ToString().Trim().ToLower()].ContainsKey(item["Brand"].ToString().Trim().ToLower()))
                        {
                            if (!String.IsNullOrEmpty(item["FP Position"].ToString()))
                                this.DeviceBrandBytePosition[item["MainDevice"].ToString().Trim().ToLower()].Add(item["Brand"].ToString().Trim().ToLower(), item["FP Position"].ToString());
                            else
                                this.DeviceBrandBytePosition[item["MainDevice"].ToString().Trim().ToLower()].Add(item["Brand"].ToString().Trim().ToLower(), String.Empty);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
                ErrorLogging.WriteToErrorLogFile(ex);
            }
        }
        public Dictionary<String,String> GetBytePositionBasedonBrands(String strMainDevice,String strBrand)
        {
            Dictionary<String, String> position = new Dictionary<String, String>();
            this.ErrorMessage = String.Empty;
            try
            {                
                if ((strMainDevice != null) && (strBrand != null))
                {
                    strMainDevice = strMainDevice.Trim().ToLower();
                    strBrand = strBrand.Trim().ToLower();
                }
                else
                {
                    position.Add("N", "");
                    return position;
                }
                if (this.DeviceBrandBytePosition == null)
                    LoadDeviceBrandBasedBytePositions();

                if (this.DeviceBrandBytePosition != null)
                {
                    if (this.DeviceBrandBytePosition.ContainsKey(strMainDevice))
                    {
                        if (this.DeviceBrandBytePosition[strMainDevice].ContainsKey(strBrand))
                        {
                            String strTemp = this.DeviceBrandBytePosition[strMainDevice][strBrand];
                            if (!String.IsNullOrEmpty(strTemp))
                                position.Add("Y", strTemp);
                            else
                                position.Add("N", "");
                        }
                    }
                }

                /*
                if (strMainDevice != null)
                {
                    strMainDevice = strMainDevice.Trim().ToLower();
                    switch (strMainDevice)
                    {
                        case "tv":
                            #region for TV
                            if (strBrand != null)
                            {
                                strBrand = strBrand.Trim().ToLower();
                                switch (strBrand)
                                {
                                    case "samsung":
                                        position.Add("Y", "8,9,10,11,17,21,22");
                                        break;
                                    case "philips":
                                        position.Add("Y", "8,9,10,11,17,21,22");
                                        break;
                                    case "sony":
                                        position.Add("Y", "8,9,10,11,17,21,22");
                                        break;
                                    case "lg":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;
                                    case "toshiba":
                                        position.Add("Y", "8,9,10,11,17,21,22");
                                        break;
                                    case "panasonic":
                                        position.Add("Y", "8,9,10,11,17,21,22");
                                        break;
                                    case "sharp":
                                        position.Add("Y", "8,9,10,11,17,21,22");
                                        break;
                                    case "rca":
                                        position.Add("Y", "8,9,10,11,17,21,22");
                                        break;
                                    case "sanyo":
                                        position.Add("Y", "8,9,10,11,17,21,22");
                                        break;
                                    case "coby":
                                        position.Add("Y", "8,9,10,11,17,21,22");
                                        break;
                                    case "insignia":
                                        position.Add("Y", "8,9,10,11,17,21,22");
                                        break;
                                    case "jvc":
                                        position.Add("Y", "8,9,10,11,17,21,22");
                                        break;
                                    default:
                                        position.Add("N", "");
                                        break;
                                }
                            }
                            #endregion
                            break;
                        case "audio":
                            #region for Audio
                            if (strBrand != null)
                            {
                                strBrand = strBrand.Trim().ToLower();
                                switch (strBrand)
                                {
                                    case "denon":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;
                                    case "marantz":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;
                                    case "sony":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;
                                    case "onkyo":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;
                                    case "pioneer":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;
                                    case "yamaha":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;                               
                                    default:
                                        position.Add("N", "");
                                        break;
                                }
                            }
                            #endregion
                            break;
                        case "set top box":
                            #region for Set Top Box
                            if (strBrand != null)
                            {
                                strBrand = strBrand.Trim().ToLower();
                                switch (strBrand)
                                {
                                    case "samsung":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;
                                    case "philips":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;
                                    case "sony":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;
                                    case "lg":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;
                                    case "toshiba":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;
                                    case "panasonic":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;
                                    case "sharp":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;                             
                                    case "jbl":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;
                                    case "yamaha":
                                        position.Add("Y", "8,9,10,11,17");
                                        break;  
                                    default:
                                        position.Add("N", "");
                                        break;
                                }
                            }
                            #endregion
                            break;
                        default:
                            position.Add("N", "");
                            break;
                    }
                }
                */
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                this.ErrorMessage = ex.Message;
            }
            return position;
        }
        public String GetFingerPrint(String byteData)
        {
            StringBuilder newFPData = new StringBuilder();
            String[] sep = { " " };
            if (byteData.Length > 0)
            {
                List<Byte> newData = new List<Byte>();
                String[] data = byteData.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                for (int i = data.Length - 1; i > -1; i--)
                {
                    newData.Add((Byte)Convert.ToInt32(data[i], 16));
                }
                newFPData.Append("0X");
                //foreach (Byte b in newData.ToArray())
                //{
                //    if (b != 0)
                //        newFPData.AppendFormat("{0:x2}", b);
                //}
                for (int i = 4; i < 8; i++)
                {
                    newFPData.AppendFormat("{0:x2}", newData[i]);
                }
            }
            return newFPData.ToString().ToUpper();
        }
        public String GetFingerPintCheckSum(EDIDInfo m_EdId, Boolean withOSD)
        {
            String _chksm = String.Empty;
            EDIDWriter m_EdIdWriter = new EDIDWriter();
            m_EdIdWriter._combinedchecksum = false;
            m_EdIdWriter._isCustomFPAvailable = false;
            m_EdIdWriter.SetHeader("EDOSD", 1, 0);
            m_EdIdWriter.SetTocHeader("TC");
            m_EdIdWriter.SetTocCollection("EDID");
            m_EdIdWriter.SetTocCollection("EDCHKSM");
            if (m_EdId.OSD != null)
                if (m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter.SetTocCollection("OSD");
                    m_EdIdWriter.SetTocCollection("OSDCHKSM");
                }
            if (m_EdId.EdId != null && m_EdId.OSD != null)
                if (m_EdId.EdId.Count > 0 && m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter._combinedchecksum = true;
                    m_EdIdWriter.SetTocCollection("CHKSM");
                }
            if (m_EdId.FPAvailable == "Y" || m_EdId.FPAvailable == "y")
            {
                m_EdIdWriter._isCustomFPAvailable = true;
                m_EdIdWriter.SetTocCollection("FPBYDT");
                m_EdIdWriter.SetTocCollection("FP");
            }

            m_EdIdWriter.SetEdidTocHeader("EDID");
            m_EdIdWriter.SetEdidTocCollection("HI", 20);
            m_EdIdWriter.SetEdidTocCollection("BD", 5);
            m_EdIdWriter.SetEdidTocCollection("CC", 10);
            m_EdIdWriter.SetEdidTocCollection("ET", 3);
            m_EdIdWriter.SetEdidTocCollection("ST", 16);
            m_EdIdWriter.SetEdidTocCollection("D1", 18);
            m_EdIdWriter.SetEdidTocCollection("D2", 18);
            m_EdIdWriter.SetEdidTocCollection("D3", 18);
            m_EdIdWriter.SetEdidTocCollection("D4", 18);
            m_EdIdWriter.SetEdidTocCollection("EX", 1);
            m_EdIdWriter.SetEdidTocCollection("CS", 1);

            m_EdIdWriter.SetEdidChecksumHeader("EDCHKSM");
            m_EdIdWriter.SetEdidRawData(m_EdId.EdId);

            List<Byte> m_cchecksm = new List<Byte>();
            if (m_EdId.EdId != null)
            {
                Int32 count128ByteOnly = 0;
                foreach (Byte by in m_EdId.EdId)
                {
                    m_cchecksm.Add(by);
                    count128ByteOnly++;
                    if (count128ByteOnly == 128)
                        break;
                }
                m_EdIdWriter.CalculateEdidChecksum(m_cchecksm);
            }
            if (m_EdId.OSD != null)
                if (m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter.SetOSDTocHeader("OSD");
                    m_EdIdWriter.SetOSDChecksumHeader("OSDCHKSM");
                    m_EdIdWriter.SetOSDRawData(m_EdId.OSD);
                    foreach (Byte by in m_EdId.OSD)
                    {
                        m_cchecksm.Add(by);

                    }
                    m_EdIdWriter.CalculateOSDChecksum(m_EdId.OSD);
                }
            if (m_EdId.EdId != null && m_EdId.OSD != null)
                if (m_EdId.EdId.Count > 0 && m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter.SetCombinedChecksumHeader("CHKSM");
                    m_EdIdWriter.CalculateCombinedChecksum(m_cchecksm);
                }
            if (m_EdId.FPAvailable == "Y" || m_EdId.FPAvailable == "y")
            {
                m_EdIdWriter.SetCustomFPBitHeader("FPBYDT", m_EdId.FPBytePosition);
                m_EdIdWriter.SetCustomFPHeader("FP");
                m_EdIdWriter.CalculateCustomFP(GetCustomFPBytes(m_EdId, withOSD));

                if (m_EdIdWriter._customFp.stringCheckSum.Length > 0)
                {
                    _chksm = "0X" + m_EdIdWriter._customFp.stringCheckSum;
                }
            }
            return _chksm;
        }
        #endregion

        #region Pupolate EDID Data
        public EDID PopulateEdid(List<Byte> data)
        {
            EDID EDIDDetails = new EDID();
            try
            {
                EDIDDetails.EDIDData.Data = data;
                for (int i = 0; i <= 7; i++)
                {
                    EDIDDetails.Header.Data.Add(data[i]);
                }
                for (int i = 8; i <= 9; i++)
                {
                    EDIDDetails.Manufacturer.Data.Add(data[i]);
                }
                for (int i = 10; i <= 11; i++)
                {
                    EDIDDetails.ProductCode.Data.Add(data[i]);
                }
                for (int i = 12; i <= 15; i++)
                {
                    EDIDDetails.SerialNumber.Data.Add(data[i]);
                }
                for (int i = 16; i <= 16; i++)
                {
                    EDIDDetails.WOfMfg.Data.Add(data[i]);
                }
                for (int i = 17; i <= 17; i++)
                {
                    EDIDDetails.YOfMfg.Data.Add(data[i]);
                }
                for (int i = 18; i <= 18; i++)
                {
                    EDIDDetails.EDIDVerNo.Data.Add(data[i]);
                }
                for (int i = 19; i <= 19; i++)
                {
                    EDIDDetails.EDIDRevVerNo.Data.Add(data[i]);
                }
                for (int i = 20; i <= 20; i++)
                {
                    EDIDDetails.VideoIn.Data.Add(data[i]);
                }
                for (int i = 21; i <= 21; i++)
                {
                    EDIDDetails.MaxHorImg.Data.Add(data[i]);
                }
                for (int i = 22; i <= 22; i++)
                {
                    EDIDDetails.MaxVerImg.Data.Add(data[i]);
                }
                for (int i = 23; i <= 23; i++)
                {
                    EDIDDetails.DispGamma.Data.Add(data[i]);
                }
                for (int i = 24; i <= 24; i++)
                {
                    EDIDDetails.PowerAndSupport.Data.Add(data[i]);
                }
                for (int i = 25; i <= 34; i++)
                {
                    EDIDDetails.ColorChar.Data.Add(data[i]);
                }
                for (int i = 35; i <= 35; i++)
                {
                    EDIDDetails.EstTmg1.Data.Add(data[i]);
                }
                for (int i = 36; i <= 36; i++)
                {
                    EDIDDetails.EstTmg2.Data.Add(data[i]);
                }
                for (int i = 37; i <= 37; i++)
                {
                    EDIDDetails.MfgTmg.Data.Add(data[i]);
                }
                for (int i = 38; i <= 53; i++)
                {
                    EDIDDetails.StndTmg.Data.Add(data[i]);
                }
                for (int i = 54; i <= 71; i++)
                {
                    EDIDDetails.DetDes1.Data.Add(data[i]);
                }
                for (int i = 72; i <= 89; i++)
                {
                    EDIDDetails.DetDes2.Data.Add(data[i]);
                }
                for (int i = 90; i <= 107; i++)
                {
                    EDIDDetails.DetDes3.Data.Add(data[i]);
                }
                for (int i = 108; i <= 125; i++)
                {
                    EDIDDetails.DetDes4.Data.Add(data[i]);
                }
                for (int i = 126; i <= 126; i++)
                {
                    EDIDDetails.Ext.Data.Add(data[i]);
                }
                for (int i = 127; i <= 127; i++)
                {
                    EDIDDetails.ChkSum.Data.Add(data[i]);
                }

                //Added By Satyadeep Behera on 22/04/2014
                for (int i = 128; i <= 130; i++)
                {
                    EDIDDetails.ExtHeader.Data.Add(data[i]);
                }
                for (int i = 131; i <= 131; i++)
                {
                    EDIDDetails.DispSupport.Data.Add(data[i]);
                }
                for (int i = 132; i <= 139; i++)
                {
                    EDIDDetails.VidDataBlock.Data.Add(data[i]);
                }
                for (int i = 140; i <= 143; i++)
                {
                    EDIDDetails.AudDataBlock.Data.Add(data[i]);
                }
                for (int i = 144; i <= 151; i++)
                {
                    EDIDDetails.VendSpecDataBlock.Data.Add(data[i]);
                }
                for (int i = 152; i <= 169; i++)
                {
                    EDIDDetails.DetDes5.Data.Add(data[i]);
                }
                for (int i = 170; i <= 187; i++)
                {
                    EDIDDetails.DetDes6.Data.Add(data[i]);
                }
                for (int i = 188; i <= 205; i++)
                {
                    EDIDDetails.DetDes7.Data.Add(data[i]);
                }
                for (int i = 206; i <= 223; i++)
                {
                    EDIDDetails.DetDes8.Data.Add(data[i]);
                }
                for (int i = 224; i <= 241; i++)
                {
                    EDIDDetails.DetDes9.Data.Add(data[i]);
                }
                for (int i = 242; i <= 254; i++)
                {
                    EDIDDetails.PosDTDPadding.Data.Add(data[i]);
                }
                for (int i = 255; i <= 255; i++)
                {
                    EDIDDetails.ChkSum1.Data.Add(data[i]);
                }
            }
            catch { }
            return EDIDDetails;
        }        
        #endregion

        #region Get Decoded EDID
        public DecodedEDIDData GetDecodedEDIDDataFromEDIDObject(EDIDData EDIDObject)
        {
            DecodedEDIDData data = new DecodedEDIDData();
            data.IDManufacturerName = EDIDObject.GetManufacturerID();
            data.IDManufacturerHexCode = EDIDObject.GetManufacturerIDHexCode();
            data.IDProductCode = EDIDObject.GetProductID();
            data.IDSerialNumber = EDIDObject.GetSerialNumber();
            data.WeekOfManufacture = EDIDObject.GetWeekOfManufactur();
            if (data.WeekOfManufacture == 255)
            {
                data.ModelYear = (uint)(Convert.ToInt32(EDIDObject.GetYearOfManufacture()));
                data.ModelYearFlag = 1;
                data.YearOfManufacture = 0;
            }
            else
            {
                data.ModelYear = 0;
                data.ModelYearFlag = 0;
                data.YearOfManufacture = (uint)(Convert.ToInt32(EDIDObject.GetYearOfManufacture()));
            }
            string EDIDVerion = EDIDObject.GetEDIDVersion();
            data.VersionNumber = EDIDObject.GetVersionNumber();//(uint)Convert.ToUInt16(EDIDVerion[0]);
            data.RevisionNumber = EDIDObject.GetRevisionNumber();// (uint)Convert.ToUInt16(EDIDVerion[2]);
            //string VideoInput = EDIDObject.GetVideoInputDefintionByte();
            string VideoInput = EDIDObject.GetVideoInputType();            
            if (VideoInput == "DIGITAL")
            {
                data.AnalogVideoSignalInterfaceSupport = 'N';
                data.DigitalVideoSignalInterfaceSupport = 'Y';
            }
            else if (VideoInput == "ANALOG")
            {
                data.AnalogVideoSignalInterfaceSupport = 'Y';
                data.DigitalVideoSignalInterfaceSupport = 'N';
            }
            else
            {
                data.AnalogVideoSignalInterfaceSupport = 'N';
                data.DigitalVideoSignalInterfaceSupport = 'N';
            }

            data.HorizontalScreenSize = (uint)(Convert.ToInt16(EDIDObject.GetMaxHorImageSize()));
            data.VerticalScreenSize = (uint)(Convert.ToInt16(EDIDObject.GetMaxVerImageSize()));

            data.SignalLevelStandard_Video_Sync_Total = EDIDObject.GetWhiteAndSyncLevel();

            if (!EDIDObject.IsBlankToBlackSetupExpected())
                data.VideoSetupBlackLevel = 'Y';
            else
                data.VideoSetupBlackLevel = 'N';

            if (EDIDObject.IsBlankToBlackSetupExpected())
                data.VideoSetupBlankBlackLevel = 'Y';
            else
                data.VideoSetupBlankBlackLevel = 'N';

            if (EDIDObject.IsSeparateSyncsSupported())
                data.SeparateSyncHVSignalsSupported = 'Y';
            else
                data.SeparateSyncHVSignalsSupported = 'N';

            if (EDIDObject.IsCompositeSyncSupported())
                data.CompositeSyncSignalonHorizontalSupported = 'Y';
            else
                data.CompositeSyncSignalonHorizontalSupported = 'N';

            if (EDIDObject.IsSyncOnGreenSupported())
                data.CompositeSyncSignalonGreenVideoSupported = 'Y';
            else
                data.CompositeSyncSignalonGreenVideoSupported = 'N';

            if (EDIDObject.IsSerrationOfVSync())
                data.SerrationOnTheVerticalSyncSupported = 'Y';
            else
                data.SerrationOnTheVerticalSyncSupported = 'N';

            data.R720X400_at_70Hz = EDIDObject.Get_Timing_720_400_70();
            data.R720X400_at_88Hz = EDIDObject.Get_Timing_720_400_88();
            data.R640X480_at_60Hz = EDIDObject.Get_Timing_640_480_60();
            data.R640X480_at_67Hz = EDIDObject.Get_Timing_640_480_67();
            data.R640X480_at_72Hz = EDIDObject.Get_Timing_640_480_72();
            data.R640X480_at_75Hz = EDIDObject.Get_Timing_640_480_75();
            data.R800X600_at_56Hz = EDIDObject.Get_Timing_800_600_56();
            data.R800X600_at_60Hz = EDIDObject.Get_Timing_800_600_60();
            data.R800X600_at_72Hz = EDIDObject.Get_Timing_800_600_72Hz();
            data.R800X600_at_75Hz = EDIDObject.Get_Timing_800_600_75Hz();
            data.R832X624_at_75Hz = EDIDObject.Get_Timing_832_624_75Hz();
            data.R1024X768_at_87Hz = EDIDObject.Get_Timing_1024_768_87Hz();
            data.R1024X768_at_60Hz = EDIDObject.Get_Timing_1024_768_60Hz();
            data.R1024X768_at_72Hz = EDIDObject.Get_Timing_1024_768_72Hz();
            data.R1024X768_at_75Hz = EDIDObject.Get_Timing_1024_768_75Hz();
            data.R1280X1024_at_75Hz = EDIDObject.Get_Timing_1280_1024_75Hz();
            data.R1152x870_at_75Hz = EDIDObject.Get_Timing_1152_870_75Hz();

            data.Gamma = Convert.ToDouble(EDIDObject.GetDisplayGamma());
            string DMPSStandBy = EDIDObject.GetDMPSStandBy();
            if (DMPSStandBy == "YES")
                data.StandbySupport = 'Y';
            else
                data.StandbySupport = 'N';
            string DMPSSuspend = EDIDObject.GetDPMSSuspend();
            if (DMPSSuspend == "YES")
                data.SuspendSupport = 'Y';
            else
                data.SuspendSupport = 'N';
            string DMPSActiveOff = EDIDObject.GetDPMSActiveOff();
            if (DMPSActiveOff == "YES")
                data.LowPowerSupport = 'Y';
            else
                data.LowPowerSupport = 'N';


            data.Monochrome_GrayScaleDisplay = EDIDObject.IsDisplayMonochrome();
            data.RGBcolordisplay = EDIDObject.IsDisplayRGBcolor();
            data.Non_RGBcolordisplay = EDIDObject.IsDisplayNon_RGBcolor();

            data.ColorBitDepthBitsPerPrimary = (uint)(EDIDObject.GetColorBitDepth() >> 4);
            data.DVISupport = EDIDObject.IsDVISupported();
            data.HDMI_aSupport = EDIDObject.IsHDMI_a_Supported();
            data.HDMI_bSupport = EDIDObject.IsHDMI_b_Supported();
            data.MDDISupport = EDIDObject.IsMDDI_Supported();
            data.DisplayPortSupport = EDIDObject.IsDisplayPortSupported();

            data.AspectRatioLandscape = EDIDObject.GetAspectRatioLandscape().ToString();
            data.AspectRatioPortrait = EDIDObject.GetAspectRatioPortrait().ToString();


            data.RGB4_4_4 = EDIDObject.IsRGB4_4_4();
            data.RGB4_4_4_YCrCb4_4_4 = EDIDObject.IsRGB4_4_4_YCrCb4_4_4();
            data.RGB4_4_4_YCrCb4_2_2 = EDIDObject.IsRGB4_4_4_YCrCb4_2_2();
            data.RGB4_4_4_YCrCb4_4_4_YCrCb4_2_2 = EDIDObject.IsRGB4_4_4_YCrCb4_4_4_YCrCb4_2_2();
            if (EDIDObject.GetsRGBDefault())
                data.StandardDefaultColorSpace = 'Y';
            else
                data.StandardDefaultColorSpace = 'N';

            if (EDIDObject.GetNPFandRRInlcuded())
                data.NativePixelFormatIncluded = 'Y';
            else
                data.NativePixelFormatIncluded = 'N';
            if (EDIDObject.GetDCF())
                data.ContinuousFrequencyDisplay = 'Y';
            else
                data.ContinuousFrequencyDisplay = 'N';
            
            data.MonitorName = EDIDObject.GetMonitorName().Trim();
            if (data.MonitorName.Contains("SE322FS"))
            {
                int test = 0;
            }
            data.RedX = EDIDObject.GetRx();
            data.RedY = EDIDObject.GetRy();
            data.GreenX = EDIDObject.GetGx();
            data.GreenY = EDIDObject.GetGy();
            data.BlueX = EDIDObject.GetBx();
            data.BlueY = EDIDObject.GetBy();
            data.WhiteX = EDIDObject.GetWx();
            data.WhiteY = EDIDObject.GetWy();

            data.ST1_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(0);
            string AR = EDIDObject.GetImageAspectRatio(0);
            if (AR == "16:10")
            {
                data.ST1_ImageAspectRatio_16_10 = 'Y';
                data.ST1_ImageAspectRatio_4_3 = 'N';
                data.ST1_ImageAspectRatio_5_4 = 'N';
                data.ST1_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST1_ImageAspectRatio_16_10 = 'N';
                data.ST1_ImageAspectRatio_4_3 = 'Y';
                data.ST1_ImageAspectRatio_5_4 = 'N';
                data.ST1_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST1_ImageAspectRatio_16_10 = 'N';
                data.ST1_ImageAspectRatio_4_3 = 'N';
                data.ST1_ImageAspectRatio_5_4 = 'Y';
                data.ST1_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST1_ImageAspectRatio_16_10 = 'N';
                data.ST1_ImageAspectRatio_4_3 = 'N';
                data.ST1_ImageAspectRatio_5_4 = 'N';
                data.ST1_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST1_ImageAspectRatio_16_10 = 'N';
                data.ST1_ImageAspectRatio_4_3 = 'N';
                data.ST1_ImageAspectRatio_5_4 = 'N';
                data.ST1_ImageAspectRatio_16_9 = 'N';
            }

            data.ST1_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(0);

            data.ST2_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(1);
            AR = EDIDObject.GetImageAspectRatio(1);
            if (AR == "16:10")
            {
                data.ST2_ImageAspectRatio_16_10 = 'Y';
                data.ST2_ImageAspectRatio_4_3 = 'N';
                data.ST2_ImageAspectRatio_5_4 = 'N';
                data.ST2_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST2_ImageAspectRatio_16_10 = 'N';
                data.ST2_ImageAspectRatio_4_3 = 'Y';
                data.ST2_ImageAspectRatio_5_4 = 'N';
                data.ST2_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST2_ImageAspectRatio_16_10 = 'N';
                data.ST2_ImageAspectRatio_4_3 = 'N';
                data.ST2_ImageAspectRatio_5_4 = 'Y';
                data.ST2_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST2_ImageAspectRatio_16_10 = 'N';
                data.ST2_ImageAspectRatio_4_3 = 'N';
                data.ST2_ImageAspectRatio_5_4 = 'N';
                data.ST2_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST2_ImageAspectRatio_16_10 = 'N';
                data.ST2_ImageAspectRatio_4_3 = 'N';
                data.ST2_ImageAspectRatio_5_4 = 'N';
                data.ST2_ImageAspectRatio_16_9 = 'N';
            }

            data.ST2_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(1);

            data.ST3_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(2);
            AR = EDIDObject.GetImageAspectRatio(2);
            if (AR == "16:10")
            {
                data.ST3_ImageAspectRatio_16_10 = 'Y';
                data.ST3_ImageAspectRatio_4_3 = 'N';
                data.ST3_ImageAspectRatio_5_4 = 'N';
                data.ST3_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST3_ImageAspectRatio_16_10 = 'N';
                data.ST3_ImageAspectRatio_4_3 = 'Y';
                data.ST3_ImageAspectRatio_5_4 = 'N';
                data.ST3_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST3_ImageAspectRatio_16_10 = 'N';
                data.ST3_ImageAspectRatio_4_3 = 'N';
                data.ST3_ImageAspectRatio_5_4 = 'Y';
                data.ST3_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST3_ImageAspectRatio_16_10 = 'N';
                data.ST3_ImageAspectRatio_4_3 = 'N';
                data.ST3_ImageAspectRatio_5_4 = 'N';
                data.ST3_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST3_ImageAspectRatio_16_10 = 'N';
                data.ST3_ImageAspectRatio_4_3 = 'N';
                data.ST3_ImageAspectRatio_5_4 = 'N';
                data.ST3_ImageAspectRatio_16_9 = 'N';
            }
            data.ST3_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(2);

            data.ST4_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(3);
            AR = EDIDObject.GetImageAspectRatio(3);
            if (AR == "16:10")
            {
                data.ST4_ImageAspectRatio_16_10 = 'Y';
                data.ST4_ImageAspectRatio_4_3 = 'N';
                data.ST4_ImageAspectRatio_5_4 = 'N';
                data.ST4_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST4_ImageAspectRatio_16_10 = 'N';
                data.ST4_ImageAspectRatio_4_3 = 'Y';
                data.ST4_ImageAspectRatio_5_4 = 'N';
                data.ST4_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST4_ImageAspectRatio_16_10 = 'N';
                data.ST4_ImageAspectRatio_4_3 = 'N';
                data.ST4_ImageAspectRatio_5_4 = 'Y';
                data.ST4_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST4_ImageAspectRatio_16_10 = 'N';
                data.ST4_ImageAspectRatio_4_3 = 'N';
                data.ST4_ImageAspectRatio_5_4 = 'N';
                data.ST4_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST4_ImageAspectRatio_16_10 = 'N';
                data.ST4_ImageAspectRatio_4_3 = 'N';
                data.ST4_ImageAspectRatio_5_4 = 'N';
                data.ST4_ImageAspectRatio_16_9 = 'N';
            }
            data.ST4_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(3);

            data.ST5_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(4);
            AR = EDIDObject.GetImageAspectRatio(4);
            if (AR == "16:10")
            {
                data.ST5_ImageAspectRatio_16_10 = 'Y';
                data.ST5_ImageAspectRatio_4_3 = 'N';
                data.ST5_ImageAspectRatio_5_4 = 'N';
                data.ST5_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST5_ImageAspectRatio_16_10 = 'N';
                data.ST5_ImageAspectRatio_4_3 = 'Y';
                data.ST5_ImageAspectRatio_5_4 = 'N';
                data.ST5_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST5_ImageAspectRatio_16_10 = 'N';
                data.ST5_ImageAspectRatio_4_3 = 'N';
                data.ST5_ImageAspectRatio_5_4 = 'Y';
                data.ST5_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST5_ImageAspectRatio_16_10 = 'N';
                data.ST5_ImageAspectRatio_4_3 = 'N';
                data.ST5_ImageAspectRatio_5_4 = 'N';
                data.ST5_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST5_ImageAspectRatio_16_10 = 'N';
                data.ST5_ImageAspectRatio_4_3 = 'N';
                data.ST5_ImageAspectRatio_5_4 = 'N';
                data.ST5_ImageAspectRatio_16_9 = 'N';
            }
            data.ST5_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(4);

            data.ST6_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(5);
            AR = EDIDObject.GetImageAspectRatio(5);
            if (AR == "16:10")
            {
                data.ST6_ImageAspectRatio_16_10 = 'Y';
                data.ST6_ImageAspectRatio_4_3 = 'N';
                data.ST6_ImageAspectRatio_5_4 = 'N';
                data.ST6_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST6_ImageAspectRatio_16_10 = 'N';
                data.ST6_ImageAspectRatio_4_3 = 'Y';
                data.ST6_ImageAspectRatio_5_4 = 'N';
                data.ST6_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST6_ImageAspectRatio_16_10 = 'N';
                data.ST6_ImageAspectRatio_4_3 = 'N';
                data.ST6_ImageAspectRatio_5_4 = 'Y';
                data.ST6_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST6_ImageAspectRatio_16_10 = 'N';
                data.ST6_ImageAspectRatio_4_3 = 'N';
                data.ST6_ImageAspectRatio_5_4 = 'N';
                data.ST6_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST6_ImageAspectRatio_16_10 = 'N';
                data.ST6_ImageAspectRatio_4_3 = 'N';
                data.ST6_ImageAspectRatio_5_4 = 'N';
                data.ST6_ImageAspectRatio_16_9 = 'N';
            }
            data.ST6_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(5);

            data.ST7_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(6);

            AR = EDIDObject.GetImageAspectRatio(6);
            if (AR == "16:10")
            {
                data.ST7_ImageAspectRatio_16_10 = 'Y';
                data.ST7_ImageAspectRatio_4_3 = 'N';
                data.ST7_ImageAspectRatio_5_4 = 'N';
                data.ST7_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST7_ImageAspectRatio_16_10 = 'N';
                data.ST7_ImageAspectRatio_4_3 = 'Y';
                data.ST7_ImageAspectRatio_5_4 = 'N';
                data.ST7_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST7_ImageAspectRatio_16_10 = 'N';
                data.ST7_ImageAspectRatio_4_3 = 'N';
                data.ST7_ImageAspectRatio_5_4 = 'Y';
                data.ST7_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST7_ImageAspectRatio_16_10 = 'N';
                data.ST7_ImageAspectRatio_4_3 = 'N';
                data.ST7_ImageAspectRatio_5_4 = 'N';
                data.ST7_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST7_ImageAspectRatio_16_10 = 'N';
                data.ST7_ImageAspectRatio_4_3 = 'N';
                data.ST7_ImageAspectRatio_5_4 = 'N';
                data.ST7_ImageAspectRatio_16_9 = 'N';
            }
            data.ST7_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(6);

            data.ST8_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(7);
            AR = EDIDObject.GetImageAspectRatio(7);
            if (AR == "16:10")
            {
                data.ST8_ImageAspectRatio_16_10 = 'Y';
                data.ST8_ImageAspectRatio_4_3 = 'N';
                data.ST8_ImageAspectRatio_5_4 = 'N';
                data.ST8_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST8_ImageAspectRatio_16_10 = 'N';
                data.ST8_ImageAspectRatio_4_3 = 'Y';
                data.ST8_ImageAspectRatio_5_4 = 'N';
                data.ST8_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST8_ImageAspectRatio_16_10 = 'N';
                data.ST8_ImageAspectRatio_4_3 = 'N';
                data.ST8_ImageAspectRatio_5_4 = 'Y';
                data.ST8_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST8_ImageAspectRatio_16_10 = 'N';
                data.ST8_ImageAspectRatio_4_3 = 'N';
                data.ST8_ImageAspectRatio_5_4 = 'N';
                data.ST8_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST8_ImageAspectRatio_16_10 = 'N';
                data.ST8_ImageAspectRatio_4_3 = 'N';
                data.ST8_ImageAspectRatio_5_4 = 'N';
                data.ST8_ImageAspectRatio_16_9 = 'N';
            }
            data.ST8_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(7);

            data.PixelClock_MHz = EDIDObject.GetpixelClock();
            data.HorizontalAddressableVideoPixels = EDIDObject.GetHorizontalActivePixelsOfDescriptor();
            data.HorizontalBlankingPixels = EDIDObject.GetHorizontalBlankingPixels();
            data.VerticalAddressableVideoinlines = EDIDObject.GetVerticalActivePixels();
            data.VerticalBlankinginlines = EDIDObject.GetVerticalBlankingPixels();
            data.HorizontalFrontPorchpixels = EDIDObject.GetHorizontalSyncOffset();
            data.HorizontalSyncPulseWidthPixels = EDIDObject.GetHorizontalSyncPulseWidth();
            data.VerticalFrontPorchlines = EDIDObject.GetVerticalSyncOffset();
            data.VerticalSyncPulseWidthlines = EDIDObject.GetVerticalSyncPulseWidth();
            data.HorizontalImageSize = EDIDObject.GetHorizontalDisplaySize();
            data.VerticalImageSize = EDIDObject.GetVerticalDisplaySize();
            data.HorizontalBorderPixels = EDIDObject.GetHorizontalBorderPixels();
            data.VerticalBorderLines = EDIDObject.GetVerticalBorderPixels();
            if (EDIDObject.GetInterlaced())
                data.Interlaced = 'Y';
            else
                data.Interlaced = 'N';

            if (EDIDObject.IsNormalDisplay_NoStereo())
                data.NormalDisplay_NoStereo = 'Y';
            else
                data.NormalDisplay_NoStereo = 'N';

            if (EDIDObject.IsFieldsequentialstereo_leftimage())
                data.Fieldsequentialstereo_leftimage = 'Y';
            else
                data.Fieldsequentialstereo_leftimage = 'N';

            if (EDIDObject.IsFieldsequentialstereo_rightimage())
                data.Fieldsequentialstereo_rightimage = 'Y';
            else
                data.Fieldsequentialstereo_rightimage = 'N';

            if (EDIDObject.IsTwowayinterleavedstereo_rightimage())
                data.Twowayinterleavedstereo_rightimage = 'Y';
            else
                data.Twowayinterleavedstereo_rightimage = 'N';

            if (EDIDObject.IsTwowayinterleavedstereo_leftimage())
                data.Twowayinterleavedstereo_leftimage = 'Y';
            else
                data.Twowayinterleavedstereo_leftimage = 'N';

            if (EDIDObject.IsFourwayinterleavedstereo())
                data.Fourwayinterleavedstereo = 'Y';
            else
                data.Fourwayinterleavedstereo = 'N';

            if (EDIDObject.IsSidebySideinterleavedstereo())
                data.SidebySideinterleavedstereo = 'Y';
            else
                data.SidebySideinterleavedstereo = 'N';

            if (EDIDObject.IsAnalogCompositeSync())
                data.AnalogCompositeSync = 'Y';
            else
                data.AnalogCompositeSync = 'N';

            if (EDIDObject.IsBipolarAnalogCompositeSync())
                data.BipolarAnalogCompositeSync = 'Y';
            else
                data.BipolarAnalogCompositeSync = 'N';

            if (EDIDObject.IsAnalogWithoutSerrations())
                data.AnalogWithoutSerrations = 'Y';
            else
                data.AnalogWithoutSerrations = 'N';

            if (EDIDObject.IsAnalogWithSerrations())
                data.AnalogWithSerrations = 'Y';
            else
                data.AnalogWithSerrations = 'N';

            if (EDIDObject.IsSyncOnGreenSignalonly())
                data.SyncOnGreenSignalonly = 'Y';
            else
                data.SyncOnGreenSignalonly = 'N';

            if (EDIDObject.IsSyncOnallthreeRGBvideosignals())
                data.SyncOnallthreeRGBvideosignals = 'Y';
            else
                data.SyncOnallthreeRGBvideosignals = 'N';

            if (EDIDObject.IsDigitalCompositeSync())
                data.DigitalCompositeSync = 'Y';
            else
                data.DigitalCompositeSync = 'N';

            if (EDIDObject.IsDigitalWithoutSerrations())
                data.DigitalWithoutSerrations = 'Y';
            else
                data.DigitalWithoutSerrations = 'N';

            if (EDIDObject.IsDigitalWithSerrations())
                data.DigitalWithSerrations = 'Y';
            else
                data.DigitalWithSerrations = 'N';

            if (EDIDObject.IsDigitalSeparateSync())
                data.DigitalSeparateSync = 'Y';
            else
                data.DigitalSeparateSync = 'N';

            if (EDIDObject.IsDigitalVerticalSyncPositive())
                data.DigitalVerticalSyncPositive = 'Y';
            else
                data.DigitalVerticalSyncPositive = 'N';

            if (EDIDObject.IsDigitalHorizontalSyncPositive())
                data.DigitalHorizontalSyncPositive = 'Y';
            else
                data.DigitalHorizontalSyncPositive = 'N';

            data.DisplayProductSerialNumberDescriptorTagNumber = EDIDObject.GetMonitorSerialNumber().Trim();
            data.PhysicalAddress = EDIDObject.GetPhysicalAddress();
            /*data.StringDescriptorTagnumber;
            data.MinVerticalRate_Flag;
            data.MaxVerticalRate_Flag;
            data.MinHorizontalRate_flag;
            data.MaxHorizontalRate_flag;
            data.MinVerticalRate;
            data.MaxVerticalRate;
            data.MinHorizontalRate;
            data.MaxHorizontalRate;
            data.Maxpixelclock;
            data.DefaultGTFsupported;
            data.RangeLimits;
            data.SecondaryGTFsupported;
            data.LineFeed;
            data.GTFSecondaryCurvesupported;
            data.Startbreakfrequency;
            data.CX2;
            data.M;
            data.K;
            data.J;
            data.CVTsupported;
            data.CVTStandardVersionNumber;
            data.AdditionalPixelClockPrecision;
            data.MaximumActivePixelsperLine_MSB;
            data.MaximumActivePixelsperLine_LSB;
            data.AR_4_3PreferedAspectRatio;
            data.AR_16_9SupportedAspectRatio;
            data.AR_16_10SupportedAspectRatio;
            data.AR_5_4SupportedAspectRatio;
            data.AR_15_9SupportedAspectRatio;
            data.AR_4_3PreferedAspectRatio;
            data.AR_16_9PreferedAspectRatio;
            data.AR_16_10PreferedAspectRatio;
            data.AR_5_4PreferedAspectRatio;
            data.AR_15_9PreferedAspectRatio;
            data.StandardCVTBlankingSupport;
            data.ReducedCVTBlankingSupport;
            data.HorizontalShrinkSupport;
            data.HorizontalStretch;
            data.VerticalShrink;
            data.VerticalStretch;
            data.PreferredVerticalRefreshRate;
            data.ColorPointDescriptorTagNumber;
            data.WhitePointIndexNumber;
            data.White_xy;
            data.White_x;
            data.White_y;
            data.ColorPointDescriptorValueStored;*/
            return data;
        }
        public DataRow CreateDecodedEdIdRow(DataTable DecodedEdIdDataTable, DecodedEDIDData data)
        {
            DataRow DecodedEDIDDataRow = DecodedEdIdDataTable.NewRow();

            for (int i = 0; i < DecodedEdIdDataTable.Columns.Count; i++)
            {
                DecodedEDIDDataRow[DecodedEdIdDataTable.Columns[i]] = DBNull.Value;
            }
            //DecodedEDIDDataRow["RID"] = 
            //DecodedEDIDDataRow["FK_EDID_RID"]           = data.FK_ConsoleEDID_RID;
            DecodedEDIDDataRow["FK_ConsoleEDID_RID"]    = data.FK_ConsoleEDID_RID;
            DecodedEDIDDataRow["IDManufacturerName"]    = data.IDManufacturerName;
            DecodedEDIDDataRow["IDProductCode"]         = data.IDProductCode;
            DecodedEDIDDataRow["IDSerialNumber"]        = data.IDSerialNumber;
            DecodedEDIDDataRow["WeekOfManufacture"]     = data.WeekOfManufacture;
            DecodedEDIDDataRow["YearOfManufacture"]     = data.YearOfManufacture;
            DecodedEDIDDataRow["ModelYearFlag"]         = data.ModelYearFlag;
            DecodedEDIDDataRow["ModelYear"]             = data.ModelYear;
            DecodedEDIDDataRow["VersionNumber"]         = data.VersionNumber;
            DecodedEDIDDataRow["RevisionNumber"]        = data.RevisionNumber;
            DecodedEDIDDataRow["AnalogVideoSignalInterfaceSupport"] = data.AnalogVideoSignalInterfaceSupport;
            DecodedEDIDDataRow["DigitalVideoSignalInterfaceSupport"] = data.DigitalVideoSignalInterfaceSupport;
            DecodedEDIDDataRow["SignalLevelStandard_Video_Sync_Total"] = data.SignalLevelStandard_Video_Sync_Total;
            DecodedEDIDDataRow["VideoSetupBlackLevel"] = data.VideoSetupBlackLevel;
            DecodedEDIDDataRow["VideoSetupBlankBlackLevel"] = data.VideoSetupBlankBlackLevel;
            DecodedEDIDDataRow["SeparateSyncH&VSignalsSupported"] = data.SeparateSyncHVSignalsSupported;
            DecodedEDIDDataRow["CompositeSyncSignalonHorizontalSupported"] = data.CompositeSyncSignalonHorizontalSupported;
            DecodedEDIDDataRow["CompositeSyncSignalonGreenVideoSupported"] = data.CompositeSyncSignalonGreenVideoSupported;
            DecodedEDIDDataRow["SerrationOnTheVerticalSyncSupported"] = data.SerrationOnTheVerticalSyncSupported;
            DecodedEDIDDataRow["ColorBitDepthBitsPerPrimary"] = data.ColorBitDepthBitsPerPrimary;
            DecodedEDIDDataRow["DVISupport"] = data.DVISupport;
            DecodedEDIDDataRow["HDMI-aSupport"] = data.HDMI_aSupport;
            DecodedEDIDDataRow["HDMI-bSupport"] = data.HDMI_bSupport;
            DecodedEDIDDataRow["MDDISupport"] = data.MDDISupport;
            DecodedEDIDDataRow["DisplayPortSupport"] = data.DisplayPortSupport;
            DecodedEDIDDataRow["HorizontalScreenSize"] = data.HorizontalScreenSize;
            DecodedEDIDDataRow["VerticalScreenSize"] = data.VerticalScreenSize;
            DecodedEDIDDataRow["AspectRatioLandscape"] = data.AspectRatioLandscape;
            DecodedEDIDDataRow["AspectRatioPortrait"] = data.AspectRatioPortrait;
            DecodedEDIDDataRow["Gamma"] = data.Gamma;
            DecodedEDIDDataRow["StandbySupport"] = data.StandbySupport;
            DecodedEDIDDataRow["SuspendSupport"] = data.SuspendSupport;
            DecodedEDIDDataRow["LowPowerSupport"] = data.LowPowerSupport;
            DecodedEDIDDataRow["Monochrome_GrayScaleDisplay"] = data.Monochrome_GrayScaleDisplay;
            DecodedEDIDDataRow["RGBcolordisplay"] = data.RGBcolordisplay;
            DecodedEDIDDataRow["Non-RGBcolordisplay"] = data.Non_RGBcolordisplay;
            DecodedEDIDDataRow["RGB4:4:4"] = data.RGB4_4_4;
            DecodedEDIDDataRow["RGB4:4:4_YCrCb4:4:4"] = data.RGB4_4_4_YCrCb4_4_4;
            DecodedEDIDDataRow["RGB4:4:4_YCrCb4:2:2"] = data.RGB4_4_4_YCrCb4_2_2;
            DecodedEDIDDataRow["RGB4:4:4_YCrCb 4:4:4_YCrCb4:2:2"] = data.RGB4_4_4_YCrCb4_4_4_YCrCb4_2_2;
            DecodedEDIDDataRow["StandardDefaultColorSpace"] = data.StandardDefaultColorSpace;
            DecodedEDIDDataRow["NativePixelFormatIncluded"] = data.NativePixelFormatIncluded;
            DecodedEDIDDataRow["ContinuousFrequencyDisplay"] = data.ContinuousFrequencyDisplay;
            DecodedEDIDDataRow["RedX"] = data.RedX;
            DecodedEDIDDataRow["RedY"] = data.RedY;
            DecodedEDIDDataRow["GreenX"] = data.GreenX;
            DecodedEDIDDataRow["GreenY"] = data.GreenY;
            DecodedEDIDDataRow["BlueX"] = data.BlueX;
            DecodedEDIDDataRow["BlueY"] = data.BlueY;
            DecodedEDIDDataRow["WhiteX"] = data.WhiteX;
            DecodedEDIDDataRow["WhiteY"] = data.WhiteY;
            DecodedEDIDDataRow["720x400@70Hz"] = data.R720X400_at_70Hz;
            DecodedEDIDDataRow["720x400@88Hz"] = data.R720X400_at_88Hz;
            DecodedEDIDDataRow["640x480@60Hz"] = data.R640X480_at_60Hz;
            DecodedEDIDDataRow["640x480@67Hz"] = data.R640X480_at_67Hz;
            DecodedEDIDDataRow["640x480@72Hz"] = data.R640X480_at_72Hz;
            DecodedEDIDDataRow["640x480@75Hz"] = data.R640X480_at_75Hz;
            DecodedEDIDDataRow["800x600@56Hz"] = data.R800X600_at_56Hz;
            DecodedEDIDDataRow["800x600@60Hz"] = data.R800X600_at_60Hz;
            DecodedEDIDDataRow["800x600@72Hz"] = data.R800X600_at_72Hz;
            DecodedEDIDDataRow["800x600@75Hz"] = data.R800X600_at_75Hz;
            DecodedEDIDDataRow["832x624@75Hz"] = data.R832X624_at_75Hz;
            DecodedEDIDDataRow["1024x768@87Hz"] = data.R1024X768_at_87Hz;
            DecodedEDIDDataRow["1024x768@60Hz"] = data.R1024X768_at_60Hz;
            DecodedEDIDDataRow["1024x768@72Hz"] = data.R1024X768_at_72Hz;
            DecodedEDIDDataRow["1024x768@75Hz"] = data.R1024X768_at_75Hz;
            DecodedEDIDDataRow["1280x1024@75Hz"] = data.R1280X1024_at_75Hz;
            DecodedEDIDDataRow["1152x870@75Hz"] = data.R1152x870_at_75Hz;
            DecodedEDIDDataRow["ST1_HorizontalActivePixels"] = data.ST1_HorizontalActivePixels;
            DecodedEDIDDataRow["ST1_Image Aspect Ratio_16:10"] = data.ST1_ImageAspectRatio_16_10;
            DecodedEDIDDataRow["ST1_Image Aspect Ratio_4:3"] = data.ST1_ImageAspectRatio_4_3;
            DecodedEDIDDataRow["ST1_Image Aspect Ratio_5:4"] = data.ST1_ImageAspectRatio_5_4;
            DecodedEDIDDataRow["ST1_Image Aspect Ratio_16:9"] = data.ST1_ImageAspectRatio_16_9;
            DecodedEDIDDataRow["ST1_ImageRefreshRate_Hz"] = data.ST1_ImageRefreshRate_Hz;
            DecodedEDIDDataRow["ST2_HorizontalActivePixels"] = data.ST2_HorizontalActivePixels;
            DecodedEDIDDataRow["ST2_ImageAspectRatio_16:10"] = data.ST2_ImageAspectRatio_16_10;
            DecodedEDIDDataRow["ST2_ImageAspectRatio_4:3"] = data.ST2_ImageAspectRatio_4_3;
            DecodedEDIDDataRow["ST2_ImageAspectRatio_5:4"] = data.ST2_ImageAspectRatio_5_4;
            DecodedEDIDDataRow["ST2_ImageAspectRatio_16:9"] = data.ST2_ImageAspectRatio_16_9;
            DecodedEDIDDataRow["ST2_ImageRefreshRate_Hz"] = data.ST2_ImageRefreshRate_Hz;
            DecodedEDIDDataRow["ST3_HorizontalActivePixels"] = data.ST3_HorizontalActivePixels;
            DecodedEDIDDataRow["ST3_ImageAspectRatio_16:10"] = data.ST3_ImageAspectRatio_16_10;
            DecodedEDIDDataRow["ST3_ImageAspectRatio_4:3"] = data.ST3_ImageAspectRatio_4_3;
            DecodedEDIDDataRow["ST3_ImageAspectRatio_5:4"] = data.ST3_ImageAspectRatio_5_4;
            DecodedEDIDDataRow["ST3_ImageAspectRatio_16:9"] = data.ST3_ImageAspectRatio_16_9;
            DecodedEDIDDataRow["ST3_ImageRefreshRate_Hz"] = data.ST3_ImageRefreshRate_Hz;
            DecodedEDIDDataRow["ST4_HorizontalActivePixels"] = data.ST4_HorizontalActivePixels;
            DecodedEDIDDataRow["ST4_ImageAspectRatio_16:10"] = data.ST4_ImageAspectRatio_16_10;
            DecodedEDIDDataRow["ST4_ImageAspectRatio_4:3"] = data.ST4_ImageAspectRatio_4_3;
            DecodedEDIDDataRow["ST4_ImageAspectRatio_5:4"] = data.ST4_ImageAspectRatio_5_4;
            DecodedEDIDDataRow["ST4_ImageAspectRatio_16:9"] = data.ST4_ImageAspectRatio_16_9;
            DecodedEDIDDataRow["ST4_ImageRefreshRate_Hz"] = data.ST4_ImageRefreshRate_Hz;
            DecodedEDIDDataRow["ST5_HorizontalActivePixels"] = data.ST5_HorizontalActivePixels;
            DecodedEDIDDataRow["ST5_ImageAspectRatio_16:10"] = data.ST5_ImageAspectRatio_16_10;
            DecodedEDIDDataRow["ST5_ImageAspectRatio_4:3"] = data.ST5_ImageAspectRatio_4_3;
            DecodedEDIDDataRow["ST5_ImageAspectRatio_5:4"] = data.ST5_ImageAspectRatio_5_4;
            DecodedEDIDDataRow["ST5_ImageAspectRatio_16:9"] = data.ST5_ImageAspectRatio_16_9;
            DecodedEDIDDataRow["ST5_ImageRefreshRate_Hz"] = data.ST5_ImageRefreshRate_Hz;
            DecodedEDIDDataRow["ST6_HorizontalActivePixels"] = data.ST6_HorizontalActivePixels;
            DecodedEDIDDataRow["ST6_ImageAspectRatio_16:10"] = data.ST6_ImageAspectRatio_16_10;
            DecodedEDIDDataRow["ST6_ImageAspectRatio_4:3"] = data.ST6_ImageAspectRatio_4_3;
            DecodedEDIDDataRow["ST6_ImageAspectRatio_5:4"] = data.ST6_ImageAspectRatio_5_4;
            DecodedEDIDDataRow["ST6_ImageAspectRatio_16:9"] = data.ST6_ImageAspectRatio_16_9;
            DecodedEDIDDataRow["ST6_ImageRefreshRate_Hz"] = data.ST6_ImageRefreshRate_Hz;
            DecodedEDIDDataRow["ST7_HorizontalActivePixels"] = data.ST7_HorizontalActivePixels;
            DecodedEDIDDataRow["ST7_ImageAspectRatio_16:10"] = data.ST7_ImageAspectRatio_16_10;
            DecodedEDIDDataRow["ST7_ImageAspectRatio_4:3"] = data.ST7_ImageAspectRatio_4_3;
            DecodedEDIDDataRow["ST7_ImageAspectRatio_5:4"] = data.ST7_ImageAspectRatio_5_4;
            DecodedEDIDDataRow["ST7_ImageAspectRatio_16:9"] = data.ST7_ImageAspectRatio_16_9;
            DecodedEDIDDataRow["ST7_ImageRefreshRate_Hz"] = data.ST7_ImageRefreshRate_Hz;
            DecodedEDIDDataRow["ST8_HorizontalActivePixels"] = data.ST8_HorizontalActivePixels;
            DecodedEDIDDataRow["ST8_ImageAspectRatio_16:10"] = data.ST8_ImageAspectRatio_16_10;
            DecodedEDIDDataRow["ST8_ImageAspectRatio_4:3"] = data.ST8_ImageAspectRatio_4_3;
            DecodedEDIDDataRow["ST8_ImageAspectRatio_5:4"] = data.ST8_ImageAspectRatio_5_4;
            DecodedEDIDDataRow["ST8_ImageAspectRatio_16:9"] = data.ST8_ImageAspectRatio_16_9;
            DecodedEDIDDataRow["ST8_ImageRefreshRate_Hz"] = data.ST8_ImageRefreshRate_Hz;
            DecodedEDIDDataRow["PixelClock_MHz"] = data.PixelClock_MHz;
            DecodedEDIDDataRow["HorizontalAddressableVideoPixels"] = data.HorizontalAddressableVideoPixels;
            DecodedEDIDDataRow["HorizontalBlankingPixels"] = data.HorizontalBlankingPixels;
            DecodedEDIDDataRow["VerticalAddressableVideoinlines"] = data.VerticalAddressableVideoinlines;
            DecodedEDIDDataRow["VerticalBlankinginlines"] = data.VerticalBlankinginlines;
            DecodedEDIDDataRow["HorizontalFrontPorchpixels"] = data.HorizontalFrontPorchpixels;
            DecodedEDIDDataRow["HorizontalSyncPulseWidthPixels"] = data.HorizontalSyncPulseWidthPixels;
            DecodedEDIDDataRow["VerticalFrontPorchlines"] = data.VerticalFrontPorchlines;
            DecodedEDIDDataRow["VerticalSyncPulseWidthlines"] = data.VerticalSyncPulseWidthlines;
            DecodedEDIDDataRow["HorizontalImageSize"] = data.HorizontalImageSize;
            DecodedEDIDDataRow["VerticalImageSize"] = data.VerticalImageSize;
            DecodedEDIDDataRow["HorizontalBorderPixels"] = data.HorizontalBorderPixels;
            DecodedEDIDDataRow["VerticalBorderLines"] = data.VerticalBorderLines;
            DecodedEDIDDataRow["Interlaced"] = data.Interlaced;
            DecodedEDIDDataRow["NormalDisplay_NoStereo"] = data.NormalDisplay_NoStereo;
            DecodedEDIDDataRow["Fieldsequentialstereo_rightimage"] = data.Fieldsequentialstereo_rightimage;
            DecodedEDIDDataRow["Fieldsequentialstereo_leftimage"] = data.Fieldsequentialstereo_leftimage;
            DecodedEDIDDataRow["2wayinterleavedstereo_rightimage"] = data.Twowayinterleavedstereo_rightimage;
            DecodedEDIDDataRow["2wayinterleavedstereo_leftimage"] = data.Twowayinterleavedstereo_leftimage;
            DecodedEDIDDataRow["4wayinterleavedstereo"] = data.Fourwayinterleavedstereo;
            DecodedEDIDDataRow["SidebySideinterleavedstereo"] = data.SidebySideinterleavedstereo;
            DecodedEDIDDataRow["AnalogCompositeSync"] = data.AnalogCompositeSync;
            DecodedEDIDDataRow["BipolarAnalogCompositeSync"] = data.BipolarAnalogCompositeSync;
            DecodedEDIDDataRow["AnalogWithoutSerrations"] = data.AnalogWithoutSerrations;
            DecodedEDIDDataRow["AnalogWithSerrations"] = data.AnalogWithSerrations;
            DecodedEDIDDataRow["SyncOnGreenSignalonly"] = data.SyncOnGreenSignalonly;
            DecodedEDIDDataRow["SyncOnallthreeRGBvideosignals"] = data.SyncOnallthreeRGBvideosignals;
            DecodedEDIDDataRow["DigitalCompositeSync"] = data.DigitalCompositeSync;
            DecodedEDIDDataRow["DigitalWithoutSerrations"] = data.DigitalWithoutSerrations;
            DecodedEDIDDataRow["DigitalWithSerrations"] = data.DigitalWithSerrations;
            DecodedEDIDDataRow["DigitalSeparateSync"] = data.DigitalSeparateSync;
            DecodedEDIDDataRow["DigitalVerticalSyncPositive"] = data.DigitalVerticalSyncPositive;
            DecodedEDIDDataRow["DigitalHorizontalSyncPositive"] = data.DigitalHorizontalSyncPositive;
            DecodedEDIDDataRow["DisplayProductSerialNumberDescriptorTagNumber"] = data.DisplayProductSerialNumberDescriptorTagNumber;
            DecodedEDIDDataRow["StringDescriptorTagnumber"] = data.StringDescriptorTagnumber;
            DecodedEDIDDataRow["MinVerticalRate_Flag"] = DBNull.Value;
            DecodedEDIDDataRow["MinVerticalRate_Flag"] = DBNull.Value;
            DecodedEDIDDataRow["MaxVerticalRate_Flag"] = DBNull.Value;
            DecodedEDIDDataRow["MinHorizontalRate_flag"] = DBNull.Value;
            DecodedEDIDDataRow["MaxHorizontalRate_flag"] = DBNull.Value;
            DecodedEDIDDataRow["MinVerticalRate"] = DBNull.Value;
            DecodedEDIDDataRow["MaxVerticalRate"] = DBNull.Value;
            DecodedEDIDDataRow["MinHorizontalRate"] = DBNull.Value;
            DecodedEDIDDataRow["MaxHorizontalRate"] = DBNull.Value;
            DecodedEDIDDataRow["Maxpixelclock"] = DBNull.Value;
            DecodedEDIDDataRow["DefaultGTFsupported"] = DBNull.Value;
            DecodedEDIDDataRow["RangeLimits"] = DBNull.Value;
            DecodedEDIDDataRow["SecondaryGTFsupported"] = DBNull.Value;
            DecodedEDIDDataRow["LineFeed"] = DBNull.Value;
            DecodedEDIDDataRow["GTFSecondaryCurvesupported"] = DBNull.Value;
            DecodedEDIDDataRow["Startbreakfrequency"] = DBNull.Value;
            DecodedEDIDDataRow["Cx2"] = DBNull.Value;
            DecodedEDIDDataRow["M"] = DBNull.Value;
            DecodedEDIDDataRow["K"] = DBNull.Value;
            DecodedEDIDDataRow["J"] = DBNull.Value;
            DecodedEDIDDataRow["CVTsupported"] = DBNull.Value;
            DecodedEDIDDataRow["CVTStandardVersionNumber"] = DBNull.Value;
            DecodedEDIDDataRow["AdditionalPixelClockPrecision"] = DBNull.Value;
            DecodedEDIDDataRow["MaximumActivePixelsperLine_MSB"] = DBNull.Value;
            DecodedEDIDDataRow["MaximumActivePixelsperLine_LSB"] = DBNull.Value;
            DecodedEDIDDataRow["4:3SupportedAspectRatio"] = DBNull.Value;
            DecodedEDIDDataRow["16:9SupportedAspectRatio"] = DBNull.Value;
            DecodedEDIDDataRow["16:10SupportedAspectRatio"] = DBNull.Value;
            DecodedEDIDDataRow["5:4SupportedAspectRatio"] = DBNull.Value;
            DecodedEDIDDataRow["15:9SupportedAspectRatio"] = DBNull.Value;
            DecodedEDIDDataRow["4:3PreferedAspectRatio"] = DBNull.Value;
            DecodedEDIDDataRow["16:9PreferedAspectRatio"] = DBNull.Value;
            DecodedEDIDDataRow["16:10PreferedAspectRatio"] = DBNull.Value;
            DecodedEDIDDataRow["5:4PreferedAspectRatio"] = DBNull.Value;
            DecodedEDIDDataRow["15:9PreferedAspectRatio"] = DBNull.Value;
            DecodedEDIDDataRow["StandardCVTBlankingSupport"] = DBNull.Value;
            DecodedEDIDDataRow["ReducedCVTBlankingSupport"] = DBNull.Value;
            DecodedEDIDDataRow["HorizontalShrinkSupport"] = DBNull.Value;
            DecodedEDIDDataRow["HorizontalStretch"] = DBNull.Value;
            DecodedEDIDDataRow["VerticalShrink"] = DBNull.Value;
            DecodedEDIDDataRow["VerticalStretch"] = DBNull.Value;
            DecodedEDIDDataRow["PreferredVerticalRefreshRate"] = DBNull.Value;
            DecodedEDIDDataRow["ColorPointDescriptorTagNumber"] = DBNull.Value;
            DecodedEDIDDataRow["WhitePointIndexNumber"] = DBNull.Value;
            DecodedEDIDDataRow["White_xy"] = DBNull.Value;
            DecodedEDIDDataRow["White_x"] = DBNull.Value;
            DecodedEDIDDataRow["White_y"] = DBNull.Value;
            DecodedEDIDDataRow["ColorPointDescriptorValueStored"] = DBNull.Value;
            //DecodedEDIDDataRow["CreationDate"] = data.
            //DecodedEDIDDataRow["ModificationDate"] = 
            DecodedEDIDDataRow["MonitorName"] = data.MonitorName;
            DecodedEDIDDataRow["PhysicalAddress"] = data.PhysicalAddress;
            return DecodedEDIDDataRow;
        }
        #endregion

        #region Update Decoded EdId
        public void UpdateDecodedEdId(ref String m_Message)
        {
            this.ErrorMessage = String.Empty;
            try
            {
                EDIDDataAccess m_DBAccess       = new EDIDDataAccess();
                EDIDData m_EDIDObject           = new EDIDData();
                EDIDParser m_EDIDDecoder        = new EDIDParser();
                DataTable gapInEdIdDecoded      = m_DBAccess.GetGapsInEdIdTable();
                DataTable DecodedEDIDTableObj   = m_DBAccess.CreateDecodedEDIDDataTable();
                if (gapInEdIdDecoded != null)
                {
                    Int64 intRowCount = 0;
                    foreach (DataRow item in gapInEdIdDecoded.Rows)
                    {
                        m_Message = "Updating Decoded EdId " + intRowCount.ToString() + " of " + gapInEdIdDecoded.Rows.Count.ToString();
                        intRowCount++;
                        if (!String.IsNullOrEmpty(Convert.ToString(item["EDID"])))
                        {
                            try
                            {
                                Byte[] ediddata = Convert.FromBase64String(item["EDID"].ToString());
                                if (ediddata != null)
                                {
                                    m_DBAccess.SetEdIdValidity(Int64.Parse(item["EDID_RID"].ToString()), 1);
                                    m_EDIDObject = m_EDIDDecoder.ParseEDID(ediddata);
                                    DecodedEDIDData decodedEdId = GetDecodedEDIDDataFromEDIDObject(m_EDIDObject);
                                    decodedEdId.FK_ConsoleEDID_RID = UInt64.Parse(item["EDID_RID"].ToString());
                                    DataRow decodedEDIDRow = CreateDecodedEdIdRow(DecodedEDIDTableObj, decodedEdId);
                                    DecodedEDIDTableObj.Rows.Add(decodedEDIDRow);
                                }
                            }
                            catch { }
                        }
                    }
                    if (DecodedEDIDTableObj.Rows.Count > 0)
                    {
                        try
                        {
                            Int32 recordUpdated = 0;
                            recordUpdated = m_DBAccess.UpdateDecodedEdId(DecodedEDIDTableObj);                           
                        }
                        catch (Exception exp1)
                        {
                            this.ErrorMessage = exp1.Message;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
        }
        #endregion

        #region Update Linked ID
        public void UpdateLinkedID(EDIDInfo edid)
        {
            this.ErrorMessage = String.Empty;
            
            try
            {
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandText = "[dbo].[EDID_Sp_UpdateFingerPrintTrustLevels]";
                objCmd.CommandType = CommandType.StoredProcedure;                                                               
                objCmd.Parameters.Add(objDb.CreateParameter("@vcharMainDevice", DbType.String, ParameterDirection.Input, edid.MainDevice.Length, edid.MainDevice));
                objCmd.Parameters.Add(objDb.CreateParameter("@vcharSubDevice", DbType.String, ParameterDirection.Input, edid.SubDevice.Length, edid.SubDevice));
                objCmd.Parameters.Add(objDb.CreateParameter("@vcharBrand", DbType.String, ParameterDirection.Input, edid.Brand.Length, edid.Brand));
                objCmd.Parameters.Add(objDb.CreateParameter("@vcharModel", DbType.String, ParameterDirection.Input, edid.Model.Length, edid.Model));
                if (!String.IsNullOrEmpty(edid.Region))
                    objCmd.Parameters.Add(objDb.CreateParameter("@vcharCountry", DbType.String, ParameterDirection.Input, edid.Region.Length, edid.Region));
                objCmd.Parameters.Add(objDb.CreateParameter("@vcharDataSource", DbType.String, ParameterDirection.Input, edid.DataSource.Length, edid.DataSource));
                objCmd.Parameters.Add(objDb.CreateParameter("@vcharLinkedID", DbType.String, ParameterDirection.Input, edid.DACPublishedID.Length, edid.DACPublishedID));
                DataTable dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult.Rows.Count > 0)
                {
                    if (!String.IsNullOrEmpty(dtResult.Rows[0][0].ToString()))
                    {
                        this.ErrorMessage = dtResult.Rows[0][0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }            
        }
        #endregion
    }
}