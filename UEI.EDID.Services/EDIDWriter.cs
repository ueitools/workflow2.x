﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UEI.EDID
{
    public class EDIDWriter
    {
        #region Variables
        public Boolean _combinedchecksum = false;
        public Boolean _isCustomFPAvailable = false;
        public Header _header;
        public TOCHeader _tocheader;
        public TOCCollection _toc;
        public TOCHeader _edidHeader;
        public TOCCollection _edidToc;
        public TOCHeader _osdHeader;
        public TOCCollection _osdToc;
        public RawData _edidrawdata;
        public RawData _osdrawdata;
        public TOCHeader _edidchksmHeader;
        public Checksum _edidchksm;
        public TOCHeader _osdchksmHeader;
        public Checksum _osdchksm;
        public TOCHeader _combinedchksmHeader;
        public Checksum _combinedchksm;
        private Int16 _headersize = 0;
        private Int16 _tocheadersize = 0;
        private Int16 _tocsize = 0;
        private Int16 _edidheadersize = 0;
        private Int16 _edidtocsize = 0;
        private Int16 _edidsize = 0;
        private Int16 _osdheadersize = 0;
        private Int16 _osdtocsize = 0;
        private Int16 _osdsize = 0;
        private Int16 _osdchkheadersize = 0;
        private Int16 _osdStartAddress = 0;
        public TOCHeaderFPBitPos _customFPByteHeader;
        public TOCHeader _customFpHeader;
        public Checksum _customFp;

        #endregion

        #region Properties
        #endregion

        #region Constrcutor
        public EDIDWriter()
        {
            _header = new Header();
            _tocheader = new TOCHeader();
            _toc = new TOCCollection();
            _toc.toccollection = new List<TOC>();
            _edidHeader = new TOCHeader();
            _edidToc = new TOCCollection();
            _edidToc.toccollection = new List<TOC>();
            _osdHeader = new TOCHeader();
            _osdToc = new TOCCollection();
            _osdToc.toccollection = new List<TOC>();
            _edidrawdata = new RawData();
            _osdrawdata = new RawData();
            _edidchksmHeader = new TOCHeader();
            _edidchksm = new Checksum();
            _osdchksmHeader = new TOCHeader();
            _osdchksm = new Checksum();
            _combinedchksmHeader = new TOCHeader();
            _combinedchksm = new Checksum();
        }
        #endregion

        #region Sub Classess
        public class Header
        {
            public Header()
            {
                _version = new Byte[2];
                _flags = new Byte[2];
            }
            public String type;
            public Int16 version;
            public Int16 flags;
            public Byte[] _type;
            public Byte[] _version;
            public Byte[] _flags;
            public void ConvertToBytes()
            {
                _type = System.Text.UTF8Encoding.UTF8.GetBytes(type);
                _version = BitConverter.GetBytes(version);
                _flags = BitConverter.GetBytes(flags);
            }
            public List<Byte> GetBytes()
            {
                List<Byte> _bytes = new List<Byte>();
                foreach (Byte by in _type)
                {
                    _bytes.Add(by);
                }
                foreach (Byte by in _version)
                {
                    _bytes.Add(by);
                }
                foreach (Byte by in _flags)
                {
                    _bytes.Add(by);
                }
                return _bytes;
            }
        }
        public class TOCCollection
        {
            public TOCCollection()
            {
                toccollection = new List<TOC>();
                _toccollection = new List<List<Byte>>();
            }
            public List<TOC> toccollection;
            public List<List<Byte>> _toccollection;
            public void ConvertToBytes()
            {
                foreach (TOC _toc in toccollection)
                {
                    _toc.ConvertToByte();
                }
            }
            public List<Byte> GetBytes()
            {
                List<Byte> _bytes = new List<Byte>();
                _toccollection = new List<List<Byte>>();
                foreach (TOC _toc in toccollection)
                {
                    _toccollection.Add(_toc.GetBytes());
                }

                foreach (List<Byte> _byt in _toccollection)
                {
                    foreach (Byte by in _byt)
                    {
                        _bytes.Add(by);
                    }
                }
                return _bytes;
            }
        }
        public class TOC
        {
            public TOC()
            {
                lettercount = 0;
                type = String.Empty;
                offset = 0;
            }
            public Int16 lettercount = 0;
            public String type;
            public Int16 size;
            public Int32 offset;
            public Byte[] _lettercount;
            public Byte[] _type;
            public Byte[] _size;
            public Byte[] _offset;
            public void ConvertToByte()
            {
                _lettercount = BitConverter.GetBytes(lettercount);
                _type = System.Text.UTF8Encoding.UTF8.GetBytes(type);
                _size = BitConverter.GetBytes(size);
                _offset = BitConverter.GetBytes(offset);
            }
            public List<Byte> GetBytes()
            {
                List<Byte> _bytes = new List<Byte>();
                foreach (Byte by in _lettercount)
                {
                    _bytes.Add(by);
                }
                foreach (Byte by in _type)
                {
                    _bytes.Add(by);
                }
                foreach (Byte by in _size)
                {
                    _bytes.Add(by);
                }
                foreach (Byte by in _offset)
                {
                    _bytes.Add(by);
                }
                return _bytes;
            }
        }
        public class TOCHeader
        {
            public TOCHeader()
            {
                type = String.Empty;
                size = 0;

            }
            public String type;
            public Int16 size;

            public Byte[] _type;
            public Byte[] _size;

            public void ConvertToByte()
            {
                _type = System.Text.UTF8Encoding.UTF8.GetBytes(type);
                _size = BitConverter.GetBytes(size);
            }
            public List<Byte> GetBytes()
            {
                List<Byte> _bytes = new List<Byte>();
                foreach (Byte by in _type)
                {
                    _bytes.Add(by);
                }
                foreach (Byte by in _size)
                {
                    _bytes.Add(by);
                }
                return _bytes;
            }
        }
        public class RawData
        {
            public RawData()
            {
                _rawdata = new List<Byte>();
            }
            public List<Byte> _rawdata;
            public void ConvertToBytes()
            {
            }
            public List<Byte> GetBytes()
            {
                return _rawdata;
            }
        }
        public class Checksum
        {
            public String stringCheckSum;
            public long checksum;
            public List<Byte> _binstream = new List<Byte>();
            public Byte[] _checksum;
            public void ConvertToByte()
            {
                _checksum = BitConverter.GetBytes(checksum);                
            }
            public void ConvertToString()
            {
                stringCheckSum = checksum.ToString("X8");
            }
            public List<Byte> GetBytes()
            {
                List<Byte> data = new List<Byte>();

                foreach (Byte by in _checksum)
                {
                    data.Add(by);
                }
                return data;
            }
            public void CalculateChecksum()
            {
                ICSharpCode.SharpZipLib.Checksums.Crc32 crc_32 = new ICSharpCode.SharpZipLib.Checksums.Crc32();
                crc_32.Update(_binstream.ToArray(), 0, _binstream.Count);
                checksum = crc_32.Value;
                this.ConvertToByte();
                this.ConvertToString();
            }
        }
        public class TOCHeaderFPBitPos
        {
            public TOCHeaderFPBitPos()
            {
                type = String.Empty;
                size = 0;
            }
            public String type;
            public Int16 size;
            public String bitPos;
            public Byte[] _type;
            public Byte[] _size;
            public Byte[] _byteBitPos;
            public void ConvertToByte()
            {
                _type = System.Text.UTF8Encoding.UTF8.GetBytes(type);
                _size = BitConverter.GetBytes(bitPos.Length);
                _byteBitPos = System.Text.UTF8Encoding.UTF8.GetBytes(bitPos);
            }
            public List<Byte> GetBytes()
            {
                List<Byte> _bytes = new List<Byte>();
                foreach (Byte by in _type)
                {
                    _bytes.Add(by);
                }
                foreach (Byte by in _size)
                {
                    _bytes.Add(by);
                }
                foreach (Byte by in _byteBitPos)
                {
                    _bytes.Add(by);
                }
                return _bytes;
            }
        }
        #endregion

        #region Methods
        public void SetHeader(String type, Int16 version, Int16 flags)
        {
            _header = new Header();
            _header.type = type;
            _header.version = version;
            _header.flags = flags;
            _header.ConvertToBytes();
        }
        public void SetTocHeader(String type)
        {
            _tocheader = new TOCHeader();
            _tocheader.type = type;
            _tocheader.size = 0;
            _tocheader.ConvertToByte();
        }
        public void SetTocCollection(String type)
        {
            TOC _t = new TOC();
            _t.lettercount = (Int16)type.Length;
            _t.type = type;
            _t.size = 0;
            _t.offset = 0;
            _t.ConvertToByte();
            _toc.toccollection.Add(_t);
        }
        public void SetEdidTocHeader(String type)
        {
            _edidHeader = new TOCHeader();
            _edidHeader.type = type;
            _edidHeader.size = 0;
            _edidHeader.ConvertToByte();
        }
        public void SetEdidTocCollection(String type, Int16 size)
        {
            TOC _etoc = new TOC();
            _etoc.lettercount = (Int16)type.Length;
            _etoc.type = type;
            _etoc.size = size;
            _etoc.offset = 0;

            _etoc.ConvertToByte();
            _edidToc.toccollection.Add(_etoc);
        }
        public void SetOSDTocHeader(String type)
        {
            _osdHeader = new TOCHeader();
            _osdHeader.type = type;
            _osdHeader.size = 0;
            _osdHeader.ConvertToByte();
        }
        public void SetOSDTocCollection(String type, Int16 size)
        {
            TOC _ostoc = new TOC();
            _ostoc.lettercount = (Int16)type.Length;
            _ostoc.type = type;
            _ostoc.size = size;
            _ostoc.offset = 0;
            _ostoc.ConvertToByte();
            _osdToc.toccollection.Add(_ostoc);
        }
        public void SetEdidRawData(List<Byte> _rawdata)
        {
            _edidrawdata._rawdata = _rawdata;
        }
        public void SetOSDRawData(List<Byte> _rawdata)
        {
            _osdrawdata._rawdata = _rawdata;
        }
        public void SetEdidChecksumHeader(String type)
        {
            _edidchksmHeader.type = type;
            _edidchksmHeader.size = 0;
            _edidchksmHeader.ConvertToByte();
        }
        public void CalculateEdidChecksum(List<Byte> data)
        {
            _edidchksm._binstream = data;
            _edidchksm.CalculateChecksum();
        }
        public void SetOSDChecksumHeader(String type)
        {
            _osdchksmHeader.type = type;
            _osdchksmHeader.size = 0;
            _osdchksmHeader.ConvertToByte();
        }
        public void CalculateOSDChecksum(List<Byte> data)
        {
            _osdchksm._binstream = data;
            _osdchksm.CalculateChecksum();
        }
        public void SetCombinedChecksumHeader(String type)
        {
            _combinedchksmHeader.type = type;
            _combinedchksmHeader.size = 0;
            _combinedchksmHeader.ConvertToByte();
        }
        public void CalculateCombinedChecksum(List<Byte> data)
        {
            _combinedchksm._binstream = data;
            _combinedchksm.CalculateChecksum();
        }
        public void SetCustomFPBitHeader(String type, String bitpos)
        {
            _customFPByteHeader = new TOCHeaderFPBitPos();
            _customFPByteHeader.type = type;
            _customFPByteHeader.size = 0;
            _customFPByteHeader.bitPos = bitpos;
            _customFPByteHeader.ConvertToByte();
        }
        public void SetCustomFPHeader(String type)
        {
            _customFpHeader = new TOCHeader();
            _customFpHeader.type = type;
            _customFpHeader.size = 8;//currently hard coded to 8 bytes, later needs to be changed
            _customFpHeader.ConvertToByte();
        }
        public void CalculateCustomFP(List<Byte> data)
        {
            _customFp = new Checksum();
            _customFp._binstream = data;
            _customFp.CalculateChecksum();
        }
        private void SetTC(Int16 size)
        {
            _tocheader.size = size;
            _tocheader.ConvertToByte();
        }
        private void SetTOCOffset(String type, Int16 offset)
        {
            for (int i = 0; i < _toc.toccollection.Count; i++)
            {
                if (type == _toc.toccollection[i].type)
                {
                    _toc.toccollection[i].offset = offset;
                    _toc.toccollection[i].ConvertToByte();
                    break;
                }
            }
        }
        private void SetToCCollectionOffset(String type, Int16 size, Int32 offset)
        {
            for (int i = 0; i < _toc.toccollection.Count; i++)
            {
                if (type == _toc.toccollection[i].type)
                {
                    _toc.toccollection[i].size = size;
                    _toc.toccollection[i].offset = offset;
                    _toc.toccollection[i].ConvertToByte();
                }
            }
        }
        private void SetEdidHeaderSize()
        {
            _edidHeader.size = (Int16)_edidToc.toccollection.Count;
            _edidHeader.ConvertToByte();
        }
        private void SetEdidTocCollectionOffset(Int16 startaddressEdid)
        {
            Int16 _nextaddress = startaddressEdid;
            for (int i = 0; i < _edidToc.toccollection.Count; i++)
            {
                _edidToc.toccollection[i].offset = _nextaddress;
                _nextaddress += _edidToc.toccollection[i].size;
                _edidToc.toccollection[i].ConvertToByte();
            }
        }
        private void SetOSDHeaderSize()
        {
            _osdHeader.size = (Int16)_osdToc.toccollection.Count;
            _osdHeader.ConvertToByte();
        }
        private void SetOSDTocCollectionOffset(Int16 startaddressEdid)
        {
            Int16 _nextaddress = startaddressEdid;
            for (int i = 0; i < _osdToc.toccollection.Count; i++)
            {
                _osdToc.toccollection[i].offset = _nextaddress;
                _nextaddress += _edidToc.toccollection[i].size;
                _osdToc.toccollection[i].ConvertToByte();
            }
        }
        public void CalculateAddress()
        {
            List<Byte> _allBytes = new List<Byte>();

            #region HEADER
            List<Byte> _temp = _header.GetBytes();
            foreach (Byte by in _temp)
            {
                _allBytes.Add(by);
            }
            _headersize = (Int16)_allBytes.Count;
            _temp = _tocheader.GetBytes();
            foreach (Byte by in _temp)
            {
                _allBytes.Add(by);
            }
            _tocheadersize = (Int16)_temp.Count;
            #endregion

            #region TOC HEADER
            _temp = _toc.GetBytes();
            foreach (Byte by in _temp)
            {
                _allBytes.Add(by);
            }
            _tocsize = (Int16)_temp.Count;
            SetTC(_tocsize);
            #endregion

            #region EDID HEADER
            Int32 _edidStartaddress = (Int32)(_headersize + _tocheadersize + _tocsize);
            _temp = _edidHeader.GetBytes();
            foreach (Byte by in _temp)
            {
                _allBytes.Add(by);
            }
            _edidheadersize = (Int16)(_temp.Count);
            #endregion

            #region EDID TOC
            Int16 _edidtocstartaddress = (Int16)(_allBytes.Count - _edidheadersize);
            _temp = _edidToc.GetBytes();
            foreach (Byte by in _temp)
            {
                _allBytes.Add(by);
            }
            _edidtocsize = (Int16)(_temp.Count);
            SetToCCollectionOffset("EDID", (Int16)(_edidtocsize + _edidheadersize), _edidtocstartaddress);
            #endregion

            #region EDID Checksum Header
            //int _edidchhdsize = 0;
            #endregion
            
            #region Set IDID Header Size
            SetEdidHeaderSize();
            Int16 _edidrawstartaddress = (Int16)(_headersize + _tocheadersize + _tocsize + _edidheadersize + _edidtocsize + _osdheadersize + _osdtocsize);
            SetEdidTocCollectionOffset(_edidrawstartaddress);
            #endregion

            #region EDID Raw data
            _temp = _edidrawdata.GetBytes();
            foreach (Byte by in _temp)
            {
                _allBytes.Add(by);
            }

            _edidsize = (Int16)(_temp.Count);
            #endregion

            #region EDID Checksum
            _temp = new List<Byte>();
            Int16 _edidchksmstartaddress = (Int16)_allBytes.Count;
            _temp = _edidchksm.GetBytes();
            foreach (Byte by in _temp)
            {
                _allBytes.Add(by);
            }
            Int16 _edidchksize = (Int16)_temp.Count;
            SetToCCollectionOffset("EDCHKSM", _edidchksize, _edidchksmstartaddress);
            #endregion

            #region OSD raw data
            Int16 _osdstartaddress = (Int16)_allBytes.Count;
            _temp = new List<Byte>();
            if (_osdrawdata._rawdata.Count > 0)
            {
                _temp = _osdrawdata.GetBytes();
                foreach (Byte by in _temp)
                {
                    _allBytes.Add(by);
                }
                _osdsize = (Int16)(_temp.Count);
                SetToCCollectionOffset("OSD", _osdsize, _osdstartaddress);
            }
            #endregion

            #region OSD checksum
            if (_osdrawdata._rawdata.Count > 0)
            {
                Int16 _osdchkstartaddress = (Int16)_allBytes.Count;
                _temp = new List<Byte>();
                _temp = _osdchksm.GetBytes();
                foreach (Byte by in _temp)
                {
                    _allBytes.Add(by);
                }
                Int16 _osdchksize = (Int16)_temp.Count;
                SetToCCollectionOffset("OSDCHKSM", _osdchksize, _osdchkstartaddress);
            }
            #endregion

            #region Combined checksum
            Int16 _combinedchkstartaddress = (Int16)_allBytes.Count;

            if (_combinedchecksum)
            {
                _temp = new List<Byte>();
                _temp = _combinedchksm.GetBytes();
                foreach (Byte by in _temp)
                {
                    _allBytes.Add(by);
                }
                Int16 _combinedchksize = (Int16)_temp.Count;
                SetToCCollectionOffset("CHKSM", _combinedchksize, _combinedchkstartaddress);
            }
            #endregion

            #region Custom FP Header & Checksum
            Int16 _customfpstartaddress = (Int16)_allBytes.Count;
            if (_isCustomFPAvailable)
            {
                _temp = new List<byte>();
                _temp = _customFPByteHeader.GetBytes();
                foreach (byte by in _temp)
                {
                    _allBytes.Add(by);
                }
                Int16 _combinedchksize = (Int16)_temp.Count;
                SetToCCollectionOffset("FPBYDT", _combinedchksize, _customfpstartaddress);

                Int16 _customfpdatastartaddress = (Int16)_allBytes.Count;
                _temp = new List<byte>();
                _temp = _customFp.GetBytes();
                foreach (byte by in _temp)
                {
                    _allBytes.Add(by);
                }
                Int16 _chksize = (Int16)_temp.Count;
                SetToCCollectionOffset("FP", _chksize, _customfpdatastartaddress);
            }
            #endregion
            //return _allBytes;
        }
        public List<Byte> GetBytes()
        {
            List<Byte> _allBytes = new List<Byte>();

            #region HEADER
            List<Byte> _temp = _header.GetBytes();
            foreach (Byte by in _temp)
            {
                _allBytes.Add(by);
            }
            _temp = _tocheader.GetBytes();
            foreach (Byte by in _temp)
            {
                _allBytes.Add(by);
            }
            #endregion

            #region TOC HEADER
            _temp = _toc.GetBytes();
            foreach (Byte by in _temp)
            {
                _allBytes.Add(by);
            }
            #endregion

            #region EDID HEADER
            _temp = _edidHeader.GetBytes();
            foreach (Byte by in _temp)
            {
                _allBytes.Add(by);
            }
            #endregion

            #region EDID TOC
            _temp = _edidToc.GetBytes();
            foreach (Byte by in _temp)
            {
                _allBytes.Add(by);
            }
            #endregion


            #region OSD Header andd TOC
            #endregion

            #region EDID Raw data
            _temp = _edidrawdata.GetBytes();
            foreach (Byte by in _temp)
            {
                _allBytes.Add(by);
            }
            #endregion

            #region EDID Checksum
            _temp = new List<Byte>();
            _temp = _edidchksm.GetBytes();
            foreach (Byte by in _temp)
            {
                _allBytes.Add(by);
            }
            #endregion

            #region OSD raw data
            _temp = new List<Byte>();
            if (_osdrawdata._rawdata.Count > 0)
            {
                _temp = _osdrawdata.GetBytes();
                foreach (Byte by in _temp)
                {
                    _allBytes.Add(by);
                }
            }
            #endregion

            #region OSD checksum
            if (_osdrawdata._rawdata.Count > 0)
            {
                _temp = new List<Byte>();
                _temp = _osdchksm.GetBytes();
                foreach (Byte by in _temp)
                {
                    _allBytes.Add(by);
                }
            }            
            #endregion

            #region Combined checksum
            if (_combinedchecksum)
            {
                _temp = new List<Byte>();
                _temp = _combinedchksm.GetBytes();
                foreach (Byte by in _temp)
                {
                    _allBytes.Add(by);
                }
            }

            #endregion

            #region Custom FP
            if (_isCustomFPAvailable)
            {
                #region FP Bit Data
                _temp = new List<byte>();
                _temp = _customFPByteHeader.GetBytes();
                foreach (byte by in _temp)
                {
                    _allBytes.Add(by);
                }
                #endregion

                #region Custom FP
                _temp = new List<byte>();
                _temp = _customFpHeader.GetBytes();
                foreach (byte by in _temp)
                {
                    _allBytes.Add(by);
                }

                _temp = new List<byte>();
                _temp = _customFp.GetBytes();
                foreach (byte by in _temp)
                {
                    _allBytes.Add(by);
                }
                #endregion
            }
            #endregion

            return _allBytes;
        }
        #endregion
    }
}