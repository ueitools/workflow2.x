﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UEI.EDID
{
    public class Conversion
    {      
        #region Methods
        public static String ConvertByteStreamToString(List<Byte> data)
        {
            String _data = String.Empty;
            foreach (Byte b in data)
            {
                String _temp = b.ToString("X");
                _data += _temp + " ";
            }
            return _data;
        }
        public static List<Byte> ConvertToLittleEndian(List<Byte> byteData)
        {
            List<Byte> _littelEndian = new List<Byte>();
            for (int Len = byteData.Count - 1; Len >= 0; Len--)
            {
                _littelEndian.Add(byteData[Len]);
            }
            return _littelEndian;
        }
        public static Byte[] ConvertToLittleEndian(Byte[] byteData)
        {
            Byte[] _littelEndian = new Byte[byteData.Length];
            int counter = 0;
            for (int Len = byteData.Length - 1; Len >= 0; Len--)
            {
                _littelEndian[counter] = byteData[Len];
                counter++;
            }
            return _littelEndian;
        }
        public static Byte[] ConvertToLittleEndian(Byte[] byteData, int byteLen)
        {
            Byte[] _littelEndian = new Byte[byteLen];
            Byte[] _tempData = new Byte[byteLen];

            if (byteData.Length < byteLen)
            {
                int len = byteLen - byteData.Length;
                int counter = byteLen - 1;
                for (int i = len - 1; i >= 0; i--)
                {
                    _tempData[counter] = (Byte)0;
                }
                for (int j = 0; j < byteData.Length; j++)
                {
                    _tempData[j] = byteData[j];
                }
            }
            int _counter = 0;
            for (int Len = _tempData.Length - 1; Len >= 0; Len--)
            {
                _littelEndian[_counter] = byteData[Len];
                _counter++;
            }
            return _littelEndian;
        }
        public static Byte[] ConvertTo4ByteLittleEndian(int intData)
        {
            Byte[] _littleEndian = new Byte[4];
            _littleEndian[3] = (Byte)((intData >> 24) & 0xff);
            _littleEndian[2] = (Byte)((intData >> 16) & 0xff);
            _littleEndian[1] = (Byte)((intData >> 8) & 0xff);
            _littleEndian[0] = (Byte)(intData & 0xff);

            return _littleEndian;
        }
        public static Byte[] ConvertTo3ByteLittleEndian(int intData)
        {
            Byte[] _littleEndian = new Byte[3];
            _littleEndian[2] = (Byte)((intData >> 16) & 0xff);
            _littleEndian[1] = (Byte)((intData >> 8) & 0xff);
            _littleEndian[0] = (Byte)(intData & 0xff);
            return _littleEndian;
        }
        public static Byte[] ConvertTo2ByteLittleEndian(int intData)
        {
            Byte[] _littleEndian = new Byte[2];
            _littleEndian[1] = (Byte)((intData >> 8) & 0xff);
            _littleEndian[0] = (Byte)(intData & 0xff);
            return _littleEndian;
        }
        //public static Byte[] ConvertToByte(int _data,int byteSize)
        //{
        //    Byte[] _retData = new Byte[byteSize];

        //    _retData=ConvertToByte(

        //    //try
        //    //{
        //    //    //Byte[] _tempData = Encoding.ASCII.GetBytes(_data.ToString());
        //    //    Byte[] _tempData = ConvertToByte(_data);
        //    //    Byte[] _newTempData = new Byte[byteSize];
        //    //    if (_tempData.Length > byteSize)
        //    //    {
        //    //        for(int size=0;size<byteSize;size++)
        //    //        {
        //    //            _newTempData[size] = _tempData[size];
        //    //        }
        //    //        _tempData = new Byte[byteSize];
        //    //        _tempData = _newTempData;
        //    //    }

        //    //    for (int i = 0; i < _tempData.Length; i++)
        //    //    {
        //    //        _retData[i] = _tempData[i];
        //    //    }

        //    //    if (_retData.Length < byteSize)
        //    //    {
        //    //        int len = byteSize - _retData.Length;
        //    //        for (int i = len; i > 0; i--)
        //    //        {
        //    //            _retData[i] = (Byte)0;
        //    //        }

        //    //    }

        //        return _retData;
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    throw ex;
        //    //}
        //}
        public static Byte[] ConvertToByte(int _data)
        {
            return BitConverter.GetBytes(_data);
        }
        public static Byte[] ConvertToByte(int data, int bytesize)
        {
            Byte[] retdata = new Byte[bytesize];
            switch (bytesize)
            {
                //case 1:
                //    {
                //        retdata[0] = (Byte)(data & 0xff);
                //        break;
                //    }
                //case 2:
                //    {
                //        retdata[0] = (Byte)(data & 0xff);
                //        retdata[1] = (Byte)((data >> 8) & 0xff);
                //        break;
                //    }
                //case 3:
                //    {
                //        retdata[0] = (Byte)(data & 0xff);
                //        retdata[1] = (Byte)((data >> 8) & 0xff);
                //        retdata[2] = (Byte)((data >> 16) & 0xff);
                //        break;
                //    }
                //case 4:
                //    {
                //        retdata[0] = (Byte)(data & 0xff);
                //        retdata[1] = (Byte)((data >> 8) & 0xff);
                //        retdata[2] = (Byte)((data >> 16) & 0xff);
                //        retdata[3] = (Byte)((data >> 24) & 0xff);
                //        break;
                //    }
                case 1:
                    {
                        retdata[0] = (Byte)(data & 0xff);
                        break;
                    }
                case 2:
                    {
                        retdata[1] = (Byte)(data & 0xff);
                        retdata[0] = (Byte)((data >> 8) & 0xff);
                        break;
                    }
                case 3:
                    {
                        retdata[2] = (Byte)(data & 0xff);
                        retdata[1] = (Byte)((data >> 8) & 0xff);
                        retdata[0] = (Byte)((data >> 16) & 0xff);
                        break;
                    }
                case 4:
                    {
                        retdata[3] = (Byte)(data & 0xff);
                        retdata[2] = (Byte)((data >> 8) & 0xff);
                        retdata[1] = (Byte)((data >> 16) & 0xff);
                        retdata[0] = (Byte)((data >> 24) & 0xff);
                        break;
                    }
            }
            return retdata;
        }
        public static Byte[] ConvertToByte(String _data, int byteSize)
        {
            Byte[] _retData = new Byte[byteSize];
            try
            {
                _retData = Encoding.ASCII.GetBytes(_data.Trim());
                return _retData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Byte[] ConvertUTF16ToUTF8(String utf16data)
        {
            Byte[] utf16Bytes = Encoding.Unicode.GetBytes(utf16data);
            Byte[] utf8Bytes = Encoding.Convert(Encoding.Unicode, Encoding.UTF8, utf16Bytes);
            return utf8Bytes;
        }
        public static Byte[] ConvertUTF32ToUTF8(String utf32data)
        {
            Byte[] utf32Bytes = Encoding.Unicode.GetBytes(utf32data);
            Byte[] utf8Bytes = Encoding.Convert(Encoding.Unicode, Encoding.UTF8, utf32Bytes);
            return utf8Bytes;
        }
        public static Byte[] ConvertUTF8ToUTF16(String utf8data)
        {
            //UTF8 bytes
            Byte[] utf8Bytes = Encoding.UTF8.GetBytes(utf8data);
            //Converting to Unicode from UTF8 bytes
            Byte[] unicodeBytes = Encoding.Convert(Encoding.UTF8, Encoding.Unicode, utf8Bytes);
            return unicodeBytes;
        }
        public static String ConvertByteArrayToUTF8(Byte[] data)
        {
            String _data = Encoding.UTF8.GetString(data);
            return _data;
        }
        //public static String ConvertByteArrayToISO3166(Byte[] data)
        //{
        //    Encoding isoEnc = Encoding.GetEncoding("iso-8859-1");
        //    String utfResult = Encoding.UTF8.GetString(isoEnc.GetBytes(myNode.InnerText));
        //}
        public static Byte[] ConvertISO3166toByte(String data)
        {
            Encoding isoEnc = Encoding.GetEncoding("iso 3166-1");
            return isoEnc.GetBytes(data);
        }
        //public static String ConvertByteArrayToISO3166(Byte[] data)
        //{
        //    Encoding isoEnc = Encoding.GetEncoding("iso 3166-1");
        //    String _data = Encoding.UTF8.GetString();
        //}
        public static String ConvertByteToString(Byte[] data)
        {
            return Encoding.ASCII.GetString(data);
        }
        public static int ConvertByteArrayToInt32(Byte[] data)
        {
            if (data.Length == 3)
            {
                Byte[] _temp = new Byte[4];
                _temp[3] = 0x00;
                _temp[2] = data[2];
                _temp[1] = data[1];
                _temp[0] = data[0];
                data = _temp;

            }
            return BitConverter.ToInt32(data, 0);
        }
        public static int ConvertByteArrayToInt16(Byte[] data)
        {
            return BitConverter.ToInt16(data, 0);
        }
        public static int ConvertBytetoInt(Byte data)
        {
            return (int)data;
        }
        private const string hexDigits = "0123456789ABCDEF";
        public static byte[] HexStringToBytes(string str)
        {
            // Determine the number of bytes
            byte[] bytes = new byte[str.Length >> 1];
            for (int i = 0; i < str.Length; i += 2)
            {
                int highDigit = hexDigits.IndexOf(Char.ToUpperInvariant(str[i]));
                int lowDigit = hexDigits.IndexOf(Char.ToUpperInvariant(str[i + 1]));
                if (highDigit == -1 || lowDigit == -1)
                {
                    //throw new ArgumentException("The string contains an invalid digit.", "s");
                    throw new ArgumentException("The string contains an invalid digit.");
                }
                bytes[i >> 1] = (byte)((highDigit << 4) | lowDigit);
            }
            return bytes;
        }
        public static byte[] ConvertHexToBytes(string input)
        {
            var result = new byte[(input.Length + 1) / 2];
            var offset = 0;
            if (input.Length % 2 == 1)
            {
                // If length of input is odd, the first character has an implicit 0 prepended.
                result[0] = (byte)Convert.ToUInt32(input[0] + "", 16);
                offset = 1;
            }
            for (int i = 0; i < input.Length / 2; i++)
            {
                result[i + offset] = (byte)Convert.ToUInt32(input.Substring(i * 2 + offset, 2), 16);
            }
            return result;
        }
        #endregion
    }
}