﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UEI.EDID
{
    public class CalculateFingerprint
    {
        #region Properties
        public String EdIdHexData { get; set; }
        public String EdIdBase64String { get; set; }
        public String ErrorMessage { get; set; }
        #endregion

        #region Get EDID with Fingerprint
        public EDIDInfo GetEdIdWithFingerprint()
        {
            this.ErrorMessage = String.Empty;
            EDIDInfo m_edid = null;
            try
            {
                if ((String.IsNullOrEmpty(this.EdIdHexData)) && (String.IsNullOrEmpty(this.EdIdBase64String)))
                    this.ErrorMessage = "EDID data not provided";
                else
                {
                    String[] sep = { " " };
                    if (!String.IsNullOrEmpty(this.EdIdHexData))
                    {
                        m_edid = new EDIDInfo();
                        this.EdIdHexData = this.EdIdHexData.Trim();
                        if (this.EdIdHexData.EndsWith("\n"))
                        {
                            this.EdIdHexData = this.EdIdHexData.Replace("\n", "");
                        }
                        m_edid.EdIdRawData = this.EdIdHexData;
                        m_edid.EdId = new List<Byte>();
                        String[] data = this.EdIdHexData.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                        if (data.Length > 1)
                        {
                            foreach (String bytedata in data)
                            {
                                m_edid.EdId.Add((Byte)Convert.ToInt16(bytedata.Trim(), 16));
                            }
                        }
                        Byte[] _bindatatemp = CreateBinFile(m_edid);
                        m_edid = ReadBinFile(_bindatatemp);
                    }
                    else if (!String.IsNullOrEmpty(this.EdIdBase64String))
                    {
                        if (this.EdIdBase64String.EndsWith("|0") || this.EdIdBase64String.EndsWith("|1"))
                        {
                            this.EdIdBase64String = this.EdIdBase64String.Remove(this.EdIdBase64String.Length - 2, 2);
                        }
                        Byte[] bindata = Convert.FromBase64String(this.EdIdBase64String);
                        if (bindata != null)
                        {
                            List<Byte> byteData = new List<Byte>();
                            foreach (Byte item in bindata)
                            {
                                byteData.Add(item);
                            }
                            //Convert to Our bin file Strcuture for EDID Reader
                            Byte[] _bindatatemp = ConvertEdidToBin(byteData);
                            if (_bindatatemp != null)
                            {
                                m_edid = new EDIDInfo();
                                m_edid = ReadBinFile(_bindatatemp);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return m_edid;
        }
        public EDIDInfo GetEdIdWithFingerprint(Byte[] m_FileStructure)
        {
            this.ErrorMessage = String.Empty;
            EDIDInfo m_edid = null;
            try
            {
                if (m_FileStructure == null)
                    this.ErrorMessage = "EDID data not provided";
                else
                {
                    String[] sep = { " " };
                    List<Byte> byteData = new List<Byte>();
                    foreach (Byte item in m_FileStructure)
                    {
                        byteData.Add(item);
                    }
                    //Convert to Our bin file Strcuture for EDID Reader
                    Byte[] _bindatatemp = ConvertEdidToBin(byteData);
                    if (_bindatatemp != null)
                    {
                        m_edid = new EDIDInfo();
                        m_edid = ReadBinFile(_bindatatemp);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return m_edid;
        }
        #endregion

        #region Read Bin File
        public EDIDInfo ReadBinFile(Byte[] m_FileStructure)
        {
            EDIDInfo m_EdId = new EDIDInfo();
            try
            {
                String[] sep = { " " };
                EDIDReader m_EdIdReader = new EDIDReader();
                m_EdIdReader.ReadBinFile(m_FileStructure);
                m_EdId.EdIdRawData = m_EdIdReader.GetEdidData();
                m_EdId.OSDRawData = m_EdIdReader.GetOSDData();
                String[] data = m_EdId.EdIdRawData.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                foreach (String bytedata in data)
                {
                    if (m_EdId.EdId == null)
                        m_EdId.EdId = new List<Byte>();
                    m_EdId.EdId.Add((Byte)Convert.ToInt32(bytedata, 16));
                }
                data = m_EdId.OSDRawData.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                foreach (String bytedata in data)
                {
                    if (m_EdId.OSD == null)
                        m_EdId.OSD = new List<Byte>();
                    m_EdId.OSD.Add((Byte)Convert.ToInt32(bytedata, 16));
                }
                m_EdIdReader.GetEDIDChecksum();
                m_EdIdReader.GetOSDChecksum();
                m_EdIdReader.GetCombinedChecksum();


                m_EdId.FPBytePosition = m_EdIdReader.GetCustomFPBitPositions();
                if (m_EdId.FPBytePosition != null)
                {
                    if (m_EdId.FPBytePosition.ToString().Trim() != String.Empty)
                    {
                        m_EdId.FPAvailable = "Y";
                        ValidateBytePosition(m_EdId);
                    }
                }
                m_EdId.CustomFP = GetFingerPintCheckSum(m_EdId, false);
                if (m_EdId.OSD != null)
                    if (m_EdId.OSD.Count > 0)
                        m_EdId.CustomFPOSD = GetFingerPintCheckSum(m_EdId, true);
                m_EdId.Only128FP = GetFingerPrint(m_EdIdReader.GetEDIDChecksum());
                m_EdId.Only128FPOSD = GetFingerPrint(m_EdIdReader.GetCombinedChecksum());
                m_EdId.OnlyOSDFP = GetFingerPrint(m_EdIdReader.GetOSDChecksum());

                try
                {
                    EDIDParser m_EDIDDecoder = new EDIDParser();
                    EDIDData m_EDIDObject = m_EDIDDecoder.ParseEDID(m_EdId.EdId.ToArray());
                    DecodedEDIDData m_EDID = GetDecodedEDIDDataFromEDIDObject(m_EDIDObject);
                    if (m_EDID != null)
                    {
                        List<Byte> bytePositionforType0 = new List<Byte>();
                        List<Byte> bytePositionforType1 = new List<Byte>();
                        List<Byte> bytePositionforType2 = new List<Byte>();
                        List<Byte> bytePositionforType3 = new List<Byte>();

                        EDIDInfo m_newfpruleedid = new EDIDInfo();
                        m_newfpruleedid.EdId = m_EdId.EdId;
                        m_newfpruleedid.EdIdRawData = m_EdId.EdIdRawData;

                        m_EdId.ManufacturerCode = m_EDID.IDManufacturerName;
                        m_EdId.ManufacturerHexCode = m_EDID.IDManufacturerHexCode;
                        m_EdId.SerialNumber = m_EDID.IDSerialNumber;
                        m_newfpruleedid.ManufacturerCode = m_EDID.IDManufacturerName; ;
                        m_newfpruleedid.ManufacturerHexCode = m_EDID.IDManufacturerHexCode;
                        m_newfpruleedid.PhysicalAddress = m_EDID.PhysicalAddress;
                        m_EdId.PhysicalAddress = m_EDID.PhysicalAddress;

                        //Product Code
                        String temp = m_EDID.IDProductCode.Substring(5, 4);
                        Int32 pc = 0;
                        if (Int32.TryParse(temp, out pc))
                            m_newfpruleedid.ProductCode = String.Format("{0:0000}", pc);
                        else
                            m_newfpruleedid.ProductCode = temp;

                        m_EdId.ProductCode = m_newfpruleedid.ProductCode;

                        //Year of Manufacture
                        if (m_EDID.YearOfManufacture == 0)
                            m_newfpruleedid.Year = (int)m_EDID.ModelYear;
                        else
                            m_newfpruleedid.Year = (int)m_EDID.YearOfManufacture;

                        m_EdId.Year = m_newfpruleedid.Year;

                        m_EdId.ManufacturerName = m_EDID.MonitorName;
                        m_newfpruleedid.ManufacturerName = m_EDID.MonitorName;

                        if (m_EDID.AnalogVideoSignalInterfaceSupport == 'Y')
                        {
                            m_newfpruleedid.VideoInputDefinition = "Analog";
                        }
                        else if (m_EDID.DigitalVideoSignalInterfaceSupport == 'Y')
                        {
                            m_newfpruleedid.VideoInputDefinition = "Digital";
                        }
                        m_EdId.VideoInputDefinition = m_newfpruleedid.VideoInputDefinition;

                        m_newfpruleedid.VerticalScreenSize = (int)m_EDID.VerticalScreenSize;
                        m_EdId.VerticalScreenSize = m_newfpruleedid.VerticalScreenSize;

                        m_newfpruleedid.HorizontalScreenSize = (int)m_EDID.HorizontalScreenSize;
                        m_EdId.HorizontalScreenSize = m_newfpruleedid.HorizontalScreenSize;

                        //Manufacturer ID (8-9), Product Code (10-11), Year of Manufacture (17)
                        bytePositionforType0.Add(m_newfpruleedid.EdId[8]);
                        bytePositionforType0.Add(m_newfpruleedid.EdId[9]);
                        bytePositionforType0.Add(m_newfpruleedid.EdId[10]);
                        bytePositionforType0.Add(m_newfpruleedid.EdId[11]);
                        bytePositionforType0.Add(m_newfpruleedid.EdId[17]);

                        bytePositionforType1.Add(m_newfpruleedid.EdId[8]);
                        bytePositionforType1.Add(m_newfpruleedid.EdId[9]);
                        bytePositionforType1.Add(m_newfpruleedid.EdId[10]);
                        bytePositionforType1.Add(m_newfpruleedid.EdId[11]);
                        bytePositionforType1.Add(m_newfpruleedid.EdId[17]);

                        bytePositionforType2.Add(m_newfpruleedid.EdId[8]);
                        bytePositionforType2.Add(m_newfpruleedid.EdId[9]);
                        bytePositionforType2.Add(m_newfpruleedid.EdId[10]);
                        bytePositionforType2.Add(m_newfpruleedid.EdId[11]);
                        bytePositionforType2.Add(m_newfpruleedid.EdId[17]);

                        bytePositionforType3.Add(m_newfpruleedid.EdId[8]);
                        bytePositionforType3.Add(m_newfpruleedid.EdId[9]); 
                        //Monitor Name(s) 
                        //Parsed from monitor descriptor fields at positions 54-71, 72-89, 90-105 and 106-125.
                        //Monitor Range Limits Descriptor(s)
                        //Parsed from monitor descriptor fields at positions 54-71, 72-89, 90-105 and 106-125.
                        if (!String.IsNullOrEmpty(m_EDID.MonitorName))
                        {
                            if (m_EDIDObject.GetDescriptor1_Type() == "Monitor Name")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorByteDescriptor1();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType1.Add(descriptor[i]);
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                            if (m_EDIDObject.GetDescriptor2_Type() == "Monitor Name")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorByteDescriptor2();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType1.Add(descriptor[i]);
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                            if (m_EDIDObject.GetDescriptor3_Type() == "Monitor Name")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorByteDescriptor3();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType1.Add(descriptor[i]);
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                            if (m_EDIDObject.GetDescriptor4_Type() == "Monitor Name")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorByteDescriptor4();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType1.Add(descriptor[i]);
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                        }
                        else
                        {
                            bytePositionforType1.Add(0xA);
                            bytePositionforType2.Add(0xA);
                        }

                        //Basic display Parameters (20-24) : 5
                        for (int i = 20; i <= 24; i++)
                        {
                            bytePositionforType2.Add(m_newfpruleedid.EdId[i]);
                        }
                        //Color characteristics (25-34) : 10
                        for (int i = 25; i <= 34; i++)
                        {
                            bytePositionforType2.Add(m_newfpruleedid.EdId[i]);
                        }
                        //Established timings (35-37)   : 3
                        for (int i = 35; i <= 37; i++)
                        {
                            bytePositionforType2.Add(m_newfpruleedid.EdId[i]);
                        }
                        //Standard timings (38-53)  : 16
                        for (int i = 38; i <= 53; i++)
                        {
                            bytePositionforType2.Add(m_newfpruleedid.EdId[i]);
                        }
                        if ((m_EDIDObject.GetDescriptor1_Type() != "Monitor range limits") && (m_EDIDObject.GetDescriptor2_Type() != "Monitor range limits") && (m_EDIDObject.GetDescriptor3_Type() != "Monitor range limits") && (m_EDIDObject.GetDescriptor4_Type() != "Monitor range limits"))
                        {
                            bytePositionforType2.Add(0xA);
                        }
                        else
                        {
                            if (m_EDIDObject.GetDescriptor1_Type() == "Monitor range limits")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorRangeLimitByteDescriptor1();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                            if (m_EDIDObject.GetDescriptor2_Type() == "Monitor range limits")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorRangeLimitByteDescriptor2();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                            if (m_EDIDObject.GetDescriptor3_Type() == "Monitor range limits")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorRangeLimitByteDescriptor3();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                            if (m_EDIDObject.GetDescriptor4_Type() == "Monitor range limits")
                            {
                                List<Byte> descriptor = m_EDIDObject.GetMonitorRangeLimitByteDescriptor4();
                                descriptor.Add(0xA);
                                for (int i = 0; i < descriptor.Count; i++)
                                {
                                    bytePositionforType2.Add(descriptor[i]);
                                }
                            }
                        }
                        if (m_EDIDObject.GetExtendedDataBlock())
                        {
                            //N1.0.0.0,     with N1 ≠ 0	            00
                            //N1.N2.x.x,    with N1,2 ≠ 0	        80
                            //0.0.0.0 or    invalid address	        80
                            //              No extended data block	00
                            if (!String.IsNullOrEmpty(m_EdId.PhysicalAddress))
                            {
                                String[] parts = m_EdId.PhysicalAddress.Split('.');
                                if (parts.Length == 4)
                                {
                                    if (parts[0] == "0" && parts[1] == "0" && parts[2] == "0" && parts[3] == "0")
                                    {
                                        bytePositionforType3.Add(0x80);
                                    }
                                    else if (parts[0] != "0" && parts[1] == "0" && parts[2] == "0" && parts[3] == "0")
                                    {
                                        bytePositionforType3.Add(0X00);
                                    }
                                    else if (parts[0] != "0" && parts[1] != "0" && parts[2] == "0" && parts[3] == "0")
                                    {
                                        bytePositionforType3.Add(0x80);
                                    }
                                    else if (parts[0] != "0" && parts[1] != "0" && parts[2] != "0" && parts[3] == "0")
                                    {
                                        bytePositionforType3.Add(0x80);
                                    }
                                    else if (parts[0] != "0" && parts[1] != "0" && parts[2] != "0" && parts[3] != "0")
                                    {
                                        bytePositionforType3.Add(0x80);
                                    }
                                }
                            }
                            else
                            {
                                //Extended DataBlock exists but Block1 doesn't exists
                                //No extended data block "00"                                
                                bytePositionforType3.Add(0X00);
                            }
                            bytePositionforType3.Add(0X00);
                        }
                        else
                        {
                            //No extended data block "00"
                            bytePositionforType3.Add(0X00);
                            bytePositionforType3.Add(0X00);
                        }
                        //Type 0                 
                        if (bytePositionforType0.Count > 0)
                        {
                            try
                            {
                                ICSharpCode.SharpZipLib.Checksums.Crc32 crc_32 = new ICSharpCode.SharpZipLib.Checksums.Crc32();
                                crc_32.Update(bytePositionforType0.ToArray(), 0, bytePositionforType0.Count);
                                m_EdId.CustomFPType0x00 = "0X" + crc_32.Value.ToString("X8");
                            }
                            catch { }
                        }

                        //Type 1                       
                        if (bytePositionforType1.Count > 0)
                        {
                            try
                            {
                                ICSharpCode.SharpZipLib.Checksums.Crc32 crc_32 = new ICSharpCode.SharpZipLib.Checksums.Crc32();
                                crc_32.Update(bytePositionforType1.ToArray(), 0, bytePositionforType1.Count);
                                m_EdId.CustomFPType0x01 = "0X" + crc_32.Value.ToString("X8");
                            }
                            catch { }
                        }

                        //Type 2
                        if (bytePositionforType2.Count > 0)
                        {
                            try
                            {
                                ICSharpCode.SharpZipLib.Checksums.Crc32 crc_32 = new ICSharpCode.SharpZipLib.Checksums.Crc32();
                                crc_32.Update(bytePositionforType2.ToArray(), 0, bytePositionforType2.Count);
                                m_EdId.CustomFPType0x02 = "0X" + crc_32.Value.ToString("X8");
                            }
                            catch { }
                        }

                        //Type 3
                        if (bytePositionforType3.Count > 0)
                        {
                            try
                            {
                                ICSharpCode.SharpZipLib.Checksums.Crc32 crc_32 = new ICSharpCode.SharpZipLib.Checksums.Crc32();
                                crc_32.Update(bytePositionforType3.ToArray(), 0, bytePositionforType3.Count);
                                m_EdId.CustomFPType0x03 = "0X" + crc_32.Value.ToString("X8");
                            }
                            catch { }
                        }
                    }
                }
                catch { }

            }
            catch { }
            return m_EdId;
        }
        #endregion

        #region Validate Position
        private Boolean ValidateBytePosition(EDIDInfo m_EdId)
        {
            Boolean isValid = false;
            m_EdId.BytePositionforFingerPrint = new List<Int32>();
            String[] sep = { ",", ";", " ", "\t" };
            String[] data = m_EdId.FPBytePosition.Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
            if (data.Length > 0)
            {
                for (Int32 i = 0; i < data.Length; i++)
                {
                    try
                    {
                        m_EdId.BytePositionforFingerPrint.Add(Int32.Parse(data[i]));
                        isValid = true;
                    }
                    catch
                    {
                        isValid = false;
                        break;
                    }
                }
            }
            return isValid;
        }
        #endregion

        #region Get Fingerprint
        private String GetFingerPintCheckSum(EDIDInfo m_EdId, Boolean withOSD)
        {
            String _chksm = String.Empty;
            EDIDWriter m_EdIdWriter = new EDIDWriter();
            m_EdIdWriter._combinedchecksum = false;
            m_EdIdWriter._isCustomFPAvailable = false;
            m_EdIdWriter.SetHeader("EDOSD", 1, 0);
            m_EdIdWriter.SetTocHeader("TC");
            m_EdIdWriter.SetTocCollection("EDID");
            m_EdIdWriter.SetTocCollection("EDCHKSM");
            if (m_EdId.OSD != null)
                if (m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter.SetTocCollection("OSD");
                    m_EdIdWriter.SetTocCollection("OSDCHKSM");
                }
            if (m_EdId.EdId != null && m_EdId.OSD != null)
                if (m_EdId.EdId.Count > 0 && m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter._combinedchecksum = true;
                    m_EdIdWriter.SetTocCollection("CHKSM");
                }
            if (m_EdId.FPAvailable == "Y" || m_EdId.FPAvailable == "y")
            {
                m_EdIdWriter._isCustomFPAvailable = true;
                m_EdIdWriter.SetTocCollection("FPBYDT");
                m_EdIdWriter.SetTocCollection("FP");
            }

            m_EdIdWriter.SetEdidTocHeader("EDID");
            m_EdIdWriter.SetEdidTocCollection("HI", 20);
            m_EdIdWriter.SetEdidTocCollection("BD", 5);
            m_EdIdWriter.SetEdidTocCollection("CC", 10);
            m_EdIdWriter.SetEdidTocCollection("ET", 3);
            m_EdIdWriter.SetEdidTocCollection("ST", 16);
            m_EdIdWriter.SetEdidTocCollection("D1", 18);
            m_EdIdWriter.SetEdidTocCollection("D2", 18);
            m_EdIdWriter.SetEdidTocCollection("D3", 18);
            m_EdIdWriter.SetEdidTocCollection("D4", 18);
            m_EdIdWriter.SetEdidTocCollection("EX", 1);
            m_EdIdWriter.SetEdidTocCollection("CS", 1);

            m_EdIdWriter.SetEdidChecksumHeader("EDCHKSM");
            m_EdIdWriter.SetEdidRawData(m_EdId.EdId);

            List<Byte> m_cchecksm = new List<Byte>();
            if (m_EdId.EdId != null)
            {
                Int32 count128ByteOnly = 0;
                foreach (Byte by in m_EdId.EdId)
                {
                    m_cchecksm.Add(by);
                    count128ByteOnly++;
                    if (count128ByteOnly == 128)
                        break;
                }
                m_EdIdWriter.CalculateEdidChecksum(m_cchecksm);
            }
            if (m_EdId.OSD != null)
                if (m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter.SetOSDTocHeader("OSD");
                    m_EdIdWriter.SetOSDChecksumHeader("OSDCHKSM");
                    m_EdIdWriter.SetOSDRawData(m_EdId.OSD);
                    foreach (Byte by in m_EdId.OSD)
                    {
                        m_cchecksm.Add(by);

                    }
                    m_EdIdWriter.CalculateOSDChecksum(m_EdId.OSD);
                }
            if (m_EdId.EdId != null && m_EdId.OSD != null)
                if (m_EdId.EdId.Count > 0 && m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter.SetCombinedChecksumHeader("CHKSM");
                    m_EdIdWriter.CalculateCombinedChecksum(m_cchecksm);
                }
            if (m_EdId.FPAvailable == "Y" || m_EdId.FPAvailable == "y")
            {
                m_EdIdWriter.SetCustomFPBitHeader("FPBYDT", m_EdId.FPBytePosition);
                m_EdIdWriter.SetCustomFPHeader("FP");
                m_EdIdWriter.CalculateCustomFP(GetCustomFPBytes(m_EdId, withOSD));

                if (m_EdIdWriter._customFp.stringCheckSum.Length > 0)
                {
                    _chksm = "0X" + m_EdIdWriter._customFp.stringCheckSum;
                }
            }
            return _chksm;
        }
        private String GetFingerPrint(String byteData)
        {
            StringBuilder newFPData = new StringBuilder();
            String[] sep = { " " };
            if (byteData.Length > 0)
            {
                List<Byte> newData = new List<Byte>();
                String[] data = byteData.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                for (int i = data.Length - 1; i > -1; i--)
                {
                    newData.Add((Byte)Convert.ToInt32(data[i], 16));
                }
                newFPData.Append("0X");
                //foreach (Byte b in newData.ToArray())
                //{
                //    if (b != 0)
                //        newFPData.AppendFormat("{0:x2}", b);
                //}
                for (int i = 4; i < 8; i++)
                {
                    newFPData.AppendFormat("{0:x2}", newData[i]);
                }
            }
            return newFPData.ToString().ToUpper();
        }
        private List<Byte> GetCustomFPBytes(EDIDInfo m_EdId, Boolean withOSD)
        {
            List<Byte> _customfpdata = new List<Byte>();
            if (m_EdId.BytePositionforFingerPrint != null && m_EdId.EdId != null)
            {
                if (m_EdId.BytePositionforFingerPrint.Count > 0)
                {
                    foreach (Int32 by in m_EdId.BytePositionforFingerPrint)
                    {
                        _customfpdata.Add(m_EdId.EdId[by]);
                    }
                }
            }
            if (m_EdId.OSD != null && withOSD == true)
            {
                foreach (Byte by in m_EdId.OSD)
                {
                    _customfpdata.Add(by);
                }
            }
            return _customfpdata;
        }
        #endregion

        #region Get Decoded EDID
        public DecodedEDIDData GetDecodedEDIDDataFromEDIDObject(EDIDData EDIDObject)
        {
            DecodedEDIDData data = new DecodedEDIDData();
            data.IDManufacturerName = EDIDObject.GetManufacturerID();
            data.IDManufacturerHexCode = EDIDObject.GetManufacturerIDHexCode();
            data.IDProductCode = EDIDObject.GetProductID();
            data.IDSerialNumber = EDIDObject.GetSerialNumber();
            data.WeekOfManufacture = EDIDObject.GetWeekOfManufactur();
            if (data.WeekOfManufacture == 255)
            {
                data.ModelYear = (uint)(Convert.ToInt32(EDIDObject.GetYearOfManufacture()));
                data.ModelYearFlag = 1;
                data.YearOfManufacture = 0;
            }
            else
            {
                data.ModelYear = 0;
                data.ModelYearFlag = 0;
                data.YearOfManufacture = (uint)(Convert.ToInt32(EDIDObject.GetYearOfManufacture()));
            }
            string EDIDVerion = EDIDObject.GetEDIDVersion();
            data.VersionNumber = EDIDObject.GetVersionNumber();//(uint)Convert.ToUInt16(EDIDVerion[0]);
            data.RevisionNumber = EDIDObject.GetRevisionNumber();// (uint)Convert.ToUInt16(EDIDVerion[2]);
            //string VideoInput = EDIDObject.GetVideoInputDefintionByte();
            string VideoInput = EDIDObject.GetVideoInputType();
            if (VideoInput == "DIGITAL")
            {
                data.AnalogVideoSignalInterfaceSupport = 'N';
                data.DigitalVideoSignalInterfaceSupport = 'Y';
            }
            else if (VideoInput == "ANALOG")
            {
                data.AnalogVideoSignalInterfaceSupport = 'Y';
                data.DigitalVideoSignalInterfaceSupport = 'N';
            }
            else
            {
                data.AnalogVideoSignalInterfaceSupport = 'N';
                data.DigitalVideoSignalInterfaceSupport = 'N';
            }

            data.HorizontalScreenSize = (uint)(Convert.ToInt16(EDIDObject.GetMaxHorImageSize()));
            data.VerticalScreenSize = (uint)(Convert.ToInt16(EDIDObject.GetMaxVerImageSize()));

            data.SignalLevelStandard_Video_Sync_Total = EDIDObject.GetWhiteAndSyncLevel();

            if (!EDIDObject.IsBlankToBlackSetupExpected())
                data.VideoSetupBlackLevel = 'Y';
            else
                data.VideoSetupBlackLevel = 'N';

            if (EDIDObject.IsBlankToBlackSetupExpected())
                data.VideoSetupBlankBlackLevel = 'Y';
            else
                data.VideoSetupBlankBlackLevel = 'N';

            if (EDIDObject.IsSeparateSyncsSupported())
                data.SeparateSyncHVSignalsSupported = 'Y';
            else
                data.SeparateSyncHVSignalsSupported = 'N';

            if (EDIDObject.IsCompositeSyncSupported())
                data.CompositeSyncSignalonHorizontalSupported = 'Y';
            else
                data.CompositeSyncSignalonHorizontalSupported = 'N';

            if (EDIDObject.IsSyncOnGreenSupported())
                data.CompositeSyncSignalonGreenVideoSupported = 'Y';
            else
                data.CompositeSyncSignalonGreenVideoSupported = 'N';

            if (EDIDObject.IsSerrationOfVSync())
                data.SerrationOnTheVerticalSyncSupported = 'Y';
            else
                data.SerrationOnTheVerticalSyncSupported = 'N';

            data.R720X400_at_70Hz = EDIDObject.Get_Timing_720_400_70();
            data.R720X400_at_88Hz = EDIDObject.Get_Timing_720_400_88();
            data.R640X480_at_60Hz = EDIDObject.Get_Timing_640_480_60();
            data.R640X480_at_67Hz = EDIDObject.Get_Timing_640_480_67();
            data.R640X480_at_72Hz = EDIDObject.Get_Timing_640_480_72();
            data.R640X480_at_75Hz = EDIDObject.Get_Timing_640_480_75();
            data.R800X600_at_56Hz = EDIDObject.Get_Timing_800_600_56();
            data.R800X600_at_60Hz = EDIDObject.Get_Timing_800_600_60();
            data.R800X600_at_72Hz = EDIDObject.Get_Timing_800_600_72Hz();
            data.R800X600_at_75Hz = EDIDObject.Get_Timing_800_600_75Hz();
            data.R832X624_at_75Hz = EDIDObject.Get_Timing_832_624_75Hz();
            data.R1024X768_at_87Hz = EDIDObject.Get_Timing_1024_768_87Hz();
            data.R1024X768_at_60Hz = EDIDObject.Get_Timing_1024_768_60Hz();
            data.R1024X768_at_72Hz = EDIDObject.Get_Timing_1024_768_72Hz();
            data.R1024X768_at_75Hz = EDIDObject.Get_Timing_1024_768_75Hz();
            data.R1280X1024_at_75Hz = EDIDObject.Get_Timing_1280_1024_75Hz();
            data.R1152x870_at_75Hz = EDIDObject.Get_Timing_1152_870_75Hz();

            data.Gamma = Convert.ToDouble(EDIDObject.GetDisplayGamma());
            string DMPSStandBy = EDIDObject.GetDMPSStandBy();
            if (DMPSStandBy == "YES")
                data.StandbySupport = 'Y';
            else
                data.StandbySupport = 'N';
            string DMPSSuspend = EDIDObject.GetDPMSSuspend();
            if (DMPSSuspend == "YES")
                data.SuspendSupport = 'Y';
            else
                data.SuspendSupport = 'N';
            string DMPSActiveOff = EDIDObject.GetDPMSActiveOff();
            if (DMPSActiveOff == "YES")
                data.LowPowerSupport = 'Y';
            else
                data.LowPowerSupport = 'N';


            data.Monochrome_GrayScaleDisplay = EDIDObject.IsDisplayMonochrome();
            data.RGBcolordisplay = EDIDObject.IsDisplayRGBcolor();
            data.Non_RGBcolordisplay = EDIDObject.IsDisplayNon_RGBcolor();

            data.ColorBitDepthBitsPerPrimary = (uint)(EDIDObject.GetColorBitDepth() >> 4);
            data.DVISupport = EDIDObject.IsDVISupported();
            data.HDMI_aSupport = EDIDObject.IsHDMI_a_Supported();
            data.HDMI_bSupport = EDIDObject.IsHDMI_b_Supported();
            data.MDDISupport = EDIDObject.IsMDDI_Supported();
            data.DisplayPortSupport = EDIDObject.IsDisplayPortSupported();

            data.AspectRatioLandscape = EDIDObject.GetAspectRatioLandscape().ToString();
            data.AspectRatioPortrait = EDIDObject.GetAspectRatioPortrait().ToString();


            data.RGB4_4_4 = EDIDObject.IsRGB4_4_4();
            data.RGB4_4_4_YCrCb4_4_4 = EDIDObject.IsRGB4_4_4_YCrCb4_4_4();
            data.RGB4_4_4_YCrCb4_2_2 = EDIDObject.IsRGB4_4_4_YCrCb4_2_2();
            data.RGB4_4_4_YCrCb4_4_4_YCrCb4_2_2 = EDIDObject.IsRGB4_4_4_YCrCb4_4_4_YCrCb4_2_2();
            if (EDIDObject.GetsRGBDefault())
                data.StandardDefaultColorSpace = 'Y';
            else
                data.StandardDefaultColorSpace = 'N';

            if (EDIDObject.GetNPFandRRInlcuded())
                data.NativePixelFormatIncluded = 'Y';
            else
                data.NativePixelFormatIncluded = 'N';
            if (EDIDObject.GetDCF())
                data.ContinuousFrequencyDisplay = 'Y';
            else
                data.ContinuousFrequencyDisplay = 'N';

            data.MonitorName = EDIDObject.GetMonitorName().Trim();
            if (data.MonitorName.Contains("SE322FS"))
            {
                int test = 0;
            }
            data.RedX = EDIDObject.GetRx();
            data.RedY = EDIDObject.GetRy();
            data.GreenX = EDIDObject.GetGx();
            data.GreenY = EDIDObject.GetGy();
            data.BlueX = EDIDObject.GetBx();
            data.BlueY = EDIDObject.GetBy();
            data.WhiteX = EDIDObject.GetWx();
            data.WhiteY = EDIDObject.GetWy();

            data.ST1_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(0);
            string AR = EDIDObject.GetImageAspectRatio(0);
            if (AR == "16:10")
            {
                data.ST1_ImageAspectRatio_16_10 = 'Y';
                data.ST1_ImageAspectRatio_4_3 = 'N';
                data.ST1_ImageAspectRatio_5_4 = 'N';
                data.ST1_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST1_ImageAspectRatio_16_10 = 'N';
                data.ST1_ImageAspectRatio_4_3 = 'Y';
                data.ST1_ImageAspectRatio_5_4 = 'N';
                data.ST1_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST1_ImageAspectRatio_16_10 = 'N';
                data.ST1_ImageAspectRatio_4_3 = 'N';
                data.ST1_ImageAspectRatio_5_4 = 'Y';
                data.ST1_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST1_ImageAspectRatio_16_10 = 'N';
                data.ST1_ImageAspectRatio_4_3 = 'N';
                data.ST1_ImageAspectRatio_5_4 = 'N';
                data.ST1_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST1_ImageAspectRatio_16_10 = 'N';
                data.ST1_ImageAspectRatio_4_3 = 'N';
                data.ST1_ImageAspectRatio_5_4 = 'N';
                data.ST1_ImageAspectRatio_16_9 = 'N';
            }

            data.ST1_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(0);

            data.ST2_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(1);
            AR = EDIDObject.GetImageAspectRatio(1);
            if (AR == "16:10")
            {
                data.ST2_ImageAspectRatio_16_10 = 'Y';
                data.ST2_ImageAspectRatio_4_3 = 'N';
                data.ST2_ImageAspectRatio_5_4 = 'N';
                data.ST2_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST2_ImageAspectRatio_16_10 = 'N';
                data.ST2_ImageAspectRatio_4_3 = 'Y';
                data.ST2_ImageAspectRatio_5_4 = 'N';
                data.ST2_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST2_ImageAspectRatio_16_10 = 'N';
                data.ST2_ImageAspectRatio_4_3 = 'N';
                data.ST2_ImageAspectRatio_5_4 = 'Y';
                data.ST2_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST2_ImageAspectRatio_16_10 = 'N';
                data.ST2_ImageAspectRatio_4_3 = 'N';
                data.ST2_ImageAspectRatio_5_4 = 'N';
                data.ST2_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST2_ImageAspectRatio_16_10 = 'N';
                data.ST2_ImageAspectRatio_4_3 = 'N';
                data.ST2_ImageAspectRatio_5_4 = 'N';
                data.ST2_ImageAspectRatio_16_9 = 'N';
            }

            data.ST2_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(1);

            data.ST3_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(2);
            AR = EDIDObject.GetImageAspectRatio(2);
            if (AR == "16:10")
            {
                data.ST3_ImageAspectRatio_16_10 = 'Y';
                data.ST3_ImageAspectRatio_4_3 = 'N';
                data.ST3_ImageAspectRatio_5_4 = 'N';
                data.ST3_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST3_ImageAspectRatio_16_10 = 'N';
                data.ST3_ImageAspectRatio_4_3 = 'Y';
                data.ST3_ImageAspectRatio_5_4 = 'N';
                data.ST3_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST3_ImageAspectRatio_16_10 = 'N';
                data.ST3_ImageAspectRatio_4_3 = 'N';
                data.ST3_ImageAspectRatio_5_4 = 'Y';
                data.ST3_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST3_ImageAspectRatio_16_10 = 'N';
                data.ST3_ImageAspectRatio_4_3 = 'N';
                data.ST3_ImageAspectRatio_5_4 = 'N';
                data.ST3_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST3_ImageAspectRatio_16_10 = 'N';
                data.ST3_ImageAspectRatio_4_3 = 'N';
                data.ST3_ImageAspectRatio_5_4 = 'N';
                data.ST3_ImageAspectRatio_16_9 = 'N';
            }
            data.ST3_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(2);

            data.ST4_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(3);
            AR = EDIDObject.GetImageAspectRatio(3);
            if (AR == "16:10")
            {
                data.ST4_ImageAspectRatio_16_10 = 'Y';
                data.ST4_ImageAspectRatio_4_3 = 'N';
                data.ST4_ImageAspectRatio_5_4 = 'N';
                data.ST4_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST4_ImageAspectRatio_16_10 = 'N';
                data.ST4_ImageAspectRatio_4_3 = 'Y';
                data.ST4_ImageAspectRatio_5_4 = 'N';
                data.ST4_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST4_ImageAspectRatio_16_10 = 'N';
                data.ST4_ImageAspectRatio_4_3 = 'N';
                data.ST4_ImageAspectRatio_5_4 = 'Y';
                data.ST4_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST4_ImageAspectRatio_16_10 = 'N';
                data.ST4_ImageAspectRatio_4_3 = 'N';
                data.ST4_ImageAspectRatio_5_4 = 'N';
                data.ST4_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST4_ImageAspectRatio_16_10 = 'N';
                data.ST4_ImageAspectRatio_4_3 = 'N';
                data.ST4_ImageAspectRatio_5_4 = 'N';
                data.ST4_ImageAspectRatio_16_9 = 'N';
            }
            data.ST4_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(3);

            data.ST5_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(4);
            AR = EDIDObject.GetImageAspectRatio(4);
            if (AR == "16:10")
            {
                data.ST5_ImageAspectRatio_16_10 = 'Y';
                data.ST5_ImageAspectRatio_4_3 = 'N';
                data.ST5_ImageAspectRatio_5_4 = 'N';
                data.ST5_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST5_ImageAspectRatio_16_10 = 'N';
                data.ST5_ImageAspectRatio_4_3 = 'Y';
                data.ST5_ImageAspectRatio_5_4 = 'N';
                data.ST5_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST5_ImageAspectRatio_16_10 = 'N';
                data.ST5_ImageAspectRatio_4_3 = 'N';
                data.ST5_ImageAspectRatio_5_4 = 'Y';
                data.ST5_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST5_ImageAspectRatio_16_10 = 'N';
                data.ST5_ImageAspectRatio_4_3 = 'N';
                data.ST5_ImageAspectRatio_5_4 = 'N';
                data.ST5_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST5_ImageAspectRatio_16_10 = 'N';
                data.ST5_ImageAspectRatio_4_3 = 'N';
                data.ST5_ImageAspectRatio_5_4 = 'N';
                data.ST5_ImageAspectRatio_16_9 = 'N';
            }
            data.ST5_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(4);

            data.ST6_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(5);
            AR = EDIDObject.GetImageAspectRatio(5);
            if (AR == "16:10")
            {
                data.ST6_ImageAspectRatio_16_10 = 'Y';
                data.ST6_ImageAspectRatio_4_3 = 'N';
                data.ST6_ImageAspectRatio_5_4 = 'N';
                data.ST6_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST6_ImageAspectRatio_16_10 = 'N';
                data.ST6_ImageAspectRatio_4_3 = 'Y';
                data.ST6_ImageAspectRatio_5_4 = 'N';
                data.ST6_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST6_ImageAspectRatio_16_10 = 'N';
                data.ST6_ImageAspectRatio_4_3 = 'N';
                data.ST6_ImageAspectRatio_5_4 = 'Y';
                data.ST6_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST6_ImageAspectRatio_16_10 = 'N';
                data.ST6_ImageAspectRatio_4_3 = 'N';
                data.ST6_ImageAspectRatio_5_4 = 'N';
                data.ST6_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST6_ImageAspectRatio_16_10 = 'N';
                data.ST6_ImageAspectRatio_4_3 = 'N';
                data.ST6_ImageAspectRatio_5_4 = 'N';
                data.ST6_ImageAspectRatio_16_9 = 'N';
            }
            data.ST6_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(5);

            data.ST7_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(6);

            AR = EDIDObject.GetImageAspectRatio(6);
            if (AR == "16:10")
            {
                data.ST7_ImageAspectRatio_16_10 = 'Y';
                data.ST7_ImageAspectRatio_4_3 = 'N';
                data.ST7_ImageAspectRatio_5_4 = 'N';
                data.ST7_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST7_ImageAspectRatio_16_10 = 'N';
                data.ST7_ImageAspectRatio_4_3 = 'Y';
                data.ST7_ImageAspectRatio_5_4 = 'N';
                data.ST7_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST7_ImageAspectRatio_16_10 = 'N';
                data.ST7_ImageAspectRatio_4_3 = 'N';
                data.ST7_ImageAspectRatio_5_4 = 'Y';
                data.ST7_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST7_ImageAspectRatio_16_10 = 'N';
                data.ST7_ImageAspectRatio_4_3 = 'N';
                data.ST7_ImageAspectRatio_5_4 = 'N';
                data.ST7_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST7_ImageAspectRatio_16_10 = 'N';
                data.ST7_ImageAspectRatio_4_3 = 'N';
                data.ST7_ImageAspectRatio_5_4 = 'N';
                data.ST7_ImageAspectRatio_16_9 = 'N';
            }
            data.ST7_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(6);

            data.ST8_HorizontalActivePixels = (uint)EDIDObject.GetHorizontalActivePixels(7);
            AR = EDIDObject.GetImageAspectRatio(7);
            if (AR == "16:10")
            {
                data.ST8_ImageAspectRatio_16_10 = 'Y';
                data.ST8_ImageAspectRatio_4_3 = 'N';
                data.ST8_ImageAspectRatio_5_4 = 'N';
                data.ST8_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "4:3")
            {
                data.ST8_ImageAspectRatio_16_10 = 'N';
                data.ST8_ImageAspectRatio_4_3 = 'Y';
                data.ST8_ImageAspectRatio_5_4 = 'N';
                data.ST8_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "5:4")
            {
                data.ST8_ImageAspectRatio_16_10 = 'N';
                data.ST8_ImageAspectRatio_4_3 = 'N';
                data.ST8_ImageAspectRatio_5_4 = 'Y';
                data.ST8_ImageAspectRatio_16_9 = 'N';
            }
            else if (AR == "16:9")
            {
                data.ST8_ImageAspectRatio_16_10 = 'N';
                data.ST8_ImageAspectRatio_4_3 = 'N';
                data.ST8_ImageAspectRatio_5_4 = 'N';
                data.ST8_ImageAspectRatio_16_9 = 'Y';
            }
            else
            {
                data.ST8_ImageAspectRatio_16_10 = 'N';
                data.ST8_ImageAspectRatio_4_3 = 'N';
                data.ST8_ImageAspectRatio_5_4 = 'N';
                data.ST8_ImageAspectRatio_16_9 = 'N';
            }
            data.ST8_ImageRefreshRate_Hz = (uint)EDIDObject.GetRefreshRate(7);

            data.PixelClock_MHz = EDIDObject.GetpixelClock();
            data.HorizontalAddressableVideoPixels = EDIDObject.GetHorizontalActivePixelsOfDescriptor();
            data.HorizontalBlankingPixels = EDIDObject.GetHorizontalBlankingPixels();
            data.VerticalAddressableVideoinlines = EDIDObject.GetVerticalActivePixels();
            data.VerticalBlankinginlines = EDIDObject.GetVerticalBlankingPixels();
            data.HorizontalFrontPorchpixels = EDIDObject.GetHorizontalSyncOffset();
            data.HorizontalSyncPulseWidthPixels = EDIDObject.GetHorizontalSyncPulseWidth();
            data.VerticalFrontPorchlines = EDIDObject.GetVerticalSyncOffset();
            data.VerticalSyncPulseWidthlines = EDIDObject.GetVerticalSyncPulseWidth();
            data.HorizontalImageSize = EDIDObject.GetHorizontalDisplaySize();
            data.VerticalImageSize = EDIDObject.GetVerticalDisplaySize();
            data.HorizontalBorderPixels = EDIDObject.GetHorizontalBorderPixels();
            data.VerticalBorderLines = EDIDObject.GetVerticalBorderPixels();
            if (EDIDObject.GetInterlaced())
                data.Interlaced = 'Y';
            else
                data.Interlaced = 'N';

            if (EDIDObject.IsNormalDisplay_NoStereo())
                data.NormalDisplay_NoStereo = 'Y';
            else
                data.NormalDisplay_NoStereo = 'N';

            if (EDIDObject.IsFieldsequentialstereo_leftimage())
                data.Fieldsequentialstereo_leftimage = 'Y';
            else
                data.Fieldsequentialstereo_leftimage = 'N';

            if (EDIDObject.IsFieldsequentialstereo_rightimage())
                data.Fieldsequentialstereo_rightimage = 'Y';
            else
                data.Fieldsequentialstereo_rightimage = 'N';

            if (EDIDObject.IsTwowayinterleavedstereo_rightimage())
                data.Twowayinterleavedstereo_rightimage = 'Y';
            else
                data.Twowayinterleavedstereo_rightimage = 'N';

            if (EDIDObject.IsTwowayinterleavedstereo_leftimage())
                data.Twowayinterleavedstereo_leftimage = 'Y';
            else
                data.Twowayinterleavedstereo_leftimage = 'N';

            if (EDIDObject.IsFourwayinterleavedstereo())
                data.Fourwayinterleavedstereo = 'Y';
            else
                data.Fourwayinterleavedstereo = 'N';

            if (EDIDObject.IsSidebySideinterleavedstereo())
                data.SidebySideinterleavedstereo = 'Y';
            else
                data.SidebySideinterleavedstereo = 'N';

            if (EDIDObject.IsAnalogCompositeSync())
                data.AnalogCompositeSync = 'Y';
            else
                data.AnalogCompositeSync = 'N';

            if (EDIDObject.IsBipolarAnalogCompositeSync())
                data.BipolarAnalogCompositeSync = 'Y';
            else
                data.BipolarAnalogCompositeSync = 'N';

            if (EDIDObject.IsAnalogWithoutSerrations())
                data.AnalogWithoutSerrations = 'Y';
            else
                data.AnalogWithoutSerrations = 'N';

            if (EDIDObject.IsAnalogWithSerrations())
                data.AnalogWithSerrations = 'Y';
            else
                data.AnalogWithSerrations = 'N';

            if (EDIDObject.IsSyncOnGreenSignalonly())
                data.SyncOnGreenSignalonly = 'Y';
            else
                data.SyncOnGreenSignalonly = 'N';

            if (EDIDObject.IsSyncOnallthreeRGBvideosignals())
                data.SyncOnallthreeRGBvideosignals = 'Y';
            else
                data.SyncOnallthreeRGBvideosignals = 'N';

            if (EDIDObject.IsDigitalCompositeSync())
                data.DigitalCompositeSync = 'Y';
            else
                data.DigitalCompositeSync = 'N';

            if (EDIDObject.IsDigitalWithoutSerrations())
                data.DigitalWithoutSerrations = 'Y';
            else
                data.DigitalWithoutSerrations = 'N';

            if (EDIDObject.IsDigitalWithSerrations())
                data.DigitalWithSerrations = 'Y';
            else
                data.DigitalWithSerrations = 'N';

            if (EDIDObject.IsDigitalSeparateSync())
                data.DigitalSeparateSync = 'Y';
            else
                data.DigitalSeparateSync = 'N';

            if (EDIDObject.IsDigitalVerticalSyncPositive())
                data.DigitalVerticalSyncPositive = 'Y';
            else
                data.DigitalVerticalSyncPositive = 'N';

            if (EDIDObject.IsDigitalHorizontalSyncPositive())
                data.DigitalHorizontalSyncPositive = 'Y';
            else
                data.DigitalHorizontalSyncPositive = 'N';

            data.DisplayProductSerialNumberDescriptorTagNumber = EDIDObject.GetMonitorSerialNumber().Trim();

            /*data.StringDescriptorTagnumber;
            data.MinVerticalRate_Flag;
            data.MaxVerticalRate_Flag;
            data.MinHorizontalRate_flag;
            data.MaxHorizontalRate_flag;
            data.MinVerticalRate;
            data.MaxVerticalRate;
            data.MinHorizontalRate;
            data.MaxHorizontalRate;
            data.Maxpixelclock;
            data.DefaultGTFsupported;
            data.RangeLimits;
            data.SecondaryGTFsupported;
            data.LineFeed;
            data.GTFSecondaryCurvesupported;
            data.Startbreakfrequency;
            data.CX2;
            data.M;
            data.K;
            data.J;
            data.CVTsupported;
            data.CVTStandardVersionNumber;
            data.AdditionalPixelClockPrecision;
            data.MaximumActivePixelsperLine_MSB;
            data.MaximumActivePixelsperLine_LSB;
            data.AR_4_3PreferedAspectRatio;
            data.AR_16_9SupportedAspectRatio;
            data.AR_16_10SupportedAspectRatio;
            data.AR_5_4SupportedAspectRatio;
            data.AR_15_9SupportedAspectRatio;
            data.AR_4_3PreferedAspectRatio;
            data.AR_16_9PreferedAspectRatio;
            data.AR_16_10PreferedAspectRatio;
            data.AR_5_4PreferedAspectRatio;
            data.AR_15_9PreferedAspectRatio;
            data.StandardCVTBlankingSupport;
            data.ReducedCVTBlankingSupport;
            data.HorizontalShrinkSupport;
            data.HorizontalStretch;
            data.VerticalShrink;
            data.VerticalStretch;
            data.PreferredVerticalRefreshRate;
            data.ColorPointDescriptorTagNumber;
            data.WhitePointIndexNumber;
            data.White_xy;
            data.White_x;
            data.White_y;
            data.ColorPointDescriptorValueStored;*/
            return data;
        }
        #endregion

        #region Create Bin File
        private Byte[] CreateBinFile(EDIDInfo m_EdId)
        {
            EDIDWriter m_EdIdWriter = new EDIDWriter();
            m_EdIdWriter._combinedchecksum = false;
            m_EdIdWriter._isCustomFPAvailable = false;
            m_EdIdWriter.SetHeader("EDOSD", 1, 0);
            m_EdIdWriter.SetTocHeader("TC");
            m_EdIdWriter.SetTocCollection("EDID");
            m_EdIdWriter.SetTocCollection("EDCHKSM");
            if (m_EdId.OSD != null)
                if (m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter.SetTocCollection("OSD");
                    m_EdIdWriter.SetTocCollection("OSDCHKSM");
                }
            if (m_EdId.EdId != null && m_EdId.OSD != null)
                if (m_EdId.EdId.Count > 0 && m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter._combinedchecksum = true;
                    m_EdIdWriter.SetTocCollection("CHKSM");
                }
            if (m_EdId.FPAvailable == "Y" || m_EdId.FPAvailable == "y")
            {
                m_EdIdWriter._isCustomFPAvailable = true;
                m_EdIdWriter.SetTocCollection("FPBYDT");
                m_EdIdWriter.SetTocCollection("FP");
            }

            m_EdIdWriter.SetEdidTocHeader("EDID");
            m_EdIdWriter.SetEdidTocCollection("HI", 20);
            m_EdIdWriter.SetEdidTocCollection("BD", 5);
            m_EdIdWriter.SetEdidTocCollection("CC", 10);
            m_EdIdWriter.SetEdidTocCollection("ET", 3);
            m_EdIdWriter.SetEdidTocCollection("ST", 16);
            m_EdIdWriter.SetEdidTocCollection("D1", 18);
            m_EdIdWriter.SetEdidTocCollection("D2", 18);
            m_EdIdWriter.SetEdidTocCollection("D3", 18);
            m_EdIdWriter.SetEdidTocCollection("D4", 18);
            m_EdIdWriter.SetEdidTocCollection("EX", 1);
            m_EdIdWriter.SetEdidTocCollection("CS", 1);

            m_EdIdWriter.SetEdidChecksumHeader("EDCHKSM");
            m_EdIdWriter.SetEdidRawData(m_EdId.EdId);

            List<Byte> m_cchecksm = new List<Byte>();
            if (m_EdId.EdId != null)
            {
                Int32 count128ByteOnly = 0;
                foreach (Byte by in m_EdId.EdId)
                {
                    m_cchecksm.Add(by);
                    count128ByteOnly++;
                    if (count128ByteOnly == 128)
                        break;
                }
                m_EdIdWriter.CalculateEdidChecksum(m_cchecksm);
            }
            if (m_EdId.OSD != null)
                if (m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter.SetOSDTocHeader("OSD");
                    m_EdIdWriter.SetOSDChecksumHeader("OSDCHKSM");
                    m_EdIdWriter.SetOSDRawData(m_EdId.OSD);
                    foreach (Byte by in m_EdId.OSD)
                    {
                        m_cchecksm.Add(by);
                    }
                    m_EdIdWriter.CalculateOSDChecksum(m_EdId.OSD);
                }
            if (m_EdId.EdId != null && m_EdId.OSD != null)
                if (m_EdId.EdId.Count > 0 && m_EdId.OSD.Count > 0)
                {
                    m_EdIdWriter.SetCombinedChecksumHeader("CHKSM");
                    m_EdIdWriter.CalculateCombinedChecksum(m_cchecksm);
                }
            if (m_EdId.FPAvailable == "Y" || m_EdId.FPAvailable == "y")
            {
                m_EdIdWriter.SetCustomFPBitHeader("FPBYDT", m_EdId.FPBytePosition);
                m_EdIdWriter.SetCustomFPHeader("FP");
                m_EdIdWriter.CalculateCustomFP(GetCustomFPBytes(m_EdId, true));
            }
            m_EdIdWriter.CalculateAddress();
            List<Byte> m_bytes = m_EdIdWriter.GetBytes();

            //System.IO.File.WriteAllBytes(@"C:\Working\EDIDBin.bin", m_bytes.ToArray());

            return m_bytes.ToArray();
        }
        #endregion

        #region Convert Edid To Bin
        public Byte[] ConvertEdidToBin(List<Byte> byteStream)
        {
            EDIDWriter m_EdIdWriter = new EDIDWriter();
            m_EdIdWriter._combinedchecksum = false;
            m_EdIdWriter._isCustomFPAvailable = false;
            m_EdIdWriter.SetHeader("EDOSD", 1, 0);
            m_EdIdWriter.SetTocHeader("TC");
            m_EdIdWriter.SetTocCollection("EDID");
            m_EdIdWriter.SetTocCollection("EDCHKSM");
            m_EdIdWriter.SetEdidTocHeader("EDID");
            m_EdIdWriter.SetEdidTocCollection("HI", 20);
            m_EdIdWriter.SetEdidTocCollection("BD", 5);
            m_EdIdWriter.SetEdidTocCollection("CC", 10);
            m_EdIdWriter.SetEdidTocCollection("ET", 3);
            m_EdIdWriter.SetEdidTocCollection("ST", 16);
            m_EdIdWriter.SetEdidTocCollection("D1", 18);
            m_EdIdWriter.SetEdidTocCollection("D2", 18);
            m_EdIdWriter.SetEdidTocCollection("D3", 18);
            m_EdIdWriter.SetEdidTocCollection("D4", 18);
            m_EdIdWriter.SetEdidTocCollection("EX", 1);
            m_EdIdWriter.SetEdidTocCollection("CS", 1);

            m_EdIdWriter.SetEdidChecksumHeader("EDCHKSM");
            m_EdIdWriter.SetEdidRawData(byteStream);

            List<Byte> m_cchecksm = new List<Byte>();
            if (byteStream != null)
            {
                //Take only 128 Byte
                Int32 count128ByteOnly = 0;
                foreach (Byte by in byteStream)
                {
                    m_cchecksm.Add(by);
                    count128ByteOnly++;
                    if (count128ByteOnly == 128)
                        break;
                }
                m_EdIdWriter.CalculateEdidChecksum(m_cchecksm);
            }
            m_EdIdWriter.CalculateAddress();
            List<Byte> m_bytes = m_EdIdWriter.GetBytes();

            //System.IO.File.WriteAllBytes(@"C:\Working\EDIDBin.bin", m_bytes.ToArray());

            return m_bytes.ToArray();
        }
        #endregion
    }
}