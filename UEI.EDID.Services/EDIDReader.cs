﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace UEI.EDID
{
    public class EDIDReader
    {
        #region Variables
        public String _binfile = String.Empty;
        private Int32 _nextAddress = 0;
        private Int32 _headersize = 9;
        //private Int32 _tcSize = 0;
        public Dictionary<Int32, Int32> _edidmap;
        public Dictionary<String, Int32> _tocmap;
        public Dictionary<String, Int32> _tocmapsize;
        #endregion

        #region Properties
        #endregion

        #region Constrcutor
        #endregion

        #region Methods
        public void ReadBinFile()
        {
            _edidmap = new Dictionary<Int32, Int32>();
            Byte[] _data = File.ReadAllBytes(_binfile);
            Int32 counter = 0;
            foreach (Byte by in _data)
            {
                _edidmap.Add(counter, by);
                counter++;
            }

        }
        public void ReadBinFile(Byte[] bindata)
        {
            _edidmap = new Dictionary<Int32, Int32>();            
            Int32 counter = 0;
            foreach (Byte by in bindata)
            {
                _edidmap.Add(counter, by);
                counter++;
            }
        }
        //public String GetEdidData()
        //{
        //    GetTocMap();
        //    Int32 _edidsize = 128;
        //    Int32 _chksumsize = 8;

        //    Int32 _dif = _edidsize + _chksumsize;
        //    Int32 _val = 0;
        //    _tocmap.TryGetValue("EDCHKSM", out _val);
        //    Int32 address = _val - _edidsize;
        //    StringBuilder _ediddata = new StringBuilder();
        //    for (Int32 i = address; i < (address + 128); i++)
        //    {
        //        Int32 data = Conversion.ConvertBytetoInt(GetData(1, i)[0]);
        //        _ediddata.Append(String.Format("{0} ", data.ToString("X")));
        //    }
        //    return _ediddata.ToString();
        //}
        public String GetEdidData()
        {
            List<byte> _edidFromBin = new List<byte>();
            GetTocMap();
            //Int32 _edidsize = 128;
            Int32 _chksumsize = 8;

            Int32 _dif = EdidSize + _chksumsize;
            Int32 _val = 0;
            _tocmap.TryGetValue("EDCHKSM", out _val);
            Int32 address = _val - EdidSize;
            StringBuilder _ediddata = new StringBuilder();
            for (Int32 i = address; i < (address + EdidSize); i++)
            {
                Int32 data = Conversion.ConvertBytetoInt(GetData(1, i)[0]);
                _ediddata.Append(String.Format("{0} ", data.ToString("X2")));
            }
            return _ediddata.ToString();
        }
        public void GetTocMap()
        {
            _nextAddress = _headersize;
            _tocmap = new Dictionary<String, Int32>();
            _tocmapsize = new Dictionary<String, Int32>();
            String _tc = GetByteToString(GetData(2, _nextAddress));
            _nextAddress += 2;
            Int32 _size = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
            _nextAddress += 2;
            Int32 _bytecounter = 0;
            while (_bytecounter < _size)
            {
                Int32 _letsize = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
                _nextAddress += 2;
                _bytecounter += 2;
                String _toc = Conversion.ConvertByteToString(GetData(_letsize, _nextAddress));
                _nextAddress += _letsize;
                _bytecounter += _letsize;

                Int32 _totalsize = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
                _nextAddress += 2;
                _bytecounter += 2;
                Int32 _offset = Conversion.ConvertByteArrayToInt16(GetData(4, _nextAddress));
                _nextAddress += 4;
                _bytecounter += 4;
                _tocmap.Add(_toc, _offset);
                _tocmapsize.Add(_toc, _totalsize);
            }
            GetEdidSize();
        }
        public int EdidSize = 0;
        private void GetEdidSize()
        {
            //int EdidOffset = 20 + 5 + 10 + 3 + 16 + 18 + 18 + 18 + 18 + 1 + 1;
            int _edidheader = 0;
            int _edidChksm = 0;
            _tocmap.TryGetValue("EDID", out _edidheader);
            _tocmap.TryGetValue("EDCHKSM", out _edidChksm);

            EdidSize = (_edidChksm - _edidheader)-116;//116 is a hard coded offset from the EDID and all other header info offsets

        }
        public String GetOSDData()
        {
            GetTocMap();
            Int32 _val = 0;
            _tocmap.TryGetValue("OSD", out _val);
            Int32 _size = 0;
            _tocmapsize.TryGetValue("OSD", out _size);
            StringBuilder _osd = new StringBuilder();
            for (Int32 i = _val; i < (_val + _size); i++)
            {
                Int32 data = Conversion.ConvertBytetoInt(GetData(1, i)[0]);
                _osd.Append(String.Format("{0} ", data.ToString("X2")));
            }
            return _osd.ToString();
        }
        public String GetEDIDChecksum()
        {
            Int32 _startAddress = 0;
            Int32 _sizeofChecksum = 8;
            _tocmap.TryGetValue("EDCHKSM", out _startAddress);
            Byte[] data = GetData(_sizeofChecksum, _startAddress);
            StringBuilder _chksm = new StringBuilder();
            if (_startAddress > 0)
            {
                for (Int32 i = 0; i < data.Length; i++)
                {

                    _chksm.Append(String.Format("{0} ", data[i].ToString("X")));
                }
            }
            return _chksm.ToString();
        }
        public String GetCombinedChecksum()
        {
            Int32 _startAddress = 0;
            Int32 _sizeofChecksum = 8;
            _tocmap.TryGetValue("CHKSM", out _startAddress);
            Byte[] data = GetData(_sizeofChecksum, _startAddress);
            StringBuilder _chksm = new StringBuilder();

            if (_startAddress > 0)
            {
                for (Int32 i = 0; i < data.Length; i++)
                {

                    _chksm.Append(String.Format("{0} ", data[i].ToString("X")));
                }
            }
            return _chksm.ToString();
        }
        public String GetOSDChecksum()
        {
            Int32 _startAddress = 0;
            Int32 _sizeofChecksum = 8;
            _tocmap.TryGetValue("OSDCHKSM", out _startAddress);
            StringBuilder _chksm = new StringBuilder();
            if (_startAddress > 0)
            {
                Byte[] data = GetData(_sizeofChecksum, _startAddress);

                for (Int32 i = 0; i < data.Length; i++)
                {

                    _chksm.Append(String.Format("{0} ", data[i].ToString("X")));
                }
            }
            return _chksm.ToString();
        }
        public String GetByteToString(Byte[] data)
        {
            String _data = String.Empty;
            _data = "0x";
            Int32 len = data.Length - 1;
            for (Int32 i = len; i >= 0; i--)
            {
                //Int32 dt = Convert.ToInt16(data[i].ToString(), 16);
                _data += data[i].ToString("X");
            }
            return _data;
        }
        public Byte[] GetData(Int32 sizeofbyte, Int32 startaddress)
        {
            Byte[] _data = new Byte[sizeofbyte];
            Int32 _len = sizeofbyte - 1;
            try
            {
                for (Int32 i = 0; i < _data.Length; i++)
                {
                    _data[i] = (Byte)_edidmap[startaddress];
                    startaddress++;

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return _data;
        }
        public Byte[] ConvertLittleEndianToByte(Byte[] data)
        {
            Byte[] _bytedata = new Byte[data.Length];
            Int32 counter = 0;
            for (Int32 i = data.Length - 1; i >= 0; i--)
            {
                _bytedata[counter] = data[i];
                counter++;
            }
            return _bytedata;
        }
        public String GetCustomFPBitPositions()
        {
            Int32 _startAddress = 0;            
            _tocmap.TryGetValue("FPBYDT", out _startAddress);
            StringBuilder _chksm = new StringBuilder();
            if (_startAddress > 0)
            {
                _startAddress += 6;
                Byte[] len = GetData(4, _startAddress);
                _startAddress += 4;
                Int32 _size = (Int32)Conversion.ConvertByteArrayToInt32(len);
                Byte[] data = GetData(_size, _startAddress);                
                if (_startAddress > 0)
                {
                    _chksm.Append(Conversion.ConvertByteArrayToUTF8(data));
                }
            }
            return _chksm.ToString();
        }
        public String GetCustomFP()
        {
            Int32 _startAddress = 0;            
            StringBuilder _chksm = new StringBuilder();
            _tocmap.TryGetValue("FP", out _startAddress);
            if (_startAddress > 0)
            {
                _startAddress += 2;
                Byte[] len = GetData(2, _startAddress);
                _startAddress += 2;
                Int16 _size = (Int16)Conversion.ConvertByteArrayToInt16(len);
                Byte[] data = GetData(_size, _startAddress);
                if (_startAddress > 0)
                {
                    for (Int32 i = 0; i < data.Length; i++)
                    {
                        _chksm.Append(String.Format("{0} ", data[i].ToString("X")));                        
                    }                    
                }
            }
            return _chksm.ToString();
        }
        #endregion
    }
}