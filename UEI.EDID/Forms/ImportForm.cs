﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report.ExportToExcel;

namespace UEI.EDID.Forms
{
    public partial class ImportForm : Form
    {
        private enum ProcessType
        {
            GetExcelSheets,
            GetEdIdFromExcel,
            GenerateFingerPrint,
            CreateEdId,
            UpdateEdId,
            GetEdId,
            GetFiles,
            GetEDIDFromFile
        }

        #region Variables
        private ProgressForm m_Progress             = null;
        private BackgroundWorker m_BackgroundWorker = null;
        private ProcessType m_ProcessType           = ProcessType.GetExcelSheets;
        #endregion

        #region Properties
        public String           EDIDDirectoryPath { get; set; }
        public List<String>     EDIDFiles { get; set; }
        public String           EDIDFile { get; set; }
        public List<String>     WorkSheets { get; set; }
        public String           SelectedWorkSheet { get; set; }
        public String           BytePositions { get; set; }
        public List<Int32>      BytePositionforFingerPrint { get; set; }
        public List<EDIDInfo>   EdIdData { get; set; }
        public DataTable        EdIdDataTable { get; set; }
        public EDIDInfo         SelectedEdId { get; set; }
        public String           SelectedMainDeviceType { get; set; }
        public String           SelectedSubDeviceType { get; set; }
        public String           SelectedBrand { get; set; }
        #endregion

        #region Constrcutor
        public ImportForm()
        {
            InitializeComponent();
            this.Load += ImportForm_Load;
        }
        void ImportForm_Load(object sender, EventArgs e)
        {
            this.btnCreate.Enabled                      = false;
            this.btnUpdate.Enabled                      = false;
            this.btncalculateFingerPrint.Enabled        = false;
            this.btnExportToExcel.Enabled               = false;

            this.m_BackgroundWorker                     = new BackgroundWorker();
            this.m_BackgroundWorker.DoWork              += m_BackgroundWorker_DoWork;
            this.m_BackgroundWorker.RunWorkerCompleted  += m_BackgroundWorker_RunWorkerCompleted;
            this.btnClose.Click                         += btnClose_Click;
            this.Timer.Tick                             += Timer_Tick;
            this.edidGridView.MouseDown                 += new MouseEventHandler(DataGridViewMouseDown);
            this.calculateFingerPrintToolStripMenuItem.Click += calculateFingerPrintToolStripMenuItem_Click;
            this.btnExportToExcel.Click                 += btnExportToExcel_Click;
            this.btnDownload.Click                      += btnDownload_Click;
            this.txtDirectoryPath.TextChanged           += txtDirectoryPath_TextChanged;
        }                                                             
        #endregion

        #region Down Load Import File Format
        void btnDownload_Click(object sender, EventArgs e)
        {
            SaveFileDialog m_SaveFileDialog = new SaveFileDialog();
            m_SaveFileDialog.Title = "EDID Excel File Format";
            m_SaveFileDialog.Filter = "Excel File (*.xlsx)|*.xlsx";
            m_SaveFileDialog.FileName = "InputFile";
            m_SaveFileDialog.CheckPathExists = true;
            if (m_SaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileStream m_FileStream = null;
                try
                {
                    m_FileStream = new FileStream(m_SaveFileDialog.FileName, FileMode.Create);
                    m_FileStream.Write(UEI.EDID.Properties.Resources.InputFileFormat, 0, UEI.EDID.Properties.Resources.InputFileFormat.Length);
                    m_FileStream.Close();
                }
                catch (Exception ex)
                {
                    m_FileStream.Close();
                    MessageBox.Show(ex.Message, "EDID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        } 
        #endregion

        #region Export To Excel
        void btnExportToExcel_Click(object sender, EventArgs e)
        {
            if (EdIdDataTable != null)
            {
                ExportUpdatedExcelSheet();
            }
        }
        #endregion

        #region Close
        void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region Background Progress
        private String m_Message = "Please Wait...";
        private void ResetTimer()
        {
            if (m_BackgroundWorker != null || m_BackgroundWorker.IsBusy == false)
                m_BackgroundWorker.RunWorkerAsync();

            if (m_Progress == null)
            {
                m_Progress = new ProgressForm();
                m_Progress.StartPosition = FormStartPosition.CenterParent;
            }            
            m_Progress.SetMessage(m_Message);
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();
            Timer.Start();
        }
        void Timer_Tick(object sender, EventArgs e)
        {
            ProgressMessage();
        }
        private void ProgressMessage()
        {
            if (this.m_Progress != null)
                m_Progress.SetMessage(m_Message, "Please Wait...");
        }
        void m_BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Timer.Stop();
            m_Progress.DialogResult = DialogResult.OK;
            m_Progress.Close();
            switch (m_ProcessType)
            {
                case ProcessType.GetExcelSheets:
                    this.edidGridView.DataSource = null;
                    this.btnCreate.Enabled = false;
                    this.btnUpdate.Enabled = false;
                    this.btncalculateFingerPrint.Enabled = false;
                    this.btnExportToExcel.Enabled = false;
                    cmbSheets.DataSource = null;
                    this.btnGetEDIDDetails.Enabled = false;
                    if (WorkSheets.Count > 0)
                    {
                        cmbSheets.DataSource = WorkSheets;
                        this.btnGetEDIDData.Enabled = true;
                    }
                    else
                    {
                        this.btnGetEDIDData.Enabled = false;
                    }
                    break;
                case ProcessType.GetFiles:
                    this.edidGridView.DataSource = null;
                    this.btnCreate.Enabled = false;
                    this.btnUpdate.Enabled = false;
                    this.btncalculateFingerPrint.Enabled = false;
                    this.btnExportToExcel.Enabled = false;
                    this.btnGetEDIDData.Enabled = false;
                    cmbSheets.DataSource = null;
                    if (this.EDIDFiles.Count > 0)
                    {                                                
                        this.btnGetEDIDDetails.Enabled = true;
                        this.btncalculateFingerPrint.Enabled = true;
                    }
                    else
                    {
                        this.btnGetEDIDDetails.Enabled = false;
                        this.btncalculateFingerPrint.Enabled = false;
                    }
                    break;
                case ProcessType.GetEDIDFromFile:
                     this.edidGridView.DataSource = null;
                     if (EdIdData != null)
                     {
                         this.edidGridView.DataSource = EdIdData;
                         
                         this.btnExportToExcel.Enabled = true;
                         if (EdIdData.Count == 0)
                         {                           
                             this.btnExportToExcel.Enabled = false;
                         }
                     }
                     else
                     {
                         this.btnCreate.Enabled = false;
                         this.btnUpdate.Enabled = false;
                         this.btncalculateFingerPrint.Enabled = false;
                         this.btnExportToExcel.Enabled = false;
                     }
                    break;
                case ProcessType.GetEdIdFromExcel:
                    this.edidGridView.DataSource = null;
                    if (EdIdData != null)
                    {
                        this.edidGridView.DataSource = EdIdData;
                        this.btnCreate.Enabled = true;
                        this.btnUpdate.Enabled = true;
                        this.btncalculateFingerPrint.Enabled = true;
                        this.btnExportToExcel.Enabled = true;
                        if (EdIdData.Count == 0)
                        {
                            this.btnCreate.Enabled = false;
                            this.btnUpdate.Enabled = false;
                            this.btncalculateFingerPrint.Enabled = false;
                            this.btnExportToExcel.Enabled = false;
                        }
                    }
                    else
                    {
                        this.btnCreate.Enabled = false;
                        this.btnUpdate.Enabled = false;
                        this.btncalculateFingerPrint.Enabled = false;
                        this.btnExportToExcel.Enabled = false;
                    }
                    break;
                case ProcessType.GenerateFingerPrint:
                    this.edidGridView.DataSource = null;
                    if (EdIdData != null)
                    {
                        this.btnCreate.Enabled = true;
                        this.edidGridView.DataSource = EdIdData;
                        this.btnUpdate.Enabled = true;
                        this.btncalculateFingerPrint.Enabled = true;
                        this.btnExportToExcel.Enabled = true;
                        if (EdIdData.Count == 0)
                        {
                            this.btnCreate.Enabled = false;
                            this.btnUpdate.Enabled = false;
                            this.btncalculateFingerPrint.Enabled = false;
                            this.btnExportToExcel.Enabled = false;
                        }
                    }
                    else
                    {
                        this.btnCreate.Enabled = false;
                        this.btnUpdate.Enabled = false;
                        this.btncalculateFingerPrint.Enabled = false;
                        this.btnExportToExcel.Enabled = false;
                    }
                    break;
                case ProcessType.CreateEdId:
                    ExportUpdatedExcelSheet();
                    break;
                case ProcessType.UpdateEdId:
                    ExportUpdatedExcelSheet();
                    break;
                case ProcessType.GetEdId:
                    break;                
            }
        }
        void m_BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (m_ProcessType)
            {
                case ProcessType.GetFiles:
                    m_Message = "Loading EDID Files...";
                    GetFiles();
                    break;
                case ProcessType.GetEDIDFromFile:
                    m_Message = "Loading EDID  data from File...";
                    GetEDIDDataFromFile();
                    break;
                case ProcessType.GetExcelSheets:
                    m_Message = "Loading Excel Worksheets...";
                    GetExcelSheets();
                    break;
                case ProcessType.GetEdIdFromExcel:
                    m_Message = "Loading EdId Data from Selected Excel Worksheets...";
                    GetEdIdDataFromExcel();
                    break;
                case ProcessType.GenerateFingerPrint:
                    GenerateFingerPrintCalculation();
                    break;
                case ProcessType.CreateEdId:
                    m_Message = "Creating EDID Data, Please Wait...";
                    CreateEdIdData();
                    break;
                case ProcessType.UpdateEdId:
                    m_Message = "Updating EDID Data, Please Wait...";
                    UpdateEdIdData();
                    break;
                case ProcessType.GetEdId:
                    break;                
            }
        }
        #endregion

        #region Generate Finger Print Calculation
        private void GenerateFingerPrintCalculation()
        {
            EdIdServices m_Service = new EdIdServices();
            if (EdIdData != null)
            {
                if (this.EdIdData.Count > 0)
                {
                    
                    if (this.SelectedMainDeviceType != String.Empty && this.SelectedSubDeviceType != String.Empty && this.SelectedBrand != String.Empty)
                    {
                        foreach (EDIDInfo item in this.EdIdData)
                        {
                            if (item.MainDevice == this.SelectedMainDeviceType && item.SubDevice == this.SelectedSubDeviceType && item.Brand == this.SelectedBrand)
                            {
                                item.FPBytePosition = this.BytePositions;
                                item.FPAvailable = "Y";
                                item.BytePositionforFingerPrint = this.BytePositionforFingerPrint;
                                item.CustomFP   = m_Service.GetFingerPintCheckSum(item,false);
                                if (item.OSD != null)
                                    if (item.OSD.Count > 0)
                                        item.CustomFPOSD = m_Service.GetFingerPintCheckSum(item, true);
                                DataRow[] drUpdate = EdIdDataTable.Select("[SL NO]='" + item.SlNo + "'");
                                if (drUpdate.Length > 0)
                                {
                                    if (EdIdDataTable.Columns.Contains("FPBytes"))
                                        drUpdate[0]["FPBytes"] = item.FPBytePosition;
                                    if (EdIdDataTable.Columns.Contains("FPAvailable"))
                                        drUpdate[0]["FPAvailable"] = item.FPAvailable;
                                    if (EdIdDataTable.Columns.Contains("FP Byts"))
                                        drUpdate[0]["FP Bytes"] = item.FPBytePosition;
                                    if (EdIdDataTable.Columns.Contains("FP Available"))
                                        drUpdate[0]["FP Available"] = item.FPAvailable;
                                    if (EdIdDataTable.Columns.Contains("Custom FP"))
                                        drUpdate[0]["Custom FP"] = item.CustomFP;
                                    if (EdIdDataTable.Columns.Contains("Custom FP + OSD"))
                                        if (item.OSD != null)
                                            if (item.OSD.Count > 0)
                                                drUpdate[0]["Custom FP + OSD"] = item.CustomFPOSD;                                        
                                }
                            }
                        }
                    }
                    else if (this.SelectedMainDeviceType != String.Empty && this.SelectedSubDeviceType != String.Empty && this.SelectedBrand == String.Empty)
                    {
                        foreach (EDIDInfo item in this.EdIdData)
                        {
                            if (item.MainDevice == this.SelectedMainDeviceType && item.SubDevice == this.SelectedSubDeviceType)
                            {
                                item.FPBytePosition = this.BytePositions;
                                item.FPAvailable = "Y";
                                item.BytePositionforFingerPrint = this.BytePositionforFingerPrint;
                                item.CustomFP = m_Service.GetFingerPintCheckSum(item, false);
                                if (item.OSD != null)
                                    if (item.OSD.Count > 0)
                                        item.CustomFPOSD = m_Service.GetFingerPintCheckSum(item, true);
                                DataRow[] drUpdate = EdIdDataTable.Select("[SL NO]='" + item.SlNo + "'");
                                if (drUpdate.Length > 0)
                                {
                                    if (EdIdDataTable.Columns.Contains("FPBytes"))
                                        drUpdate[0]["FPBytes"] = item.FPBytePosition;
                                    if (EdIdDataTable.Columns.Contains("FPAvailable"))
                                        drUpdate[0]["FPAvailable"] = item.FPAvailable;
                                    if (EdIdDataTable.Columns.Contains("FP Byts"))
                                        drUpdate[0]["FP Bytes"] = item.FPBytePosition;
                                    if (EdIdDataTable.Columns.Contains("FP Available"))
                                        drUpdate[0]["FP Available"] = item.FPAvailable;
                                    if (EdIdDataTable.Columns.Contains("Custom FP"))
                                        drUpdate[0]["Custom FP"] = item.CustomFP;
                                    if (EdIdDataTable.Columns.Contains("Custom FP + OSD"))
                                        if (item.OSD != null)
                                            if (item.OSD.Count > 0)
                                                drUpdate[0]["Custom FP + OSD"] = item.CustomFPOSD;
                                }
                            }
                        }
                    }
                    else if (this.SelectedMainDeviceType != String.Empty && this.SelectedSubDeviceType == String.Empty && this.SelectedBrand == String.Empty)
                    {
                        foreach (EDIDInfo item in this.EdIdData)
                        {
                            if (item.MainDevice == this.SelectedMainDeviceType)
                            {
                                item.FPBytePosition = this.BytePositions;
                                item.FPAvailable = "Y";
                                item.BytePositionforFingerPrint = this.BytePositionforFingerPrint;
                                item.CustomFP = m_Service.GetFingerPintCheckSum(item, false);
                                if (item.OSD != null)
                                    if (item.OSD.Count > 0)
                                        item.CustomFPOSD = m_Service.GetFingerPintCheckSum(item, true);
                                DataRow[] drUpdate = EdIdDataTable.Select("[SL NO]='" + item.SlNo + "'");
                                if (drUpdate.Length > 0)
                                {
                                    if (EdIdDataTable.Columns.Contains("FPBytes"))
                                        drUpdate[0]["FPBytes"] = item.FPBytePosition;
                                    if (EdIdDataTable.Columns.Contains("FPAvailable"))
                                        drUpdate[0]["FPAvailable"] = item.FPAvailable;
                                    if (EdIdDataTable.Columns.Contains("FP Byts"))
                                        drUpdate[0]["FP Bytes"] = item.FPBytePosition;
                                    if (EdIdDataTable.Columns.Contains("FP Available"))
                                        drUpdate[0]["FP Available"] = item.FPAvailable;
                                    if (EdIdDataTable.Columns.Contains("Custom FP"))
                                        drUpdate[0]["Custom FP"] = item.CustomFP;
                                    if (EdIdDataTable.Columns.Contains("Custom FP + OSD"))
                                        if (item.OSD != null)
                                            if (item.OSD.Count > 0)
                                                drUpdate[0]["Custom FP + OSD"] = item.CustomFPOSD;
                                }
                            }
                        }
                    }                    
                    else if (this.SelectedMainDeviceType == String.Empty && this.SelectedSubDeviceType == String.Empty && this.SelectedBrand == String.Empty)
                    {
                        foreach (EDIDInfo item in this.EdIdData)
                        {
                            item.FPBytePosition = this.BytePositions;
                            item.FPAvailable = "Y";
                            item.BytePositionforFingerPrint = this.BytePositionforFingerPrint;
                            item.CustomFP = m_Service.GetFingerPintCheckSum(item, false);
                            if (item.OSD != null)
                                if (item.OSD.Count > 0)
                                    item.CustomFPOSD = m_Service.GetFingerPintCheckSum(item, true);
                            DataRow[] drUpdate = EdIdDataTable.Select("[SL NO]='" + item.SlNo + "'");
                            if (drUpdate.Length > 0)
                            {
                                if (EdIdDataTable.Columns.Contains("FPBytes"))
                                    drUpdate[0]["FPBytes"] = item.FPBytePosition;
                                if (EdIdDataTable.Columns.Contains("FPAvailable"))
                                    drUpdate[0]["FPAvailable"] = item.FPAvailable;
                                if (EdIdDataTable.Columns.Contains("FP Byts"))
                                    drUpdate[0]["FP Bytes"] = item.FPBytePosition;
                                if (EdIdDataTable.Columns.Contains("FP Available"))
                                    drUpdate[0]["FP Available"] = item.FPAvailable;
                                if (EdIdDataTable.Columns.Contains("Custom FP"))
                                    drUpdate[0]["Custom FP"] = item.CustomFP;
                                if (EdIdDataTable.Columns.Contains("Custom FP + OSD"))
                                    if (item.OSD != null)
                                        if (item.OSD.Count > 0)
                                            drUpdate[0]["Custom FP + OSD"] = item.CustomFPOSD;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region DataGridView Mouse Click
        private void DataGridViewMouseDown(Object Sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hitTestInfo = this.edidGridView.HitTest(e.X, e.Y);
            if (hitTestInfo.RowIndex != -1 && hitTestInfo.ColumnIndex == -1)
            {
                if (this.edidGridView.Rows[hitTestInfo.RowIndex].Cells["EdIdRawData"].Value.ToString() != String.Empty)
                {
                    this.edidGridView.ClearSelection();
                    this.edidGridView.Rows[hitTestInfo.RowIndex].Selected = true;
                    SelectedEdId = (EDIDInfo)this.edidGridView.Rows[hitTestInfo.RowIndex].DataBoundItem;
                    this.contextMenu.Show(this.edidGridView, new Point(e.X, e.Y));
                }
            }
        }
        #endregion

        #region Browse EDID Excel File
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            this.EDIDFile = String.Empty;
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.Title = "Select EDID Excel File";
            openfile.Filter = "Excel File (*.xls,*.xlsx)|*.xls;*.xlsx";
            
            if (openfile.ShowDialog() == DialogResult.OK)
            {                
                FileInfo m_File = new FileInfo(openfile.FileName);
                if (m_File.Extension.ToLower() == ".xls" || m_File.Extension.ToLower() == ".xlsx")
                {
                    txtEDIDFile.Text = openfile.FileName;
                    this.EDIDFile = txtEDIDFile.Text;
                    m_ProcessType = ProcessType.GetExcelSheets;
                    this.m_Message = "Loading Worksheets from Excel File.";
                    ResetTimer();
                }
                else
                {
                    this.btnGetEDIDData.Enabled = false;
                    this.edidGridView.DataSource = null;
                    this.btncalculateFingerPrint.Enabled = false;
                    this.btnUpdate.Enabled = false;                    
                    this.cmbSheets.DataSource = null;
                    txtEDIDFile.Text = String.Empty;
                    this.EDIDFile = String.Empty;
                    MessageBox.Show("Please select Excel File Only", "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        #endregion       

        #region Get EdId Data from Excel File
        private void btnGetEDIDData_Click(object sender, EventArgs e)
        {
            if (this.cmbSheets.SelectedItem != null)
            {
                if (!String.IsNullOrEmpty(this.cmbSheets.SelectedItem.ToString()))
                {
                    this.SelectedWorkSheet = this.cmbSheets.SelectedItem.ToString();
                    m_ProcessType = ProcessType.GetEdIdFromExcel;
                    this.m_Message = "Loading EDID Data from Worksheet.";
                    ResetTimer();
                }
                else
                {
                    MessageBox.Show("Please select Excel Worksheet", "Invalid Worksheet", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Please select Excel Worksheet", "Invalid Worksheet", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        #endregion        

        #region Read Excel File
        private DataTable SupressEmptyColumns(DataTable dtSource)
        {
            System.Collections.ArrayList columnsToRemove = new System.Collections.ArrayList();
            foreach (DataColumn dc in dtSource.Columns)
            {
                bool colEmpty = true;
                foreach (DataRow dr in dtSource.Rows)
                {
                    if (dr[dc.ColumnName].ToString() != String.Empty)
                    {
                        colEmpty = false;
                    }
                }
                if (colEmpty == true)
                {
                    columnsToRemove.Add(dc.ColumnName);
                }
            }
            foreach (String columnName in columnsToRemove)
            {
                if (dtSource.Columns.Contains(columnName) == true)
                    dtSource.Columns.Remove(columnName);
            }
            return dtSource;
        }        
        private void GetExcelSheets()
        {
            try
            {
                FileInfo m_File = new FileInfo(EDIDFile);                
                OleDbConnection m_ExcelConnection = null;
                if (m_File.Extension.ToLower() == ".xls")
                    m_ExcelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + EDIDFile + ";Extended Properties=Excel 8.0;");
                else if (m_File.Extension.ToLower() == ".xlsx")
                    m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + EDIDFile + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");

                m_ExcelConnection.Open();
                DataTable dtExcelSheets = m_ExcelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                m_ExcelConnection.Close();
                if (dtExcelSheets.Rows.Count > 0)
                {
                    if (WorkSheets == null)
                        WorkSheets = new List<String>();
                    else
                        WorkSheets.Clear();
                    foreach (DataRow dr in dtExcelSheets.Rows)
                    {                            
                        WorkSheets.Add(dr["TABLE_NAME"].ToString());
                    }                        
                }
                else
                {
                    MessageBox.Show("EDID Summary File is Empty", "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private Boolean CheckEmptyRow(DataRow m_Row)
        {
            String valuesarr = String.Empty;
            foreach (Object item in m_Row.ItemArray)
            {
                if (item != null)
                    valuesarr += item.ToString();
            }
            if (String.IsNullOrEmpty(valuesarr))
                return true;
            else
                return false;
        }
        private DataTable BuildHeadersFromFirstRowThenRemoveFirstRow(DataTable dt)
        {
            DataRow firstRow = dt.Rows[0];

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                dt.Columns[i].ColumnName = firstRow[i].ToString().Trim();
                if (dt.Columns[i].ColumnName.Contains("Region"))
                    dt.Columns[i].MaxLength = 32767;
            }
            dt.Rows.RemoveAt(0);
            return dt;
        }
        private void GetEdIdDataFromExcel()
        {
            try
            {
                EdIdServices m_Service = new EdIdServices();
                FileInfo m_File = new FileInfo(EDIDFile);
                OleDbConnection m_ExcelConnection = null;
                if (m_File.Extension.ToLower() == ".xls")
                    m_ExcelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + EDIDFile + ";Extended Properties=Excel 8.0;");
                else if (m_File.Extension.ToLower() == ".xlsx")
                    m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + EDIDFile + ";Extended Properties=\"Excel 12.0;IMEX=1;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text;MaxScanRows=0;\"");

                //Provider=Microsoft.Jet.OLEDB.4.0;
                //Microsoft.ACE.OLEDB.12.0
                OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [" + SelectedWorkSheet + "]", m_ExcelConnection);
                
                EdIdDataTable = new DataTable("EDID");
                m_ExcelAdapter.FillSchema(EdIdDataTable, SchemaType.Source);                           
                m_ExcelAdapter.Fill(EdIdDataTable);                

                if (EdIdData == null)
                    EdIdData = new List<EDIDInfo>();
                else
                    EdIdData.Clear();

                if (EdIdDataTable.Rows.Count > 0)
                {
                    if (!this.EdIdDataTable.Columns.Contains("Horizontal Screen Size"))
                    {
                        this.EdIdDataTable.Columns.Add("Horizontal Screen Size");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("Vertical Screen Size"))
                    {
                        this.EdIdDataTable.Columns.Add("Vertical Screen Size");
                    }
                    if (this.EdIdDataTable.Columns.Contains("FingerPrint"))
                    {
                        this.EdIdDataTable.Columns.Remove("FingerPrint");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("Manufacturer Hex Code"))
                    {
                        this.EdIdDataTable.Columns.Add("Manufacturer Hex Code");
                    }
                    if ((!this.EdIdDataTable.Columns.Contains("FPAvailable")) && (!this.EdIdDataTable.Columns.Contains("FP Available")))
                    {
                        this.EdIdDataTable.Columns.Add("FPAvailable");
                    }
                    if ((!this.EdIdDataTable.Columns.Contains("FPBytes")) && (!this.EdIdDataTable.Columns.Contains("FP Bytes")))
                    {
                        this.EdIdDataTable.Columns.Add("FPBytes");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("Custom FP Type0x00"))
                    {
                        this.EdIdDataTable.Columns.Add("Custom FP Type0x00");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("Custom FP Type0x01"))
                    {
                        this.EdIdDataTable.Columns.Add("Custom FP Type0x01");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("Custom FP Type0x02"))
                    {
                        this.EdIdDataTable.Columns.Add("Custom FP Type0x02");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("Custom FP Type0x03"))
                    {
                        this.EdIdDataTable.Columns.Add("Custom FP Type0x03");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("Custom FP"))
                    {
                        this.EdIdDataTable.Columns.Add("Custom FP");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("Custom FP + OSD"))
                    {
                        this.EdIdDataTable.Columns.Add("Custom FP + OSD");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("128 FP"))
                    {
                        this.EdIdDataTable.Columns.Add("128 FP");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("128 FP + OSD"))
                    {
                        this.EdIdDataTable.Columns.Add("128 FP + OSD");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("OSD FP"))
                    {
                        this.EdIdDataTable.Columns.Add("OSD FP");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("LinkedID"))
                    {
                        this.EdIdDataTable.Columns.Add("LinkedID");
                    }
                                                           
                    String[] sep = { " " };
                    EDIDInfo newEDIDInfo = null;
                    foreach (DataRow row in EdIdDataTable.Rows)
                    {
                        if (!CheckEmptyRow(row))
                        {
                            newEDIDInfo = new EDIDInfo();
                            newEDIDInfo.DataSource = "Capture EDID";
                            foreach (DataColumn col in EdIdDataTable.Columns)
                            {
                                if (col.ColumnName.Trim() == "SL NO")
                                {
                                    if (!String.IsNullOrEmpty(row[col].ToString()))
                                        newEDIDInfo.SlNo = Int32.Parse(row[col].ToString());
                                }
                                if (col.ColumnName.Trim() == "Main Device")
                                {
                                    newEDIDInfo.MainDevice = row[col].ToString();
                                }
                                if (col.ColumnName.Trim() == "Sub Device")
                                {
                                    newEDIDInfo.SubDevice = row[col].ToString();
                                }
                                if (col.ColumnName.Trim() == "LinkedID")
                                {
                                    newEDIDInfo.DACPublishedID = row[col].ToString();
                                }
                                if (col.ColumnName.Trim() == "Component")
                                {
                                    if (row[col].ToString().Trim() != "-")
                                        newEDIDInfo.Component = row[col].ToString();
                                    else
                                        newEDIDInfo.Component = String.Empty;
                                }
                                if (col.ColumnName.Trim() == "Region")
                                {
                                    if (row[col].ToString().Trim() != "-")
                                        newEDIDInfo.Region = row[col].ToString();
                                    else
                                        newEDIDInfo.Region = String.Empty;
                                }
                                if (col.ColumnName.Trim() == "Brand")
                                {
                                    newEDIDInfo.Brand = row[col].ToString();
                                }
                                if (col.ColumnName.Trim() == "Model")
                                {
                                    newEDIDInfo.Model = row[col].ToString();
                                }
                                if (col.ColumnName.Trim() == "EDID Block" || col.ColumnName.Trim() == "EDID")
                                {
                                    String tempEDIDData = String.Empty;
                                    tempEDIDData = row[col].ToString().Trim();
                                    if (tempEDIDData.EndsWith("\n"))
                                    {
                                        tempEDIDData = tempEDIDData.Replace("\n", "");
                                    }
                                    newEDIDInfo.EdIdRawData = tempEDIDData;

                                    newEDIDInfo.EdId = new List<Byte>();
                                    String[] data = tempEDIDData.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                    if (data.Length > 1)
                                    {
                                        foreach (String bytedata in data)
                                        {
                                            newEDIDInfo.EdId.Add((Byte)Convert.ToInt16(bytedata.Trim(), 16));
                                        }
                                    }
                                }
                                if (col.ColumnName.Trim() == "Manufacturer (3 char) code")
                                {
                                    newEDIDInfo.ManufacturerCode = row[col].ToString();
                                }
                                if (col.ColumnName.Trim() == "Manufacturer name" || col.ColumnName.Trim() == "Monitor Name")
                                {
                                    newEDIDInfo.ManufacturerName = row[col].ToString();
                                }
                                if (col.ColumnName.Trim() == "Product Code")
                                {
                                    newEDIDInfo.ProductCode = row[col].ToString();
                                }
                                if (col.ColumnName.Trim() == "Serial number")
                                {
                                    newEDIDInfo.SerialNumber = row[col].ToString();
                                }
                                if (col.ColumnName.Trim() == "Week")
                                {
                                    if (!String.IsNullOrEmpty(row[col].ToString()))
                                        newEDIDInfo.Week = Int32.Parse(row[col].ToString());
                                }
                                if (col.ColumnName.Trim() == "Year")
                                {
                                    if (!String.IsNullOrEmpty(row[col].ToString()))
                                        newEDIDInfo.Year = Int32.Parse(row[col].ToString());
                                }
                                if (col.ColumnName.Trim() == "Video input definition")
                                {
                                    newEDIDInfo.VideoInputDefinition = row[col].ToString();
                                }
                                if (col.ColumnName.Trim() == "OSD Name" || col.ColumnName.Trim() == "OSD")
                                {
                                    String strOSD = String.Empty;
                                    newEDIDInfo.OSDRawData = row[col].ToString().Trim();;
                                    newEDIDInfo.OSD = new List<Byte>();
                                    String[] data = row[col].ToString().Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                    if (data.Length > 1)
                                    {
                                        foreach (String _bytedata in data)
                                        {
                                            newEDIDInfo.OSD.Add((Byte)Convert.ToInt32(_bytedata.Trim(), 16));                                            
                                        }
                                        if (newEDIDInfo.OSD.Count > 0)
                                            strOSD = Encoding.ASCII.GetString(newEDIDInfo.OSD.ToArray());
                                    }
                                }                             
                                if (col.ColumnName.Trim() == "FPAvailable" || col.ColumnName.Trim() == "FP Available")
                                {
                                    newEDIDInfo.FPAvailable = row[col].ToString().ToUpper();
                                }
                                if (col.ColumnName.Trim() == "FPBytes" || col.ColumnName.Trim() == "FP Bytes")
                                {
                                    newEDIDInfo.FPBytePosition = row[col].ToString();
                                    if (m_Service.ValidateBytePosition(newEDIDInfo))
                                    {                                       
                                        newEDIDInfo.FPAvailable = "Y";
                                        Byte[] m_fileStructure = m_Service.CreateBinFile(newEDIDInfo);
                                        EDIDInfo m_edid = m_Service.ReadBinFile(m_fileStructure);

                                        //EDIDParser m_EDIDDecoder = new EDIDParser();
                                        //EDIDData m_EDIDObject = m_EDIDDecoder.ParseEDID(m_edid.EdId.ToArray());
                                        //DecodedEDIDData m_EDID = m_Service.GetDecodedEDIDDataFromEDIDObject(m_EDIDObject);
                                        if (EdIdDataTable.Columns.Contains("EDID Block"))
                                        {
                                            row["EDID Block"] = m_edid.EdIdRawData;
                                        }
                                        if (EdIdDataTable.Columns.Contains("EDID"))
                                        {
                                            row["EDID"] = m_edid.EdIdRawData;
                                        }
                                        if (EdIdDataTable.Columns.Contains("OSD Name"))
                                        {
                                            row["OSD Name"] = m_edid.OSDRawData;
                                        }
                                        if (EdIdDataTable.Columns.Contains("OSD"))
                                        {
                                            row["OSD"] = m_edid.OSDRawData;
                                        }
                                        newEDIDInfo.EdIdRawData = m_edid.EdIdRawData;
                                        newEDIDInfo.OSDRawData = m_edid.OSDRawData;
                                        newEDIDInfo.PhysicalAddress = m_edid.PhysicalAddress;
                                        newEDIDInfo.CustomFP = m_edid.CustomFP;
                                        newEDIDInfo.CustomFPType0x00 = m_edid.CustomFPType0x00;
                                        newEDIDInfo.CustomFPType0x01 = m_edid.CustomFPType0x01;
                                        newEDIDInfo.CustomFPType0x02 = m_edid.CustomFPType0x02;
                                        newEDIDInfo.CustomFPType0x03 = m_edid.CustomFPType0x03;
                                        if (newEDIDInfo.OSD != null)
                                        {
                                            if (newEDIDInfo.OSD.Count > 0)
                                                newEDIDInfo.CustomFPOSD = m_edid.CustomFPOSD;
                                        }

                                        newEDIDInfo.Only128FP = m_edid.Only128FP;
                                        newEDIDInfo.Only128FPOSD = m_edid.Only128FPOSD;
                                        newEDIDInfo.OnlyOSDFP = m_edid.OnlyOSDFP;
                                        newEDIDInfo.ManufacturerHexCode = m_edid.ManufacturerHexCode;
                                        newEDIDInfo.HorizontalScreenSize = m_edid.HorizontalScreenSize;
                                        newEDIDInfo.VerticalScreenSize = m_edid.VerticalScreenSize;
                                        row["Manufacturer Hex Code"] = newEDIDInfo.ManufacturerHexCode;
                                        row["Horizontal Screen Size"] = newEDIDInfo.HorizontalScreenSize;
                                        row["Vertical Screen Size"] = newEDIDInfo.VerticalScreenSize;

                                        row["Custom FP Type0x00"] = m_edid.CustomFPType0x00;
                                        row["Custom FP Type0x01"] = m_edid.CustomFPType0x01;
                                        row["Custom FP Type0x02"] = m_edid.CustomFPType0x02;
                                        row["Custom FP Type0x03"] = m_edid.CustomFPType0x03;

                                        row["Custom FP"] = newEDIDInfo.CustomFP;
                                        row["Custom FP + OSD"] = newEDIDInfo.CustomFPOSD;
                                        row["128 FP"] = newEDIDInfo.Only128FP;
                                        row["128 FP + OSD"] = newEDIDInfo.Only128FPOSD;
                                        row["OSD FP"] = newEDIDInfo.OnlyOSDFP;
                                    }
                                    else
                                    {
                                        newEDIDInfo.FPAvailable = "N";
                                        Byte[] m_fileStructure = m_Service.CreateBinFile(newEDIDInfo);
                                        EDIDInfo m_edid = m_Service.ReadBinFile(m_fileStructure);
                                        //EDIDParser m_EDIDDecoder = new EDIDParser();
                                        //EDIDData m_EDIDObject = m_EDIDDecoder.ParseEDID(m_edid.EdId.ToArray());
                                        //DecodedEDIDData m_EDID = m_Service.GetDecodedEDIDDataFromEDIDObject(m_EDIDObject);
                                        if (EdIdDataTable.Columns.Contains("EDID Block"))
                                        {
                                            row["EDID Block"] = m_edid.EdIdRawData;
                                        }
                                        if (EdIdDataTable.Columns.Contains("EDID"))
                                        {
                                            row["EDID"] = m_edid.EdIdRawData;
                                        }
                                        if (EdIdDataTable.Columns.Contains("OSD Name"))
                                        {
                                            row["OSD Name"] = m_edid.OSDRawData;
                                        }
                                        if (EdIdDataTable.Columns.Contains("OSD"))
                                        {
                                            row["OSD"] = m_edid.OSDRawData;
                                        }
                                        newEDIDInfo.EdIdRawData = m_edid.EdIdRawData;
                                        newEDIDInfo.OSDRawData = m_edid.OSDRawData;
                                        newEDIDInfo.PhysicalAddress = m_edid.PhysicalAddress;
                                        newEDIDInfo.CustomFPType0x00 = m_edid.CustomFPType0x00;
                                        newEDIDInfo.CustomFPType0x01 = m_edid.CustomFPType0x01;
                                        newEDIDInfo.CustomFPType0x02 = m_edid.CustomFPType0x02;
                                        newEDIDInfo.CustomFPType0x03 = m_edid.CustomFPType0x03;
                                        newEDIDInfo.CustomFP = String.Empty;
                                        newEDIDInfo.CustomFPOSD = String.Empty;
                                        newEDIDInfo.Only128FP = m_edid.Only128FP;
                                        newEDIDInfo.Only128FPOSD = m_edid.Only128FPOSD;
                                        newEDIDInfo.OnlyOSDFP = m_edid.OnlyOSDFP;
                                        newEDIDInfo.ManufacturerHexCode = m_edid.ManufacturerHexCode;                                        
                                        newEDIDInfo.HorizontalScreenSize = m_edid.HorizontalScreenSize;
                                        newEDIDInfo.VerticalScreenSize = m_edid.VerticalScreenSize;

                                        row["Manufacturer Hex Code"] = newEDIDInfo.ManufacturerHexCode;
                                        row["Horizontal Screen Size"] = newEDIDInfo.HorizontalScreenSize;
                                        row["Vertical Screen Size"] = newEDIDInfo.VerticalScreenSize;

                                        row["Custom FP Type0x00"] = m_edid.CustomFPType0x00;
                                        row["Custom FP Type0x01"] = m_edid.CustomFPType0x01;
                                        row["Custom FP Type0x02"] = m_edid.CustomFPType0x02;
                                        row["Custom FP Type0x03"] = m_edid.CustomFPType0x03;

                                        row["Custom FP"] = String.Empty;
                                        row["Custom FP + OSD"] = String.Empty;
                                        row["128 FP"] = newEDIDInfo.Only128FP;
                                        row["128 FP + OSD"] = newEDIDInfo.Only128FPOSD;
                                        row["OSD FP"] = newEDIDInfo.OnlyOSDFP;
                                    }
                                }
                            }
                            if (newEDIDInfo.EdId != null)
                            {                         

                                EdIdData.Add(newEDIDInfo);
                                newEDIDInfo = null;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region Calculate Finger Print
        private void btncalculateFingerPrint_Click(object sender, EventArgs e)
        {
            if (this.EdIdDataTable == null)
            {
                MessageBox.Show("Please select EDID Summary Excel File", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                SelectedEdId = null;
                Forms.FingerPrintCalculationForm m_FingerPrintCalculation = new FingerPrintCalculationForm();
                m_FingerPrintCalculation.EdIdDataTable = this.EdIdDataTable;
                m_FingerPrintCalculation.BytePositions = this.BytePositions;
                this.SelectedMainDeviceType = String.Empty;
                this.SelectedSubDeviceType = String.Empty;
                this.SelectedBrand = String.Empty;
                if (m_FingerPrintCalculation.ShowDialog() == DialogResult.OK)
                {
                    this.BytePositionforFingerPrint = m_FingerPrintCalculation.BytePosforFingerPrint;
                    this.BytePositions = m_FingerPrintCalculation.BytePositions;
                    this.m_ProcessType = ProcessType.GenerateFingerPrint;
                    this.SelectedMainDeviceType = m_FingerPrintCalculation.SelectedMainDeviceType;
                    this.SelectedSubDeviceType = m_FingerPrintCalculation.SelectedSubDeviceType;
                    this.SelectedBrand = m_FingerPrintCalculation.SelectedBrand;
                    this.m_Message = "Calculating Finger Print for all the records.";
                    ResetTimer();
                }
            }
        }        
        void calculateFingerPrintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedEdId != null)
            {
                EdIdServices m_Service = new EdIdServices();
                Forms.FingerPrintCalculationForm m_FingerPrintCalculation = new FingerPrintCalculationForm();
                m_FingerPrintCalculation.SelectedEdId = SelectedEdId;
                m_FingerPrintCalculation.EdIdDataTable = null;
                if (SelectedEdId.FPAvailable == "Y" || SelectedEdId.FPAvailable == "y")
                {
                    m_FingerPrintCalculation.BytePositions = SelectedEdId.FPBytePosition;
                }                
                if (m_FingerPrintCalculation.ShowDialog() == DialogResult.OK)
                {                    
                    SelectedEdId.FPBytePosition = m_FingerPrintCalculation.BytePositions;
                    SelectedEdId.FPAvailable = "Y";
                    SelectedEdId.BytePositionforFingerPrint = m_FingerPrintCalculation.BytePosforFingerPrint;
                    SelectedEdId.CustomFP = m_Service.GetFingerPintCheckSum(SelectedEdId, false);
                    if (SelectedEdId.OSD != null)
                        if (SelectedEdId.OSD.Count > 0)
                            SelectedEdId.CustomFPOSD = m_Service.GetFingerPintCheckSum(SelectedEdId, true);                                 
                    DataRow[] drUpdate = EdIdDataTable.Select("[SL NO]='" + SelectedEdId.SlNo + "'");
                    if (drUpdate.Length>0)
                    {
                        if (EdIdDataTable.Columns.Contains("FPBytes"))
                            drUpdate[0]["FPBytes"] = SelectedEdId.FPBytePosition;
                        if (EdIdDataTable.Columns.Contains("FPAvailable"))
                            drUpdate[0]["FPAvailable"] = SelectedEdId.FPAvailable;
                        if (EdIdDataTable.Columns.Contains("FP Bytes"))
                            drUpdate[0]["FP Bytes"] = SelectedEdId.FPBytePosition;
                        if (EdIdDataTable.Columns.Contains("FP Available"))
                            drUpdate[0]["FP Available"] = SelectedEdId.FPAvailable;
                        if (EdIdDataTable.Columns.Contains("Custom FP"))
                            drUpdate[0]["Custom FP"] = SelectedEdId.CustomFP;
                        if (EdIdDataTable.Columns.Contains("Custom FP + OSD"))
                        {
                            if (SelectedEdId.OSD != null)
                                if (SelectedEdId.OSD.Count > 0)
                                    drUpdate[0]["Custom FP + OSD"] = SelectedEdId.CustomFPOSD;
                        }
                    }
                    this.edidGridView.Refresh();             
                }
            }
        } 
        #endregion        

        #region Export Updated Excel Sheet
        private DataSet dsDataSet = new DataSet("EDID Summary");
        private void ExportUpdatedExcelSheet()
        {            
            if (EdIdDataTable != null)
            {                
                //EdIdDataTable = SupressEmptyColumns(EdIdDataTable);
                String m_SaveFileMessage = String.Empty;               
                if (dsDataSet.Tables.Contains(EdIdDataTable.TableName))
                    dsDataSet.Tables.Remove(EdIdDataTable.TableName);
                dsDataSet.Tables.Add(EdIdDataTable);
                try
                {
                    String m_filePath = CommonForms.Configuration.GetWorkingDirectory() + "\\";
                    if (String.IsNullOrEmpty(m_filePath))
                    {
                        FolderBrowserDialog dlg1 = new FolderBrowserDialog();
                        if (dlg1.ShowDialog() == DialogResult.OK)
                        {
                            if (dlg1.SelectedPath.EndsWith("\\") == false)
                            {
                                dlg1.SelectedPath += "\\";
                            }
                            m_filePath = dlg1.SelectedPath;
                        }
                    }
                    ExcelReportGenerator.GenerateReport(dsDataSet, m_filePath, "EDID Summary", out m_SaveFileMessage);
                    if (!String.IsNullOrEmpty(m_SaveFileMessage))
                    {
                        MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                    }                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        #endregion

        #region Create EdId Data
        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (EdIdData.Count > 0)
            {
                Int32 checkFPCalculated = 0;
                Int32 checkMainDevice = 0;
                Int32 checkSubDevice = 0;
                String checkSlNoFP = String.Empty;
                foreach (EDIDInfo item in EdIdData)
                {
                    if (item.FPAvailable == "N")
                    {
                        checkFPCalculated++;
                        checkSlNoFP += item.SlNo.ToString()+",";
                    }                    
                    if (String.IsNullOrEmpty(item.MainDevice.Trim()))
                        checkMainDevice++;
                    if (String.IsNullOrEmpty(item.SubDevice.Trim()))
                        checkSubDevice++;
                }
                if (checkMainDevice > 0 && checkSubDevice>0)
                {
                    MessageBox.Show("Main Device and Sub Device Can't be Empty\r\nMain Device is mandatory", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (checkMainDevice > 0)
                {
                    MessageBox.Show("Main Device Can't be Empty\r\nMain Device is mandatory", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //else if (checkFPCalculated > 0)
                //{
                //    if (checkSlNoFP.Length > 0)
                //        checkSlNoFP = checkSlNoFP.Remove(checkSlNoFP.Length - 1, 1);
                //    MessageBox.Show("Fingerprint\\Byte Position is not calculated for following rows\r\nSlNo: " + checkSlNoFP, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
                else
                {
                    m_ProcessType = ProcessType.CreateEdId;
                    this.m_Message = "Inserting EDID data to Database.";
                    ResetTimer();
                }
            }
            else
            {
                MessageBox.Show("There is no EDID data to Create", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void CreateEdIdData()
        {
            try
            {
                if (EdIdDataTable != null)
                {
                    //EdIdDataTable = SupressEmptyColumns(EdIdDataTable);
                    if (!EdIdDataTable.Columns.Contains("Manufacturer Hex Code"))
                    {
                        EdIdDataTable.Columns.Add("Manufacturer Hex Code");
                    }
                    if (EdIdDataTable.Columns.Contains("Fingerprint"))
                    {
                        EdIdDataTable.Columns.Remove("Fingerprint");
                    }                                     
                    if (!EdIdDataTable.Columns.Contains("FPAvailable") && !EdIdDataTable.Columns.Contains("FP Available"))
                    {
                        EdIdDataTable.Columns.Add("FPAvailable");                        
                    }
                    if (!EdIdDataTable.Columns.Contains("FPBytes") && !EdIdDataTable.Columns.Contains("FP Bytes"))
                    {
                        EdIdDataTable.Columns.Add("FPBytes");                       
                    }
                    if (!EdIdDataTable.Columns.Contains("Status"))
                    {
                        EdIdDataTable.Columns.Add("Status");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("DACPublishedIDStatus"))
                    {
                        EdIdDataTable.Columns.Add("DACPublishedIDStatus");
                    }
                    if (!EdIdDataTable.Columns.Contains("Custom FP Type0x00"))
                    {
                        EdIdDataTable.Columns.Add("Custom FP Type0x00");
                    }
                    if (!EdIdDataTable.Columns.Contains("Custom FP Type0x01"))
                    {
                        EdIdDataTable.Columns.Add("Custom FP Type0x01");
                    }
                    if (!EdIdDataTable.Columns.Contains("Custom FP Type0x02"))
                    {
                        EdIdDataTable.Columns.Add("Custom FP Type0x02");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("Custom FP Type0x03"))
                    {
                        this.EdIdDataTable.Columns.Add("Custom FP Type0x03");
                    }
                    if (!EdIdDataTable.Columns.Contains("Custom FP"))
                    {
                        EdIdDataTable.Columns.Add("Custom FP");
                    }
                    if (!EdIdDataTable.Columns.Contains("Custom FP + OSD"))
                    {
                        EdIdDataTable.Columns.Add("Custom FP + OSD");
                    }
                    if (!EdIdDataTable.Columns.Contains("128 FP"))
                    {
                        EdIdDataTable.Columns.Add("128 FP");
                    }
                    if (!EdIdDataTable.Columns.Contains("128 FP + OSD"))
                    {
                        EdIdDataTable.Columns.Add("128 FP + OSD");
                    }
                    if (!EdIdDataTable.Columns.Contains("OSD FP"))
                    {
                        EdIdDataTable.Columns.Add("OSD FP");
                    } 
                    if (EdIdDataTable.Columns.Contains("Manufacturer name"))
                    {
                        EdIdDataTable.Columns["Manufacturer name"].ColumnName = "Monitor Name";
                    }                     
                    EdIdDataTable.AcceptChanges();
                }
                EdIdServices m_UpdateEdId = new EdIdServices();
                EdIdServices m_UpdateLinkedEdId = new EdIdServices(DBConnectionString.EdId);
                foreach (EDIDInfo item in EdIdData)
                {
                    //Creating EDID
                    m_Message = String.Format("Creating {0} of {1}", item.SlNo.ToString(), EdIdData.Count.ToString());                    
                    try
                    {
                        String m_Result = String.Empty;
                        String strLinkedIdMessage = String.Empty;
                        String m_Description = String.Empty;
                        Byte[] m_fileStructure = m_UpdateEdId.CreateBinFile(item);
                        m_Description = item.ManufacturerCode + "," + item.ManufacturerName + "," + item.ProductCode + "," + item.SerialNumber + "," + item.Week.ToString() + "," + item.Year.ToString() + "," + item.VideoInputDefinition + "," + item.FPAvailable + "," + item.FPBytePosition;
                        m_Result = m_UpdateEdId.UpdateEDIDData(item.MainDevice, item.SubDevice, item.Component, item.Brand, item.Model, item.Region, m_fileStructure, m_Description, 0, m_UpdateEdId.ConvertEDIDByteStreamToBase64String(item), item.CustomFP, item.CustomFPOSD, item.Only128FP, item.Only128FPOSD, item.OSDRawData, item.OnlyOSDFP, item.FPBytePosition, item.CustomFPType0x00, item.CustomFPType0x01, item.CustomFPType0x02, item.CustomFPType0x03);
                        if (EdIdDataTable != null)
                        {
                            //Update Linked ID
                            if (!String.IsNullOrEmpty(item.DACPublishedID))
                            {
                                m_UpdateLinkedEdId.UpdateLinkedID(item);
                                strLinkedIdMessage = m_UpdateLinkedEdId.ErrorMessage;
                            }

                            DataRow[] drUpdate = EdIdDataTable.Select("[SL NO]='" + item.SlNo + "'");

                            if (EdIdDataTable.Columns.Contains("Custom FP Type0x00"))
                            {
                                drUpdate[0]["Custom FP Type0x00"] = item.CustomFPType0x00;
                            }
                            if (EdIdDataTable.Columns.Contains("Custom FP Type0x01"))
                            {
                                drUpdate[0]["Custom FP Type0x01"] = item.CustomFPType0x01;
                            }
                            if (EdIdDataTable.Columns.Contains("Custom FP Type0x02"))
                            {
                                drUpdate[0]["Custom FP Type0x02"] = item.CustomFPType0x02;
                            }
                            if (EdIdDataTable.Columns.Contains("Custom FP Type0x03"))
                            {
                                drUpdate[0]["Custom FP Type0x03"] = item.CustomFPType0x03;
                            }
                            if (EdIdDataTable.Columns.Contains("FPAvailable"))
                            {
                                drUpdate[0]["FPAvailable"] = item.FPAvailable;
                            }
                            if (EdIdDataTable.Columns.Contains("FP Available"))
                            {
                                drUpdate[0]["FP Available"] = item.FPAvailable;
                            }
                            if (EdIdDataTable.Columns.Contains("FPBytes"))
                            {
                                drUpdate[0]["FPBytes"] = item.FPBytePosition;
                            }
                            if (EdIdDataTable.Columns.Contains("FP Bytes"))
                            {
                                drUpdate[0]["FP Bytes"] = item.FPBytePosition;
                            }
                            if (m_fileStructure.Length > 0)
                            {                                                                                                                                                           
                                drUpdate[0]["Manufacturer Hex Code"] = item.ManufacturerHexCode;
                                drUpdate[0]["Custom FP"] = item.CustomFP;
                                if (item.OSD != null)
                                {
                                    if (item.OSD.Count > 0)
                                        drUpdate[0]["Custom FP + OSD"] = item.CustomFPOSD;
                                }
                                drUpdate[0]["128 FP"] = item.Only128FP;
                                drUpdate[0]["128 FP + OSD"] = item.Only128FPOSD;
                                drUpdate[0]["OSD FP"] = item.OnlyOSDFP;
                                drUpdate[0]["Custom FP Type0x00"] = item.CustomFPType0x00;
                                drUpdate[0]["Custom FP Type0x01"] = item.CustomFPType0x01;
                                drUpdate[0]["Custom FP Type0x02"] = item.CustomFPType0x02;
                                drUpdate[0]["Custom FP Type0x03"] = item.CustomFPType0x03;
                            }
                            drUpdate[0]["Status"] = m_Result;
                            if (!String.IsNullOrEmpty(item.DACPublishedID))
                            {
                                drUpdate[0]["DACPublishedIDStatus"] = strLinkedIdMessage;
                            }
                            else
                            {
                                drUpdate[0]["DACPublishedIDStatus"] = String.Empty;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        m_Message = ex.Message;
                    }
                }

                //Update Decoded EDID
                UpdateDecodedEdId();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region Update Decoded EdId
        void UpdateDecodedEdId()
        {
            EdIdServices m_UpdateDecodedEdId = new EdIdServices();
            try
            {
                m_UpdateDecodedEdId.UpdateDecodedEdId(ref m_Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region Update EdId Data
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (EdIdData.Count > 0)
            {
                 Int32 checkFPCalculated = 0;
                 Int32 checkMainDevice = 0;
                 Int32 checkSubDevice = 0;
                String checkSlNoFP = String.Empty;
                foreach (EDIDInfo item in EdIdData)
                {
                    if (item.FPAvailable == "N")
                    {
                        checkFPCalculated++;
                        checkSlNoFP += item.SlNo.ToString()+",";
                    }
                    if (String.IsNullOrEmpty(item.MainDevice.Trim()))
                        checkMainDevice++;
                    if (String.IsNullOrEmpty(item.SubDevice.Trim()))
                        checkSubDevice++;
                }
                if (checkMainDevice > 0 && checkSubDevice > 0)
                {
                    MessageBox.Show("Main Device and Sub Device Can't be Empty\r\nMain Device is mandatory", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (checkMainDevice > 0)
                {
                    MessageBox.Show("Main Device Can't be Empty\r\nMain Device is mandatory", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //else if (checkFPCalculated > 0)
                //{
                //    if (checkSlNoFP.Length > 0)
                //        checkSlNoFP = checkSlNoFP.Remove(checkSlNoFP.Length - 1, 1);
                //    MessageBox.Show("Fingerprint\\Byte Position is not calculated for following rows\r\nSlNo: " + checkSlNoFP, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
                else
                {
                    m_ProcessType = ProcessType.UpdateEdId;
                    this.m_Message = "Updating EDID data to Database.";
                    ResetTimer();
                }
            }
            else
            {
                MessageBox.Show("There is no EDID data to Update", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void UpdateEdIdData()
        {
            try
            {
                if (EdIdDataTable != null)
                {
                    //EdIdDataTable = SupressEmptyColumns(EdIdDataTable);
                    if (EdIdDataTable.Columns.Contains("Fingerprint"))
                    {
                        EdIdDataTable.Columns.Remove("Fingerprint");
                    }                     
                    if (!EdIdDataTable.Columns.Contains("FPAvailable") && !EdIdDataTable.Columns.Contains("FP Available"))
                    {
                        EdIdDataTable.Columns.Add("FPAvailable");                        
                    }
                    if (!EdIdDataTable.Columns.Contains("FPBytes") && !EdIdDataTable.Columns.Contains("FP Bytes"))
                    {
                        EdIdDataTable.Columns.Add("FPBytes");                        
                    }
                    if (!this.EdIdDataTable.Columns.Contains("Status"))
                    {
                        EdIdDataTable.Columns.Add("Status");                        
                    }
                    if (!this.EdIdDataTable.Columns.Contains("DACPublishedIDStatus"))
                    {
                        EdIdDataTable.Columns.Add("DACPublishedIDStatus");
                    }
                    if (!EdIdDataTable.Columns.Contains("Custom FP Type0x00"))
                    {
                        EdIdDataTable.Columns.Add("Custom FP Type0x00");
                    }
                    if (!EdIdDataTable.Columns.Contains("Custom FP Type0x01"))
                    {
                        EdIdDataTable.Columns.Add("Custom FP Type0x01");
                    }
                    if (!EdIdDataTable.Columns.Contains("Custom FP Type0x02"))
                    {
                        EdIdDataTable.Columns.Add("Custom FP Type0x02");
                    }
                    if (!this.EdIdDataTable.Columns.Contains("Custom FP Type0x03"))
                    {
                        this.EdIdDataTable.Columns.Add("Custom FP Type0x03");
                    }
                    if (!EdIdDataTable.Columns.Contains("Custom FP"))
                    {
                        EdIdDataTable.Columns.Add("Custom FP");
                    }
                    if (!EdIdDataTable.Columns.Contains("Custom FP + OSD"))
                    {
                        EdIdDataTable.Columns.Add("Custom FP + OSD");
                    }
                    if (!EdIdDataTable.Columns.Contains("128 FP"))
                    {
                        EdIdDataTable.Columns.Add("128 FP");
                    }
                    if (!EdIdDataTable.Columns.Contains("128 FP + OSD"))
                    {
                        EdIdDataTable.Columns.Add("128 FP + OSD");
                    }
                    if (!EdIdDataTable.Columns.Contains("OSD FP"))
                    {
                        EdIdDataTable.Columns.Add("OSD FP");
                    }
                    if (EdIdDataTable.Columns.Contains("Manufacturer name"))
                    {
                        EdIdDataTable.Columns["Manufacturer name"].ColumnName = "Monitor Name";
                    } 
                    EdIdDataTable.AcceptChanges();
                }
                EdIdServices m_UpdateEdId = new EdIdServices();
                EdIdServices m_UpdateLinkedEdId = new EdIdServices(DBConnectionString.EdId);
                foreach (EDIDInfo item in EdIdData)
                {
                    //update EDID
                    m_Message = String.Format("Updating {0} of {1}", item.SlNo.ToString(), EdIdData.Count.ToString());                    
                    try
                    {
                        String m_Result = String.Empty;
                        String strLinkedIdMessage = String.Empty;
                        String m_Description = String.Empty;
                        Byte[] m_fileStructure = m_UpdateEdId.CreateBinFile(item);
                        m_Description = item.ManufacturerCode + "," + item.ManufacturerName + "," + item.ProductCode + "," + item.SerialNumber + "," + item.Week.ToString() + "," + item.Year.ToString() + "," + item.VideoInputDefinition + "," + item.FPAvailable + "," + item.FPBytePosition;
                        m_Result = m_UpdateEdId.UpdateEDIDData(item.MainDevice, item.SubDevice, item.Component, item.Brand, item.Model, item.Region, m_fileStructure, m_Description, 1, m_UpdateEdId.ConvertEDIDByteStreamToBase64String(item), item.CustomFP, item.CustomFPOSD, item.Only128FP, item.Only128FPOSD, item.OSDRawData, item.OnlyOSDFP, item.FPBytePosition, item.CustomFPType0x00, item.CustomFPType0x01, item.CustomFPType0x02, item.CustomFPType0x03);
                        if (EdIdDataTable != null)
                        {
                            //Update Linked ID
                            if (!String.IsNullOrEmpty(item.DACPublishedID))
                            {
                                m_UpdateLinkedEdId.UpdateLinkedID(item);
                                strLinkedIdMessage = m_UpdateLinkedEdId.ErrorMessage;
                            }
                            DataRow[] drUpdate = EdIdDataTable.Select("[SL NO]='" + item.SlNo + "'");

                            if (EdIdDataTable.Columns.Contains("Custom FP Type0x00"))
                            {
                                drUpdate[0]["Custom FP Type0x00"] = item.CustomFPType0x00;
                            }
                            if (EdIdDataTable.Columns.Contains("Custom FP Type0x01"))
                            {
                                drUpdate[0]["Custom FP Type0x01"] = item.CustomFPType0x01;
                            }
                            if (EdIdDataTable.Columns.Contains("Custom FP Type0x02"))
                            {
                                drUpdate[0]["Custom FP Type0x02"] = item.CustomFPType0x02;
                            }
                            if (EdIdDataTable.Columns.Contains("Custom FP Type0x03"))
                            {
                                drUpdate[0]["Custom FP Type0x03"] = item.CustomFPType0x03;
                            }
                            if (EdIdDataTable.Columns.Contains("FP Available"))
                            {
                                drUpdate[0]["FP Available"] = item.FPAvailable;
                            }
                            if (EdIdDataTable.Columns.Contains("FPBytes"))
                            {
                                drUpdate[0]["FPBytes"] = item.FPBytePosition;
                            }
                            if (EdIdDataTable.Columns.Contains("FP Bytes"))
                            {
                                drUpdate[0]["FP Bytes"] = item.FPBytePosition;
                            }
                            if (m_fileStructure.Length > 0)
                            {
                                drUpdate[0]["Manufacturer Hex Code"] = item.ManufacturerHexCode;
                                drUpdate[0]["Custom FP"] = item.CustomFP;
                                if (item.OSD != null)
                                {
                                    if (item.OSD.Count > 0)
                                        drUpdate[0]["Custom FP + OSD"] = item.CustomFPOSD;
                                }
                                drUpdate[0]["128 FP"] = item.Only128FP;
                                drUpdate[0]["128 FP + OSD"] = item.Only128FPOSD;
                                drUpdate[0]["OSD FP"] = item.OnlyOSDFP;
                                drUpdate[0]["Custom FP Type0x00"] = item.CustomFPType0x00;
                                drUpdate[0]["Custom FP Type0x01"] = item.CustomFPType0x01;
                                drUpdate[0]["Custom FP Type0x02"] = item.CustomFPType0x02;
                                drUpdate[0]["Custom FP Type0x03"] = item.CustomFPType0x03;
                            }
                            drUpdate[0]["Status"] = m_Result;
                            if (!String.IsNullOrEmpty(item.DACPublishedID))
                            {
                                drUpdate[0]["DACPublishedIDStatus"] = strLinkedIdMessage;
                            }
                            else
                            {
                                drUpdate[0]["DACPublishedIDStatus"] = String.Empty;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        m_Message = ex.Message;
                    }
                }
                //Update Decoded EDID
                UpdateDecodedEdId();
            }             
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion             

        #region Browse EDID Text File Directory Path
        void txtDirectoryPath_TextChanged(object sender, EventArgs e)
        {
            this.EDIDFiles = new List<String>();
            this.EDIDFile = String.Empty;
            FolderBrowserDialog openPath = new FolderBrowserDialog();
            if (!String.IsNullOrEmpty(this.txtDirectoryPath.Text))
                openPath.SelectedPath = this.txtDirectoryPath.Text;
            else
                openPath.SelectedPath = @"C:\";

            if (openPath.ShowDialog() == DialogResult.OK)
            {
                this.EDIDDirectoryPath = openPath.SelectedPath;
                this.txtDirectoryPath.Text = this.EDIDDirectoryPath;
                this.m_ProcessType = ProcessType.GetFiles;
                this.m_Message = "Loading EDID Text Files.";
                ResetTimer();
            }
        } 
        private void btnBrowseDirectory_Click(object sender, EventArgs e)
        {
            this.EDIDFiles = new List<String>();
            this.EDIDFile = String.Empty;
            FolderBrowserDialog openPath = new FolderBrowserDialog();
            if (!String.IsNullOrEmpty(this.txtDirectoryPath.Text))           
                openPath.SelectedPath = this.txtDirectoryPath.Text;          
            else
                openPath.SelectedPath = @"C:\";
           
            if (openPath.ShowDialog() == DialogResult.OK)
            {                
                this.EDIDDirectoryPath = openPath.SelectedPath;
                this.txtDirectoryPath.Text = this.EDIDDirectoryPath;
                this.m_ProcessType = ProcessType.GetFiles;
                this.m_Message = "Loading EDID Text Files.";
                ResetTimer();
            }
        }
        #endregion

        #region Get EDID Data From File
        private void PrepareDataTableForExporting()
        {
            if (this.EdIdDataTable != null)
                this.EdIdDataTable.Clear();
            else
            {
                EdIdDataTable = new DataTable("EDID");
                EdIdDataTable.Columns.Add("SL NO");
                EdIdDataTable.Columns.Add("Main Device");
                EdIdDataTable.Columns.Add("Sub Device");
                EdIdDataTable.Columns.Add("Component");
                EdIdDataTable.Columns.Add("Region");
                EdIdDataTable.Columns.Add("Brand");
                EdIdDataTable.Columns.Add("Model");
                EdIdDataTable.Columns.Add("EDID Block");
                EdIdDataTable.Columns.Add("Manufacturer (3 char) code");
                EdIdDataTable.Columns.Add("Manufacturer Hex Code");
                EdIdDataTable.Columns.Add("Monitor Name");
                EdIdDataTable.Columns.Add("Product Code");
                EdIdDataTable.Columns.Add("Serial number");
                EdIdDataTable.Columns.Add("Week");
                EdIdDataTable.Columns.Add("Year");
                EdIdDataTable.Columns.Add("Horizontal Screen Size");
                EdIdDataTable.Columns.Add("Vertical Screen Size");
                EdIdDataTable.Columns.Add("Video input definition");
                EdIdDataTable.Columns.Add("OSD Name");               
                EdIdDataTable.Columns.Add("FP Available");
                EdIdDataTable.Columns.Add("FP Bytes");
                EdIdDataTable.Columns.Add("Custom FP Type0x00");
                EdIdDataTable.Columns.Add("Custom FP Type0x01");
                EdIdDataTable.Columns.Add("Custom FP Type0x02");
                EdIdDataTable.Columns.Add("Custom FP Type0x03");

                EdIdDataTable.Columns.Add("Custom FP");
                EdIdDataTable.Columns.Add("Custom FP + OSD");
                EdIdDataTable.Columns.Add("128 FP");
                EdIdDataTable.Columns.Add("128 FP + OSD");
                EdIdDataTable.Columns.Add("OSD FP");
                EdIdDataTable.Columns.Add("CEC Physical Address");
            }
            if (this.EdIdData != null)
            {
                FillEdIdDataTableDate();
            }
        }
        private void FillEdIdDataTableDate()
        {
            EdIdServices m_Service = null;
            if (this.EdIdData != null)
            {
                DataRow dr = null;
                foreach (EDIDInfo item in this.EdIdData)
                {
                    m_Service = new EdIdServices();
                    dr = EdIdDataTable.NewRow();

                    dr["SL NO"] = item.SlNo;
                    dr["Main Device"] = item.MainDevice;
                    dr["Sub Device"] = item.SubDevice;
                    dr["Component"] = item.Component;
                    dr["Region"] = item.Region;
                    dr["Brand"] = item.Brand;
                    dr["Model"] = item.Model;
                    dr["EDID Block"] = item.EdIdRawData;
                    dr["Manufacturer (3 char) code"] = item.ManufacturerCode;
                    dr["Manufacturer Hex Code"] = item.ManufacturerHexCode;
                    dr["Monitor Name"] = item.ManufacturerName;
                    dr["Horizontal Screen Size"] = item.HorizontalScreenSize;
                    dr["Vertical Screen Size"] = item.VerticalScreenSize;
                    dr["Product Code"] = item.ProductCode;
                    dr["Serial number"] = item.SerialNumber;
                    dr["Week"] = item.Week;
                    dr["Year"] = item.Year;                    
                    dr["Video input definition"] = item.VideoInputDefinition;
                    dr["OSD Name"] = item.OSDRawData;
                    dr["FP Available"] = item.FPAvailable;
                    dr["FP Bytes"] = item.FPBytePosition;
                    dr["CEC Physical Address"] = item.PhysicalAddress;
                    dr["Custom FP Type0x00"] = item.CustomFPType0x00;
                    dr["Custom FP Type0x01"] = item.CustomFPType0x01;
                    dr["Custom FP Type0x02"] = item.CustomFPType0x02;
                    dr["Custom FP Type0x03"] = item.CustomFPType0x03;
                   
                    dr["Custom FP"] = item.CustomFP;
                    dr["Custom FP + OSD"] = item.CustomFPOSD;
                    dr["128 FP"] = item.Only128FP;
                    dr["128 FP + OSD"] = item.Only128FPOSD;
                    dr["OSD FP"] = item.OnlyOSDFP;

                    //Dictionary<String, String> m_Position = m_Service.GetBytePositionBasedonBrands(item.MainDevice, item.Brand);
                    //if (m_Position.Count > 0)
                    //    foreach (KeyValuePair<String, String> pair in m_Position)
                    //    {
                    //        item.FPAvailable = pair.Key;
                    //        item.FPBytePosition = pair.Value;
                    //    }
                    //else
                    //{
                    //    item.FPAvailable = "N";
                    //    item.FPBytePosition = String.Empty;
                    //}

                    //dr["FP Available"] = item.FPAvailable;
                    //dr["FP Bytes"] = item.FPBytePosition;
                    //if (item.EdId != null)
                    //{
                    //    m_Service.ValidateBytePosition(item);
                    //    Byte[] tempBinData = m_Service.CreateBinFile(item);
                    //    if (tempBinData.Length > 0)
                    //    {
                    //        try
                    //        {
                    //            EDIDInfo m_edid = m_Service.ReadBinFile(tempBinData);                                
                    //            item.CustomFP = m_edid.CustomFP;
                    //            if (item.OSD != null)
                    //            {
                    //                if (item.OSD.Count > 0)
                    //                    item.CustomFPOSD = m_edid.CustomFPOSD;
                    //            }
                    //            item.Only128FP = m_edid.Only128FP;
                    //            item.Only128FPOSD = m_edid.Only128FPOSD;
                    //            item.OnlyOSDFP = m_edid.OnlyOSDFP;

                    //            dr["Custom FP"] = item.CustomFP;
                    //            dr["Custom FP + OSD"] = item.CustomFPOSD;
                    //            dr["128 FP"] = item.Only128FP;
                    //            dr["128 FP + OSD"] = item.Only128FPOSD;
                    //            dr["OSD FP"] = item.OnlyOSDFP;                                
                    //        }
                    //        catch
                    //        {
                    //            dr["Custom FP"] = String.Empty;
                    //            dr["Custom FP + OSD"] = String.Empty;
                    //            dr["128 FP"] = String.Empty;
                    //            dr["128 FP + OSD"] = String.Empty;
                    //            dr["OSD FP"] = String.Empty;                           
                    //        }
                    //    }
                    //}

                    this.EdIdDataTable.Rows.Add(dr);
                    dr = null;
                }
                this.EdIdDataTable.AcceptChanges();
            }
        }        
        private void GetFiles()
        {
            if (!String.IsNullOrEmpty(this.EDIDDirectoryPath))
            {
                String[] textFiles = Directory.GetFiles(this.EDIDDirectoryPath, "*.txt", SearchOption.AllDirectories);
                if (textFiles.Length > 0)
                {
                    this.EDIDFiles = new List<String>();
                    foreach (String item in textFiles)
                    {
                        this.EDIDFiles.Add(item);
                    }
                }
                String[] binFiles = Directory.GetFiles(this.EDIDDirectoryPath, "*.bin", SearchOption.AllDirectories);
                if (binFiles.Length > 0)
                {
                    if (this.EDIDFiles == null)
                        this.EDIDFiles = new List<String>();
                    foreach (String item in binFiles)
                    {
                        this.EDIDFiles.Add(item);
                    }
                }
            }
        }
        private EDIDInfo GetEDIDDataFromTextFile(String edidFile)
        {
            EdIdServices m_Service = new EdIdServices(DBConnectionString.UEIPUBLIC);
            EDIDInfo edidTemp = new EDIDInfo();
            Int32 dataToCheck1 = 0;
            String dataToCheck2 = String.Empty;
            String[] sep = { "|", " " };
            String[] sep1 = { ":" };
            //String[] sep2 = { "," };
            StreamReader streamReader = new StreamReader(edidFile);
            while (!streamReader.EndOfStream)
            {
                String data = streamReader.ReadLine();
                dataToCheck2 = String.Format("{0:000}", dataToCheck1) + "  |";
                                
                if (data.Contains(dataToCheck2))
                {
                    String[] actualData = data.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    for (Int32 i = 1; i < actualData.Length; i++)
                    {                        
                        Byte byteData = (Byte)Convert.ToInt32(actualData[i], 16);
                        if (edidTemp.EdId == null)
                            edidTemp.EdId = new List<Byte>();
                        edidTemp.EdId.Add(byteData);
                        edidTemp.EdIdRawData += actualData[i] + " ";
                    }                   
                    dataToCheck1 = dataToCheck1 + 10;
                    if (dataToCheck1 == 130)
                    {
                        dataToCheck1 = 128;
                    }
                }                            
            }
            streamReader.Close();
            streamReader = null;
            if (edidTemp != null)
            {
                if (edidTemp.EdId != null)
                {
                    try
                    {
                        Byte[] m_fileStructure = m_Service.CreateBinFile(edidTemp);
                        edidTemp = m_Service.ReadBinFile(m_fileStructure);
                    }
                    catch { }
                }
            }
            //File Name format Main Device, Brand, Model
            //Separator may be any one from "_", " ", "-"

            FileInfo m_File = new FileInfo(edidFile);
            String[] data2 = m_File.Name.Replace(m_File.Extension, "").Split(',');

            if (!String.IsNullOrEmpty(data2[0]) && String.IsNullOrEmpty(edidTemp.MainDevice))
                edidTemp.MainDevice = data2[0].Trim();
            if (data2.Length > 1)
            {
                if (data2[1].Trim().ToLower().Contains("avreceiver") || data2[1].Trim().ToLower().Contains("av receiver") || data2[1].Trim().ToLower().Contains("a v receiver"))
                {
                    edidTemp.SubDevice = "A/V Receiver";
                }
                else
                    edidTemp.SubDevice = data2[1].Trim();
            }

            if (data2.Length > 2)
            {
                if (!String.IsNullOrEmpty(data2[2]) && String.IsNullOrEmpty(edidTemp.Brand))
                    edidTemp.Brand = data2[2].Trim();
            }
            if (data2.Length > 3)
            {
                if (!String.IsNullOrEmpty(data2[3]))
                    edidTemp.Model = data2[3].Trim();
            }
            //Get Country Information from DB
            try
            {
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                {
                    if (!String.IsNullOrEmpty(edidTemp.MainDevice) && !String.IsNullOrEmpty(edidTemp.SubDevice) && !String.IsNullOrEmpty(edidTemp.Brand) && !String.IsNullOrEmpty(edidTemp.Model))
                    {
                        String countries = m_Service.GetLocationForEDIDModel(edidTemp.MainDevice,edidTemp.SubDevice,edidTemp.Brand, edidTemp.Model);
                        edidTemp.Region = countries;
                    }
                    else
                        edidTemp.Region = String.Empty;
                }
                else                
                    edidTemp.Region = String.Empty;                
            }
            catch { edidTemp.Region = String.Empty; }

            //edidTemp.Region = String.Empty;
            edidTemp.Component = String.Empty;
            
            return edidTemp;
        }
        private EDIDInfo GetEDIDDataFromBinFile(String edidFile)
        {
            EdIdServices m_Service = new EdIdServices(DBConnectionString.UEIPUBLIC);
            EDIDInfo edidTemp = new EDIDInfo();
            Byte[] data = File.ReadAllBytes(edidFile);            
            if(data != null)
            {
                edidTemp.EdId = new List<Byte>();
                foreach (Byte item in data)
                {
                    edidTemp.EdId.Add(item);
                    edidTemp.EdIdRawData += item.ToString("X") + " ";
                }
                if (edidTemp.EdIdRawData.Length > 0)
                {
                    edidTemp.EdIdRawData = edidTemp.EdIdRawData.Trim();
                }
                try
                {
                    Byte[] m_fileStructure = m_Service.CreateBinFile(edidTemp);
                    edidTemp = m_Service.ReadBinFile(m_fileStructure);
                }
                catch { }
                //String[] sep2 = { "_", " ", "-" };
                FileInfo m_File = new FileInfo(edidFile);
                String[] data2 = m_File.Name.Replace(m_File.Extension,"").Split(',');

                if (!String.IsNullOrEmpty(data2[0]) && String.IsNullOrEmpty(edidTemp.MainDevice))
                    edidTemp.MainDevice = data2[0].Trim();
                if (data2.Length > 1)
                {
                    if (!String.IsNullOrEmpty(data2[1]))
                    {
                        if (data2[1].Trim().ToLower().Contains("avreceiver") || data2[1].Trim().ToLower().Contains("av receiver") || data2[1].Trim().ToLower().Contains("a v receiver"))
                        {
                            edidTemp.SubDevice = "A/V Receiver";
                        }
                        else
                            edidTemp.SubDevice = data2[1].Trim();
                    }
                }

                if (data2.Length > 2)
                {
                    if (!String.IsNullOrEmpty(data2[2]) && String.IsNullOrEmpty(edidTemp.Brand))
                    {
                        edidTemp.Brand = data2[2].Trim();
                        if (edidTemp.Brand.Trim().ToLower().Contains("harman kardon"))
                        {
                            edidTemp.Brand = "Harman/Kardon";
                        }
                    }
                }
                if (data2.Length > 3)
                {
                    if (!String.IsNullOrEmpty(data2[3]))
                        edidTemp.Model = data2[3].Trim();
                }
                //Get Country Information from DB
                try
                {
                    if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                    {
                        if (!String.IsNullOrEmpty(edidTemp.MainDevice) && !String.IsNullOrEmpty(edidTemp.SubDevice) && !String.IsNullOrEmpty(edidTemp.Brand) && !String.IsNullOrEmpty(edidTemp.Model))
                        {
                            String countries = m_Service.GetLocationForEDIDModel(edidTemp.MainDevice, edidTemp.SubDevice, edidTemp.Brand, edidTemp.Model);
                            edidTemp.Region = countries;
                        }
                        else
                            edidTemp.Region = String.Empty;
                    }
                    else
                        edidTemp.Region = String.Empty;
                }
                catch { edidTemp.Region = String.Empty; }                             
            }
            //edidTemp.Region = String.Empty;  
            edidTemp.Component = String.Empty;           
            return edidTemp;
        }
        public String ConvertByteStreamToString(List<Byte> data)
        {
            String _data = String.Empty;
            foreach (Byte b in data)
            {
                String _temp = b.ToString("X");
                _data += _temp + " ";
            }
            return _data;
        }
        private void GetEDIDDataFromFile()
        {
            if (this.EDIDFiles.Count > 0)
            {
                this.EdIdData = null;
                Int32 fileCount = 1;
                foreach (String edidFile in this.EDIDFiles)
                {                    
                    FileInfo m_File              = new FileInfo(edidFile);
                    String fileExtension         = m_File.Extension.ToLower();
                    EDIDInfo edidTemp            = new EDIDInfo();
                    edidTemp.Brand               = String.Empty;
                    edidTemp.Component           = String.Empty;
                    edidTemp.CustomFPType0x00 = String.Empty;
                    edidTemp.CustomFPType0x01 = String.Empty;
                    edidTemp.CustomFPType0x02 = String.Empty;
                    edidTemp.CustomFPType0x03 = String.Empty;
                    edidTemp.CustomFP            = String.Empty;
                    edidTemp.CustomFPOSD        = String.Empty;
                    edidTemp.Only128FP          = String.Empty;
                    edidTemp.Only128FPOSD       = String.Empty;
                    edidTemp.OnlyOSDFP          = String.Empty;
                    edidTemp.FPAvailable         = String.Empty;
                    edidTemp.MainDevice          = String.Empty;
                    edidTemp.ManufacturerCode    = String.Empty;
                    edidTemp.ManufacturerName    = String.Empty;
                    edidTemp.Model               = String.Empty;
                    edidTemp.ProductCode         = String.Empty;
                    edidTemp.Region              = String.Empty;
                    edidTemp.SerialNumber        = String.Empty;
                    edidTemp.SubDevice           = String.Empty;
                    edidTemp.VideoInputDefinition = String.Empty;
                    edidTemp.ManufacturerHexCode = String.Empty;
                    m_Message                   = String.Format("Loading {0} of {1}", fileCount.ToString(), this.EDIDFiles.Count.ToString());
                    switch (fileExtension)
                    {
                        case ".txt":                            
                            edidTemp = GetEDIDDataFromTextFile(edidFile);
                            break;
                        case ".bin":
                            edidTemp = GetEDIDDataFromBinFile(edidFile);
                            break;
                    }
                    edidTemp.DataSource = "Capture EDID";
                    edidTemp.FPAvailable = "N";
                    edidTemp.SlNo = fileCount;

                    if (this.EdIdData == null)
                        this.EdIdData = new List<EDIDInfo>();
                    this.EdIdData.Add(edidTemp);
                    edidTemp = null;
                    fileCount = fileCount + 1;
                }
                if (this.EdIdData != null)
                {
                    this.EdIdDataTable = null;
                    PrepareDataTableForExporting();
                }
            }
        }
        private void btnGetEDIDDetails_Click(object sender, EventArgs e)
        {
            if (this.EDIDFiles != null)
            {
                if (this.EDIDFiles.Count > 0)
                {
                    this.m_ProcessType = ProcessType.GetEDIDFromFile;
                    this.m_Message = "Loading EDID Data from EDID Files.";
                    ResetTimer();
                }
            }
            else if (!String.IsNullOrEmpty(this.txtDirectoryPath.Text))
            {
                this.EDIDDirectoryPath = this.txtDirectoryPath.Text;
                this.m_ProcessType = ProcessType.GetFiles;
                this.m_Message = "Loading EDID Files.";
                ResetTimer();
                if (this.EDIDFiles.Count > 0)
                {
                    this.m_ProcessType = ProcessType.GetEDIDFromFile;
                    this.m_Message = "Loading EDID Data from EDID Files.";
                    ResetTimer();
                }
            }
        }
        #endregion               
    }
}