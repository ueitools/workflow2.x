﻿namespace UEI.EDID.Forms
{
    partial class ViewEdIdForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewEdIdForm));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Header Information");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Basic Display Parameters");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Chromaticity Coordinates");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Established Timing Bitmap");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Standard Timing Information");
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnExcel = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeView = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.etchedLine1 = new UEI.EDID.Forms.EtchedLine();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.edidMoretreeView = new System.Windows.Forms.TreeView();
            this.txtModeDetails = new System.Windows.Forms.TextBox();
            this.txtFP = new System.Windows.Forms.TextBox();
            this.txtFPPosition = new System.Windows.Forms.TextBox();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.txtBrand = new System.Windows.Forms.TextBox();
            this.txtCombinedChecksum = new System.Windows.Forms.TextBox();
            this.txtOSDChecksum = new System.Windows.Forms.TextBox();
            this.txtEDIDChecksum = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtOSDData = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEDIDData = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.etchedLine2 = new UEI.EDID.Forms.EtchedLine();
            this.toolStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 451);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1043, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnExcel});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1043, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnExcel
            // 
            this.btnExcel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnExcel.Enabled = false;
            this.btnExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExcel.Image")));
            this.btnExcel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(23, 22);
            this.btnExcel.Text = "Export To Excel";
            this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeView);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.etchedLine1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel2.Controls.Add(this.txtFP);
            this.splitContainer1.Panel2.Controls.Add(this.txtFPPosition);
            this.splitContainer1.Panel2.Controls.Add(this.txtModel);
            this.splitContainer1.Panel2.Controls.Add(this.txtBrand);
            this.splitContainer1.Panel2.Controls.Add(this.txtCombinedChecksum);
            this.splitContainer1.Panel2.Controls.Add(this.txtOSDChecksum);
            this.splitContainer1.Panel2.Controls.Add(this.txtEDIDChecksum);
            this.splitContainer1.Panel2.Controls.Add(this.label11);
            this.splitContainer1.Panel2.Controls.Add(this.label10);
            this.splitContainer1.Panel2.Controls.Add(this.label9);
            this.splitContainer1.Panel2.Controls.Add(this.label8);
            this.splitContainer1.Panel2.Controls.Add(this.label7);
            this.splitContainer1.Panel2.Controls.Add(this.label6);
            this.splitContainer1.Panel2.Controls.Add(this.txtOSDData);
            this.splitContainer1.Panel2.Controls.Add(this.label5);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.txtEDIDData);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.etchedLine2);
            this.splitContainer1.Size = new System.Drawing.Size(1043, 426);
            this.splitContainer1.SplitterDistance = 197;
            this.splitContainer1.TabIndex = 2;
            // 
            // treeView
            // 
            this.treeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.HideSelection = false;
            this.treeView.Location = new System.Drawing.Point(0, 38);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(197, 388);
            this.treeView.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(191, 34);
            this.label1.TabIndex = 1;
            this.label1.Text = "Main Device\\Sub Device\\Brand\\Model";
            // 
            // etchedLine1
            // 
            this.etchedLine1.Dock = System.Windows.Forms.DockStyle.Top;
            this.etchedLine1.Edge = UEI.EDID.Forms.EtchEdge.Bottom;
            this.etchedLine1.Location = new System.Drawing.Point(0, 0);
            this.etchedLine1.Name = "etchedLine1";
            this.etchedLine1.Size = new System.Drawing.Size(197, 38);
            this.etchedLine1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.splitContainer2);
            this.groupBox1.Location = new System.Drawing.Point(334, 45);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(505, 364);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "More Details";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 16);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.edidMoretreeView);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.txtModeDetails);
            this.splitContainer2.Size = new System.Drawing.Size(499, 345);
            this.splitContainer2.SplitterDistance = 163;
            this.splitContainer2.TabIndex = 0;
            // 
            // edidMoretreeView
            // 
            this.edidMoretreeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.edidMoretreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.edidMoretreeView.HideSelection = false;
            this.edidMoretreeView.Location = new System.Drawing.Point(0, 0);
            this.edidMoretreeView.Name = "edidMoretreeView";
            treeNode1.Name = "Node0";
            treeNode1.Text = "Header Information";
            treeNode2.Name = "Node1";
            treeNode2.Text = "Basic Display Parameters";
            treeNode3.Name = "Node2";
            treeNode3.Text = "Chromaticity Coordinates";
            treeNode4.Name = "Node3";
            treeNode4.Text = "Established Timing Bitmap";
            treeNode5.Name = "Node4";
            treeNode5.Text = "Standard Timing Information";
            this.edidMoretreeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5});
            this.edidMoretreeView.Size = new System.Drawing.Size(163, 345);
            this.edidMoretreeView.TabIndex = 3;
            // 
            // txtModeDetails
            // 
            this.txtModeDetails.BackColor = System.Drawing.Color.White;
            this.txtModeDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtModeDetails.Location = new System.Drawing.Point(0, 0);
            this.txtModeDetails.Multiline = true;
            this.txtModeDetails.Name = "txtModeDetails";
            this.txtModeDetails.ReadOnly = true;
            this.txtModeDetails.Size = new System.Drawing.Size(332, 345);
            this.txtModeDetails.TabIndex = 5;
            // 
            // txtFP
            // 
            this.txtFP.BackColor = System.Drawing.Color.White;
            this.txtFP.Location = new System.Drawing.Point(125, 387);
            this.txtFP.Multiline = true;
            this.txtFP.Name = "txtFP";
            this.txtFP.ReadOnly = true;
            this.txtFP.Size = new System.Drawing.Size(203, 22);
            this.txtFP.TabIndex = 20;
            // 
            // txtFPPosition
            // 
            this.txtFPPosition.BackColor = System.Drawing.Color.White;
            this.txtFPPosition.Location = new System.Drawing.Point(125, 355);
            this.txtFPPosition.Multiline = true;
            this.txtFPPosition.Name = "txtFPPosition";
            this.txtFPPosition.ReadOnly = true;
            this.txtFPPosition.Size = new System.Drawing.Size(203, 22);
            this.txtFPPosition.TabIndex = 19;
            // 
            // txtModel
            // 
            this.txtModel.BackColor = System.Drawing.Color.White;
            this.txtModel.Location = new System.Drawing.Point(125, 325);
            this.txtModel.Multiline = true;
            this.txtModel.Name = "txtModel";
            this.txtModel.ReadOnly = true;
            this.txtModel.Size = new System.Drawing.Size(203, 22);
            this.txtModel.TabIndex = 18;
            // 
            // txtBrand
            // 
            this.txtBrand.BackColor = System.Drawing.Color.White;
            this.txtBrand.Location = new System.Drawing.Point(125, 296);
            this.txtBrand.Multiline = true;
            this.txtBrand.Name = "txtBrand";
            this.txtBrand.ReadOnly = true;
            this.txtBrand.Size = new System.Drawing.Size(203, 22);
            this.txtBrand.TabIndex = 17;
            // 
            // txtCombinedChecksum
            // 
            this.txtCombinedChecksum.BackColor = System.Drawing.Color.White;
            this.txtCombinedChecksum.Location = new System.Drawing.Point(125, 266);
            this.txtCombinedChecksum.Multiline = true;
            this.txtCombinedChecksum.Name = "txtCombinedChecksum";
            this.txtCombinedChecksum.ReadOnly = true;
            this.txtCombinedChecksum.Size = new System.Drawing.Size(203, 22);
            this.txtCombinedChecksum.TabIndex = 16;
            // 
            // txtOSDChecksum
            // 
            this.txtOSDChecksum.BackColor = System.Drawing.Color.White;
            this.txtOSDChecksum.Location = new System.Drawing.Point(125, 236);
            this.txtOSDChecksum.Multiline = true;
            this.txtOSDChecksum.Name = "txtOSDChecksum";
            this.txtOSDChecksum.ReadOnly = true;
            this.txtOSDChecksum.Size = new System.Drawing.Size(203, 22);
            this.txtOSDChecksum.TabIndex = 15;
            // 
            // txtEDIDChecksum
            // 
            this.txtEDIDChecksum.BackColor = System.Drawing.Color.White;
            this.txtEDIDChecksum.Location = new System.Drawing.Point(125, 205);
            this.txtEDIDChecksum.Multiline = true;
            this.txtEDIDChecksum.Name = "txtEDIDChecksum";
            this.txtEDIDChecksum.ReadOnly = true;
            this.txtEDIDChecksum.Size = new System.Drawing.Size(203, 22);
            this.txtEDIDChecksum.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 396);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Custom Fingerprint";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 364);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(111, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Fingerprint Bit Position";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 334);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Model";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 305);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Brand";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 275);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Combined Checksum";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 245);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "OSD Checksum";
            // 
            // txtOSDData
            // 
            this.txtOSDData.BackColor = System.Drawing.Color.White;
            this.txtOSDData.Location = new System.Drawing.Point(125, 124);
            this.txtOSDData.Multiline = true;
            this.txtOSDData.Name = "txtOSDData";
            this.txtOSDData.ReadOnly = true;
            this.txtOSDData.Size = new System.Drawing.Size(203, 73);
            this.txtOSDData.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 219);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "EDID Checksum";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "OSD Data";
            // 
            // txtEDIDData
            // 
            this.txtEDIDData.BackColor = System.Drawing.Color.White;
            this.txtEDIDData.Location = new System.Drawing.Point(125, 45);
            this.txtEDIDData.Multiline = true;
            this.txtEDIDData.Name = "txtEDIDData";
            this.txtEDIDData.ReadOnly = true;
            this.txtEDIDData.Size = new System.Drawing.Size(203, 73);
            this.txtEDIDData.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "EDID Data";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(288, 34);
            this.label2.TabIndex = 2;
            this.label2.Text = "Extended Display Identification Data Details";
            // 
            // etchedLine2
            // 
            this.etchedLine2.Dock = System.Windows.Forms.DockStyle.Top;
            this.etchedLine2.Edge = UEI.EDID.Forms.EtchEdge.Bottom;
            this.etchedLine2.Location = new System.Drawing.Point(0, 0);
            this.etchedLine2.Name = "etchedLine2";
            this.etchedLine2.Size = new System.Drawing.Size(842, 38);
            this.etchedLine2.TabIndex = 1;
            // 
            // ViewEdIdForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1043, 473);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ViewEdIdForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "View EDID";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnExcel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private EtchedLine etchedLine1;
        private System.Windows.Forms.Label label2;
        private EtchedLine etchedLine2;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.TextBox txtEDIDData;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEDIDChecksum;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtOSDData;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFP;
        private System.Windows.Forms.TextBox txtFPPosition;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.TextBox txtBrand;
        private System.Windows.Forms.TextBox txtCombinedChecksum;
        private System.Windows.Forms.TextBox txtOSDChecksum;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TreeView edidMoretreeView;
        private System.Windows.Forms.TextBox txtModeDetails;
    }
}