﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace UEI.EDID.Forms
{
    public partial class ImportCECForm : Form
    {

        #region Variables
        private ProgressForm        m_Progress = null;
        private BackgroundWorker    m_BackgroundWorker = null;
        private ProcessType         m_ProcessType = ProcessType.GetFiles;
        #endregion

        #region Properties
        public String       CECDirectoryPath { get; set; }
        public List<String> CECFiles { get; set; }
        private String      m_ErrorMessage = "Please Wait...";
        public String       ErrorMessage
        {
            get { return m_ErrorMessage; }
            set { m_ErrorMessage = value; }
        }
        public DataTable    CECDeviceDataTable { get; set; }
        public DataTable    CECKeysDataTable { get; set; }
        public DataTable    CECCommandDataTable { get; set; }
        #endregion

        #region Constructor
        public ImportCECForm()
        {
            InitializeComponent();
            this.Load += ImportCECForm_Load;
        }
        void ImportCECForm_Load(object sender, EventArgs e)
        {
            this.btnBrowse.Click += btnBrowse_Click;
            this.btnGetCECDetails.Click += btnGetCECDetails_Click;
            this.btnGetCECDetails.Enabled = false;
            this.m_BackgroundWorker = new BackgroundWorker();
            this.m_BackgroundWorker.DoWork += m_BackgroundWorker_DoWork;
            this.m_BackgroundWorker.RunWorkerCompleted += m_BackgroundWorker_RunWorkerCompleted;
        }               
        #endregion

        #region Event
        void btnBrowse_Click(object sender, EventArgs e)
        {
            BrowseCECFiles();
        }
        void btnGetCECDetails_Click(object sender, EventArgs e)
        {
            if (this.CECFiles != null)
            {
                if (this.CECFiles.Count > 0)
                {
                    this.m_ProcessType = ProcessType.GetCECFromFile;
                    this.m_Message = "Reading CEC Text Files.";
                    ResetTimer();
                }                
            }
        } 
        #endregion

        #region Browse CEC Files
        void BrowseCECFiles()
        {
            this.CECFiles = new List<String>();
            FolderBrowserDialog openPath = new FolderBrowserDialog();
            if (!String.IsNullOrEmpty(this.txtDirectoryPath.Text))
                openPath.SelectedPath = this.txtDirectoryPath.Text;
            else
                openPath.SelectedPath = @"C:\";

            if (openPath.ShowDialog() == DialogResult.OK)
            {
                this.CECDirectoryPath = openPath.SelectedPath;
                this.txtDirectoryPath.Text = this.CECDirectoryPath;
                this.m_ProcessType = ProcessType.GetFiles;
                this.m_Message = "Loading CEC Text Files.";
                ResetTimer();
            }
        }
        void GetFiles()
        {
            this.ErrorMessage = String.Empty;
            try
            {
                if (!String.IsNullOrEmpty(this.CECDirectoryPath))
                {
                    String[] textFiles = Directory.GetFiles(this.CECDirectoryPath, "*.txt", SearchOption.AllDirectories);
                    if (textFiles.Length > 0)
                    {
                        this.CECFiles = new List<String>();
                        foreach (String item in textFiles)
                        {
                            this.CECFiles.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
        }
        private DataTable SupressEmptyColumns(DataTable dtSource)
        {
            System.Collections.ArrayList columnsToRemove = new System.Collections.ArrayList();
            foreach (DataColumn dc in dtSource.Columns)
            {
                bool colEmpty = true;
                foreach (DataRow dr in dtSource.Rows)
                {
                    if (dr[dc.ColumnName].ToString() != String.Empty)
                    {
                        colEmpty = false;
                    }
                }
                if (colEmpty == true)
                {
                    columnsToRemove.Add(dc.ColumnName);
                }
            }
            foreach (String columnName in columnsToRemove)
            {
                if (dtSource.Columns.Contains(columnName) == true)
                    dtSource.Columns.Remove(columnName);
            }
            return dtSource;
        }        
        void GetCECDataFromTextFile()
        {
            try
            {                
                this.ErrorMessage = String.Empty;
                String[] sep = { "\t\r\n", " " };
                String[] sep1 = { "="};
                String[] sep2 = { "," };                
                if (this.CECFiles.Count > 0)
                {                    
                    this.CECDeviceDataTable = new DataTable("CECDevices");
                    this.CECKeysDataTable   = new DataTable("CECKeys");
                    this.CECCommandDataTable = new DataTable("CECCommands");

                    this.CECDeviceDataTable.Columns.Add("Device SLNo", Type.GetType("System.Int32"));
                    this.CECDeviceDataTable.Columns.Add("Device Type");
                    this.CECDeviceDataTable.Columns.Add("Sub Device Type");
                    this.CECDeviceDataTable.Columns.Add("Brand");
                    this.CECDeviceDataTable.Columns.Add("Model");
                    this.CECDeviceDataTable.Columns.Add("Region");
                    this.CECDeviceDataTable.Columns.Add("Country");
                    this.CECDeviceDataTable.Columns.Add("CEC Present");
                    this.CECDeviceDataTable.Columns.Add("CEC Enabled");
                    this.CECDeviceDataTable.Columns.Add("Vendor ID");
                    this.CECDeviceDataTable.Columns.Add("OSD Name");
                    this.CECDeviceDataTable.Columns.Add("EDID");
                    this.CECDeviceDataTable.Columns.Add("CEC Version");
                    this.CECDeviceDataTable.Columns.Add("POD ID");
                    this.CECDeviceDataTable.Columns.Add("Tool Version");
                    this.CECDeviceDataTable.Columns.Add("Logical Address");
                    this.CECDeviceDataTable.Columns.Add("Connected Device");
                    this.CECDeviceDataTable.Columns.Add("Comments");
                    this.CECDeviceDataTable.AcceptChanges();

                    this.CECKeysDataTable.Columns.Add("Key SLNo", Type.GetType("System.Int32"));
                    this.CECKeysDataTable.Columns.Add("Key Name");
                    this.CECKeysDataTable.Columns.Add("Command");
                    this.CECKeysDataTable.Columns.Add("Key Group");
                    this.CECKeysDataTable.AcceptChanges();

                    this.CECCommandDataTable.Columns.Add("Command SLNo", Type.GetType("System.Int32"));
                    this.CECCommandDataTable.Columns.Add("Device SLNo", Type.GetType("System.Int32"));
                    this.CECCommandDataTable.Columns.Add("Key SLNo", Type.GetType("System.Int32"));
                    this.CECCommandDataTable.Columns.Add("Command");
                    this.CECCommandDataTable.Columns.Add("Commands Worked");
                    this.CECCommandDataTable.Columns.Add("Command Status");
                    this.CECCommandDataTable.Columns.Add("Command Acknowledgement Recevied");
                    this.CECCommandDataTable.Columns.Add("Command Delay");
                    this.CECCommandDataTable.Columns.Add("Command Date");
                    this.CECCommandDataTable.Columns.Add("Command Time");
                    this.CECCommandDataTable.Columns.Add("Comments");
                    this.CECCommandDataTable.AcceptChanges();

                    String strColumnHeading = String.Empty;                    
                    Int32 fileCount = 1;
                    DataRow dr1 = null;
                    DataRow dr2 = null;
                    DataRow dr3 = null;
                    foreach (String cecFile in this.CECFiles)
                    {
                        m_Message = String.Format("Reading {0} of {1} Files", fileCount.ToString(), this.CECFiles.Count.ToString());
                        try
                        {
                            dr1 = this.CECDeviceDataTable.NewRow();
                            dr1["Device SLNo"] = fileCount;
                            StreamReader streamReader = new StreamReader(cecFile);
                            String line = streamReader.ReadLine();
                            if (line.Contains("[Capture ID]"))
                            {
                                line = streamReader.ReadLine();
                                if (line.Contains("ID"))
                                {
                                    String[] podid = line.Split(sep1, StringSplitOptions.RemoveEmptyEntries);
                                    if (podid.Length > 1)
                                        dr1["POD ID"] = podid[1];
                                }
                                line = streamReader.ReadLine();
                                if (line.Contains("[Tool Version]"))
                                {
                                    line = streamReader.ReadLine();
                                    dr1["Tool Version"] = line.Trim();
                                }
                                do
                                {
                                    line = streamReader.ReadLine();
                                } while (!line.Contains("[Selected Device]"));
                                line = streamReader.ReadLine();
                                if (line.Contains("Logical Address"))
                                {
                                    String[] logadd = line.Split(sep1, StringSplitOptions.RemoveEmptyEntries);
                                    if (logadd.Length > 1)
                                        dr1["Logical Address"] = logadd[1];
                                }
                                line = streamReader.ReadLine();
                                if (line.Contains("Device Type"))
                                {
                                    String[] device = line.Split(sep1, StringSplitOptions.RemoveEmptyEntries);
                                    if (device.Length > 1)
                                        dr1["Device Type"] = device[1];
                                }
                                do
                                {
                                    line = streamReader.ReadLine();
                                } while (!line.Contains("[Brand Device Model Selections]"));
                                line = streamReader.ReadLine();
                                if (line.Contains("Brand Name"))
                                {
                                    String[] brand = line.Split(sep1, StringSplitOptions.RemoveEmptyEntries);
                                    if (brand.Length > 1)
                                        dr1["Brand"] = brand[1];
                                }
                                line = streamReader.ReadLine();
                                if (line.Contains("Device Type"))
                                {
                                    String[] subdevice = line.Split(sep1, StringSplitOptions.RemoveEmptyEntries);
                                    if (subdevice.Length > 1)
                                        dr1["Sub Device Type"] = subdevice[1];
                                }
                                line = streamReader.ReadLine();
                                if (line.Contains("Model"))
                                {
                                    String[] model = line.Split(sep1, StringSplitOptions.RemoveEmptyEntries);
                                    if (model.Length > 1)
                                        dr1["Model"] = model[1];
                                }
                                while (!streamReader.EndOfStream)
                                {
                                    line = streamReader.ReadLine();
                                    if (line.Contains("[Ping All Devices Selections]"))
                                    {
                                        line = streamReader.ReadLine();
                                        String[] subdevice = line.Split(sep1, StringSplitOptions.RemoveEmptyEntries);
                                        if (subdevice.Length > 1)
                                            dr1["CEC Present"] = subdevice[1];
                                    }
                                    if (line.Contains("<Device Vendor ID>"))
                                    {
                                        String[] vendid = line.Replace("<Device Vendor ID>", "").Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                        if (vendid.Length > 0)
                                        {
                                            String strVenID = String.Empty;
                                            for (int i = 2; i < vendid.Length - 1; i++)
                                            {
                                                strVenID += vendid[i] + " ";
                                            }
                                            dr1["Vendor ID"] = strVenID;
                                        }
                                    }
                                    if (line.Contains("<Give OSD Name>"))
                                    {
                                        String[] osdname = line.Replace("<Give OSD Name>", "").Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                        if (osdname.Length > 0)
                                        {
                                            String strosdname = String.Empty;
                                            for (int i = 2; i < osdname.Length - 1; i++)
                                            {
                                                strosdname += osdname[i] + " ";
                                            }
                                            dr1["OSD Name"] = strosdname;
                                        }
                                    }
                                    if (line.Contains("<CEC Version>"))
                                    {
                                        String[] cecver = line.Replace("<CEC Version>", "").Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                        if (cecver.Length > 0)
                                        {
                                            String strcecver = String.Empty;
                                            for (int i = 2; i < cecver.Length - 1; i++)
                                            {
                                                strcecver += cecver[i] + " ";
                                            }
                                            dr1["CEC Version"] = strcecver;
                                        }
                                    }
                                }                                                                                                
                            }
                            streamReader.Close();
                            streamReader = null;
                            this.CECDeviceDataTable.Rows.Add(dr1);
                        }
                        catch { }
                       
                        fileCount = fileCount + 1;
                    }
                    this.CECDeviceDataTable.AcceptChanges();
                    if (this.CECDeviceDataTable.Rows.Count > 0)
                    {
                        this.CECDeviceDataTable = SupressEmptyColumns(this.CECDeviceDataTable);
                    }
                    this.CECKeysDataTable.AcceptChanges();
                    if (this.CECKeysDataTable.Rows.Count > 0)
                    {
                        this.CECKeysDataTable = SupressEmptyColumns(this.CECKeysDataTable);
                    }
                    this.CECCommandDataTable.AcceptChanges();
                    if (this.CECCommandDataTable.Rows.Count > 0)
                    {
                        this.CECCommandDataTable = SupressEmptyColumns(this.CECCommandDataTable);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
        }
        #endregion       

        #region Background Progress
        private String m_Message = "Please Wait...";
        private void ResetTimer()
        {
            if (m_BackgroundWorker != null || m_BackgroundWorker.IsBusy == false)
                m_BackgroundWorker.RunWorkerAsync();

            if (m_Progress == null)
            {
                m_Progress = new ProgressForm();
                m_Progress.StartPosition = FormStartPosition.CenterParent;
            }
            m_Progress.SetMessage(m_Message);
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();
            Timer.Start();
        }
        void Timer_Tick(object sender, EventArgs e)
        {
            ProgressMessage();
        }
        private void ProgressMessage()
        {
            if (this.m_Progress != null)
                m_Progress.SetMessage(m_Message, "Please Wait...");
        }
        void m_BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Timer.Stop();
            m_Progress.DialogResult = DialogResult.OK;
            m_Progress.Close();
            switch (this.m_ProcessType)
            {
                case ProcessType.GetFiles:
                    if (!String.IsNullOrEmpty(this.ErrorMessage))
                    {
                        MessageBox.Show(this.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        this.btnGetCECDetails.Enabled = true;
                    }
                    break;
                case ProcessType.GetCECFromFile:
                    this.dataGridView1.DataSource = null;
                    this.dataGridView2.DataSource = null;
                    this.dataGridView3.DataSource = null;
                    if (!String.IsNullOrEmpty(this.ErrorMessage))
                    {
                        MessageBox.Show(this.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {                        
                        this.dataGridView1.DataSource = this.CECDeviceDataTable;
                        this.dataGridView2.DataSource = this.CECKeysDataTable;
                        this.dataGridView3.DataSource = this.CECCommandDataTable;
                    }
                    break;
            }
        }
        void m_BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (this.m_ProcessType)
            {
                case ProcessType.GetFiles:
                    GetFiles();
                    break;
                case ProcessType.GetCECFromFile:
                    GetCECDataFromTextFile();
                    break;               
            }
        }
        #endregion

        #region Propcess Types
        private enum ProcessType
        {                       
            GetFiles,
            GetCECFromFile
        }
        #endregion
    }
}
