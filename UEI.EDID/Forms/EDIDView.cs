﻿using CommonForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report;
using UEI.Workflow2010.Report.ExportToExcel;
using UEI.Workflow2010.Report.Model;
using UEI.Workflow2010.Report.Service;

namespace UEI.EDID.Forms
{
    public partial class EDIDView : Form
    {
        public enum ProcessTypes
        {
            GetEdId,
            FilterByLocation,
            FilterByDevice,
            UpdateLinkedID
        }
        #region Variables
        private IList<Region> m_Regions                 = new List<Region>();
        private IList<DeviceType> m_DeviceTypes         = new List<DeviceType>();
        private IList<DataSources> m_DataSources        = new List<DataSources>();
        private SelType m_SelectedType                  = SelType.MODE;
        public List<String> m_SelectedModes             = new List<String>();
        private IdSetupCodeInfo m_IDSetupCodeInfoParams = null;
        private BrandFilter m_BrandFilter               = null;
        private ModelFilter m_ModelFilter               = null;
        private IList<Id> m_Ids                         = new List<Id>();
        private ProgressForm m_Progress                 = null;
        private BackgroundWorker m_BackgroundWorker     = null;
        private EdIdServices m_Service                  = null;        
        private LinkingIDTrustLevelForm m_LinkingIdTrustLevel = null;
        #endregion

        #region Properties
        public TreeNode         SelectedLocationTreeNode { get; set; }
        public TreeNode         SelectedDeviceTypeTreeNode { get; set; }
        public ProcessTypes     ProcessType { get; set; }
        public List<EDIDInfo>   EdIdData { get; set; }
        public List<EDIDInfo>   FilteredEdIdData { get; set; }
        public DataTable        EdIdDataTable { get; set; }
        public DataTable        EdIdWithFileStrcuture { get; set; }
        public EDIDInfo         SelectedEdId { get; set; }
        //public EDID             EDIDDetails { get; set; }   
        public DecodedEDIDData EDIDDetails { get; set; }
        #endregion

        #region Constrcutor
        public EDIDView()
        {
            InitializeComponent();
            this.Load += EDIDView_Load;
        }
        void EDIDView_Load(object sender, EventArgs e)
        {
            this.lblStatus.Text                         = String.Empty;
            this.m_BackgroundWorker                     = new BackgroundWorker();
            this.Timer.Tick                             += Timer_Tick;
            this.m_BackgroundWorker.DoWork              += m_BackgroundWorker_DoWork;
            this.m_BackgroundWorker.RunWorkerCompleted  += m_BackgroundWorker_RunWorkerCompleted;
            this.treeViewLocations.BeforeSelect         += treeViewLocations_BeforeSelect;
            this.treeViewLocations.AfterSelect          += treeViewLocations_AfterSelect;
            this.treeViewDevices.BeforeSelect           += treeViewDevices_BeforeSelect;
            this.treeViewDevices.AfterSelect            += treeViewDevices_AfterSelect;
            this.edidDataGridView.MouseDown             += new MouseEventHandler(DataGridViewMouseDown);
            this.edidDataGridView.SelectionChanged      += new EventHandler(DataGridViewSelectionChanged);
            try
            {
                Reports m_Service = new Reports();
                IList<String> idList = GetIDs();
                if (idList != null && idList.Count > 0)
                {
                    ReportFilterForm m_ReportFilterForm = new ReportFilterForm(ReportType.EdId, idList);
                    if (m_ReportFilterForm.ShowDialog() == DialogResult.OK)
                    {
                        m_Ids                   = m_ReportFilterForm.Ids;
                        m_Regions               = m_ReportFilterForm.Regions;
                        m_DeviceTypes           = m_ReportFilterForm.DeviceTypes;
                        m_SelectedModes         = m_ReportFilterForm._selectedModes;
                        m_IDSetupCodeInfoParams = m_ReportFilterForm.IdSetupCodeInfoParam;
                        m_BrandFilter           = m_ReportFilterForm.BrandSpecificSel;
                        m_ModelFilter           = m_ReportFilterForm.ModelSpecificSel;
                        m_DataSources           = m_ReportFilterForm.DataSources;
                        m_ReportFilterForm.Close();
                        this.ProcessType        = ProcessTypes.GetEdId;
                        ResetTimer();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Some error occured in report generation.Error logged");
            }  
        }        
        #endregion

        #region Tree View Selected
        void treeViewDevices_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeNode node = ((TreeNode)e.Node);
                if (node != null && node.Tag != null)
                {
                    this.SelectedDeviceTypeTreeNode = node;
                    this.SelectedLocationTreeNode = null;
                    this.ProcessType = ProcessTypes.FilterByDevice;
                    ResetTimer();
                }
            }
        }
        void treeViewDevices_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Action == TreeViewAction.Unknown)
            {
                if (e.Node.Parent == null)
                    e.Cancel = true;
            }
        }
        void treeViewLocations_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {                
                TreeNode node = ((TreeNode)e.Node);
                if (node != null && node.Tag != null)
                {
                    this.SelectedDeviceTypeTreeNode = null;
                    this.SelectedLocationTreeNode = node;
                    this.ProcessType = ProcessTypes.FilterByLocation;
                    ResetTimer();
                }
            }
        }
        void treeViewLocations_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Action == TreeViewAction.Unknown)
            {
                if (e.Node.Parent == null)
                    e.Cancel = true;
            }
        }
        #endregion

        #region Bind Tree View
        private void LoadFilteredEDIDData()
        {
            this.FilteredEdIdData = null;            
            if (this.SelectedDeviceTypeTreeNode != null && this.EdIdData != null)
            {
                switch (this.SelectedDeviceTypeTreeNode.Tag.ToString())
                {
                    case "MainDevice":
                        foreach (EDIDInfo item in this.EdIdData)
                        {
                            if (FilteredEdIdData == null)
                                FilteredEdIdData = new List<EDIDInfo>();
                            if (item.MainDevice == this.SelectedDeviceTypeTreeNode.Text)
                                FilteredEdIdData.Add(item);
                        }
                        break;
                    case "SubDevice":
                        foreach (EDIDInfo item in this.EdIdData)
                        {
                            if (FilteredEdIdData == null)
                                FilteredEdIdData = new List<EDIDInfo>();
                            if ((item.MainDevice == this.SelectedDeviceTypeTreeNode.Parent.Text) && (item.SubDevice == this.SelectedDeviceTypeTreeNode.Text))
                                FilteredEdIdData.Add(item);
                        }
                        break;
                }
            }
            if (this.SelectedLocationTreeNode != null && this.EdIdData != null)
            {
                foreach (EDIDInfo item in this.EdIdData)
                {
                    if (FilteredEdIdData == null)
                        FilteredEdIdData = new List<EDIDInfo>();
                    if (item.Region == this.SelectedLocationTreeNode.Text)
                        FilteredEdIdData.Add(item);
                }
            }
        }
        private void BindFilteredEDIDData()
        {
            if (FilteredEdIdData != null)
            {
                this.lblStatus.Text = "No of Records :" + FilteredEdIdData.Count.ToString();
                this.edidDataGridView.DataSource = FilteredEdIdData;
                this.edidDataGridView.Rows[0].Cells[0].Selected = false;
                this.edidDataGridView.Columns["EdIdRawData"].Visible = false;
                this.edidDataGridView.Columns["Region"].HeaderText = "Country";
                this.edidDataGridView.Columns["ConsoleCount"].Visible = false;
            }
        }
        private void BindEDIDData()
        {
            this.lblStatus.Text = String.Empty;
            this.edidDataGridView.DataSource = null;
            if (this.treeViewLocations.Nodes.Count > 0)
                this.treeViewLocations.Nodes.Clear();

            if (this.treeViewDevices.Nodes.Count > 0)
                this.treeViewDevices.Nodes.Clear();
            if (this.EdIdData != null)
            {
                foreach (EDIDInfo item in this.EdIdData)
                {
                    if ((!this.treeViewDevices.Nodes.ContainsKey(item.MainDevice)) && (!String.IsNullOrEmpty(item.MainDevice)))
                    {                       
                        this.treeViewDevices.Nodes.Add(item.MainDevice, item.MainDevice);
                        this.treeViewDevices.Nodes[item.MainDevice].Tag = "MainDevice";
                    }
                    if ((!this.treeViewDevices.Nodes[item.MainDevice].Nodes.ContainsKey(item.SubDevice)) && (!String.IsNullOrEmpty(item.SubDevice)))
                    {
                        this.treeViewDevices.Nodes[item.MainDevice].Nodes.Add(item.SubDevice, item.SubDevice);
                        this.treeViewDevices.Nodes[item.MainDevice].Nodes[item.SubDevice].Tag = "SubDevice";
                    }
                    if ((!this.treeViewLocations.Nodes.ContainsKey(item.Region))&&(!String.IsNullOrEmpty(item.Region)))
                    {
                        this.treeViewLocations.Nodes.Add(item.Region, item.Region);
                        this.treeViewLocations.Nodes[item.Region].Tag = "Country";
                    }
                }
                this.lblStatus.Text = "No of Records :" + this.EdIdData.Count.ToString();
                this.edidDataGridView.DataSource = this.EdIdData;
                this.edidDataGridView.Rows[0].Cells[0].Selected = false;
                this.edidDataGridView.Columns["EdIdRawData"].Visible = false;
                this.edidDataGridView.Columns["Region"].HeaderText = "Country";
                this.edidDataGridView.Columns["ConsoleCount"].Visible = false;
            }
        }
        #endregion

        #region DataGridView Mouse Click
        private void DataGridViewMouseDown(Object Sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hitTestInfo = this.edidDataGridView.HitTest(e.X, e.Y);
            if (hitTestInfo.RowIndex != -1 && hitTestInfo.ColumnIndex == -1)
            {
                this.edidDataGridView.ClearSelection();
                this.edidDataGridView.Rows[hitTestInfo.RowIndex].Selected = true;
                this.SelectedEdId = (EDIDInfo)this.edidDataGridView.Rows[hitTestInfo.RowIndex].DataBoundItem;
                BindEdIdDetails();
                if (this.SelectedEdId != null)
                {
                    if ((e.Button == System.Windows.Forms.MouseButtons.Right) && (this.SelectedEdId.DataSource.Contains("Capture EDID")))
                    {
                        this.contextMenu.Show(this.edidDataGridView, new System.Drawing.Point(e.X, e.Y));
                    }
                }
            }           
        }
        #endregion

        #region DataGridView Row Select
        void DataGridViewSelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow m_Row in this.edidDataGridView.SelectedRows)
            {
                this.edidDataGridView.Rows[m_Row.Index].Selected = true;
                this.SelectedEdId = (EDIDInfo)this.edidDataGridView.Rows[m_Row.Index].DataBoundItem;
                BindEdIdDetails();
            }
        }
        #endregion

        #region Bind EDID Details
        void ClearAllControls()
        {
            //this.edidMoretreeView.SelectedNode = null;
            //this.txtEDIDData.Text = String.Empty;
            //this.txtOSDData.Text = String.Empty;
            //this.txtEDIDChecksum.Text = String.Empty;
            //this.txtOSDChecksum.Text = String.Empty;
            //this.txtCombinedChecksum.Text = String.Empty;
            //this.txtBrand.Text = String.Empty;
            //this.txtModel.Text = String.Empty;
            //this.txtFPPosition.Text = String.Empty;
            //this.txtFP.Text = String.Empty;
            //this.txtModeDetails.Text = String.Empty;
        }
        void BindEdIdDetails()
        {
            m_Service = new EdIdServices();
            if (!String.IsNullOrEmpty(SelectedEdId.SlNo.ToString()) && EdIdWithFileStrcuture != null)
            {
                DataRow[] dr = EdIdWithFileStrcuture.Select("SlNo='" + SelectedEdId.SlNo.ToString() + "'");
                if (dr.Length > 0)
                {
                    Byte[] tempBinData = (Byte[])dr[0][1];
                    if (tempBinData.Length > 0)
                    {                   
                        EDIDInfo m_FileStructure = m_Service.ReadBinFile(tempBinData);
                        if (m_FileStructure.EdId != null)
                        {
                            EDIDParser m_EDIDDecoder = new EDIDParser();
                            EDIDData m_EDIDObject = m_EDIDDecoder.ParseEDID(m_FileStructure.EdId.ToArray());
                            this.EDIDDetails = m_Service.GetDecodedEDIDDataFromEDIDObject(m_EDIDObject);
                            if (this.EDIDDetails != null)
                                this.edidPropertyDetails.SelectedObject = this.EDIDDetails;
                        }
                    }
                }
            }
        }
        #endregion

        #region Background Progress
        void m_BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Timer.Stop();
            m_Progress.DialogResult = DialogResult.OK;
            m_Progress.Close();
            if (!String.IsNullOrEmpty(this.m_Service.ErrorMessage))
            {
                MessageBox.Show(this.m_Service.ErrorMessage, "EDID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                switch (this.ProcessType)
                {
                    case ProcessTypes.GetEdId:
                        if (this.treeViewLocations.Nodes.Count > 0)
                            this.treeViewLocations.Nodes.Clear();
                        if (this.treeViewDevices.Nodes.Count > 0)
                            this.treeViewDevices.Nodes.Clear();
                        if (this.EdIdData != null)
                        {
                            BindEDIDData();
                            this.btnExcel.Enabled = true;
                        }
                        else if (this.m_Service.ErrorMessage != String.Empty)
                        {
                            this.btnExcel.Enabled = false;
                            MessageBox.Show(this.m_Service.ErrorMessage, "EDID", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            this.btnExcel.Enabled = false;
                            MessageBox.Show("No Data Found", "EDID", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    case ProcessTypes.FilterByLocation:
                        BindFilteredEDIDData();
                        break;
                    case ProcessTypes.FilterByDevice:
                        BindFilteredEDIDData();
                        break;
                    case ProcessTypes.UpdateLinkedID:                        
                        break;
                }
        }
        void m_BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (this.ProcessType)
            {
                case ProcessTypes.GetEdId:
                    GetEdIdData();
                    break;
                case ProcessTypes.FilterByLocation:
                    LoadFilteredEDIDData();
                    break;
                case ProcessTypes.FilterByDevice:
                    LoadFilteredEDIDData();
                    break;   
                case ProcessTypes.UpdateLinkedID:
                    UpdateLinkedID();
                    break;
            }            
        }
        private void ResetTimer()
        {
            if (m_BackgroundWorker != null || m_BackgroundWorker.IsBusy == false)
                m_BackgroundWorker.RunWorkerAsync();

            if (m_Progress == null)
            {
                m_Progress = new ProgressForm();
                m_Progress.StartPosition = FormStartPosition.CenterParent;
            }
            m_Progress.SetMessage("Please Wait...");
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();
            Timer.Start();
        }
        void Timer_Tick(object sender, EventArgs e)
        {
            ProgressMessage();
        }
        private String m_Message = "Please Wait...";
        private void ProgressMessage()
        {
            if (this.m_Progress != null)
                m_Progress.SetMessage(m_Message, "Please Wait...");
        }
        #endregion

        #region Get ID List
        private IList<String> GetIDs()
        {
            IList<String> idList = null;
            IDSelectionForm idSelectionForm = null;
            try
            {
                //Use id selection form object
                idSelectionForm = new IDSelectionForm();
                DialogResult result = idSelectionForm.ShowDialog();

                if (result == DialogResult.OK)
                {
                    m_SelectedType = idSelectionForm.SelectedType;
                    idList = idSelectionForm.SelectedIDList;
                    if (idList == null || idList.Count == 0)
                        MessageBox.Show("No ids retrieved for the selection.");
                }
                return idList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idSelectionForm != null)
                {
                    idSelectionForm.Dispose();
                }
            }
        }
        #endregion

        #region Get EdId Data
        private void GetEdIdData()
        {
            try
            {
                m_Service = new EdIdServices();
                DataTable resultCollection = null;
                m_Message = "Loading EDID";
                EdIdData = m_Service.GetEdIdData(m_Regions, m_SelectedModes, m_DeviceTypes, m_DataSources, m_IDSetupCodeInfoParams, m_BrandFilter, m_ModelFilter, ref resultCollection, ref m_Message);
                this.EdIdWithFileStrcuture = resultCollection;
            }
            catch { }
            
        }
        #endregion

        #region Export To Excel
        private DataSet dsDataSet = new DataSet("EDID Summary");
        private void CreateEdIdDataTable()
        {
            if (EdIdDataTable == null)
            {
                EdIdDataTable = new DataTable("EDID");
                EdIdDataTable.Columns.Add("SL NO");
                EdIdDataTable.Columns.Add("Main Device");
                EdIdDataTable.Columns.Add("Sub Device");
                EdIdDataTable.Columns.Add("Component");
                EdIdDataTable.Columns.Add("Region");
                EdIdDataTable.Columns.Add("Brand");
                EdIdDataTable.Columns.Add("Model");
                EdIdDataTable.Columns.Add("EDID Block");
                EdIdDataTable.Columns.Add("Manufacturer (3 char) code");
                EdIdDataTable.Columns.Add("Manufacturer Hex Code");
                EdIdDataTable.Columns.Add("Monitor Name");
                EdIdDataTable.Columns.Add("Product Code");
                EdIdDataTable.Columns.Add("Serial number");
                EdIdDataTable.Columns.Add("Week");
                EdIdDataTable.Columns.Add("Year");
                EdIdDataTable.Columns.Add("Horizontal Screen Size");
                EdIdDataTable.Columns.Add("Vertical Screen Size");
                EdIdDataTable.Columns.Add("Video input definition");
                EdIdDataTable.Columns.Add("OSD Name");
                EdIdDataTable.Columns.Add("FP Available");
                EdIdDataTable.Columns.Add("FP Bytes");

                EdIdDataTable.Columns.Add("Custom FP Type0x00");
                EdIdDataTable.Columns.Add("Custom FP Type0x01");
                EdIdDataTable.Columns.Add("Custom FP Type0x02");
                EdIdDataTable.Columns.Add("Custom FP Type0x03");

                EdIdDataTable.Columns.Add("Custom FP");
                EdIdDataTable.Columns.Add("Custom FP + OSD");
                EdIdDataTable.Columns.Add("128 FP");
                EdIdDataTable.Columns.Add("128 FP + OSD");
                EdIdDataTable.Columns.Add("OSD FP");
                EdIdDataTable.Columns.Add("CEC Physical Address");
                EdIdDataTable.Columns.Add("DAC Published ID");
                EdIdDataTable.Columns.Add("Trusted Level");
            }
            else
                EdIdDataTable.Clear();
        }
        private void FillEdIdDataTableDate()
        {
            EdIdServices m_Service = new EdIdServices();
            if (this.EdIdData != null)
            {
                DataRow dr = null;
                foreach (EDIDInfo item in this.EdIdData)
                {
                    dr = EdIdDataTable.NewRow();
                    dr["SL NO"] = item.SlNo;
                    dr["Main Device"] = item.MainDevice;
                    dr["Sub Device"] = item.SubDevice;
                    dr["Component"] = item.Component;
                    dr["Region"] = item.Region;
                    dr["Brand"] = item.Brand;
                    dr["Model"] = item.Model;
                    dr["EDID Block"] = item.EdIdRawData;
                    dr["Manufacturer (3 char) code"] = item.ManufacturerCode;
                    dr["Manufacturer Hex Code"] = item.ManufacturerHexCode;
                    dr["Monitor Name"] = item.ManufacturerName;
                    dr["Product Code"] = item.ProductCode;
                    dr["Serial number"] = item.SerialNumber;
                    dr["Week"] = item.Week;
                    dr["Year"] = item.Year;
                    dr["Horizontal Screen Size"] = item.HorizontalScreenSize;
                    dr["Vertical Screen Size"] = item.VerticalScreenSize;
                    dr["Video input definition"] = item.VideoInputDefinition;
                    dr["OSD Name"] = item.OSDRawData;
                    dr["FP Available"] = item.FPAvailable;
                    dr["FP Bytes"] = item.FPBytePosition;
                    dr["Custom FP Type0x00"] = item.CustomFPType0x00;
                    dr["Custom FP Type0x01"] = item.CustomFPType0x01;
                    dr["Custom FP Type0x02"] = item.CustomFPType0x02;
                    dr["Custom FP Type0x03"] = item.CustomFPType0x03;
                    dr["CEC Physical Address"] = item.PhysicalAddress;
                    dr["DAC Published ID"] = item.DACPublishedID;
                    dr["Trusted Level"] = item.TrustLevel;
                    DataRow[] dr1 = EdIdWithFileStrcuture.Select("SlNo='" + item.SlNo + "'");
                    if (dr1.Length > 0)
                    {
                        Byte[] tempBinData = (Byte[])dr1[0][1];
                        if (tempBinData.Length > 0)
                        {
                            try
                            {
                                EDIDReader m_EdIdReader = new EDIDReader();
                                m_EdIdReader.ReadBinFile(tempBinData);
                                m_EdIdReader.GetEdidData();
                                m_EdIdReader.GetOSDData();

                                dr["Custom FP"] = m_Service.GetFingerPintCheckSum(item, false);
                                if (item.OSD != null)
                                {
                                    if (item.OSD.Count > 0)
                                        dr["Custom FP + OSD"] = m_Service.GetFingerPintCheckSum(item, true);
                                }
                                dr["128 FP"] = m_Service.GetFingerPrint(m_EdIdReader.GetEDIDChecksum());
                                dr["128 FP + OSD"] = m_Service.GetFingerPrint(m_EdIdReader.GetCombinedChecksum());
                                dr["OSD FP"] = m_Service.GetFingerPrint(m_EdIdReader.GetOSDChecksum());
                            }
                            catch
                            {                                
                                dr["Custom FP"] = String.Empty;
                                dr["Custom FP + OSD"] = String.Empty;
                                dr["128 FP"] = String.Empty;
                                dr["128 FP + OSD"] = String.Empty;
                                dr["OSD FP"] = String.Empty;
                            }
                        }
                    }

                    this.EdIdDataTable.Rows.Add(dr);
                    dr = null;
                }
                this.EdIdDataTable.AcceptChanges();
            }
        }
        private void btnExcel_Click(object sender, EventArgs e)
        {
            if (EdIdData != null)
            {
                CreateEdIdDataTable();
                FillEdIdDataTableDate();
                String m_SaveFileMessage = String.Empty;
                if (dsDataSet.Tables.Contains(EdIdDataTable.TableName))
                    dsDataSet.Tables.Remove(EdIdDataTable.TableName);
                dsDataSet.Tables.Add(EdIdDataTable);
                try
                {
                    String m_filePath = CommonForms.Configuration.GetWorkingDirectory() + "\\";
                    if (String.IsNullOrEmpty(m_filePath))
                    {
                        FolderBrowserDialog dlg1 = new FolderBrowserDialog();
                        if (dlg1.ShowDialog() == DialogResult.OK)
                        {
                            if (dlg1.SelectedPath.EndsWith("\\") == false)
                            {
                                dlg1.SelectedPath += "\\";
                            }
                            m_filePath = dlg1.SelectedPath;
                        }
                    }
                    ExcelReportGenerator.GenerateReport(dsDataSet, m_filePath, "EDID Summary", out m_SaveFileMessage);
                    if (!String.IsNullOrEmpty(m_SaveFileMessage))
                    {
                        MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        #endregion       

        #region Update Linked ID
        void UpdateLinkedID()
        {
            try
            {
                m_Message = "Updating Linked ID";
                m_Service = new EdIdServices(BusinessObject.DBConnectionString.EdId);
                m_Service.UpdateLinkedID(this.SelectedEdId);                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "EDID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void updateLinkedIDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.SelectedEdId != null)
                {
                    m_LinkingIdTrustLevel = new LinkingIDTrustLevelForm();
                    m_LinkingIdTrustLevel.SelectedEDID = this.SelectedEdId;
                    if (m_LinkingIdTrustLevel.ShowDialog() == DialogResult.OK)
                    {
                        this.ProcessType = ProcessTypes.UpdateLinkedID;
                        ResetTimer();
                        if (!String.IsNullOrEmpty(this.m_Service.ErrorMessage))
                        {
                            this.SelectedEdId = m_LinkingIdTrustLevel.SelectedEDID;
                            if (this.EdIdData != null)
                            {
                                foreach (EDIDInfo item in this.EdIdData)
                                {
                                    if (item.SlNo == this.SelectedEdId.SlNo)
                                    {
                                        item.DACPublishedID = this.SelectedEdId.DACPublishedID;
                                        item.TrustLevel = this.SelectedEdId.TrustLevel;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "EDID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
    }
}