﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.EDID.Forms
{
    public partial class FingerPrintCalculationForm : Form
    {
        #region Variables
        #endregion

        #region Properties
        public EDIDInfo     SelectedEdId { get; set; }
        public DataTable    EdIdDataTable { get; set; }
        public String       SelectedMainDeviceType { get; set; }
        public String       SelectedSubDeviceType { get; set; }
        public String       SelectedBrand { get; set; }
        public String       BytePositions { get; set; }
        public List<Int32>  BytePosforFingerPrint { get; set; }
        public String       ErrorMessage { get; set; }
        #endregion

        #region Constrcutor
        public FingerPrintCalculationForm()
        {
            InitializeComponent();
            this.Load += FingerPrintCalculationForm_Load;
        }
        void FingerPrintCalculationForm_Load(object sender, EventArgs e)
        {
            if (this.SelectedEdId != null)
            {
                this.cmbMainDeviceType.Items.Clear();
                this.cmbSubDeviceType.Items.Clear();
                this.cmbBrand.Items.Clear();
                if(!String.IsNullOrEmpty(this.SelectedEdId.MainDevice))
                this.cmbMainDeviceType.Items.Add(this.SelectedEdId.MainDevice);
                if (!String.IsNullOrEmpty(this.SelectedEdId.SubDevice))
                this.cmbSubDeviceType.Items.Add(this.SelectedEdId.SubDevice);
                if (!String.IsNullOrEmpty(this.SelectedEdId.Brand))
                this.cmbBrand.Items.Add(this.SelectedEdId.Brand);
                this.cmbMainDeviceType.SelectedText = this.SelectedEdId.MainDevice;
                this.cmbSubDeviceType.SelectedText = this.SelectedEdId.SubDevice;
                this.cmbBrand.SelectedText = this.SelectedEdId.Brand;
            }
            else
            {
                this.cmbMainDeviceType.SelectedIndexChanged += cmbMainDeviceType_SelectedIndexChanged;
                this.cmbSubDeviceType.SelectedIndexChanged  += cmbSubDeviceType_SelectedIndexChanged;
                this.cmbBrand.SelectedIndexChanged          += cmbBrand_SelectedIndexChanged;
                BindMainDevices();
            }
            if (!String.IsNullOrEmpty(BytePositions))
                this.txtBytePosition.Text = this.BytePositions;
            this.btnCancel.Click += btnCancel_Click;
            this.btnOK.Click += btnOK_Click;
            this.txtBytePosition.Focus();
        }

        

                                    
        #endregion

        #region Cancel
        void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region Bind Main Devices
        void BindMainDevices()
        {
            this.cmbMainDeviceType.Items.Clear();
            if (this.EdIdDataTable != null)
            {
                DataTable dtTemp = this.EdIdDataTable.DefaultView.ToTable(true, new String[] { "Main Device" });
                if (dtTemp.Rows.Count > 0)
                {
                    this.cmbMainDeviceType.Items.Add("");
                    foreach (DataRow item in dtTemp.Rows)
                    {
                        if (!String.IsNullOrEmpty(item[0].ToString()))
                            this.cmbMainDeviceType.Items.Add(item[0].ToString());
                    }
                }
            }
        }
        void cmbMainDeviceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmbMainDeviceType.SelectedIndex != -1)
            {
                if (!String.IsNullOrEmpty(this.cmbMainDeviceType.Text))
                {
                    this.SelectedMainDeviceType = this.cmbMainDeviceType.Text;
                    this.cmbSubDeviceType.Text = String.Empty;
                    BindSubDevices();
                }
                else
                {
                    this.cmbSubDeviceType.Items.Clear();
                    this.cmbBrand.Items.Clear();
                    this.cmbSubDeviceType.Text = String.Empty;
                    this.cmbBrand.Text = String.Empty;
                }
            }
        }
        #endregion

        #region Bind Sub Devices
        void BindSubDevices()
        {
            this.cmbSubDeviceType.Items.Clear();
            if (this.EdIdDataTable != null)
            {
                DataRow[] drSubdevices = null;
                if (!String.IsNullOrEmpty(this.SelectedMainDeviceType))
                    drSubdevices = this.EdIdDataTable.Select("[Main Device]='" + this.SelectedMainDeviceType + "'");
                else
                    drSubdevices = this.EdIdDataTable.Select("Sub Device");
                if (drSubdevices.Length > 0)
                {
                    this.cmbSubDeviceType.Items.Add("");
                    foreach (DataRow item in drSubdevices)
                    {
                        if (!String.IsNullOrEmpty(item["Sub Device"].ToString()) && !this.cmbSubDeviceType.Items.Contains(item["Sub Device"].ToString()))
                            this.cmbSubDeviceType.Items.Add(item["Sub Device"].ToString());
                    }
                }
            }
        }
        void cmbSubDeviceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmbSubDeviceType.SelectedIndex != -1)
            {
                if (!String.IsNullOrEmpty(this.cmbSubDeviceType.Text))
                {
                    this.SelectedSubDeviceType = this.cmbSubDeviceType.Text;
                    this.cmbBrand.Text = String.Empty;
                    BindBrands();
                }
                else
                {
                    this.cmbBrand.Items.Clear();
                    this.cmbBrand.Text = String.Empty;
                }
            }
        }
        #endregion

        #region Bind Brands
        void BindBrands()
        {
            this.cmbBrand.Items.Clear();
            if (this.EdIdDataTable != null)
            {
                DataRow[] drBrands = null;
                if (!String.IsNullOrEmpty(this.SelectedMainDeviceType) && !String.IsNullOrEmpty(this.SelectedSubDeviceType))
                    drBrands = this.EdIdDataTable.Select("[Sub Device] = '" + this.SelectedSubDeviceType + "' and [Main Device] = '" + this.SelectedMainDeviceType + "'");
                else if (!String.IsNullOrEmpty(this.SelectedMainDeviceType))
                    drBrands = this.EdIdDataTable.Select("[Main Device] = '" + this.SelectedMainDeviceType + "'");
                if (!String.IsNullOrEmpty(this.SelectedSubDeviceType))
                    drBrands = this.EdIdDataTable.Select("[Sub Device] = '" + this.SelectedSubDeviceType + "'");
                else
                    drBrands = this.EdIdDataTable.Select("Brand");
                if (drBrands.Length > 0)
                {
                    this.cmbBrand.Items.Add("");
                    foreach (DataRow item in drBrands)
                    {
                        if (!String.IsNullOrEmpty(item["Brand"].ToString()) && !this.cmbBrand.Items.Contains(item["Brand"].ToString()))
                            this.cmbBrand.Items.Add(item["Brand"].ToString());
                    }
                }
            }
        }
        void cmbBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmbBrand.SelectedIndex != -1)
            {
                if (!String.IsNullOrEmpty(this.cmbBrand.Text))
                {
                    this.SelectedBrand = this.cmbBrand.Text;                    
                }
            }
        }
        #endregion

        #region OK
        
        Boolean ValidateOtherSelections()
        {
            ErrorMessage = String.Empty;
            Boolean isValid = false;
            this.SelectedMainDeviceType = String.Empty;
            this.SelectedSubDeviceType = String.Empty;
            this.SelectedBrand = String.Empty;
            if (!String.IsNullOrEmpty(cmbMainDeviceType.Text))
            {
                if (cmbMainDeviceType.Items.Contains(cmbMainDeviceType.Text))
                {
                    this.SelectedMainDeviceType = cmbMainDeviceType.Text;                    
                }
                else
                {
                    ErrorMessage = "Main Device doesn't Exists";                    
                }
            }
            if (!String.IsNullOrEmpty(cmbSubDeviceType.Text))
            {
                if (cmbSubDeviceType.Items.Contains(cmbSubDeviceType.Text))
                {
                    this.SelectedSubDeviceType = cmbSubDeviceType.Text;                           
                }
                else
                {
                    ErrorMessage = "Sub Device doesn't Exists";                   
                }
            }
            if (!String.IsNullOrEmpty(cmbBrand.Text))
            {
                if (cmbBrand.Items.Contains(cmbBrand.Text))
                {
                    this.SelectedBrand = cmbBrand.Text;                   
                }
                else
                {
                    ErrorMessage = "Brand doesn't Exists";                    
                }
            }
            isValid = (String.IsNullOrEmpty(ErrorMessage) == true) ? true : false; 
            return isValid;
        }
        void btnOK_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(this.txtBytePosition.Text.Trim()))
            {
                if (ValidateBytePosition())
                {
                    if (ValidateOtherSelections())
                    {
                        BytePositions = this.txtBytePosition.Text.Trim();

                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show(this.ErrorMessage,"Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Please enter valid Byte Position", "Invalid Byte Position", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtBytePosition.Focus();
                    return;
                }
            }
            else
            {
                MessageBox.Show("Byte Position Can't be Empty", "Invalid Byte Position", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private Boolean ValidateBytePosition()
        {
            Boolean isValid = false;
            this.BytePositions = String.Empty;
            this.BytePosforFingerPrint = new List<Int32>();
            String[] sep = { ",", ";", " ", "\t" };
            String[] data = this.txtBytePosition.Text.Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
            if (data.Length > 0)
            {
                for (Int32 i = 0; i < data.Length; i++)
                {
                    try
                    {
                        BytePosforFingerPrint.Add(Int32.Parse(data[i]));
                        isValid = true;
                    }
                    catch
                    {
                        isValid = false;
                        break;
                    }
                }
            }            
            return isValid;
        }
        #endregion
    }
}
