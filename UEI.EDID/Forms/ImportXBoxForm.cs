﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report.ExportToExcel;

namespace UEI.EDID.Forms
{
    public partial class ImportXBoxForm : Form
    {
        private enum ProcessType
        {
            UpdateEdId,
            GetEdId
        }

        #region Variables
        private ProgressForm m_Progress = null;
        private BackgroundWorker m_BackgroundWorker = null;
        private ProcessType m_ProcessType = ProcessType.GetEdId;
        #endregion

        #region Properties
        public List<XBox> EdIdData { get; set; }
        public DataTable XBoxInvalidEdIdDataTable { get; set; }
        #endregion

        #region Constructor
        public ImportXBoxForm()
        {
            InitializeComponent();
            this.Load += ImportXBoxForm_Load;
        }
        void ImportXBoxForm_Load(object sender, EventArgs e)
        {            
            this.btnUpdate.Enabled = false;
            this.m_BackgroundWorker = new BackgroundWorker();
            this.m_BackgroundWorker.DoWork += m_BackgroundWorker_DoWork;
            this.m_BackgroundWorker.RunWorkerCompleted += m_BackgroundWorker_RunWorkerCompleted;
            this.btnClose.Click += btnClose_Click;
            this.btnUpdate.Click += btnUpdate_Click;
            this.btnGetXBoxData.Click += btnGetXBoxData_Click;
            this.Timer.Tick += Timer_Tick;
        }
        #endregion

        #region Update XBox Edid Data
        void btnGetXBoxData_Click(object sender, EventArgs e)
        {
            m_ProcessType = ProcessType.GetEdId;
            ResetTimer();
        }
        private void GetXBoxEdidData()
        {
            try
            {
                EdIdServices m_GetEdId = new EdIdServices();
                this.EdIdData = m_GetEdId.GetXBoxEdIDData(ref m_StatusMessage, ref m_StatusMessage1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region Update XBox EdId Data
        void btnUpdate_Click(object sender, EventArgs e)
        {
            m_ProcessType = ProcessType.UpdateEdId;
            ResetTimer();
        }
        private DataSet dsDataSet = new DataSet("XBox EDID Summary");
        private void CreateDataTable()
        {
            if (this.XBoxInvalidEdIdDataTable == null)
            {
                this.XBoxInvalidEdIdDataTable = new DataTable("XBox");
                this.XBoxInvalidEdIdDataTable.Columns.Add("RID");
                this.XBoxInvalidEdIdDataTable.Columns.Add("Brand");
                this.XBoxInvalidEdIdDataTable.Columns.Add("EDID");
                this.XBoxInvalidEdIdDataTable.Columns.Add("EDID Length");
            }
            else
                this.XBoxInvalidEdIdDataTable.Clear();
        }
        private void ExportInvalidXBoxData()
        {
            if (XBoxInvalidEdIdDataTable != null)
            {
                //EdIdDataTable = SupressEmptyColumns(EdIdDataTable);
                String m_SaveFileMessage = String.Empty;
                if (dsDataSet.Tables.Contains(XBoxInvalidEdIdDataTable.TableName))
                    dsDataSet.Tables.Remove(XBoxInvalidEdIdDataTable.TableName);
                dsDataSet.Tables.Add(XBoxInvalidEdIdDataTable);
                try
                {                    
                    String m_filePath = CommonForms.Configuration.GetWorkingDirectory() + "\\";
                    if (String.IsNullOrEmpty(m_filePath))
                    {
                        FolderBrowserDialog dlg1 = new FolderBrowserDialog();
                        if (dlg1.ShowDialog() == DialogResult.OK)
                        {
                            if (dlg1.SelectedPath.EndsWith("\\") == false)
                            {
                                dlg1.SelectedPath += "\\";
                            }
                            m_filePath = dlg1.SelectedPath;
                        }
                    }
                    ExcelReportGenerator.GenerateReport(dsDataSet, m_filePath, "XBox EDID Summary", out m_SaveFileMessage);
                    if (!String.IsNullOrEmpty(m_SaveFileMessage))
                    {
                        MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                    }
                    MessageBox.Show("Invalid list of XBox EDID data report saved to your working directory.", "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        private void UpdateXBoxEdidData()
        {
            try
            {
                EdIdServices m_UpdateEdId = new EdIdServices();
                if (this.EdIdData != null)
                {
                    Int32 l_Count = 0;
                    foreach (XBox item in this.EdIdData)
                    {
                        m_StatusMessage1 = "Please Wait...";
                        l_Count++;
                        try
                        {
                            if (item.EdId.Count == 0)
                            {
                                this.StatusMessage = " Updating XBox EDID {" + l_Count.ToString() + "} of {" + this.EdIdData.Count.ToString() + "}";
                                this.StatusMessage1 = " Error in XBox EDID RID {" + item.RID.ToString() + "}";
                                if (this.XBoxInvalidEdIdDataTable == null)
                                    CreateDataTable();
                                DataRow dr = this.XBoxInvalidEdIdDataTable.NewRow();
                                dr[0] = item.RID;
                                dr[1] = item.Brand;
                                dr[2] = item.EdIdRawData;
                                dr[3] = item.EdId.Count;
                                this.XBoxInvalidEdIdDataTable.Rows.Add(dr);
                                this.XBoxInvalidEdIdDataTable.AcceptChanges();
                            }
                            else
                            {
                                this.StatusMessage = " Updating XBox EDID {" + l_Count.ToString() + "} of {" + this.EdIdData.Count.ToString() + "}";                                
                                this.StatusMessage1 = "Updating RID {" + item.RID.ToString() + "}";
                                
                                EDIDInfo edidInfo   = new EDIDInfo();
                                edidInfo.Brand = item.Brand;
                                edidInfo.EdIdRawData = item.EdIdRawData;
                                edidInfo.EdId = item.EdId;
                                edidInfo.OSDRawData = String.Empty;                              
                                Dictionary<String, String> m_Position = m_UpdateEdId.GetBytePositionBasedonBrands(item.MainDevice, item.Brand);
                                if (!String.IsNullOrEmpty(m_UpdateEdId.ErrorMessage))
                                {
                                    MessageBox.Show(m_UpdateEdId.ErrorMessage, "EDID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    break;
                                }
                                foreach (KeyValuePair<String, String> pair in m_Position)
                                {
                                    edidInfo.FPAvailable = pair.Key;
                                    edidInfo.FPBytePosition = pair.Value;
                                }
                               
                                try
                                {                                    
                                    if (edidInfo.FPAvailable == "Y")
                                    {
                                        m_UpdateEdId.ValidateBytePosition(edidInfo);
                                    }
                                    Byte[] byteData = m_UpdateEdId.CreateBinFile(edidInfo);
                                    EDIDInfo m_edid = m_UpdateEdId.ReadBinFile(byteData);
                                    if (String.IsNullOrEmpty(m_edid.CustomFP))
                                    m_edid.CustomFP = String.Empty;
                                    if (String.IsNullOrEmpty(m_edid.CustomFPOSD))
                                    m_edid.CustomFPOSD = String.Empty;
                                    if (String.IsNullOrEmpty(m_edid.Only128FP))
                                    m_edid.Only128FP = String.Empty;
                                    if (String.IsNullOrEmpty(m_edid.Only128FPOSD))
                                    m_edid.Only128FPOSD = String.Empty;
                                    if (String.IsNullOrEmpty(m_edid.OSDRawData))
                                    m_edid.OSDRawData = String.Empty;
                                    if (String.IsNullOrEmpty(m_edid.OnlyOSDFP))
                                    m_edid.OnlyOSDFP = String.Empty;
                                    if (String.IsNullOrEmpty(m_edid.FPBytePosition))
                                    m_edid.FPBytePosition = String.Empty;
                                    if (String.IsNullOrEmpty(m_edid.CustomFPType0x00))
                                        m_edid.CustomFPType0x00 = String.Empty;
                                    if (String.IsNullOrEmpty(m_edid.CustomFPType0x01))
                                        m_edid.CustomFPType0x01 = String.Empty;
                                    if (String.IsNullOrEmpty(m_edid.CustomFPType0x02))
                                        m_edid.CustomFPType0x02 = String.Empty;
                                    if (String.IsNullOrEmpty(m_edid.CustomFPType0x03))
                                        m_edid.CustomFPType0x03 = String.Empty;
                                    if (String.IsNullOrEmpty(m_edid.PhysicalAddress))
                                        m_edid.PhysicalAddress = String.Empty;

                                    String status = m_UpdateEdId.UpdateEdIdXBoxData(item.RID, byteData, m_edid.CustomFP, m_edid.CustomFPOSD, m_edid.Only128FP, m_edid.Only128FPOSD, m_edid.OSDRawData, m_edid.OnlyOSDFP, m_edid.FPBytePosition, m_edid.CustomFPType0x00, m_edid.CustomFPType0x01, m_edid.CustomFPType0x02, m_edid.CustomFPType0x03, m_edid.PhysicalAddress);
                                    //String status = m_UpdateEdId.UpdateEdIdXBoxData(item.RID, byteData, fingerPrint);
                                    if (!String.IsNullOrEmpty(status))
                                        item.Status = Int32.Parse(status);
                                }
                                catch
                                {
                                    this.StatusMessage = " Updating XBox EDID {" + l_Count.ToString() + "} of {" + this.EdIdData.Count.ToString() + "}";
                                    this.StatusMessage1 = " Error in XBox EDID RID {" + item.RID.ToString() + "}";
                                    if (this.XBoxInvalidEdIdDataTable == null)
                                        CreateDataTable();
                                    DataRow dr = this.XBoxInvalidEdIdDataTable.NewRow();
                                    dr[0] = item.RID;
                                    dr[1] = item.Brand;
                                    dr[2] = item.EdIdRawData;
                                    if (item.EdId != null)
                                        dr[3] = item.EdId.Count;
                                    else
                                        dr[3] = 0;
                                    this.XBoxInvalidEdIdDataTable.Rows.Add(dr);
                                    this.XBoxInvalidEdIdDataTable.AcceptChanges();
                                }
                            }
                        }
                        catch 
                        {
                            this.StatusMessage = " Updating XBox EDID {" + l_Count.ToString() + "} of {" + this.EdIdData.Count.ToString() + "}";
                            this.StatusMessage1 = " Error in XBox EDID RID {" + item.RID.ToString() + "}";
                            if (this.XBoxInvalidEdIdDataTable == null)
                                CreateDataTable();
                            DataRow dr = this.XBoxInvalidEdIdDataTable.NewRow();
                            dr[0] = item.RID;
                            dr[1] = item.Brand;
                            dr[2] = item.EdIdRawData;
                            if (item.EdId != null)
                                dr[3] = item.EdId.Count;
                            else
                                dr[3] = 0;
                            this.XBoxInvalidEdIdDataTable.Rows.Add(dr);
                            this.XBoxInvalidEdIdDataTable.AcceptChanges();
                        }
                    }
                    if (this.XBoxInvalidEdIdDataTable != null)
                    {
                        ExportInvalidXBoxData();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }       
        #endregion

        #region Close
        void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region Background Progress
        private String m_StatusMessage = "Please Wait...";
        public String StatusMessage
        {
            get { return m_StatusMessage; }
            set { m_StatusMessage = value; }
        }
        private String m_StatusMessage1 = "Please Wait...";
        public String StatusMessage1
        {
            get { return m_StatusMessage1; }
            set { m_StatusMessage1 = value; }
        }
        private void ResetTimer()
        {
            if (m_BackgroundWorker != null || m_BackgroundWorker.IsBusy == false)
                m_BackgroundWorker.RunWorkerAsync();

            if (m_Progress == null)
                m_Progress = new ProgressForm();

            m_Progress.SetMessage(this.StatusMessage, "Please Wait...");
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();            
            Timer.Start();
        }
        void Timer_Tick(object sender, EventArgs e)
        {
            ProgressMessage();
        }
        private void ProgressMessage()
        {
            if (m_Progress != null)
                m_Progress.SetMessage(this.StatusMessage, this.StatusMessage1);
        }
        void m_BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {            
            Timer.Stop();
            m_Progress.DialogResult = DialogResult.OK;
            m_Progress.Close();

            switch (m_ProcessType)
            {
                case ProcessType.UpdateEdId:
                    this.dataGridView.DataSource = null;
                    if (this.EdIdData != null)
                    {
                        this.dataGridView.DataSource = this.EdIdData;
                        this.btnUpdate.Enabled = true;
                    }
                    break;
                case ProcessType.GetEdId:
                    this.dataGridView.DataSource = null;
                    if (this.EdIdData != null)
                    {
                        this.dataGridView.DataSource = this.EdIdData;
                        this.btnUpdate.Enabled = true;
                    }
                    else
                    {
                        this.btnUpdate.Enabled = false;
                        MessageBox.Show("No Data Found", "XBox", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
            }

        }
        void m_BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (m_ProcessType)
            {
                case ProcessType.UpdateEdId:
                    this.StatusMessage = "Updating XBox EDID Data";
                    UpdateXBoxEdidData();
                    break;
                case ProcessType.GetEdId:
                    this.StatusMessage = "Loading XBox EDID Data";
                    GetXBoxEdidData();
                    break;
            }
        }
        #endregion
    }
}