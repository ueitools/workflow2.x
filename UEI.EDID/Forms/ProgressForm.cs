﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.EDID.Forms
{
    public partial class ProgressForm : Form
    {
        private String m_Message1 = String.Empty;
        public String Message1
        {
            get { return m_Message1; }
            set
            {
                lblMsg1.Text = value;
                m_Message1 = value;
            }
        }
        private String m_Message2 = String.Empty;
        public String Message2
        {
            get { return m_Message2; }
            set
            {
                lblMsg2.Text = value;
                m_Message2 = value;
            }
        }

        public ProgressForm()
        {
            InitializeComponent();
        }
        internal void SetMessage(string p)
        {            
            Message1 = p;
            Message2 = String.Empty;
        }
        internal void SetMessage(String msg1, String msg2)
        {
            Message1 = msg1;
            Message2 = msg2;
        }
        private void Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}