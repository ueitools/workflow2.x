﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report.ExportToExcel;

namespace UEI.EDID.Forms
{
    public partial class XBoxGroupingForm : Form
    {
        #region Variables
        private DataSet objDataSet = null;
        private EdIdServices m_Service = null;
        private XBoxReportFilterForm m_ReportFilterForm = null;
        private ProgressForm m_Progress = null;
        private Timer m_Timer = null;
        private BackgroundWorker m_BackgroundWorker = null;
        #endregion

        #region Properties
        private String m_StatusMessage = "Please Wait...";
        public String StatusMessage
        {
            get { return m_StatusMessage; }
            set { m_StatusMessage = value; }
        }
        private String m_StatusMessage1 = String.Empty;
        public String StatusMessage1
        {
            get { return m_StatusMessage1; }
            set { m_StatusMessage1 = value; }
        }
        public List<EDIDGroupData>  EdIdData { get; set; }
        public EDIDGroup            EdIdGrouped { get; set; }
        public Dictionary<String, EDIDGroup> ClusteredEdIdGroups { get; set; }
        public List<String>         SelectedDeviceType { get; set; }
        public List<String>         SelectedBrand { get; set; }
        public List<String>         SelectedCountries { get; set; }
        public List<String>         SelectedDataSources { get; set; }
        public List<String>         SelectedModels { get; set; }
        public FPIDLinkCollection   LinkedIds { get; set; }
        public String               SelectedEdIdExcelFile { get; set; }
        public Boolean              NewFPRule { get; set; }
        public FingerPrintTypes     FpType { get; set; }
        #endregion

        #region Constructor
        public XBoxGroupingForm()
        {
            InitializeComponent();
            this.m_BackgroundWorker = new BackgroundWorker();
            this.m_BackgroundWorker.DoWork += m_BackgroundWorker_DoWork;
            this.m_BackgroundWorker.RunWorkerCompleted += m_BackgroundWorker_RunWorkerCompleted;
            this.m_Timer = new System.Windows.Forms.Timer();
            this.m_Timer.Enabled = true;
            this.m_Timer.Interval = 100;
            this.m_Timer.Tick += m_Timer_Tick;
            this.m_Service = new EdIdServices(DBConnectionString.EdId);            
            this.Load += XBoxGroupingForm_Load;
            this.btnExcel.Click += btnExcel_Click;
        }
        void btnExcel_Click(object sender, EventArgs e)
        {
            if (this.objDataSet != null)
            {
                Export();                
            }
        }
        void XBoxGroupingForm_Load(object sender, EventArgs e)
        {
            this.objDataSet = null;
            this.m_ReportFilterForm = new XBoxReportFilterForm();
            this.m_ReportFilterForm.ReportName = "XBoxCluterReport";
            this.m_ReportFilterForm.ShowDialog();
            if (this.m_ReportFilterForm.DialogResult == DialogResult.OK)
            {
                this.SelectedDeviceType     = this.m_ReportFilterForm.SelectedDeviceType;
                this.SelectedBrand          = this.m_ReportFilterForm.SelectedBrand;
                this.SelectedCountries      = this.m_ReportFilterForm.SelectedCountries;
                this.SelectedDataSources    = this.m_ReportFilterForm.SelectedDataSources;
                this.SelectedModels         = this.m_ReportFilterForm.SelectedModel;
                this.SelectedEdIdExcelFile  = this.m_ReportFilterForm.SelectedEdIdExcelFile;
                this.NewFPRule              = this.m_ReportFilterForm.NewFPRule;
                this.FpType                 = this.m_ReportFilterForm.FPType;
                this.m_ReportFilterForm     = null;
                ResetTimer();
                if (this.objDataSet != null)
                {
                    this.btnExcel.Enabled = true;
                    this.dataGridView.DataSource = this.objDataSet.Tables[0];
                    foreach (DataGridViewColumn column in this.dataGridView.Columns)
                    {
                        column.SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
        }
        #endregion

        #region Export To Excel
        void ExportToExcel()
        {
            String[] sep1 = { "," };
            if (this.ClusteredEdIdGroups != null)
            {
                if (this.ClusteredEdIdGroups.Count > 0)
                {
                    DataTable dtResult = new DataTable("Grouped EDID");
                    dtResult.Columns.Add("Group");
                    dtResult.Columns.Add("Sub Group");
                    dtResult.Columns.Add("EDID Block");
                    if (this.NewFPRule)
                    {
                        dtResult.Columns.Add("Custom FP Type0x00");
                        dtResult.Columns.Add("Custom FP Type0x01");
                        dtResult.Columns.Add("Custom FP Type0x02");
                        dtResult.Columns.Add("Custom FP Type0x03");
                    }
                    //dtResult.Columns.Add("Custom FP");
                    //dtResult.Columns.Add("128 FP");
                    dtResult.Columns.Add("Main Device");
                    dtResult.Columns.Add("Sub Device");
                    dtResult.Columns.Add("Region");
                    dtResult.Columns.Add("Brand");
                    dtResult.Columns.Add("User Entered Model");
                    dtResult.Columns.Add("User Entered Codeset");
                    dtResult.Columns.Add("Manufacturer (3 char) Code");
                    dtResult.Columns.Add("Monitor Name");
                    dtResult.Columns.Add("Product Code");
                    dtResult.Columns.Add("Year");
                    dtResult.Columns.Add("Week");
                    dtResult.Columns.Add("Horizontal Screen Size");
                    dtResult.Columns.Add("Vertical Screen Size");                    
                    dtResult.Columns.Add("Console Count", Type.GetType("System.Int64"));
                    dtResult.Columns.Add("Suggested ID");                    
                    dtResult.Columns.Add("DAC Published ID");                    
                    dtResult.Columns.Add("Build Rule");
                    dtResult.Columns.Add("CEC Physical Address");
                    dtResult.Columns.Add("CPR Rule");
                    dtResult.Columns.Add("Discrete Profile");
                    dtResult.Columns.Add("Discrete ID");
                    dtResult.Columns.Add("Trust Level");

                    dtResult.AcceptChanges();

                    //Generate Report for all Cluster Groups
                    if (this.ClusteredEdIdGroups != null)
                    {
                        foreach (String clusteredGroup in this.ClusteredEdIdGroups.Keys)
                        {
                            Int32 intRowCount = 1;
                            foreach (String itemKey in this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups.Keys)
                            {
                                //if (this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups[itemKey] != null && itemKey.Contains("PCGroup_"))
                                if (this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups[itemKey] != null)
                                {
                                    this.m_StatusMessage = "Generating group report " + intRowCount.ToString() + " of " + this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups.Keys.Count.ToString();
                                    intRowCount = intRowCount + 1;
                                    FillDataTableForReport(ref dtResult, itemKey, clusteredGroup);
                                }
                            }
                        }
                    }
                    if (objDataSet == null)
                    {
                        objDataSet = new DataSet("Grouped EDID Summary");
                    }
                    if (objDataSet.Tables.Count > 0)
                    {
                        if (objDataSet.Tables.Contains(dtResult.TableName))
                            objDataSet.Tables.Remove(dtResult.TableName);
                    }
                    objDataSet.Tables.Add(dtResult);
                }
            }
        }
        void FillDataTableForReport(ref DataTable dtResult, String itemKey, String clusteredGroup)
        {
            if (this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups[itemKey] != null)
            {
                this.m_StatusMessage1 = "Generating report for Group: " + itemKey;
                DataRow drGroup = null;
                drGroup = dtResult.NewRow();
                drGroup["Group"] = itemKey;
                drGroup["Sub Group"] = String.Empty;
                drGroup["EDID Block"] = String.Empty;
                if (this.NewFPRule)
                {
                    drGroup["Custom FP Type0x00"] = String.Empty;
                    drGroup["Custom FP Type0x01"] = String.Empty;
                    drGroup["Custom FP Type0x02"] = String.Empty;
                    drGroup["Custom FP Type0x03"] = String.Empty;
                }
                //drGroup["Custom FP"] = String.Empty;
                //drGroup["128 FP"] = String.Empty;
                drGroup["Main Device"] = String.Empty;
                drGroup["Sub Device"] = String.Empty;
                drGroup["Region"] = String.Empty;
                drGroup["Brand"] = String.Empty;
                drGroup["User Entered Model"] = String.Empty;
                drGroup["User Entered Codeset"] = String.Empty;
                drGroup["Product Code"] = String.Empty;
                drGroup["Year"] = String.Empty;
                drGroup["Week"] = String.Empty;
                drGroup["Suggested ID"] = String.Empty;
                drGroup["DAC Published ID"] = String.Empty;
                drGroup["Trust Level"] = String.Empty;
                drGroup["Build Rule"] = String.Empty;
                drGroup["Manufacturer (3 char) Code"] = String.Empty;
                drGroup["Monitor Name"] = String.Empty;
                drGroup["Horizontal Screen Size"] = String.Empty;
                drGroup["Vertical Screen Size"] = String.Empty;
                dtResult.Rows.Add(drGroup);

                Int64 totalConsoleCount = 0;
                Int64 intSubGroupCount = 1;
                foreach (String groupItem in this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups[itemKey].Keys)
                {
                    try
                    {
                        if (this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups[itemKey][groupItem] != null)
                        {
                            if (this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups[itemKey][groupItem].Count > 0)
                            {
                                Int64 totalFPConsoleCount = 0;
                                String strSubGroupName = itemKey + "." + intSubGroupCount.ToString();
                                intSubGroupCount = intSubGroupCount + 1;
                                DataRow drSubGroup = null;
                                drSubGroup = dtResult.NewRow();
                                drSubGroup["Group"] = String.Empty;
                                drSubGroup["Sub Group"] = strSubGroupName;
                                if (this.NewFPRule)
                                {
                                    drSubGroup["Custom FP Type0x00"] = String.Empty;
                                    drSubGroup["Custom FP Type0x01"] = String.Empty;
                                    drSubGroup["Custom FP Type0x02"] = String.Empty;
                                    drSubGroup["Custom FP Type0x03"] = String.Empty;
                                }
                                //drSubGroup["Custom FP"] = String.Empty;
                                //drSubGroup["128 FP"] = String.Empty;
                                drSubGroup["Main Device"] = String.Empty;
                                drSubGroup["Sub Device"] = String.Empty;
                                drSubGroup["Region"] = String.Empty;

                                drSubGroup["Brand"] = String.Empty;
                                drSubGroup["EDID Block"] = String.Empty;
                                drSubGroup["User Entered Model"] = String.Empty;
                                drSubGroup["User Entered Codeset"] = String.Empty;
                                drSubGroup["Product Code"] = String.Empty;
                                drSubGroup["Year"] = String.Empty;
                                drSubGroup["Week"] = String.Empty;
                                drSubGroup["Suggested ID"] = String.Empty;
                                drSubGroup["DAC Published ID"] = String.Empty;
                                drSubGroup["Trust Level"] = String.Empty;
                                drSubGroup["Build Rule"] = String.Empty;
                                drSubGroup["Manufacturer (3 char) Code"] = String.Empty;
                                drSubGroup["Monitor Name"] = String.Empty;
                                drSubGroup["Horizontal Screen Size"] = String.Empty;
                                drSubGroup["Vertical Screen Size"] = String.Empty;
                                drSubGroup["Discrete Profile"] = String.Empty;
                                drSubGroup["Discrete ID"] = String.Empty;
                                dtResult.Rows.Add(drSubGroup);
                                foreach (EDIDGroupData itemEdId in this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups[itemKey][groupItem])
                                {
                                    try
                                    {
                                        DataRow dr1 = null;
                                        dr1 = dtResult.NewRow();
                                        dr1["Group"] = String.Empty;
                                        if (this.NewFPRule)
                                        {
                                            dr1["Custom FP Type0x00"] = itemEdId.CustomFPType0x00;
                                            dr1["Custom FP Type0x01"] = itemEdId.CustomFPType0x01;
                                            dr1["Custom FP Type0x02"] = itemEdId.CustomFPType0x02;
                                            dr1["Custom FP Type0x03"] = itemEdId.CustomFPType0x03;
                                        }
                                        dr1["CEC Physical Address"] = itemEdId.PhysicalAddress;
                                        //dr1["Custom FP"] = itemEdId.CustomFP;
                                        dr1["EDID Block"] = itemEdId.EDIDBlock;
                                        //dr1["128 FP"] = itemEdId.Only128FP;
                                        dr1["Main Device"] = itemEdId.MainDevice;
                                        dr1["Sub Device"] = String.Empty;
                                        dr1["Region"] = itemEdId.Region;
                                        dr1["Brand"] = itemEdId.Brand;
                                        dr1["User Entered Model"] = itemEdId.UserEnteredModel;
                                        dr1["User Entered Codeset"] = itemEdId.UserEnteredCodeset;
                                        dr1["Product Code"] = itemEdId.ProductCode;
                                        dr1["Year"] = itemEdId.ModelYear;
                                        dr1["Week"] = itemEdId.ModelWeek;
                                        dr1["Console Count"] = itemEdId.CounsoleCount;
                                        dr1["Suggested ID"] = itemEdId.SuggestedID;
                                        dr1["DAC Published ID"] = itemEdId.DACPublishedID;
                                        dr1["Trust Level"] = itemEdId.TrustLevel;
                                        dr1["Build Rule"] = itemEdId.BuildRule;
                                        dr1["Manufacturer (3 char) Code"] = itemEdId.ManufacturerName;
                                        dr1["Monitor Name"] = itemEdId.MonitorName;
                                        dr1["Horizontal Screen Size"] = itemEdId.HorizontalScreenSize;
                                        dr1["Vertical Screen Size"] = itemEdId.VerticalScreenSize;
                                        dr1["CPR Rule"] = itemEdId.CPRRule;
                                        dr1["Discrete Profile"] = itemEdId.DiscreteProfile;
                                        dr1["Discrete ID"] = itemEdId.DiscreteID;
                                        dtResult.Rows.Add(dr1);
                                        totalConsoleCount += itemEdId.CounsoleCount;
                                        totalFPConsoleCount += itemEdId.CounsoleCount;
                                        dr1 = null;
                                    }
                                    catch (Exception ex1)
                                    {
                                        String s1 = ex1.Message;
                                    }
                                }
                                
                                //if (totalFPConsoleCount == 0)
                                //    drSubGroup["Console Count"] = 0;
                                //else
                                //    drSubGroup["Console Count"] = totalFPConsoleCount;
                                
                                drSubGroup = null;
                            }
                        }
                    }
                    catch (Exception ex2)
                    {
                        String s2 = ex2.Message;
                    }

                }
                
                //if (totalConsoleCount == 0)
                //    drGroup["Console Count"] = 0;
                //else
                //    drGroup["Console Count"] = totalConsoleCount;
                
                drGroup = null;
            }
            dtResult.AcceptChanges();
        }
        void Export()
        {
            String m_SaveFileMessage = String.Empty;
            try
            {
                String m_filePath = CommonForms.Configuration.GetWorkingDirectory() + "\\";
                if (String.IsNullOrEmpty(m_filePath))
                {
                    FolderBrowserDialog dlg1 = new FolderBrowserDialog();
                    if (dlg1.ShowDialog() == DialogResult.OK)
                    {
                        if (dlg1.SelectedPath.EndsWith("\\") == false)
                        {
                            dlg1.SelectedPath += "\\";
                        }
                        m_filePath = dlg1.SelectedPath;
                    }
                }

                ExcelReportGenerator.GenerateReport(objDataSet, m_filePath, "Grouped EDID Summary", out m_SaveFileMessage);
                if (!String.IsNullOrEmpty(m_SaveFileMessage))
                {
                    MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        #endregion

        #region Load Linked ID
        void LoadLinkedID()
        {
            try
            {
                this.m_StatusMessage = "Please Wait...";
                this.m_StatusMessage1 = "Loading Linked Id from direct capture edid";
                this.LinkedIds = this.m_Service.LoadLinkedID();
            }
            catch { }
        }
        #endregion

        #region Progress
        private void ResetTimer()
        {
            if (m_BackgroundWorker != null || m_BackgroundWorker.IsBusy == false)
                m_BackgroundWorker.RunWorkerAsync();

            if (m_Progress == null)
                m_Progress = new ProgressForm();
            m_Progress.SetMessage(StatusMessage, StatusMessage1);
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();
            m_Timer.Start();
        }
        void m_Timer_Tick(object sender, EventArgs e)
        {
            if (this.m_Progress != null)
            {
                this.m_Progress.SetMessage(StatusMessage, StatusMessage1);
            }
        }
        void m_BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.m_Timer.Stop();
            this.m_Progress.DialogResult = DialogResult.OK;
            this.m_Progress.Close();

            if (m_Service.ErrorMessage != String.Empty)
            {
                MessageBox.Show(m_Service.ErrorMessage, "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                ExportToExcel();
            }
        }
        void m_BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!String.IsNullOrEmpty(this.SelectedEdIdExcelFile))
                LoadEdIdFromExcelFile();
            else
            {
                this.EdIdData = this.m_Service.GetXBoxEDIDClusterReport(this.SelectedDeviceType, this.SelectedBrand, this.SelectedModels, this.SelectedDataSources, this.SelectedCountries, ref this.m_StatusMessage1);

                //if (this.NewFPRule)
                //    this.EdIdData = this.m_Service.GetEDIDGroupDataWithConsoleCountNewFPRule(this.SelectedDeviceType, this.SelectedBrand, this.SelectedModels, this.SelectedDataSources, this.SelectedCountries, ref this.m_StatusMessage1);
                //else
                //    this.EdIdData = this.m_Service.GetEDIDGroupDataWithConsoleCount(this.SelectedDeviceType, this.SelectedBrand, this.SelectedModels, this.SelectedDataSources, this.SelectedCountries, ref this.m_StatusMessage1);
            }
            if (this.EdIdData != null)
            {
                if (this.EdIdData.Count > 0)
                    this.EdIdGrouped = this.m_Service.GetGroupEdId(this.EdIdData, ref this.m_StatusMessage, ref  this.m_StatusMessage1);
                try
                {
                    LoadLinkedID();
                }
                catch { }
                m_Service = new EdIdServices(DBConnectionString.UEIPUBLIC);
                if (this.EdIdGrouped != null)
                    this.ClusteredEdIdGroups = this.m_Service.SetLinkedIdInGroupedEdId(this.EdIdGrouped, this.LinkedIds,this.FpType, ref this.m_StatusMessage, ref  this.m_StatusMessage1);
                if (this.ClusteredEdIdGroups != null)
                    this.ClusteredEdIdGroups = this.m_Service.SetMappedIdInGroupedEdId(this.ClusteredEdIdGroups, ref this.m_StatusMessage, ref  this.m_StatusMessage1);
                else
                    this.m_Service.ErrorMessage = "No Data Found";
            }
        }
        #endregion

        #region Load EDID from Excel File
        void LoadEdIdFromExcelFile()
        {
            this.m_Service.ErrorMessage = String.Empty;
            if (!String.IsNullOrEmpty(this.SelectedEdIdExcelFile) && File.Exists(this.SelectedEdIdExcelFile))
            {
                String[] sep = { " " };
                String[] sep1 = { "," };
                try
                {
                    FileInfo m_File = new FileInfo(this.SelectedEdIdExcelFile);
                    if (m_File.Extension.ToLower() == ".xls" || m_File.Extension.ToLower() == ".xlsx")
                    {
                        OleDbConnection m_ExcelConnection = null;
                        if (m_File.Extension.ToLower() == ".xls")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + this.SelectedEdIdExcelFile + ";Extended Properties=Excel 8.0;");
                        else if (m_File.Extension.ToLower() == ".xlsx")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.SelectedEdIdExcelFile + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");

                        m_ExcelConnection.Open();
                        DataTable dtExcelSheets = m_ExcelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                        m_ExcelConnection.Close();
                        if (dtExcelSheets != null)
                        {
                            foreach (DataRow drExcelSheet in dtExcelSheets.Rows)
                            {
                                OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [" + drExcelSheet["TABLE_NAME"].ToString() + "]", m_ExcelConnection);
                                DataTable dtTable = new DataTable();
                                m_ExcelAdapter.Fill(dtTable);
                                if (dtTable.Rows.Count > 0)
                                {
                                    this.EdIdData = new List<EDIDGroupData>();
                                    Int32 intSLNo = 1;
                                    foreach (DataRow item in dtTable.Rows)
                                    {
                                        try
                                        {
                                            EDIDGroupData newedidgroupdata = new EDIDGroupData();
                                            if (!CheckEmptyRow(item))
                                            {
                                                newedidgroupdata.SlNo = intSLNo;
                                                if (dtTable.Columns.Contains("Brand"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Brand"].ToString()))
                                                    {
                                                        newedidgroupdata.Brand = item["Brand"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Custom FP"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Custom FP"].ToString()))
                                                    {
                                                        newedidgroupdata.CustomFP = item["Custom FP"].ToString().Trim();
                                                    }
                                                }

                                                if (dtTable.Columns.Contains("Custom FP Type0x00"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Custom FP Type0x00"].ToString()))
                                                    {
                                                        newedidgroupdata.CustomFPType0x00 = item["Custom FP Type0x00"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Custom FP Type0x01"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Custom FP Type0x01"].ToString()))
                                                    {
                                                        newedidgroupdata.CustomFPType0x01 = item["Custom FP Type0x01"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Custom FP Type0x02"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Custom FP Type0x02"].ToString()))
                                                    {
                                                        newedidgroupdata.CustomFPType0x02 = item["Custom FP Type0x02"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Custom FP Type0x03"))
                                                {                                                    
                                                    if (!String.IsNullOrEmpty(item["Custom FP Type0x03"].ToString()))
                                                    {
                                                        newedidgroupdata.CustomFPType0x03 = item["Custom FP Type0x03"].ToString().Trim();
                                                    }
                                                }

                                                if (dtTable.Columns.Contains("EDID Block"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["EDID Block"].ToString()))
                                                    {
                                                        newedidgroupdata.EDIDBlock = item["EDID Block"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Horizontal Screen Size"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Horizontal Screen Size"].ToString()))
                                                    {
                                                        newedidgroupdata.HorizontalScreenSize = Int32.Parse(item["Horizontal Screen Size"].ToString().Trim());
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Vertical Screen Size"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Vertical Screen Size"].ToString()))
                                                    {
                                                        newedidgroupdata.VerticalScreenSize = Int32.Parse(item["Vertical Screen Size"].ToString().Trim());
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Manufacturer (3 char) code"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Manufacturer (3 char) code"].ToString()))
                                                    {
                                                        newedidgroupdata.ManufacturerName = item["Manufacturer (3 char) code"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Week"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Week"].ToString()))
                                                    {
                                                        newedidgroupdata.ModelWeek = Int32.Parse(item["Week"].ToString().Trim());
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Year"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Year"].ToString()))
                                                    {
                                                        newedidgroupdata.ModelYear = Int32.Parse(item["Year"].ToString().Trim());
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Monitor Name"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Monitor Name"].ToString()))
                                                    {
                                                        newedidgroupdata.MonitorName = item["Monitor Name"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("128 FP"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["128 FP"].ToString()))
                                                    {
                                                        newedidgroupdata.Only128FP = item["128 FP"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Product Code"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Product Code"].ToString()))
                                                    {
                                                        newedidgroupdata.ProductCode = item["Product Code"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Console Count"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Console Count"].ToString()))
                                                    {
                                                        newedidgroupdata.CounsoleCount = Int64.Parse(item["Console Count"].ToString().Trim());
                                                    }
                                                }
                                                //newedidgroupdata.CounsoleCount = 101;
                                                this.EdIdData.Add(newedidgroupdata);
                                                newedidgroupdata = null;
                                                intSLNo++;
                                            }
                                        }
                                        catch { }
                                    }
                                }
                            }                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        Boolean CheckEmptyRow(DataRow m_Row)
        {
            String valuesarr = String.Empty;
            try
            {
                foreach (Object item in m_Row.ItemArray)
                {
                    if (item != null)
                        valuesarr += item.ToString();
                }
            }
            catch { valuesarr = String.Empty; }
            if (String.IsNullOrEmpty(valuesarr))
                return true;
            else
                return false;
        } 
        #endregion
    }
}