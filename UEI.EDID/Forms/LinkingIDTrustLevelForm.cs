﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.EDID.Forms
{
    public partial class LinkingIDTrustLevelForm : Form
    {
        #region Variables
        #endregion

        #region Properties
        public EDIDInfo SelectedEDID { get; set; }
        #endregion

        #region Constructor
        public LinkingIDTrustLevelForm()
        {
            InitializeComponent();
            this.Load += LinkingIDTrustLevelForm_Load;
            this.btnCancel.Click += btnCancel_Click;
            this.btnOK.Click += btnOK_Click;
        }       
        void LinkingIDTrustLevelForm_Load(object sender, EventArgs e)
        {
            if (this.SelectedEDID != null)
            {
                this.txtMainDevice.Text = this.SelectedEDID.MainDevice;
                this.txtSubDevice.Text = this.SelectedEDID.SubDevice;
                this.txtBrand.Text = this.SelectedEDID.Brand;
                this.txtID.Text = this.SelectedEDID.Codeset;
                switch (this.SelectedEDID.DataSource)
                {
                    case "Capture EDID":
                        this.txtTrustLevel.Text = "100";
                        this.txtTrustLevel.ReadOnly = true;                        
                        break;
                    case "Telemetry EDID":
                        this.txtTrustLevel.Text = "100";                                              
                        break;
                }             
            }
        }
        #endregion

        #region Events
        void btnOK_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(this.txtID.Text.Trim()))
            {
                MessageBox.Show("Please entered Valid ID", "Invalid ID", MessageBoxButtons.OK, MessageBoxIcon.Error);                
            }
            if(!String.IsNullOrEmpty(this.txtID.Text.Trim()))
            {
                String strID = String.Empty;
                IList<String> allModes = new List<String>();
                Char chr = 'A';
                for (Int32 item = 0; item < 26; item++)
                {
                    allModes.Add(chr.ToString());
                    chr++;
                }
                int IDNumber;
                if (this.txtID.Text.Trim().Length == 5 && allModes.Contains(this.txtID.Text.Trim().ToUpper().Substring(0, 1)) && (int.TryParse(this.txtID.Text.Trim().ToUpper().Substring(1), out IDNumber)))
                {
                    strID = this.txtID.Text.Trim().ToUpper();
                }
                if (String.IsNullOrEmpty(strID))
                {
                    MessageBox.Show("Please entered Valid ID", "Invalid ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    this.SelectedEDID.DACPublishedID = this.txtID.Text.Trim().ToUpper();
                    if ((this.SelectedEDID.DataSource == "Telemetry EDID") || (this.SelectedEDID.DataSource == "TelemetryData"))
                    {
                        Int32 m_Result = 0;
                        if (String.IsNullOrEmpty(this.txtTrustLevel.Text.Trim()))
                        {
                            MessageBox.Show("Please entered Trust Level", "Invalid Trust Level", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else if (!Int32.TryParse(this.txtTrustLevel.Text.Trim(), out m_Result) || (m_Result < 0 || m_Result > 100))
                        {
                            MessageBox.Show("Trust Level should be an integer value between 0 and 100", "Invalid Trust Level", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            this.SelectedEDID.TrustLevel = Int32.Parse(this.txtTrustLevel.Text.Trim());
                            this.DialogResult = DialogResult.OK;
                            this.Close();
                        }
                    }
                    else
                    {
                        this.SelectedEDID.TrustLevel = Int32.Parse(this.txtTrustLevel.Text.Trim());
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                }
            }
           
        }
        void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region Methods        
        #endregion
    }
}