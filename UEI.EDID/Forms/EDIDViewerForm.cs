﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report.ExportToExcel;

namespace UEI.EDID.Forms
{
    public partial class EDIDViewerForm : Form
    {
        #region Variables
        private DecodedEDIDData EDIDDetails = null;
        private DataSet objDataSet = null;
        private EdIdServices m_Service = null;
        private XBoxReportFilterForm m_ReportFilterForm = null;
        private ProgressForm m_Progress = null;
        private Timer m_Timer = null;
        private BackgroundWorker m_BackgroundWorker = null;
        #endregion

        #region Properties
        private String m_StatusMessage = "Please Wait...";
        public String StatusMessage
        {
            get { return m_StatusMessage; }
            set { m_StatusMessage = value; }
        }
        private String m_StatusMessage1 = String.Empty;
        public String StatusMessage1
        {
            get { return m_StatusMessage1; }
            set { m_StatusMessage1 = value; }
        }
        public List<EDIDGroupData> EdIdData { get; set; }
        public EDIDGroup EdIdGrouped { get; set; }
        public Dictionary<String, EDIDGroup> ClusteredEdIdGroups { get; set; }
        public List<String> SelectedDeviceType { get; set; }
        public FingerPrintTypes FPType { get; set; }
        public List<String> SelectedBrand { get; set; }
        public List<String> SelectedCountries { get; set; }
        public List<String> SelectedDataSources { get; set; }
        public List<String> SelectedModels { get; set; }
        public FPIDLinkCollection LinkedIds { get; set; }
        public String SelectedEdIdExcelFile { get; set; }
        #endregion

        #region Constructor
        public EDIDViewerForm()
        {
            InitializeComponent();
            this.m_Service = new EdIdServices();
            this.m_BackgroundWorker = new BackgroundWorker();
            this.m_BackgroundWorker.DoWork += m_BackgroundWorker_DoWork;
            this.m_BackgroundWorker.RunWorkerCompleted += m_BackgroundWorker_RunWorkerCompleted;
            this.m_Timer = new System.Windows.Forms.Timer();
            this.m_Timer.Enabled = true;
            this.m_Timer.Interval = 100;
            this.m_Timer.Tick += m_Timer_Tick;
            this.Load += EDIDViewerForm_Load;
        }
        void EDIDViewerForm_Load(object sender, EventArgs e)
        {            
            //btnMigrate
            if (!System.Security.Principal.WindowsIdentity.GetCurrent().Name.Contains(@"BLR\bsatyadeep") && !System.Security.Principal.WindowsIdentity.GetCurrent().Name.Contains(@"BLR\rvibhakar"))
            {
                btnMigrate.Visible = false;
                btnDecodeXBoxEDID.Visible = false;                
                btnFillData.Visible = false;
                btnCompare.Visible = false;
                btnConvertToBase64.Visible = false;                
            }
            this.btnGetEDIDInfo.Click += btnGetEDIDInfo_Click;            
            this.btnClear.Click += btnClear_Click;
            this.btnBin.Click += btnBin_Click;
            this.btnCreateBin.Click += btnCreateBin_Click;
            this.btnMigrate.Click += btnMigrate_Click;
            this.btnEDIDExcelFile.Click += btnEDIDExcelFile_Click;
            this.btnDownload.Click += btnDownload_Click;            
            this.btnDecodeXBoxEDID.Click += btnDecodeXBoxEDID_Click;
            this.btnFillData.Click += btnFillData_Click;
            this.btnCompare.Click += btnCompare_Click;
            this.btnConvertToBase64.Click += btnConvertToBase64_Click;            
        }               
        void btnCompare_Click(object sender, EventArgs e)
        {
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.Title = "Select EDID Excel File";
            openfile.Filter = "Excel File (*.xls,*.xlsx)|*.xls;*.xlsx";

            if (openfile.ShowDialog() == DialogResult.OK)
            {
                FileInfo m_File = new FileInfo(openfile.FileName);
                if (m_File.Extension.ToLower() == ".xls" || m_File.Extension.ToLower() == ".xlsx")
                {
                    OleDbConnection m_ExcelConnection = null;
                    if (m_File.Extension.ToLower() == ".xls")
                        m_ExcelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + openfile.FileName + ";Extended Properties=Excel 8.0;");
                    else if (m_File.Extension.ToLower() == ".xlsx")
                        m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + openfile.FileName + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");

                    m_ExcelConnection.Open();
                    DataTable dtExcelSheets = m_ExcelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    m_ExcelConnection.Close();
                    if (dtExcelSheets != null)
                    {
                        foreach (DataRow drExcelSheet in dtExcelSheets.Rows)
                        {
                            OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [" + drExcelSheet["TABLE_NAME"].ToString() + "]", m_ExcelConnection);
                            DataTable dtTable = new DataTable();
                            m_ExcelAdapter.Fill(dtTable);
                            if (dtTable.Rows.Count > 0)
                            {
                                foreach (DataRow item in dtTable.Rows)
                                {
                                    if ((!String.IsNullOrEmpty(item["FP1"].ToString())) && (!String.IsNullOrEmpty(item["FP2"].ToString())) && (String.IsNullOrEmpty(item["FP3"].ToString())))
                                    {
                                        DataRow[] dr = dtTable.Select("FP1=" + item["FP1"].ToString() + " and FP2=" + item["FP2"].ToString() + " and FP3=" + item["FP3"].ToString());
                                        if (dr.Length > 0)
                                        {
                                            String duplicateCustomFP = String.Empty;
                                            foreach (DataRow item1 in dr)
                                            {
                                                //128 FP
                                                //if (!String.IsNullOrEmpty(item1["Custom FP"].ToString()))
                                                //{
                                                //    if (item["Custom FP"].ToString() != item1["Custom FP"].ToString())
                                                //    {
                                                //        duplicateCustomFP += item1["Custom FP"].ToString() + ",";
                                                //    }
                                                //}
                                                if (!String.IsNullOrEmpty(item1["Custom FP"].ToString()))
                                                {
                                                    if (item["128 FP"].ToString() != item1["128 FP"].ToString())
                                                    {
                                                        duplicateCustomFP += item1["128 FP"].ToString() + ",";
                                                    }
                                                }
                                            }
                                            if (!String.IsNullOrEmpty(duplicateCustomFP))
                                            {
                                                duplicateCustomFP = duplicateCustomFP.Remove(duplicateCustomFP.Length - 1, 1);
                                                item["Other Custom FP"] = duplicateCustomFP;
                                            }
                                        }
                                    }
                                }
                            }

                            dtTable.AcceptChanges();
                            objDataSet = null;

                            objDataSet = new DataSet("Grouped EDID Summary");
                            if (objDataSet.Tables.Count > 0)
                            {
                                if (objDataSet.Tables.Contains(dtTable.TableName))
                                    objDataSet.Tables.Remove(dtTable.TableName);
                            }
                            objDataSet.Tables.Add(dtTable);
                        }                        
                    }

                    String m_SaveFileMessage = String.Empty;
                    try
                    {
                        String m_filePath = CommonForms.Configuration.GetWorkingDirectory();
                        ExcelReportGenerator.GenerateReport(objDataSet, m_filePath, "EDID Summary", out m_SaveFileMessage);
                        if (!String.IsNullOrEmpty(m_SaveFileMessage))
                        {
                            MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }
        void btnFillData_Click(object sender, EventArgs e)
        {
            String[] sep = { " " };
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.Title = "Select EDID Excel File";
            openfile.Filter = "Excel File (*.xls,*.xlsx)|*.xls;*.xlsx";

            if (openfile.ShowDialog() == DialogResult.OK)
            {
                FileInfo m_File = new FileInfo(openfile.FileName);
                if (m_File.Extension.ToLower() == ".xls" || m_File.Extension.ToLower() == ".xlsx")
                {
                    OleDbConnection m_ExcelConnection = null;
                    if (m_File.Extension.ToLower() == ".xls")
                        m_ExcelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + openfile.FileName + ";Extended Properties=Excel 8.0;");
                    else if (m_File.Extension.ToLower() == ".xlsx")
                        m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + openfile.FileName + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");

                    m_ExcelConnection.Open();
                    DataTable dtExcelSheets = m_ExcelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    m_ExcelConnection.Close();
                    if (dtExcelSheets != null)
                    {
                        foreach (DataRow drExcelSheet in dtExcelSheets.Rows)
                        {
                            OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [" + drExcelSheet["TABLE_NAME"].ToString() + "]", m_ExcelConnection);
                            DataTable dtTable = new DataTable();
                            m_ExcelAdapter.Fill(dtTable);
                            if (dtTable.Rows.Count > 0)
                            {
                                OleDbConnection m_ExcelConnection1 = null;
                                String file1 = @"E:\Working Folder\UEI\Agile\XBox\from Paulo\Telemetry\fp-edids-20141119-count.tsv\EDID from Paulo_1.xlsx";
                                m_ExcelConnection1 = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + file1 + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
                                OleDbDataAdapter m_ExcelAdapter1 = new OleDbDataAdapter("SELECT * FROM [EDID$]", m_ExcelConnection1);
                                DataTable dtTable1 = new DataTable();
                                m_ExcelAdapter1.Fill(dtTable1);

                                String file2 = @"E:\Working Folder\UEI\Agile\XBox\from Paulo\Telemetry\fp-edids-20141119-count.tsv\EDID from Paulo_2.xlsx";
                                m_ExcelConnection1 = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + file2 + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
                                m_ExcelAdapter1 = new OleDbDataAdapter("SELECT * FROM [EDID$]", m_ExcelConnection1);
                                DataTable dtTable2 = new DataTable();
                                m_ExcelAdapter1.Fill(dtTable2);

                                String file3 = @"E:\Working Folder\UEI\Agile\XBox\from Paulo\Telemetry\fp-edids-20141119-count.tsv\EDID from Paulo_3.xlsx";
                                m_ExcelConnection1 = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + file3 + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
                                m_ExcelAdapter1 = new OleDbDataAdapter("SELECT * FROM [EDID$]", m_ExcelConnection1);
                                DataTable dtTable3 = new DataTable();
                                m_ExcelAdapter1.Fill(dtTable3);

                                String file4 = @"E:\Working Folder\UEI\Agile\XBox\from Paulo\Telemetry\fp-edids-20141119-count.tsv\EDID from Paulo_4.xlsx";
                                m_ExcelConnection1 = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + file4 + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
                                m_ExcelAdapter1 = new OleDbDataAdapter("SELECT * FROM [EDID$]", m_ExcelConnection1);
                                DataTable dtTable4 = new DataTable();
                                m_ExcelAdapter1.Fill(dtTable4);

                                String file5 = @"E:\Working Folder\UEI\Agile\XBox\from Paulo\Telemetry\fp-edids-20141119-count.tsv\EDID from Paulo_5.xlsx";
                                m_ExcelConnection1 = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + file5 + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
                                m_ExcelAdapter1 = new OleDbDataAdapter("SELECT * FROM [EDID$]", m_ExcelConnection1);
                                DataTable dtTable5 = new DataTable();
                                m_ExcelAdapter1.Fill(dtTable5);

                                String file6 = @"E:\Working Folder\UEI\Agile\XBox\from Paulo\Telemetry\fp-edids-20141119-count.tsv\EDID from Paulo_6.xlsx";
                                m_ExcelConnection1 = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + file6 + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
                                m_ExcelAdapter1 = new OleDbDataAdapter("SELECT * FROM [EDID$]", m_ExcelConnection1);
                                DataTable dtTable6 = new DataTable();
                                m_ExcelAdapter1.Fill(dtTable6);

                                foreach (DataRow item in dtTable.Rows)
                                {
                                    if (dtTable.Columns.Contains("EDID Block"))
                                    {
                                        if (!String.IsNullOrEmpty(item["EDID Block"].ToString()))
                                        {
                                            String base64String = String.Empty;
                                            String tempEDIDData = String.Empty;
                                            EDIDInfo m_edid = new EDIDInfo();
                                            tempEDIDData = item["EDID Block"].ToString().Trim();
                                            if (tempEDIDData.EndsWith("\n"))
                                            {
                                                tempEDIDData = tempEDIDData.Replace("\n", "");
                                            }
                                            m_edid.EdIdRawData = tempEDIDData;

                                            m_edid.EdId = new List<Byte>();
                                            String[] data = tempEDIDData.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                            if (data.Length > 1)
                                            {
                                                foreach (String bytedata in data)
                                                {
                                                    m_edid.EdId.Add((Byte)Convert.ToInt16(bytedata.Trim(), 16));
                                                }
                                            }
                                            try
                                            {
                                                base64String = this.m_Service.ConvertEDIDByteStreamToBase64String(m_edid);
                                            }
                                            catch { }
                                            if ((dtTable1 != null)&&(!String.IsNullOrEmpty(base64String)))
                                            {

                                                DataRow[] dr = dtTable1.Select("EDID='" + base64String + "'");
                                                if (dr.Length > 0)
                                                {
                                                    item["FP1"] = dr[0]["FP1"].ToString();
                                                    item["FP2"] = dr[0]["FP2"].ToString();
                                                    item["FP3"] = dr[0]["FP3"].ToString();
                                                }
                                            }

                                            if ((dtTable2 != null) && (!String.IsNullOrEmpty(base64String)) && (String.IsNullOrEmpty(item["FP1"].ToString())) && (String.IsNullOrEmpty(item["FP2"].ToString())) && (String.IsNullOrEmpty(item["FP3"].ToString())))
                                            {
                                                DataRow[] dr = dtTable1.Select("EDID='" + base64String + "'");
                                                if (dr.Length > 0)
                                                {
                                                    item["FP1"] = dr[0]["FP1"].ToString();
                                                    item["FP2"] = dr[0]["FP2"].ToString();
                                                    item["FP3"] = dr[0]["FP3"].ToString();
                                                }
                                            }

                                            if ((dtTable3 != null) && (!String.IsNullOrEmpty(base64String)) && (String.IsNullOrEmpty(item["FP1"].ToString())) && (String.IsNullOrEmpty(item["FP2"].ToString())) && (String.IsNullOrEmpty(item["FP3"].ToString())))
                                            {
                                                DataRow[] dr = dtTable1.Select("EDID='" + base64String + "'");
                                                if (dr.Length > 0)
                                                {
                                                    item["FP1"] = dr[0]["FP1"].ToString();
                                                    item["FP2"] = dr[0]["FP2"].ToString();
                                                    item["FP3"] = dr[0]["FP3"].ToString();
                                                }
                                            }

                                            if ((dtTable4 != null) && (!String.IsNullOrEmpty(base64String)) && (String.IsNullOrEmpty(item["FP1"].ToString())) && (String.IsNullOrEmpty(item["FP2"].ToString())) && (String.IsNullOrEmpty(item["FP3"].ToString())))
                                            {
                                                DataRow[] dr = dtTable1.Select("EDID='" + base64String + "'");
                                                if (dr.Length > 0)
                                                {
                                                    item["FP1"] = dr[0]["FP1"].ToString();
                                                    item["FP2"] = dr[0]["FP2"].ToString();
                                                    item["FP3"] = dr[0]["FP3"].ToString();
                                                }
                                            }

                                            if ((dtTable5 != null) && (!String.IsNullOrEmpty(base64String)) && (String.IsNullOrEmpty(item["FP1"].ToString())) && (String.IsNullOrEmpty(item["FP2"].ToString())) && (String.IsNullOrEmpty(item["FP3"].ToString())))
                                            {
                                                DataRow[] dr = dtTable1.Select("EDID='" + base64String + "'");
                                                if (dr.Length > 0)
                                                {
                                                    item["FP1"] = dr[0]["FP1"].ToString();
                                                    item["FP2"] = dr[0]["FP2"].ToString();
                                                    item["FP3"] = dr[0]["FP3"].ToString();
                                                }
                                            }

                                            if ((dtTable6 != null) && (!String.IsNullOrEmpty(base64String)) && (String.IsNullOrEmpty(item["FP1"].ToString())) && (String.IsNullOrEmpty(item["FP2"].ToString())) && (String.IsNullOrEmpty(item["FP3"].ToString())))
                                            {
                                                DataRow[] dr = dtTable1.Select("EDID='" + base64String + "'");
                                                if (dr.Length > 0)
                                                {
                                                    item["FP1"] = dr[0]["FP1"].ToString();
                                                    item["FP2"] = dr[0]["FP2"].ToString();
                                                    item["FP3"] = dr[0]["FP3"].ToString();
                                                }
                                            }
                                        }
                                    }
                                }
                                dtTable.AcceptChanges();
                                objDataSet = null;

                                objDataSet = new DataSet("Grouped EDID Summary");                                
                                if (objDataSet.Tables.Count > 0)
                                {
                                    if (objDataSet.Tables.Contains(dtTable.TableName))
                                        objDataSet.Tables.Remove(dtTable.TableName);
                                }
                                objDataSet.Tables.Add(dtTable);

                            }
                        }
                    }
                }                
                String m_SaveFileMessage = String.Empty;
                try
                {
                    String m_filePath = CommonForms.Configuration.GetWorkingDirectory();
                    ExcelReportGenerator.GenerateReport(objDataSet, m_filePath, "EDID Summary", out m_SaveFileMessage);
                    if (!String.IsNullOrEmpty(m_SaveFileMessage))
                    {
                        MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }              
        #endregion               

        #region Convert to Base 64
        void btnConvertToBase64_Click(object sender, EventArgs e)
        {
            objDataSet = null;
            String[] sep = { " " };
            String[] sep1 = { "," };
            try
            {
                OpenFileDialog openfile = new OpenFileDialog();
                openfile.Title = "Select EDID Excel File";
                openfile.Filter = "Excel File (*.xls,*.xlsx)|*.xls;*.xlsx";

                if (openfile.ShowDialog() == DialogResult.OK)
                {
                    FileInfo m_File = new FileInfo(openfile.FileName);
                    if (m_File.Extension.ToLower() == ".xls" || m_File.Extension.ToLower() == ".xlsx")
                    {
                        OleDbConnection m_ExcelConnection = null;
                        if (m_File.Extension.ToLower() == ".xls")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + openfile.FileName + ";Extended Properties=Excel 8.0;");
                        else if (m_File.Extension.ToLower() == ".xlsx")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + openfile.FileName + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");

                        m_ExcelConnection.Open();
                        DataTable dtExcelSheets = m_ExcelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                        m_ExcelConnection.Close();
                        if (dtExcelSheets != null)
                        {
                            foreach (DataRow drExcelSheet in dtExcelSheets.Rows)
                            {
                                OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [" + drExcelSheet["TABLE_NAME"].ToString() + "]", m_ExcelConnection);
                                DataTable dtTable = new DataTable();
                                m_ExcelAdapter.Fill(dtTable);
                                if (dtTable.Rows.Count > 0)
                                {
                                    DataTable objDataTable = new DataTable(drExcelSheet["TABLE_NAME"].ToString().Replace("$", ""));
                                    foreach (DataColumn item in dtTable.Columns)
                                    {
                                        objDataTable.Columns.Add(item.ColumnName);
                                    }
                                    objDataTable.AcceptChanges();
                                    DataRow dr = null;
                                    foreach (DataRow item in dtTable.Rows)
                                    {
                                        if (!CheckEmptyRow(item))
                                        {
                                            dr = objDataTable.NewRow();
                                            foreach (DataColumn item1 in dtTable.Columns)
                                            {
                                                dr[item1.ColumnName] = item[item1.ColumnName];
                                                if (item1.ColumnName.Contains("EDID Block"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["EDID Block"].ToString()))
                                                    {
                                                        String base64String = item["EDID Block"].ToString();
                                                        if (base64String.EndsWith("\n"))
                                                        {
                                                            base64String = base64String.Replace("\n", "");
                                                        }                                                        
                                                        List<Byte> m_edid = new List<Byte>();
                                                        String[] data = base64String.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                                        if (data.Length > 1)
                                                        {
                                                            foreach (String bytedata in data)
                                                            {
                                                                m_edid.Add((Byte)Convert.ToInt16(bytedata.Trim(), 16));
                                                            }
                                                        }
                                                        dr[item1.ColumnName] = System.Convert.ToBase64String(m_edid.ToArray(), 0, m_edid.ToArray().Length);
                                                    }
                                                }
                                                
                                            }
                                            objDataTable.Rows.Add(dr);
                                            dr = null;                                      
                                        }
                                    }
                                    objDataTable.AcceptChanges();
                                    if (objDataSet == null)
                                    {
                                        objDataSet = new DataSet("EDID Summary");
                                    }
                                    if (objDataSet.Tables.Count > 0)
                                    {
                                        if (objDataSet.Tables.Contains(objDataTable.TableName))
                                            objDataSet.Tables.Remove(objDataTable.TableName);
                                    }
                                    objDataSet.Tables.Add(objDataTable);
                                }
                            }
                            String m_SaveFileMessage = String.Empty;
                            try
                            {
                                String m_filePath = CommonForms.Configuration.GetWorkingDirectory();
                                ExcelReportGenerator.GenerateReport(objDataSet, m_filePath, "EDID Summary", out m_SaveFileMessage);
                                if (!String.IsNullOrEmpty(m_SaveFileMessage))
                                {
                                    MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region Decode XBox EDID
        void btnDecodeXBoxEDID_Click(object sender, EventArgs e)
        {
            objDataSet = null;            
            String[] sep = { " " };
            String[] sep1 = { "," };
            try
            {
                OpenFileDialog openfile = new OpenFileDialog();
                openfile.Title = "Select EDID Excel File";
                openfile.Filter = "Excel File (*.xls,*.xlsx)|*.xls;*.xlsx";

                if (openfile.ShowDialog() == DialogResult.OK)
                {
                    FileInfo m_File = new FileInfo(openfile.FileName);
                    if (m_File.Extension.ToLower() == ".xls" || m_File.Extension.ToLower() == ".xlsx")
                    {
                        OleDbConnection m_ExcelConnection = null;
                        if (m_File.Extension.ToLower() == ".xls")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + openfile.FileName + ";Extended Properties=Excel 8.0;");
                        else if (m_File.Extension.ToLower() == ".xlsx")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + openfile.FileName + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");

                        m_ExcelConnection.Open();
                        DataTable dtExcelSheets = m_ExcelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                        m_ExcelConnection.Close();
                        if (dtExcelSheets != null)
                        {
                            foreach (DataRow drExcelSheet in dtExcelSheets.Rows)
                            {
                                OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [" + drExcelSheet["TABLE_NAME"].ToString() + "]", m_ExcelConnection);
                                DataTable dtTable = new DataTable();
                                m_ExcelAdapter.Fill(dtTable);
                                if (dtTable.Rows.Count > 0)
                                {
                                    DataTable objDataTable = new DataTable(drExcelSheet["TABLE_NAME"].ToString().Replace("$", ""));
                                    objDataTable.Columns.Add("SL NO");
                                    objDataTable.Columns.Add("Main Device");
                                    objDataTable.Columns.Add("Sub Device");
                                    objDataTable.Columns.Add("Component");
                                    objDataTable.Columns.Add("Region");
                                    objDataTable.Columns.Add("Brand");
                                    objDataTable.Columns.Add("Model");
                                    objDataTable.Columns.Add("Codeset");
                                    objDataTable.Columns.Add("EDID");
                                    objDataTable.Columns.Add("EDID Block");
                                    objDataTable.Columns.Add("Manufacturer (3 char) code");
                                    objDataTable.Columns.Add("Manufacturer Hex Code");
                                    objDataTable.Columns.Add("Monitor Name");
                                    objDataTable.Columns.Add("Monitor Range Limits");
                                    objDataTable.Columns.Add("Product Code");
                                    objDataTable.Columns.Add("Serial number");
                                    objDataTable.Columns.Add("Week");
                                    objDataTable.Columns.Add("Year");
                                    objDataTable.Columns.Add("Horizontal Screen Size");
                                    objDataTable.Columns.Add("Vertical Screen Size");
                                    objDataTable.Columns.Add("Video input definition");
                                    objDataTable.Columns.Add("OSD");
                                    objDataTable.Columns.Add("Custom FP Type0x00");
                                    objDataTable.Columns.Add("Custom FP Type0x01");
                                    objDataTable.Columns.Add("Custom FP Type0x02");
                                    objDataTable.Columns.Add("Custom FP Type0x03");
                                    objDataTable.Columns.Add("Console Count");
                                    objDataTable.Columns.Add("CEC Physical Address");
                                    objDataTable.AcceptChanges();
                                    Int32 intSLNo = 1;
                                    foreach (DataRow item in dtTable.Rows)
                                    {
                                        if (!CheckEmptyRow(item))
                                        {
                                            DataRow dr = null;
                                            EdIdServices m_service = null;
                                            dr = objDataTable.NewRow();                                                                                                                                                                             
                                            if (dtTable.Columns.Contains("EDID"))
                                            {
                                                if (!String.IsNullOrEmpty(item["EDID"].ToString()))
                                                {
                                                    String data = item["EDID"].ToString().Trim();
                                                    if (data.EndsWith("|0") || data.EndsWith("|1"))
                                                        data = data.Remove(data.Length - 2, 2);
                                                    dr["EDID"] = data.Trim();                                                    
                                                    try
                                                    {
                                                        Byte[] _bindata = Convert.FromBase64String(data);
                                                        if (_bindata != null && _bindata.Length > 0)
                                                        {
                                                            EDIDInfo edidByteData = new EDIDInfo();
                                                            edidByteData.EdId = new List<byte>();
                                                            List<Byte> _tempedidData = new List<Byte>();
                                                            foreach (Byte byteData in _bindata)
                                                            {
                                                                _tempedidData.Add(byteData);
                                                            }
                                                            m_service = new EdIdServices();
                                                            Byte[] edidData = m_service.ConvertEdidToBin(_tempedidData);
                                                            edidByteData = m_service.ReadBinFile(edidData);
                                                            dr["EDID Block"] = edidByteData.EdIdRawData.Trim();
                                                            dr["Custom FP Type0x00"] = edidByteData.CustomFPType0x00;
                                                            dr["Custom FP Type0x01"] = edidByteData.CustomFPType0x01;
                                                            dr["Custom FP Type0x02"] = edidByteData.CustomFPType0x02;
                                                            dr["Custom FP Type0x03"] = edidByteData.CustomFPType0x03;
                                                            dr["Manufacturer (3 char) code"] = edidByteData.ManufacturerCode;
                                                            dr["Manufacturer Hex Code"] = edidByteData.ManufacturerHexCode;
                                                            dr["Monitor Name"] = edidByteData.ManufacturerName;
                                                            dr["Product Code"] = edidByteData.ProductCode;
                                                            dr["Serial number"] = edidByteData.SerialNumber;
                                                            dr["Week"] = edidByteData.Week;
                                                            dr["Year"] = edidByteData.Year;
                                                            dr["Video input definition"] = edidByteData.VideoInputDefinition;
                                                            dr["Horizontal Screen Size"] = edidByteData.HorizontalScreenSize;
                                                            dr["Vertical Screen Size"] = edidByteData.VerticalScreenSize;
                                                            dr["CEC Physical Address"] = edidByteData.PhysicalAddress;
                                                        }
                                                    }
                                                    catch (Exception ex) { String s = ex.Message; }
                                                }
                                            }
                                            if (dr != null)
                                                objDataTable.Rows.Add(dr);
                                            dr = null;
                                            intSLNo++;
                                        }
                                    }
                                    if (objDataSet == null)
                                    {
                                        objDataSet = new DataSet("EDID Summary");
                                    }
                                    if (objDataSet.Tables.Count > 0)
                                    {
                                        if (objDataSet.Tables.Contains(objDataTable.TableName))
                                            objDataSet.Tables.Remove(objDataTable.TableName);
                                    }
                                    objDataSet.Tables.Add(objDataTable);
                                }
                            }
                            String m_SaveFileMessage = String.Empty;
                            try
                            {
                                String m_filePath = CommonForms.Configuration.GetWorkingDirectory();
                                ExcelReportGenerator.GenerateReport(objDataSet, m_filePath, "EDID Summary", out m_SaveFileMessage);
                                if (!String.IsNullOrEmpty(m_SaveFileMessage))
                                {
                                    MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        } 
        #endregion        

        #region Download
        void btnDownload_Click(object sender, EventArgs e)
        {
            SaveFileDialog m_SaveFileDialog = new SaveFileDialog();
            m_SaveFileDialog.Title = "EDID Excel File Format";
            m_SaveFileDialog.Filter = "Excel File (*.xlsx)|*.xlsx";
            m_SaveFileDialog.FileName = "InputFile";
            m_SaveFileDialog.CheckPathExists = true;
            if (m_SaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileStream m_FileStream = null;
                try
                {
                    m_FileStream = new FileStream(m_SaveFileDialog.FileName, FileMode.Create);
                    m_FileStream.Write(UEI.EDID.Properties.Resources.InputFormat, 0, UEI.EDID.Properties.Resources.InputFormat.Length);
                    m_FileStream.Close();
                }
                catch (Exception ex)
                {
                    m_FileStream.Close();
                    MessageBox.Show(ex.Message, "EDID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        #endregion

        #region Brand Profile
        void btnEDIDExcelFile_Click(object sender, EventArgs e)
        {
            objDataSet = null;
            String[] sep = { " " };
            String[] sep1 = { "," };
            try
            {
                OpenFileDialog openfile = new OpenFileDialog();
                openfile.Title = "Select EDID Excel File";
                openfile.Filter = "Excel File (*.xls,*.xlsx)|*.xls;*.xlsx";

                if (openfile.ShowDialog() == DialogResult.OK)
                {
                    FileInfo m_File = new FileInfo(openfile.FileName);
                    if (m_File.Extension.ToLower() == ".xls" || m_File.Extension.ToLower() == ".xlsx")
                    {
                        OleDbConnection m_ExcelConnection = null;
                        if (m_File.Extension.ToLower() == ".xls")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + openfile.FileName + ";Extended Properties=Excel 8.0;");
                        else if (m_File.Extension.ToLower() == ".xlsx")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + openfile.FileName + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");

                        m_ExcelConnection.Open();
                        DataTable dtExcelSheets = m_ExcelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                        m_ExcelConnection.Close();
                        if (dtExcelSheets != null)
                        {
                            foreach (DataRow drExcelSheet in dtExcelSheets.Rows)
                            {
                                try
                                {
                                    //FilterData
                                    //RULES
                                    //SHARP BRAND RULES
                                    //TOSHIBA_BRAND RULE
                                    if ((!drExcelSheet["TABLE_NAME"].ToString().Contains("FilterData")) && (!drExcelSheet["TABLE_NAME"].ToString().ToUpper().Trim().Contains("SHARP BRAND RULES")) && (!drExcelSheet["TABLE_NAME"].ToString().Trim().ToUpper().Contains("TOSHIBA_BRAND RULE")))
                                    //if (!drExcelSheet["TABLE_NAME"].ToString().Contains("FilterData"))
                                    {
                                        OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [" + drExcelSheet["TABLE_NAME"].ToString() + "]", m_ExcelConnection);
                                        DataTable dtTable = new DataTable();
                                        m_ExcelAdapter.Fill(dtTable);
                                        if (dtTable != null)
                                            if (dtTable.Rows.Count > 0)
                                            {
                                                DataTable objDataTable = new DataTable(drExcelSheet["TABLE_NAME"].ToString().Replace("$", ""));
                                                objDataTable.Columns.Add("Brand");
                                                objDataTable.Columns.Add("Model");

                                                objDataTable.Columns.Add("Profile");
                                                objDataTable.Columns.Add("Fingerprint To Be Publish in DAC3");
                                                objDataTable.Columns.Add("ID");
                                                objDataTable.Columns.Add("Auto detect source");
                                                objDataTable.Columns.Add("Published in DB");
                                                objDataTable.Columns.Add("EDID Block");
                                                objDataTable.Columns.Add("Manufacturer (3 char) code");
                                                objDataTable.Columns.Add("Manufacturer Hex Code");
                                                objDataTable.Columns.Add("Monitor Name");
                                                objDataTable.Columns.Add("Product Code");
                                                objDataTable.Columns.Add("Serial number");
                                                objDataTable.Columns.Add("Week");
                                                objDataTable.Columns.Add("Year");
                                                objDataTable.Columns.Add("Horizontal Screen Size");
                                                objDataTable.Columns.Add("Vertical Screen Size");
                                                objDataTable.Columns.Add("Video input definition");
                                                objDataTable.Columns.Add("OSD");
                                                objDataTable.Columns.Add("FP Available");
                                                objDataTable.Columns.Add("FP Bytes");
                                                objDataTable.Columns.Add("Custom FP Type0x00");
                                                objDataTable.Columns.Add("Custom FP Type0x01");
                                                objDataTable.Columns.Add("Custom FP Type0x02");
                                                objDataTable.Columns.Add("Custom FP Type0x03");
                                                objDataTable.Columns.Add("Custom FP");
                                                objDataTable.Columns.Add("Custom FP + OSD");
                                                objDataTable.Columns.Add("128 FP");
                                                objDataTable.Columns.Add("128 FP + OSD");
                                                objDataTable.Columns.Add("OSD FP");
                                                objDataTable.Columns.Add("Present in Xbox");
                                                objDataTable.Columns.Add("Console Count", Type.GetType("System.Int64"));
                                                objDataTable.Columns.Add("CEC Physical Address");
                                                objDataTable.AcceptChanges();
                                                Int32 intSLNo = 1;
                                                foreach (DataRow item in dtTable.Rows)
                                                {
                                                    if (!CheckEmptyRow(item))
                                                    {
                                                        DataRow dr = null;
                                                        EdIdServices m_service = null;
                                                        dr = objDataTable.NewRow();                                                        
                                                        if (dtTable.Columns.Contains("Profile"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["Profile"].ToString()))
                                                            {
                                                                dr["Profile"] = item["Profile"].ToString().Trim();
                                                            }
                                                        }
                                                        if (dtTable.Columns.Contains("Fingerprint To Be Publish in DAC3"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["Fingerprint To Be Publish in DAC3"].ToString()))
                                                            {
                                                                dr["Fingerprint To Be Publish in DAC3"] = item["Fingerprint To Be Publish in DAC3"].ToString().Trim();
                                                            }
                                                        }
                                                        if (dtTable.Columns.Contains("ID"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["ID"].ToString()))
                                                            {
                                                                dr["ID"] = item["ID"].ToString().Trim();
                                                            }
                                                        }
                                                        if (dtTable.Columns.Contains("Auto detect source"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["Auto detect source"].ToString()))
                                                            {
                                                                dr["Auto detect source"] = item["Auto detect source"].ToString().Trim();
                                                            }
                                                        }
                                                        if (dtTable.Columns.Contains("Published in DB"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["Published in DB"].ToString()))
                                                            {
                                                                dr["Published in DB"] = item["Published in DB"].ToString().Trim();
                                                            }
                                                        }
                                                        if (dtTable.Columns.Contains("Present in Xbox"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["Present in Xbox"].ToString()))
                                                            {
                                                                dr["Present in Xbox"] = item["Present in Xbox"].ToString().Trim();
                                                            }
                                                        }
                                                        if (dtTable.Columns.Contains("Console Count"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["Console Count"].ToString()))
                                                            {
                                                                dr["Console Count"] = item["Console Count"].ToString().Trim();
                                                            }
                                                        }
                                                        if (dtTable.Columns.Contains("Brand"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["Brand"].ToString()))
                                                            {
                                                                dr["Brand"] = item["Brand"].ToString().Trim();
                                                            }
                                                        }
                                                        if (dtTable.Columns.Contains("Model"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["Model"].ToString()))
                                                            {
                                                                dr["Model"] = item["Model"].ToString().Trim();
                                                            }
                                                        }
                                                        if (dtTable.Columns.Contains("Profile"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["Profile"].ToString()))
                                                            {
                                                                dr["Profile"] = item["Profile"].ToString().Trim();
                                                            }
                                                        }
                                                        if (dtTable.Columns.Contains("Fingerprint To Be Publish in DAC3"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["Fingerprint To Be Publish in DAC3"].ToString()))
                                                            {
                                                                dr["Fingerprint To Be Publish in DAC3"] = item["Fingerprint To Be Publish in DAC3"].ToString().Trim();
                                                            }
                                                        }
                                                        if (dtTable.Columns.Contains("ID"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["ID"].ToString()))
                                                            {
                                                                dr["ID"] = item["ID"].ToString().Trim();
                                                            }
                                                        }
                                                        List<Byte> osdbyteData = null;
                                                        if (dtTable.Columns.Contains("OSD"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["OSD"].ToString()))
                                                            {
                                                                dr["OSD"] = item["OSD"].ToString();
                                                                osdbyteData = new List<Byte>();
                                                                String[] data = item["OSD"].ToString().Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                                                if (data.Length > 1)
                                                                {
                                                                    foreach (String _bytedata in data)
                                                                    {
                                                                        osdbyteData.Add((Byte)Convert.ToInt32(_bytedata.Trim(), 16));
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (dtTable.Columns.Contains("FP Available"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["FP Available"].ToString()))
                                                            {
                                                                dr["FP Available"] = item["FP Available"].ToString();
                                                            }
                                                        }
                                                        if (dtTable.Columns.Contains("FP Bytes"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["FP Bytes"].ToString()))
                                                            {
                                                                dr["FP Bytes"] = item["FP Bytes"].ToString();
                                                            }
                                                        }
                                                        if (dtTable.Columns.Contains("EDID Block"))
                                                        {
                                                            if (!String.IsNullOrEmpty(item["EDID Block"].ToString()))
                                                            {
                                                                String data = item["EDID Block"].ToString().Trim();
                                                                if (data.EndsWith("\n"))
                                                                {
                                                                    data = data.Replace("\n", "");
                                                                }
                                                                dr["EDID Block"] = data.Trim();
                                                                List<Byte> _bindata = null;

                                                                String[] datas = data.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                                                                if (datas.Length > 1)
                                                                {
                                                                    _bindata = new List<Byte>();
                                                                    foreach (String bytedata in datas)
                                                                    {
                                                                        _bindata.Add((Byte)Convert.ToInt16(bytedata.Trim(), 16));
                                                                    }
                                                                }
                                                                else if (data.Length > 0)
                                                                {
                                                                    _bindata = new List<Byte>();
                                                                    while (!String.IsNullOrEmpty(data))
                                                                    {
                                                                        String subData = data.Substring(0, 2);
                                                                        data = data.Remove(0, 2);
                                                                        _bindata.Add((Byte)Convert.ToInt16(subData.Trim(), 16));
                                                                    }
                                                                }
                                                                if (_bindata != null && _bindata.Count > 0)
                                                                {
                                                                    EDIDInfo edidByteData = new EDIDInfo();
                                                                    edidByteData.EdId = new List<byte>();
                                                                    m_service = new EdIdServices();
                                                                    Byte[] edidData = m_service.ConvertEdidToBin(_bindata);
                                                                    edidByteData = m_service.ReadBinFile(edidData);
                                                                    String strMake = String.Empty;
                                                                    String strDevice = String.Empty;
                                                                    //strDevice = "TV";
                                                                    if (!String.IsNullOrEmpty(item["Brand"].ToString()))
                                                                        strMake = item["Brand"].ToString().Trim().ToLower();
                                                                    if (!String.IsNullOrEmpty(item["MainDevice"].ToString()))
                                                                        strDevice = item["MainDevice"].ToString().Trim().ToLower();
                                                                    if (!String.IsNullOrEmpty(dr["FP Bytes"].ToString()))
                                                                    {
                                                                        dr["FP Available"] = "Y";
                                                                        edidByteData.FPAvailable = "Y";
                                                                        edidByteData.FPBytePosition = dr["FP Bytes"].ToString();
                                                                    }
                                                                    else
                                                                    {
                                                                        //Consider Standard Byte Position based on Brand to calculate Fingerprint
                                                                        Dictionary<String, String> m_Position = m_service.GetBytePositionBasedonBrands(strDevice, strMake);
                                                                        foreach (KeyValuePair<String, String> pair in m_Position)
                                                                        {
                                                                            dr["FP Available"] = pair.Key;
                                                                            dr["FP Bytes"] = pair.Value;
                                                                            edidByteData.FPAvailable = pair.Key;
                                                                            edidByteData.FPBytePosition = pair.Value;
                                                                        }
                                                                    }
                                                                    edidByteData.BytePositionforFingerPrint = new List<Int32>();
                                                                    String[] values = edidByteData.FPBytePosition.Split(sep1, StringSplitOptions.RemoveEmptyEntries);
                                                                    for (Int32 i = 0; i < values.Length; i++)
                                                                    {
                                                                        edidByteData.BytePositionforFingerPrint.Add(Int32.Parse(values[i]));
                                                                    }

                                                                    try
                                                                    {
                                                                        //if osd present consider it
                                                                        edidByteData.OSD = osdbyteData;
                                                                        m_service = new EdIdServices();
                                                                        Byte[] m_fileStructure = m_service.CreateBinFile(edidByteData);
                                                                        EDIDInfo m_edid = m_service.ReadBinFile(m_fileStructure);

                                                                        //EDIDParser m_EDIDDecoder = new EDIDParser();
                                                                        //EDIDData m_EDIDObject = m_EDIDDecoder.ParseEDID(m_edid.EdId.ToArray());
                                                                        //DecodedEDIDData m_EDID = m_service.GetDecodedEDIDDataFromEDIDObject(m_EDIDObject);

                                                                        edidByteData.CustomFP = m_edid.CustomFP;
                                                                        if (edidByteData.OSD != null)
                                                                        {
                                                                            if (edidByteData.OSD.Count > 0)
                                                                                edidByteData.CustomFPOSD = m_edid.CustomFPOSD;
                                                                        }

                                                                        dr["Custom FP Type0x00"] = m_edid.CustomFPType0x00;
                                                                        dr["Custom FP Type0x01"] = m_edid.CustomFPType0x01;
                                                                        dr["Custom FP Type0x02"] = m_edid.CustomFPType0x02;
                                                                        dr["Custom FP Type0x03"] = m_edid.CustomFPType0x03;
                                                                        dr["Custom FP"] = m_edid.CustomFP;
                                                                        dr["Custom FP + OSD"] = m_edid.CustomFPOSD;
                                                                        dr["128 FP"] = m_edid.Only128FP;
                                                                        dr["128 FP + OSD"] = m_edid.Only128FPOSD;
                                                                        dr["OSD FP"] = m_edid.OnlyOSDFP;

                                                                        dr["Manufacturer (3 char) code"] = m_edid.ManufacturerCode;
                                                                        dr["Manufacturer Hex Code"] = m_edid.ManufacturerHexCode;
                                                                        dr["Monitor Name"] = m_edid.ManufacturerName;

                                                                        dr["Product Code"] = m_edid.ProductCode; ;

                                                                        dr["Serial number"] = m_edid.SerialNumber;
                                                                        dr["Week"] = m_edid.Week;

                                                                        
                                                                            dr["Year"] = m_edid.Year;


                                                                            dr["Video input definition"] = m_edid.VideoInputDefinition;                                                                       
                                                                        dr["Horizontal Screen Size"] = m_edid.HorizontalScreenSize;
                                                                        dr["Vertical Screen Size"] = m_edid.VerticalScreenSize;
                                                                        dr["CEC Physical Address"] = m_edid.PhysicalAddress;
                                                                    }
                                                                    catch
                                                                    {
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (dr != null)
                                                            objDataTable.Rows.Add(dr);
                                                        dr = null;
                                                        intSLNo++;
                                                    }
                                                }
                                                if (objDataSet == null)
                                                {
                                                    objDataSet = new DataSet("EDID Summary");
                                                }
                                                if (objDataSet.Tables.Count > 0)
                                                {
                                                    if (objDataSet.Tables.Contains(objDataTable.TableName))
                                                        objDataSet.Tables.Remove(objDataTable.TableName);
                                                }
                                                objDataSet.Tables.Add(objDataTable);
                                            }
                                    }
                                }
                                catch { }
                            }
                            String m_SaveFileMessage = String.Empty;
                            try
                            {
                                String m_filePath = CommonForms.Configuration.GetWorkingDirectory();
                                ExcelReportGenerator.GenerateReport(objDataSet, m_filePath, "EDID Summary", out m_SaveFileMessage);
                                if (!String.IsNullOrEmpty(m_SaveFileMessage))
                                {
                                    MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region Migrate EDID
        void btnMigrate_Click(object sender, EventArgs e)
        {
            try
            {
                m_Service = new EdIdServices();
                m_Service.ErrorMessage = String.Empty;                
                m_Service.MigrateandUpdateCapturedEDID();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Migration Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region Create Bin File
        void btnCreateBin_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtBase64EDIDData.Text))
            {
                String tempEDIDData = String.Empty;
                tempEDIDData = txtBase64EDIDData.Text.Trim();
                //if (txtBase64EDIDData.Text.EndsWith("\n"))
                //{
                //    tempEDIDData = txtBase64EDIDData.Text.Replace("\n", "");
                //}
                List<Byte> edidData = new List<Byte>();
                String[] sep = { " " };
                String[] data = tempEDIDData.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                if (data.Length > 1)
                {
                    edidData = new List<Byte>();
                    foreach (String bytedata in data)
                    {
                        edidData.Add((Byte)Convert.ToInt16(bytedata.Trim(), 16));
                    }
                }
                else if (data.Length > 0)
                {
                    edidData = new List<Byte>();
                    if (!String.IsNullOrEmpty(tempEDIDData))
                    {
                        if (tempEDIDData.EndsWith("|0") || tempEDIDData.EndsWith("|1"))
                            tempEDIDData = tempEDIDData.Remove(data.Length - 2, 2);
                        Byte[] _binData = Convert.FromBase64String(tempEDIDData);

                        edidData = null;
                        foreach (Byte item in _binData)
                        {
                            if (edidData == null)
                                edidData = new List<byte>();
                            edidData.Add(item);
                        }
                    }
                }

                m_Service = new EdIdServices();
                Byte[] binData = m_Service.ConvertEdidToBin(edidData);

                String filePath = CommonForms.Configuration.GetWorkingDirectory();
                if (String.IsNullOrEmpty(filePath))
                {
                    FolderBrowserDialog dlg1 = new FolderBrowserDialog();
                    if (dlg1.ShowDialog() == DialogResult.OK)
                    {
                        if (dlg1.SelectedPath.EndsWith("\\") == false)
                        {
                            dlg1.SelectedPath += "\\";
                        }
                        filePath = dlg1.SelectedPath;
                    }
                }
                filePath += "EDIDBin.bin";
                //System.IO.File.WriteAllBytes(filePath, binData);
                System.IO.File.WriteAllBytes(filePath, edidData.ToArray());
                EDIDInfo edidINfo = m_Service.ReadBinFile(binData);

                EDIDParser m_EDIDDecoder = new EDIDParser();
                EDIDData m_EDIDObject = m_EDIDDecoder.ParseEDID(edidINfo.EdId.ToArray());
                this.EDIDDetails = m_Service.GetDecodedEDIDDataFromEDIDObject(m_EDIDObject);
                if (this.EDIDDetails != null)
                    this.propertyGrid.SelectedObject = this.EDIDDetails;
            }
            else
                MessageBox.Show("Please enter EDID Data", "EDID", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        #endregion

        #region Load Bin
        void btnBin_Click(object sender, EventArgs e)
        {
            OpenFileDialog openBin = new OpenFileDialog();
            openBin.Filter = "Bin File|*.bin";
            if (openBin.ShowDialog() == DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(openBin.FileName))
                {
                    byte[] binData = System.IO.File.ReadAllBytes(openBin.FileName);
                    if (binData != null)
                    {                                             
                        try
                        {                                                        
                            EDID tempEDIDDetails = new EDID();
                            m_Service = new EdIdServices();
                            EDIDInfo edidByteData1 = null;
                            try
                            {
                                edidByteData1 = m_Service.ReadBinFile(binData);
                                if (edidByteData1 != null)
                                {
                                    if (edidByteData1.EdId == null)
                                    {
                                        List<Byte> byteStream = null;
                                        foreach (Byte item in binData)
                                        {
                                            if (byteStream == null)
                                                byteStream = new List<byte>();
                                            byteStream.Add(item);
                                        }
                                        binData = m_Service.ConvertEdidToBin(byteStream);
                                        edidByteData1 = m_Service.ReadBinFile(binData);
                                        txtBase64EDIDHexDatainBin.Text = edidByteData1.EdIdRawData;
                                        txtBase64EDIDData.Text = Convert.ToBase64String(edidByteData1.EdId.ToArray());
                                    }
                                    else
                                    {
                                        txtBase64EDIDHexDatainBin.Text = edidByteData1.EdIdRawData;
                                        txtBase64EDIDData.Text = Convert.ToBase64String(edidByteData1.EdId.ToArray());
                                    }
                                }
                            }
                            catch { }
                                                                                    
                            if (edidByteData1 != null)
                            {
                                try
                                {
                                    EDIDParser m_EDIDDecoder = new EDIDParser();
                                    EDIDData m_EDIDObject = m_EDIDDecoder.ParseEDID(edidByteData1.EdId.ToArray());
                                    this.EDIDDetails = m_Service.GetDecodedEDIDDataFromEDIDObject(m_EDIDObject);
                                }
                                catch { }
                            }
                            
                            if (this.EDIDDetails != null)
                                this.propertyGrid.SelectedObject = this.EDIDDetails;
                            
                            //String _data = String.Empty;                           
                            //_data = "0x";
                            //Int32 len = binData.Length - 1;
                            //for (Int32 i = len; i >= 0; i--)
                            //{
                            //    //Int32 dt = Convert.ToInt16(data[i].ToString(), 16);
                            //    _data += binData[i].ToString("X");
                            //}
                            
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show(ex.Message, "EDID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }                
            }           
        }
        #endregion

        #region Clear All Field
        void btnClear_Click(object sender, EventArgs e)
        {
            this.txtBase64EDIDData.Text = String.Empty;           
            this.txtBase64EDIDHexDatainBin.Text = String.Empty;
            this.propertyGrid.SelectedObject = null;
            this.propertyGrid1.SelectedObject = null;
        }
        #endregion

        #region Get EDID Info from Base 64
        void btnGetEDIDInfo_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(this.txtBase64EDIDData.Text))
            {
                try
                {
                    String data = this.txtBase64EDIDData.Text.Trim();

                    if (data.EndsWith("\n"))
                        data = data.Replace("\n", "");
                    if (data.EndsWith("|0") || data.EndsWith("|1") || data.EndsWith("|2") || data.EndsWith("|3") || data.EndsWith("|4"))
                        data = data.Remove(data.Length - 2, 2);

                    Byte[] _bindata = Convert.FromBase64String(data);
                    List<Byte> binData = new List<Byte>();                   
                    Int32 len = _bindata.Length;
                    for (Int32 i = 0; i < len; i++)
                    {                                               
                        binData.Add(_bindata[i]);
                    }                   
                    if (_bindata != null)
                    {
                        String filePath = CommonForms.Configuration.GetWorkingDirectory();
                        filePath += "EDIDBin.bin";                        
                        System.IO.File.WriteAllBytes(filePath, _bindata);
                        EDIDDetails = new DecodedEDIDData();

                        try
                        {
                            m_Service = new EdIdServices();
                            Byte[] edidData = m_Service.ConvertEdidToBin(binData);
                            EDIDInfo edidByteData = m_Service.ReadBinFile(edidData);
                            txtBase64EDIDHexDatainBin.Text = edidByteData.EdIdRawData;
                            EDIDParser m_EDIDDecoder = new EDIDParser();
                            EDIDData m_EDIDObject = m_EDIDDecoder.ParseEDID(edidByteData.EdId.ToArray());
                            EDIDDetails = m_Service.GetDecodedEDIDDataFromEDIDObject(m_EDIDObject);
                            if (EDIDDetails != null)
                            {
                                this.propertyGrid.SelectedObject = this.EDIDDetails;
                                this.propertyGrid1.SelectedObject = edidByteData;
                            }
                        }
                        catch { }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "EDID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        #endregion

        #region EDID Info from Hex Code
        private void txtBase64EDIDHexDatainBin_TextChanged(object sender, EventArgs e)
        {
            String[] sep = { " ", "," };
            if (!String.IsNullOrEmpty(this.txtBase64EDIDHexDatainBin.Text))
            {
                List<Byte> binData = new List<byte>();
                String[] data = this.txtBase64EDIDHexDatainBin.Text.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                if (data.Length > 1)
                {
                    foreach (String bytedata in data)
                    {
                        binData.Add((Byte)Convert.ToInt16(bytedata.Trim(), 16));
                    }
                }
                else
                {
                    String data1 = this.txtBase64EDIDHexDatainBin.Text.Trim();
                    while (!String.IsNullOrEmpty(data1))
                    {
                        String subData = data1.Substring(0, 2);
                        data1 = data1.Remove(0, 2);
                        binData.Add((Byte)Convert.ToInt16(subData.Trim(), 16));
                    }  
                }
                if (binData != null)
                {
                    m_Service = new EdIdServices();
                    Byte[] edidData = m_Service.ConvertEdidToBin(binData);
                    EDIDInfo edidByteData = m_Service.ReadBinFile(edidData);
                    txtBase64EDIDData.Text = System.Convert.ToBase64String(edidByteData.EdId.ToArray(), 0, edidByteData.EdId.ToArray().Length);
                    EDIDParser m_EDIDDecoder = new EDIDParser();
                    EDIDData m_EDIDObject = m_EDIDDecoder.ParseEDID(edidByteData.EdId.ToArray());
                    EDIDDetails = m_Service.GetDecodedEDIDDataFromEDIDObject(m_EDIDObject);
                    if (EDIDDetails != null)
                    {
                        this.propertyGrid.SelectedObject = this.EDIDDetails;
                        this.propertyGrid1.SelectedObject = edidByteData;
                    }
                }
            }
        }
        #endregion
        
        private Boolean CheckEmptyRow(DataRow m_Row)
        {
            String valuesarr = String.Empty;
            try
            {
                foreach (Object item in m_Row.ItemArray)
                {
                    if (item != null)
                        valuesarr += item.ToString();
                }
            }
            catch { valuesarr = String.Empty; }
            if (String.IsNullOrEmpty(valuesarr))
                return true;
            else
                return false;
        }       
        private void button2_Click(object sender, EventArgs e)
        {
            objDataSet = null;
            BrandTranslationCollection brandTranslattion = null;
            String[] sep = { " " };
            String[] sep1 = { "," };
            try
            {
                OpenFileDialog openfile = new OpenFileDialog();
                openfile.Title = "Select EDID Excel File";
                openfile.Filter = "Excel File (*.xls,*.xlsx)|*.xls;*.xlsx";

                if (openfile.ShowDialog() == DialogResult.OK)
                {                                        
                    FileInfo m_File = new FileInfo(openfile.FileName);
                    if (m_File.Extension.ToLower() == ".xls" || m_File.Extension.ToLower() == ".xlsx")
                    {
                        OleDbConnection m_ExcelConnection = null;
                        if (m_File.Extension.ToLower() == ".xls")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + openfile.FileName + ";Extended Properties=Excel 8.0;");
                        else if (m_File.Extension.ToLower() == ".xlsx")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + openfile.FileName + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");

                        m_ExcelConnection.Open();
                        DataTable dtExcelSheets = m_ExcelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                        m_ExcelConnection.Close();
                        if (dtExcelSheets != null)
                        {
                            foreach (DataRow drExcelSheet in dtExcelSheets.Rows)
                            {
                                OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [" + drExcelSheet["TABLE_NAME"].ToString() + "]", m_ExcelConnection);
                                DataTable dtTable = new DataTable();
                                m_ExcelAdapter.Fill(dtTable);
                                if (dtTable.Rows.Count > 0)
                                {
                                    DataTable objDataTable = new DataTable(drExcelSheet["TABLE_NAME"].ToString().Replace("$", ""));
                                    objDataTable.Columns.Add("SL NO");
                                    objDataTable.Columns.Add("Main Device");
                                    objDataTable.Columns.Add("Sub Device");
                                    objDataTable.Columns.Add("Component");
                                    objDataTable.Columns.Add("Region");
                                    objDataTable.Columns.Add("Brand");
                                    objDataTable.Columns.Add("Model");
                                    objDataTable.Columns.Add("Codeset");
                                    objDataTable.Columns.Add("EDID");
                                    objDataTable.Columns.Add("EDID Block");
                                    objDataTable.Columns.Add("Manufacturer (3 char) code");
                                    objDataTable.Columns.Add("Manufacturer Hex Code");
                                    objDataTable.Columns.Add("Monitor Name");                                    
                                    objDataTable.Columns.Add("Product Code");
                                    objDataTable.Columns.Add("Serial number");
                                    objDataTable.Columns.Add("Week");
                                    objDataTable.Columns.Add("Year");
                                    objDataTable.Columns.Add("Horizontal Screen Size");
                                    objDataTable.Columns.Add("Vertical Screen Size");
                                    objDataTable.Columns.Add("Video input definition");
                                    objDataTable.Columns.Add("OSD");                                    
                                    objDataTable.Columns.Add("FP Available");
                                    objDataTable.Columns.Add("FP Bytes");
                                    objDataTable.Columns.Add("Custom FP Type0x00");
                                    objDataTable.Columns.Add("Custom FP Type0x01");
                                    objDataTable.Columns.Add("Custom FP Type0x02");
                                    objDataTable.Columns.Add("Custom FP Type0x03");
                                    objDataTable.Columns.Add("Custom FP");
                                    objDataTable.Columns.Add("Custom FP + OSD");
                                    objDataTable.Columns.Add("128 FP");
                                    objDataTable.Columns.Add("128 FP + OSD");
                                    objDataTable.Columns.Add("OSD FP");
                                    objDataTable.Columns.Add("CEC Physical Address");
                                    objDataTable.AcceptChanges();
                                    Int32 intSLNo = 1;
                                    foreach (DataRow item in dtTable.Rows)
                                    {
                                        if (!CheckEmptyRow(item))
                                        {
                                            DataRow dr = null;
                                            EdIdServices m_service = null;
                                            dr = objDataTable.NewRow();
                                            dr[0] = intSLNo;
                                            dr[1] = String.Empty;
                                            dr[2] = String.Empty;
                                            dr[3] = String.Empty;
                                            dr["FP Available"] = String.Empty;
                                            dr["FP Bytes"] = String.Empty;
                                            dr["Custom FP"] = String.Empty;
                                            dr["Custom FP + OSD"] = String.Empty;
                                            dr["128 FP"] =String.Empty;
                                            dr["128 FP + OSD"] = String.Empty;
                                            dr["OSD FP"] = String.Empty;                                                                                                                                                                                                                               
                                            dr["Manufacturer (3 char) code"] = String.Empty;
                                            dr["Manufacturer Hex Code"] = String.Empty;                                            
                                            dr["Monitor Name"] = String.Empty;                                          
                                            dr["Product Code"] = String.Empty;                                            
                                            dr["Serial number"] = String.Empty;
                                            dr["Custom FP Type0x00"] = String.Empty;
                                            dr["Custom FP Type0x01"] = String.Empty;
                                            dr["Custom FP Type0x02"] = String.Empty;
                                            dr["Custom FP Type0x03"] = String.Empty;
                                            dr["Week"] = String.Empty;                                           
                                            dr["Year"] = String.Empty;                                            
                                            dr["Horizontal Screen Size"] = String.Empty;
                                            dr["Vertical Screen Size"] = String.Empty;
                                            
                                            if (dtTable.Columns.Contains("Country"))
                                            {
                                                if (!String.IsNullOrEmpty(item["Country"].ToString()))
                                                {
                                                    dr[4] = item["Country"].ToString().Trim();
                                                }
                                            }
                                            if (dtTable.Columns.Contains("MainDevice"))
                                            {
                                                if (!String.IsNullOrEmpty(item["MainDevice"].ToString()))
                                                {
                                                    dr[1] = item["MainDevice"].ToString().Trim();
                                                }
                                            }
                                            if (dtTable.Columns.Contains("Make"))
                                            {
                                                if (!String.IsNullOrEmpty(item["Make"].ToString()))
                                                {
                                                    dr[5] = item["Make"].ToString().Trim();
                                                }
                                            }
                                            if (dtTable.Columns.Contains("Model"))
                                            {
                                                if (!String.IsNullOrEmpty(item["Model"].ToString()))
                                                {
                                                    dr[6] = item["Model"].ToString().Trim();
                                                }
                                            }
                                            if (dtTable.Columns.Contains("Codeset"))
                                            {
                                                if (!String.IsNullOrEmpty(item["Codeset"].ToString()))
                                                {
                                                    dr[7] = item["Codeset"].ToString().Trim();
                                                }
                                            }
                                            List<Byte> osdbyteData = null;
                                            if (dtTable.Columns.Contains("OSD"))
                                            {
                                                if (!String.IsNullOrEmpty(item["OSD"].ToString()))
                                                {
                                                    dr["OSD"] = item["OSD"].ToString();
                                                    osdbyteData = new List<Byte>();
                                                    String[] data = item["OSD"].ToString().Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                                    if (data.Length > 1)
                                                    {
                                                        foreach (String _bytedata in data)
                                                        {
                                                            osdbyteData.Add((Byte)Convert.ToInt32(_bytedata.Trim(), 16));
                                                        }
                                                    }                                                    
                                                }
                                            }
                                            if (dtTable.Columns.Contains("FP Available"))
                                            {
                                                if (!String.IsNullOrEmpty(item["FP Available"].ToString()))
                                                {
                                                    dr["FP Available"] = item["FP Available"].ToString();
                                                }
                                            }
                                            if (dtTable.Columns.Contains("FP Bytes"))
                                            {
                                                if (!String.IsNullOrEmpty(item["FP Bytes"].ToString()))
                                                {
                                                    dr["FP Bytes"] = item["FP Bytes"].ToString();
                                                }
                                            }
                                            if (dtTable.Columns.Contains("EDID"))
                                            {
                                                if (!String.IsNullOrEmpty(item["EDID"].ToString()))
                                                {
                                                    String data = item["EDID"].ToString().Trim();
                                                    if (data.EndsWith("|0") || data.EndsWith("|1"))
                                                        data = data.Remove(data.Length - 2, 2);
                                                    dr["EDID"] = data.Trim();
                                                    Byte[] _bindata = Convert.FromBase64String(data);
                                                    if (_bindata != null && _bindata.Length > 0)
                                                    {
                                                        EDIDInfo edidByteData = new EDIDInfo();
                                                        edidByteData.EdId = new List<byte>();
                                                        List<Byte> _tempedidData = new List<Byte>();
                                                        foreach (Byte byteData in _bindata)
                                                        {
                                                            _tempedidData.Add(byteData);
                                                        }
                                                        m_service = new EdIdServices();
                                                        Byte[] edidData = m_service.ConvertEdidToBin(_tempedidData);
                                                        edidByteData = m_service.ReadBinFile(edidData);
                                                        dr["EDID Block"] = edidByteData.EdIdRawData.Trim();
                                                        String strMake = String.Empty;
                                                        String strDevice = String.Empty;
                                                        if (!String.IsNullOrEmpty(item["Make"].ToString()))
                                                            strMake = item["Make"].ToString().Trim().ToLower();
                                                        if (!String.IsNullOrEmpty(item["MainDevice"].ToString()))
                                                            strDevice = item["MainDevice"].ToString().Trim().ToLower();

                                                        //
                                                        if (brandTranslattion == null)
                                                            brandTranslattion = m_service.LoadBrandTranslation();
                                                        if ((String.IsNullOrEmpty(strMake)) && (String.IsNullOrEmpty(strDevice)))
                                                        {
                                                            Byte[] m_fileStructure = m_service.CreateBinFile(edidByteData);
                                                            EDIDInfo m_edid = m_service.ReadBinFile(m_fileStructure);
                                                            BrandTranslationData _btData = brandTranslattion.GetBrand(strDevice, m_edid.ManufacturerCode);
                                                            if (_btData != null)
                                                            {
                                                                strDevice = _btData.MainDevice;
                                                                strMake = _btData.Brand;
                                                                dr["Main Device"] = _btData.MainDevice;
                                                                dr["Sub Device"] = _btData.SubDevice;
                                                                dr["Brand"] = _btData.Brand;
                                                            }
                                                        }
                                                        if (!String.IsNullOrEmpty(dr["FP Bytes"].ToString()))
                                                        {
                                                            dr["FP Available"] = "Y";
                                                            edidByteData.FPAvailable = "Y";
                                                            edidByteData.FPBytePosition = dr["FP Bytes"].ToString();
                                                        }
                                                        else
                                                        {
                                                            //Consider Standard Byte Position based on Brand to calculate Fingerprint
                                                            Dictionary<String, String> m_Position = m_service.GetBytePositionBasedonBrands(strDevice,strMake);
                                                            foreach (KeyValuePair<String, String> pair in m_Position)
                                                            {
                                                                dr["FP Available"] = pair.Key;
                                                                dr["FP Bytes"] = pair.Value;
                                                                edidByteData.FPAvailable = pair.Key;
                                                                edidByteData.FPBytePosition = pair.Value;
                                                            }
                                                        }
                                                        edidByteData.BytePositionforFingerPrint = new List<Int32>();                                                        
                                                        String[] values = edidByteData.FPBytePosition.Split(sep1, StringSplitOptions.RemoveEmptyEntries);
                                                        for (Int32 i = 0; i < values.Length; i++)
                                                        {
                                                            edidByteData.BytePositionforFingerPrint.Add(Int32.Parse(values[i]));
                                                        }

                                                        try
                                                        {
                                                            //if osd present consider it
                                                            edidByteData.OSD = osdbyteData;                                                                                                                        
                                                            m_service = new EdIdServices();
                                                            Byte[] m_fileStructure = m_service.CreateBinFile(edidByteData);
                                                            EDIDInfo m_edid = m_service.ReadBinFile(m_fileStructure);
                                                            edidByteData.CustomFP = m_edid.CustomFP;
                                                            if (edidByteData.OSD != null)
                                                            {
                                                                if (edidByteData.OSD.Count > 0)
                                                                    edidByteData.CustomFPOSD = m_edid.CustomFPOSD;
                                                            }

                                                            dr["Custom FP Type0x00"] = m_edid.CustomFPType0x00;
                                                            dr["Custom FP Type0x01"] = m_edid.CustomFPType0x01;
                                                            dr["Custom FP Type0x02"] = m_edid.CustomFPType0x02;
                                                            dr["Custom FP Type0x03"] = m_edid.CustomFPType0x03;
                                                            dr["Custom FP"] = m_edid.CustomFP;
                                                            dr["Custom FP + OSD"] = m_edid.CustomFPOSD;
                                                            dr["128 FP"] = m_edid.Only128FP;
                                                            dr["128 FP + OSD"] = m_edid.Only128FPOSD;
                                                            dr["OSD FP"] = m_edid.OnlyOSDFP;

                                                            dr["Manufacturer (3 char) code"] = m_edid.ManufacturerCode;
                                                            dr["Manufacturer Hex Code"] = m_edid.ManufacturerHexCode;
                                                            dr["Monitor Name"] = m_edid.ManufacturerName;
                                                            dr["Product Code"] = m_edid.ProductCode;
                                                            dr["Serial number"] = m_edid.SerialNumber;
                                                            dr["Week"] = m_edid.Week;
                                                            dr["Year"] = m_edid.Year;                                                            
                                                            dr["Video input definition"] = m_edid.VideoInputDefinition;
                                                            dr["Horizontal Screen Size"] = m_edid.HorizontalScreenSize;
                                                            dr["Vertical Screen Size"] = m_edid.VerticalScreenSize;
                                                            dr["CEC Physical Address"] = m_edid.PhysicalAddress;
                                                        }
                                                        catch
                                                        {
                                                            
                                                        }                                                       
                                                    }
                                                }
                                            }
                                            if (dr != null)
                                                objDataTable.Rows.Add(dr);
                                            dr = null;
                                            intSLNo++;
                                        }
                                    }
                                    if (objDataSet == null)
                                    {
                                        objDataSet = new DataSet("EDID Summary");
                                    }
                                    if (objDataSet.Tables.Count > 0)
                                    {
                                        if (objDataSet.Tables.Contains(objDataTable.TableName))
                                            objDataSet.Tables.Remove(objDataTable.TableName);
                                    }
                                    objDataSet.Tables.Add(objDataTable);
                                }                                
                            }
                            String m_SaveFileMessage = String.Empty;
                            try
                            {
                                String m_filePath = CommonForms.Configuration.GetWorkingDirectory();
                                ExcelReportGenerator.GenerateReport(objDataSet, m_filePath, "EDID Summary", out m_SaveFileMessage);
                                if (!String.IsNullOrEmpty(m_SaveFileMessage))
                                {
                                    MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            objDataSet = null;
            BrandTranslationCollection brandTranslattion = null;
            String[] sep = { " " };
            String[] sep1 = { "," };
            try
            {
                OpenFileDialog openfile = new OpenFileDialog();
                openfile.Title = "Select EDID Excel File";
                openfile.Filter = "Excel File (*.xls,*.xlsx)|*.xls;*.xlsx";

                if (openfile.ShowDialog() == DialogResult.OK)
                {
                    FileInfo m_File = new FileInfo(openfile.FileName);
                    if (m_File.Extension.ToLower() == ".xls" || m_File.Extension.ToLower() == ".xlsx")
                    {
                        OleDbConnection m_ExcelConnection = null;
                        if (m_File.Extension.ToLower() == ".xls")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + openfile.FileName + ";Extended Properties=Excel 8.0;");
                        else if (m_File.Extension.ToLower() == ".xlsx")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + openfile.FileName + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");

                        m_ExcelConnection.Open();
                        DataTable dtExcelSheets = m_ExcelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                        m_ExcelConnection.Close();
                        if (dtExcelSheets != null)
                        {
                            foreach (DataRow drExcelSheet in dtExcelSheets.Rows)
                            {
                                OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [" + drExcelSheet["TABLE_NAME"].ToString() + "]", m_ExcelConnection);
                                DataTable dtTable = new DataTable();
                                m_ExcelAdapter.Fill(dtTable);
                                if (dtTable.Rows.Count > 0)
                                {
                                    DataTable objDataTable = new DataTable(drExcelSheet["TABLE_NAME"].ToString().Replace("$", ""));
                                    objDataTable.Columns.Add("SL NO");
                                    objDataTable.Columns.Add("Main Device");
                                    objDataTable.Columns.Add("Sub Device");
                                    objDataTable.Columns.Add("Component");
                                    objDataTable.Columns.Add("Region");
                                    objDataTable.Columns.Add("Brand");
                                    objDataTable.Columns.Add("Model");
                                    objDataTable.Columns.Add("Codeset");
                                    objDataTable.Columns.Add("EDID");
                                    objDataTable.Columns.Add("EDID Block");
                                    objDataTable.Columns.Add("Manufacturer (3 char) code");
                                    objDataTable.Columns.Add("Manufacturer Hex Code");
                                    objDataTable.Columns.Add("Monitor Name");                              
                                    objDataTable.Columns.Add("Product Code");
                                    objDataTable.Columns.Add("Serial number");
                                    objDataTable.Columns.Add("Week");
                                    objDataTable.Columns.Add("Year");
                                    objDataTable.Columns.Add("Video input definition");
                                    objDataTable.Columns.Add("Horizontal Screen Size");
                                    objDataTable.Columns.Add("Vertical Screen Size");
                                    objDataTable.Columns.Add("OSD");
                                    objDataTable.Columns.Add("FP Available");
                                    objDataTable.Columns.Add("FP Bytes");
                                    objDataTable.Columns.Add("Custom FP Type0x00");
                                    objDataTable.Columns.Add("Custom FP Type0x01");
                                    objDataTable.Columns.Add("Custom FP Type0x02");
                                    objDataTable.Columns.Add("Custom FP Type0x03");
                                    objDataTable.Columns.Add("Custom FP");
                                    objDataTable.Columns.Add("Custom FP + OSD");
                                    objDataTable.Columns.Add("128 FP");
                                    objDataTable.Columns.Add("128 FP + OSD");
                                    objDataTable.Columns.Add("OSD FP");
                                    objDataTable.Columns.Add("CEC Physical Address");

                                    objDataTable.AcceptChanges();
                                    Int32 intSLNo = 1;
                                    foreach (DataRow item in dtTable.Rows)
                                    {
                                        if (!CheckEmptyRow(item))
                                        {
                                            DataRow dr = null;
                                            EdIdServices m_service = null;
                                            dr = objDataTable.NewRow();
                                            dr[0] = intSLNo;
                                            dr[1] = String.Empty;
                                            dr[2] = String.Empty;
                                            dr[3] = String.Empty;
                                            dr["FP Available"] = String.Empty;
                                            dr["FP Bytes"] = String.Empty;
                                            dr["Custom FP"] = String.Empty;
                                            dr["Custom FP + OSD"] = String.Empty;
                                            dr["128 FP"] = String.Empty;
                                            dr["128 FP + OSD"] = String.Empty;
                                            dr["OSD FP"] = String.Empty;
                                            dr["Manufacturer (3 char) code"] = String.Empty;
                                            dr["Manufacturer Hex Code"] = String.Empty;
                                            dr["Monitor Name"] = String.Empty;
                                            dr["Product Code"] = String.Empty;
                                            dr["Serial number"] = String.Empty;
                                            dr["Week"] = String.Empty;
                                            dr["Year"] = String.Empty;
                                            dr["Horizontal Screen Size"] = String.Empty;
                                            dr["Vertical Screen Size"] = String.Empty;
                                            dr["Custom FP Type0x00"] = String.Empty;
                                            dr["Custom FP Type0x01"] = String.Empty;
                                            dr["Custom FP Type0x02"] = String.Empty;
                                            dr["Custom FP Type0x03"] = String.Empty;
                                            if (dtTable.Columns.Contains("Country"))
                                            {
                                                if (!String.IsNullOrEmpty(item["Country"].ToString()))
                                                {
                                                    dr[4] = item["Country"].ToString().Trim();
                                                }
                                            }
                                            if (dtTable.Columns.Contains("MainDevice"))
                                            {
                                                if (!String.IsNullOrEmpty(item["MainDevice"].ToString()))
                                                {
                                                    dr[1] = item["MainDevice"].ToString().Trim();
                                                }
                                            }
                                            if (dtTable.Columns.Contains("Make"))
                                            {
                                                if (!String.IsNullOrEmpty(item["Make"].ToString()))
                                                {
                                                    dr[5] = item["Make"].ToString().Trim();
                                                }
                                            }
                                            if (dtTable.Columns.Contains("Model"))
                                            {
                                                if (!String.IsNullOrEmpty(item["Model"].ToString()))
                                                {
                                                    dr[6] = item["Model"].ToString().Trim();
                                                }
                                            }
                                            if (dtTable.Columns.Contains("Codeset"))
                                            {
                                                if (!String.IsNullOrEmpty(item["Codeset"].ToString()))
                                                {
                                                    dr[7] = item["Codeset"].ToString().Trim();
                                                }
                                            }
                                            List<Byte> osdbyteData = null;
                                            if (dtTable.Columns.Contains("OSD"))
                                            {
                                                if (!String.IsNullOrEmpty(item["OSD"].ToString()))
                                                {
                                                    dr["OSD"] = item["OSD"].ToString();
                                                    osdbyteData = new List<Byte>();
                                                    String[] data = item["OSD"].ToString().Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                                    if (data.Length > 1)
                                                    {
                                                        foreach (String _bytedata in data)
                                                        {
                                                            osdbyteData.Add((Byte)Convert.ToInt32(_bytedata.Trim(), 16));
                                                        }
                                                    }
                                                }
                                            }
                                            if (dtTable.Columns.Contains("FP Available"))
                                            {
                                                if (!String.IsNullOrEmpty(item["FP Available"].ToString()))
                                                {
                                                    dr["FP Available"] = item["FP Available"].ToString();
                                                }
                                            }
                                            if (dtTable.Columns.Contains("FP Bytes"))
                                            {
                                                if (!String.IsNullOrEmpty(item["FP Bytes"].ToString()))
                                                {
                                                    dr["FP Bytes"] = item["FP Bytes"].ToString();
                                                }
                                            }
                                            if (dtTable.Columns.Contains("EDID"))
                                            {
                                                if (!String.IsNullOrEmpty(item["EDID"].ToString()))
                                                {
                                                    String data = item["EDID"].ToString().Trim();
                                                    if (data.EndsWith("\n"))
                                                    {
                                                        data = data.Replace("\n", "");
                                                    }                                                                                                        
                                                    List<Byte> _bindata = null;

                                                    String[] datas = data.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                                    
                                                    if (datas.Length > 1)
                                                    {
                                                        _bindata = new List<Byte>();
                                                        foreach (String bytedata in datas)
                                                        {
                                                            _bindata.Add((Byte)Convert.ToInt16(bytedata.Trim(), 16));
                                                        }
                                                    }
                                                    else if (data.Length > 0)
                                                    {
                                                        _bindata = new List<Byte>();                                                        
                                                        while (!String.IsNullOrEmpty(data))
                                                        {
                                                            String subData = data.Substring(0, 2);
                                                            data = data.Remove(0, 2);
                                                            _bindata.Add((Byte)Convert.ToInt16(subData.Trim(), 16));                                                           
                                                        }                                                                                                            
                                                    }
                                                    if (_bindata != null && _bindata.Count > 0)
                                                    {
                                                        EDIDInfo edidByteData = new EDIDInfo();
                                                        edidByteData.EdId = new List<byte>();                                                        
                                                        m_service = new EdIdServices();
                                                        Byte[] edidData = m_service.ConvertEdidToBin(_bindata);
                                                        edidByteData = m_service.ReadBinFile(edidData);
                                                        dr["EDID Block"] = edidByteData.EdIdRawData;

                                                        String strMake = String.Empty;
                                                        String strDevice = String.Empty;
                                                        if (!String.IsNullOrEmpty(item["Make"].ToString()))
                                                            strMake = item["Make"].ToString().Trim().ToLower();
                                                        if (!String.IsNullOrEmpty(item["MainDevice"].ToString()))
                                                            strDevice = item["MainDevice"].ToString().Trim().ToLower();

                                                        if (brandTranslattion == null)
                                                            brandTranslattion = m_service.LoadBrandTranslation();
                                                        if ((String.IsNullOrEmpty(strMake)) && (String.IsNullOrEmpty(strDevice)))
                                                        {
                                                            Byte[] m_fileStructure = m_service.CreateBinFile(edidByteData);
                                                            EDIDInfo m_edid = m_service.ReadBinFile(m_fileStructure);
                                                            BrandTranslationData _btData = brandTranslattion.GetBrand(strDevice, m_edid.ManufacturerCode);
                                                            if (_btData != null)
                                                            {
                                                                strDevice = _btData.MainDevice;
                                                                strMake = _btData.Brand;
                                                                dr["Main Device"] = _btData.MainDevice;
                                                                dr["Sub Device"] = _btData.SubDevice;
                                                                dr["Brand"] = _btData.Brand;
                                                            }
                                                        }

                                                        if (!String.IsNullOrEmpty(dr["FP Bytes"].ToString()))
                                                        {
                                                            dr["FP Available"] = "Y";
                                                            edidByteData.FPAvailable = "Y";
                                                            edidByteData.FPBytePosition = dr["FP Bytes"].ToString();
                                                        }
                                                        else
                                                        {
                                                            //Consider Standard Byte Position based on Brand to calculate Fingerprint
                                                            Dictionary<String, String> m_Position = m_service.GetBytePositionBasedonBrands(strDevice,strMake);
                                                            foreach (KeyValuePair<String, String> pair in m_Position)
                                                            {
                                                                dr["FP Available"] = pair.Key;
                                                                dr["FP Bytes"] = pair.Value;
                                                                edidByteData.FPAvailable = pair.Key;
                                                                edidByteData.FPBytePosition = pair.Value;
                                                            }
                                                        }
                                                        edidByteData.BytePositionforFingerPrint = new List<Int32>();
                                                        String[] values = edidByteData.FPBytePosition.Split(sep1, StringSplitOptions.RemoveEmptyEntries);
                                                        for (Int32 i = 0; i < values.Length; i++)
                                                        {
                                                            edidByteData.BytePositionforFingerPrint.Add(Int32.Parse(values[i]));
                                                        }

                                                        try
                                                        {
                                                            //if osd present consider it
                                                            edidByteData.OSD = osdbyteData;
                                                            m_service = new EdIdServices();
                                                            Byte[] m_fileStructure = m_service.CreateBinFile(edidByteData);
                                                            EDIDInfo m_edid = m_service.ReadBinFile(m_fileStructure);
                                                            edidByteData.CustomFP = m_edid.CustomFP;
                                                            if (edidByteData.OSD != null)
                                                            {
                                                                if (edidByteData.OSD.Count > 0)
                                                                    edidByteData.CustomFPOSD = m_edid.CustomFPOSD;
                                                            }

                                                            dr["Custom FP Type0x00"] = m_edid.CustomFPType0x00;
                                                            dr["Custom FP Type0x01"] = m_edid.CustomFPType0x01;
                                                            dr["Custom FP Type0x02"] = m_edid.CustomFPType0x02;
                                                            dr["Custom FP Type0x03"] = m_edid.CustomFPType0x03;

                                                            dr["Custom FP"] = m_edid.CustomFP;
                                                            dr["Custom FP + OSD"] = m_edid.CustomFPOSD;
                                                            dr["128 FP"] = m_edid.Only128FP;
                                                            dr["128 FP + OSD"] = m_edid.Only128FPOSD;
                                                            dr["OSD FP"] = m_edid.OnlyOSDFP;

                                                            dr["Manufacturer (3 char) code"] = m_edid.ManufacturerCode;
                                                            dr["Manufacturer Hex Code"] = m_edid.ManufacturerHexCode;
                                                            dr["Monitor Name"] = m_edid.ManufacturerName;
                                                            dr["Product Code"] = m_edid.ProductCode;
                                                            dr["Serial number"] = m_edid.SerialNumber;
                                                            dr["Week"] = m_edid.Week;
                                                            dr["Year"] = m_edid.Year;
                                                            dr["Video input definition"] = m_edid.VideoInputDefinition;
                                                            dr["Horizontal Screen Size"] = m_edid.HorizontalScreenSize;
                                                            dr["Vertical Screen Size"] = m_edid.VerticalScreenSize;
                                                            dr["CEC Physical Address"] = m_edid.PhysicalAddress;
                                                        }
                                                        catch
                                                        {                                                           
                                                        }                                                     
                                                    }
                                                }
                                            }
                                            if (dr != null)
                                                objDataTable.Rows.Add(dr);
                                            dr = null;
                                            intSLNo++;
                                        }
                                    }
                                    if (objDataSet == null)
                                    {
                                        objDataSet = new DataSet("EDID Summary");
                                    }
                                    if (objDataSet.Tables.Count > 0)
                                    {
                                        if (objDataSet.Tables.Contains(objDataTable.TableName))
                                            objDataSet.Tables.Remove(objDataTable.TableName);
                                    }
                                    objDataSet.Tables.Add(objDataTable);
                                }
                            }
                            String m_SaveFileMessage = String.Empty;
                            try
                            {
                                String m_filePath = CommonForms.Configuration.GetWorkingDirectory();
                                ExcelReportGenerator.GenerateReport(objDataSet, m_filePath, "EDID Summary", out m_SaveFileMessage);
                                if (!String.IsNullOrEmpty(m_SaveFileMessage))
                                {
                                    MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Progress
        private void ResetTimer()
        {
            if (m_BackgroundWorker != null || m_BackgroundWorker.IsBusy == false)
                m_BackgroundWorker.RunWorkerAsync();

            if (m_Progress == null)
                m_Progress = new ProgressForm();
            m_Progress.SetMessage(StatusMessage, StatusMessage1);
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();
            m_Timer.Start();
        }
        void m_Timer_Tick(object sender, EventArgs e)
        {
            if (this.m_Progress != null)
            {
                this.m_Progress.SetMessage(StatusMessage, StatusMessage1);
            }
        }
        void m_BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.m_Timer.Stop();
            this.m_Progress.DialogResult = DialogResult.OK;
            this.m_Progress.Close();

            if (m_Service.ErrorMessage != String.Empty)
            {
                MessageBox.Show(m_Service.ErrorMessage, "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                ExportToExcel();
            }
        }
        void m_BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            this.m_Service = new EdIdServices(BusinessObject.DBConnectionString.EdId);
            if (!String.IsNullOrEmpty(this.SelectedEdIdExcelFile))
                LoadEdIdFromExcelFile();
            else
                this.EdIdData = this.m_Service.GetEDIDGroupDataWithConsoleCountNewFPRule(this.SelectedDeviceType, this.SelectedBrand, this.SelectedModels, this.SelectedDataSources, this.SelectedCountries, ref this.m_StatusMessage1);
            if (this.EdIdData != null)
            {
                if (this.EdIdData.Count > 0)
                    this.EdIdGrouped = this.m_Service.GetGroupEdId(this.EdIdData, ref this.m_StatusMessage, ref  this.m_StatusMessage1);
                try
                {
                    LoadLinkedID();
                }
                catch { }
                if (this.EdIdGrouped != null)
                    this.ClusteredEdIdGroups = this.m_Service.SetLinkedIdInGroupedEdId(this.EdIdGrouped, this.LinkedIds, FPType, ref this.m_StatusMessage, ref  this.m_StatusMessage1);
                if (this.ClusteredEdIdGroups != null)
                    this.ClusteredEdIdGroups = this.m_Service.SetMappedIdInGroupedEdId(this.ClusteredEdIdGroups, ref this.m_StatusMessage, ref  this.m_StatusMessage1);
                else
                    this.m_Service.ErrorMessage = "No Data Found";
            }
        }
        #endregion

        #region Export To Excel
        void ExportToExcel()
        {
            String[] sep1 = { "," };
            if (this.EdIdGrouped != null)
            {
                if (this.ClusteredEdIdGroups.Count > 0)
                {
                    DataTable dtResult = new DataTable("Grouped EDID");
                    dtResult.Columns.Add("Group");
                    dtResult.Columns.Add("Sub Group");
                    dtResult.Columns.Add("EDID Block");
                    dtResult.Columns.Add("Custom FP Type0x00");
                    dtResult.Columns.Add("Custom FP Type0x01");
                    dtResult.Columns.Add("Custom FP Type0x02");
                    dtResult.Columns.Add("Custom FP Type0x03");
                    dtResult.Columns.Add("Custom FP");
                    dtResult.Columns.Add("128 FP");
                    dtResult.Columns.Add("Brand");
                    dtResult.Columns.Add("User Entered Model");
                    dtResult.Columns.Add("User Entered Codeset");
                    dtResult.Columns.Add("Manufacturer (3 char) Code");
                    dtResult.Columns.Add("Monitor Name");
                    dtResult.Columns.Add("Product Code");
                    dtResult.Columns.Add("Year");
                    dtResult.Columns.Add("Week");
                    dtResult.Columns.Add("Horizontal Screen Size");
                    dtResult.Columns.Add("Vertical Screen Size");
                    dtResult.Columns.Add("Console Count", Type.GetType("System.Int64"));
                    dtResult.Columns.Add("Suggested ID");                    
                    dtResult.Columns.Add("Trust Level");
                    dtResult.Columns.Add("Build Rule");
                    dtResult.AcceptChanges();

                    //Generate Report for all Cluster Groups
                    if (this.ClusteredEdIdGroups != null)
                    {
                        foreach (String clusteredGroup in this.ClusteredEdIdGroups.Keys)
                        {
                            Int32 intRowCount = 1;
                            foreach (String itemKey in this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups.Keys)
                            {
                                if (this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups[itemKey] != null && itemKey.Contains("PCGroup_"))
                                {
                                    this.m_StatusMessage = "Generating group report " + intRowCount.ToString() + " of " + this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups.Keys.Count.ToString();
                                    intRowCount = intRowCount + 1;
                                    FillDataTableForReport(ref dtResult, itemKey, clusteredGroup);
                                }
                            }
                        }
                    }
                    if (objDataSet == null)
                    {
                        objDataSet = new DataSet("Grouped EDID Summary");
                    }
                    if (objDataSet.Tables.Count > 0)
                    {
                        if (objDataSet.Tables.Contains(dtResult.TableName))
                            objDataSet.Tables.Remove(dtResult.TableName);
                    }
                    objDataSet.Tables.Add(dtResult);
                    if (this.objDataSet != null)
                    {
                        Export("Grouped EDID Summary");
                    }
                }
            }
        }
        void FillDataTableForReport(ref DataTable dtResult, String itemKey, String clusteredGroup)
        {
            if (this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups[itemKey] != null)
            {
                this.m_StatusMessage1 = "Generating report for Group: " + itemKey;
                DataRow drGroup = null;
                drGroup = dtResult.NewRow();
                drGroup["Group"] = itemKey;
                drGroup["Sub Group"] = String.Empty;
                drGroup["EDID Block"] = String.Empty;
                drGroup["Custom FP"] = String.Empty;
                drGroup["128 FP"] = String.Empty;
                drGroup["Brand"] = String.Empty;
                drGroup["User Entered Model"] = String.Empty;
                drGroup["User Entered Codeset"] = String.Empty;
                drGroup["Product Code"] = String.Empty;
                drGroup["Year"] = String.Empty;
                drGroup["Week"] = String.Empty;
                drGroup["Suggested ID"] = String.Empty;                
                drGroup["Trust Level"] = String.Empty;
                drGroup["Build Rule"] = String.Empty;
                drGroup["Manufacturer (3 char) Code"] = String.Empty;
                drGroup["Monitor Name"] = String.Empty;
                drGroup["Horizontal Screen Size"] = String.Empty;
                drGroup["Vertical Screen Size"] = String.Empty;

                drGroup["Custom FP Type0x00"] = String.Empty;
                drGroup["Custom FP Type0x01"] = String.Empty;
                drGroup["Custom FP Type0x02"] = String.Empty;
                drGroup["Custom FP Type0x03"] = String.Empty;

                dtResult.Rows.Add(drGroup);

                Int64 totalConsoleCount = 0;
                Int32 intSubGroupCount = 1;
                foreach (String groupItem in this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups[itemKey].Keys)
                {
                    if (this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups[itemKey][groupItem] != null)
                    {
                        if (this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups[itemKey][groupItem].Count > 0)
                        {
                            Int64 totalFPConsoleCount = 0;
                            String strSubGroupName = itemKey + "." + intSubGroupCount.ToString();
                            intSubGroupCount = intSubGroupCount + 1;
                            DataRow drSubGroup = null;
                            drSubGroup = dtResult.NewRow();
                            drSubGroup["Group"] = String.Empty;
                            drSubGroup["Sub Group"] = strSubGroupName;
                            drSubGroup["Custom FP"] = String.Empty;
                            drSubGroup["128 FP"] = String.Empty;
                            drSubGroup["Brand"] = String.Empty;
                            drSubGroup["EDID Block"] = String.Empty;
                            drSubGroup["User Entered Model"] = String.Empty;
                            drSubGroup["User Entered Codeset"] = String.Empty;
                            drSubGroup["Product Code"] = String.Empty;
                            drSubGroup["Year"] = String.Empty;
                            drSubGroup["Week"] = String.Empty;
                            drSubGroup["Suggested ID"] = String.Empty;                            
                            drSubGroup["Trust Level"] = String.Empty;
                            drSubGroup["Build Rule"] = String.Empty;
                            drSubGroup["Manufacturer (3 char) Code"] = String.Empty;
                            drSubGroup["Monitor Name"] = String.Empty;
                            drSubGroup["Horizontal Screen Size"] = String.Empty;
                            drSubGroup["Vertical Screen Size"] = String.Empty;
                            drSubGroup["Custom FP Type0x00"] = String.Empty;
                            drSubGroup["Custom FP Type0x01"] = String.Empty;
                            drSubGroup["Custom FP Type0x02"] = String.Empty;
                            drSubGroup["Custom FP Type0x03"] = String.Empty;

                            dtResult.Rows.Add(drSubGroup);
                            foreach (EDIDGroupData itemEdId in this.ClusteredEdIdGroups[clusteredGroup].EdIdGroups[itemKey][groupItem])
                            {
                                DataRow dr1 = null;
                                dr1 = dtResult.NewRow();
                                dr1["Group"] = String.Empty;
                                dr1["Custom FP"] = itemEdId.CustomFP;
                                dr1["EDID Block"] = itemEdId.EDIDBlock;
                                dr1["128 FP"] = itemEdId.Only128FP;
                                dr1["Brand"] = itemEdId.Brand;
                                dr1["User Entered Model"] = itemEdId.UserEnteredModel;
                                dr1["User Entered Codeset"] = itemEdId.UserEnteredCodeset;
                                dr1["Product Code"] = itemEdId.ProductCode;
                                dr1["Year"] = itemEdId.ModelYear;
                                dr1["Week"] = itemEdId.ModelWeek;
                                dr1["Console Count"] = itemEdId.CounsoleCount;
                                dr1["Suggested ID"] = itemEdId.SuggestedID;                                
                                dr1["Trust Level"] = itemEdId.TrustLevel;
                                dr1["Build Rule"] = itemEdId.BuildRule;
                                dr1["Manufacturer (3 char) Code"] = itemEdId.ManufacturerName;
                                dr1["Monitor Name"] = itemEdId.MonitorName;
                                dr1["Horizontal Screen Size"] = itemEdId.HorizontalScreenSize;
                                dr1["Vertical Screen Size"] = itemEdId.VerticalScreenSize;
                                dr1["Custom FP Type0x00"] = itemEdId.CustomFPType0x00;
                                dr1["Custom FP Type0x01"] = itemEdId.CustomFPType0x01;
                                dr1["Custom FP Type0x02"] = itemEdId.CustomFPType0x02;
                                dr1["Custom FP Type0x03"] = itemEdId.CustomFPType0x03;
                                dtResult.Rows.Add(dr1);
                                totalConsoleCount += itemEdId.CounsoleCount;
                                totalFPConsoleCount += itemEdId.CounsoleCount;
                                dr1 = null;
                            }
                            if (totalFPConsoleCount == 0)
                                drSubGroup["Console Count"] = String.Empty;
                            else
                                drSubGroup["Console Count"] = totalFPConsoleCount.ToString();
                            drSubGroup = null;
                        }
                    }
                }
                if (totalConsoleCount == 0)
                    drGroup["Console Count"] = String.Empty;
                else
                    drGroup["Console Count"] = totalConsoleCount.ToString();
                drGroup = null;
            }
            dtResult.AcceptChanges();
        }
        void Export(String reportname)
        {
            String m_SaveFileMessage = String.Empty;
            try
            {
                String m_filePath = CommonForms.Configuration.GetWorkingDirectory() + "\\";
                if (String.IsNullOrEmpty(m_filePath))
                {
                    FolderBrowserDialog dlg1 = new FolderBrowserDialog();
                    if (dlg1.ShowDialog() == DialogResult.OK)
                    {
                        if (dlg1.SelectedPath.EndsWith("\\") == false)
                        {
                            dlg1.SelectedPath += "\\";
                        }
                        m_filePath = dlg1.SelectedPath;
                    }
                }

                ExcelReportGenerator.GenerateReport(objDataSet, m_filePath, reportname, out m_SaveFileMessage);
                if (!String.IsNullOrEmpty(m_SaveFileMessage))
                {
                    MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        #endregion

        #region Load Linked ID
        void LoadLinkedID()
        {
            try
            {
                this.m_StatusMessage = "Please Wait...";
                this.m_StatusMessage1 = "Loading Linked Id from direct capture edid";
                this.LinkedIds = this.m_Service.LoadLinkedID();
            }
            catch { }
        }
        #endregion

        #region Load EDID from Excel File
        void LoadEdIdFromExcelFile()
        {
            this.m_Service.ErrorMessage = String.Empty;
            if (!String.IsNullOrEmpty(this.SelectedEdIdExcelFile) && File.Exists(this.SelectedEdIdExcelFile))
            {
                String[] sep = { " " };
                String[] sep1 = { "," };
                try
                {
                    FileInfo m_File = new FileInfo(this.SelectedEdIdExcelFile);
                    if (m_File.Extension.ToLower() == ".xls" || m_File.Extension.ToLower() == ".xlsx")
                    {
                        OleDbConnection m_ExcelConnection = null;
                        if (m_File.Extension.ToLower() == ".xls")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + this.SelectedEdIdExcelFile + ";Extended Properties=Excel 8.0;");
                        else if (m_File.Extension.ToLower() == ".xlsx")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.SelectedEdIdExcelFile + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");

                        m_ExcelConnection.Open();
                        DataTable dtExcelSheets = m_ExcelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                        m_ExcelConnection.Close();
                        if (dtExcelSheets != null)
                        {
                            foreach (DataRow drExcelSheet in dtExcelSheets.Rows)
                            {
                                OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [" + drExcelSheet["TABLE_NAME"].ToString() + "]", m_ExcelConnection);
                                DataTable dtTable = new DataTable();
                                m_ExcelAdapter.Fill(dtTable);
                                if (dtTable.Rows.Count > 0)
                                {
                                    this.EdIdData = new List<EDIDGroupData>();
                                    Int32 intSLNo = 1;
                                    foreach (DataRow item in dtTable.Rows)
                                    {
                                        try
                                        {
                                            EDIDGroupData newedidgroupdata = new EDIDGroupData();
                                            if (!CheckEmptyRow(item))
                                            {
                                                newedidgroupdata.SlNo = intSLNo;
                                                if (dtTable.Columns.Contains("Brand"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Brand"].ToString()))
                                                    {
                                                        newedidgroupdata.Brand = item["Brand"].ToString().Trim();
                                                    }
                                                }

                                                if (dtTable.Columns.Contains("Custom FP Type0x00"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Custom FP Type0x00"].ToString()))
                                                    {
                                                        newedidgroupdata.CustomFPType0x00 = item["Custom FP Type0x00"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Custom FP Type0x01"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Custom FP Type0x01"].ToString()))
                                                    {
                                                        newedidgroupdata.CustomFPType0x01 = item["Custom FP Type0x01"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Custom FP Type0x02"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Custom FP Type0x02"].ToString()))
                                                    {
                                                        newedidgroupdata.CustomFPType0x02 = item["Custom FP Type0x02"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Custom FP Type0x03"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Custom FP Type0x03"].ToString()))
                                                    {
                                                        newedidgroupdata.CustomFPType0x03 = item["Custom FP Type0x03"].ToString().Trim();
                                                    }
                                                }

                                                if (dtTable.Columns.Contains("Custom FP"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Custom FP"].ToString()))
                                                    {
                                                        newedidgroupdata.CustomFP = item["Custom FP"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("EDID Block"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["EDID Block"].ToString()))
                                                    {
                                                        newedidgroupdata.EDIDBlock = item["EDID Block"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Horizontal Screen Size"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Horizontal Screen Size"].ToString()))
                                                    {
                                                        newedidgroupdata.HorizontalScreenSize = Int32.Parse(item["Horizontal Screen Size"].ToString().Trim());
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Vertical Screen Size"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Vertical Screen Size"].ToString()))
                                                    {
                                                        newedidgroupdata.VerticalScreenSize = Int32.Parse(item["Vertical Screen Size"].ToString().Trim());
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Manufacturer (3 char) code"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Manufacturer (3 char) code"].ToString()))
                                                    {
                                                        newedidgroupdata.ManufacturerName = item["Manufacturer (3 char) code"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Week"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Week"].ToString()))
                                                    {
                                                        newedidgroupdata.ModelWeek = Int32.Parse(item["Week"].ToString().Trim());
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Year"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Year"].ToString()))
                                                    {
                                                        newedidgroupdata.ModelYear = Int32.Parse(item["Year"].ToString().Trim());
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Monitor Name"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Monitor Name"].ToString()))
                                                    {
                                                        newedidgroupdata.MonitorName = item["Monitor Name"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("128 FP"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["128 FP"].ToString()))
                                                    {
                                                        newedidgroupdata.Only128FP = item["128 FP"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Product Code"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Product Code"].ToString()))
                                                    {
                                                        newedidgroupdata.ProductCode = item["Product Code"].ToString().Trim();
                                                    }
                                                }
                                                if (dtTable.Columns.Contains("Console Count"))
                                                {
                                                    if (!String.IsNullOrEmpty(item["Console Count"].ToString()))
                                                    {
                                                        newedidgroupdata.CounsoleCount = Int64.Parse(item["Console Count"].ToString().Trim());
                                                    }
                                                }
                                                //newedidgroupdata.CounsoleCount = 101;
                                                this.EdIdData.Add(newedidgroupdata);
                                                newedidgroupdata = null;
                                                intSLNo++;
                                            }
                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }        
        #endregion
    }
}