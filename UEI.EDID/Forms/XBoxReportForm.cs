﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report.ExportToExcel;

namespace UEI.EDID.Forms
{
    public partial class XBoxReportForm : Form
    {
        #region Variables
        private DataSet objDataSet = null;
        private EdIdServices m_Service = null;
        private XBoxReportFilterForm m_ReportFilterForm = null;
        private ProgressForm m_Progress = null;
        private Timer m_Timer = null;
        private BackgroundWorker m_BackgroundWorker = null;
        #endregion

        #region Properties
        private String m_StatusMessage = "Please Wait...";
        public String StatusMessage
        {
            get { return m_StatusMessage; }
            set { m_StatusMessage = value; }
        }
        private String m_StatusMessage1 = String.Empty;
        public String StatusMessage1
        {
            get { return m_StatusMessage1; }
            set { m_StatusMessage1 = value; }
        }
        public List<String>     SelectedDeviceType { get; set; }
        public List<String>     SelectedBrand { get; set; }
        public List<String>     SelectedCountries { get; set; }
        public List<String>     SelectedDataSources { get; set; }
        public List<String>     SelectedModels { get; set; }
        public List<EDIDInfo>   EdIdData { get; set; }
        public Boolean          NewFPRule { get; set; }
        public FingerPrintTypes FPType { get; set; }
        #endregion

        #region Constructor
        public XBoxReportForm()
        {
            InitializeComponent();
            this.m_BackgroundWorker = new BackgroundWorker();
            this.m_BackgroundWorker.DoWork += m_BackgroundWorker_DoWork;
            this.m_BackgroundWorker.RunWorkerCompleted += m_BackgroundWorker_RunWorkerCompleted;
            this.m_Timer = new System.Windows.Forms.Timer();
            this.m_Timer.Enabled = true;
            this.m_Timer.Interval = 100;
            this.m_Timer.Tick += m_Timer_Tick;
            this.m_Service = new EdIdServices(DBConnectionString.EdId);
            this.Load += XBoxReportForm_Load;
            this.btnExcel.Click += btnExcel_Click;
        }
        void btnExcel_Click(object sender, EventArgs e)
        {
            if (this.objDataSet != null)
            {
                Export();
            }
        }
        void XBoxReportForm_Load(object sender, EventArgs e)
        {
            this.objDataSet = null;
            this.m_ReportFilterForm = new XBoxReportFilterForm();
            this.m_ReportFilterForm.ReportName = "XBoxReport";
            this.m_ReportFilterForm.ShowDialog();
            if (this.m_ReportFilterForm.DialogResult == DialogResult.OK)
            {
                this.SelectedDeviceType     = this.m_ReportFilterForm.SelectedDeviceType;
                this.SelectedBrand          = this.m_ReportFilterForm.SelectedBrand;
                this.SelectedCountries      = this.m_ReportFilterForm.SelectedCountries;
                this.SelectedDataSources    = this.m_ReportFilterForm.SelectedDataSources;
                this.SelectedModels         = this.m_ReportFilterForm.SelectedModel;
                this.NewFPRule              = this.m_ReportFilterForm.NewFPRule;
                this.FPType                 = this.m_ReportFilterForm.FPType;
                this.m_ReportFilterForm     = null;
                ResetTimer();
                if (this.objDataSet != null)
                {
                    this.btnExcel.Enabled = true;
                    this.dataGridView.DataSource = this.objDataSet.Tables[0];
                    foreach (DataGridViewColumn column in this.dataGridView.Columns)
                    {
                        column.SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
        }
        #endregion

        #region Export To Excel
        void ExportToExcel()
        {
            EdIdServices m_Service1 = new EdIdServices(DBConnectionString.UEIPUBLIC);
            EdIdServices m_Service2 = new EdIdServices(DBConnectionString.QuickSet);
            FPIDLinkCollection LinkedIds = null;
            try
            {
                LinkedIds = this.m_Service.LoadLinkedID();
            }
            catch { }
            String[] sep1 = { "," };
            if (this.EdIdData != null)
            {
                if (this.EdIdData.Count > 0)
                {
                    DataTable objDataTable = new DataTable("EDID Summary");
                    objDataTable.Columns.Add("SL NO");
                    objDataTable.Columns.Add("Main Device");
                    objDataTable.Columns.Add("Sub Device");
                    objDataTable.Columns.Add("Component");
                    objDataTable.Columns.Add("Region");
                    objDataTable.Columns.Add("Brand");
                    objDataTable.Columns.Add("User Entered Model");
                    objDataTable.Columns.Add("User Entered Codeset");                    
                    objDataTable.Columns.Add("EDID Block");
                    objDataTable.Columns.Add("Manufacturer (3 char) code");
                    objDataTable.Columns.Add("Manufacturer Hex Code");
                    objDataTable.Columns.Add("Monitor Name");
                    objDataTable.Columns.Add("Product Code");
                    objDataTable.Columns.Add("Serial number");
                    objDataTable.Columns.Add("Week");
                    objDataTable.Columns.Add("Year");
                    objDataTable.Columns.Add("Horizontal Screen Size");
                    objDataTable.Columns.Add("Vertical Screen Size");
                    objDataTable.Columns.Add("Video input definition");
                    objDataTable.Columns.Add("OSD");
                    if (this.NewFPRule)
                    {
                        objDataTable.Columns.Add("Custom FP Type0x00");
                        objDataTable.Columns.Add("Custom FP Type0x01");
                        objDataTable.Columns.Add("Custom FP Type0x02");
                        objDataTable.Columns.Add("Custom FP Type0x03");
                    }
                    //objDataTable.Columns.Add("FP Available");
                    //objDataTable.Columns.Add("FP Bytes");
                    //objDataTable.Columns.Add("Custom FP");
                    //objDataTable.Columns.Add("Custom FP + OSD");
                    //objDataTable.Columns.Add("128 FP");
                    //objDataTable.Columns.Add("128 FP + OSD");
                    //objDataTable.Columns.Add("OSD FP");
                    objDataTable.Columns.Add("Console Count", Type.GetType("System.Int64"));
                    objDataTable.Columns.Add("UEI2 ID");
                    objDataTable.Columns.Add("Suggested ID");
                    objDataTable.Columns.Add("DAC Published ID");
                    objDataTable.Columns.Add("CEC Physical Address");
                    objDataTable.Columns.Add("Build Rule");                    
                    objDataTable.Columns.Add("Discrete Profile");
                    objDataTable.Columns.Add("Discrete ID");
                    objDataTable.Columns.Add("Trust Level");
                    objDataTable.AcceptChanges();
                    Int32 intRowCount = 1;
                    foreach (EDIDInfo itemKey in this.EdIdData)
                    {
                        this.m_StatusMessage = "Generating report " + intRowCount.ToString() + " of " + this.EdIdData.Count.ToString();
                        this.m_StatusMessage1 = "Please wait...";
                        DataRow dr = objDataTable.NewRow();
                        dr["SL NO"] = intRowCount;
                        intRowCount = intRowCount + 1;
                        dr["Main Device"] = itemKey.MainDevice;
                        dr["Sub Device"] = itemKey.SubDevice;
                        dr["Component"] = itemKey.Component;
                        dr["Region"] = itemKey.Region;
                        dr["Brand"] = itemKey.Brand;
                        dr["User Entered Model"] = itemKey.Model;
                        dr["User Entered Codeset"] = itemKey.Codeset;
                        //dr["EDID"] = itemKey.EdIdRawData;
                        dr["EDID Block"] = itemKey.EdIdRawData;
                        dr["Manufacturer (3 char) code"] = itemKey.ManufacturerCode;
                        dr["Manufacturer Hex Code"] = itemKey.ManufacturerHexCode;
                        dr["Monitor Name"] = itemKey.ManufacturerName;
                        dr["Horizontal Screen Size"] = itemKey.HorizontalScreenSize;
                        dr["Vertical Screen Size"] = itemKey.VerticalScreenSize;
                        dr["Product Code"] = itemKey.ProductCode;
                        dr["Serial number"] = itemKey.SerialNumber;
                        dr["Week"] = itemKey.Week;
                        dr["Year"] = itemKey.Year;
                        dr["Video input definition"] = itemKey.VideoInputDefinition;
                        dr["OSD"] = itemKey.OSDRawData;
                        dr["CEC Physical Address"] = itemKey.PhysicalAddress;
                        dr["UEI2 ID"] = itemKey.SuggestedID;
                        if (this.NewFPRule)
                        {
                            dr["Custom FP Type0x00"] = itemKey.CustomFPType0x00;
                            dr["Custom FP Type0x01"] = itemKey.CustomFPType0x01;
                            dr["Custom FP Type0x02"] = itemKey.CustomFPType0x02;
                            dr["Custom FP Type0x03"] = itemKey.CustomFPType0x03;
                        }
                        //dr["FP Available"] = itemKey.FPAvailable;
                        //dr["FP Bytes"] = itemKey.FPBytePosition;
                        //dr["Custom FP"] = itemKey.CustomFP;
                        //dr["Custom FP + OSD"] = itemKey.CustomFPOSD;
                        //dr["128 FP"] = itemKey.Only128FP;
                        //dr["128 FP + OSD"] = itemKey.Only128FPOSD;
                        //dr["OSD FP"] = itemKey.OnlyOSDFP;
                        dr["Console Count"] = itemKey.ConsoleCount;
                        
                        //Based on Monitor Name and Brand                       
                        try
                        {
                            DataTable dtID = m_Service1.GetUEI2IDByMonitorName(itemKey);
                            if (dtID.Rows.Count > 0)
                            {
                                itemKey.SuggestedID = dtID.Rows[0]["ID"].ToString();
                                dr["Suggested ID"] = itemKey.SuggestedID;
                                dr["Rule"] = "1";
                                dr["Trust Level"] = "100%";
                                //Get Discrete Profile and Discrete ID
                                //if (String.IsNullOrEmpty(dr["Discrete Profile"].ToString()) || String.IsNullOrEmpty(dr["Discrete ID"].ToString()))
                                //{
                                //    try
                                //    {
                                //        DataTable dtDiscreteProfile = m_Service2.GetDiscreteProfile(itemKey);
                                //        if (dtDiscreteProfile.Rows.Count > 0)
                                //        {
                                //            dr["Discrete Profile"] = dtDiscreteProfile.Rows[0]["ProfileName"].ToString();
                                //            dr["Discrete ID"] = dtDiscreteProfile.Rows[0]["DiscreteID"].ToString();
                                //        }
                                //    }
                                //    catch (Exception ex2)
                                //    {
                                //        String s3 = ex2.Message;
                                //    }
                                //}
                            }
                        }
                        catch (Exception ex2)
                        {
                            String s2 = ex2.Message;
                        }
                        
                        if (!String.IsNullOrEmpty(itemKey.SuggestedID))
                        {
                            dr["Suggested ID"] = itemKey.SuggestedID;
                            dr["Build Rule"] = "1";
                            dr["Trust Level"] = "100%";
                            
                        }
                        //Get from Captured EDID Source
                        if (LinkedIds != null)
                        {
                            Dictionary<String, String> linkedidnRule = LinkedIds.GetIDs(itemKey, itemKey.Brand, this.FPType);
                            if (!String.IsNullOrEmpty(linkedidnRule["LinkedID"].ToString()))
                            {
                                itemKey.DACPublishedID = linkedidnRule["LinkedID"].ToString().Trim();
                                dr["DAC Published ID"] = itemKey.DACPublishedID;
                                dr["Build Rule"] = "2";
                                dr["Trust Level"] = "100%";
                                dr["Discrete Profile"] = linkedidnRule["DiscreteProfile"].ToString().Trim();
                                dr["Discrete ID"] = linkedidnRule["DiscreteID"].ToString().Trim();
                            }
                        }
                        //Get From User Entered Model and Code Set
                        if (String.IsNullOrEmpty(itemKey.DACPublishedID))
                        {
                            try
                            {
                                DataTable dtID = m_Service1.GetUEI2IDByUserEnteredModelCodeSet(itemKey);
                                if (dtID.Rows.Count > 0)
                                {
                                    itemKey.SuggestedID = dtID.Rows[0]["ID"].ToString();
                                    if (!String.IsNullOrEmpty(itemKey.SuggestedID))
                                    {
                                        dr["Suggested ID"] = itemKey.SuggestedID;
                                        dr["Build Rule"] = "3";
                                        dr["Trust Level"] = "90%";
                                    }
                                    //Get Discrete Profile and Discrete ID
                                    //if (String.IsNullOrEmpty(dr["Discrete Profile"].ToString()) || String.IsNullOrEmpty(dr["Discrete ID"].ToString()))
                                    //{
                                    //    try
                                    //    {
                                    //        DataTable dtDiscreteProfile = m_Service2.GetDiscreteProfile(itemKey);
                                    //        if (dtDiscreteProfile.Rows.Count > 0)
                                    //        {
                                    //            dr["Discrete Profile"] = dtDiscreteProfile.Rows[0]["ProfileName"].ToString();
                                    //            dr["Discrete ID"] = dtDiscreteProfile.Rows[0]["DiscreteID"].ToString();
                                    //        }
                                    //    }
                                    //    catch (Exception ex2)
                                    //    {
                                    //        String s3 = ex2.Message;
                                    //    }
                                    //}
                                }
                            }
                            catch (Exception ex1)
                            {
                                String s2 = ex1.Message;
                            }
                        }

                                                                                                                      
                        objDataTable.Rows.Add(dr);
                        dr = null;
                    }
                    objDataTable.AcceptChanges();
                    if (objDataSet == null)
                    {
                        objDataSet = new DataSet("XBox EDID Summary");
                    }
                    if (objDataSet.Tables.Count > 0)
                    {
                        if (objDataSet.Tables.Contains(objDataTable.TableName))
                            objDataSet.Tables.Remove(objDataTable.TableName);
                    }
                    objDataSet.Tables.Add(objDataTable);                    
                }
            }
        }
        void Export()
        {
            String m_SaveFileMessage = String.Empty;
            try
            {
                String m_filePath = CommonForms.Configuration.GetWorkingDirectory() + "\\";
                if (String.IsNullOrEmpty(m_filePath))
                {
                    FolderBrowserDialog dlg1 = new FolderBrowserDialog();
                    if (dlg1.ShowDialog() == DialogResult.OK)
                    {
                        if (dlg1.SelectedPath.EndsWith("\\") == false)
                        {
                            dlg1.SelectedPath += "\\";
                        }
                        m_filePath = dlg1.SelectedPath;
                    }
                }                
                ExcelReportGenerator.GenerateReport(objDataSet, m_filePath, "XBox EDID Summary", out m_SaveFileMessage);
                if (!String.IsNullOrEmpty(m_SaveFileMessage))
                {
                    MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        #endregion

        #region Progress
        private void ResetTimer()
        {
            if (m_BackgroundWorker != null || m_BackgroundWorker.IsBusy == false)
                m_BackgroundWorker.RunWorkerAsync();

            if (m_Progress == null)
                m_Progress = new ProgressForm();
            m_Progress.SetMessage(StatusMessage, StatusMessage1);
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();
            m_Timer.Start();
        }
        void m_Timer_Tick(object sender, EventArgs e)
        {
            if (this.m_Progress != null)
            {
                this.m_Progress.SetMessage(StatusMessage, StatusMessage1);
            }
        }
        void m_BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.m_Timer.Stop();
            this.m_Progress.DialogResult = DialogResult.OK;
            this.m_Progress.Close();

            if (m_Service.ErrorMessage != String.Empty)
            {
                MessageBox.Show(m_Service.ErrorMessage, "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                ExportToExcel();               
            }
        }
        void m_BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            this.EdIdData = this.m_Service.GetXBoxEDIDReport(this.SelectedDeviceType, this.SelectedBrand, this.SelectedModels, this.SelectedDataSources, this.SelectedCountries, ref this.m_StatusMessage1);
            if (this.EdIdData != null)
            {
                ExportToExcel();
            }
            else
                this.m_Service.ErrorMessage = "No Data Found";
        }
        #endregion
    }
}
