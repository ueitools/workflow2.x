﻿namespace UEI.EDID.Forms
{
    partial class XBoxReportFilterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabCountries = new System.Windows.Forms.TabPage();
            this.chkCountries = new System.Windows.Forms.CheckedListBox();
            this.tabDataSources = new System.Windows.Forms.TabPage();
            this.chkDataSources = new System.Windows.Forms.CheckedListBox();
            this.tabDevices = new System.Windows.Forms.TabPage();
            this.chkDeviceTypes = new System.Windows.Forms.CheckedListBox();
            this.tabBrands = new System.Windows.Forms.TabPage();
            this.lstBrands = new System.Windows.Forms.ListBox();
            this.btnRemoveBrand = new System.Windows.Forms.Button();
            this.btnAddBrand = new System.Windows.Forms.Button();
            this.txtBrand = new System.Windows.Forms.TextBox();
            this.btnBrandBrowse = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabModels = new System.Windows.Forms.TabPage();
            this.lstModels = new System.Windows.Forms.ListBox();
            this.btnRemoveModel = new System.Windows.Forms.Button();
            this.btnAddModel = new System.Windows.Forms.Button();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.btnBrowseModel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tabOthers = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdoFP128 = new System.Windows.Forms.RadioButton();
            this.rdoFP = new System.Windows.Forms.RadioButton();
            this.rdoFP0x03 = new System.Windows.Forms.RadioButton();
            this.rdoFP0x02 = new System.Windows.Forms.RadioButton();
            this.rdoFP0x01 = new System.Windows.Forms.RadioButton();
            this.rdoFP0x00 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkNewFPRule = new System.Windows.Forms.CheckBox();
            this.txtEDIDBase64ExcelFile = new System.Windows.Forms.TextBox();
            this.btnDownload = new System.Windows.Forms.Button();
            this.btnBrowseEDIDExcelFile = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.etchedLine1 = new UEI.Workflow2010.Report.Forms.EtchedLine();
            this.tabControl1.SuspendLayout();
            this.tabCountries.SuspendLayout();
            this.tabDataSources.SuspendLayout();
            this.tabDevices.SuspendLayout();
            this.tabBrands.SuspendLayout();
            this.tabModels.SuspendLayout();
            this.tabOthers.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabCountries);
            this.tabControl1.Controls.Add(this.tabDataSources);
            this.tabControl1.Controls.Add(this.tabDevices);
            this.tabControl1.Controls.Add(this.tabBrands);
            this.tabControl1.Controls.Add(this.tabModels);
            this.tabControl1.Controls.Add(this.tabOthers);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(529, 294);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabCountries
            // 
            this.tabCountries.Controls.Add(this.chkCountries);
            this.tabCountries.Location = new System.Drawing.Point(4, 22);
            this.tabCountries.Name = "tabCountries";
            this.tabCountries.Padding = new System.Windows.Forms.Padding(3);
            this.tabCountries.Size = new System.Drawing.Size(521, 268);
            this.tabCountries.TabIndex = 3;
            this.tabCountries.Text = "Countries";
            this.tabCountries.UseVisualStyleBackColor = true;
            // 
            // chkCountries
            // 
            this.chkCountries.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chkCountries.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkCountries.FormattingEnabled = true;
            this.chkCountries.Location = new System.Drawing.Point(3, 3);
            this.chkCountries.Name = "chkCountries";
            this.chkCountries.Size = new System.Drawing.Size(186, 262);
            this.chkCountries.TabIndex = 1;
            // 
            // tabDataSources
            // 
            this.tabDataSources.Controls.Add(this.chkDataSources);
            this.tabDataSources.Location = new System.Drawing.Point(4, 22);
            this.tabDataSources.Name = "tabDataSources";
            this.tabDataSources.Padding = new System.Windows.Forms.Padding(3);
            this.tabDataSources.Size = new System.Drawing.Size(521, 268);
            this.tabDataSources.TabIndex = 4;
            this.tabDataSources.Text = "Data Sources";
            this.tabDataSources.UseVisualStyleBackColor = true;
            // 
            // chkDataSources
            // 
            this.chkDataSources.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chkDataSources.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkDataSources.FormattingEnabled = true;
            this.chkDataSources.Location = new System.Drawing.Point(3, 3);
            this.chkDataSources.Name = "chkDataSources";
            this.chkDataSources.Size = new System.Drawing.Size(186, 262);
            this.chkDataSources.TabIndex = 2;
            // 
            // tabDevices
            // 
            this.tabDevices.Controls.Add(this.chkDeviceTypes);
            this.tabDevices.Location = new System.Drawing.Point(4, 22);
            this.tabDevices.Name = "tabDevices";
            this.tabDevices.Padding = new System.Windows.Forms.Padding(3);
            this.tabDevices.Size = new System.Drawing.Size(521, 268);
            this.tabDevices.TabIndex = 0;
            this.tabDevices.Text = "Devices";
            this.tabDevices.UseVisualStyleBackColor = true;
            // 
            // chkDeviceTypes
            // 
            this.chkDeviceTypes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chkDeviceTypes.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkDeviceTypes.FormattingEnabled = true;
            this.chkDeviceTypes.Location = new System.Drawing.Point(3, 3);
            this.chkDeviceTypes.Name = "chkDeviceTypes";
            this.chkDeviceTypes.Size = new System.Drawing.Size(186, 262);
            this.chkDeviceTypes.TabIndex = 2;
            // 
            // tabBrands
            // 
            this.tabBrands.Controls.Add(this.lstBrands);
            this.tabBrands.Controls.Add(this.btnRemoveBrand);
            this.tabBrands.Controls.Add(this.btnAddBrand);
            this.tabBrands.Controls.Add(this.txtBrand);
            this.tabBrands.Controls.Add(this.btnBrandBrowse);
            this.tabBrands.Controls.Add(this.label1);
            this.tabBrands.Location = new System.Drawing.Point(4, 22);
            this.tabBrands.Name = "tabBrands";
            this.tabBrands.Padding = new System.Windows.Forms.Padding(3);
            this.tabBrands.Size = new System.Drawing.Size(521, 268);
            this.tabBrands.TabIndex = 1;
            this.tabBrands.Text = "Brands";
            this.tabBrands.UseVisualStyleBackColor = true;
            // 
            // lstBrands
            // 
            this.lstBrands.Dock = System.Windows.Forms.DockStyle.Left;
            this.lstBrands.FormattingEnabled = true;
            this.lstBrands.Location = new System.Drawing.Point(3, 3);
            this.lstBrands.Name = "lstBrands";
            this.lstBrands.Size = new System.Drawing.Size(185, 262);
            this.lstBrands.TabIndex = 13;
            // 
            // btnRemoveBrand
            // 
            this.btnRemoveBrand.Location = new System.Drawing.Point(198, 75);
            this.btnRemoveBrand.Name = "btnRemoveBrand";
            this.btnRemoveBrand.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveBrand.TabIndex = 12;
            this.btnRemoveBrand.Text = "Remove";
            this.btnRemoveBrand.UseVisualStyleBackColor = true;
            // 
            // btnAddBrand
            // 
            this.btnAddBrand.Location = new System.Drawing.Point(198, 46);
            this.btnAddBrand.Name = "btnAddBrand";
            this.btnAddBrand.Size = new System.Drawing.Size(75, 23);
            this.btnAddBrand.TabIndex = 11;
            this.btnAddBrand.Text = "Add";
            this.btnAddBrand.UseVisualStyleBackColor = true;
            // 
            // txtBrand
            // 
            this.txtBrand.Location = new System.Drawing.Point(198, 20);
            this.txtBrand.Name = "txtBrand";
            this.txtBrand.Size = new System.Drawing.Size(174, 20);
            this.txtBrand.TabIndex = 10;
            // 
            // btnBrandBrowse
            // 
            this.btnBrandBrowse.Location = new System.Drawing.Point(198, 104);
            this.btnBrandBrowse.Name = "btnBrandBrowse";
            this.btnBrandBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrandBrowse.TabIndex = 9;
            this.btnBrandBrowse.Text = "Browse";
            this.btnBrandBrowse.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(195, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Enter Brand Name:";
            // 
            // tabModels
            // 
            this.tabModels.Controls.Add(this.lstModels);
            this.tabModels.Controls.Add(this.btnRemoveModel);
            this.tabModels.Controls.Add(this.btnAddModel);
            this.tabModels.Controls.Add(this.txtModel);
            this.tabModels.Controls.Add(this.btnBrowseModel);
            this.tabModels.Controls.Add(this.label2);
            this.tabModels.Location = new System.Drawing.Point(4, 22);
            this.tabModels.Name = "tabModels";
            this.tabModels.Padding = new System.Windows.Forms.Padding(3);
            this.tabModels.Size = new System.Drawing.Size(521, 268);
            this.tabModels.TabIndex = 2;
            this.tabModels.Text = "Models";
            this.tabModels.UseVisualStyleBackColor = true;
            // 
            // lstModels
            // 
            this.lstModels.Dock = System.Windows.Forms.DockStyle.Left;
            this.lstModels.FormattingEnabled = true;
            this.lstModels.Location = new System.Drawing.Point(3, 3);
            this.lstModels.Name = "lstModels";
            this.lstModels.Size = new System.Drawing.Size(185, 262);
            this.lstModels.TabIndex = 18;
            // 
            // btnRemoveModel
            // 
            this.btnRemoveModel.Location = new System.Drawing.Point(198, 76);
            this.btnRemoveModel.Name = "btnRemoveModel";
            this.btnRemoveModel.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveModel.TabIndex = 17;
            this.btnRemoveModel.Text = "Remove";
            this.btnRemoveModel.UseVisualStyleBackColor = true;
            // 
            // btnAddModel
            // 
            this.btnAddModel.Location = new System.Drawing.Point(198, 47);
            this.btnAddModel.Name = "btnAddModel";
            this.btnAddModel.Size = new System.Drawing.Size(75, 23);
            this.btnAddModel.TabIndex = 16;
            this.btnAddModel.Text = "Add";
            this.btnAddModel.UseVisualStyleBackColor = true;
            // 
            // txtModel
            // 
            this.txtModel.Location = new System.Drawing.Point(198, 21);
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(174, 20);
            this.txtModel.TabIndex = 15;
            // 
            // btnBrowseModel
            // 
            this.btnBrowseModel.Location = new System.Drawing.Point(198, 105);
            this.btnBrowseModel.Name = "btnBrowseModel";
            this.btnBrowseModel.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseModel.TabIndex = 14;
            this.btnBrowseModel.Text = "Browse";
            this.btnBrowseModel.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(195, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Enter Model Name:";
            // 
            // tabOthers
            // 
            this.tabOthers.Controls.Add(this.groupBox1);
            this.tabOthers.Controls.Add(this.groupBox2);
            this.tabOthers.Location = new System.Drawing.Point(4, 22);
            this.tabOthers.Name = "tabOthers";
            this.tabOthers.Padding = new System.Windows.Forms.Padding(3);
            this.tabOthers.Size = new System.Drawing.Size(521, 268);
            this.tabOthers.TabIndex = 5;
            this.tabOthers.Text = "Others";
            this.tabOthers.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdoFP128);
            this.groupBox1.Controls.Add(this.rdoFP);
            this.groupBox1.Controls.Add(this.rdoFP0x03);
            this.groupBox1.Controls.Add(this.rdoFP0x02);
            this.groupBox1.Controls.Add(this.rdoFP0x01);
            this.groupBox1.Controls.Add(this.rdoFP0x00);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(515, 161);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Linked ID to be selected based on";
            // 
            // rdoFP128
            // 
            this.rdoFP128.AutoSize = true;
            this.rdoFP128.Location = new System.Drawing.Point(183, 43);
            this.rdoFP128.Name = "rdoFP128";
            this.rdoFP128.Size = new System.Drawing.Size(59, 17);
            this.rdoFP128.TabIndex = 5;
            this.rdoFP128.TabStop = true;
            this.rdoFP128.Text = "128 FP";
            this.rdoFP128.UseVisualStyleBackColor = true;
            // 
            // rdoFP
            // 
            this.rdoFP.AutoSize = true;
            this.rdoFP.Location = new System.Drawing.Point(183, 20);
            this.rdoFP.Name = "rdoFP";
            this.rdoFP.Size = new System.Drawing.Size(76, 17);
            this.rdoFP.TabIndex = 4;
            this.rdoFP.Text = "Custom FP";
            this.rdoFP.UseVisualStyleBackColor = true;
            // 
            // rdoFP0x03
            // 
            this.rdoFP0x03.AutoSize = true;
            this.rdoFP0x03.Location = new System.Drawing.Point(11, 89);
            this.rdoFP0x03.Name = "rdoFP0x03";
            this.rdoFP0x03.Size = new System.Drawing.Size(126, 17);
            this.rdoFP0x03.TabIndex = 3;
            this.rdoFP0x03.TabStop = true;
            this.rdoFP0x03.Text = "Custom FP Type0x03";
            this.rdoFP0x03.UseVisualStyleBackColor = true;
            // 
            // rdoFP0x02
            // 
            this.rdoFP0x02.AutoSize = true;
            this.rdoFP0x02.Location = new System.Drawing.Point(11, 66);
            this.rdoFP0x02.Name = "rdoFP0x02";
            this.rdoFP0x02.Size = new System.Drawing.Size(126, 17);
            this.rdoFP0x02.TabIndex = 2;
            this.rdoFP0x02.TabStop = true;
            this.rdoFP0x02.Text = "Custom FP Type0x02";
            this.rdoFP0x02.UseVisualStyleBackColor = true;
            // 
            // rdoFP0x01
            // 
            this.rdoFP0x01.AutoSize = true;
            this.rdoFP0x01.Location = new System.Drawing.Point(11, 43);
            this.rdoFP0x01.Name = "rdoFP0x01";
            this.rdoFP0x01.Size = new System.Drawing.Size(126, 17);
            this.rdoFP0x01.TabIndex = 1;
            this.rdoFP0x01.TabStop = true;
            this.rdoFP0x01.Text = "Custom FP Type0x01";
            this.rdoFP0x01.UseVisualStyleBackColor = true;
            // 
            // rdoFP0x00
            // 
            this.rdoFP0x00.AutoSize = true;
            this.rdoFP0x00.Location = new System.Drawing.Point(11, 20);
            this.rdoFP0x00.Name = "rdoFP0x00";
            this.rdoFP0x00.Size = new System.Drawing.Size(126, 17);
            this.rdoFP0x00.TabIndex = 0;
            this.rdoFP0x00.TabStop = true;
            this.rdoFP0x00.Text = "Custom FP Type0x00";
            this.rdoFP0x00.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.chkNewFPRule);
            this.groupBox2.Controls.Add(this.txtEDIDBase64ExcelFile);
            this.groupBox2.Controls.Add(this.btnDownload);
            this.groupBox2.Controls.Add(this.btnBrowseEDIDExcelFile);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(3, 164);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(515, 101);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Select Base 64 EDID Excel File";
            // 
            // chkNewFPRule
            // 
            this.chkNewFPRule.AutoSize = true;
            this.chkNewFPRule.Checked = true;
            this.chkNewFPRule.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkNewFPRule.Location = new System.Drawing.Point(9, 65);
            this.chkNewFPRule.Name = "chkNewFPRule";
            this.chkNewFPRule.Size = new System.Drawing.Size(125, 17);
            this.chkNewFPRule.TabIndex = 8;
            this.chkNewFPRule.Text = "New Fingerprint Rule";
            this.chkNewFPRule.UseVisualStyleBackColor = true;
            // 
            // txtEDIDBase64ExcelFile
            // 
            this.txtEDIDBase64ExcelFile.Location = new System.Drawing.Point(168, 22);
            this.txtEDIDBase64ExcelFile.Name = "txtEDIDBase64ExcelFile";
            this.txtEDIDBase64ExcelFile.Size = new System.Drawing.Size(201, 20);
            this.txtEDIDBase64ExcelFile.TabIndex = 5;
            // 
            // btnDownload
            // 
            this.btnDownload.Location = new System.Drawing.Point(375, 49);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(75, 47);
            this.btnDownload.TabIndex = 7;
            this.btnDownload.Text = "Download Input File Format";
            this.btnDownload.UseVisualStyleBackColor = true;
            // 
            // btnBrowseEDIDExcelFile
            // 
            this.btnBrowseEDIDExcelFile.Location = new System.Drawing.Point(375, 20);
            this.btnBrowseEDIDExcelFile.Name = "btnBrowseEDIDExcelFile";
            this.btnBrowseEDIDExcelFile.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseEDIDExcelFile.TabIndex = 6;
            this.btnBrowseEDIDExcelFile.Text = "Browse";
            this.btnBrowseEDIDExcelFile.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(373, 296);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(454, 296);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // etchedLine1
            // 
            this.etchedLine1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.etchedLine1.Edge = UEI.Workflow2010.Report.Forms.EtchEdge.Top;
            this.etchedLine1.Location = new System.Drawing.Point(0, 294);
            this.etchedLine1.Name = "etchedLine1";
            this.etchedLine1.Size = new System.Drawing.Size(529, 26);
            this.etchedLine1.TabIndex = 0;
            // 
            // XBoxReportFilterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(529, 320);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.etchedLine1);
            this.Name = "XBoxReportFilterForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Report Filter Form";
            this.tabControl1.ResumeLayout(false);
            this.tabCountries.ResumeLayout(false);
            this.tabDataSources.ResumeLayout(false);
            this.tabDevices.ResumeLayout(false);
            this.tabBrands.ResumeLayout(false);
            this.tabBrands.PerformLayout();
            this.tabModels.ResumeLayout(false);
            this.tabModels.PerformLayout();
            this.tabOthers.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Workflow2010.Report.Forms.EtchedLine etchedLine1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabDevices;
        private System.Windows.Forms.TabPage tabBrands;
        private System.Windows.Forms.TabPage tabModels;
        private System.Windows.Forms.TabPage tabCountries;
        private System.Windows.Forms.CheckedListBox chkDeviceTypes;
        private System.Windows.Forms.CheckedListBox chkCountries;
        private System.Windows.Forms.Button btnBrandBrowse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRemoveBrand;
        private System.Windows.Forms.Button btnAddBrand;
        private System.Windows.Forms.TextBox txtBrand;
        private System.Windows.Forms.Button btnRemoveModel;
        private System.Windows.Forms.Button btnAddModel;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.Button btnBrowseModel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TabPage tabDataSources;
        private System.Windows.Forms.CheckedListBox chkDataSources;
        private System.Windows.Forms.ListBox lstBrands;
        private System.Windows.Forms.ListBox lstModels;
        private System.Windows.Forms.TabPage tabOthers;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Button btnBrowseEDIDExcelFile;
        private System.Windows.Forms.TextBox txtEDIDBase64ExcelFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkNewFPRule;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdoFP128;
        private System.Windows.Forms.RadioButton rdoFP;
        private System.Windows.Forms.RadioButton rdoFP0x03;
        private System.Windows.Forms.RadioButton rdoFP0x02;
        private System.Windows.Forms.RadioButton rdoFP0x01;
        private System.Windows.Forms.RadioButton rdoFP0x00;
    }
}