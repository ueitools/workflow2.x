﻿using CommonForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using UEI.Workflow2010.Report;
using UEI.Workflow2010.Report.ExportToExcel;
using UEI.Workflow2010.Report.Model;
using UEI.Workflow2010.Report.Service;

namespace UEI.EDID.Forms
{
    public partial class ViewEdIdForm : Form
    {
        #region Variables
        private IList<Region> m_Regions                 = new List<Region>();
        private IList<DeviceType> m_DeviceTypes         = new List<DeviceType>();
        private SelType m_SelectedType                  = SelType.MODE;        
        public List<String> m_SelectedModes             = new List<String>();
        private IdSetupCodeInfo m_IDSetupCodeInfoParams = null;
        private BrandFilter m_BrandFilter               = null;
        private ModelFilter m_ModelFilter               = null;
        private IList<Id> m_Ids                         = new List<Id>();
        private ProgressForm m_Progress                 = null;
        private BackgroundWorker m_BackgroundWorker     = null;        
        #endregion

        #region Properties
        public List<EDIDInfo>       EdIdData { get; set; }
        public DataTable            EdIdDataTable { get; set; }
        public DataTable            EdIdWithFileStrcuture { get; set; }
        public String               SelectedNode { get; set; }
        public EDID                 EDIDDetails { get; set; }
        public String               SelectedBrand { get; set; }
        public String               SelectedModel { get; set; }
        #endregion

        #region Constrcutor
        public ViewEdIdForm()
        {
            InitializeComponent();
            this.Load += ViewEdIdForm_Load;
        }       
        #endregion

        #region Load
        void ViewEdIdForm_Load(object sender, EventArgs e)
        {
            this.m_BackgroundWorker                     = new BackgroundWorker();
            this.m_BackgroundWorker.DoWork              += m_BackgroundWorker_DoWork;
            this.m_BackgroundWorker.RunWorkerCompleted  += m_BackgroundWorker_RunWorkerCompleted;
            this.treeView.BeforeSelect                  += treeView_BeforeSelect;
            this.treeView.AfterSelect                   += treeView_AfterSelect;
            this.edidMoretreeView.BeforeSelect += edidMoretreeView_BeforeSelect;
            this.edidMoretreeView.AfterSelect += edidMoretreeView_AfterSelect;
            try
            {
                Reports m_Service = new Reports();
                IList<String> idList = GetIDs();
                if (idList != null && idList.Count > 0)
                {
                    ReportFilterForm m_ReportFilterForm = new ReportFilterForm(ReportType.EdId, idList);
                    if (m_ReportFilterForm.ShowDialog() == DialogResult.OK)
                    {
                        m_Ids                   = m_ReportFilterForm.Ids;
                        m_Regions               = m_ReportFilterForm.Regions;
                        m_DeviceTypes           = m_ReportFilterForm.DeviceTypes;
                        m_SelectedModes         = m_ReportFilterForm._selectedModes;
                        m_IDSetupCodeInfoParams = m_ReportFilterForm.IdSetupCodeInfoParam;
                        m_BrandFilter           = m_ReportFilterForm.BrandSpecificSel;
                        m_ModelFilter           = m_ReportFilterForm.ModelSpecificSel;
                        m_ReportFilterForm.Close();
                        ResetTimer();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                MessageBox.Show("Some error occured in report generation.Error logged");
            }  
        }                                              
        #endregion

        #region EDID More Details
        private void DisplayHeaderInformation()
        {
            int subnodes = 10;//0 to 7
            StringBuilder _info = new StringBuilder();            
            for (int i = 0; i <= subnodes - 1; i++)
            {
                switch (i)
                {
                    case 0:
                        {
                            _info.Append("Fixed Header: ");
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.Header.Data));
                            break;
                        }
                    case 1:
                        {
                            _info.Append("Manufacturer ID: ");
                            _info.Append(Conversion.ConvertByteStreamToString(this.EDIDDetails.Manufacturer.Data));
                            _info.AppendLine(String.Format("({0})", this.EDIDDetails.Manufacturer.GetDetails()));
                            //GetManufacturerDetails(this.EDIDDetails._manufacturer.data);
                            break;
                        }
                    case 2:
                        {
                            _info.Append("Manufacturer Product Code: ");
                            _info.Append(Conversion.ConvertByteStreamToString(this.EDIDDetails.ProductCode.Data));
                            _info.AppendLine(String.Format("({0})", this.EDIDDetails.ProductCode.GetDetails()));
                            break;
                        }
                    case 3:
                        {
                            _info.Append("Serial Number: ");
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.SerialNumber.Data));
                            _info.AppendLine(String.Format("({0})", this.EDIDDetails.SerialNumber.GetDetails()));
                            break;
                        }
                    case 4:
                        {
                            _info.Append("Week of Manufacturer: ");
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.WOfMfg.Data));
                            break;
                        }
                    case 5:
                        {
                            _info.Append("Year of Manufacturer: ");
                            _info.Append(Conversion.ConvertByteStreamToString(this.EDIDDetails.YOfMfg.Data));
                            _info.AppendLine(String.Format("({0})", this.EDIDDetails.YOfMfg.GetDetails()));
                            break;
                        }
                    case 6:
                        {
                            _info.Append("EDID Version: ");
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.EDIDVerNo.Data));
                            break;
                        }
                    case 7:
                        {
                            _info.Append("EDID revision: ");
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.EDIDRevVerNo.Data));
                            break;
                        }
                    case 8:
                        {
                            _info.Append("Monitor Name: ");
                            _info.AppendLine(this.EDIDDetails.Monitor.GetDetails(this.EDIDDetails.DetDes2, this.EDIDDetails.DetDes3,this.EDIDDetails.DetDes4));
                            break;
                        }
                    case 9:
                        {
                            _info.Append("Monitor Serial Number: ");
                            _info.AppendLine(this.EDIDDetails.MonitorSerialNo.GetDetails(this.EDIDDetails.DetDes3,this.EDIDDetails.DetDes4));
                            break;
                        }
                }
            }
            this.txtModeDetails.Text = String.Empty;
            this.txtModeDetails.Text = _info.ToString();
        }
        private void DisplayBasicDisplayParameters()
        {
            int subnodes = 5;//0 to 7
            StringBuilder _info = new StringBuilder();            
            for (int i = 0; i <= subnodes - 1; i++)
            {
                switch (i)
                {
                    case 0:
                        {
                            _info.Append("Video Input Parameters: ");
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.VideoIn.Data));
                            _info.Append(this.EDIDDetails.VideoIn.GetDetails());
                            break;
                        }
                    case 1:
                        {
                            _info.Append("Maximum Horizontal Image Size: ");
                            _info.Append(Conversion.ConvertByteStreamToString(this.EDIDDetails.MaxHorImg.Data));
                            _info.AppendLine(this.EDIDDetails.MaxHorImg.GetDetails());
                            //GetManufacturerDetails(this.EDIDDetails._manufacturer.data);
                            break;
                        }
                    case 2:
                        {
                            _info.Append("Maximum Vertical Image Size: ");
                            _info.Append(Conversion.ConvertByteStreamToString(this.EDIDDetails.MaxVerImg.Data));
                            _info.AppendLine(String.Format("({0})", this.EDIDDetails.MaxVerImg.GetDetails()));
                            break;
                        }
                    case 3:
                        {
                            _info.Append("Display Gamma: ");
                            _info.Append(Conversion.ConvertByteStreamToString(this.EDIDDetails.DispGamma.Data));
                            _info.AppendLine(String.Format("({0})", this.EDIDDetails.DispGamma.GetDetails()));
                            break;
                        }
                    case 4:
                        {
                            _info.Append("Power And Supported Features: ");
                            _info.Append(Conversion.ConvertByteStreamToString(this.EDIDDetails.PowerAndSupport.Data));
                            _info.AppendLine(String.Format("({0})", this.EDIDDetails.PowerAndSupport.GetDetails()));
                            break;
                        }

                }
            }
            this.txtModeDetails.Text = String.Empty;
            this.txtModeDetails.Text = _info.ToString();
        }
        private void DisplayChromaticityCoordinates()
        {
            int subnodes = 7;//0 to 7
            StringBuilder _info = new StringBuilder();            
            for (int i = 0; i <= subnodes - 1; i++)
            {
                switch (i)
                {
                    case 0:
                        {
                            _info.Append("Red and Green LSB: ");
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.ColorChar.Data));
                            break;
                        }
                    case 1:
                        {
                            _info.Append("Blue and While LSB: ");
                            _info.AppendLine();
                            //_info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails._maxhorimg.data));
                            //GetManufacturerDetails(this.EDIDDetails._manufacturer.data);
                            break;
                        }
                    case 2:
                        {
                            _info.Append("Red x value MSB: ");
                            _info.AppendLine();
                            //_info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails._maxverimg.data));
                            break;
                        }
                    case 3:
                        {
                            _info.Append("Red y value MSB: ");
                            _info.AppendLine();
                            //_info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails._dispgamma.data));
                            break;
                        }
                    case 4:
                        {
                            _info.Append("Green x and y value MSB: ");
                            _info.AppendLine();
                            // _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails._powerandsupport.data));
                            break;
                        }
                    case 5:
                        {
                            _info.Append("Blue x and y value MSB: ");
                            _info.AppendLine();
                            // _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails._powerandsupport.data));
                            break;
                        }
                    case 6:
                        {
                            _info.Append("Default white point x and y value MSB: ");
                            _info.AppendLine();
                            //_info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails._powerandsupport.data));
                            break;
                        }

                }
            }
            this.txtModeDetails.Text = String.Empty;
            this.txtModeDetails.Text = _info.ToString();

        }
        private void DisplayEstablishedTimingBitmap()
        {
            int subnodes = 3;//0 to 7
            StringBuilder _info = new StringBuilder();            
            for (int i = 0; i <= subnodes - 1; i++)
            {
                switch (i)
                {
                    case 0:
                        {
                            _info.Append("EstablishedTimingsI: ");
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.EstTmg1.Data));
                            _info.AppendLine(String.Format("{0}", this.EDIDDetails.EstTmg1.GetDetails()));
                            break;
                        }
                    case 1:
                        {
                            _info.Append("EstablishedTimingsII: ");
                            //_info.AppendLine();
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.EstTmg2.Data));
                            _info.AppendLine(String.Format("{0}", this.EDIDDetails.EstTmg2.GetDetails()));
                            break;
                        }
                    case 2:
                        {
                            _info.Append("ManufacturersTimings: ");
                            //_info.AppendLine();
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.MfgTmg.Data));
                            _info.AppendLine(String.Format("{0}", this.EDIDDetails.MfgTmg.GetDetails()));
                            break;
                        }
                }
            }
            this.txtModeDetails.Text = String.Empty;
            this.txtModeDetails.Text = _info.ToString();
        }
        private void DisplayStandardTimingInformation()
        {
            int subnodes = 7;//0 to 7
            StringBuilder _info = new StringBuilder();            
            for (int i = 0; i <= subnodes - 1; i++)
            {
                switch (i)
                {
                    case 0:
                        {
                            _info.Append("Standard Timing Information: ");
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.StndTmg.Data));
                            break;
                        }
                    case 1:
                        {
                            _info.Append("Descriptor I: ");
                            //_info.AppendLine();
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.DetDes1.Data));
                            //GetManufacturerDetails(this.EDIDDetails._manufacturer.data);
                            break;
                        }
                    case 2:
                        {
                            _info.Append("Descriptor II: ");
                            //_info.AppendLine();
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.DetDes2.Data));
                            break;
                        }
                    case 3:
                        {
                            _info.Append("Descriptor III: ");
                            //_info.AppendLine();
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.DetDes3.Data));
                            break;
                        }
                    case 4:
                        {
                            _info.Append("Descriptor IV: ");
                            //_info.AppendLine();
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.DetDes4.Data));
                            break;
                        }
                    case 5:
                        {
                            _info.Append("Number of Extensions: ");
                            //_info.AppendLine();
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.Ext.Data));
                            break;
                        }

                    case 6:
                        {
                            _info.Append("Checksum: ");
                            //_info.AppendLine();
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.ChkSum.Data));
                            break;
                        }
                }
            }
            this.txtModeDetails.Text = String.Empty;
            this.txtModeDetails.Text = _info.ToString();
        }
        private void DisplayVendorInformation()
        {
            int subnodes = 1;//0 to 7
            StringBuilder _info = new StringBuilder();
            for (int i = 0; i <= subnodes - 1; i++)
            {
                switch (i)
                {
                    case 0:
                        {
                            _info.Append("Vendor Specific Data Block (VSDB): ");
                            _info.AppendLine(Conversion.ConvertByteStreamToString(this.EDIDDetails.VendSpecDataBlock.Data));
                            break;
                        }
                }
            }
            this.txtModeDetails.Text = String.Empty;
            this.txtModeDetails.Text = _info.ToString();
        }
        void edidMoretreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeNode node = ((TreeNode)e.Node);
                if (node != null && this.EDIDDetails != null)
                {
                    switch (node.Text)
                    {
                        case "Header Information":
                            {
                                DisplayHeaderInformation();
                                break;
                            }
                        case "Basic Display Parameters":
                            {
                                DisplayBasicDisplayParameters();
                                break;
                            }
                        case "Chromaticity Coordinates":
                            {
                                DisplayChromaticityCoordinates();
                                break;
                            }
                        case "Established Timing Bitmap":
                            {
                                DisplayEstablishedTimingBitmap();
                                break;
                            }
                        case "Standard Timing Information":
                            {
                                DisplayStandardTimingInformation();
                                break;
                            }
                        case "Vendor Specific Information":
                            {
                                DisplayVendorInformation();
                                break;
                            }
                    }
                }
            }
        }
        void edidMoretreeView_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Action == TreeViewAction.Unknown)
            {
                if (e.Node.Parent == null)
                    e.Cancel = true;
            }
        }
        #endregion

        #region Tree View Select
        void treeView_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Action == TreeViewAction.Unknown)
            {
                if (e.Node.Parent == null)
                    e.Cancel = true;
            }
        }
        void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                this.SelectedModel = String.Empty;
                this.SelectedBrand = String.Empty;
                TreeNode node = ((TreeNode)e.Node);
                if (node != null && node.Tag != null)
                {                    
                    String[] strSep = node.Tag.ToString().Split(','); //"Model," + item.SlNo.ToString();
                    if (strSep[0] == "Model")
                    {
                        this.SelectedBrand = node.Parent.Text;
                        this.SelectedModel = node.Text;
                        ClearAllControls();
                        SelectedNode = strSep[1];
                        BindEdIdDetails();
                    }
                    else
                    {
                        SelectedNode = String.Empty;
                        ClearAllControls();
                        EDIDDetails = null;                        
                    }
                }
                else
                {
                    SelectedNode = String.Empty;
                    ClearAllControls();
                    EDIDDetails = null;
                }
            }
        } 
        #endregion

        #region Bind EDID Details
        void ClearAllControls()
        {
            this.edidMoretreeView.SelectedNode   = null;
            this.txtEDIDData.Text           = String.Empty;
            this.txtOSDData.Text            = String.Empty;
            this.txtEDIDChecksum.Text       = String.Empty;
            this.txtOSDChecksum.Text        = String.Empty;
            this.txtCombinedChecksum.Text   = String.Empty;
            this.txtBrand.Text              = String.Empty;
            this.txtModel.Text              = String.Empty;
            this.txtFPPosition.Text         = String.Empty;
            this.txtFP.Text                 = String.Empty;
            this.txtModeDetails.Text        = String.Empty;
        }
        void BindEdIdDetails()
        {
            EdIdServices m_Service = new EdIdServices();
            if (!String.IsNullOrEmpty(SelectedNode) && EdIdWithFileStrcuture != null)
            {
                DataRow[] dr = EdIdWithFileStrcuture.Select("SlNo='"+ SelectedNode +"'");
                if (dr.Length > 0)
                {
                    Byte[] tempBinData =(Byte[]) dr[0][1];
                    if (tempBinData.Length > 0)
                    {
                        EDIDReader m_EdIdReader = new EDIDReader();
                        m_EdIdReader.ReadBinFile(tempBinData);
                        this.txtEDIDData.Text = m_EdIdReader.GetEdidData();
                        this.txtOSDData.Text = m_EdIdReader.GetOSDData();
                        this.txtEDIDChecksum.Text = m_Service.GetFingerPrint(m_EdIdReader.GetEDIDChecksum());
                        this.txtOSDChecksum.Text = m_Service.GetFingerPrint(m_EdIdReader.GetOSDChecksum());
                        this.txtCombinedChecksum.Text = m_Service.GetFingerPrint(m_EdIdReader.GetCombinedChecksum());
                        this.txtBrand.Text = this.SelectedBrand;
                        this.txtModel.Text = this.SelectedModel;
                        this.txtFPPosition.Text = m_EdIdReader.GetCustomFPBitPositions();
                        this.txtFP.Text = m_Service.GetFingerPrint(m_EdIdReader.GetCustomFP());
                        this.txtModeDetails.Text = String.Empty;
                        String[] sep = { " ", ",", "\t" };
                        String[] data = this.txtEDIDData.Text.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                        List<Byte> _ediddata = new List<Byte>();
                        foreach (String bytedata in data)
                        {
                            _ediddata.Add((byte)Convert.ToInt32(bytedata, 16));
                        }
                        if (_ediddata.Count > 0)
                        {
                            this.EDIDDetails = m_Service.PopulateEdid(_ediddata);
                        }
                    }                    
                }
            }
        }                
        #endregion

        #region Background Progress
        void m_BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_Progress.DialogResult = DialogResult.OK;
            m_Progress.Close();
            this.treeView.Nodes.Clear();
            if (this.EdIdData != null)
            {
                BindTreeView();
                this.btnExcel.Enabled = true;               
            }
            else
            {
                this.btnExcel.Enabled = false;
                MessageBox.Show("No Data Found", "EDID", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        void m_BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            GetEdIdData();
        }
        private void ResetTimer()
        {
            if (m_BackgroundWorker != null || m_BackgroundWorker.IsBusy == false)
                m_BackgroundWorker.RunWorkerAsync();

            if (m_Progress == null)
            {
                m_Progress = new ProgressForm();
                m_Progress.StartPosition = FormStartPosition.CenterParent;
            }
            m_Progress.SetMessage("Please Wait...");
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();            
        }
        #endregion

        #region Bind Tree View
        private void BindTreeView()
        {
            this.treeView.Nodes.Clear();
            if (this.EdIdData != null)
            {
                foreach (EDIDInfo item in this.EdIdData)
                {
                    if ((!String.IsNullOrEmpty(item.EdIdRawData)) && (!item.MainDevice.Contains("IDBased_Device")))
                    {
                    if (!this.treeView.Nodes.ContainsKey(item.MainDevice))
                    {
                        this.treeView.Nodes.Add(item.MainDevice, item.MainDevice);
                    }
                    if (!this.treeView.Nodes[item.MainDevice].Nodes.ContainsKey(item.SubDevice))
                    {
                        this.treeView.Nodes[item.MainDevice].Nodes.Add(item.SubDevice, item.SubDevice);
                    }
                    if (!String.IsNullOrEmpty(item.Component))
                    {
                        if (!this.treeView.Nodes[item.MainDevice].Nodes[item.SubDevice].Nodes.ContainsKey(item.Component))
                        {
                            this.treeView.Nodes[item.MainDevice].Nodes[item.SubDevice].Nodes.Add(item.Component, item.Component);
                        }
                        if (!String.IsNullOrEmpty(item.Brand))
                        {
                            if (!this.treeView.Nodes[item.MainDevice].Nodes[item.SubDevice].Nodes[item.Component].Nodes.ContainsKey(item.Brand))
                            {
                                this.treeView.Nodes[item.MainDevice].Nodes[item.SubDevice].Nodes[item.Component].Nodes.Add(item.Brand, item.Brand);
                            }
                            if (!String.IsNullOrEmpty(item.Model))
                            {
                                if (!this.treeView.Nodes[item.MainDevice].Nodes[item.SubDevice].Nodes[item.Component].Nodes[item.Brand].Nodes.ContainsKey(item.Model))
                                {
                                    this.treeView.Nodes[item.MainDevice].Nodes[item.SubDevice].Nodes[item.Component].Nodes[item.Brand].Nodes.Add(item.Model, item.Model);
                                    this.treeView.Nodes[item.MainDevice].Nodes[item.SubDevice].Nodes[item.Component].Nodes[item.Brand].Nodes[item.Model].Tag = "Model," + item.SlNo.ToString();
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!this.treeView.Nodes[item.MainDevice].Nodes[item.SubDevice].Nodes.ContainsKey("-1"))
                        {
                            this.treeView.Nodes[item.MainDevice].Nodes[item.SubDevice].Nodes.Add("-1", String.Empty);
                        }
                        if (!String.IsNullOrEmpty(item.Brand))
                        {
                            if (!this.treeView.Nodes[item.MainDevice].Nodes[item.SubDevice].Nodes["-1"].Nodes.ContainsKey(item.Brand))
                            {
                                this.treeView.Nodes[item.MainDevice].Nodes[item.SubDevice].Nodes["-1"].Nodes.Add(item.Brand, item.Brand);
                            }
                            if (!String.IsNullOrEmpty(item.Model))
                            {
                                if (!this.treeView.Nodes[item.MainDevice].Nodes[item.SubDevice].Nodes["-1"].Nodes[item.Brand].Nodes.ContainsKey(item.Model))
                                {
                                    this.treeView.Nodes[item.MainDevice].Nodes[item.SubDevice].Nodes["-1"].Nodes[item.Brand].Nodes.Add(item.Model, item.Model);
                                    this.treeView.Nodes[item.MainDevice].Nodes[item.SubDevice].Nodes["-1"].Nodes[item.Brand].Nodes[item.Model].Tag = "Model," + item.SlNo.ToString();
                                }
                            }
                        }
                    }
                }
                }
            }
        }
        #endregion

        #region Get ID List
        private IList<String> GetIDs()
        {
            IList<String> idList = null;
            IDSelectionForm idSelectionForm = null;
            try
            {
                //Use id selection form object
                idSelectionForm = new IDSelectionForm();
                DialogResult result = idSelectionForm.ShowDialog();

                if (result == DialogResult.OK)
                {
                    m_SelectedType = idSelectionForm.SelectedType;
                    idList = idSelectionForm.SelectedIDList;
                    if (idList == null || idList.Count == 0)
                        MessageBox.Show("No ids retrieved for the selection.");
                }
                return idList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idSelectionForm != null)
                {
                    idSelectionForm.Dispose();
                }
            }
        }
        #endregion

        #region Get EdId Data
        private void GetEdIdData()
        {
            EdIdServices m_GetEdIdData = new EdIdServices();
            DataTable resultCollection = null;
            //EdIdData = m_GetEdIdData.GetEdIdData(m_Regions, m_SelectedModes, m_DeviceTypes, m_IDSetupCodeInfoParams,m_BrandFilter,m_ModelFilter, ref resultCollection);
            this.EdIdWithFileStrcuture = resultCollection;
        }
        #endregion

        #region Export To Excel
        private DataSet dsDataSet = new DataSet("EDID Summary");
        private void CreateEdIdDataTable()
        {
            if (EdIdDataTable == null)
            {
                EdIdDataTable = new DataTable("EDID");
                EdIdDataTable.Columns.Add("SL NO");
                EdIdDataTable.Columns.Add("Main Device");
                EdIdDataTable.Columns.Add("Sub Device");
                EdIdDataTable.Columns.Add("Component");
                EdIdDataTable.Columns.Add("Region");
                EdIdDataTable.Columns.Add("Brand");
                EdIdDataTable.Columns.Add("Model");
                EdIdDataTable.Columns.Add("EDID Block");
                EdIdDataTable.Columns.Add("Manufacturer (3 char) code");
                EdIdDataTable.Columns.Add("Manufacturer Hex Code");
                EdIdDataTable.Columns.Add("Monitor Name");
                EdIdDataTable.Columns.Add("Product Code");
                EdIdDataTable.Columns.Add("Serial number");
                EdIdDataTable.Columns.Add("Week");
                EdIdDataTable.Columns.Add("Year");
                EdIdDataTable.Columns.Add("Video input definition");
                EdIdDataTable.Columns.Add("OSD Name");                
                EdIdDataTable.Columns.Add("FP Available");
                EdIdDataTable.Columns.Add("FP Bytes");
                EdIdDataTable.Columns.Add("Custom FP");
                EdIdDataTable.Columns.Add("Custom FP + OSD");
                EdIdDataTable.Columns.Add("128 FP");
                EdIdDataTable.Columns.Add("128 FP + OSD");
                EdIdDataTable.Columns.Add("OSD FP");
                
            }
            else
                EdIdDataTable.Clear();
        }
        private void FillEdIdDataTableDate()
        {
            EdIdServices m_Service = new EdIdServices();
            if (this.EdIdData != null)
            {
                DataRow dr = null;
                foreach (EDIDInfo item in this.EdIdData)
                {
                    dr = EdIdDataTable.NewRow();
                    dr["SL NO"] = item.SlNo;
                    dr["Main Device"] = item.MainDevice;
                    dr["Sub Device"] = item.SubDevice;
                    dr["Component"] = item.Component;
                    dr["Region"] = item.Region;
                    dr["Brand"] = item.Brand;
                    dr["Model"] = item.Model;
                    dr["EDID Block"] = item.EdIdRawData;
                    dr["Manufacturer (3 char) code"] = item.ManufacturerCode;
                    dr["Manufacturer Hex Code"] = item.ManufacturerHexCode;
                    dr["Monitor Name"] = item.ManufacturerName;
                    dr["Product Code"] = item.ProductCode;
                    dr["Serial number"] = item.SerialNumber;
                    dr["Week"] = item.Week;
                    dr["Year"] = item.Year;
                    dr["Video input definition"] = item.VideoInputDefinition;
                    dr["OSD Name"] = item.OSDRawData;                    
                    dr["FP Available"] = item.FPAvailable;
                    dr["FP Bytes"] = item.FPBytePosition;
                    
                    DataRow[] dr1 = EdIdWithFileStrcuture.Select("SlNo='" + item.SlNo + "'");
                    if (dr1.Length > 0)
                    {
                        Byte[] tempBinData = (Byte[])dr1[0][1];
                        if (tempBinData.Length > 0)
                        {
                            try
                            {
                                EDIDReader m_EdIdReader = new EDIDReader();
                                m_EdIdReader.ReadBinFile(tempBinData);
                                m_EdIdReader.GetEdidData();
                                m_EdIdReader.GetOSDData();

                                dr["Custom FP"] = m_Service.GetFingerPintCheckSum(item, false);
                                if (item.OSD != null)
                                {
                                    if (item.OSD.Count > 0)
                                        dr["Custom FP + OSD"] = m_Service.GetFingerPintCheckSum(item, true);
                                }
                                dr["128 FP"] = m_Service.GetFingerPrint(m_EdIdReader.GetEDIDChecksum());
                                dr["128 FP + OSD"] = m_Service.GetFingerPrint(m_EdIdReader.GetCombinedChecksum());
                                dr["OSD FP"] = m_Service.GetFingerPrint(m_EdIdReader.GetOSDChecksum());
                            }
                            catch
                            {
                                dr["Custom FP"] = String.Empty;
                                dr["Custom FP + OSD"] = String.Empty;
                                dr["128 FP"] = String.Empty;
                                dr["128 FP + OSD"] = String.Empty;
                                dr["OSD FP"] = String.Empty;
                            }
                        }
                    }
                    
                    this.EdIdDataTable.Rows.Add(dr);
                    dr = null;
                }
                this.EdIdDataTable.AcceptChanges();
            }
        }
        private void btnExcel_Click(object sender, EventArgs e)
        {
            if (EdIdData != null)
            {
                CreateEdIdDataTable();
                FillEdIdDataTableDate();
                String m_SaveFileMessage = String.Empty;
                if (dsDataSet.Tables.Contains(EdIdDataTable.TableName))
                    dsDataSet.Tables.Remove(EdIdDataTable.TableName);
                dsDataSet.Tables.Add(EdIdDataTable);
                try
                {
                    String m_filePath = CommonForms.Configuration.GetWorkingDirectory();
                    ExcelReportGenerator.GenerateReport(dsDataSet, m_filePath, "EDID Summary", out m_SaveFileMessage);
                    if (!String.IsNullOrEmpty(m_SaveFileMessage))
                    {
                        MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        #endregion
    }
}