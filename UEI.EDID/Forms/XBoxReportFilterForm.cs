﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace UEI.EDID.Forms
{
    public partial class XBoxReportFilterForm : Form
    {
        #region Variables
        private EdIdServices m_Service = null;
        private ProgressForm m_Progress = null;
        private Timer m_Timer = null;
        private BackgroundWorker m_BackgroundWorker = null;
        #endregion

        #region Properties
        private String m_StatusMessage = String.Empty;
        public String StatusMessage
        {
            get { return m_StatusMessage; }
            set { m_StatusMessage = value; }
        }
        public List<String> DeviceTypes { get; set; }
        public List<String> Brands { get; set; }
        public List<String> Countries { get; set; }
        public List<String> DataSources { get; set; }
        public List<String> SelectedDeviceType { get; set; }
        public List<String> SelectedBrand { get; set; }
        public List<String> SelectedCountries { get; set; }
        public List<String> SelectedDataSources { get; set; }
        public List<String> SelectedModel { get; set; }
        public String       SelectedEdIdExcelFile { get; set; }
        public String       ReportName { get; set; }
        private Boolean m_NewFPRule = true;
        public Boolean NewFPRule
        {
            get { return m_NewFPRule; }
            set { m_NewFPRule = value; }
        }
        private FingerPrintTypes m_FPType = FingerPrintTypes.NotSet;
        public FingerPrintTypes FPType
        {
            get { return m_FPType; }
            set { m_FPType = value; }
        }
        #endregion

        #region Constructor
        public XBoxReportFilterForm()
        {
            InitializeComponent();
            this.Load += XBoxReportFilterForm_Load;
        }
        void XBoxReportFilterForm_Load(object sender, EventArgs e)
        {
            switch (this.ReportName)
            {
                case "XBoxReport":
                    //tabControl1.Controls.Remove(tabOthers);
                    tabControl1.Controls.Remove(tabModels);
                    break;
                case "XBoxCluterReport":
                    //tabControl1.Controls.Remove(tabCountries);
                    break;
            }
            this.m_BackgroundWorker = new BackgroundWorker();
            this.m_BackgroundWorker.DoWork += m_BackgroundWorker_DoWork;
            this.m_BackgroundWorker.RunWorkerCompleted += m_BackgroundWorker_RunWorkerCompleted;
            this.m_Timer = new System.Windows.Forms.Timer();
            this.m_Timer.Enabled = true;
            this.m_Timer.Interval = 100;
            this.m_Timer.Tick += m_Timer_Tick;
            this.btnCancel.Click += btnCancel_Click;
            this.btnOK.Click += btnOK_Click;
            this.m_Service = new EdIdServices(DBConnectionString.EdId);
            this.btnBrandBrowse.Click += btnBrandBrowse_Click;
            this.btnBrowseModel.Click += btnModelBrowse_Click;
            this.btnAddBrand.Click += btnAddBrand_Click;
            this.btnRemoveBrand.Click += btnRemoveBrand_Click;
            this.btnAddModel.Click += btnAddModel_Click;
            this.btnRemoveModel.Click += btnRemoveModel_Click;
            this.txtBrand.KeyUp += txtBrand_KeyUp;
            this.txtModel.KeyUp += txtModel_KeyUp;
            ResetTimer();
            //tabControl1.TabPages.Remove(tabCountries);
            this.btnDownload.Click += btnDownload_Click;
            this.btnBrowseEDIDExcelFile.Click += btnBrowseEDIDExcelFile_Click;
            this.chkNewFPRule.CheckedChanged += chkNewFPRule_CheckedChanged;
            this.rdoFP0x00.CheckedChanged += rdoFP0x00_CheckedChanged;
            this.rdoFP0x01.CheckedChanged += rdoFP0x01_CheckedChanged;
            this.rdoFP0x02.CheckedChanged += rdoFP0x02_CheckedChanged;
            this.rdoFP0x03.CheckedChanged += rdoFP0x03_CheckedChanged;
            this.rdoFP.CheckedChanged += rdoFP_CheckedChanged;
            this.rdoFP128.CheckedChanged += rdoFP128_CheckedChanged;
        }      
                       
        #endregion

        #region Fingerprint Type Selection
        void rdoFP128_CheckedChanged(object sender, EventArgs e)
        {
            this.FPType = FingerPrintTypes.Only128FP;
        }
        void rdoFP_CheckedChanged(object sender, EventArgs e)
        {
            this.FPType = FingerPrintTypes.CustomFP;
        }
        void rdoFP0x03_CheckedChanged(object sender, EventArgs e)
        {
            this.FPType = FingerPrintTypes.CustomFPType0x03;
        }
        void rdoFP0x02_CheckedChanged(object sender, EventArgs e)
        {
            this.FPType = FingerPrintTypes.CustomFPType0x02;
        }
        void rdoFP0x01_CheckedChanged(object sender, EventArgs e)
        {
            this.FPType = FingerPrintTypes.CustomFPType0x01;
        }
        void rdoFP0x00_CheckedChanged(object sender, EventArgs e)
        {
            this.FPType = FingerPrintTypes.CustomFPType0x00;
        }
        #endregion

        #region Event
        void chkNewFPRule_CheckedChanged(object sender, EventArgs e)
        {
            this.NewFPRule = this.chkNewFPRule.Checked;
        } 
        void btnBrowseEDIDExcelFile_Click(object sender, EventArgs e)
        {
            this.SelectedEdIdExcelFile = String.Empty;
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.Title = "Select EDID Excel File";
            openfile.Filter = "Excel File (*.xls,*.xlsx)|*.xls;*.xlsx";

            if (openfile.ShowDialog() == DialogResult.OK)
            {
                this.SelectedEdIdExcelFile = openfile.FileName;
                this.txtEDIDBase64ExcelFile.Text = openfile.FileName;
            }
            else
            {
                this.SelectedEdIdExcelFile = String.Empty;
            }
        }
        void btnDownload_Click(object sender, EventArgs e)
        {
            SaveFileDialog m_SaveFileDialog = new SaveFileDialog();
            m_SaveFileDialog.Title = "EDID Excel File Format";
            m_SaveFileDialog.Filter = "Excel File (*.xlsx)|*.xlsx";
            m_SaveFileDialog.FileName = "InputFile";
            m_SaveFileDialog.CheckPathExists = true;
            if (m_SaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileStream m_FileStream = null;
                try
                {
                    m_FileStream = new FileStream(m_SaveFileDialog.FileName, FileMode.Create);
                    m_FileStream.Write(UEI.EDID.Properties.Resources.ClusterFileFormat, 0, UEI.EDID.Properties.Resources.ClusterFileFormat.Length);
                    m_FileStream.Close();
                }
                catch (Exception ex)
                {
                    m_FileStream.Close();
                    MessageBox.Show(ex.Message, "EDID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        void txtModel_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                String userEntry = this.txtModel.Text.Trim();
                if (!String.IsNullOrEmpty(userEntry))
                {
                    if (!this.lstModels.Items.Contains(userEntry))
                    {
                        this.lstModels.Items.Add(userEntry);
                        this.txtModel.Text = String.Empty;
                    }
                }
            }
        }
        void txtBrand_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string userEntry = this.txtBrand.Text.Trim();
                if (!String.IsNullOrEmpty(userEntry))
                {
                    if (!this.lstBrands.Items.Contains(userEntry))
                    {
                        this.lstBrands.Items.Add(userEntry);
                        this.txtBrand.Text = String.Empty;
                    }
                }
            }
        }
        void btnRemoveModel_Click(object sender, EventArgs e)
        {
            while (this.lstModels.SelectedItems.Count > 0)
            {
                this.lstModels.Items.Remove(this.lstModels.SelectedItem);
            }
        }
        void btnAddModel_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(this.txtModel.Text.Trim()))
            {
                if (!this.lstModels.Items.Contains(this.txtModel.Text.Trim()))
                {
                    this.lstModels.Items.Add(this.txtModel.Text.Trim());
                    this.txtModel.Text = String.Empty;
                }
            }
        }
        void btnRemoveBrand_Click(object sender, EventArgs e)
        {
            while (this.lstBrands.SelectedItems.Count > 0)
            {
                this.lstBrands.Items.Remove(this.lstBrands.SelectedItem);
            }
        }
        void btnAddBrand_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(this.txtBrand.Text.Trim()))
            {
                if (!this.lstBrands.Items.Contains(this.txtBrand.Text.Trim()))
                {
                    this.lstBrands.Items.Add(this.txtBrand.Text.Trim());
                    this.txtBrand.Text = String.Empty;
                }
            }
        }
        void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        void btnOK_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
        void btnModelBrowse_Click(object sender, EventArgs e)
        {
            List<String> m_ModelList = LoadFromFile();
            if (m_ModelList.Count > 0)
                foreach (String item in m_ModelList)
                {
                    if (!String.IsNullOrEmpty(item))
                    {
                        if (!lstModels.Items.Contains(item))
                            lstModels.Items.Add(item);
                    }
                }
        }
        void btnBrandBrowse_Click(object sender, EventArgs e)
        {
            List<String> m_BrandList = LoadFromFile();
            if (m_BrandList.Count > 0)
                foreach (String item in m_BrandList)
                {
                    if (!String.IsNullOrEmpty(item))
                    {
                        if (!lstBrands.Items.Contains(item))
                            lstBrands.Items.Add(item);
                    }
                }
        }
        #endregion

        #region Load From File
        private List<String> LoadFromFile()
        {
            try
            {
                List<String> dataList = new List<String>();

                using (OpenFileDialog fileDialog = new OpenFileDialog())
                {
                    fileDialog.RestoreDirectory = true;
                    fileDialog.Filter = "Text file (*.txt)|*.txt";
                    fileDialog.DefaultExt = "txt";
                    if (fileDialog.ShowDialog() == DialogResult.OK)
                    {
                        String filePath = fileDialog.FileName;

                        FileInfo fileInfo = new FileInfo(filePath);
                        StreamReader reader = new StreamReader(fileInfo.FullName, Encoding.Default);// fileInfo.OpenText();

                        if (reader != null)
                        {
                            string data = String.Empty;
                            do
                            {
                                data = reader.ReadLine();

                                if (!String.IsNullOrEmpty(data.Trim()))
                                    dataList.Add(data);

                            } while (!reader.EndOfStream);
                            reader.Close();
                        }

                    }
                }
                return dataList;
            }
            catch
            {
                return new List<string>();
            }
        }        
        #endregion        

        #region Validate
        Boolean ValidateForm()
        {
            this.SelectedBrand = null;
            this.SelectedDeviceType = null;
            this.SelectedModel = null;
            Boolean isValid = true;
            if (!String.IsNullOrEmpty(this.txtEDIDBase64ExcelFile.Text.Trim()))
                return true;
            if (chkDeviceTypes.CheckedItems.Count == 0)
            {
                MessageBox.Show("Please select one or more Devices", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
            {
                this.SelectedDeviceType = new List<String>();
                foreach (String item in this.chkDeviceTypes.CheckedItems)
                {
                    this.SelectedDeviceType.Add(item);
                }
            }
            if (chkDataSources.CheckedItems.Count == 0)
            {
                MessageBox.Show("Please select one or more Data Sources(M)", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
            {
                this.SelectedDataSources = new List<String>();
                foreach (String item in this.chkDataSources.CheckedItems)
                {
                    this.SelectedDataSources.Add(item);
                }
            }
            if (lstBrands.Items.Count > 0)
            {
                this.SelectedBrand = new List<String>();
                foreach (String item in this.lstBrands.Items)
                {
                    this.SelectedBrand.Add(item);
                }
            }
            else
            {
                if (this.ReportName == "XBoxCluterReport")
                {
                    MessageBox.Show("Please select one or more Brands", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }                       
            }
            
            if (this.FPType == FingerPrintTypes.NotSet)
            {
                MessageBox.Show("Please select Fingerprint Type for Linking ID from Captured EDID", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            
            if (this.lstModels.Items.Count > 0)
            {
                this.SelectedModel = new List<String>();
                foreach (String item in this.lstModels.Items)
                {
                    this.SelectedModel.Add(item);
                }
            }             
            if (chkCountries.CheckedItems.Count > 0)
            {
                this.SelectedCountries = new List<String>();
                foreach (String item in this.chkCountries.CheckedItems)
                {
                    this.SelectedCountries.Add(item);
                }
            }
            return isValid;
        }
        #endregion

        #region Device Types
        void LoadDeviceTypes()
        {
            this.SelectedDataSources = new List<String>();
            foreach (String item in this.chkDataSources.CheckedItems)
            {
                this.SelectedDataSources.Add(item);
            }
            if (this.SelectedDataSources.Count > 0)
            {
                this.DeviceTypes = m_Service.GetAllXBoxConsoleDevices(this.SelectedDataSources);
            }
            else
            {
                MessageBox.Show("Please select a Data Source", "EDID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void BindDeviceTypes()
        {
            this.chkDeviceTypes.Items.Clear();
            if (this.DeviceTypes != null)
            {
                foreach (String item in this.DeviceTypes)
                {
                    this.chkDeviceTypes.Items.Add(item);
                }
            }
        }
        #endregion        

        #region Data Sources
        void LoadDataSources()
        {
            this.DataSources = m_Service.GetAllDataSources();
        }
        void BindDataSources()
        {
            this.chkDataSources.Items.Clear();
            if (this.chkDataSources != null)
            {
                foreach (String item in this.DataSources)
                {
                    if (item.Contains("Telemetry EDID"))
                        this.chkDataSources.Items.Add(item,true);
                }
            }
        }
        #endregion

        #region Contries
        void LoadCountries()
        {
            this.Countries = m_Service.GetAllXBoxConsoleCountries();
        }
        void BindCountries()
        {
            this.chkCountries.Items.Clear();
            if (this.chkCountries != null)
            {
                foreach (String item in this.Countries)
                {
                    this.chkCountries.Items.Add(item);
                }
            }
        }
        #endregion

        #region Load Brands
        void LoadBrands()
        {
            this.Brands = m_Service.GetAllXBoxConsoleBrands();
        }
        void BindBrands()
        {
            if (this.Brands != null)
            {
                txtBrand.AutoCompleteMode = AutoCompleteMode.Suggest;
                txtBrand.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txtBrand.AutoCompleteCustomSource.AddRange(this.Brands.ToArray());
            }
        }
        #endregion

        #region Progress
        private void ResetTimer()
        {
            if (m_BackgroundWorker != null || m_BackgroundWorker.IsBusy == false)
                m_BackgroundWorker.RunWorkerAsync();

            if (m_Progress == null)
                m_Progress = new ProgressForm();
            m_Progress.SetMessage("Please Wait...", "");
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();
            m_Timer.Start();
        }
        void m_Timer_Tick(object sender, EventArgs e)
        {
            if (this.m_Progress != null)
            {
                this.m_Progress.SetMessage("Please Wait...", this.StatusMessage);
            }
        }
        void m_BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.m_Timer.Stop();
            this.m_Progress.DialogResult = DialogResult.OK;
            this.m_Progress.Close();
            if (m_Service.ErrorMessage != String.Empty)
            {
                MessageBox.Show(m_Service.ErrorMessage, "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                BindBrands();
                BindCountries();
                BindDataSources();
            }
        }
        void m_BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            LoadBrands();          
            LoadCountries();
            LoadDataSources();
        }
        #endregion

        #region Tab Selection
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this.tabControl1.SelectedTab.Text)
            {
                case "Devices":
                    if (this.chkDeviceTypes.Items.Count == 0)
                    {
                        LoadDeviceTypes();
                        BindDeviceTypes();
                    }
                    break;
            }
        }
        #endregion
    }
}