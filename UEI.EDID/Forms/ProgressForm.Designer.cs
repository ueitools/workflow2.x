﻿namespace UEI.EDID.Forms
{
    partial class ProgressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMsg1 = new System.Windows.Forms.Label();
            this.Cancel = new System.Windows.Forms.Button();
            this.Process = new System.Windows.Forms.Label();
            this.progress1 = new UEI.EDID.Forms.Progress();
            this.lblMsg2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblMsg1
            // 
            this.lblMsg1.Location = new System.Drawing.Point(73, 38);
            this.lblMsg1.Name = "lblMsg1";
            this.lblMsg1.Size = new System.Drawing.Size(244, 13);
            this.lblMsg1.TabIndex = 16;
            this.lblMsg1.Text = "label1";
            // 
            // Cancel
            // 
            this.Cancel.AutoSize = true;
            this.Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancel.Location = new System.Drawing.Point(73, 91);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(50, 23);
            this.Cancel.TabIndex = 15;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // Process
            // 
            this.Process.AutoSize = true;
            this.Process.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Process.Location = new System.Drawing.Point(20, 39);
            this.Process.Name = "Process";
            this.Process.Size = new System.Drawing.Size(48, 13);
            this.Process.TabIndex = 14;
            this.Process.Text = "Process:";
            // 
            // progress1
            // 
            this.progress1.AutoProgress = true;
            this.progress1.IndicatorColor = System.Drawing.Color.LightGreen;
            this.progress1.Location = new System.Drawing.Point(12, 13);
            this.progress1.Name = "progress1";
            this.progress1.Position = 13;
            this.progress1.ProgressBoxStyle = UEI.EDID.Forms.Progress.ProgressBoxStyleConstants.SOLIDSMALLER;
            this.progress1.ShowBorder = false;
            this.progress1.Size = new System.Drawing.Size(305, 17);
            this.progress1.TabIndex = 17;
            // 
            // lblMsg2
            // 
            this.lblMsg2.Location = new System.Drawing.Point(73, 64);
            this.lblMsg2.Name = "lblMsg2";
            this.lblMsg2.Size = new System.Drawing.Size(244, 13);
            this.lblMsg2.TabIndex = 18;
            this.lblMsg2.Text = "label1";
            // 
            // ProgressForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(327, 149);
            this.Controls.Add(this.lblMsg2);
            this.Controls.Add(this.progress1);
            this.Controls.Add(this.lblMsg1);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.Process);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ProgressForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Progress";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMsg1;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Label Process;
        private Progress progress1;
        private System.Windows.Forms.Label lblMsg2;
    }
}