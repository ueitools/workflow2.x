﻿namespace UEI.EDID.Forms
{
    partial class EDIDViewerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnConvertToBase64 = new System.Windows.Forms.Button();
            this.btnCompare = new System.Windows.Forms.Button();
            this.btnFillData = new System.Windows.Forms.Button();
            this.btnDecodeXBoxEDID = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDownload = new System.Windows.Forms.Button();
            this.btnEDIDExcelFile = new System.Windows.Forms.Button();
            this.btnMigrate = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtBase64EDIDHexDatainBin = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnCreateBin = new System.Windows.Forms.Button();
            this.btnBin = new System.Windows.Forms.Button();
            this.txtBase64EDIDData = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnGetEDIDInfo = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.propertyGrid = new System.Windows.Forms.PropertyGrid();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 459);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(682, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(682, 459);
            this.splitContainer1.SplitterDistance = 401;
            this.splitContainer1.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnConvertToBase64);
            this.groupBox1.Controls.Add(this.btnCompare);
            this.groupBox1.Controls.Add(this.btnFillData);
            this.groupBox1.Controls.Add(this.btnDecodeXBoxEDID);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnDownload);
            this.groupBox1.Controls.Add(this.btnEDIDExcelFile);
            this.groupBox1.Controls.Add(this.btnMigrate);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(401, 459);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // btnConvertToBase64
            // 
            this.btnConvertToBase64.Location = new System.Drawing.Point(236, 405);
            this.btnConvertToBase64.Name = "btnConvertToBase64";
            this.btnConvertToBase64.Size = new System.Drawing.Size(156, 23);
            this.btnConvertToBase64.TabIndex = 22;
            this.btnConvertToBase64.Text = "Convert To Base64";
            this.btnConvertToBase64.UseVisualStyleBackColor = true;
            // 
            // btnCompare
            // 
            this.btnCompare.Location = new System.Drawing.Point(309, 263);
            this.btnCompare.Name = "btnCompare";
            this.btnCompare.Size = new System.Drawing.Size(65, 23);
            this.btnCompare.TabIndex = 21;
            this.btnCompare.Text = "Compare";
            this.btnCompare.UseVisualStyleBackColor = true;
            // 
            // btnFillData
            // 
            this.btnFillData.Location = new System.Drawing.Point(238, 263);
            this.btnFillData.Name = "btnFillData";
            this.btnFillData.Size = new System.Drawing.Size(65, 23);
            this.btnFillData.TabIndex = 20;
            this.btnFillData.Text = "Fill Data";
            this.btnFillData.UseVisualStyleBackColor = true;
            // 
            // btnDecodeXBoxEDID
            // 
            this.btnDecodeXBoxEDID.Location = new System.Drawing.Point(236, 363);
            this.btnDecodeXBoxEDID.Name = "btnDecodeXBoxEDID";
            this.btnDecodeXBoxEDID.Size = new System.Drawing.Size(156, 23);
            this.btnDecodeXBoxEDID.TabIndex = 19;
            this.btnDecodeXBoxEDID.Text = "Decode XBox EDID";
            this.btnDecodeXBoxEDID.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 360);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "EDID Block Brand Profile";
            // 
            // btnDownload
            // 
            this.btnDownload.Location = new System.Drawing.Point(238, 292);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(154, 65);
            this.btnDownload.TabIndex = 15;
            this.btnDownload.Text = "Download Input File Format";
            this.btnDownload.UseVisualStyleBackColor = true;
            // 
            // btnEDIDExcelFile
            // 
            this.btnEDIDExcelFile.Location = new System.Drawing.Point(6, 376);
            this.btnEDIDExcelFile.Name = "btnEDIDExcelFile";
            this.btnEDIDExcelFile.Size = new System.Drawing.Size(156, 23);
            this.btnEDIDExcelFile.TabIndex = 14;
            this.btnEDIDExcelFile.Text = "Browse Brand Profile EDID Excel file";
            this.btnEDIDExcelFile.UseVisualStyleBackColor = true;
            // 
            // btnMigrate
            // 
            this.btnMigrate.Location = new System.Drawing.Point(6, 405);
            this.btnMigrate.Name = "btnMigrate";
            this.btnMigrate.Size = new System.Drawing.Size(212, 23);
            this.btnMigrate.TabIndex = 13;
            this.btnMigrate.Text = "Migrate Captured EDID to EDID Staging";
            this.btnMigrate.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 276);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "EDID (Hex Fromat) Block";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 318);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "EDID (Base 64 Format) Block";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 292);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Browse XBox EDID Excel file";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtBase64EDIDHexDatainBin);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(3, 141);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(395, 116);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Byte Data of Base 64 EDID Data";
            // 
            // txtBase64EDIDHexDatainBin
            // 
            this.txtBase64EDIDHexDatainBin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBase64EDIDHexDatainBin.Location = new System.Drawing.Point(3, 16);
            this.txtBase64EDIDHexDatainBin.Multiline = true;
            this.txtBase64EDIDHexDatainBin.Name = "txtBase64EDIDHexDatainBin";
            this.txtBase64EDIDHexDatainBin.Size = new System.Drawing.Size(389, 97);
            this.txtBase64EDIDHexDatainBin.TabIndex = 7;
            this.txtBase64EDIDHexDatainBin.TextChanged += new System.EventHandler(this.txtBase64EDIDHexDatainBin_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnCreateBin);
            this.groupBox3.Controls.Add(this.btnBin);
            this.groupBox3.Controls.Add(this.txtBase64EDIDData);
            this.groupBox3.Controls.Add(this.btnClear);
            this.groupBox3.Controls.Add(this.btnGetEDIDInfo);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(3, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(395, 125);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Enter Base 64 EDID Data";
            // 
            // btnCreateBin
            // 
            this.btnCreateBin.Location = new System.Drawing.Point(297, 96);
            this.btnCreateBin.Name = "btnCreateBin";
            this.btnCreateBin.Size = new System.Drawing.Size(92, 23);
            this.btnCreateBin.TabIndex = 5;
            this.btnCreateBin.Text = "Create Bin";
            this.btnCreateBin.UseVisualStyleBackColor = true;
            // 
            // btnBin
            // 
            this.btnBin.Location = new System.Drawing.Point(297, 72);
            this.btnBin.Name = "btnBin";
            this.btnBin.Size = new System.Drawing.Size(92, 23);
            this.btnBin.TabIndex = 4;
            this.btnBin.Text = "Bin File";
            this.btnBin.UseVisualStyleBackColor = true;
            // 
            // txtBase64EDIDData
            // 
            this.txtBase64EDIDData.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtBase64EDIDData.Location = new System.Drawing.Point(3, 16);
            this.txtBase64EDIDData.Multiline = true;
            this.txtBase64EDIDData.Name = "txtBase64EDIDData";
            this.txtBase64EDIDData.Size = new System.Drawing.Size(288, 106);
            this.txtBase64EDIDData.TabIndex = 1;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(297, 43);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(58, 23);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // btnGetEDIDInfo
            // 
            this.btnGetEDIDInfo.Location = new System.Drawing.Point(297, 14);
            this.btnGetEDIDInfo.Name = "btnGetEDIDInfo";
            this.btnGetEDIDInfo.Size = new System.Drawing.Size(92, 23);
            this.btnGetEDIDInfo.TabIndex = 2;
            this.btnGetEDIDInfo.Text = "Get EDID Info";
            this.btnGetEDIDInfo.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(6, 334);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(156, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Browse XBox EDID Excel file";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox5);
            this.splitContainer2.Size = new System.Drawing.Size(277, 459);
            this.splitContainer2.SplitterDistance = 234;
            this.splitContainer2.TabIndex = 23;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.propertyGrid);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(277, 234);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "More Details";
            // 
            // propertyGrid
            // 
            this.propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid.Location = new System.Drawing.Point(3, 16);
            this.propertyGrid.Name = "propertyGrid";
            this.propertyGrid.Size = new System.Drawing.Size(271, 215);
            this.propertyGrid.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.propertyGrid1);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(277, 221);
            this.groupBox5.TabIndex = 23;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "EDID Info";
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.Location = new System.Drawing.Point(3, 16);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(271, 202);
            this.propertyGrid1.TabIndex = 0;
            // 
            // EDIDViewerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(682, 481);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "EDIDViewerForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "EDID Decode Form";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnGetEDIDInfo;
        private System.Windows.Forms.TextBox txtBase64EDIDData;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtBase64EDIDHexDatainBin;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnBin;
        private System.Windows.Forms.Button btnCreateBin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnMigrate;
        private System.Windows.Forms.Button btnEDIDExcelFile;
        private System.Windows.Forms.PropertyGrid propertyGrid;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDecodeXBoxEDID;
        private System.Windows.Forms.Button btnFillData;
        private System.Windows.Forms.Button btnCompare;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.Button btnConvertToBase64;

    }
}