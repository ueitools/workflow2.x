﻿using CommonForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using UEI.EDID.Forms;
using UEI.Workflow2010.Report.ExportToExcel;
namespace UEI.EDID
{
    public partial class MainForm : Form
    {
        #region Variables
        private ProgressForm m_Progress             = null;
        private BackgroundWorker m_BackgroundWorker = null;
        private ProcessType m_ProcessType           = ProcessType.UpdateDACPublishedIDforCapturedEDID;
        private Timer m_Timer                       = null;
        private EdIdServices m_Service              = null;
        #endregion

        #region Properties
        private String m_StatusMessage = "Please Wait...";
        public String StatusMessage
        {
            get { return m_StatusMessage; }
            set { m_StatusMessage = value; }
        }
        private String m_StatusMessage1 = String.Empty;
        public String StatusMessage1
        {
            get { return m_StatusMessage1; }
            set { m_StatusMessage1 = value; }
        }
        private String m_LinkedIDExcelFile = String.Empty;
        public String LinkedIDExcelFile
        {
            get { return m_LinkedIDExcelFile; }
            set { m_LinkedIDExcelFile = value; }
        }
        #endregion

        #region Constructor
        public MainForm()
        {
            InitializeComponent();
            this.Load += MainForm_Load;
        }
        void MainForm_Load(object sender, EventArgs e)
        {
            this.m_Service                              = new EdIdServices(BusinessObject.DBConnectionString.EdId);
            this.m_BackgroundWorker                     = new BackgroundWorker();
            this.m_BackgroundWorker.DoWork              += m_BackgroundWorker_DoWork;
            this.m_BackgroundWorker.RunWorkerCompleted  += m_BackgroundWorker_RunWorkerCompleted;
            this.m_Timer                                = new System.Windows.Forms.Timer();
            this.m_Timer.Enabled                        = true;
            this.m_Timer.Interval                       = 100;
            this.m_Timer.Tick                           += m_Timer_Tick;
            this.lblCopyRight.Text                      = String.Format("Copyright ©{0} UEIC", DateTime.Now.Year);
            this.Text                                   = String.Format("EDID Tool V {0}", System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).FileVersion);
        }
        #endregion

        #region Exit
        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Set Working Directory
        private void setWorkingDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CommonForms.Configuration.SetWorkingDirectory();
        }
        #endregion

        #region Import EDID Data
        private void importEDIDDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Forms.ImportForm m_ImportEdIdData = new Forms.ImportForm();
            m_ImportEdIdData.ShowDialog();
        }
        #endregion

        #region View EdId Data
        private void viewEDIDDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Forms.ViewEdIdForm m_ViewEdIdData = new Forms.ViewEdIdForm();
            Forms.EDIDView m_ViewEdIdData = new Forms.EDIDView();
            m_ViewEdIdData.MdiParent = this;
            m_ViewEdIdData.Show();
            if (m_ViewEdIdData.EdIdData == null)
            {
                m_ViewEdIdData.Close();               
            }
            else
            {
                m_ViewEdIdData.WindowState = FormWindowState.Maximized;
            }
        }        
        #endregion

        #region Import XBox EDID Data
        private void importXBoxEDIDDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Forms.ImportXBoxForm m_ImportEdIdData = new Forms.ImportXBoxForm();
            m_ImportEdIdData.ShowDialog();
        }
        #endregion

        #region EDID Viewer
        private void toolStripEDIDViewer_Click(object sender, EventArgs e)
        {
            Forms.EDIDViewerForm m_Open = new Forms.EDIDViewerForm();
            m_Open.ShowDialog(this);
        }
        #endregion

        #region XBox Report
        private void xBoxReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Forms.XBoxReportForm m_XBoxReport = new Forms.XBoxReportForm();
            m_XBoxReport.Dock = DockStyle.Fill;
            m_XBoxReport.MdiParent = this;
            m_XBoxReport.WindowState = FormWindowState.Maximized;
            m_XBoxReport.Show();             
        }
        #endregion

        private void eDIDGroupingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Forms.XBoxGroupingForm m_GroupReport = new Forms.XBoxGroupingForm();
            m_GroupReport.Dock = DockStyle.Fill;
            m_GroupReport.MdiParent = this;
            m_GroupReport.WindowState = FormWindowState.Maximized;
            m_GroupReport.Show();            
        }
        private void decodeEDIDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Forms.EDIDViewerForm m_Open = new Forms.EDIDViewerForm();
            m_Open.ShowDialog(this);
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            Forms.ImportCECForm m_Open = new Forms.ImportCECForm();
            m_Open.ShowDialog(this);
        }

        #region Variables
        private DataSet objDataSet = null;
        #endregion

        #region Update DAC Published ID
        private void forCaptureEDIDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.Title = "Select Captured EDID Excel File With LinkedID";
            openfile.Filter = "Excel File (*.xls,*.xlsx)|*.xls;*.xlsx";
            openfile.CheckFileExists = true;
            if (openfile.ShowDialog() == DialogResult.OK)
            {
                FileInfo m_File = new FileInfo(openfile.FileName);
                if (m_File.Extension.ToLower() == ".xls" || m_File.Extension.ToLower() == ".xlsx")
                {
                    this.LinkedIDExcelFile = openfile.FileName;
                    this.m_ProcessType = ProcessType.UpdateDACPublishedIDforCapturedEDID;
                    ResetTimer();
                }
            }
            else
                MessageBox.Show("Please select Captured EDID with Linked ID Excel File", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private Boolean CheckEmptyRow(DataRow m_Row)
        {
            String valuesarr = String.Empty;
            try
            {
                foreach (Object item in m_Row.ItemArray)
                {
                    if (item != null)
                        valuesarr += item.ToString();
                }
            }
            catch { valuesarr = String.Empty; }
            if (String.IsNullOrEmpty(valuesarr))
                return true;
            else
                return false;
        }       
        void UpdateDACPublishedIDforCapturedEDID()
        {
            objDataSet = null;
            DataTable dtStatus = null;
            List<EDIDInfo> edid = null;
            try
            {                
                if (!String.IsNullOrEmpty(this.LinkedIDExcelFile))
                {
                    FileInfo m_File = new FileInfo(this.LinkedIDExcelFile);
                    if (m_File.Extension.ToLower() == ".xls" || m_File.Extension.ToLower() == ".xlsx")
                    {
                        OleDbConnection m_ExcelConnection = null;
                        if (m_File.Extension.ToLower() == ".xls")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + this.LinkedIDExcelFile + ";Extended Properties=Excel 8.0;");
                        else if (m_File.Extension.ToLower() == ".xlsx")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.LinkedIDExcelFile + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");

                        m_ExcelConnection.Open();
                        DataTable dtExcelSheets = m_ExcelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                        m_ExcelConnection.Close();
                        if (dtExcelSheets != null)
                        {
                            foreach (DataRow drExcelSheet in dtExcelSheets.Rows)
                            {
                                OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [" + drExcelSheet["TABLE_NAME"].ToString() + "]", m_ExcelConnection);
                                DataTable dtTable = new DataTable();
                                m_ExcelAdapter.Fill(dtTable);
                                if (dtTable.Rows.Count > 0)
                                {
                                    edid = new List<EDIDInfo>();
                                    foreach (DataRow item in dtTable.Rows)
                                    {
                                        if (!CheckEmptyRow(item))
                                        {
                                            EDIDInfo newedid = new EDIDInfo();
                                            if (dtTable.Columns.Contains("Main Device"))
                                            {
                                                newedid.MainDevice = item["Main Device"].ToString();
                                            }
                                            if (dtTable.Columns.Contains("Sub Device"))
                                            {
                                                newedid.SubDevice = item["Sub Device"].ToString();
                                            }
                                            if (dtTable.Columns.Contains("Region"))
                                            {
                                                newedid.Region = item["Region"].ToString();
                                            }
                                            if (dtTable.Columns.Contains("Brand"))
                                            {
                                                newedid.Brand = item["Brand"].ToString();
                                            }
                                            if (dtTable.Columns.Contains("Model"))
                                            {
                                                newedid.Model = item["Model"].ToString();
                                            }
                                            if (dtTable.Columns.Contains("LinkedID"))
                                            {
                                                newedid.DACPublishedID = item["LinkedID"].ToString();
                                            }
                                            if (edid.Count > 0)
                                            {
                                                EDIDInfo isExist = edid.Find(ed => (ed.MainDevice == newedid.MainDevice) && (ed.SubDevice == newedid.SubDevice) && (ed.Brand == newedid.Brand) && (ed.Model == newedid.Model) && (ed.DACPublishedID != newedid.DACPublishedID));
                                                if (isExist != null)
                                                {
                                                    isExist.Region = isExist.Region + "," + newedid.Region;
                                                }
                                                else
                                                {
                                                    newedid.DataSource = "Capture EDID";
                                                    edid.Add(newedid);
                                                }
                                            }
                                            else
                                            {
                                                newedid.DataSource = "Capture EDID";
                                                edid.Add(newedid);
                                            }
                                            newedid = null;
                                        }
                                    }
                                    if (edid.Count > 0)
                                    {
                                        dtStatus = new DataTable();
                                        dtStatus.Columns.Add("Main Device");
                                        dtStatus.Columns.Add("Sub Device");
                                        dtStatus.Columns.Add("Brand");
                                        dtStatus.Columns.Add("Model");
                                        dtStatus.Columns.Add("Region");
                                        dtStatus.Columns.Add("LinkedID");
                                        dtStatus.Columns.Add("Status");
                                        dtStatus.AcceptChanges();
                                        DataRow dr = null;
                                        
                                        for (int i = 0; i < edid.Count; i++)
                                        {
                                            try
                                            {
                                                this.m_StatusMessage = "Updating Linked Id for Captured EDID " + edid.Count.ToString() + " of " + (i+1).ToString();
                                                this.m_StatusMessage1 = "Please wait...";
                                                m_Service.UpdateLinkedID(edid[i]);
                                                dr = dtStatus.NewRow();
                                                dr["Main Device"] = edid[i].MainDevice;
                                                dr["Sub Device"] = edid[i].SubDevice;
                                                dr["Brand"] = edid[i].Brand;
                                                dr["Model"] = edid[i].Model;
                                                dr["Region"] = edid[i].Region;
                                                dr["LinkedID"] = edid[i].DACPublishedID;
                                                dr["Status"] = m_Service.ErrorMessage;
                                                dtStatus.Rows.Add(dr);
                                            }
                                            catch
                                            {
                                                dr = dtStatus.NewRow();
                                                dr["Main Device"] = edid[i].MainDevice;
                                                dr["Sub Device"] = edid[i].SubDevice;
                                                dr["Brand"] = edid[i].Brand;
                                                dr["Model"] = edid[i].Model;
                                                dr["Region"] = edid[i].Region;
                                                dr["LinkedID"] = edid[i].DACPublishedID;
                                                dr["Status"] = m_Service.ErrorMessage;
                                                dtStatus.Rows.Add(dr);
                                            }
                                        }
                                        if (dtStatus.Rows.Count > 0)
                                        {
                                            if (objDataSet == null)
                                            {
                                                objDataSet = new DataSet("LinkedIDUpdateStaus");
                                            }
                                            if (objDataSet.Tables.Count > 0)
                                            {
                                                if (objDataSet.Tables.Contains(dtStatus.TableName))
                                                    objDataSet.Tables.Remove(dtStatus.TableName);
                                            }
                                            objDataSet.Tables.Add(dtStatus);
                                            if (this.objDataSet != null)
                                            {
                                                Export("UpdateStatus");
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.m_Service.ErrorMessage = ex.Message;                
            }
        }
        void Export(String reportname)
        {
            String m_SaveFileMessage = String.Empty;
            try
            {
                String m_filePath = CommonForms.Configuration.GetWorkingDirectory() + "\\";
                if (String.IsNullOrEmpty(m_filePath))
                {
                    FolderBrowserDialog dlg1 = new FolderBrowserDialog();
                    if (dlg1.ShowDialog() == DialogResult.OK)
                    {
                        if (dlg1.SelectedPath.EndsWith("\\") == false)
                        {
                            dlg1.SelectedPath += "\\";
                        }
                        m_filePath = dlg1.SelectedPath;
                    }
                }

                ExcelReportGenerator.GenerateReport(objDataSet, m_filePath, reportname, out m_SaveFileMessage);
                if (!String.IsNullOrEmpty(m_SaveFileMessage))
                {
                    MessageBox.Show("Report not saved." + m_SaveFileMessage, "Report Save");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Export To Excel", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void forTelemetryEDIDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.Title = "Select Telemetry EDID Excel File With LinkedID";
            openfile.Filter = "Excel File (*.xls,*.xlsx)|*.xls;*.xlsx";
            openfile.CheckFileExists = true;
            if (openfile.ShowDialog() == DialogResult.OK)
            {
                FileInfo m_File = new FileInfo(openfile.FileName);
                if (m_File.Extension.ToLower() == ".xls" || m_File.Extension.ToLower() == ".xlsx")
                {
                    this.LinkedIDExcelFile = openfile.FileName;
                    this.m_ProcessType = ProcessType.UpdateDACPublishedIDforTelemetryEDID;
                    ResetTimer();
                }
            }
            else
                MessageBox.Show("Please select Telemetry EDID with Linked ID Excel File", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        void UpdateDACPublishedIDforTelemetryEDID()
        {
            objDataSet = null;
            DataTable dtStatus = null;
            List<EDIDInfo> edid = null;
            try
            {                
                if (!String.IsNullOrEmpty(this.LinkedIDExcelFile))
                {
                    FileInfo m_File = new FileInfo(this.LinkedIDExcelFile);
                    if (m_File.Extension.ToLower() == ".xls" || m_File.Extension.ToLower() == ".xlsx")
                    {
                        OleDbConnection m_ExcelConnection = null;
                        if (m_File.Extension.ToLower() == ".xls")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + this.LinkedIDExcelFile + ";Extended Properties=Excel 8.0;");
                        else if (m_File.Extension.ToLower() == ".xlsx")
                            m_ExcelConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.LinkedIDExcelFile + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");

                        m_ExcelConnection.Open();
                        DataTable dtExcelSheets = m_ExcelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                        m_ExcelConnection.Close();
                        if (dtExcelSheets != null)
                        {
                            foreach (DataRow drExcelSheet in dtExcelSheets.Rows)
                            {
                                OleDbDataAdapter m_ExcelAdapter = new OleDbDataAdapter("SELECT * FROM [" + drExcelSheet["TABLE_NAME"].ToString() + "]", m_ExcelConnection);
                                DataTable dtTable = new DataTable();
                                m_ExcelAdapter.Fill(dtTable);
                                if (dtTable.Rows.Count > 0)
                                {
                                    edid = new List<EDIDInfo>();
                                    foreach (DataRow item in dtTable.Rows)
                                    {
                                        if (!CheckEmptyRow(item))
                                        {
                                            EDIDInfo newedid = new EDIDInfo();
                                            if (dtTable.Columns.Contains("Main Device"))
                                            {
                                                newedid.MainDevice = item["Main Device"].ToString();
                                            }
                                            if (dtTable.Columns.Contains("Sub Device"))
                                            {
                                                newedid.SubDevice = item["Sub Device"].ToString();
                                            }
                                            if (dtTable.Columns.Contains("Region"))
                                            {
                                                newedid.Region = item["Region"].ToString();
                                            }
                                            if (dtTable.Columns.Contains("Brand"))
                                            {
                                                newedid.Brand = item["Brand"].ToString();
                                            }
                                            if (dtTable.Columns.Contains("Model"))
                                            {
                                                newedid.Model = item["Model"].ToString();
                                            }
                                            if (dtTable.Columns.Contains("LinkedID"))
                                            {
                                                newedid.DACPublishedID = item["LinkedID"].ToString();
                                            }
                                            if (edid.Count > 0)
                                            {
                                                EDIDInfo isExist = edid.Find(ed => (ed.MainDevice == newedid.MainDevice) && (ed.SubDevice == newedid.SubDevice) && (ed.Brand == newedid.Brand) && (ed.Model == newedid.Model) && (ed.DACPublishedID != newedid.DACPublishedID));
                                                if (isExist != null)
                                                {
                                                    isExist.Region = isExist.Region + "," + newedid.Region;
                                                }
                                                else
                                                {
                                                    newedid.DataSource = "Telemetry EDID";
                                                    edid.Add(newedid);
                                                }
                                            }
                                            else
                                            {
                                                newedid.DataSource = "Telemetry EDID";
                                                edid.Add(newedid);
                                            }
                                            newedid = null;
                                        }
                                    }
                                    if (edid.Count > 0)
                                    {
                                        dtStatus = new DataTable();
                                        dtStatus.Columns.Add("Main Device");
                                        dtStatus.Columns.Add("Sub Device");
                                        dtStatus.Columns.Add("Brand");
                                        dtStatus.Columns.Add("Model");
                                        dtStatus.Columns.Add("Region");
                                        dtStatus.Columns.Add("LinkedID");
                                        dtStatus.Columns.Add("Status");
                                        dtStatus.AcceptChanges();
                                        DataRow dr = null;                                        
                                        for (int i = 0; i < edid.Count; i++)
                                        {
                                            try
                                            {
                                                this.m_StatusMessage = "Updating Linked Id for Telemetry EDID " + edid.Count.ToString() + " of " + (i + 1).ToString();
                                                this.m_StatusMessage1 = "Please wait...";

                                                m_Service.UpdateLinkedID(edid[i]);
                                                dr = dtStatus.NewRow();
                                                dr["Main Device"] = edid[i].MainDevice;
                                                dr["Sub Device"] = edid[i].SubDevice;
                                                dr["Brand"] = edid[i].Brand;
                                                dr["Model"] = edid[i].Model;
                                                dr["Region"] = edid[i].Region;
                                                dr["LinkedID"] = edid[i].DACPublishedID;
                                                dr["Status"] = m_Service.ErrorMessage;
                                                dtStatus.Rows.Add(dr);
                                            }
                                            catch
                                            {
                                                dr = dtStatus.NewRow();
                                                dr["Main Device"] = edid[i].MainDevice;
                                                dr["Sub Device"] = edid[i].SubDevice;
                                                dr["Brand"] = edid[i].Brand;
                                                dr["Model"] = edid[i].Model;
                                                dr["Region"] = edid[i].Region;
                                                dr["LinkedID"] = edid[i].DACPublishedID;
                                                dr["Status"] = m_Service.ErrorMessage;
                                                dtStatus.Rows.Add(dr);
                                            }
                                        }
                                        if (dtStatus.Rows.Count > 0)
                                        {
                                            if (objDataSet == null)
                                            {
                                                objDataSet = new DataSet("LinkedIDUpdateStaus");
                                            }
                                            if (objDataSet.Tables.Count > 0)
                                            {
                                                if (objDataSet.Tables.Contains(dtStatus.TableName))
                                                    objDataSet.Tables.Remove(dtStatus.TableName);
                                            }
                                            objDataSet.Tables.Add(dtStatus);
                                            if (this.objDataSet != null)
                                            {
                                                Export("UpdateStatus");
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.m_Service.ErrorMessage = ex.Message;                
            }
        }
        private enum ProcessType
        {
            UpdateDACPublishedIDforCapturedEDID,
            UpdateDACPublishedIDforTelemetryEDID
        }
        #endregion      
  
        #region Progress
        private void ResetTimer()
        {
            if (m_BackgroundWorker != null || m_BackgroundWorker.IsBusy == false)
                m_BackgroundWorker.RunWorkerAsync();

            if (m_Progress == null)
                m_Progress = new ProgressForm();
            m_Progress.SetMessage(StatusMessage, StatusMessage1);
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();
            m_Timer.Start();
        }
        void m_Timer_Tick(object sender, EventArgs e)
        {
            if (this.m_Progress != null)
            {
                this.m_Progress.SetMessage(StatusMessage, StatusMessage1);
            }
        }
        void m_BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.m_Timer.Stop();
            this.m_Progress.DialogResult = DialogResult.OK;
            this.m_Progress.Close();
            if (!String.IsNullOrEmpty(this.m_Service.ErrorMessage))
            {
                MessageBox.Show(this.m_Service.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                switch (this.m_ProcessType)
                {
                    case ProcessType.UpdateDACPublishedIDforCapturedEDID:
                        break;
                    case ProcessType.UpdateDACPublishedIDforTelemetryEDID:
                        break;
                }
            }
        }
        void m_BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (this.m_ProcessType)
            {
                case ProcessType.UpdateDACPublishedIDforCapturedEDID:
                    UpdateDACPublishedIDforCapturedEDID();
                    break;
                case ProcessType.UpdateDACPublishedIDforTelemetryEDID:
                    UpdateDACPublishedIDforTelemetryEDID();
                    break;               
            }
        }
        #endregion
    }
}