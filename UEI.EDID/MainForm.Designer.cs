﻿namespace UEI.EDID
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.setWorkingDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTools = new System.Windows.Forms.ToolStripMenuItem();
            this.viewEDIDDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.importEDIDDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.importXBoxEDIDDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xBoxReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eDIDGroupingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.updateDACPublishedID = new System.Windows.Forms.ToolStripMenuItem();
            this.forCaptureEDIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.forTelemetryEDIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.decodeEDIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblCopyRight = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.toolStripSettings,
            this.toolStripTools,
            this.helpMenu});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(806, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolsStripMenuItem_Click);
            // 
            // toolStripSettings
            // 
            this.toolStripSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setWorkingDirectoryToolStripMenuItem});
            this.toolStripSettings.Name = "toolStripSettings";
            this.toolStripSettings.Size = new System.Drawing.Size(61, 20);
            this.toolStripSettings.Text = "Settings";
            // 
            // setWorkingDirectoryToolStripMenuItem
            // 
            this.setWorkingDirectoryToolStripMenuItem.Name = "setWorkingDirectoryToolStripMenuItem";
            this.setWorkingDirectoryToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.setWorkingDirectoryToolStripMenuItem.Text = "Set Working Directory";
            this.setWorkingDirectoryToolStripMenuItem.Click += new System.EventHandler(this.setWorkingDirectoryToolStripMenuItem_Click);
            // 
            // toolStripTools
            // 
            this.toolStripTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewEDIDDataToolStripMenuItem,
            this.toolStripMenuItem1,
            this.importEDIDDataToolStripMenuItem,
            this.toolStripMenuItem2,
            this.importXBoxEDIDDataToolStripMenuItem,
            this.xBoxReportToolStripMenuItem,
            this.eDIDGroupingToolStripMenuItem,
            this.toolStripMenuItem3,
            this.updateDACPublishedID,
            this.toolStripMenuItem5,
            this.decodeEDIDToolStripMenuItem});
            this.toolStripTools.Name = "toolStripTools";
            this.toolStripTools.Size = new System.Drawing.Size(48, 20);
            this.toolStripTools.Text = "Tools";
            // 
            // viewEDIDDataToolStripMenuItem
            // 
            this.viewEDIDDataToolStripMenuItem.Name = "viewEDIDDataToolStripMenuItem";
            this.viewEDIDDataToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.viewEDIDDataToolStripMenuItem.Text = "View EDID";
            this.viewEDIDDataToolStripMenuItem.Click += new System.EventHandler(this.viewEDIDDataToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(205, 6);
            // 
            // importEDIDDataToolStripMenuItem
            // 
            this.importEDIDDataToolStripMenuItem.Name = "importEDIDDataToolStripMenuItem";
            this.importEDIDDataToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.importEDIDDataToolStripMenuItem.Text = "Import Capture EDID";
            this.importEDIDDataToolStripMenuItem.Click += new System.EventHandler(this.importEDIDDataToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(205, 6);
            // 
            // importXBoxEDIDDataToolStripMenuItem
            // 
            this.importXBoxEDIDDataToolStripMenuItem.Name = "importXBoxEDIDDataToolStripMenuItem";
            this.importXBoxEDIDDataToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.importXBoxEDIDDataToolStripMenuItem.Text = "Import XBox EDID";
            this.importXBoxEDIDDataToolStripMenuItem.Click += new System.EventHandler(this.importXBoxEDIDDataToolStripMenuItem_Click);
            // 
            // xBoxReportToolStripMenuItem
            // 
            this.xBoxReportToolStripMenuItem.Name = "xBoxReportToolStripMenuItem";
            this.xBoxReportToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.xBoxReportToolStripMenuItem.Text = "XBox Report";
            this.xBoxReportToolStripMenuItem.Click += new System.EventHandler(this.xBoxReportToolStripMenuItem_Click);
            // 
            // eDIDGroupingToolStripMenuItem
            // 
            this.eDIDGroupingToolStripMenuItem.Name = "eDIDGroupingToolStripMenuItem";
            this.eDIDGroupingToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.eDIDGroupingToolStripMenuItem.Text = "XBox Cluster Report";
            this.eDIDGroupingToolStripMenuItem.Click += new System.EventHandler(this.eDIDGroupingToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(205, 6);
            // 
            // updateDACPublishedID
            // 
            this.updateDACPublishedID.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.forCaptureEDIDToolStripMenuItem,
            this.forTelemetryEDIDToolStripMenuItem});
            this.updateDACPublishedID.Name = "updateDACPublishedID";
            this.updateDACPublishedID.Size = new System.Drawing.Size(208, 22);
            this.updateDACPublishedID.Text = "Update DAC Published ID";
            // 
            // forCaptureEDIDToolStripMenuItem
            // 
            this.forCaptureEDIDToolStripMenuItem.Name = "forCaptureEDIDToolStripMenuItem";
            this.forCaptureEDIDToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.forCaptureEDIDToolStripMenuItem.Text = "For Capture EDID";
            this.forCaptureEDIDToolStripMenuItem.Click += new System.EventHandler(this.forCaptureEDIDToolStripMenuItem_Click);
            // 
            // forTelemetryEDIDToolStripMenuItem
            // 
            this.forTelemetryEDIDToolStripMenuItem.Name = "forTelemetryEDIDToolStripMenuItem";
            this.forTelemetryEDIDToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.forTelemetryEDIDToolStripMenuItem.Text = "For Telemetry EDID";
            this.forTelemetryEDIDToolStripMenuItem.Click += new System.EventHandler(this.forTelemetryEDIDToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(205, 6);
            // 
            // decodeEDIDToolStripMenuItem
            // 
            this.decodeEDIDToolStripMenuItem.Name = "decodeEDIDToolStripMenuItem";
            this.decodeEDIDToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.decodeEDIDToolStripMenuItem.Text = "Decode EDID";
            this.decodeEDIDToolStripMenuItem.Click += new System.EventHandler(this.decodeEDIDToolStripMenuItem_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(44, 20);
            this.helpMenu.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.aboutToolStripMenuItem.Text = "&About ... ...";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.toolStripStatusLabel1,
            this.lblCopyRight});
            this.statusStrip.Location = new System.Drawing.Point(0, 431);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(806, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(752, 17);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // lblCopyRight
            // 
            this.lblCopyRight.Name = "lblCopyRight";
            this.lblCopyRight.Size = new System.Drawing.Size(0, 17);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(806, 453);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem toolStripSettings;
        private System.Windows.Forms.ToolStripMenuItem setWorkingDirectoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripTools;
        private System.Windows.Forms.ToolStripMenuItem importEDIDDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewEDIDDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem importXBoxEDIDDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xBoxReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblCopyRight;
        private System.Windows.Forms.ToolStripMenuItem eDIDGroupingToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem decodeEDIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem updateDACPublishedID;
        private System.Windows.Forms.ToolStripMenuItem forCaptureEDIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem forTelemetryEDIDToolStripMenuItem;
    }
}



