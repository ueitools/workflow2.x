using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Security.Principal;
using BusinessObject;
using CommonForms;

namespace USBCaptureImport {
    public partial class MainForm : Form {
        private const string VERSION = "Version 1.05";

        private int _newTnNumber;
        private Capt32_USB.ProjectInfo _importData;
        private string _importFilePath;
        private string _importFileName;
        private string _importTnName;
        private CountryCollection _cc;
        private DeviceTypeCollection _dt;
        private BrandCollection _bc;

        public MainForm() {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e) {
            lblVersion.Text = VERSION;
            lblCopyright.Text = string.Format("Copyright � {0} UEIC", DateTime.Now.Year);
            AssignedTnNumber.Text = "Not Assigned";
            _importFilePath = Configuration.GetWorkingDirectory();

            _dt = DBFunctions.GetAllDeviceTypeList();
            _bc = DBFunctions.GetAllBrandList();
            _cc = DBFunctions.GetAllCountryList();

            brandCollectionBindingSource.DataSource = _bc;
            deviceTypeListBindingSource.DataSource = _dt;

            foreach (Country item in _cc) {
                if (!regionComboBox.Items.Contains(item.Region)) {
                    regionComboBox.Items.Add(item.Region);
                }

                if (regionComboBox.Items[0].Equals(item.Region) &&
                    !countryComboBox.Items.Contains(item.Name)) {
                    countryComboBox.Items.Add(item.Name);
                }
            }

            branchComboBox.Text = "CA";
            isRetailComboBox.Text = "N";
            isTargetComboBox.Text = "Y";
            isCodebookComboBox.Text = "Y";
            isPartNumberComboBox.Text = "N";
        }

        private void BtnClose_Click(object sender, EventArgs e) {
            Close();
        }

        private void regionComboBox_SelectedIndexChanged(object sender, EventArgs e) {
            countryComboBox.Items.Clear();

            foreach (Country item in _cc) {
                if (regionComboBox.SelectedItem.Equals(item.Region) &&
                    !countryComboBox.Items.Contains(item.Name)) {
                    countryComboBox.Items.Add(item.Name);
                }
            }
            countryComboBox.SelectedIndex = 0;
        }

        private void BtnFileBrowse_Click(object sender, EventArgs e) {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Please locate the Import File.";
            dlg.Filter = "Zip files (*.zip)|*.zip|All files (*.*)|*.*";
            dlg.DefaultExt = ".zip";
            DialogResult dlgResult = dlg.ShowDialog();
            if (dlgResult == DialogResult.OK) {
                _importFileName = Path.GetFileNameWithoutExtension(dlg.FileName);
                _importFilePath = Path.GetDirectoryName(dlg.FileName);
                string[] captureSetFiles = Capt32_USB.ZipHelper.UnZipProject(dlg.FileName);
                TxtImportPath.Text = dlg.FileName;
                LoadCaptureSetInfo(captureSetFiles);
            }
        }

        private void BtnStart_Click(object sender, EventArgs e) {
            if (string.IsNullOrEmpty(TxtImportPath.Text)) {
                MessageBox.Show("You haven't selected a file to import yet.", "Error");
                return;
            }

            if (!VerifyTNValue()) {
                return;
            }

            AssignedTnNumber.Text = _newTnNumber.ToString();

            TN newTN = new TN();
            CopyAndRenameTNFiles(newTN);
            Capt32_USB.ZipHelper.ZipProject(
                _importFilePath + "\\" + _importTnName, _importTnName, false);

            SetupTNClass(newTN);
            SaveToDB(newTN);
        }

        private bool VerifyTNValue() {
            TNAssign TNA = new TNAssign();
            return TNA.VerifyAndLockTN(CB_AutoAssignTN.Checked, 
                                        AssignedTnNumber.Text, 
                                        out _newTnNumber, 
                                        ChkRestrictedTN.Checked);

            //if (CB_AutoAssignTN.Checked) {
            //    _newTnNumber = new TNAssign().GetNextTN(ChkRestrictedTN.Checked);
            //} else {
            //    if (!int.TryParse(AssignedTnNumber.Text, out _newTnNumber)) {
            //        MessageBox.Show("The TN value entered is not in the correct format", "Error");
            //        return false;
            //    }
            //}
            //return true;
        }

        //////////////////////////
        private void CopyAndRenameTNFiles(ITnHelper newTN) {
            _importTnName = string.Format("TN{0:D4}", _newTnNumber);
            string sourcePath = _importFilePath + "\\" + _importFileName;
            string destPath = _importFilePath + "\\" + _importTnName;
            string prjFile = string.Format("{0}\\L{1:D4}.prj", destPath, _newTnNumber);

            Directory.CreateDirectory(_importFilePath + "\\" + _importTnName);
            IList<string> report = _importData.CreatePrjHeader(_newTnNumber.ToString());

            foreach (string line in report) {
                File.AppendAllText(prjFile, line);
            }

            int fileNumber = 0;
            foreach (Capt32_USB.CaptureInfo item in _importData.FileList) {
                if (string.IsNullOrEmpty(item.FileName)) {
                    continue;
                }

                string captureExtention = Path.GetExtension(item.FileName);
                string targetFile = string.Format("!{0:D4}{1:X2}{2}",
                                                  _newTnNumber, fileNumber++, captureExtention);
                if (!File.Exists(sourcePath + "\\" + item.FileName)) {
                    continue;
                }
                File.Copy(sourcePath + "\\" + item.FileName,
                          destPath + "\\" + targetFile);
                File.AppendAllText(prjFile,
                                   string.Format("{0}   : {1}\r\n",
                                                 targetFile, item.KeyLabel));
                Function newFunc = new Function();
                newFunc.Label = item.KeyLabel;
                newFunc.Filename = targetFile;
                newFunc.TN = _newTnNumber;
                newTN.FunctionList.Add(newFunc);
            }
        }

        private void SetupTNClass(ITnHelper newTN) {
            //FunctionCollection FunctionList = new FunctionCollection();

            newTN.Header.TN = _newTnNumber;
            newTN.Header.LegacyBrand = brandComboBox.Text;
            newTN.Header.LegacyDeviceType = deviceTypeComboBox.Text;
            newTN.Header.LegacyRemoteModel = _importData.RemoteModel;
            newTN.Header.LegacyTargetModel = modelTextBox.Text;
            newTN.Header.Sender_Name = _importData.CustomerName;
            newTN.Header.Sender_Company = _importData.CompanyName;
            newTN.Header.Sender_Address = _importData.Address[0] + " " +
                                          _importData.Address[1] + " " +
                                          _importData.Address[2] + " " +
                                          _importData.Address[3];
            newTN.Header.Text = _importData.Comments;

            newTN.Header.Status = StatusFlag.MODIFIED;

            newTN.Header.LogList.Add(Log.Create(newTN.Header.TN, "Imported"));
            Device dr = new Device();
            SetupDrClass(dr);
            newTN.DeviceList.Add(dr);
        }

        private void SetupDrClass(Device dr) {
            dr.ID = "--"; // we don't have an ID but it can't be null either.
            dr.Brand = brandComboBox.Text;
            dr.Model = modelTextBox.Text;
            dr.Collector = collectorTextBox.Text;

            dr.DeviceTypeList.Clear();
            if (!string.IsNullOrEmpty(subDeviceTypeComboBox.Text)) {
                foreach (DeviceType item in _dt) {
                    if (item.Name.Equals(subDeviceTypeComboBox.Text)) {
                        dr.DeviceTypeList.Add(item);
                    }
                }
            }
            dr.CountryList.Clear();
            if (!string.IsNullOrEmpty(countryComboBox.Text)) {
                foreach (Country item in _cc) {
                    if (item.Name.Equals(countryComboBox.Text)) {
                        dr.CountryList.Add(item);
                    }
                }
            }
            dr.Text = textTextBox.Text;

            dr.Branch = branchComboBox.Text;
            dr.IsRetail = isRetailComboBox.Text;
            dr.IsTarget = isTargetComboBox.Text;
            dr.IsCodebook = isCodebookComboBox.Text;
            dr.IsPartNumber = isPartNumberComboBox.Text;
        }

        private void SaveToDB(TN newTN) {
            DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);

            if (DAOFactory.GetDBConnectionString() !=
                DBConnectionString.UEITEMP) {
                throw new ApplicationException(
                    "Committing to Public DB: " +
                    DAOFactory.GetDBConnectionString());
            }

            CopyExtras(newTN.Header.TN);

            try {
                DAOFactory.BeginTransaction();
                TNAssign store = new TNAssign();
                store.WriteTnToNetwork(_importTnName, _importFilePath, newTN.Header.TN);
                newTN.Save();
                DAOFactory.CommitTransaction();
                MessageBox.Show("TN has been saved " +
                                "to the database", "Success");
            } catch (Exception ex) {
                DAOFactory.RollBackTransaction();
                MessageBox.Show(ex.Message, "Save Failed");
            }
        }

        //////////////////////////

        private void CopyExtras(int tn) {
            string sourcePath = _importFilePath + "\\" + _importFileName + "\\";

            // copy image
            bool permission = true;
            if (!string.IsNullOrEmpty(_importData.ImageFile)) {
                string destPath = Properties.Settings.Default.ImagePath;
                string ext = Path.GetExtension(_importData.ImageFile);
                destPath += PicPathSubDir(tn);
                if (Directory.Exists(destPath) == false) 
                    Directory.CreateDirectory(destPath);
                
                string destImage = string.Format("TN{0:D5}{1}", tn, ext);
                if (File.Exists(destPath + destImage))
                    permission = AskPermission(destPath, destImage);
                
                if (permission)
                    CopyFileWarnIfFail(sourcePath + _importData.ImageFile, destPath, destImage);
            }

            // copy PDF
            permission = true;
            if (!string.IsNullOrEmpty(_importData.DeviceManual)) {
                string manualPath = Properties.Settings.Default.ManualPath;
                manualPath += PicPathSubDir(tn);
                if (Directory.Exists(manualPath) == false)
                    Directory.CreateDirectory(manualPath);
                
                string destFile = string.Format("TN{0:D5}.pdf", tn);
                if (File.Exists(manualPath + destFile))
                    permission = AskPermission(manualPath, destFile);

                if (permission)
                    CopyFileWarnIfFail(sourcePath + _importData.DeviceManual, manualPath, destFile);
            }
        }

        private void CopyFileWarnIfFail(string sourcePath, string destPath, string destFile)
        {
            try{
                File.Copy(sourcePath, destPath + destFile, true);
            }catch (Exception e){
                MessageBox.Show(destFile + " was not copied\r\n" + e.Message, "Error");
            }
        }

        private bool AskPermission(string path, string file) {
            string msg = "The file:\n" + path + file + "\nexists, do you want to replace it?";
            bool permission =
                (MessageBox.Show(msg, "Info", MessageBoxButtons.YesNo) == DialogResult.Yes);
            if (permission) {
                try {
                    File.Delete(path + file);
                } catch (Exception ex) {
                    MessageBox.Show(ex.Message, "File will not be copied");
                    permission = false;
                }
            }

            return permission;
        }

        /// <summary>
        /// Generate a subdirectory based on the TN number
        /// </summary>
        /// <param name="tn"></param>
        /// <returns></returns>
        public static string PicPathSubDir(int tn) {
            int tnGrp = tn - (tn % 1000);

            string subDir;
            if (tn >= 90000) {
                subDir = "TN90000-\\"; // Highest numbered group (so far)
            } else // else build the directory group name
            {
                subDir = string.Format("TN{0:D5}-TN{1:D5}\\", tnGrp, (tnGrp + 999));
            }

            return subDir;
        }

        public static Log CreateLog(int tn, string contents) {
            Log log = new Log();
            log.TN = tn;
            log.Time = DateTime.Now;
            log.Content = contents;

            string name = WindowsIdentity.GetCurrent().Name;
            if (name.Contains("\\")) {
                log.Operator = name.Substring(name.LastIndexOf("\\") + 1);
            }

            return log;
        }

        private void LoadCaptureSetInfo(IEnumerable<string> captureSetFiles) {
            string xmlFile = string.Empty;
            string prjFile = string.Empty;
            foreach (string captureSetFile in captureSetFiles) {
                if (captureSetFile.ToLower().EndsWith(".xml")) {
                    xmlFile = captureSetFile;
                } else if (captureSetFile.ToLower().EndsWith(".prj")) {
                    prjFile = captureSetFile;
                }
            }

            if (File.Exists(xmlFile)) {
                try {
                    _importData = new Capt32_USB.ProjectInfo();
                    _importData.FromXMLFile(xmlFile, ref _importData);
                } catch (Exception) {
                    _importData = new Capt32_USB.ReportPrj();
                    _importData.FromXMLFile(xmlFile, ref _importData);
                }
                if (CheckForImportFileNameError())
                    _importData.ToXMLFile(xmlFile, _importData);

            } else {
                _importData = new Capt32_USB.ProjectInfo();
                _importData.FromPRJFile(prjFile);
            }

            ImportDataToDisplay();
        }

        private bool CheckForImportFileNameError()
        {
            bool Error = false;
            foreach (Capt32_USB.CaptureInfo capture in _importData.FileList)
            {
                if (capture.FileName.Contains(".."))
                {
                    capture.FileName = capture.FileName.Replace("..", ".");
                    Error = true;
                }
            }
            if (Error)
                MessageBox.Show("File names with double dots (\"..\") were found and corrected.\n", "Warning");
            return Error;
        }

        private void ImportDataToDisplay() {
            collectorTextBox.Text = _importData.UserName;
            modelTextBox.Text = _importData.TargetModel;
            deviceTypeComboBox.Text = _importData.DeviceType;
            subDeviceTypeComboBox.Text = _importData.SubDeviceType;
            brandComboBox.Text = _importData.BrandName;

            foreach (Country item in _cc) {
                if (item.Name.Equals(_importData.Address[3])) {
                    regionComboBox.SelectedItem = item.Region;
                    Update();
                    countryComboBox.SelectedItem = item.Name;
                    break;
                }
            }

            TxtImportContent.Clear();
            IList<string> view = _importData.CreatePrjHeader(_newTnNumber.ToString());
            foreach (string line in view) {
                TxtImportContent.Text += line;
            }
        }

        private void CB_AutoAssignTN_CheckedChanged(object sender, EventArgs e) {
            AssignedTnNumber.ReadOnly = CB_AutoAssignTN.Checked;
            ChkRestrictedTN.Enabled = CB_AutoAssignTN.Checked;
        }

        private void brandComboBox_SelectedIndexChanged(object sender, EventArgs e) {
            if (_importData != null) {
                _importData.BrandName = brandComboBox.Text;
            }
        }
    }
}