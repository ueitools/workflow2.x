namespace USBCaptureImport
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label branchLabel;
            System.Windows.Forms.Label brandLabel;
            System.Windows.Forms.Label collectorLabel;
            System.Windows.Forms.Label isCodebookLabel;
            System.Windows.Forms.Label isPartNumberLabel;
            System.Windows.Forms.Label isRetailLabel;
            System.Windows.Forms.Label isTargetLabel;
            System.Windows.Forms.Label modelLabel;
            System.Windows.Forms.Label textLabel;
            System.Windows.Forms.Label nameLabel;
            System.Windows.Forms.Label nameLabel1;
            System.Windows.Forms.Label regionLabel;
            System.Windows.Forms.Label label3;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.BtnFileBrowse = new System.Windows.Forms.Button();
            this.TxtImportPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnStart = new System.Windows.Forms.Button();
            this.BtnClose = new System.Windows.Forms.Button();
            this.TxtImportContent = new System.Windows.Forms.TextBox();
            this.ChkRestrictedTN = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.branchComboBox = new System.Windows.Forms.ComboBox();
            this.collectorTextBox = new System.Windows.Forms.TextBox();
            this.isCodebookComboBox = new System.Windows.Forms.ComboBox();
            this.isPartNumberComboBox = new System.Windows.Forms.ComboBox();
            this.isRetailComboBox = new System.Windows.Forms.ComboBox();
            this.isTargetComboBox = new System.Windows.Forms.ComboBox();
            this.modelTextBox = new System.Windows.Forms.TextBox();
            this.textTextBox = new System.Windows.Forms.TextBox();
            this.deviceTypeComboBox = new System.Windows.Forms.ComboBox();
            this.deviceTypeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.countryComboBox = new System.Windows.Forms.ComboBox();
            this.regionComboBox = new System.Windows.Forms.ComboBox();
            this.brandComboBox = new System.Windows.Forms.ComboBox();
            this.brandCollectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.subDeviceTypeComboBox = new System.Windows.Forms.ComboBox();
            this.CB_AutoAssignTN = new System.Windows.Forms.CheckBox();
            this.AssignedTnNumber = new System.Windows.Forms.TextBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblCopyright = new System.Windows.Forms.Label();
            branchLabel = new System.Windows.Forms.Label();
            brandLabel = new System.Windows.Forms.Label();
            collectorLabel = new System.Windows.Forms.Label();
            isCodebookLabel = new System.Windows.Forms.Label();
            isPartNumberLabel = new System.Windows.Forms.Label();
            isRetailLabel = new System.Windows.Forms.Label();
            isTargetLabel = new System.Windows.Forms.Label();
            modelLabel = new System.Windows.Forms.Label();
            textLabel = new System.Windows.Forms.Label();
            nameLabel = new System.Windows.Forms.Label();
            nameLabel1 = new System.Windows.Forms.Label();
            regionLabel = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.deviceTypeListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brandCollectionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // branchLabel
            // 
            branchLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            branchLabel.AutoSize = true;
            branchLabel.Location = new System.Drawing.Point(637, 344);
            branchLabel.Name = "branchLabel";
            branchLabel.Size = new System.Drawing.Size(44, 13);
            branchLabel.TabIndex = 12;
            branchLabel.Text = "Branch:";
            // 
            // brandLabel
            // 
            brandLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            brandLabel.AutoSize = true;
            brandLabel.Location = new System.Drawing.Point(11, 344);
            brandLabel.Name = "brandLabel";
            brandLabel.Size = new System.Drawing.Size(38, 13);
            brandLabel.TabIndex = 14;
            brandLabel.Text = "Brand:";
            // 
            // collectorLabel
            // 
            collectorLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            collectorLabel.AutoSize = true;
            collectorLabel.Location = new System.Drawing.Point(11, 414);
            collectorLabel.Name = "collectorLabel";
            collectorLabel.Size = new System.Drawing.Size(51, 13);
            collectorLabel.TabIndex = 16;
            collectorLabel.Text = "Collector:";
            // 
            // isCodebookLabel
            // 
            isCodebookLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            isCodebookLabel.AutoSize = true;
            isCodebookLabel.Location = new System.Drawing.Point(637, 448);
            isCodebookLabel.Name = "isCodebookLabel";
            isCodebookLabel.Size = new System.Drawing.Size(70, 13);
            isCodebookLabel.TabIndex = 22;
            isCodebookLabel.Text = "Is Codebook:";
            // 
            // isPartNumberLabel
            // 
            isPartNumberLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            isPartNumberLabel.AutoSize = true;
            isPartNumberLabel.Location = new System.Drawing.Point(637, 484);
            isPartNumberLabel.Name = "isPartNumberLabel";
            isPartNumberLabel.Size = new System.Drawing.Size(80, 13);
            isPartNumberLabel.TabIndex = 24;
            isPartNumberLabel.Text = "Is Part Number:";
            // 
            // isRetailLabel
            // 
            isRetailLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            isRetailLabel.AutoSize = true;
            isRetailLabel.Location = new System.Drawing.Point(637, 379);
            isRetailLabel.Name = "isRetailLabel";
            isRetailLabel.Size = new System.Drawing.Size(48, 13);
            isRetailLabel.TabIndex = 26;
            isRetailLabel.Text = "Is Retail:";
            // 
            // isTargetLabel
            // 
            isTargetLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            isTargetLabel.AutoSize = true;
            isTargetLabel.Location = new System.Drawing.Point(637, 414);
            isTargetLabel.Name = "isTargetLabel";
            isTargetLabel.Size = new System.Drawing.Size(52, 13);
            isTargetLabel.TabIndex = 28;
            isTargetLabel.Text = "Is Target:";
            // 
            // modelLabel
            // 
            modelLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            modelLabel.AutoSize = true;
            modelLabel.Location = new System.Drawing.Point(11, 379);
            modelLabel.Name = "modelLabel";
            modelLabel.Size = new System.Drawing.Size(39, 13);
            modelLabel.TabIndex = 30;
            modelLabel.Text = "Model:";
            // 
            // textLabel
            // 
            textLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            textLabel.AutoSize = true;
            textLabel.Location = new System.Drawing.Point(11, 552);
            textLabel.Name = "textLabel";
            textLabel.Size = new System.Drawing.Size(33, 13);
            textLabel.TabIndex = 32;
            textLabel.Text = "Note:";
            // 
            // nameLabel
            // 
            nameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            nameLabel.AutoSize = true;
            nameLabel.Location = new System.Drawing.Point(11, 448);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new System.Drawing.Size(71, 13);
            nameLabel.TabIndex = 33;
            nameLabel.Text = "Device Type:";
            // 
            // nameLabel1
            // 
            nameLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            nameLabel1.AutoSize = true;
            nameLabel1.Location = new System.Drawing.Point(233, 517);
            nameLabel1.Name = "nameLabel1";
            nameLabel1.Size = new System.Drawing.Size(46, 13);
            nameLabel1.TabIndex = 34;
            nameLabel1.Text = "Country:";
            // 
            // regionLabel
            // 
            regionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            regionLabel.AutoSize = true;
            regionLabel.Location = new System.Drawing.Point(11, 517);
            regionLabel.Name = "regionLabel";
            regionLabel.Size = new System.Drawing.Size(44, 13);
            regionLabel.TabIndex = 36;
            regionLabel.Text = "Region:";
            // 
            // label3
            // 
            label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(11, 484);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(63, 13);
            label3.TabIndex = 38;
            label3.Text = "Sub Device";
            // 
            // BtnFileBrowse
            // 
            this.BtnFileBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnFileBrowse.Location = new System.Drawing.Point(710, 26);
            this.BtnFileBrowse.Name = "BtnFileBrowse";
            this.BtnFileBrowse.Size = new System.Drawing.Size(75, 23);
            this.BtnFileBrowse.TabIndex = 1;
            this.BtnFileBrowse.Text = "Browse";
            this.BtnFileBrowse.UseVisualStyleBackColor = true;
            this.BtnFileBrowse.Click += new System.EventHandler(this.BtnFileBrowse_Click);
            // 
            // TxtImportPath
            // 
            this.TxtImportPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtImportPath.Location = new System.Drawing.Point(73, 27);
            this.TxtImportPath.Name = "TxtImportPath";
            this.TxtImportPath.Size = new System.Drawing.Size(619, 20);
            this.TxtImportPath.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Import File";
            // 
            // BtnStart
            // 
            this.BtnStart.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.BtnStart.Location = new System.Drawing.Point(264, 630);
            this.BtnStart.Name = "BtnStart";
            this.BtnStart.Size = new System.Drawing.Size(75, 23);
            this.BtnStart.TabIndex = 15;
            this.BtnStart.Text = "Start";
            this.BtnStart.UseVisualStyleBackColor = true;
            this.BtnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // BtnClose
            // 
            this.BtnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.BtnClose.Location = new System.Drawing.Point(465, 630);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Size = new System.Drawing.Size(75, 23);
            this.BtnClose.TabIndex = 16;
            this.BtnClose.Text = "Close";
            this.BtnClose.UseVisualStyleBackColor = true;
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // TxtImportContent
            // 
            this.TxtImportContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtImportContent.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtImportContent.Location = new System.Drawing.Point(14, 74);
            this.TxtImportContent.Multiline = true;
            this.TxtImportContent.Name = "TxtImportContent";
            this.TxtImportContent.ReadOnly = true;
            this.TxtImportContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TxtImportContent.Size = new System.Drawing.Size(771, 243);
            this.TxtImportContent.TabIndex = 17;
            this.TxtImportContent.TabStop = false;
            // 
            // ChkRestrictedTN
            // 
            this.ChkRestrictedTN.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ChkRestrictedTN.AutoSize = true;
            this.ChkRestrictedTN.Location = new System.Drawing.Point(127, 594);
            this.ChkRestrictedTN.Name = "ChkRestrictedTN";
            this.ChkRestrictedTN.Size = new System.Drawing.Size(92, 17);
            this.ChkRestrictedTN.TabIndex = 14;
            this.ChkRestrictedTN.Text = "Restricted TN";
            this.ChkRestrictedTN.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(326, 596);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Assigned TN:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Content";
            // 
            // branchComboBox
            // 
            this.branchComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.branchComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.branchComboBox.FormattingEnabled = true;
            this.branchComboBox.Items.AddRange(new object[] {
            "",
            "EU",
            "BL",
            "CA",
            "CY",
            "TM"});
            this.branchComboBox.Location = new System.Drawing.Point(723, 341);
            this.branchComboBox.Name = "branchComboBox";
            this.branchComboBox.Size = new System.Drawing.Size(62, 21);
            this.branchComboBox.TabIndex = 8;
            // 
            // collectorTextBox
            // 
            this.collectorTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.collectorTextBox.Location = new System.Drawing.Point(88, 411);
            this.collectorTextBox.Name = "collectorTextBox";
            this.collectorTextBox.Size = new System.Drawing.Size(494, 20);
            this.collectorTextBox.TabIndex = 4;
            // 
            // isCodebookComboBox
            // 
            this.isCodebookComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.isCodebookComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.isCodebookComboBox.FormattingEnabled = true;
            this.isCodebookComboBox.Items.AddRange(new object[] {
            "",
            "Y",
            "N"});
            this.isCodebookComboBox.Location = new System.Drawing.Point(723, 445);
            this.isCodebookComboBox.Name = "isCodebookComboBox";
            this.isCodebookComboBox.Size = new System.Drawing.Size(62, 21);
            this.isCodebookComboBox.TabIndex = 11;
            // 
            // isPartNumberComboBox
            // 
            this.isPartNumberComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.isPartNumberComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.isPartNumberComboBox.FormattingEnabled = true;
            this.isPartNumberComboBox.Items.AddRange(new object[] {
            "",
            "Y",
            "N"});
            this.isPartNumberComboBox.Location = new System.Drawing.Point(723, 481);
            this.isPartNumberComboBox.Name = "isPartNumberComboBox";
            this.isPartNumberComboBox.Size = new System.Drawing.Size(62, 21);
            this.isPartNumberComboBox.TabIndex = 12;
            // 
            // isRetailComboBox
            // 
            this.isRetailComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.isRetailComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.isRetailComboBox.FormattingEnabled = true;
            this.isRetailComboBox.Items.AddRange(new object[] {
            "",
            "Y",
            "N"});
            this.isRetailComboBox.Location = new System.Drawing.Point(723, 376);
            this.isRetailComboBox.Name = "isRetailComboBox";
            this.isRetailComboBox.Size = new System.Drawing.Size(62, 21);
            this.isRetailComboBox.TabIndex = 9;
            // 
            // isTargetComboBox
            // 
            this.isTargetComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.isTargetComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.isTargetComboBox.FormattingEnabled = true;
            this.isTargetComboBox.Items.AddRange(new object[] {
            "",
            "Y",
            "N"});
            this.isTargetComboBox.Location = new System.Drawing.Point(723, 411);
            this.isTargetComboBox.Name = "isTargetComboBox";
            this.isTargetComboBox.Size = new System.Drawing.Size(62, 21);
            this.isTargetComboBox.TabIndex = 10;
            // 
            // modelTextBox
            // 
            this.modelTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.modelTextBox.Location = new System.Drawing.Point(88, 376);
            this.modelTextBox.Name = "modelTextBox";
            this.modelTextBox.Size = new System.Drawing.Size(494, 20);
            this.modelTextBox.TabIndex = 3;
            // 
            // textTextBox
            // 
            this.textTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textTextBox.Location = new System.Drawing.Point(88, 549);
            this.textTextBox.Name = "textTextBox";
            this.textTextBox.Size = new System.Drawing.Size(697, 20);
            this.textTextBox.TabIndex = 13;
            // 
            // deviceTypeComboBox
            // 
            this.deviceTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceTypeComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.deviceTypeComboBox.FormattingEnabled = true;
            this.deviceTypeComboBox.Items.AddRange(new object[] {
            "Audio - Amplifier",
            "Set Top Box Keyboard",
            "Cable Set Top Box",
            "Audio - CD",
            "Home Automation",
            "Audio - Digital Tape",
            "Audio - Cassette Deck",
            "Video - Laser Disk Player",
            "Audio - Accessory",
            "Video - Accessory",
            "Audio - Phonograph",
            "Lighting",
            "Audio - Receiver",
            "Satellite Set Top Box",
            "Television",
            "Video - VCR",
            "Video - DVD"});
            this.deviceTypeComboBox.Location = new System.Drawing.Point(88, 445);
            this.deviceTypeComboBox.Name = "deviceTypeComboBox";
            this.deviceTypeComboBox.Size = new System.Drawing.Size(494, 21);
            this.deviceTypeComboBox.TabIndex = 5;
            // 
            // countryComboBox
            // 
            this.countryComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.countryComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.countryComboBox.FormattingEnabled = true;
            this.countryComboBox.Location = new System.Drawing.Point(285, 514);
            this.countryComboBox.Name = "countryComboBox";
            this.countryComboBox.Size = new System.Drawing.Size(297, 21);
            this.countryComboBox.TabIndex = 7;
            // 
            // regionComboBox
            // 
            this.regionComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.regionComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.regionComboBox.FormattingEnabled = true;
            this.regionComboBox.Location = new System.Drawing.Point(88, 514);
            this.regionComboBox.Name = "regionComboBox";
            this.regionComboBox.Size = new System.Drawing.Size(109, 21);
            this.regionComboBox.TabIndex = 6;
            this.regionComboBox.SelectedIndexChanged += new System.EventHandler(this.regionComboBox_SelectedIndexChanged);
            // 
            // brandComboBox
            // 
            this.brandComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.brandComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.brandComboBox.DataSource = this.brandCollectionBindingSource;
            this.brandComboBox.DisplayMember = "Name";
            this.brandComboBox.FormattingEnabled = true;
            this.brandComboBox.Location = new System.Drawing.Point(88, 341);
            this.brandComboBox.Name = "brandComboBox";
            this.brandComboBox.Size = new System.Drawing.Size(494, 21);
            this.brandComboBox.TabIndex = 2;
            this.brandComboBox.SelectedIndexChanged += new System.EventHandler(this.brandComboBox_SelectedIndexChanged);
            // 
            // brandCollectionBindingSource
            // 
            this.brandCollectionBindingSource.DataSource = typeof(BusinessObject.BrandCollection);
            // 
            // subDeviceTypeComboBox
            // 
            this.subDeviceTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.subDeviceTypeComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.subDeviceTypeComboBox.DataSource = this.deviceTypeListBindingSource;
            this.subDeviceTypeComboBox.DisplayMember = "Name";
            this.subDeviceTypeComboBox.FormattingEnabled = true;
            this.subDeviceTypeComboBox.Location = new System.Drawing.Point(88, 481);
            this.subDeviceTypeComboBox.Name = "subDeviceTypeComboBox";
            this.subDeviceTypeComboBox.Size = new System.Drawing.Size(494, 21);
            this.subDeviceTypeComboBox.TabIndex = 37;
            // 
            // CB_AutoAssignTN
            // 
            this.CB_AutoAssignTN.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.CB_AutoAssignTN.AutoSize = true;
            this.CB_AutoAssignTN.Checked = true;
            this.CB_AutoAssignTN.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CB_AutoAssignTN.Location = new System.Drawing.Point(578, 594);
            this.CB_AutoAssignTN.Name = "CB_AutoAssignTN";
            this.CB_AutoAssignTN.Size = new System.Drawing.Size(100, 17);
            this.CB_AutoAssignTN.TabIndex = 39;
            this.CB_AutoAssignTN.Text = "Assign New TN";
            this.CB_AutoAssignTN.UseVisualStyleBackColor = true;
            this.CB_AutoAssignTN.CheckedChanged += new System.EventHandler(this.CB_AutoAssignTN_CheckedChanged);
            // 
            // AssignedTnNumber
            // 
            this.AssignedTnNumber.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.AssignedTnNumber.Location = new System.Drawing.Point(403, 592);
            this.AssignedTnNumber.MaxLength = 5;
            this.AssignedTnNumber.Name = "AssignedTnNumber";
            this.AssignedTnNumber.ReadOnly = true;
            this.AssignedTnNumber.Size = new System.Drawing.Size(76, 20);
            this.AssignedTnNumber.TabIndex = 40;
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(12, 641);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(66, 13);
            this.lblVersion.TabIndex = 41;
            this.lblVersion.Text = "Version 1.01";
            // 
            // lblCopyright
            // 
            this.lblCopyright.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCopyright.AutoSize = true;
            this.lblCopyright.Location = new System.Drawing.Point(666, 641);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(51, 13);
            this.lblCopyright.TabIndex = 42;
            this.lblCopyright.Text = "Copyright";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 663);
            this.Controls.Add(this.lblCopyright);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.AssignedTnNumber);
            this.Controls.Add(this.CB_AutoAssignTN);
            this.Controls.Add(label3);
            this.Controls.Add(this.subDeviceTypeComboBox);
            this.Controls.Add(regionLabel);
            this.Controls.Add(this.regionComboBox);
            this.Controls.Add(nameLabel1);
            this.Controls.Add(this.countryComboBox);
            this.Controls.Add(nameLabel);
            this.Controls.Add(this.deviceTypeComboBox);
            this.Controls.Add(branchLabel);
            this.Controls.Add(this.branchComboBox);
            this.Controls.Add(brandLabel);
            this.Controls.Add(this.brandComboBox);
            this.Controls.Add(collectorLabel);
            this.Controls.Add(this.collectorTextBox);
            this.Controls.Add(isCodebookLabel);
            this.Controls.Add(this.isCodebookComboBox);
            this.Controls.Add(isPartNumberLabel);
            this.Controls.Add(this.isPartNumberComboBox);
            this.Controls.Add(isRetailLabel);
            this.Controls.Add(this.isRetailComboBox);
            this.Controls.Add(isTargetLabel);
            this.Controls.Add(this.isTargetComboBox);
            this.Controls.Add(modelLabel);
            this.Controls.Add(this.modelTextBox);
            this.Controls.Add(textLabel);
            this.Controls.Add(this.textTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ChkRestrictedTN);
            this.Controls.Add(this.TxtImportContent);
            this.Controls.Add(this.BtnClose);
            this.Controls.Add(this.BtnStart);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtImportPath);
            this.Controls.Add(this.BtnFileBrowse);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(600, 500);
            this.Name = "MainForm";
            this.Text = "Import External Capture File";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.deviceTypeListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brandCollectionBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnFileBrowse;
        private System.Windows.Forms.TextBox TxtImportPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnStart;
        private System.Windows.Forms.Button BtnClose;
        private System.Windows.Forms.TextBox TxtImportContent;
        private System.Windows.Forms.CheckBox ChkRestrictedTN;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox branchComboBox;
        private System.Windows.Forms.TextBox collectorTextBox;
        private System.Windows.Forms.ComboBox isCodebookComboBox;
        private System.Windows.Forms.ComboBox isPartNumberComboBox;
        private System.Windows.Forms.ComboBox isRetailComboBox;
        private System.Windows.Forms.ComboBox isTargetComboBox;
        private System.Windows.Forms.TextBox modelTextBox;
        private System.Windows.Forms.TextBox textTextBox;
        private System.Windows.Forms.BindingSource deviceTypeListBindingSource;
        private System.Windows.Forms.ComboBox deviceTypeComboBox;
        private System.Windows.Forms.ComboBox countryComboBox;
        private System.Windows.Forms.ComboBox regionComboBox;
        private System.Windows.Forms.ComboBox brandComboBox;
        private System.Windows.Forms.BindingSource brandCollectionBindingSource;
        private System.Windows.Forms.ComboBox subDeviceTypeComboBox;
        private System.Windows.Forms.CheckBox CB_AutoAssignTN;
        private System.Windows.Forms.TextBox AssignedTnNumber;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblCopyright;
    }
}

