using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Capt32_USB
{
    public class CaptureInfo
    {
        public string KeyLabel;
        public string FileName;
    }

    public class ProjectInfo
    {
        private const string _NOFILE = "!---------";
        private string _comments;

        public string TimeStamp;
        public string UserName;
        public string BrandName;
        public string DeviceType;
        public string SubDeviceType;
        public string RemoteModel;
        public string TargetModel;
        public string Mode;
        public string CustomerName;
        public string CompanyName;
        public string[] Address = new string[4];
        public string Markets;
        public string Comments
        {
            get { return _comments; }
            set
            {
                if (value.Length > 255)
                    _comments = value.Substring(0, 255);
                else _comments = value;
            }
        }
        public string ImageFile;
        public string DeviceManual;
        public string TrackingNumber;
        public CaptureInfo[] FileList;

        public void UpdatePrj()
        {
            DateTime TimeFormat = new DateTime(DateTime.Now.Ticks);
            TimeStamp =
                string.Format("{0} {1}", TimeFormat.ToString("dd MMM yyyy"),
                                         TimeFormat.ToString("HH:mm"));
        }

        public void UpdateCaptureList(IList<CaptureInfo> ProjList)
        {
            FileList = new CaptureInfo[ProjList.Count];

            for (int index = 0; index < ProjList.Count; index++ )
            {
                CaptureInfo entry = new CaptureInfo();
                if (!string.IsNullOrEmpty(ProjList[index].FileName))
                    entry.FileName = string.Format("!0000{0:X2}.U1", index);
                    
                entry.KeyLabel = ProjList[index].KeyLabel;
                FileList[index] = entry;
            }
        }

        public void ToXMLFile(string FileName, ProjectInfo XmlPrj)
        {
            XmlSerializer Xml = new XmlSerializer(XmlPrj.GetType());
            StreamWriter SWriter = new StreamWriter(FileName);
            Xml.Serialize(SWriter, XmlPrj);
            SWriter.Close();
        }

        public void FromXMLFile(string FileName, ref ProjectInfo XmlPrj)
        {
            XmlSerializer Xml = new XmlSerializer(XmlPrj.GetType());
            StreamReader SReader = new StreamReader(FileName);
            XmlPrj = (ProjectInfo)Xml.Deserialize(SReader);
            SReader.Close();
        }

        private enum PrjSections {
            None,
            Main,
            Customer,
            Capture,
        }

        public void FromPRJFile(string FileName) {
            StreamReader stream = new StreamReader(FileName);
            PrjSections section = PrjSections.None;

            for (int index = 0; index < Address.Length; index++) {
                Address[index] = string.Empty;
            }

            string buf;
            while ((buf = stream.ReadLine()) != null) {
                buf = buf.Trim();
                if (buf.Contains("REMOTE CONTROL CAPTURE")) {
                    section = PrjSections.Main;
                } else if (buf.Contains("CUSTOMER")) {
                    section = PrjSections.Customer;
                } else if (buf.Contains("CAPTURE LIST")) {
                    section = PrjSections.Capture;
                } else if (buf.Contains("Comments")) {
                    continue;
                } else {
                    string[] args = buf.Split(':');
                    if (args.Length < 2) {
                        continue;
                    }

                    string key = args[0].Trim();
                    string value = args[1].Trim();

                    SetValue(key, value, section);
                }
            }
        }

        private void SetValue(string key, string value, PrjSections section) {
            switch (section) {
                case PrjSections.Main:
                    SetMainValue(key, value);
                    break;
                case PrjSections.Customer:
                    SetCustomerValue(key, value);
                    break;
                case PrjSections.Capture:
                    AddCaptureFile(key, value);
                    break;
            }
        }

        private void SetMainValue(string key, string value) {
            if (key == "Brand Name") {
                BrandName = value;
            } else if (key == "Device Type") {
                DeviceType = value;
            } else if (key == "Subdevice Type") {
                SubDeviceType = value;
            } else if (key == "Remote Model") {
                RemoteModel = value;
            } else if (key == "Target Model") {
                TargetModel = value;
            } else if (key == "Mode") {
                Mode = value;
            }
        }

        private void SetCustomerValue(string key, string value) {
            if (key == "Customer Name") {
                CustomerName = value;
            } else if (key == "Company Name") {
                CompanyName = value;
            } else if (key == "Address") {
                for (int index = 0; index < Address.Length; index++) {
                    if (Address[index] == string.Empty) {
                        Address[index] = value;
                        break;
                    }
                }
            }
        }

        private void AddCaptureFile(string key, string value) {
            List<CaptureInfo> files = null;
            if (FileList == null) {
                files = new List<CaptureInfo>();
            } else {
                files = new List<CaptureInfo>(FileList);
            }

            CaptureInfo captureInfo = new CaptureInfo();
            captureInfo.KeyLabel = value;
            captureInfo.FileName = key;
            files.Add(captureInfo);

            FileList = files.ToArray();
        }

        public IList<string> CreatePrjHeader(string TN)
        {
            if (!string.IsNullOrEmpty(TN))
                TrackingNumber = TN;
            IList<string> report = new List<string>();

            report.Add("REMOTE CONTROL CAPTURE ----------------------------------------\r\n\r\n");
            report.Add("Copyright 2007 Universal Electronics, Inc.  All Rights Reserved\r\n\r\n");
            report.Add("Tracking Number: " + TrackingNumber + "\r\n");
            report.Add("Primary Number : 0\r\n");
            report.Add("Box Number     : 0\r\n\r\n");
            report.Add("Executor       : \r\n");
            report.Add("Id             : \r\n\r\n");
            report.Add("Brand Name     : " + BrandName + "\r\n");
            report.Add("Device Type    : " + DeviceType + "\r\n");
            report.Add("Subdevice Type : " + SubDeviceType + "\r\n");
            report.Add("Remote Model   : " + RemoteModel + "\r\n");
            report.Add("Target Model   : " + TargetModel + "\r\n");
            report.Add("Homer Model    : \r\n");
            report.Add("Mode           : " + Mode + "\r\n\r\n");
            report.Add("CUSTOMER-------------------------------------------------------\r\n\r\n");
            report.Add("Customer Name  : " + CustomerName + "\r\n");
            report.Add("Company Name   : " + CompanyName + "\r\n");
            report.Add("Address        : " + Address[0] + "\r\n");
            report.Add("Address        : " + Address[1] + "\r\n");
            report.Add("Address        : " + Address[2] + "\r\n");
            report.Add("Address        : " + Address[3] + "\r\n");
            report.Add("Phone          : \r\n\r\n");
            AddComments(report);
            report.Add("Markets        : " + Markets + "\r\n\r\n");
            report.Add("TRACKING-------------------------------------------------------\r\n\r\n");
            report.Add("Logged In      : " + TimeStamp + "       By: " + UserName + "\r\n\r\n");
            report.Add("CAPTURE LIST---------------------------------------------------\r\n\r\n");

            return report;
        }

        private void AddComments(IList<string> report)
        {
            if (string.IsNullOrEmpty(Comments)) {
                return;
            }

            string[] lines = Comments.Split(new char[] { '\r', '\n' }, 
                                            StringSplitOptions.RemoveEmptyEntries);
            string Heading = "Comments";
            foreach (string line in lines)
            {
                report.Add(Heading + "       : " + line + " \r\n");
                Heading = "        ";
            }
            report.Add("\r\n");
        }

        public IList<string> CreatePrjCaptureList()
        {
            IList<string> report = new List<string>();
            string line;

            foreach (CaptureInfo item in FileList)
            {
                string fileName = item.FileName;

                if (string.IsNullOrEmpty(fileName))
                    fileName = _NOFILE;

                line = string.Format("{0}   : {1}\r\n", fileName,
                                                        item.KeyLabel);
                report.Add(line);
            }
            return report;
        }
    }

    public class ReportPrj : ProjectInfo
    {
        public void FromXMLFile(string FileName, ref ReportPrj XmlPrj)
        {
            XmlSerializer Xml = new XmlSerializer(XmlPrj.GetType());
            StreamReader SReader = new StreamReader(FileName);
            XmlPrj = (ReportPrj)Xml.Deserialize(SReader);
            SReader.Close();
        }
    }
}
