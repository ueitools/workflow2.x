using System;
using System.IO;
using System.Windows.Forms;
using BusinessObject;

namespace USBCaptureImport
{
    public class TNAssign
    {
        string domain = null;
        string user = null;
        LockStatusFunctions LockStatus = null;

        public TNAssign()
        {
            domain = SystemInformation.UserDomainName;
            user = SystemInformation.UserName;
            LockStatus = new LockStatusFunctions();
        }

        public bool VerifyAndLockTN(bool AutoAssign, string AssignedTnNumber, out int tnNumber, bool restricted)
        {
            bool Result = false;
            tnNumber = 0;

            try
            {
                if (AutoAssign){
                    tnNumber = GetNextTN(restricted);
                    Result = true;
                }else{
                    if (!int.TryParse(AssignedTnNumber, out tnNumber)){
                        MessageBox.Show("The TN value entered is not in the correct format", "Error");
                    }
                    else{
                        if (LockStatus.LockExistingTN(tnNumber, domain, user))
                            Result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            
            return Result;
        }

        public int GetNextTN(bool restricted)
        {
            if (restricted)
                return LockStatus.IssueNewRestrictedTN(domain, user);
            else
                return LockStatus.IssueNewTN(domain, user);
        }

        public void WriteTnToNetwork(string ImportTnName, string ImportFilePath, int tn)
        {
            string capPath = Properties.Settings.Default.CAPDIR;
            string sourceFile = "\\" + ImportTnName + ".zip";
            int backupNum = 0;
            string backupFile;

            if (!File.Exists(ImportFilePath + sourceFile))     // Don't have a local copy?
                return;

            do
            {
                backupNum++;
                backupFile = string.Format("\\TN{0:D4}-{1}.ZIP", tn, backupNum);
            }
            while (File.Exists(ImportFilePath + backupFile) && backupNum < 500);

            if (File.Exists(capPath + sourceFile))
                File.Move(capPath + sourceFile, ImportFilePath + backupFile);

            File.Copy(ImportFilePath + sourceFile, capPath + sourceFile);
        }
    }
}
