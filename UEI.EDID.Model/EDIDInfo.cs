﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UEI.EDID
{
    public class EDIDInfo
    {
        #region Variables
        #endregion

        #region Properties
        public Int32    SlNo { get; set; }
        public String   MainDevice { get; set; }
        public String   SubDevice { get; set; }
        public String   Component { get; set; }
        public String   Region { get; set; }
        public String   Brand { get; set; }
        public String   Model { get; set; }
        public String   Codeset { get; set; }
        public String   DataSource { get; set; }
        public String   EdIdRawData { get; set; }
        public List<Byte> EdId { get; set; }
        public String   ManufacturerCode { get; set; }
        public String   ManufacturerHexCode { get; set; }
        public String   ManufacturerName { get; set; }        
        public String   ProductCode { get; set; }
        public String   SerialNumber { get; set; }
        public Int32    Week { get; set; }
        public Int32    Year { get; set; }
        public Int32    HorizontalScreenSize { get; set; }
        public Int32    VerticalScreenSize { get; set; }
        public String   VideoInputDefinition { get; set; }
        public String   OSDRawData { get; set; }
        public List<Byte> OSD { get; set; }
        public String   FPAvailable { get; set; }
        public String   FPBytePosition { get; set; }
        public List<Int32> BytePositionforFingerPrint { get; set; }
        public String   CustomFPType0x00 { get; set; }
        public String   CustomFPType0x01 { get; set; }
        public String   CustomFPType0x02 { get; set; }
        public String   CustomFPType0x03 { get; set; }
        public String   CustomFP { get; set; }
        public String   CustomFPOSD { get; set; }
        public String   Only128FP { get; set; }
        public String   Only128FPOSD { get; set; }
        public String   OnlyOSDFP { get; set; }
        public Int64    ConsoleCount { get; set; }
        public String   SuggestedID { get; set; }
        public String   DACPublishedID { get; set; }
        public Int32    TrustLevel { get; set; }
        public String   PhysicalAddress { get; set; }
        //public String   DiscreteProfile { get; set; }
        //public String   DiscreteID { get; set; }
        #endregion

        #region Constrcutor
        public EDIDInfo()
        {            
        }
        #endregion

        #region Methods
        #endregion
    }
    public enum FingerPrintTypes
    {
        CustomFPType0x00,
        CustomFPType0x01,
        CustomFPType0x02,
        CustomFPType0x03,
        CustomFP,
        Only128FP,
        NotSet
    }
}