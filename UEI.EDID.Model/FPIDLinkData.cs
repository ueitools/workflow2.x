﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace UEI.EDID
{
    public class FPIDLinkData
    {
        private String  m_MainDevice = String.Empty;
        public  String  MainDevice
        {
            get { return m_MainDevice; }
            set { m_MainDevice = value; }
        }
        private String m_SubDevice = String.Empty;
        public String SubDevice
        {
            get { return m_SubDevice; }
            set { m_SubDevice = value; }
        }
        private String  m_Brand = String.Empty;
        public  String  Brand
        {
            get { return m_Brand; }
            set { m_Brand = value; }
        }
        private String  m_Model = String.Empty;
        public  String  Model
        {
            get { return m_Model; }
            set { m_Model = value; }
        }
        private String  m_UEIID = String.Empty;
        public  String  UEIID
        {
            get { return m_UEIID; }
            set { m_UEIID = value; }
        }
        private String  m_LinkedID = String.Empty;
        public  String  LinkedID
        {
            get { return m_LinkedID; }
            set { m_LinkedID = value; }
        }
        private String  m_CustomFPType0x00 = String.Empty;
        public  String  CustomFPType0x00
        {
            get { return m_CustomFPType0x00; }
            set { m_CustomFPType0x00 = value; }
        }        
        private String  m_CustomFPType0x01 = String.Empty;
        public  String  CustomFPType0x01
        {
            get { return m_CustomFPType0x01; }
            set { m_CustomFPType0x01 = value; }
        }
        private String  m_CustomFPType0x02 = String.Empty;
        public  String  CustomFPType0x02
        {
            get { return m_CustomFPType0x02; }
            set { m_CustomFPType0x02 = value; }
        }
        private String  m_CustomFPType0x03 = String.Empty;
        public  String  CustomFPType0x03
        {
            get { return m_CustomFPType0x03; }
            set { m_CustomFPType0x03 = value; }
        }
        private String  m_CustomFP = String.Empty;
        public  String  CustomFP
        {
            get { return m_CustomFP; }
            set { m_CustomFP = value; }
        }
        private String  m_Only128FP = String.Empty;
        public  String  Only128FP
        {
            get { return m_Only128FP; }
            set { m_Only128FP = value; }
        }
        private String  m_ProductCode = String.Empty;
        public  String  ProductCode
        {
            get { return m_ProductCode; }
            set { m_ProductCode = value; }
        }
        private Int32   m_ModelWeek;
        public  Int32   ModelWeek
        {
            get { return m_ModelWeek; }
            set { m_ModelWeek = value; }
        }
        private Int32   m_ModelYear;
        public  Int32   ModelYear
        {
            get { return m_ModelYear; }
            set { m_ModelYear = value; }
        }
        private Int32   m_HorizontalScreenSize;
        public  Int32   HorizontalScreenSize
        {
            get { return m_HorizontalScreenSize; }
            set { m_HorizontalScreenSize = value; }
        }
        private Int32   m_VerticalScreenSize;
        public  Int32   VerticalScreenSize
        {
            get { return m_VerticalScreenSize; }
            set { m_VerticalScreenSize = value; }
        }
        private String  m_CPRRule = String.Empty;
        public  String  CPRRule
        {
            get { return m_CPRRule; }
            set { m_CPRRule = value; }
        }
        private String m_Control = String.Empty;
        public String Control
        {
            get { return m_Control; }
            set { m_Control = value; }
        }
        private String  m_PhysicalAddress = String.Empty;
        public  String  PhysicalAddress
        {
            get { return m_PhysicalAddress; }
            set { m_PhysicalAddress = value; }
        }
        private String m_DiscreteProfile = String.Empty;
        public String DiscreteProfile
        {
            get { return m_DiscreteProfile; }
            set { m_DiscreteProfile = value; }
        }
        private String m_DiscreteID = String.Empty;
        public String DiscreteID
        {
            get { return m_DiscreteID; }
            set { m_DiscreteID = value; }
        }
    }
    public class FPIDLinkCollection : Collection<FPIDLinkData>
    {        
        public Dictionary<String, String> GetIDs(EDIDGroupData itemEdId, FingerPrintTypes fpType)
        {
            String strDetectedDeviceType = String.Empty;
            strDetectedDeviceType = itemEdId.MainDevice;

            Dictionary<String, String> result = new Dictionary<String, String>();
            String strIDs = String.Empty;
            String strSPRRule = String.Empty;
            List<String> lstId = new List<String>();
            List<String> lstCPRRule = new List<String>();

            String strDiscreteID = String.Empty;
            String strDiscreteProfile = String.Empty;
            List<String> lstDiscreteProfile = new List<String>();
            List<String> lstDiscreteID = new List<String>();

            if (this.Items != null)
            {
                foreach (FPIDLinkData item in this.Items)
                {
                    switch (fpType)
                    {
                        case FingerPrintTypes.CustomFPType0x00:
                            if ((item.CustomFPType0x00 == itemEdId.CustomFPType0x00) && (item.Brand.Trim().ToLower() == itemEdId.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemEdId.MainDevice.Trim().ToLower()))
                            {
                                if (!lstId.Contains(item.LinkedID))
                                    lstId.Add(item.LinkedID);
                                if (!lstCPRRule.Contains(item.CPRRule))
                                    lstCPRRule.Add(item.CPRRule);
                                if (!lstDiscreteID.Contains(item.DiscreteID))
                                    lstDiscreteID.Add(item.DiscreteID);
                                if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                    lstDiscreteProfile.Add(item.DiscreteProfile);
                            }
                            break;
                        case FingerPrintTypes.CustomFPType0x01:
                            if ((item.CustomFPType0x01 == itemEdId.CustomFPType0x01) && (item.Brand.Trim().ToLower() == itemEdId.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemEdId.MainDevice.Trim().ToLower()))
                            {
                                if (!lstId.Contains(item.LinkedID))
                                    lstId.Add(item.LinkedID);
                                if (!lstCPRRule.Contains(item.CPRRule))
                                    lstCPRRule.Add(item.CPRRule);
                                if (!lstDiscreteID.Contains(item.DiscreteID))
                                    lstDiscreteID.Add(item.DiscreteID);
                                if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                    lstDiscreteProfile.Add(item.DiscreteProfile);
                            }
                            break;
                        case FingerPrintTypes.CustomFPType0x02:
                            if ((item.CustomFPType0x02 == itemEdId.CustomFPType0x02) && (item.Brand.Trim().ToLower() == itemEdId.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemEdId.MainDevice.Trim().ToLower()))
                            {
                                if (!lstId.Contains(item.LinkedID))
                                    lstId.Add(item.LinkedID);
                                if (!lstCPRRule.Contains(item.CPRRule))
                                    lstCPRRule.Add(item.CPRRule);
                                if (!lstDiscreteID.Contains(item.DiscreteID))
                                    lstDiscreteID.Add(item.DiscreteID);
                                if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                    lstDiscreteProfile.Add(item.DiscreteProfile);
                            }
                            break;
                        case FingerPrintTypes.CustomFPType0x03:
                            if ((item.CustomFPType0x03 == itemEdId.CustomFPType0x03) && (item.Brand.Trim().ToLower() == itemEdId.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemEdId.MainDevice.Trim().ToLower()))
                            {
                                if (!lstId.Contains(item.LinkedID))
                                    lstId.Add(item.LinkedID);
                                if (!lstCPRRule.Contains(item.CPRRule))
                                    lstCPRRule.Add(item.CPRRule);
                                if (!lstDiscreteID.Contains(item.DiscreteID))
                                    lstDiscreteID.Add(item.DiscreteID);
                                if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                    lstDiscreteProfile.Add(item.DiscreteProfile);
                            }
                            break;
                        case FingerPrintTypes.CustomFP:
                            if ((item.CustomFP == itemEdId.CustomFP) && (item.Brand.Trim().ToLower() == itemEdId.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemEdId.MainDevice.Trim().ToLower()))
                            {
                                if (!lstId.Contains(item.LinkedID))
                                    lstId.Add(item.LinkedID);
                                if (!lstCPRRule.Contains(item.CPRRule))
                                    lstCPRRule.Add(item.CPRRule);
                                if (!lstDiscreteID.Contains(item.DiscreteID))
                                    lstDiscreteID.Add(item.DiscreteID);
                                if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                    lstDiscreteProfile.Add(item.DiscreteProfile);
                            }
                            break;
                        case FingerPrintTypes.Only128FP:
                            if ((item.Only128FP == itemEdId.Only128FP) && (item.Brand.Trim().ToLower() == itemEdId.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemEdId.MainDevice.Trim().ToLower()))
                            {
                                if (!lstId.Contains(item.LinkedID))
                                    lstId.Add(item.LinkedID);
                                if (!lstCPRRule.Contains(item.CPRRule))
                                    lstCPRRule.Add(item.CPRRule);
                                if (!lstDiscreteID.Contains(item.DiscreteID))
                                    lstDiscreteID.Add(item.DiscreteID);
                                if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                    lstDiscreteProfile.Add(item.DiscreteProfile);
                            }
                            break;
                        case FingerPrintTypes.NotSet:
                            break;                       
                    }                    
                }                
            }
            if (lstId.Count > 0)
            {
                for (int i = 0; i < lstId.Count; i++)
                {
                    if(!String.IsNullOrEmpty(lstId[i]))
                    strIDs += lstId[i] + ",";
                }
                if (strIDs.Length > 0)
                    strIDs = strIDs.Remove(strIDs.Length - 1, 1);
            }
            if (lstCPRRule.Count > 0)
            {
                for (int i = 0; i < lstCPRRule.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstCPRRule[i]))
                    strSPRRule += lstCPRRule[i] + ",";
                }
                if (strSPRRule.Length > 0)
                    strSPRRule = strSPRRule.Remove(strSPRRule.Length - 1, 1);
            }

            if (lstDiscreteProfile.Count > 0)
            {
                for (int i = 0; i < lstDiscreteProfile.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstDiscreteProfile[i]))
                        strDiscreteProfile += lstDiscreteProfile[i] + ",";
                }
                if (strDiscreteProfile.Length > 0)
                    strDiscreteProfile = strDiscreteProfile.Remove(strDiscreteProfile.Length - 1, 1);
            }
            if (lstDiscreteID.Count > 0)
            {
                for (int i = 0; i < lstDiscreteID.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstDiscreteID[i]))
                        strDiscreteID += lstDiscreteID[i] + ",";
                }
                if (strDiscreteID.Length > 0)
                    strDiscreteID = strDiscreteID.Remove(strDiscreteID.Length - 1, 1);
            }
            result.Add("LinkedID", strIDs);
            result.Add("CPRRule", strSPRRule);
            result.Add("DiscreteID", strDiscreteID);
            result.Add("DiscreteProfile", strDiscreteProfile);
            
            return result;
        }
        public Dictionary<String, String> GetIDs(EDIDGroupData itemEdId, String groupName)
        {
            Dictionary<String, String> result = new Dictionary<String, String>();
            String strIDs = String.Empty;
            String strSPRRule = String.Empty;
            List<String> lstId = new List<String>();
            List<String> lstCPRRule = new List<String>();

            String strDiscreteID = String.Empty;
            String strDiscreteProfile = String.Empty;
            List<String> lstDiscreteProfile = new List<String>();
            List<String> lstDiscreteID = new List<String>();

            if (this.Items != null)
            {
                foreach (FPIDLinkData item in this.Items)
                {
                    if (groupName.Contains("PCGroup_"))
                    {
                        if ((item.ModelWeek == itemEdId.ModelWeek) && (item.ModelYear == itemEdId.ModelYear) && (item.Brand.Trim().ToLower() == itemEdId.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemEdId.MainDevice.Trim().ToLower()))
                        {
                            if (!lstId.Contains(item.LinkedID))
                                lstId.Add(item.LinkedID);
                            if (!lstCPRRule.Contains(item.CPRRule))
                                lstCPRRule.Add(item.CPRRule);
                            if (!lstDiscreteID.Contains(item.DiscreteID))
                                lstDiscreteID.Add(item.DiscreteID);
                            if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                lstDiscreteProfile.Add(item.DiscreteProfile);
                        }
                    }
                    else if (groupName.Contains("MYGroup_"))
                    {
                        if ((item.ModelWeek == itemEdId.ModelWeek) && (item.ProductCode == itemEdId.ProductCode) && (item.Brand.Trim().ToLower() == itemEdId.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemEdId.MainDevice.Trim().ToLower()))
                        {
                            if (!lstId.Contains(item.LinkedID))
                                lstId.Add(item.LinkedID);
                            if (!lstCPRRule.Contains(item.CPRRule))
                                lstCPRRule.Add(item.CPRRule);
                            if (!lstDiscreteID.Contains(item.DiscreteID))
                                lstDiscreteID.Add(item.DiscreteID);
                            if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                lstDiscreteProfile.Add(item.DiscreteProfile);
                        }
                    }
                    else if (groupName.Contains("MWGroup_"))
                    {
                        if ((item.ProductCode.Trim().ToLower() == itemEdId.ProductCode.Trim().ToLower()) && (item.ModelYear == itemEdId.ModelYear) && (item.Brand.Trim().ToLower() == itemEdId.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemEdId.MainDevice.Trim().ToLower()))
                        {
                            if (!lstId.Contains(item.LinkedID))
                                lstId.Add(item.LinkedID);
                            if (!lstCPRRule.Contains(item.CPRRule))
                                lstCPRRule.Add(item.CPRRule);
                            if (!lstDiscreteID.Contains(item.DiscreteID))
                                lstDiscreteID.Add(item.DiscreteID);
                            if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                lstDiscreteProfile.Add(item.DiscreteProfile);
                        }
                    }
                }               
            }
            if (lstId.Count > 0)
            {
                for (int i = 0; i < lstId.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstId[i]))
                        strIDs += lstId[i] + ",";
                }
                if (strIDs.Length > 0)
                    strIDs = strIDs.Remove(strIDs.Length - 1, 1);
            }
            if (lstCPRRule.Count > 0)
            {
                for (int i = 0; i < lstCPRRule.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstCPRRule[i]))
                    strSPRRule += lstCPRRule[i] + ",";
                }
                if (strSPRRule.Length > 0)
                    strSPRRule = strSPRRule.Remove(strSPRRule.Length - 1, 1);
            }

            if (lstDiscreteProfile.Count > 0)
            {
                for (int i = 0; i < lstDiscreteProfile.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstDiscreteProfile[i]))
                        strDiscreteProfile += lstDiscreteProfile[i] + ",";
                }
                if (strDiscreteProfile.Length > 0)
                    strDiscreteProfile = strDiscreteProfile.Remove(strDiscreteProfile.Length - 1, 1);
            }
            if (lstDiscreteID.Count > 0)
            {
                for (int i = 0; i < lstDiscreteID.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstDiscreteID[i]))
                        strDiscreteID += lstDiscreteID[i] + ",";
                }
                if (strDiscreteID.Length > 0)
                    strDiscreteID = strDiscreteID.Remove(strDiscreteID.Length - 1, 1);
            }
            result.Add("LinkedID", strIDs);
            result.Add("CPRRule", strSPRRule);
            result.Add("DiscreteID", strDiscreteID);
            result.Add("DiscreteProfile", strDiscreteProfile);
            return result;            
        }        
        public Dictionary<String,String> GetIDs(EDIDInfo itemKey, String brandName, FingerPrintTypes fpType)
        {
            Dictionary<String, String> result = new Dictionary<String, String>();
            String strIDs           = String.Empty;
            String strSPRRule       = String.Empty;
            List<String> lstId      = new List<String>();
            List<String> lstCPRRule = new List<String>();
            
            String strDiscreteID            = String.Empty;
            String strDiscreteProfile       = String.Empty;
            List<String> lstDiscreteProfile = new List<String>();
            List<String> lstDiscreteID      = new List<String>();
            if (this.Items != null)
            {
                foreach (FPIDLinkData item in this.Items)
                {
                    switch (fpType)
                    {
                        case FingerPrintTypes.CustomFPType0x00:
                            if ((item.CustomFPType0x00 == itemKey.CustomFPType0x00) && (item.Brand.Trim().ToLower() == itemKey.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemKey.MainDevice.Trim().ToLower()))
                            {
                                if (!lstId.Contains(item.LinkedID))
                                    lstId.Add(item.LinkedID);
                                if (!lstCPRRule.Contains(item.CPRRule))
                                    lstCPRRule.Add(item.CPRRule);
                                if (!lstDiscreteID.Contains(item.DiscreteID))
                                    lstDiscreteID.Add(item.DiscreteID);
                                if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                    lstDiscreteProfile.Add(item.DiscreteProfile);
                            }
                            break;
                        case FingerPrintTypes.CustomFPType0x01:
                            if ((item.CustomFPType0x01 == itemKey.CustomFPType0x01) && (item.Brand.Trim().ToLower() == itemKey.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemKey.MainDevice.Trim().ToLower()))
                            {
                                if (!lstId.Contains(item.LinkedID))
                                    lstId.Add(item.LinkedID);
                                if (!lstCPRRule.Contains(item.CPRRule))
                                    lstCPRRule.Add(item.CPRRule);
                                if (!lstDiscreteID.Contains(item.DiscreteID))
                                    lstDiscreteID.Add(item.DiscreteID);
                                if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                    lstDiscreteProfile.Add(item.DiscreteProfile);
                            }
                            break;
                        case FingerPrintTypes.CustomFPType0x02:
                            if ((item.CustomFPType0x02 == itemKey.CustomFPType0x02) && (item.Brand.Trim().ToLower() == itemKey.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemKey.MainDevice.Trim().ToLower()))
                            {
                                if (!lstId.Contains(item.LinkedID))
                                    lstId.Add(item.LinkedID);
                                if (!lstCPRRule.Contains(item.CPRRule))
                                    lstCPRRule.Add(item.CPRRule);
                                if (!lstDiscreteID.Contains(item.DiscreteID))
                                    lstDiscreteID.Add(item.DiscreteID);
                                if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                    lstDiscreteProfile.Add(item.DiscreteProfile);
                            }
                            break;
                        case FingerPrintTypes.CustomFPType0x03:
                            if ((item.CustomFPType0x03 == itemKey.CustomFPType0x03) && (item.Brand.Trim().ToLower() == itemKey.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemKey.MainDevice.Trim().ToLower()))
                            {
                                if (!lstId.Contains(item.LinkedID))
                                    lstId.Add(item.LinkedID);
                                if (!lstCPRRule.Contains(item.CPRRule))
                                    lstCPRRule.Add(item.CPRRule);
                                if (!lstDiscreteID.Contains(item.DiscreteID))
                                    lstDiscreteID.Add(item.DiscreteID);
                                if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                    lstDiscreteProfile.Add(item.DiscreteProfile);
                            }
                            break;
                        case FingerPrintTypes.CustomFP:
                            if ((item.CustomFP == itemKey.CustomFP) && (item.Brand.Trim().ToLower() == itemKey.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemKey.MainDevice.Trim().ToLower()))
                            {
                                if (!lstId.Contains(item.LinkedID))
                                    lstId.Add(item.LinkedID);
                                if (!lstCPRRule.Contains(item.CPRRule))
                                    lstCPRRule.Add(item.CPRRule);
                                if (!lstDiscreteID.Contains(item.DiscreteID))
                                    lstDiscreteID.Add(item.DiscreteID);
                                if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                    lstDiscreteProfile.Add(item.DiscreteProfile);
                            }
                            break;
                        case FingerPrintTypes.Only128FP:
                            if ((item.Only128FP == itemKey.Only128FP) && (item.Brand.Trim().ToLower() == itemKey.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemKey.MainDevice.Trim().ToLower()))
                            {
                                if (!lstId.Contains(item.LinkedID))
                                    lstId.Add(item.LinkedID);
                                if (!lstCPRRule.Contains(item.CPRRule))
                                    lstCPRRule.Add(item.CPRRule);
                                if (!lstDiscreteID.Contains(item.DiscreteID))
                                    lstDiscreteID.Add(item.DiscreteID);
                                if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                    lstDiscreteProfile.Add(item.DiscreteProfile);
                            }
                            break;
                        case FingerPrintTypes.NotSet:
                            if ((item.CustomFP == itemKey.CustomFP) && (item.Brand.Trim().ToLower() == itemKey.Brand.Trim().ToLower()) && (item.MainDevice.Trim().ToLower() == itemKey.MainDevice.Trim().ToLower()))
                            {
                                if (!lstId.Contains(item.LinkedID))
                                    lstId.Add(item.LinkedID);
                                if (!lstCPRRule.Contains(item.CPRRule))
                                    lstCPRRule.Add(item.CPRRule);
                                if (!lstDiscreteID.Contains(item.DiscreteID))
                                    lstDiscreteID.Add(item.DiscreteID);
                                if (!lstDiscreteProfile.Contains(item.DiscreteProfile))
                                    lstDiscreteProfile.Add(item.DiscreteProfile);
                            }
                            break;                        
                    }                    
                }
            }
            if (lstId.Count > 0)
            {
                for (int i = 0; i < lstId.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstId[i]))
                    strIDs += lstId[i] + ",";
                }
                if (strIDs.Length > 0)
                    strIDs = strIDs.Remove(strIDs.Length - 1, 1);
            }
            if (lstCPRRule.Count > 0)
            {
                for (int i = 0; i < lstCPRRule.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstCPRRule[i]))
                    strSPRRule += lstCPRRule[i] + ",";
                }
                if (strSPRRule.Length > 0)
                    strSPRRule = strSPRRule.Remove(strSPRRule.Length - 1, 1);
            }

            if (lstDiscreteProfile.Count > 0)
            {
                for (int i = 0; i < lstDiscreteProfile.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstDiscreteProfile[i]))
                        strDiscreteProfile += lstDiscreteProfile[i] + ",";
                }
                if (strDiscreteProfile.Length > 0)
                    strDiscreteProfile = strDiscreteProfile.Remove(strDiscreteProfile.Length - 1, 1);
            }
            if (lstDiscreteID.Count > 0)
            {
                for (int i = 0; i < lstDiscreteID.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstDiscreteID[i]))
                        strDiscreteID += lstDiscreteID[i] + ",";
                }
                if (strDiscreteID.Length > 0)
                    strDiscreteID = strDiscreteID.Remove(strDiscreteID.Length - 1, 1);
            }
            result.Add("LinkedID", strIDs);
            result.Add("CPRRule", strSPRRule);
            result.Add("DiscreteID", strDiscreteID);
            result.Add("DiscreteProfile", strDiscreteProfile);
            return result;
        }
    }
}