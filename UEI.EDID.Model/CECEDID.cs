﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using UEI.Workflow2010.Report.Model;

namespace UEI.EDID
{    
    public class CECEDID
    {
        public Int32        SlNo { get; set; }
        public List<DeviceType> Devices { get; set; }
        public List<String> Region { get; set; }
        public String       Component { get; set; }
        public String       Brand { get; set; }
        public String       Model { get; set; }
        public String       EdIdRawData { get; set; }
        public List<Byte>   EdId { get; set; }
        public String       ManufacturerCode { get; set; }
        public String       ManufacturerHexCode { get; set; }
        public String       ManufacturerName { get; set; }
        public String       ProductCode { get; set; }
        public String       SerialNumber { get; set; }
        public Int32        Week { get; set; }
        public Int32        Year { get; set; }
        public String       VideoInputDefinition { get; set; }
        public String       OSDRawData { get; set; }
        public List<Byte>   OSD { get; set; }
        public String       FPAvailable { get; set; }
        public String       FPBytePosition { get; set; }
        public List<Int32>  BytePositionforFingerPrint { get; set; }
        public String       CustomFP { get; set; }
        public String       CustomFPOSD { get; set; }
        public String       Only128FP { get; set; }
        public String       Only128FPOSD { get; set; }
        public String       OnlyOSDFP { get; set; }
    }
    public class CECEDIDModel : Collection<CECEDID>
    {

    }
}