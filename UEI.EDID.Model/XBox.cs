﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UEI.EDID
{
    public class XBox
    {
        public String MainDevice { get; set; }
        public String Brand { get; set; }
        public Int32 RID { get; set; }
        public String EdIdRawData { get; set; }
        public List<Byte> EdId { get; set; }
        private int m_Status = 0;
        public int Status
        {
            get { return m_Status; }
            set { m_Status = value; }
        }
    }
}