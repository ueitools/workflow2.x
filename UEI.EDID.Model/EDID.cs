﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace UEI.EDID
{
    public class EDID
    {
        #region Variables
        #endregion

        #region Properties
        public IDHeader Header { get; set; }
        public IDManufacturer Manufacturer { get; set; }
        public IDProductCode ProductCode { get; set; }
        public IDSerialNumber SerialNumber { get; set; }
        public WeekOfManufacturer WOfMfg { get; set; }
        public YearOfManufacturer YOfMfg { get; set; }
        public EDIDVersionNumber EDIDVerNo { get; set; }
        public EDIDRevisionNumber EDIDRevVerNo { get; set; }
        public MonitorName Monitor { get; set; }
        public MonitorSerialNumber MonitorSerialNo { get; set; }
        public VideoInputDefinition VideoIn { get; set; }
        public MaxHorizontalImage MaxHorImg { get; set; }
        public MaxVerticalImage MaxVerImg { get; set; }
        public DisplayGamma DispGamma { get; set; }
        public PowerManagementAndSupportFeatures PowerAndSupport { get; set; }
        public ColorCharacteristics ColorChar { get; set; }
        public EstablishedTimingsI EstTmg1 { get; set; }
        public EstablishedTimingsII EstTmg2 { get; set; }
        public ManufacturersTimings MfgTmg { get; set; }
        public StandardTimings StndTmg { get; set; }
        public DetailedDescriptorsI DetDes1 { get; set; }
        public DetailedDescriptorsII DetDes2 { get; set; }
        public DetailedDescriptorsIII DetDes3 { get; set; }
        public DetailedDescriptorsIV DetDes4 { get; set; }
        public Extensions Ext { get; set; }
        public Checksum ChkSum { get; set; }
        public ExtensionHeader ExtHeader { get; set; }
        public DisplaySupport DispSupport { get; set; }
        public VideoDataBlock VidDataBlock { get; set; }
        public AudioDataBlock AudDataBlock { get; set; }
        public VendorSpecificDataBlock VendSpecDataBlock { get; set; }
        public DetailedDescriptorsI_256 DetDes5 { get; set; }
        public DetailedDescriptorsII_256 DetDes6 { get; set; }
        public DetailedDescriptorsIII_256 DetDes7 { get; set; }
        public DetailedDescriptorsIV_256 DetDes8 { get; set; }
        public DetailedDescriptorsV_256 DetDes9 { get; set; }
        public PostDTDPadding PosDTDPadding { get; set; }
        public Checksum_256 ChkSum1 { get; set; }
        public RawData EDIDData { get; set; }
        public static Boolean IsDigital { get; set; }
        #endregion

        #region Constrcutor
        public EDID()
        {
            this.EDIDData = new RawData();
            this.Monitor = new MonitorName();
            this.MonitorSerialNo = new MonitorSerialNumber();
            this.ColorChar = new ColorCharacteristics();
            this.DetDes1 = new DetailedDescriptorsI();
            this.DetDes2 = new DetailedDescriptorsII();
            this.DetDes3 = new DetailedDescriptorsIII();
            this.DetDes4 = new DetailedDescriptorsIV();
            this.DispGamma = new DisplayGamma();
            this.Ext = new Extensions();
            this.EDIDRevVerNo = new EDIDRevisionNumber();
            this.EDIDVerNo = new EDIDVersionNumber();
            this.EstTmg1 = new EstablishedTimingsI();
            this.EstTmg2 = new EstablishedTimingsII();
            this.Header = new IDHeader();
            this.MfgTmg = new ManufacturersTimings();
            this.Manufacturer = new IDManufacturer();
            this.MaxHorImg = new MaxHorizontalImage();
            this.MaxVerImg = new MaxVerticalImage();
            this.PowerAndSupport = new PowerManagementAndSupportFeatures();
            this.ProductCode = new IDProductCode();
            this.SerialNumber = new IDSerialNumber();
            this.StndTmg = new StandardTimings();
            this.VideoIn = new VideoInputDefinition();
            this.WOfMfg = new WeekOfManufacturer();
            this.YOfMfg = new YearOfManufacturer();
            this.ChkSum = new Checksum();
            this.ExtHeader = new ExtensionHeader();
            this.DispSupport = new DisplaySupport();
            this.VidDataBlock = new VideoDataBlock();
            this.AudDataBlock = new AudioDataBlock();
            this.VendSpecDataBlock = new VendorSpecificDataBlock();
            this.DetDes5 = new DetailedDescriptorsI_256();
            this.DetDes6 = new DetailedDescriptorsII_256();
            this.DetDes7 = new DetailedDescriptorsIII_256();
            this.DetDes8 = new DetailedDescriptorsIV_256();
            this.DetDes9 = new DetailedDescriptorsV_256();
            this.PosDTDPadding = new PostDTDPadding();
            this.ChkSum1 = new Checksum_256();
        }
        #endregion

        #region Methods
        #endregion

        #region Sub Classes
        public class IDHeader
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }
            public IDHeader()
            {
                this.Name = String.Empty;
                this.Description = "Fixed Header Pattern";
                this.Bytes = "0-7";
                this.Data = new List<Byte>();
            }
        }
        public class IDManufacturer
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }
            public IDManufacturer()
            {
                this.Name = string.Empty;
                this.Bytes = "8-9";
                this.Description = "Manufacturer ID";
                this.Data = new List<Byte>();
            }            
            public String GetByteData()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {                    
                    String hex = BitConverter.ToString(this.Data.ToArray());
                    hex = hex.Replace("-", " ");
                    _details.Append(hex);
                }
                return _details.ToString().ToUpper();
            }
            public String GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    Byte[] _temp = new Byte[this.Data.Count];
                    int _cnt = 0;

                    for (int i = this.Data.Count - 1; i >= 0; i--)
                    {
                        _temp[_cnt] = this.Data[i];
                        _cnt++;
                    }
                    BitArray _ba = new BitArray(_temp);
                    String _devId = String.Empty;

                    String _let1 = String.Empty;
                    String _let2 = String.Empty;
                    String _let3 = String.Empty;

                    String _tempdata = String.Empty;
                    for (int i = _ba.Count - 1; i >= 0; i--)
                    {
                        if (_ba[i])
                        {
                            _tempdata += "1";
                        }
                        else
                        {
                            _tempdata += "0";
                        }
                    }

                    for (int i = 4; i >= 0; i--)
                    {
                        if (_ba[i])
                            _devId += "1";
                        else
                            _devId += "0";
                    }
                    _let1 = _devId;

                    _devId = String.Empty;
                    for (int i = 9; i >= 5; i--)
                    {
                        if (_ba[i])
                            _devId += "1";
                        else
                            _devId += "0";
                    }
                    _let2 = _devId;

                    _devId = String.Empty;
                    for (int i = 14; i >= 10; i--)
                    {
                        if (_ba[i])
                            _devId += "1";
                        else
                            _devId += "0";
                    }
                    _let3 = _devId;

                    _details.Append(Convert.ToChar(Convert.ToInt32(_let3, 2) + 64).ToString() + Convert.ToChar(Convert.ToInt32(_let2, 2) + 64).ToString() + Convert.ToChar(Convert.ToInt32(_let1, 2) + 64).ToString());
                }
                return _details.ToString();
            }
        }
        public class IDProductCode
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public String GetByteData()
            {                                
                String _strTemp = String.Empty;
                try
                {
                    Int16 _temp = BitConverter.ToInt16(this.Data.ToArray(), 0);
                    _strTemp = _temp.ToString("X4");
                }
                catch
                {
                    _strTemp = String.Empty;
                }                                
                return _strTemp;
            }
            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    Byte[] _temp = new byte[this.Data.Count];
                    int _cnt = 0;
                    for (int i = this.Data.Count - 1; i >= 0; i--)
                    {
                        _temp[_cnt] = this.Data[i];
                        _cnt++;
                    }

                    BitArray _ba = new BitArray(_temp);
                    string _tempdata = string.Empty;
                    for (int i = _ba.Count - 1; i >= 0; i--)
                    {
                        if (_ba[i])
                        {
                            _tempdata += "1";
                        }
                        else
                        {
                            _tempdata += "0";
                        }
                    }
                    if (_tempdata.Length > 0)
                        _details.Append(Convert.ToInt64(_tempdata, 2).ToString());
                }
                return _details.ToString();
            }
            public IDProductCode()
            {
                this.Name = string.Empty;
                this.Description = "Manufacturer Product Code";
                this.Bytes = "10-11";
                this.Data = new List<Byte>();
            }
        }
        public class IDSerialNumber
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public IDSerialNumber()
            {
                this.Name = string.Empty;
                this.Description = "Serial Number";
                this.Bytes = "12-15";
                this.Data = new List<byte>();
            }
            public String GetByteData()
            {
                String _strTemp = String.Empty;
                try
                {
                    
                    Int32 _temp = BitConverter.ToInt32(this.Data.ToArray(), 0);
                    if (_temp == 0 || _temp == 1)
                        _strTemp = "N/A";
                    else
                    _strTemp = _temp.ToString("X4");
                }
                catch
                {
                    _strTemp = "N/A";
                }
                return _strTemp;
            }
            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    Byte[] _temp = new byte[this.Data.Count];
                    int _cnt = 0;
                    for (int i = this.Data.Count - 1; i >= 0; i--)
                    {
                        _temp[_cnt] = this.Data[i];
                        _cnt++;
                    }

                    BitArray _ba = new BitArray(_temp);
                    string _tempdata = string.Empty;
                    for (int i = _ba.Count - 1; i >= 0; i--)
                    {
                        if (_ba[i])
                        {
                            _tempdata += "1";
                        }
                        else
                        {
                            _tempdata += "0";
                        }
                    }
                    if (_tempdata.Length > 0)
                    _details.Append(Convert.ToInt64(_tempdata, 2).ToString());
                }
                return _details.ToString();
            }
        }
        public class WeekOfManufacturer
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public WeekOfManufacturer()
            {
                this.Name = string.Empty;
                this.Description = "Week of Manufacturer";
                this.Bytes = "16";
                this.Data = new List<byte>();
            }
            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    int _weekofMan = this.Data[0];
                    _details.Append(_weekofMan.ToString());
                }
                return _details.ToString();
            }
        }
        public class YearOfManufacturer
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    int _yearofMan = this.Data[0] + 1990;
                    _details.Append(_yearofMan.ToString());
                }
                return _details.ToString();
            }

            public YearOfManufacturer()
            {
                this.Name = string.Empty;
                this.Description = "Year of manufacturer";
                this.Bytes = "17";
                this.Data = new List<byte>();
            }
        }
        public class EDIDVersionNumber
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public EDIDVersionNumber()
            {
                this.Name = string.Empty;
                this.Description = "EDID Version";
                this.Bytes = "18";
                this.Data = new List<byte>();
            }
            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    int _edidVerNo = this.Data[0];
                    _details.Append(_edidVerNo.ToString());
                }
                return _details.ToString();
            }
        }
        public class EDIDRevisionNumber
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public EDIDRevisionNumber()
            {
                this.Name = string.Empty;
                this.Description = "EDID Revision Version";
                this.Bytes = "19";
                this.Data = new List<byte>();
            }

            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    int _edidVerNo = this.Data[0];
                    _details.Append(_edidVerNo.ToString());
                }
                return _details.ToString();
            }
        }
        public class VideoInputDefinition
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }
            public VideoInputDefinition()
            {
                this.Name = String.Empty;
                this.Description = "Video Input Definition";
                this.Bytes = "20";
                this.Data = new List<Byte>();
            }
            public string GetVideoInput()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    bool _isBit7true = false;
                    string _bit65 = string.Empty;

                    BitArray _ba = new BitArray(this.Data.ToArray());
                    for (int i = 7; i >= 0; i--)
                    {
                        if (i == 7)
                        {
                            if (_ba[i])
                            {
                                _isBit7true = true;
                                IsDigital = true;
                                _details.AppendLine("\tDigital");
                            }
                            else
                            {
                                IsDigital = false;
                                _details.AppendLine("\tAnalog");
                            }
                        }                        
                    }
                }
                return _details.ToString();
            }
            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    bool _isBit7true = false;
                    string _bit65 = string.Empty;

                    BitArray _ba = new BitArray(this.Data.ToArray());
                    for (int i = 7; i >= 0; i--)
                    {
                        if (i == 7)
                        {
                            if (_ba[i])
                            {
                                _isBit7true = true;
                                IsDigital = true;
                                _details.AppendLine("\tDigital Input");
                            }
                            else
                            {
                                IsDigital = false;
                                _details.AppendLine("\tAnalog Input");
                            }
                        }
                        if (i != 7 && _isBit7true)
                        {
                            switch (i)
                            {
                                case 6:
                                case 5:
                                case 4:
                                case 3:
                                case 2:
                                case 1:
                                    {
                                        _details.AppendLine("\tReserved ");
                                        break;
                                    }
                                case 0:
                                    {
                                        if (_ba[i])
                                        {
                                            _details.AppendLine("Signal is compatible with VESA DFP 1.x TMDS CRGB, 1 pixel per clock, up to 8 bits per color, MSB aligned");
                                        }
                                        else
                                        {

                                        }
                                        break;
                                    }
                            }
                        }
                        else if (i != 7 && _isBit7true == false)
                        {
                            switch (i)
                            {
                                case 6:
                                    {
                                        if (_ba[i])
                                        {
                                            _bit65 += "1";
                                        }
                                        else
                                        {
                                            _bit65 += "0";
                                        }
                                        break;
                                    }
                                case 5:
                                    {
                                        if (_ba[i])
                                        {
                                            _bit65 += "1";
                                        }
                                        else
                                        {
                                            _bit65 += "0";
                                        }

                                        switch (_bit65)
                                        {
                                            case "00":
                                                {
                                                    _details.AppendLine("\t+0.7/−0.3 V");
                                                    break;
                                                }
                                            case "01":
                                                {
                                                    _details.AppendLine("\t+0.714/−0.286 V");
                                                    break;
                                                }
                                            case "10":
                                                {
                                                    _details.AppendLine("\t+1.0/−0.4 V");
                                                    break;
                                                }
                                            case "11":
                                                {
                                                    _details.AppendLine("\t+0.7/0 V");
                                                    break;
                                                }
                                        }

                                        break;
                                    }
                                case 4:
                                    {
                                        if (_ba[i])
                                            _details.AppendLine("\tBlank-to-black setup (pedestal) expected");
                                        break;
                                    }
                                case 3:
                                    {
                                        if (_ba[i])
                                            _details.AppendLine("\tSeparate sync supported");
                                        break;
                                    }
                                case 2:
                                    {
                                        if (_ba[i])
                                            _details.AppendLine("\tComposite sync (on HSync) supported");
                                        break;
                                    }
                                case 1:
                                    {
                                        if (_ba[i])
                                            _details.AppendLine("\tSync on green supported");
                                        break;
                                    }
                                case 0:
                                    {
                                        if (_ba[i])
                                            _details.AppendLine("\tVSync pulse must be serrated when composite or sync-on-green is used.");
                                        break;
                                    }

                            }
                        }
                    }
                }
                return _details.ToString();
            }
        }
        public class MaxHorizontalImage
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    string _horImg = this.Data[0].ToString();
                    _details.Append(string.Format("\t{0}", _horImg));
                }
                return _details.ToString();
            }
            public MaxHorizontalImage()
            {
                this.Name = String.Empty;
                this.Description = "Maximum Horizontal Image";
                this.Bytes = "21";
                this.Data = new List<Byte>();
            }
        }
        public class MaxVerticalImage
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }
            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    string _verImg = this.Data[0].ToString();
                    _details.Append(string.Format("\t{0}", _verImg));
                }
                return _details.ToString();
            }
            public MaxVerticalImage()
            {
                this.Name = String.Empty;
                this.Description = "Maximum Vertical Image";
                this.Bytes = "22";
                this.Data = new List<Byte>();
            }
        }
        public class DisplayGamma
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }
            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    float _temp = ((float)this.Data[0] + (float)100) / (float)100;
                    _details.Append(_temp.ToString());
                }
                return _details.ToString();
            }
            public DisplayGamma()
            {
                this.Name = String.Empty;
                this.Description = "Display Gamma";
                this.Bytes = "23";
                this.Data = new List<Byte>();
            }
        }
        public class PowerManagementAndSupportFeatures
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }
            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    BitArray _ba = new BitArray(this.Data.ToArray());
                    string _bit34 = string.Empty;
                    for (int i = _ba.Count - 1; i >= 0; i--)
                    {
                        switch (i)
                        {
                            case 7:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\tDPMS standby supported");
                                    break;
                                }
                            case 6:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\tDPMS suspend supported");
                                    break;
                                }
                            case 5:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\tDPMS active-off supported");
                                    break;
                                }
                            case 4:
                                {
                                    if (_ba[i])
                                        _bit34 += "1";
                                    else
                                        _bit34 += "0";
                                    break;
                                }
                            case 3:
                                {
                                    if (_ba[i])
                                        _bit34 += "1";
                                    else
                                        _bit34 += "0";

                                    switch (_bit34)
                                    {
                                        case "00":
                                            {
                                                if (IsDigital)
                                                    _details.AppendLine("\tRGB 4:4:4");
                                                else
                                                    _details.AppendLine("\tMonochrome or Grayscale");
                                                break;
                                            }
                                        case "01":
                                            {
                                                if (IsDigital)
                                                    _details.AppendLine("\tRGB 4:4:4 + YCrCb 4:4:4");
                                                else
                                                    _details.AppendLine("\tRGB color");
                                                break;
                                            }
                                        case "10":
                                            {
                                                if (IsDigital)
                                                    _details.AppendLine("\tRGB 4:4:4 + YCrCb 4:2:2");
                                                else
                                                    _details.AppendLine("\tNon-RGB color");
                                                break;
                                            }
                                        case "11":
                                            {
                                                if (IsDigital)
                                                    _details.AppendLine("\tRGB 4:4:4 + YCrCb 4:4:4 + YCrCb 4:2:2");
                                                else
                                                    _details.AppendLine("\tUndefined");
                                                break;
                                            }
                                    }
                                    break;
                                }
                            case 2:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\tStandard sRGB colour space. Bytes 25–34 must contain sRGB standard values");
                                    break;

                                }
                            case 1:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\tthis bit specifies whether the preferred timing mode includes native pixel format and refresh rate.");
                                    break;
                                }
                            case 0:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("GTF supported with default parameter values.");
                                    break;
                                }
                        }
                    }
                }
                return _details.ToString();
            }
            public PowerManagementAndSupportFeatures()
            {
                this.Name = String.Empty;
                this.Description = "Power Management and Supported Feattures";
                this.Bytes = "24";
                this.Data = new List<Byte>();
            }
        }
        public class ColorCharacteristics
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public ColorCharacteristics()
            {
                this.Name = String.Empty;
                this.Description = "Color Charecteristics";
                this.Bytes = "25-34";
                this.Data = new List<Byte>();
            }
        }
        public class EstablishedTimingsI
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public EstablishedTimingsI()
            {
                this.Name = String.Empty;
                this.Description = "Established Timings - I";
                this.Bytes = "35";
                this.Data = new List<Byte>();
            }

            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    BitArray _ba = new BitArray(this.Data.ToArray());
                    for (int i = _ba.Count - 1; i >= 0; i--)
                    {
                        switch (i)
                        {
                            case 7:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t720x400 @70 Hz");
                                    break;
                                }
                            case 6:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t720x400 @88 Hz");
                                    break;
                                }
                            case 5:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t640x480 @60 Hz");
                                    break;
                                }
                            case 4:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t640x480 @67 Hz");
                                    break;
                                }
                            case 3:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t640x480 @72 Hz");
                                    break;
                                }
                            case 2:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t640x480 @75 Hz");
                                    break;
                                }
                            case 1:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t800x600 @56 Hz");
                                    break;
                                }
                            case 0:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t800x600 @60 Hz");
                                    break;
                                }
                        }
                    }
                }
                return _details.ToString();
            }
        }
        public class EstablishedTimingsII
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public EstablishedTimingsII()
            {
                this.Name = String.Empty;
                this.Description = "Established Timings - II";
                this.Bytes = "36";
                this.Data = new List<Byte>();
            }
            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    BitArray _ba = new BitArray(this.Data.ToArray());
                    for (int i = _ba.Count - 1; i >= 0; i--)
                    {
                        switch (i)
                        {
                            case 7:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t800x600 @72 Hz");
                                    break;
                                }
                            case 6:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t800x600 @75 Hz");
                                    break;
                                }
                            case 5:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t832x624 @75 Hz");
                                    break;
                                }
                            case 4:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t1024x768 @87 Hz, interlaced (1024x768i)");
                                    break;
                                }
                            case 3:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t1024x768 @60 Hz");
                                    break;
                                }
                            case 2:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t1024x768 @72 Hz");
                                    break;
                                }
                            case 1:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t1024x768 @75 Hz");
                                    break;
                                }
                            case 0:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t1280x1024 @75 Hz");
                                    break;
                                }
                        }
                    }
                }
                return _details.ToString();
            }
        }
        public class ManufacturersTimings
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public ManufacturersTimings()
            {
                this.Name = string.Empty;
                this.Description = "Manufacturer Timings";
                this.Bytes = "37";
                this.Data = new List<Byte>();
            }
            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    BitArray _ba = new BitArray(this.Data.ToArray());
                    for (int i = _ba.Count - 1; i >= 0; i--)
                    {
                        switch (i)
                        {
                            case 7:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\tOther manufacturer-specific display modes");
                                    break;
                                }
                            case 6:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\tOther manufacturer-specific display modes");
                                    break;
                                }
                            case 5:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\tOther manufacturer-specific display modes");
                                    break;
                                }
                            case 4:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\tOther manufacturer-specific display modes");
                                    break;
                                }
                            case 3:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\tOther manufacturer-specific display modes");
                                    break;
                                }
                            case 2:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\tOther manufacturer-specific display modes");
                                    break;
                                }
                            case 1:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\tOther manufacturer-specific display modes");
                                    break;
                                }
                            case 0:
                                {
                                    if (_ba[i])
                                        _details.AppendLine("\t1152x870 @ 75 Hz (Apple Macintosh II)");
                                    break;
                                }
                        }
                    }
                }
                return _details.ToString();
            }
        }
        public class StandardTimings
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public StandardTimings()
            {
                this.Name = String.Empty;
                this.Description = "Standard Timings";
                this.Bytes = "38-53";
                this.Data = new List<Byte>();
            }
        }
        public class DetailedDescriptorsI
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public DetailedDescriptorsI()
            {
                this.Name = String.Empty;
                this.Description = "Detailed Descriptor - I";
                this.Bytes = "54-71";
                this.Data = new List<Byte>();
            }
        }
        public class DetailedDescriptorsII
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public DetailedDescriptorsII()
            {
                this.Name = String.Empty;
                this.Description = "Detailed Description - II";
                this.Bytes = "72-89";
                this.Data = new List<Byte>();
            }
        }
        public class DetailedDescriptorsIII
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public DetailedDescriptorsIII()
            {
                this.Name = String.Empty;
                this.Description = "Detailed Descriptor - III";
                this.Bytes = "90-107";
                this.Data = new List<Byte>();
            }                      
        }
        public class DetailedDescriptorsIV
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public DetailedDescriptorsIV()
            {
                this.Name = String.Empty;
                this.Description = "Detailed Descriptor - IV";
                this.Bytes = "108-125";
                this.Data = new List<Byte>();
            }           
        }
        public class MonitorSerialNumber
        {
            public string GetDetails(DetailedDescriptorsIII Desc3, DetailedDescriptorsIV Desc4)
            {
                StringBuilder _details = new StringBuilder();
                String str = String.Empty;
                Boolean isExist = false;
                if (Desc3.Data != null)
                {
                    if (Desc3.Data[3] == 0xFC)
                    {
                        List<Byte> _temp = Desc3.Data.GetRange(5, Desc3.Data.Count - 5);
                        str = Encoding.ASCII.GetString(_temp.ToArray());
                        str = str.Replace("\n", "").Replace("\r", "").Replace("\r\n", "").TrimEnd();
                        isExist = true;
                    }
                    if (Desc3.Data[3] == 0xFF)
                    {
                        List<Byte> _temp = Desc3.Data.GetRange(5, Desc3.Data.Count - 5);
                        str = Encoding.ASCII.GetString(_temp.ToArray());
                        str = str.Replace("\n", "").Replace("\r", "").Replace("\r\n", "").TrimEnd();
                        isExist = true;
                    }
                }
                if (Desc4.Data != null && isExist == false)
                {
                    if (Desc4.Data[3] == 0xFC)
                    {
                        List<Byte> _temp = Desc4.Data.GetRange(5, Desc4.Data.Count - 5);
                        str = Encoding.ASCII.GetString(_temp.ToArray());
                        str = str.Replace("\n", "").Replace("\r", "").Replace("\r\n", "").TrimEnd();
                    }
                    if (Desc4.Data[3] == 0xFF)
                    {
                        List<Byte> _temp = Desc4.Data.GetRange(5, Desc4.Data.Count - 5);
                        str = Encoding.ASCII.GetString(_temp.ToArray());
                        str = str.Replace("\n", "").Replace("\r", "").Replace("\r\n", "").TrimEnd();
                    }
                }
                _details.Append(str);
                return _details.ToString();
            }
        }
        public class MonitorName
        {            
            public string GetDetails(DetailedDescriptorsII Desc2, DetailedDescriptorsIII Desc3, DetailedDescriptorsIV Desc4)
            {
                StringBuilder _details = new StringBuilder();
                String str = String.Empty;
                Boolean isExist = false;
                if (Desc2.Data != null)
                {
                    if (Desc2.Data[3] == 0xFC)
                    {
                        List<Byte> _temp = Desc2.Data.GetRange(5, Desc2.Data.Count - 5);
                        str = Encoding.ASCII.GetString(_temp.ToArray());
                        str = str.Replace("\n", "").Replace("\r", "").Replace("\r\n", "").TrimEnd();
                        isExist = true;
                    }
                }
                if (Desc3.Data != null && isExist == false)
                {
                    if (Desc3.Data[3] == 0xFC)
                    {
                        List<Byte> _temp = Desc3.Data.GetRange(5, Desc3.Data.Count - 5);
                        str = Encoding.ASCII.GetString(_temp.ToArray());
                        str = str.Replace("\n", "").Replace("\r", "").Replace("\r\n", "").TrimEnd();                        
                    }
                    else if (Desc3.Data[3] == 0xFF)
                    {
                        List<Byte> _temp = Desc3.Data.GetRange(5, Desc3.Data.Count - 5);
                        str = Encoding.ASCII.GetString(_temp.ToArray());
                        str = str.Replace("\n", "").Replace("\r", "").Replace("\r\n", "").TrimEnd();
                    }
                    
                }
                if (Desc4.Data != null && isExist == false)
                {
                    if (Desc4.Data[3] == 0xFC)
                    {
                        List<Byte> _temp = Desc4.Data.GetRange(5, Desc4.Data.Count - 5);
                        str = Encoding.ASCII.GetString(_temp.ToArray());
                        str = str.Replace("\n", "").Replace("\r", "").Replace("\r\n", "").TrimEnd();
                    }
                    if (Desc4.Data[3] == 0xFF)
                    {
                        List<Byte> _temp = Desc4.Data.GetRange(5, Desc4.Data.Count - 5);
                        str = Encoding.ASCII.GetString(_temp.ToArray());
                        str = str.Replace("\n", "").Replace("\r", "").Replace("\r\n", "").TrimEnd();
                    }                   
                }
                _details.Append(str);
                return _details.ToString();
            }
        }
        public class Extensions
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public Extensions()
            {
                this.Name = String.Empty;
                this.Description = "Extensions";
                this.Bytes = "126";
                this.Data = new List<Byte>();
            }
            public string GetDetails()
            {
                StringBuilder _details = new StringBuilder();
                if (this.Data != null)
                {
                    int _headerExt = this.Data[0];
                    _details.Append(_headerExt.ToString());
                }
                return _details.ToString();
            }
        }
        public class Checksum
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public Checksum()
            {
                this.Name = String.Empty;
                this.Description = "Check Sum";
                this.Bytes = "127";
                this.Data = new List<Byte>();
            }
        }

        //Added By Satyadeep Behera on 22/04/2014
        public class ExtensionHeader
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }
            public ExtensionHeader()
            {
                this.Name = String.Empty;
                this.Description = "Extension Header";
                this.Bytes = "128-130";
                this.Data = new List<Byte>();
            }            
        }
        public class DisplaySupport
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }
            public DisplaySupport()
            {
                this.Name = String.Empty;
                this.Description = "Display Support";
                this.Bytes = "131";
                this.Data = new List<Byte>();
            }
        }
        public class VideoDataBlock
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }
            public VideoDataBlock()
            {
                this.Name = String.Empty;
                this.Description = "Video Data Block";
                this.Bytes = "132-139";
                this.Data = new List<Byte>();
            }
        }
        public class AudioDataBlock
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }
            public AudioDataBlock()
            {
                this.Name = String.Empty;
                this.Description = "Audio Data Block";
                this.Bytes = "140-143";
                this.Data = new List<Byte>();
            }
        }
        public class VendorSpecificDataBlock
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }
            public VendorSpecificDataBlock()
            {
                this.Name = String.Empty;
                this.Description = "Vendor Specific Data Block";
                this.Bytes = "144-151";
                this.Data = new List<Byte>();
            }
        }
        public class DetailedDescriptorsI_256
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public DetailedDescriptorsI_256()
            {
                this.Name = String.Empty;
                this.Description = "Detailed Descriptor - I";
                this.Bytes = "152-169";
                this.Data = new List<Byte>();
            }
        }
        public class DetailedDescriptorsII_256
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public DetailedDescriptorsII_256()
            {
                this.Name = String.Empty;
                this.Description = "Detailed Descriptor - II";
                this.Bytes = "170-187";
                this.Data = new List<Byte>();
            }
        }
        public class DetailedDescriptorsIII_256
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public DetailedDescriptorsIII_256()
            {
                this.Name = String.Empty;
                this.Description = "Detailed Descriptor - III";
                this.Bytes = "188-205";
                this.Data = new List<Byte>();
            }
        }
        public class DetailedDescriptorsIV_256
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public DetailedDescriptorsIV_256()
            {
                this.Name = String.Empty;
                this.Description = "Detailed Descriptor - IV";
                this.Bytes = "206-223";
                this.Data = new List<Byte>();
            }
        }
        public class DetailedDescriptorsV_256
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public DetailedDescriptorsV_256()
            {
                this.Name = String.Empty;
                this.Description = "Detailed Descriptor - V";
                this.Bytes = "224-241";
                this.Data = new List<Byte>();
            }
        }
        public class PostDTDPadding
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public PostDTDPadding()
            {
                this.Name = String.Empty;
                this.Description = "Post DTD Padding";
                this.Bytes = "242-254";
                this.Data = new List<Byte>();
            }
        }
        public class Checksum_256
        {
            public String Name { get; set; }
            public String Description { get; set; }
            public String Bytes { get; set; }
            public List<Byte> Data { get; set; }

            public Checksum_256()
            {
                this.Name = String.Empty;
                this.Description = "Check Sum";
                this.Bytes = "255";
                this.Data = new List<Byte>();
            }
        }
        public class RawData
        {
            public String Description { get; set; }
            public List<Byte> Data { get; set; }

            public RawData()
            {
                this.Description = "EDID Raw Data";
                this.Data = new List<Byte>();
            }
        }
        #endregion
    }
    public class EDIDData
    {
        private bool EDIDValid = false;
        private byte ReasonForInValidity = 0;
        public byte GetReasonForInValidity()
        {
            return ReasonForInValidity;
        }
        public void SetReasonForValidity(byte Val)
        {
            ReasonForInValidity = Val;
        }
        private int CalculatedCheckSum = 0xFF;
        public void SetCalculatedCheckSum(int val)
        {
            CalculatedCheckSum = val;
        }
        public int GetCalculatedCheckSum()
        {
            return CalculatedCheckSum;
        }
        private byte CheckSumByte;
        public byte GetCheckSumByteInEDID()
        {
            return CheckSumByte;
        }
        public void SetCheckSumByteInEDID(byte val)
        {
            CheckSumByte = val;
        }
        double Rx, Ry, Gx, Gy, Bx, By, Wx, Wy;
        public void SetRx(double val)
        {
            Rx = val;
        }
        public double GetRx()
        {
            return Rx;
        }
        public void SetRy(double val)
        {
            Ry = val;
        }
        public double GetRy()
        {
            return Ry;
        }
        public void SetGx(double val)
        {
            Gx = val;
        }
        public double GetGx()
        {
            return Gx;
        }
        public void SetGy(double val)
        {
            Gy = val;
        }
        public double GetGy()
        {
            return Gy;
        }
        public void SetBx(double val)
        {
            Bx = val;
        }
        public double GetBx()
        {
            return Bx;
        }
        public void SetBy(double val)
        {
            By = val;
        }
        public double GetBy()
        {
            return By;
        }
        public void SetWx(double val)
        {
            Wx = val;
        }
        public double GetWx()
        {
            return Wx;
        }
        public void SetWy(double val)
        {
            Wy = val;
        }
        public double GetWy()
        {
            return Wy;
        }
        public bool IsEDIDValid()
        {
            return EDIDValid;
        }
        public void SetEDIDValidity(bool Val)
        {
            EDIDValid = Val;
        }
        private string Header;
        public string GetHeader()
        {
            return Header;
        }
        public void SetHeader(string Val)
        {
            Header = Val;
        }
        public bool IsHeaderValid()
        {
            if (Header == "INVALID" || Header == "")
                return false;
            else
                return true;
        }
        private string ManufacturerID;
        public string GetManufacturerID()
        {
            return ManufacturerID;
        }        
        public void SetManufacturerID(string Val)
        {
            ManufacturerID = Val;
        }
        private String ManfacturerIDHexCode;
        public String GetManufacturerIDHexCode()
        {
            return ManfacturerIDHexCode;
        }
        public void SetManufacturerIDHexCode(String Val)
        {
            ManfacturerIDHexCode = Val;
        }
        public bool IsManufacturerIDValid()
        {
            if (ManufacturerID == "INVALID" || ManufacturerID == "")
                return false;
            else
                return true;
        }
        private byte ColorBitDepth = 0x00;
        public byte GetColorBitDepth()
        {
            return ColorBitDepth;
        }
        public void SetColorBitDepth(byte Val)
        {
            ColorBitDepth = Val;
        }
        private string strColorBitDefinitionString = "";
        public string GetstrColorBitDefinitionString()
        {
            return strColorBitDefinitionString;
        }
        public void SetstrColorBitDefinitionString(string Val)
        {
            strColorBitDefinitionString = Val;
        }
        private byte DVISupported = 0x00;
        public byte GetDVISupported()
        {
            return DVISupported;
        }
        public void SetDVISupported(byte Val)
        {
            DVISupported = Val;
        }
        private string DVISupportString = "";
        public string GetDVISupportString()
        {
            return DVISupportString;
        }
        public void SetDVISupportString(string Val)
        {
            DVISupportString = Val;
        }
        public char IsDVISupported()
        {
            if (DVISupportString == "DVI is supported")
                return 'Y';
            else
                return 'N';
        }
        public char IsHDMI_a_Supported()
        {
            if (DVISupportString == "HDMI-a is supported")
                return 'Y';
            else
                return 'N';
        }
        public char IsHDMI_b_Supported()
        {
            if (DVISupportString == "HDMI-b is supported")
                return 'Y';
            else
                return 'N';
        }
        public char IsMDDI_Supported()
        {
            if (DVISupportString == "MDDI is supported")
                return 'Y';
            else
                return 'N';
        }
        public char IsDisplayPortSupported()
        {
            if (DVISupportString == "Display Port is supported")
                return 'Y';
            else
                return 'N';
        }
        private string ProductID;
        public string GetProductID()
        {
            return ProductID;
        }
        public void SetProductID(string Val)
        {
            ProductID = Val;
        }
        public bool IsProductIDValid()
        {
            if (ProductID == "INVALID" || ProductID == "")
                return false;
            else
                return true;
        }
        private string SerialNumber;
        private bool NonZeroSerialNumber = false;
        public string GetSerialNumber()
        {
            return SerialNumber;
        }
        public void SetSerialNumber(string Val)
        {
            SerialNumber = Val;
        }
        public void SetNonZeroSerialNumber(bool Val)
        {
            NonZeroSerialNumber = Val;
        }
        public bool IsNonZeroSerialNumber()
        {
            return NonZeroSerialNumber;
        }
        private byte WeekOfManufactur = 0;
        public byte GetWeekOfManufactur()
        {
            return WeekOfManufactur;
        }
        public void SetWeekOfManufactur(byte Val)
        {
            WeekOfManufactur = Val;
        }
        private string YearOfManufacture;
        public string GetYearOfManufacture()
        {
            return YearOfManufacture;
        }
        public void SetYearOfManufacture(string Val)
        {
            YearOfManufacture = Val;
        }
        private uint VersionNumber = 0;
        public uint GetVersionNumber()
        {
            return VersionNumber;
        }
        public void SetVersionNumber(uint Val)
        {
            VersionNumber = Val;
        }
        private uint RevisionNumber = 0;
        public uint GetRevisionNumber()
        {
            return RevisionNumber;
        }
        public void SetRevisionNumber(uint Val)
        {
            RevisionNumber = Val;
        }
        private string EDIDVersion;
        public string GetEDIDVersion()
        {
            return EDIDVersion;
        }
        public void SetEDIDVersion(string Val)
        {
            EDIDVersion = Val;
        }
        private string MonitorName;
        public string GetMonitorName()
        {
            return MonitorName;
        }
        public void SetMonitorName(string Val)
        {
            MonitorName = Val;
        }
        private string ASCIIText = "";
        public string GetASCIIText()
        {
            return ASCIIText;
        }
        public void SetASCIIText(string Val)
        {
            ASCIIText = Val;
        }
        private string MonitorSerialNumber;
        public string GetMonitorSerialNumber()
        {
            return MonitorSerialNumber;
        }
        public void SetMonitorSerialNumber(string Val)
        {
            MonitorSerialNumber = Val;
        }
        private string VideoInputType;
        public string GetVideoInputType()
        {
            return VideoInputType;
        }
        public void SetVideoInputType(string Val)
        {
            VideoInputType = Val;
        }
        private string VideoInputDefintionByte;
        public string GetVideoInputDefintionByte()
        {
            return VideoInputDefintionByte;
        }
        public void SetVideoInputDefintionByte(string Val)
        {
            VideoInputDefintionByte = Val;
        }
        private string WhiteAndSyncLevel;
        public string GetWhiteAndSyncLevel()
        {
            return WhiteAndSyncLevel;
        }
        public void SetWhiteAndSyncLevel(string Val)
        {
            WhiteAndSyncLevel = Val;
        }
        private bool BlankToBlackSetup = false;
        public bool IsBlankToBlackSetupExpected()
        {
            return BlankToBlackSetup;
        }
        public void SetBlankToBlackSetup(bool val)
        {
            BlankToBlackSetup = val;
        }
        double AspectRatioPortrait = 0.0;
        public double GetAspectRatioPortrait()
        {
            return AspectRatioPortrait;
        }
        public void SetAspectRatioPortrait(double val)
        {
            AspectRatioPortrait = val;
        }
        double AspectRatioLandscape = 0.0;
        public double GetAspectRatioLandscape()
        {
            return AspectRatioLandscape;
        }
        public void SetAspectRatioLandscape(double val)
        {
            AspectRatioLandscape = val;
        }
        private bool SeparateSyncsSupported = false;
        public bool IsSeparateSyncsSupported()
        {
            return SeparateSyncsSupported;
        }
        public void SetSeparateSyncsSupported(bool val)
        {
            SeparateSyncsSupported = val;
        }
        private bool CompositeSyncSupported = false;
        public bool IsCompositeSyncSupported()
        {
            return CompositeSyncSupported;
        }
        public void SetCompositeSyncSupported(bool val)
        {
            CompositeSyncSupported = val;
        }

        private bool SyncOnGreenSupported = false;
        public bool IsSyncOnGreenSupported()
        {
            return SyncOnGreenSupported;
        }
        public void SetSyncOnGreenSupported(bool val)
        {
            SyncOnGreenSupported = val;
        }

        private bool SerrationOfVSync = false;
        public bool IsSerrationOfVSync()
        {
            return SerrationOfVSync;
        }
        public void SetSerrationOfVSync(bool val)
        {
            SerrationOfVSync = val;
        }

        private string DisplayIsProjector;
        public string GetDisplayIsProjector()
        {
            return DisplayIsProjector;
        }
        public void SetDisplayIsProjector(string Val)
        {
            DisplayIsProjector = Val;
        }

        private string MaxHorImageSize;
        public string GetMaxHorImageSize()
        {
            return MaxHorImageSize;
        }
        public void SetMaxHorImageSize(string Val)
        {
            MaxHorImageSize = Val;
        }

        private string MaxVerImageSize;
        public string GetMaxVerImageSize()
        {
            return MaxVerImageSize;
        }
        public void SetMaxVerImageSize(string Val)
        {
            MaxVerImageSize = Val;
        }

        private string DisplayGamma;
        public string GetDisplayGamma()
        {
            return DisplayGamma;
        }
        public void SetDisplayGamma(string Val)
        {
            DisplayGamma = Val;
        }

        string DMPSStandBy = "";
        public string GetDMPSStandBy()
        {
            return DMPSStandBy;
        }
        public void SetDMPSStandBy(string Val)
        {
            DMPSStandBy = Val;
        }

        string DPMSSuspend = "";
        public string GetDPMSSuspend()
        {
            return DPMSSuspend;
        }
        public void SetDPMSSuspend(string Val)
        {
            DPMSSuspend = Val;
        }

        string DPMSActiveOff = "";
        public string GetDPMSActiveOff()
        {
            return DPMSActiveOff;
        }
        public void SetDPMSActiveOff(string Val)
        {
            DPMSActiveOff = Val;
        }

        private string strDisplayColorType = "";
        public void SetstrDisplayColorType(string Val)
        {
            strDisplayColorType = Val;
        }
        public string GetstrDisplayColorType()
        {
            return strDisplayColorType;
        }

        private byte DisplayColorType = 0x00;
        public char IsDisplayMonochrome()
        {
            if ((DisplayColorType & 0x00) > 0)
                return 'Y';
            else
                return 'N';
        }
        public char IsDisplayRGBcolor()
        {
            if ((DisplayColorType & 0x08) > 0)
                return 'Y';
            else
                return 'N';
        }
        public char IsDisplayNon_RGBcolor()
        {
            if ((DisplayColorType & 0x10) > 0)
                return 'Y';
            else
                return 'N';
        }

        public void SetDisplayColorType(byte Val)
        {
            DisplayColorType = Val;
        }
        public byte GetDisplayColorType()
        {
            return DisplayColorType;
        }

        private byte ColorEncodingFormats = 0x00;
        public void SetColorEncodingFormats(byte Val)
        {
            ColorEncodingFormats = Val;
        }
        public byte GetColorEncodingFormats()
        {
            return ColorEncodingFormats;
        }
        public char IsRGB4_4_4()
        {
            if (strColorEncodingFormat == "RGB 4:4:4")
                return 'Y';
            else
                return 'N';
        }

        public char IsRGB4_4_4_YCrCb4_4_4()
        {
            if (strColorEncodingFormat == "RGB 4:4:4 and YCrCb 4:4:4")
                return 'Y';
            else
                return 'N';
        }
        public char IsRGB4_4_4_YCrCb4_2_2()
        {
            if (strColorEncodingFormat == "RGB 4:4:4 and YCrCb 4:2:2")
                return 'Y';
            else
                return 'N';
        }
        public char IsRGB4_4_4_YCrCb4_4_4_YCrCb4_2_2()
        {
            if (strColorEncodingFormat == "RGB 4:4:4 and YCrCb 4:4:4 and YCrCb 4:2:2")
                return 'Y';
            else
                return 'N';
        }
        private string strColorEncodingFormat = "";
        public void SetstrColorEncodingFormat(string Val)
        {
            strColorEncodingFormat = Val;
        }
        public string GetstrColorEncodingFormat()
        {
            return strColorEncodingFormat;
        }

        bool sRGBDefault = false;
        public void SetsRGBDefault(bool Val)
        {
            sRGBDefault = Val;
        }
        public bool GetsRGBDefault()
        {
            return sRGBDefault;
        }
        bool NPFandRRInlcuded = false;
        public void SetNPFandRRInlcuded(bool Val)
        {
            NPFandRRInlcuded = Val;
        }
        public bool GetNPFandRRInlcuded()
        {
            return NPFandRRInlcuded;
        }
        bool boolDCF = false;
        public void SetDCF(bool Val)
        {
            boolDCF = Val;
        }
        public bool GetDCF()
        {
            return boolDCF;
        }

        string Descriptor1IsPreferredTiming = "";
        public string GetDescriptor1IsPreferredTiming()
        {
            return Descriptor1IsPreferredTiming;
        }
        public void SetDescriptor1IsPreferredTiming(string Val)
        {
            Descriptor1IsPreferredTiming = Val;
        }

        string Descriptor1 = "";
        public string GetDescriptor1()
        {
            return Descriptor1;
        }
        public void SetDescriptor1(string Val)
        {
            Descriptor1 = Val;
        }

        //Monitor
        List<Byte> MonitorByteDescriptor1 = new List<Byte>();
        public List<Byte> GetMonitorByteDescriptor1()
        {
            return MonitorByteDescriptor1;
        }
        public void SetMonitorByteDescriptor1(List<Byte> Val)
        {
            MonitorByteDescriptor1 = Val;
        }
        //Monitor Range Limit
        List<Byte> MonitorRangeLimitByteDescriptor1 = new List<Byte>();
        public List<Byte> GetMonitorRangeLimitByteDescriptor1()
        {
            return MonitorRangeLimitByteDescriptor1;
        }
        public void SetMonitorRangeLimitByteDescriptor1(List<Byte> Val)
        {
            MonitorRangeLimitByteDescriptor1 = Val;
        }
        //Detailed Timings
        List<Byte> DetailedTimingByteDescriptor1 = new List<Byte>();
        public List<Byte> GetDetailedTimingByteDescriptor1()
        {
            return DetailedTimingByteDescriptor1;
        }
        public void SetDetailedTimingByteDescriptor1(List<Byte> Val)
        {
            DetailedTimingByteDescriptor1 = Val;
        }

        string ChromacityCoordinates = "";
        public string GetChromacityCoordinates()
        {
            return ChromacityCoordinates;
        }
        public void SetChromacityCoordinates(string Val)
        {
            ChromacityCoordinates = Val;
        }

        string EstablishedTimingBitmap = "";
        public string GetEstablishedTimingBitmap()
        {
            return EstablishedTimingBitmap;
        }
        public void SetEstablishedTimingBitmap(string Val)
        {
            EstablishedTimingBitmap = Val;
        }

        string StandardDisplayModes = "";
        public string GetStandardDisplayModes()
        {
            return StandardDisplayModes;
        }
        public void SetStandardDisplayModes(string Val)
        {
            StandardDisplayModes = Val;
        }

        string Descriptor1_Type = "";
        public string GetDescriptor1_Type()
        {
            return Descriptor1_Type;
        }
        public void SetDescriptor1_Type(string Val)
        {
            Descriptor1_Type = Val;
        }

        string Descriptor2_Type = "";
        public string GetDescriptor2_Type()
        {
            return Descriptor2_Type;
        }
        public void SetDescriptor2_Type(string Val)
        {
            Descriptor2_Type = Val;
        }
        string Descriptor2 = "";
        public string GetDescriptor2()
        {
            return Descriptor2;
        }
        public void SetDescriptor2(string Val)
        {
            Descriptor2 = Val;
        }
        //Monitor
        List<Byte> MonitorByteDescriptor2 = new List<Byte>();
        public List<Byte> GetMonitorByteDescriptor2()
        {
            return MonitorByteDescriptor2;
        }
        public void SetMonitorByteDescriptor2(List<Byte> Val)
        {
            MonitorByteDescriptor2 = Val;
        }
        //Monitor Range Limit
        List<Byte> MonitorRangeLimitByteDescriptor2 = new List<Byte>();
        public List<Byte> GetMonitorRangeLimitByteDescriptor2()
        {
            return MonitorRangeLimitByteDescriptor2;
        }
        public void SetMonitorRangeLimitByteDescriptor2(List<Byte> Val)
        {
            MonitorRangeLimitByteDescriptor2 = Val;
        }
        //Detailed Timings
        List<Byte> DetailedTimingByteDescriptor2 = new List<Byte>();
        public List<Byte> GetDetailedTimingByteDescriptor2()
        {
            return DetailedTimingByteDescriptor2;
        }
        public void SetDetailedTimingByteDescriptor2(List<Byte> Val)
        {
            DetailedTimingByteDescriptor2 = Val;
        }
        string Descriptor3_Type = "";
        public string GetDescriptor3_Type()
        {
            return Descriptor3_Type;
        }
        public void SetDescriptor3_Type(string Val)
        {
            Descriptor3_Type = Val;
        }
        string Descriptor3 = "";
        public string GetDescriptor3()
        {
            return Descriptor3;
        }
        public void SetDescriptor3(string Val)
        {
            Descriptor3 = Val;
        }
        //Monitor
        List<Byte> MonitorByteDescriptor3 = new List<Byte>();
        public List<Byte> GetMonitorByteDescriptor3()
        {
            return MonitorByteDescriptor3;
        }
        public void SetMonitorByteDescriptor3(List<Byte> Val)
        {
            MonitorByteDescriptor3 = Val;
        }
        //Monitor Range Limit
        List<Byte> MonitorRangeLimitByteDescriptor3 = new List<Byte>();
        public List<Byte> GetMonitorRangeLimitByteDescriptor3()
        {
            return MonitorRangeLimitByteDescriptor3;
        }
        public void SetMonitorRangeLimitByteDescriptor3(List<Byte> Val)
        {
            MonitorRangeLimitByteDescriptor3 = Val;
        }
        //Detailed Timings
        List<Byte> DetailedTimingByteDescriptor3 = new List<Byte>();
        public List<Byte> GetDetailedTimingByteDescriptor3()
        {
            return DetailedTimingByteDescriptor3;
        }
        public void SetDetailedTimingByteDescriptor3(List<Byte> Val)
        {
            DetailedTimingByteDescriptor3 = Val;
        }
        string Descriptor4_Type = "";
        public string GetDescriptor4_Type()
        {
            return Descriptor4_Type;
        }
        public void SetDescriptor4_Type(string Val)
        {
            Descriptor4_Type = Val;
        }
        string Descriptor4 = "";
        public string GetDescriptor4()
        {
            return Descriptor4;
        }
        public void SetDescriptor4(string Val)
        {
            Descriptor4 = Val;
        }
        //Monitor
        List<Byte> MonitorByteDescriptor4 = new List<Byte>();
        public List<Byte> GetMonitorByteDescriptor4()
        {
            return MonitorByteDescriptor4;
        }
        public void SetMonitorByteDescriptor4(List<Byte> Val)
        {
            MonitorByteDescriptor4 = Val;
        }
        //Monitor Range Limit
        List<Byte> MonitorRangeLimitByteDescriptor4 = new List<Byte>();
        public List<Byte> GetMonitorRangeLimitByteDescriptor4()
        {
            return MonitorRangeLimitByteDescriptor4;
        }
        public void SetMonitorRangeLimitByteDescriptor4(List<Byte> Val)
        {
            MonitorRangeLimitByteDescriptor4 = Val;
        }
        //Detailed Timings
        List<Byte> DetailedTimingByteDescriptor4 = new List<Byte>();
        public List<Byte> GetDetailedTimingByteDescriptor4()
        {
            return DetailedTimingByteDescriptor4;
        }
        public void SetDetailedTimingByteDescriptor4(List<Byte> Val)
        {
            DetailedTimingByteDescriptor4 = Val;
        }
        byte NumberOfExtensions = 0;
        public byte GetNumberOfExtensions()
        {
            return NumberOfExtensions;
        }
        public void SetNumberOfExtensions(byte Val)
        {
            NumberOfExtensions = Val;
        }
        char Timing_720_400_70 = ' ';
        public void Set_Timing_720_400_70(char info)
        {
            Timing_720_400_70 = info;
        }
        public char Get_Timing_720_400_70()
        {
            return Timing_720_400_70;
        }
        char Timing_720_400_88 = ' ';
        public void Set_Timing_720_400_88(char info)
        {
            Timing_720_400_88 = info;
        }
        public char Get_Timing_720_400_88()
        {
            return Timing_720_400_88;
        }
        char Timing_640_480_60 = ' ';
        public void Set_Timing_640_480_60(char info)
        {
            Timing_640_480_60 = info;
        }
        public char Get_Timing_640_480_60()
        {
            return Timing_640_480_60;
        }
        char Timing_640_480_67 = ' ';
        public void Set_Timing_640_480_67(char info)
        {
            Timing_640_480_67 = info;
        }
        public char Get_Timing_640_480_67()
        {
            return Timing_640_480_67;
        }
        char Timing_640_480_72 = ' ';
        public void Set_Timing_640_480_72(char info)
        {
            Timing_640_480_72 = info;
        }
        public char Get_Timing_640_480_72()
        {
            return Timing_640_480_72;
        }
        char Timing_640_480_75 = ' ';
        public void Set_Timing_640_480_75(char info)
        {
            Timing_640_480_75 = info;
        }
        public char Get_Timing_640_480_75()
        {
            return Timing_640_480_75;
        }
        char Timing_800_600_56 = ' ';
        public void Set_Timing_800_600_56(char info)
        {
            Timing_800_600_56 = info;
        }
        public char Get_Timing_800_600_56()
        {
            return Timing_800_600_56;
        }
        char Timing_800_600_60 = ' ';
        public void Set_Timing_800_600_60(char info)
        {
            Timing_800_600_60 = info;
        }
        public char Get_Timing_800_600_60()
        {
            return Timing_800_600_60;
        }
        char Timing_800_600_72Hz = ' ';
        public void Set_Timing_800_600_72Hz(char info)
        {
            Timing_800_600_72Hz = info;
        }
        public char Get_Timing_800_600_72Hz()
        {
            return Timing_800_600_72Hz;
        }
        char Timing_800_600_75Hz = ' ';
        public void Set_Timing_800_600_75Hz(char info)
        {
            Timing_800_600_75Hz = info;
        }
        public char Get_Timing_800_600_75Hz()
        {
            return Timing_800_600_75Hz;
        }
        char Timing_832_624_75Hz = ' ';
        public void Set_Timing_832_624_75Hz(char info)
        {
            Timing_832_624_75Hz = info;
        }
        public char Get_Timing_832_624_75Hz()
        {
            return Timing_832_624_75Hz;
        }
        char Timing_1024_768_87Hz = ' ';
        public void Set_Timing_1024_768_87Hz(char info)
        {
            Timing_1024_768_87Hz = info;
        }
        public char Get_Timing_1024_768_87Hz()
        {
            return Timing_1024_768_87Hz;
        }
        char Timing_1024_768_60Hz = ' ';
        public void Set_Timing_1024_768_60Hz(char info)
        {
            Timing_1024_768_60Hz = info;
        }
        public char Get_Timing_1024_768_60Hz()
        {
            return Timing_1024_768_60Hz;
        }
        char Timing_1024_768_72Hz = ' ';
        public void Set_Timing_1024_768_72Hz(char info)
        {
            Timing_1024_768_72Hz = info;
        }
        public char Get_Timing_1024_768_72Hz()
        {
            return Timing_1024_768_72Hz;
        }
        char Timing_1024_768_75Hz = ' ';
        public void Set_Timing_1024_768_75Hz(char info)
        {
            Timing_1024_768_75Hz = info;
        }
        public char Get_Timing_1024_768_75Hz()
        {
            return Timing_1024_768_75Hz;
        }
        char Timing_1152_870_75Hz = ' ';
        public void Set_Timing_1152_870_75Hz(char info)
        {
            Timing_1152_870_75Hz = info;
        }
        public char Get_Timing_1152_870_75Hz()
        {
            return Timing_1152_870_75Hz;
        }
        char Timing_1280_1024_75Hz = ' ';
        public void Set_Timing_1280_1024_75Hz(char info)
        {
            Timing_1280_1024_75Hz = info;
        }
        public char Get_Timing_1280_1024_75Hz()
        {
            return Timing_1280_1024_75Hz;
        }
        int[] HorizontalActivePixels = new int[8];
        public void SetHorizontalActivePixels(int index, int value)
        {
            HorizontalActivePixels[index] = value;
        }
        public int GetHorizontalActivePixels(int index)
        {
            return HorizontalActivePixels[index];
        }
        string[] ImageAspectRatio = new string[8];
        public void SetImageAspectRatio(int index, string value)
        {
            ImageAspectRatio[index] = value;
        }
        public string GetImageAspectRatio(int index)
        {
            return ImageAspectRatio[index];
        }
        int[] RefreshRate = new int[8];
        public void SetRefreshRate(int index, int value)
        {
            RefreshRate[index] = value;
        }
        public int GetRefreshRate(int index)
        {
            return RefreshRate[index];
        }
        UInt16 pixelClock = 0;
        public void SetpixelClock(UInt16 Val)
        {
            pixelClock = Val;
        }
        public UInt16 GetpixelClock()
        {
            return pixelClock;
        }
        UInt16 HorizontalActivePixelsOfDescriptor = 0;
        public void SetHorizontalActivePixelsOfDescriptor(UInt16 Val)
        {
            HorizontalActivePixelsOfDescriptor = Val;
        }
        public UInt16 GetHorizontalActivePixelsOfDescriptor()
        {
            return HorizontalActivePixelsOfDescriptor;
        }
        UInt16 HorizontalBlankingPixels = 0;
        public void SetHorizontalBlankingPixels(UInt16 Val)
        {
            HorizontalBlankingPixels = Val;
        }
        public UInt16 GetHorizontalBlankingPixels()
        {
            return HorizontalBlankingPixels;
        }
        UInt16 VerticalActivePixels = 0;
        public void SetVerticalActivePixels(UInt16 Val)
        {
            VerticalActivePixels = Val;
        }
        public UInt16 GetVerticalActivePixels()
        {
            return VerticalActivePixels;
        }
        UInt16 VerticalBlankingPixels = 0;
        public void SetVerticalBlankingPixels(UInt16 Val)
        {
            VerticalBlankingPixels = Val;
        }
        public UInt16 GetVerticalBlankingPixels()
        {
            return VerticalBlankingPixels;
        }
        UInt16 HorizontalSyncOffset = 0;
        public void SetHorizontalSyncOffset(UInt16 Val)
        {
            HorizontalSyncOffset = Val;
        }
        public UInt16 GetHorizontalSyncOffset()
        {
            return HorizontalSyncOffset;
        }
        UInt16 HorizontalSyncPulseWidth = 0;
        public void SetHorizontalSyncPulseWidth(UInt16 Val)
        {
            HorizontalSyncPulseWidth = Val;
        }
        public UInt16 GetHorizontalSyncPulseWidth()
        {
            return HorizontalSyncPulseWidth;
        }
        UInt16 VerticalSyncOffset = 0;
        public void SetVerticalSyncOffset(UInt16 Val)
        {
            VerticalSyncOffset = Val;
        }
        public UInt16 GetVerticalSyncOffset()
        {
            return VerticalSyncOffset;
        }
        UInt16 VerticalSyncPulseWidth = 0;
        public void SetVerticalSyncPulseWidth(UInt16 Val)
        {
            VerticalSyncPulseWidth = Val;
        }
        public UInt16 GetVerticalSyncPulseWidth()
        {
            return VerticalSyncPulseWidth;
        }
        UInt16 HorizontalDisplaySize = 0;
        public void SetHorizontalDisplaySize(UInt16 Val)
        {
            HorizontalDisplaySize = Val;
        }
        public UInt16 GetHorizontalDisplaySize()
        {
            return HorizontalDisplaySize;
        }
        UInt16 VerticalDisplaySize = 0;
        public void SetVerticalDisplaySize(UInt16 Val)
        {
            VerticalDisplaySize = Val;
        }
        public UInt16 GetVerticalDisplaySize()
        {
            return VerticalDisplaySize;
        }
        byte HorizontalBorderPixels = 0;
        public void SetHorizontalBorderPixels(byte Val)
        {
            HorizontalBorderPixels = Val;
        }
        public byte GetHorizontalBorderPixels()
        {
            return HorizontalBorderPixels;
        }
        byte VerticalBorderPixels = 0;
        public void SetVerticalBorderPixels(byte Val)
        {
            VerticalBorderPixels = Val;
        }
        public byte GetVerticalBorderPixels()
        {
            return VerticalBorderPixels;
        }
        bool Interlaced = false;
        public void SetInterlaced(bool Val)
        {
            Interlaced = Val;
        }
        public bool GetInterlaced()
        {
            return Interlaced;
        }
        byte SteroModeBits = 0;
        public void SetSteroModeBits(byte Val)
        {
            SteroModeBits = Val;
        }
        public byte GetSteroModeBits()
        {
            return SteroModeBits;
        }
        public bool IsNormalDisplay_NoStereo()
        {
            if (SteroModeBits == 0 || SteroModeBits == 1)
            {
                return true;
            }
            else
                return false;
        }
        public bool IsFieldsequentialstereo_rightimage()
        {
            if (SteroModeBits == 2)
            {
                return true;
            }
            else
                return false;
        }
        public bool IsFieldsequentialstereo_leftimage()
        {
            if (SteroModeBits == 4)
            {
                return true;
            }
            else
                return false;
        }
        public bool IsTwowayinterleavedstereo_rightimage()
        {
            if (SteroModeBits == 3)
            {
                return true;
            }
            else
                return false;
        }
        public bool IsTwowayinterleavedstereo_leftimage()
        {
            if (SteroModeBits == 5)
            {
                return true;
            }
            else
                return false;
        }
        public bool IsFourwayinterleavedstereo()
        {
            if (SteroModeBits == 6)
            {
                return true;
            }
            else
                return false;
        }
        public bool IsSidebySideinterleavedstereo()
        {
            if (SteroModeBits == 7)
            {
                return true;
            }
            else
                return false;
        }
        byte AnalogDigitalSignalDefinitionBits = 0;
        public void SetAnalogDigitalSignalDefinitionBits(byte Val)
        {
            AnalogDigitalSignalDefinitionBits = Val;
        }
        public byte GetAnalogDigitalSignalDefinitionBits()
        {
            return AnalogDigitalSignalDefinitionBits;
        }
        public bool IsAnalogCompositeSync()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) == 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x04) > 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsBipolarAnalogCompositeSync()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) == 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x04) == 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsAnalogWithoutSerrations()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) == 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x02) == 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsAnalogWithSerrations()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) == 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x02) > 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsSyncOnGreenSignalonly()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) == 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x01) == 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsSyncOnallthreeRGBvideosignals()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) == 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x01) > 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsDigitalCompositeSync()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) > 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x04) == 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsDigitalWithoutSerrations()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) > 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x04) == 0)
                {
                    if ((AnalogDigitalSignalDefinitionBits & 0x02) == 0)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;
        }
        public bool IsDigitalWithSerrations()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) > 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x04) == 0)
                {
                    if ((AnalogDigitalSignalDefinitionBits & 0x02) > 0)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;
        }
        public bool IsDigitalSeparateSync()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) > 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x04) > 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public bool IsDigitalVerticalSyncPositive()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) > 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x04) > 0)
                {
                    if ((AnalogDigitalSignalDefinitionBits & 0x02) > 0)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            return false;
        }
        public bool IsDigitalHorizontalSyncPositive()
        {
            if ((AnalogDigitalSignalDefinitionBits & 0x08) > 0)
            {
                if ((AnalogDigitalSignalDefinitionBits & 0x01) > 0)
                    return true;
                else
                    return false;
            }
            return false;
        }
        public string ExtractEDIDContentsInCSVFormat()
        {
            string EDIDCSVFormat = "";
            EDIDCSVFormat += Header;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += ManufacturerID;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += ProductID;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += SerialNumber;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += WeekOfManufactur.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += YearOfManufacture;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += EDIDVersion;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += MonitorName;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += VideoInputDefintionByte;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += VideoInputType;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += WhiteAndSyncLevel;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += DisplayIsProjector;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += MaxHorImageSize;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += MaxVerImageSize;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += DisplayGamma;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += DMPSStandBy;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += DPMSSuspend;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += DPMSActiveOff;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += "Hex(" + ChromacityCoordinates + ")";
            EDIDCSVFormat += ",";

            EDIDCSVFormat += "Hex(" + EstablishedTimingBitmap + ")";
            EDIDCSVFormat += ",";

            EDIDCSVFormat += "Hex(" + StandardDisplayModes + ")";
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor1IsPreferredTiming;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor1_Type;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor1;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor2_Type;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor2;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor3_Type;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor3;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor4_Type;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Descriptor4;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += NumberOfExtensions.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += CheckSumByte.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += CalculatedCheckSum.ToString();
            EDIDCSVFormat += ",";

            /*EDIDCSVFormat += (CalculatedCheckSum % 256).ToString();
            EDIDCSVFormat += ",";*/

            EDIDCSVFormat += Rx.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Ry.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Gx.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Gy.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Bx.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += By.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Wx.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += Wy.ToString();
            EDIDCSVFormat += ",";

            EDIDCSVFormat += MonitorSerialNumber;
            EDIDCSVFormat += ",";

            EDIDCSVFormat += ASCIIText;
            EDIDCSVFormat += ",";

            return EDIDCSVFormat;
        }       
        String m_PhysicalAddress = String.Empty;
        public String GetPhysicalAddress()
        {
            return m_PhysicalAddress;
        }
        public void SetPhysicalAddress(String Val)
        {
            m_PhysicalAddress = Val;
        }
        //CEC Physical Address
        List<Byte> PhysicalAddress = new List<Byte>();
        public List<Byte> GetPhysicalAddressByteData()
        {
            return PhysicalAddress;
        }
        public void SetPhysicalAddressByteData(List<Byte> Val)
        {
            PhysicalAddress = Val;
        }
        private Boolean m_ExistExtendedDataBlock = false;
        public Boolean GetExtendedDataBlock()
        {
            return m_ExistExtendedDataBlock;
        }
        public void SetExtendedDataBlock(Boolean Val)
        {
            m_ExistExtendedDataBlock = Val;
        }        
    }
}