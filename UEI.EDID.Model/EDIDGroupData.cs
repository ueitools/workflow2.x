﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UEI.EDID
{
    public class EDIDGroupData
    {
        private Int64 m_SlNo = 0;
        public Int64 SlNo
        {
            get { return m_SlNo; }
            set { m_SlNo = value; }
        }
        private String m_EDIDBlock = String.Empty;
        public String EDIDBlock
        {
            get { return m_EDIDBlock; }
            set { m_EDIDBlock = value; }
        }
        private String m_CustomFPType0 = String.Empty;
        public String CustomFPType0x00
        {
            get { return m_CustomFPType0; }
            set { m_CustomFPType0 = value; }
        }
        private String m_CustomFPType1 = String.Empty;
        public String CustomFPType0x01
        {
            get { return m_CustomFPType1; }
            set { m_CustomFPType1 = value; }
        }
        private String m_CustomFPType2 = String.Empty;
        public String CustomFPType0x02
        {
            get { return m_CustomFPType2; }
            set { m_CustomFPType2 = value; }
        }
        private String m_CustomFPType3 = String.Empty;
        public String CustomFPType0x03
        {
            get { return m_CustomFPType3; }
            set { m_CustomFPType3 = value; }
        }
        private String m_CustomFP = String.Empty;
        public String CustomFP
        {
            get { return m_CustomFP; }
            set { m_CustomFP = value; }
        }
        private String m_Only128FP = String.Empty;
        public String Only128FP
        {
            get { return m_Only128FP; }
            set { m_Only128FP = value; }
        }
        private String m_MainDevice = String.Empty;
        public String MainDevice
        {
            get { return m_MainDevice; }
            set { m_MainDevice = value; }
        }
        private String m_SubDevice = String.Empty;
        public String SubDevice
        {
            get { return m_SubDevice; }
            set { m_SubDevice = value; }
        }
        private String m_Region = String.Empty;
        public String Region
        {
            get { return m_Region; }
            set { m_Region = value; }
        }
        private String m_Brand = String.Empty;
        public String Brand
        {
            get { return m_Brand; }
            set { m_Brand = value; }
        }
        private String m_UserEnteredModel = String.Empty;
        public String UserEnteredModel
        {
            get { return m_UserEnteredModel; }
            set { m_UserEnteredModel = value; }
        }
        private String m_UserEnteredCodeset = String.Empty;
        public String UserEnteredCodeset
        {
            get { return m_UserEnteredCodeset; }
            set { m_UserEnteredCodeset = value; }
        }
        private String m_ManufacturerName;
        public String ManufacturerName
        {
            get { return m_ManufacturerName; }
            set { m_ManufacturerName = value; }
        }
        private String m_MonitorName = String.Empty;
        public String MonitorName
        {
            get { return m_MonitorName; }
            set { m_MonitorName = value; }
        }
        private String m_ProductCode = String.Empty;
        public String ProductCode
        {
            get { return m_ProductCode; }
            set { m_ProductCode = value; }
        }
        private Int32 m_ModelYear = 0;
        public Int32 ModelYear
        {
            get { return m_ModelYear; }
            set { m_ModelYear = value; }
        }
        private Int32 m_ModelWeek = 0;
        public Int32 ModelWeek
        {
            get { return m_ModelWeek; }
            set { m_ModelWeek = value; }
        }
        private Int32 m_HorizontalScreenSize = 0;
        public Int32 HorizontalScreenSize
        {
            get { return m_HorizontalScreenSize; }
            set { m_HorizontalScreenSize = value; }
        }
        private Int32 m_VerticalScreenSize = 0;
        public Int32 VerticalScreenSize
        {
            get { return m_VerticalScreenSize; }
            set { m_VerticalScreenSize = value; }
        }
        private Int64 m_ConsoleCount = 0;
        public Int64 CounsoleCount
        {
            get { return m_ConsoleCount; }
            set { m_ConsoleCount = value; }
        }
        private String m_SuggestedID = String.Empty;
        public String SuggestedID
        {
            get { return m_SuggestedID; }
            set { m_SuggestedID = value; }
        }       
        private String m_DACPublishedID = String.Empty;
        public String DACPublishedID
        {
            get { return m_DACPublishedID; }
            set { m_DACPublishedID = value; }
        }
        private String m_TrustLevel = String.Empty;
        public String TrustLevel
        {
            get { return m_TrustLevel; }
            set { m_TrustLevel = value; }
        }
        private String m_BuildRule = String.Empty;
        public String BuildRule
        {
            get { return m_BuildRule; }
            set { m_BuildRule = value; }
        }
        private String m_PhysicalAddress = String.Empty;
        public String PhysicalAddress
        {
            get { return m_PhysicalAddress; }
            set { m_PhysicalAddress = value; }
        }
        private String m_CPRRule = String.Empty;
        public String CPRRule
        {
            get { return m_CPRRule; }
            set { m_CPRRule = value; }
        }
        private String m_DiscreteProfile = String.Empty;
        public String DiscreteProfile
        {
            get { return m_DiscreteProfile; }
            set { m_DiscreteProfile = value; }
        }
        private String m_DiscreteID = String.Empty;
        public String DiscreteID
        {
            get { return m_DiscreteID; }
            set { m_DiscreteID = value; }
        }
    }
    public class EDIDGroup
    {
        private Dictionary<String, Dictionary<String, List<EDIDGroupData>>> m_EdIdGroups = new Dictionary<String, Dictionary<String, List<EDIDGroupData>>>();
        public Dictionary<String, Dictionary<String, List<EDIDGroupData>>> EdIdGroups
        {
            get { return m_EdIdGroups; }
            set { m_EdIdGroups = value; }
        }
        public String GetProductCodeGroupKey(EDIDGroupData item)
        {
            String strCustomPlus128FP = item.CustomFP + "," + item.Only128FP;
            String strProductCodeGroup = "PCGroup_"; Int32 intProductCodeGroupCount = 1;
            String groupKey = String.Empty;
            if (this.EdIdGroups.Count > 0)
            {
                for (int i = 0; i < this.EdIdGroups.Keys.Count; i++)
                {
                    if (this.EdIdGroups.ContainsKey(strProductCodeGroup + intProductCodeGroupCount.ToString()))
                    {
                        if (this.EdIdGroups[strProductCodeGroup + intProductCodeGroupCount.ToString()] != null)
                        {
                            foreach (String groupitem in this.EdIdGroups[strProductCodeGroup + intProductCodeGroupCount.ToString()].Keys)
                            {
                                if (this.EdIdGroups[strProductCodeGroup + intProductCodeGroupCount.ToString()][groupitem] != null)
                                {
                                    foreach (EDIDGroupData itemData in this.EdIdGroups[strProductCodeGroup + intProductCodeGroupCount.ToString()][groupitem])
                                    {
                                        if ((item.ModelYear == itemData.ModelYear) && (item.ModelWeek == itemData.ModelWeek) && (item.ProductCode != itemData.ProductCode))
                                        {
                                            groupKey = strProductCodeGroup + intProductCodeGroupCount.ToString();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        intProductCodeGroupCount = intProductCodeGroupCount + 1;
                    }
                }
            }
            return groupKey;
        }
        public EDIDGroupData GetChangingProductCode(EDIDGroupData item)
        {
            String temp = String.Empty;
            EDIDGroupData m_EDIDGroupData = null;
            if (this.EdIdGroups.Count > 0)
            {
                if (this.EdIdGroups.ContainsKey("MixedGroup"))
                {
                    Int32 groupItemIndex = -1;
                    foreach (String groupitem in this.EdIdGroups["MixedGroup"].Keys)
                    {
                        if (this.EdIdGroups["MixedGroup"][groupitem] != null)
                        {
                            foreach (EDIDGroupData itemData in this.EdIdGroups["MixedGroup"][groupitem])
                            {
                                if ((item.ModelYear == itemData.ModelYear) && (item.ModelWeek == itemData.ModelWeek) && (item.ProductCode != itemData.ProductCode))
                                {
                                    temp = groupitem;
                                    groupItemIndex = this.EdIdGroups["MixedGroup"][groupitem].IndexOf(itemData);
                                    m_EDIDGroupData = itemData;
                                    break;
                                }
                            }
                        }
                    }
                    if ((groupItemIndex != -1) && (!String.IsNullOrEmpty(temp)))
                        this.EdIdGroups["MixedGroup"][temp].RemoveAt(groupItemIndex);
                }
            }
            return m_EDIDGroupData;
        }
        public String GetModelYearGroupKey(EDIDGroupData item)
        {
            String strModelYearGroup = "MYGroup_"; Int32 intModelYearGroupCount = 1;
            String groupKey = String.Empty;
            if (this.EdIdGroups.Count > 0)
            {
                for (int i = 0; i < this.EdIdGroups.Keys.Count; i++)
                {
                    if (this.EdIdGroups.ContainsKey(strModelYearGroup + intModelYearGroupCount.ToString()))
                    {
                        if (this.EdIdGroups[strModelYearGroup + intModelYearGroupCount.ToString()] != null)
                        {
                            foreach (String groupitem in this.EdIdGroups[strModelYearGroup + intModelYearGroupCount.ToString()].Keys)
                            {
                                if (this.EdIdGroups[strModelYearGroup + intModelYearGroupCount.ToString()][groupitem] != null)
                                {
                                    foreach (EDIDGroupData itemData in this.EdIdGroups[strModelYearGroup + intModelYearGroupCount.ToString()][groupitem])
                                    {
                                        if ((item.ProductCode == itemData.ProductCode) && (item.ModelWeek == itemData.ModelWeek) && (item.ModelYear != itemData.ModelYear))
                                        {
                                            groupKey = strModelYearGroup + intModelYearGroupCount.ToString();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        intModelYearGroupCount = intModelYearGroupCount + 1;
                    }
                }
            }
            return groupKey;
        }
        public EDIDGroupData GetChangingModelYear(EDIDGroupData item)
        {
            String temp = String.Empty;
            EDIDGroupData m_EDIDGroupData = null;
            if (this.EdIdGroups.Count > 0)
            {
                if (this.EdIdGroups.ContainsKey("MixedGroup"))
                {
                    Int32 groupItemIndex = -1;
                    foreach (String groupitem in this.EdIdGroups["MixedGroup"].Keys)
                    {
                        if (this.EdIdGroups["MixedGroup"][groupitem] != null)
                        {
                            foreach (EDIDGroupData itemData in this.EdIdGroups["MixedGroup"][groupitem])
                            {
                                if ((item.ProductCode == itemData.ProductCode) && (item.ModelWeek == itemData.ModelWeek) && (item.ModelYear != itemData.ModelYear))
                                {
                                    groupItemIndex = this.EdIdGroups["MixedGroup"][groupitem].IndexOf(itemData);
                                    m_EDIDGroupData = itemData;
                                    temp = groupitem;
                                    break;
                                }
                            }
                        }

                    }
                    if ((groupItemIndex != -1) && (!String.IsNullOrEmpty(temp)))
                        this.EdIdGroups["MixedGroup"][temp].RemoveAt(groupItemIndex);
                }
            }
            return m_EDIDGroupData;
        }
        public EDIDGroupData GetChangingModelWeek(EDIDGroupData item)
        {
            String temp = String.Empty;
            EDIDGroupData m_EDIDGroupData = null;
            if (this.EdIdGroups.Count > 0)
            {
                if (this.EdIdGroups.ContainsKey("MixedGroup"))
                {
                    Int32 groupItemIndex = -1;
                    foreach (String groupitem in this.EdIdGroups["MixedGroup"].Keys)
                    {
                        if (this.EdIdGroups["MixedGroup"][groupitem] != null)
                        {
                            foreach (EDIDGroupData itemData in this.EdIdGroups["MixedGroup"][groupitem])
                            {
                                if ((item.ProductCode == itemData.ProductCode) && (item.ModelYear == itemData.ModelYear) && (item.ModelWeek != itemData.ModelWeek))
                                {
                                    groupItemIndex = this.EdIdGroups["MixedGroup"][groupitem].IndexOf(itemData);
                                    m_EDIDGroupData = itemData;
                                    temp = groupitem;
                                    break;
                                }
                            }
                        }

                    }
                    if ((groupItemIndex != -1) && (!String.IsNullOrEmpty(temp)))
                        this.EdIdGroups["MixedGroup"][temp].RemoveAt(groupItemIndex);
                }
            }
            return m_EDIDGroupData;
        }
        public String GetModelWeekGroupKey(EDIDGroupData item)
        {
            String strModelWeekGroup = "MWGroup_"; Int32 intModelWeekGroupCount = 1;
            String groupKey = String.Empty;
            if (this.EdIdGroups.Count > 0)
            {
                for (int i = 0; i < this.EdIdGroups.Keys.Count; i++)
                {
                    if (this.EdIdGroups.ContainsKey(strModelWeekGroup + intModelWeekGroupCount.ToString()))
                    {
                        if (this.EdIdGroups[strModelWeekGroup + intModelWeekGroupCount.ToString()] != null)
                        {
                            foreach (String groupitem in this.EdIdGroups[strModelWeekGroup + intModelWeekGroupCount.ToString()].Keys)
                            {
                                if (this.EdIdGroups[strModelWeekGroup + intModelWeekGroupCount.ToString()][groupitem] != null)
                                {
                                    foreach (EDIDGroupData itemData in this.EdIdGroups[strModelWeekGroup + intModelWeekGroupCount.ToString()][groupitem])
                                    {
                                        if ((item.ProductCode == itemData.ProductCode) && (item.ModelYear == itemData.ModelYear) && (item.ModelWeek != itemData.ModelWeek))
                                        {
                                            groupKey = strModelWeekGroup + intModelWeekGroupCount.ToString();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        intModelWeekGroupCount = intModelWeekGroupCount + 1;
                    }
                }
            }
            return groupKey;
        }
        public EDIDGroupData GetEdIdGroupData(List<EDIDGroupData> list, EDIDGroupData item)
        {
            EDIDGroupData m_Data = null;
            if (list != null)
            {
                foreach (EDIDGroupData grpData in list)
                {
                    if ((grpData.CustomFP == item.CustomFP) && (grpData.Only128FP == item.Only128FP) && (grpData.Brand == item.Brand) && (grpData.UserEnteredModel == item.UserEnteredModel) && (grpData.UserEnteredCodeset == item.UserEnteredCodeset) && (grpData.MonitorName == item.MonitorName) && (grpData.ManufacturerName == item.ManufacturerName) && (grpData.ProductCode == item.ProductCode) && (grpData.ModelYear == item.ModelYear) && (grpData.ModelWeek == item.ModelWeek) && (grpData.HorizontalScreenSize == item.HorizontalScreenSize) && (grpData.VerticalScreenSize == item.VerticalScreenSize))
                        return grpData;
                }
            }
            return m_Data;
        }
        public EDIDGroupData GetMappedIdInOtherGroupOfSameType(EDIDGroup edidGroup, EDIDGroupData edidGroupedData, String groupName)
        {
            String[] sep = { "," };
            List<String> lstId = new List<String>();
            List<String> lstCPRRule = new List<String>();
            List<String> lstRule = new List<String>();

            String strIDs   = String.Empty;
            String strCPRRule  = String.Empty;
            String strRule = String.Empty;

            String strDiscreteID = String.Empty;
            String strDiscreteProfile = String.Empty;
            List<String> lstDiscreteProfile = new List<String>();
            List<String> lstDiscreteID = new List<String>();

            foreach (String itemKey in edidGroup.EdIdGroups.Keys)
            {
                if (edidGroup.EdIdGroups[itemKey] != null)
                {
                    foreach (String groupItem in this.EdIdGroups[itemKey].Keys)
                    {
                        if (edidGroup.EdIdGroups[itemKey][groupItem] != null)
                        {
                            if (edidGroup.EdIdGroups[itemKey][groupItem].Count > 0)
                            {
                                foreach (EDIDGroupData itemEdId in edidGroup.EdIdGroups[itemKey][groupItem])
                                {
                                    if (groupName.Contains("PCGroup_"))
                                    {
                                        if ((!String.IsNullOrEmpty(itemEdId.SuggestedID)) && (itemEdId.SlNo != edidGroupedData.SlNo) && (itemEdId.ProductCode != edidGroupedData.ProductCode) && (itemEdId.ModelYear == edidGroupedData.ModelYear) && (itemEdId.ModelWeek == edidGroupedData.ModelWeek) && (itemEdId.Brand.Trim().ToLower() == edidGroupedData.Brand.Trim().ToLower()) && (itemEdId.MainDevice.Trim().ToLower() == edidGroupedData.MainDevice.Trim().ToLower()))
                                        {
                                            if (itemEdId.CounsoleCount > 100)
                                            {
                                                if (itemEdId.SuggestedID.Trim().Contains(","))
                                                {
                                                    String[] tempid = itemEdId.SuggestedID.Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                                    if (tempid.Length > 0)
                                                    {
                                                        for (int i = 0; i < tempid.Length; i++)
                                                        {
                                                            if (!lstId.Contains(tempid[i].Trim()))
                                                                lstId.Add(tempid[i].Trim());                                                           
                                                            if (!lstRule.Contains("4"))
                                                                lstRule.Add("4");
                                                            if (!lstDiscreteID.Contains(itemEdId.DiscreteID))
                                                                lstDiscreteID.Add(itemEdId.DiscreteID);
                                                            if (!lstDiscreteProfile.Contains(itemEdId.DiscreteProfile))
                                                                lstDiscreteProfile.Add(itemEdId.DiscreteProfile);
                                                        }
                                                    }
                                                    String[] tempcprrule = itemEdId.CPRRule.Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                                    if (tempcprrule.Length > 0)
                                                    {
                                                        for (int i = 0; i < tempcprrule.Length; i++)
                                                        {
                                                            if (!lstCPRRule.Contains(tempcprrule[i].Trim()))
                                                                lstCPRRule.Add(tempcprrule[i].Trim());
                                                            if (!lstRule.Contains("4"))
                                                                lstRule.Add("4");
                                                            if (!lstDiscreteID.Contains(itemEdId.DiscreteID))
                                                                lstDiscreteID.Add(itemEdId.DiscreteID);
                                                            if (!lstDiscreteProfile.Contains(itemEdId.DiscreteProfile))
                                                                lstDiscreteProfile.Add(itemEdId.DiscreteProfile);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (!lstId.Contains(itemEdId.SuggestedID.Trim()))
                                                        lstId.Add(itemEdId.SuggestedID.Trim());
                                                    if (!lstCPRRule.Contains(itemEdId.CPRRule.Trim()))
                                                        lstCPRRule.Add(itemEdId.CPRRule.Trim());
                                                    if (!lstRule.Contains("4"))
                                                        lstRule.Add("4");
                                                    if (!lstDiscreteID.Contains(itemEdId.DiscreteID))
                                                        lstDiscreteID.Add(itemEdId.DiscreteID);
                                                    if (!lstDiscreteProfile.Contains(itemEdId.DiscreteProfile))
                                                        lstDiscreteProfile.Add(itemEdId.DiscreteProfile);
                                                }
                                            }
                                        }
                                    }
                                    else if (groupName.Contains("MYGroup_"))
                                    {
                                        if ((!String.IsNullOrEmpty(itemEdId.SuggestedID)) && (itemEdId.SlNo != edidGroupedData.SlNo) && (itemEdId.ProductCode == edidGroupedData.ProductCode) && (itemEdId.ModelYear != edidGroupedData.ModelYear) && (itemEdId.ModelWeek == edidGroupedData.ModelWeek) && (itemEdId.Brand.Trim().ToLower() == edidGroupedData.Brand.Trim().ToLower()) && (itemEdId.MainDevice.Trim().ToLower() == edidGroupedData.MainDevice.Trim().ToLower()))
                                        {
                                            if (itemEdId.CounsoleCount > 100)
                                            {
                                                if (itemEdId.SuggestedID.Trim().Contains(","))
                                                {
                                                    String[] tempid = itemEdId.SuggestedID.Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                                    if (tempid.Length > 0)
                                                    {
                                                        for (int i = 0; i < tempid.Length; i++)
                                                        {
                                                            if (!lstId.Contains(tempid[i].Trim()))
                                                                lstId.Add(tempid[i].Trim());
                                                            if (!lstRule.Contains("4"))
                                                                lstRule.Add("4");
                                                            if (!lstDiscreteID.Contains(itemEdId.DiscreteID))
                                                                lstDiscreteID.Add(itemEdId.DiscreteID);
                                                            if (!lstDiscreteProfile.Contains(itemEdId.DiscreteProfile))
                                                                lstDiscreteProfile.Add(itemEdId.DiscreteProfile);
                                                        }
                                                    }
                                                    String[] tempcprrule = itemEdId.CPRRule.Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                                    if (tempcprrule.Length > 0)
                                                    {
                                                        for (int i = 0; i < tempcprrule.Length; i++)
                                                        {
                                                            if (!lstCPRRule.Contains(tempcprrule[i].Trim()))
                                                                lstCPRRule.Add(tempcprrule[i].Trim());
                                                            if (!lstRule.Contains("4"))
                                                                lstRule.Add("4");
                                                            if (!lstDiscreteID.Contains(itemEdId.DiscreteID))
                                                                lstDiscreteID.Add(itemEdId.DiscreteID);
                                                            if (!lstDiscreteProfile.Contains(itemEdId.DiscreteProfile))
                                                                lstDiscreteProfile.Add(itemEdId.DiscreteProfile);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (!lstId.Contains(itemEdId.SuggestedID.Trim()))
                                                        lstId.Add(itemEdId.SuggestedID.Trim());
                                                    if (!lstCPRRule.Contains(itemEdId.CPRRule.Trim()))
                                                        lstCPRRule.Add(itemEdId.CPRRule.Trim());
                                                    if (!lstRule.Contains("4"))
                                                        lstRule.Add("4");
                                                    if (!lstDiscreteID.Contains(itemEdId.DiscreteID))
                                                        lstDiscreteID.Add(itemEdId.DiscreteID);
                                                    if (!lstDiscreteProfile.Contains(itemEdId.DiscreteProfile))
                                                        lstDiscreteProfile.Add(itemEdId.DiscreteProfile);
                                                }
                                            }
                                        }
                                    }
                                    else if (groupName.Contains("MWGroup_"))
                                    {
                                        if ((!String.IsNullOrEmpty(itemEdId.SuggestedID)) && (itemEdId.SlNo != edidGroupedData.SlNo) && (itemEdId.ProductCode == edidGroupedData.ProductCode) && (itemEdId.ModelYear == edidGroupedData.ModelYear) && (itemEdId.ModelWeek != edidGroupedData.ModelWeek) && (itemEdId.Brand.Trim().ToLower() == edidGroupedData.Brand.Trim().ToLower()) && (itemEdId.MainDevice.Trim().ToLower() == edidGroupedData.MainDevice.Trim().ToLower()))
                                        {
                                            if (itemEdId.CounsoleCount > 100)
                                            {
                                                if (itemEdId.SuggestedID.Trim().Contains(","))
                                                {
                                                    String[] tempid = itemEdId.SuggestedID.Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                                    if (tempid.Length > 0)
                                                    {
                                                        for (int i = 0; i < tempid.Length; i++)
                                                        {
                                                            if (!lstId.Contains(tempid[i].Trim()))
                                                                lstId.Add(tempid[i].Trim());
                                                            if (!lstRule.Contains("4"))
                                                                lstRule.Add("4");
                                                            if (!lstDiscreteID.Contains(itemEdId.DiscreteID))
                                                                lstDiscreteID.Add(itemEdId.DiscreteID);
                                                            if (!lstDiscreteProfile.Contains(itemEdId.DiscreteProfile))
                                                                lstDiscreteProfile.Add(itemEdId.DiscreteProfile);
                                                        }
                                                    }
                                                    String[] tempcprrule = itemEdId.CPRRule.Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
                                                    if (tempcprrule.Length > 0)
                                                    {
                                                        for (int i = 0; i < tempcprrule.Length; i++)
                                                        {
                                                            if (!lstCPRRule.Contains(tempcprrule[i].Trim()))
                                                                lstCPRRule.Add(tempcprrule[i].Trim());
                                                            if (!lstRule.Contains("4"))
                                                                lstRule.Add("4");
                                                            if (!lstDiscreteID.Contains(itemEdId.DiscreteID))
                                                                lstDiscreteID.Add(itemEdId.DiscreteID);
                                                            if (!lstDiscreteProfile.Contains(itemEdId.DiscreteProfile))
                                                                lstDiscreteProfile.Add(itemEdId.DiscreteProfile);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (!lstId.Contains(itemEdId.SuggestedID.Trim()))
                                                        lstId.Add(itemEdId.SuggestedID.Trim());
                                                    if (!lstCPRRule.Contains(itemEdId.CPRRule.Trim()))
                                                        lstCPRRule.Add(itemEdId.CPRRule.Trim());
                                                    if (!lstRule.Contains("4"))
                                                        lstRule.Add("4");
                                                    if (!lstDiscreteID.Contains(itemEdId.DiscreteID))
                                                        lstDiscreteID.Add(itemEdId.DiscreteID);
                                                    if (!lstDiscreteProfile.Contains(itemEdId.DiscreteProfile))
                                                        lstDiscreteProfile.Add(itemEdId.DiscreteProfile);
                                                }
                                            }
                                        }
                                    }                                                                                                      
                                }
                            }
                        }
                    }
                }
            }
            if (lstId.Count > 0)
            {
                for (int i = 0; i < lstId.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstId[i]))
                        strIDs += lstId[i] + ",";
                }
                if (strIDs.Length > 0)
                    strIDs = strIDs.Remove(strIDs.Length - 1, 1);
            }
            if (lstCPRRule.Count > 0)
            {
                for (int i = 0; i < lstCPRRule.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstCPRRule[i]))
                        strCPRRule += lstCPRRule[i] + ",";
                }
                if (strCPRRule.Length > 0)
                    strCPRRule = strCPRRule.Remove(strCPRRule.Length - 1, 1);
            }
            if (lstRule.Count > 0)
            {
                for (int i = 0; i < lstRule.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstRule[i]))
                        strRule += lstRule[i] + ",";
                }
                if (strRule.Length > 0)
                    strRule = strRule.Remove(strRule.Length - 1, 1);
            }
            if (lstDiscreteProfile.Count > 0)
            {
                for (int i = 0; i < lstDiscreteProfile.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstDiscreteProfile[i]))
                        strDiscreteProfile += lstDiscreteProfile[i] + ",";
                }
                if (strDiscreteProfile.Length > 0)
                    strDiscreteProfile = strDiscreteProfile.Remove(strDiscreteProfile.Length - 1, 1);
            }
            if (lstDiscreteID.Count > 0)
            {
                for (int i = 0; i < lstDiscreteID.Count; i++)
                {
                    if (!String.IsNullOrEmpty(lstDiscreteID[i]))
                        strDiscreteID += lstDiscreteID[i] + ",";
                }
                if (strDiscreteID.Length > 0)
                    strDiscreteID = strDiscreteID.Remove(strDiscreteID.Length - 1, 1);
            }
            if (!String.IsNullOrEmpty(strIDs))
            {
                edidGroupedData.SuggestedID = strIDs;
                edidGroupedData.CPRRule = strCPRRule;
                edidGroupedData.BuildRule = strRule;
                edidGroupedData.DiscreteID = strDiscreteID;
                edidGroupedData.DiscreteProfile = strDiscreteProfile;
            }
            return edidGroupedData;
        }
    }
}
