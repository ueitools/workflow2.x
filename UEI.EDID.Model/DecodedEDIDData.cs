﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace UEI.EDID
{
    public class DecodedEDIDData
    {
        //public UInt32 RID;//	int	4
        public UInt64 FK_ConsoleEDID_RID;//	bigint	8
        public String m_IDManufacturerName;//	varchar	3            
        [ReadOnly(true)]
        [Description("Manufacturer (3 char) code")]
        [Category("Header Information")]
        [DisplayName("Manufacturer (3 char) code")]
        public String IDManufacturerName
        {
            get { return m_IDManufacturerName; }
            set { m_IDManufacturerName = value; }
        }
        public String m_IDManufacturerHexCode;//	varchar	3            
        [ReadOnly(true)]
        [Description("Manufacturer Hex Code")]
        [Category("Header Information")]
        [DisplayName("Manufacturer Hex Code")]
        public String IDManufacturerHexCode
        {
            get { return m_IDManufacturerHexCode; }
            set { m_IDManufacturerHexCode = value; }
        }
        public string m_IDProductCode;//	varchar	10 
        [ReadOnly(true)]
        [Description("Manufacturer product code")]
        [Category("Header Information")]
        [DisplayName("Product Code")]
        public String IDProductCode
        {
            get { return m_IDProductCode; }
            set { m_IDProductCode = value; }
        }
        public string m_IDSerialNumber; //	varchar	10 
        [ReadOnly(true)]
        [Description("Manufacturer serial number")]
        [Category("Header Information")]
        [DisplayName("Serial Number")]
        public String IDSerialNumber
        {
            get { return m_IDSerialNumber; }
            set { m_IDSerialNumber = value; }
        }
        public UInt32 m_WeekOfManufacture;//	int	4  
        [ReadOnly(true)]
        [Description("Week of manufacture")]
        [Category("Header Information")]
        [DisplayName("Week of Manufacture")]
        public UInt32 WeekOfManufacture
        {
            get { return m_WeekOfManufacture; }
            set { m_WeekOfManufacture = value; }
        }
        public UInt32 m_YearOfManufacture;//	int	4     
        [ReadOnly(true)]
        [Description("Year of manufacture, less 1990. (1990–2245). If week=255, it is the model year instead")]
        [Category("Header Information")]
        [DisplayName("Year of Manufacture")]
        public UInt32 YearOfManufacture
        {
            get { return m_YearOfManufacture; }
            set { m_YearOfManufacture = value; }
        }
        public UInt32 ModelYearFlag;//	int	4      
        public UInt32 m_ModelYear;//	int	4       
        [ReadOnly(true)]
        [Description("Model year of manufacture, less 1990. (1990–2245). If week=255, it is the model year instead")]
        [Category("Header Information")]
        [DisplayName("Model Year")]
        public UInt32 ModelYear
        {
            get { return m_ModelYear; }
            set { m_ModelYear = value; }
        }
        public UInt32 m_VersionNumber;//	int	4
        [ReadOnly(true)]
        [Description("EDID version")]
        [Category("Header Information")]
        [DisplayName("EDID Version")]
        public UInt32 VersionNumber
        {
            get { return m_VersionNumber; }
            set { m_VersionNumber = value; }
        }
        public UInt32 m_RevisionNumber;//	int	4
        [ReadOnly(true)]
        [Description("EDID revision")]
        [Category("Header Information")]
        [DisplayName("EDID Revision")]
        public UInt32 RevisionNumber
        {
            get { return m_RevisionNumber; }
            set { m_RevisionNumber = value; }
        }
        public char m_AnalogVideoSignalInterfaceSupport;//	char	1
        [ReadOnly(true)]
        [Description("Analog video signal interface support")]
        [Category("Basic Display Parameters")]
        [DisplayName("Analog")]
        public Char AnalogVideoSignalInterfaceSupport
        {
            get { return m_AnalogVideoSignalInterfaceSupport; }
            set { m_AnalogVideoSignalInterfaceSupport = value; }
        }
        public char m_DigitalVideoSignalInterfaceSupport;//	char	1
        [ReadOnly(true)]
        [Description("Digital video signal interface support")]
        [Category("Basic Display Parameters")]
        [DisplayName("Digital")]
        public Char DigitalVideoSignalInterfaceSupport
        {
            get { return m_DigitalVideoSignalInterfaceSupport; }
            set { m_DigitalVideoSignalInterfaceSupport = value; }
        }
        public String m_SignalLevelStandard_Video_Sync_Total;//	varchar	60
        [ReadOnly(true)]
        [Description("Video white and sync levels")]
        [Category("Basic Display Parameters")]
        [DisplayName("Video Sync Level")]
        public String SignalLevelStandard_Video_Sync_Total
        {
            get { return m_SignalLevelStandard_Video_Sync_Total; }
            set { m_SignalLevelStandard_Video_Sync_Total = value; }
        }
        public char m_VideoSetupBlackLevel;//	char	1
        [ReadOnly(true)]
        [Description("Video setup black level")]
        [Category("Basic Display Parameters")]
        [DisplayName("Video Setup Black Level")]
        public Char VideoSetupBlackLevel
        {
            get { return m_VideoSetupBlackLevel; }
            set { m_VideoSetupBlackLevel = value; }
        }
        public char m_VideoSetupBlankBlackLevel;//	char	1
        [ReadOnly(true)]
        [Description("Video setup blank black level")]
        [Category("Basic Display Parameters")]
        [DisplayName("Video Setup Blank Black Level")]
        public Char VideoSetupBlankBlackLevel
        {
            get { return m_VideoSetupBlankBlackLevel; }
            set { m_VideoSetupBlankBlackLevel = value; }
        }
        public char m_SeparateSyncHVSignalsSupported;//	char	1
        [ReadOnly(true)]
        [Description("Separate sync HV signals supported")]
        [Category("Basic Display Parameters")]
        [DisplayName("Separate Sync Supported")]
        public Char SeparateSyncHVSignalsSupported
        {
            get { return m_SeparateSyncHVSignalsSupported; }
            set { m_SeparateSyncHVSignalsSupported = value; }
        }
        public char m_CompositeSyncSignalonHorizontalSupported;//	char	1
        [ReadOnly(true)]
        [Description("Composite sync (on HSync) supported")]
        [Category("Basic Display Parameters")]
        [DisplayName("Composite Sync Supported")]
        public Char CompositeSyncSignalonHorizontalSupported
        {
            get { return m_CompositeSyncSignalonHorizontalSupported; }
            set { m_CompositeSyncSignalonHorizontalSupported = value; }
        }
        public char m_CompositeSyncSignalonGreenVideoSupported;//	char	1
        [ReadOnly(true)]
        [Description("Sync on green supported")]
        [Category("Basic Display Parameters")]
        [DisplayName("Sync On Green Supported")]
        public Char CompositeSyncSignalonGreenVideoSupported
        {
            get { return m_CompositeSyncSignalonGreenVideoSupported; }
            set { m_CompositeSyncSignalonGreenVideoSupported = value; }
        }
        public char m_SerrationOnTheVerticalSyncSupported;//	char	1
        [ReadOnly(true)]
        [Description("VSync pulse must be serrated when composite or sync-on-green is used")]
        [Category("Basic Display Parameters")]
        [DisplayName("Vertical Sync Supported")]
        public Char SerrationOnTheVerticalSyncSupported
        {
            get { return m_SerrationOnTheVerticalSyncSupported; }
            set { m_SerrationOnTheVerticalSyncSupported = value; }
        }
        public UInt32 m_ColorBitDepthBitsPerPrimary;//	int	4
        [ReadOnly(true)]
        [Description("Color bit depth bits per primary")]
        [Category("Basic Display Parameters")]
        [DisplayName("Color Bit Depth")]
        public UInt32 ColorBitDepthBitsPerPrimary
        {
            get { return m_ColorBitDepthBitsPerPrimary; }
            set { m_ColorBitDepthBitsPerPrimary = value; }
        }
        public char m_DVISupport;//	char	1
        [ReadOnly(true)]
        [Description("DVI support")]
        [Category("Header Information")]
        [DisplayName("DVI Support")]
        public Char DVISupport
        {
            get { return m_DVISupport; }
            set { m_DVISupport = value; }
        }
        public char m_HDMI_aSupport;//	char	1
        [ReadOnly(true)]
        [Description("HDMI a support")]
        [Category("Header Information")]
        [DisplayName("HDMI A Support")]
        public Char HDMI_aSupport
        {
            get { return m_HDMI_aSupport; }
            set { m_HDMI_aSupport = value; }
        }
        public char m_HDMI_bSupport;//	char	1
        [ReadOnly(true)]
        [Description("HDMI b support")]
        [Category("Header Information")]
        [DisplayName("HDMI B Support")]
        public Char HDMI_bSupport
        {
            get { return m_HDMI_bSupport; }
            set { m_HDMI_bSupport = value; }
        }
        public char m_MDDISupport;//	char	1
        [ReadOnly(true)]
        [Description("MDDI Support")]
        [Category("Header Information")]
        [DisplayName("MDDI Support")]
        public Char MDDISupport
        {
            get { return m_MDDISupport; }
            set { m_MDDISupport = value; }
        }
        public char m_DisplayPortSupport;//	char	1
        [ReadOnly(true)]
        [Description("Display port support")]
        [Category("Header Information")]
        [DisplayName("Display Port Support")]
        public Char DisplayPortSupport
        {
            get { return m_DisplayPortSupport; }
            set { m_DisplayPortSupport = value; }
        }
        public UInt32 m_HorizontalScreenSize;//	int	4
        [ReadOnly(true)]
        [Description("Horizontal screen size")]
        [Category("Timing Descriptor")]
        [DisplayName("Horizontal Screen Size")]
        public UInt32 HorizontalScreenSize
        {
            get { return m_HorizontalScreenSize; }
            set { m_HorizontalScreenSize = value; }
        }
        public UInt32 m_VerticalScreenSize;//	int	4
        [ReadOnly(true)]
        [Description("Vertical screen size")]
        [Category("Timing Descriptor")]
        [DisplayName("Vertical Screen Size")]
        public UInt32 VerticalScreenSize
        {
            get { return m_VerticalScreenSize; }
            set { m_VerticalScreenSize = value; }
        }
        public string m_AspectRatioLandscape;//	char	1
        [ReadOnly(true)]
        [Description("Aspect ratio landscape")]
        [Category("Timing Descriptor")]
        [DisplayName("Aspect Ratio Landscape")]
        public String AspectRatioLandscape
        {
            get { return m_AspectRatioLandscape; }
            set { m_AspectRatioLandscape = value; }
        }
        public string m_AspectRatioPortrait;//	char	1
        [ReadOnly(true)]
        [Description("Aspect ratio portrait")]
        [Category("Timing Descriptor")]
        [DisplayName("Aspect Ratio Portrait")]
        public String AspectRatioPortrait
        {
            get { return m_AspectRatioPortrait; }
            set { m_AspectRatioPortrait = value; }
        }
        public double m_Gamma;//	decimal	5
        [ReadOnly(true)]
        [Description("Display Gamma")]
        [Category("Basic Display Parameters")]
        [DisplayName("Gamma")]
        public Double Gamma
        {
            get { return m_Gamma; }
            set { m_Gamma = value; }
        }
        public char m_StandbySupport;//	char	1
        public char StandbySupport
        {
            get
            {
                return m_StandbySupport;
            }
            set
            {
                m_StandbySupport = value;
            }
        }
        public char m_SuspendSupport;//	char	1
        public char SuspendSupport
        {
            get { return m_SuspendSupport; }
            set { m_SuspendSupport = value; }
        }
        public char m_LowPowerSupport;//	char	1
        public char LowPowerSupport
        {
            get { return m_LowPowerSupport; }
            set { m_LowPowerSupport = value; }
        }
        public char m_Monochrome_GrayScaleDisplay;//	char	1
        public char Monochrome_GrayScaleDisplay
        {
            get { return m_Monochrome_GrayScaleDisplay; }
            set { m_Monochrome_GrayScaleDisplay = value; }
        }
        public char m_RGBcolordisplay;//	char	1
        public char RGBcolordisplay
        {
            get { return m_RGBcolordisplay; }
            set { m_RGBcolordisplay = value; }
        }
        public char m_Non_RGBcolordisplay;//	char	1
        public char Non_RGBcolordisplay
        {
            get { return m_Non_RGBcolordisplay; }
            set { m_Non_RGBcolordisplay = value; }
        }
        public char m_RGB4_4_4;//	char	1
        public char RGB4_4_4
        {
            get { return m_RGB4_4_4; }
            set { m_RGB4_4_4 = value; }
        }
        public char m_RGB4_4_4_YCrCb4_4_4;//	char	1
        public char RGB4_4_4_YCrCb4_4_4
        {
            get { return m_RGB4_4_4_YCrCb4_4_4; }
            set { m_RGB4_4_4_YCrCb4_4_4 = value; }
        }
        public char m_RGB4_4_4_YCrCb4_2_2;//	char	1
        public char RGB4_4_4_YCrCb4_2_2
        {
            get { return m_RGB4_4_4_YCrCb4_2_2; }
            set { m_RGB4_4_4_YCrCb4_2_2 = value; }
        }
        public char m_RGB4_4_4_YCrCb4_4_4_YCrCb4_2_2;//	char	1
        public char RGB4_4_4_YCrCb4_4_4_YCrCb4_2_2
        {
            get { return m_RGB4_4_4_YCrCb4_4_4_YCrCb4_2_2; }
            set { m_RGB4_4_4_YCrCb4_4_4_YCrCb4_2_2 = value; }
        }
        public char m_StandardDefaultColorSpace;//	char	1
        public char StandardDefaultColorSpace
        {
            get { return m_StandardDefaultColorSpace; }
            set { m_StandardDefaultColorSpace = value; }
        }
        public char m_NativePixelFormatIncluded;//	char	1
        public char NativePixelFormatIncluded
        {
            get { return m_NativePixelFormatIncluded; }
            set { m_NativePixelFormatIncluded = value; }
        }
        public char m_ContinuousFrequencyDisplay;//	char	1
        public char ContinuousFrequencyDisplay
        {
            get { return m_ContinuousFrequencyDisplay; }
            set { m_ContinuousFrequencyDisplay = value; }
        }
        public double m_RedX;//	decimal	5
        public double RedX
        {
            get { return m_RedX; }
            set { m_RedX = value; }
        }
        public double m_RedY;//	decimal	5
        public double RedY
        {
            get { return m_RedY; }
            set { m_RedY = value; }
        }
        public double m_GreenX;//	decimal	5
        public double GreenX
        {
            get { return m_GreenX; }
            set { m_GreenX = value; }
        }
        public double m_GreenY;//	decimal	5
        public double GreenY
        {
            get { return m_GreenY; }
            set { m_GreenY = value; }
        }
        public double m_BlueX;//	decimal	5
        public double BlueX
        {
            get { return m_BlueX; }
            set { m_BlueX = value; }
        }
        public double m_BlueY;//	decimal	5
        public double BlueY
        {
            get { return m_BlueY; }
            set { m_BlueY = value; }
        }
        public double m_WhiteX;//	decimal	5
        public double WhiteX
        {
            get { return m_WhiteX; }
            set { m_WhiteX = value; }
        }
        public double m_WhiteY;//	decimal	5
        public double WhiteY
        {
            get { return m_WhiteY; }
            set { m_WhiteY = value; }
        }
        public char m_R720X400_at_70Hz;//	char	1
        public char R720X400_at_70Hz
        {
            get { return m_R720X400_at_70Hz; }
            set { m_R720X400_at_70Hz = value; }
        }
        public char m_R720X400_at_88Hz;//	char	1
        public char R720X400_at_88Hz
        {
            get { return m_R720X400_at_88Hz; }
            set { m_R720X400_at_88Hz = value; }
        }
        public char m_R640X480_at_60Hz;//	char	1
        public char R640X480_at_60Hz
        {
            get { return m_R640X480_at_60Hz; }
            set { m_R640X480_at_60Hz = value; }
        }
        public char m_R640X480_at_67Hz;//	char	1
        public char R640X480_at_67Hz
        {
            get { return m_R640X480_at_67Hz; }
            set { m_R640X480_at_67Hz = value; }
        }
        public char m_R640X480_at_72Hz;//	char	1
        public char R640X480_at_72Hz
        {
            get { return m_R640X480_at_72Hz; }
            set { m_R640X480_at_72Hz = value; }
        }
        public char m_R640X480_at_75Hz;//	char	1
        public char R640X480_at_75Hz
        {
            get { return m_R640X480_at_75Hz; }
            set { m_R640X480_at_75Hz = value; }
        }
        public char m_R800X600_at_56Hz;//char	1
        public char R800X600_at_56Hz
        {
            get { return m_R800X600_at_56Hz; }
            set { m_R800X600_at_56Hz = value; }
        }
        public char m_R800X600_at_60Hz;//char	1
        public char R800X600_at_60Hz
        {
            get { return m_R800X600_at_60Hz; }
            set { m_R800X600_at_60Hz = value; }
        }
        public char m_R800X600_at_72Hz;//char	1
        public char R800X600_at_72Hz
        {
            get { return m_R800X600_at_72Hz; }
            set { m_R800X600_at_72Hz = value; }
        }
        public char m_R800X600_at_75Hz;//	char	1
        public char R800X600_at_75Hz
        {
            get { return m_R800X600_at_75Hz; }
            set { m_R800X600_at_75Hz = value; }
        }
        public char m_R832X624_at_75Hz;//	char	1
        public char R832X624_at_75Hz
        {
            get { return m_R832X624_at_75Hz; }
            set { m_R832X624_at_75Hz = value; }
        }
        public char m_R1024X768_at_87Hz;//	char	1
        public char R1024X768_at_87Hz
        {
            get { return m_R1024X768_at_87Hz; }
            set { m_R1024X768_at_87Hz = value; }
        }
        public char m_R1024X768_at_60Hz;//	char	1
        public char R1024X768_at_60Hz
        {
            get { return m_R1024X768_at_60Hz; }
            set { m_R1024X768_at_60Hz = value; }
        }
        public char m_R1024X768_at_72Hz;//	char	1
        public char R1024X768_at_72Hz
        {
            get { return m_R1024X768_at_72Hz; }
            set { m_R1024X768_at_72Hz = value; }
        }
        public char m_R1024X768_at_75Hz;//	char	1
        public char R1024X768_at_75Hz
        {
            get { return m_R1024X768_at_75Hz; }
            set { m_R1024X768_at_75Hz = value; }
        }
        public char m_R1280X1024_at_75Hz;//	char	1
        public char R1280X1024_at_75Hz
        {
            get { return m_R1280X1024_at_75Hz; }
            set { m_R1280X1024_at_75Hz = value; }
        }
        public char m_R1152x870_at_75Hz;//	char	1
        public char R1152x870_at_75Hz
        {
            get { return m_R1152x870_at_75Hz; }
            set { m_R1152x870_at_75Hz = value; }
        }
        public UInt32 m_ST1_HorizontalActivePixels;//	int	4
        public UInt32 ST1_HorizontalActivePixels
        {
            get { return m_ST1_HorizontalActivePixels; }
            set { m_ST1_HorizontalActivePixels = value; }
        }
        public char m_ST1_ImageAspectRatio_16_10;//	char	1
        public char ST1_ImageAspectRatio_16_10
        {
            get { return m_ST1_ImageAspectRatio_16_10; }
            set { m_ST1_ImageAspectRatio_16_10 = value; }
        }
        public char m_ST1_ImageAspectRatio_4_3;//	char	1
        public char ST1_ImageAspectRatio_4_3
        {
            get { return m_ST1_ImageAspectRatio_4_3; }
            set { m_ST1_ImageAspectRatio_4_3 = value; }
        }
        public char m_ST1_ImageAspectRatio_5_4;//	char	1
        public char ST1_ImageAspectRatio_5_4
        {
            get { return m_ST1_ImageAspectRatio_5_4; }
            set { m_ST1_ImageAspectRatio_5_4 = value; }
        }
        public char m_ST1_ImageAspectRatio_16_9;//	char	1
        public char ST1_ImageAspectRatio_16_9
        {
            get { return m_ST1_ImageAspectRatio_16_9; }
            set { m_ST1_ImageAspectRatio_16_9 = value; }
        }
        public UInt32 m_ST1_ImageRefreshRate_Hz;//	int	4
        public UInt32 ST1_ImageRefreshRate_Hz
        {
            get { return m_ST1_ImageRefreshRate_Hz; }
            set { m_ST1_ImageRefreshRate_Hz = value; }
        }
        public UInt32 m_ST2_HorizontalActivePixels;//	int	4
        public UInt32 ST2_HorizontalActivePixels
        {
            get { return m_ST2_HorizontalActivePixels; }
            set { m_ST2_HorizontalActivePixels = value; }
        }
        public char m_ST2_ImageAspectRatio_16_10;//	char	1
        public char ST2_ImageAspectRatio_16_10
        {
            get { return m_ST2_ImageAspectRatio_16_10; }
            set { m_ST2_ImageAspectRatio_16_10 = value; }
        }
        public char m_ST2_ImageAspectRatio_4_3;//	char	1
        public char ST2_ImageAspectRatio_4_3
        {
            get { return m_ST2_ImageAspectRatio_4_3; }
            set { m_ST2_ImageAspectRatio_4_3 = value; }
        }
        public char m_ST2_ImageAspectRatio_5_4;//	char	1
        public char ST2_ImageAspectRatio_5_4
        {
            get { return m_ST2_ImageAspectRatio_5_4; }
            set { m_ST2_ImageAspectRatio_5_4 = value; }
        }
        public char m_ST2_ImageAspectRatio_16_9;//	char	1
        public char ST2_ImageAspectRatio_16_9
        {
            get { return m_ST2_ImageAspectRatio_16_9; }
            set { m_ST2_ImageAspectRatio_16_9 = value; }
        }
        public UInt32 m_ST2_ImageRefreshRate_Hz;//	int	4
        public UInt32 ST2_ImageRefreshRate_Hz
        {
            get { return m_ST2_ImageRefreshRate_Hz; }
            set { m_ST2_ImageRefreshRate_Hz = value; }
        }
        public UInt32 m_ST3_HorizontalActivePixels;//	int	4
        public UInt32 ST3_HorizontalActivePixels
        {
            get { return m_ST3_HorizontalActivePixels; }
            set { m_ST3_HorizontalActivePixels = value; }
        }
        public char m_ST3_ImageAspectRatio_16_10;//	char	1
        public char ST3_ImageAspectRatio_16_10
        {
            get { return m_ST3_ImageAspectRatio_16_10; }
            set { m_ST3_ImageAspectRatio_16_10 = value; }
        }
        public char m_ST3_ImageAspectRatio_4_3;//	char	1
        public char ST3_ImageAspectRatio_4_3
        {
            get { return m_ST3_ImageAspectRatio_4_3; }
            set { m_ST3_ImageAspectRatio_4_3 = value; }
        }
        public char m_ST3_ImageAspectRatio_5_4;//	char	1
        public char ST3_ImageAspectRatio_5_4
        {
            get { return m_ST3_ImageAspectRatio_5_4; }
            set { m_ST3_ImageAspectRatio_5_4 = value; }
        }

        public char m_ST3_ImageAspectRatio_16_9;//	char	1
        public char ST3_ImageAspectRatio_16_9
        {
            get { return m_ST3_ImageAspectRatio_16_9; }
            set { m_ST3_ImageAspectRatio_16_9 = value; }
        }

        public UInt32 m_ST3_ImageRefreshRate_Hz;//	int	4
        public UInt32 ST3_ImageRefreshRate_Hz
        {
            get { return m_ST3_ImageRefreshRate_Hz; }
            set { m_ST3_ImageRefreshRate_Hz = value; }
        }

        public UInt32 m_ST4_HorizontalActivePixels;//	int	4
        public UInt32 ST4_HorizontalActivePixels
        {
            get { return m_ST4_HorizontalActivePixels; }
            set { m_ST4_HorizontalActivePixels = value; }
        }

        public char m_ST4_ImageAspectRatio_16_10;//	char	1
        public char ST4_ImageAspectRatio_16_10
        {
            get { return m_ST4_ImageAspectRatio_16_10; }
            set { m_ST4_ImageAspectRatio_16_10 = value; }
        }

        public char m_ST4_ImageAspectRatio_4_3;//	char	1
        public char ST4_ImageAspectRatio_4_3
        {
            get { return m_ST4_ImageAspectRatio_4_3; }
            set { m_ST4_ImageAspectRatio_4_3 = value; }
        }

        public char m_ST4_ImageAspectRatio_5_4;//	char	1
        public char ST4_ImageAspectRatio_5_4
        {
            get { return m_ST4_ImageAspectRatio_5_4; }
            set { m_ST4_ImageAspectRatio_5_4 = value; }
        }

        public char m_ST4_ImageAspectRatio_16_9;//	char	1
        public char ST4_ImageAspectRatio_16_9
        {
            get { return m_ST4_ImageAspectRatio_16_9; }
            set { m_ST4_ImageAspectRatio_16_9 = value; }
        }

        public UInt32 m_ST4_ImageRefreshRate_Hz;//	int	4
        public UInt32 ST4_ImageRefreshRate_Hz
        {
            get { return m_ST4_ImageRefreshRate_Hz; }
            set { m_ST4_ImageRefreshRate_Hz = value; }
        }

        public UInt32 m_ST5_HorizontalActivePixels;//	int	4
        public UInt32 ST5_HorizontalActivePixels
        {
            get { return m_ST5_HorizontalActivePixels; }
            set { m_ST5_HorizontalActivePixels = value; }
        }

        public char m_ST5_ImageAspectRatio_16_10;//	char	1
        public char ST5_ImageAspectRatio_16_10
        {
            get { return m_ST5_ImageAspectRatio_16_10; }
            set { m_ST5_ImageAspectRatio_16_10 = value; }
        }

        public char m_ST5_ImageAspectRatio_4_3;//	char	1
        public char ST5_ImageAspectRatio_4_3
        {
            get { return m_ST5_ImageAspectRatio_4_3; }
            set { m_ST5_ImageAspectRatio_4_3 = value; }
        }

        public char m_ST5_ImageAspectRatio_5_4;//	char	1
        public char ST5_ImageAspectRatio_5_4
        {
            get { return m_ST5_ImageAspectRatio_5_4; }
            set { m_ST5_ImageAspectRatio_5_4 = value; }
        }

        public char m_ST5_ImageAspectRatio_16_9;//	char	1
        public char ST5_ImageAspectRatio_16_9
        {
            get { return m_ST5_ImageAspectRatio_16_9; }
            set { m_ST5_ImageAspectRatio_16_9 = value; }
        }

        public UInt32 m_ST5_ImageRefreshRate_Hz;//	int	4
        public UInt32 ST5_ImageRefreshRate_Hz
        {
            get { return m_ST5_ImageRefreshRate_Hz; }
            set { m_ST5_ImageRefreshRate_Hz = value; }
        }

        public UInt32 m_ST6_HorizontalActivePixels;//	int	4
        public UInt32 ST6_HorizontalActivePixels
        {
            get { return m_ST6_HorizontalActivePixels; }
            set { m_ST6_HorizontalActivePixels = value; }
        }

        public char m_ST6_ImageAspectRatio_16_10;//	char	1
        public char ST6_ImageAspectRatio_16_10
        {
            get { return m_ST6_ImageAspectRatio_16_10; }
            set { m_ST6_ImageAspectRatio_16_10 = value; }
        }

        public char m_ST6_ImageAspectRatio_4_3;//	char	1
        public char ST6_ImageAspectRatio_4_3
        {
            get { return m_ST6_ImageAspectRatio_4_3; }
            set { m_ST6_ImageAspectRatio_4_3 = value; }
        }

        public char m_ST6_ImageAspectRatio_5_4;//	char	1
        public char ST6_ImageAspectRatio_5_4
        {
            get { return m_ST6_ImageAspectRatio_5_4; }
            set { m_ST6_ImageAspectRatio_5_4 = value; }
        }

        public char m_ST6_ImageAspectRatio_16_9;//	char	1
        public char ST6_ImageAspectRatio_16_9
        {
            get { return m_ST6_ImageAspectRatio_16_9; }
            set { m_ST6_ImageAspectRatio_16_9 = value; }
        }

        public UInt32 m_ST6_ImageRefreshRate_Hz;//	int	4
        public UInt32 ST6_ImageRefreshRate_Hz
        {
            get { return m_ST6_ImageRefreshRate_Hz; }
            set { m_ST6_ImageRefreshRate_Hz = value; }
        }

        public UInt32 m_ST7_HorizontalActivePixels;//	int	4
        public UInt32 ST7_HorizontalActivePixels
        {
            get { return m_ST7_HorizontalActivePixels; }
            set { m_ST7_HorizontalActivePixels = value; }
        }


        public char m_ST7_ImageAspectRatio_16_10;//	char	1
        public char ST7_ImageAspectRatio_16_10
        {
            get { return m_ST7_ImageAspectRatio_16_10; }
            set { m_ST7_ImageAspectRatio_16_10 = value; }
        }

        public char m_ST7_ImageAspectRatio_4_3;//	char	1
        public char ST7_ImageAspectRatio_4_3
        {
            get { return m_ST7_ImageAspectRatio_4_3; }
            set { m_ST7_ImageAspectRatio_4_3 = value; }
        }

        public char m_ST7_ImageAspectRatio_5_4;//	char	1
        public char ST7_ImageAspectRatio_5_4
        {
            get { return m_ST7_ImageAspectRatio_5_4; }
            set { m_ST7_ImageAspectRatio_5_4 = value; }
        }

        public char m_ST7_ImageAspectRatio_16_9;//	char	1
        public char ST7_ImageAspectRatio_16_9
        {
            get { return m_ST7_ImageAspectRatio_16_9; }
            set { m_ST7_ImageAspectRatio_16_9 = value; }
        }

        public UInt32 m_ST7_ImageRefreshRate_Hz;//	int	4
        public UInt32 ST7_ImageRefreshRate_Hz
        {
            get { return m_ST7_ImageRefreshRate_Hz; }
            set { m_ST7_ImageRefreshRate_Hz = value; }
        }

        public UInt32 m_ST8_HorizontalActivePixels;//	int	4
        public UInt32 ST8_HorizontalActivePixels
        {
            get { return m_ST8_HorizontalActivePixels; }
            set { m_ST8_HorizontalActivePixels = value; }
        }

        public char m_ST8_ImageAspectRatio_16_10;//	public char	1
        public char ST8_ImageAspectRatio_16_10
        {
            get { return m_ST8_ImageAspectRatio_16_10; }
            set { m_ST8_ImageAspectRatio_16_10 = value; }
        }

        public char m_ST8_ImageAspectRatio_4_3;//	public char	1
        public char ST8_ImageAspectRatio_4_3
        {
            get { return m_ST8_ImageAspectRatio_4_3; }
            set { m_ST8_ImageAspectRatio_4_3 = value; }
        }

        public char m_ST8_ImageAspectRatio_5_4;//	public char	1
        public char ST8_ImageAspectRatio_5_4
        {
            get { return m_ST8_ImageAspectRatio_5_4; }
            set { m_ST8_ImageAspectRatio_5_4 = value; }
        }

        public char m_ST8_ImageAspectRatio_16_9;//	public char	1
        public char ST8_ImageAspectRatio_16_9
        {
            get { return m_ST8_ImageAspectRatio_16_9; }
            set { m_ST8_ImageAspectRatio_16_9 = value; }
        }

        public UInt32 m_ST8_ImageRefreshRate_Hz;//	int	4
        public UInt32 ST8_ImageRefreshRate_Hz
        {
            get { return m_ST8_ImageRefreshRate_Hz; }
            set { m_ST8_ImageRefreshRate_Hz = value; }
        }

        public decimal m_PixelClock_MHz;//	public decimal	5
        public decimal PixelClock_MHz
        {
            get { return m_PixelClock_MHz; }
            set { m_PixelClock_MHz = value; }
        }

        public UInt32 m_HorizontalAddressableVideoPixels;//	int	4
        public UInt32 HorizontalAddressableVideoPixels
        {
            get { return m_HorizontalAddressableVideoPixels; }
            set { m_HorizontalAddressableVideoPixels = value; }
        }

        public UInt32 m_HorizontalBlankingPixels;//	int	4
        public UInt32 HorizontalBlankingPixels
        {
            get { return m_HorizontalBlankingPixels; }
            set { m_HorizontalBlankingPixels = value; }
        }
        public UInt32 m_VerticalAddressableVideoinlines;//	int	4
        public UInt32 VerticalAddressableVideoinlines
        {
            get { return m_VerticalAddressableVideoinlines; }
            set { m_VerticalAddressableVideoinlines = value; }
        }
        public UInt32 m_VerticalBlankinginlines;//	int	4
        public UInt32 VerticalBlankinginlines
        {
            get { return m_VerticalBlankinginlines; }
            set { m_VerticalBlankinginlines = value; }
        }
        public UInt32 m_HorizontalFrontPorchpixels;//	int	4
        public UInt32 HorizontalFrontPorchpixels
        {
            get { return m_HorizontalFrontPorchpixels; }
            set { m_HorizontalFrontPorchpixels = value; }
        }
        public UInt32 m_HorizontalSyncPulseWidthPixels;//	int	4
        public UInt32 HorizontalSyncPulseWidthPixels
        {
            get { return m_HorizontalSyncPulseWidthPixels; }
            set { m_HorizontalSyncPulseWidthPixels = value; }
        }
        public UInt32 m_VerticalFrontPorchlines;//	int	4
        public UInt32 VerticalFrontPorchlines
        {
            get { return m_VerticalFrontPorchlines; }
            set { m_VerticalFrontPorchlines = value; }
        }

        public UInt32 m_VerticalSyncPulseWidthlines;//	int	4

        public UInt32 VerticalSyncPulseWidthlines
        {
            get { return m_VerticalSyncPulseWidthlines; }
            set { m_VerticalSyncPulseWidthlines = value; }
        }
        public UInt32 m_HorizontalImageSize;//	int	4
        public UInt32 HorizontalImageSize
        {
            get { return m_HorizontalImageSize; }
            set { m_HorizontalImageSize = value; }
        }
        public UInt32 m_VerticalImageSize;//	int	4
        public UInt32 VerticalImageSize
        {
            get { return m_VerticalImageSize; }
            set { m_VerticalImageSize = value; }
        }

        public UInt32 m_HorizontalBorderPixels;//	int	4
        public UInt32 HorizontalBorderPixels
        {
            get { return m_HorizontalBorderPixels; }
            set { m_HorizontalBorderPixels = value; }
        }

        public UInt32 m_VerticalBorderLines;//	int	4
        public UInt32 VerticalBorderLines
        {
            get { return m_VerticalBorderLines; }
            set { m_VerticalBorderLines = value; }
        }

        public char m_Interlaced;//	public char	1
        public char Interlaced
        {
            get { return m_Interlaced; }
            set { m_Interlaced = value; }
        }

        public char m_NormalDisplay_NoStereo;//	public char	1
        public char NormalDisplay_NoStereo
        {
            get { return m_NormalDisplay_NoStereo; }
            set { m_NormalDisplay_NoStereo = value; }
        }

        public char m_Fieldsequentialstereo_rightimage;//	public char	1
        public char Fieldsequentialstereo_rightimage
        {
            get { return m_Fieldsequentialstereo_rightimage; }
            set { m_Fieldsequentialstereo_rightimage = value; }
        }

        public char m_Fieldsequentialstereo_leftimage;//	public char	1
        public char Fieldsequentialstereo_leftimage
        {
            get { return m_Fieldsequentialstereo_leftimage; }
            set { m_Fieldsequentialstereo_leftimage = value; }
        }

        public char m_Twowayinterleavedstereo_rightimage;//	public char	1
        public char Twowayinterleavedstereo_rightimage
        {
            get { return m_Twowayinterleavedstereo_rightimage; }
            set { m_Twowayinterleavedstereo_rightimage = value; }
        }


        public char m_Twowayinterleavedstereo_leftimage;//	public char	1
        public char Twowayinterleavedstereo_leftimage
        {
            get { return m_Twowayinterleavedstereo_leftimage; }
            set { m_Twowayinterleavedstereo_leftimage = value; }
        }

        public char m_Fourwayinterleavedstereo;//	public char	1
        public char Fourwayinterleavedstereo
        {
            get { return m_Fourwayinterleavedstereo; }
            set { m_Fourwayinterleavedstereo = value; }
        }

        public char m_SidebySideinterleavedstereo;//	public char	1
        public char SidebySideinterleavedstereo
        {
            get { return m_SidebySideinterleavedstereo; }
            set { m_SidebySideinterleavedstereo = value; }
        }

        public char m_AnalogCompositeSync;//	public char	1
        public char AnalogCompositeSync
        {
            get { return m_AnalogCompositeSync; }
            set { m_AnalogCompositeSync = value; }
        }

        public char m_BipolarAnalogCompositeSync;//	public char	1
        public char BipolarAnalogCompositeSync
        {
            get { return m_BipolarAnalogCompositeSync; }
            set { m_BipolarAnalogCompositeSync = value; }
        }

        public char m_AnalogWithoutSerrations;//	public char	1
        public char AnalogWithoutSerrations
        {
            get { return m_AnalogWithoutSerrations; }
            set { m_AnalogWithoutSerrations = value; }
        }

        public char m_AnalogWithSerrations;//	public char	1
        public char AnalogWithSerrations
        {
            get { return m_AnalogWithSerrations; }
            set { m_AnalogWithSerrations = value; }
        }

        public char m_SyncOnGreenSignalonly;//	public char	1
        public char SyncOnGreenSignalonly
        {
            get { return m_SyncOnGreenSignalonly; }
            set { m_SyncOnGreenSignalonly = value; }
        }

        public char m_SyncOnallthreeRGBvideosignals;//	public char	1
        public char SyncOnallthreeRGBvideosignals
        {
            get { return m_SyncOnallthreeRGBvideosignals; }
            set { m_SyncOnallthreeRGBvideosignals = value; }
        }

        public char m_DigitalCompositeSync;//	public char	1
        public char DigitalCompositeSync
        {
            get { return m_DigitalCompositeSync; }
            set { m_DigitalCompositeSync = value; }
        }

        public char m_DigitalWithoutSerrations;//	public char	1
        public char DigitalWithoutSerrations
        {
            get { return m_DigitalWithoutSerrations; }
            set { m_DigitalWithoutSerrations = value; }
        }

        public char m_DigitalWithSerrations;//	public char	1
        public char DigitalWithSerrations
        {
            get { return m_DigitalWithSerrations; }
            set { m_DigitalWithSerrations = value; }
        }

        public char m_DigitalSeparateSync;//	public char	1
        public char DigitalSeparateSync
        {
            get { return m_DigitalSeparateSync; }
            set { m_DigitalSeparateSync = value; }
        }


        public char m_DigitalVerticalSyncPositive;//	public char	1
        public char DigitalVerticalSyncPositive
        {
            get { return m_DigitalVerticalSyncPositive; }
            set { m_DigitalVerticalSyncPositive = value; }
        }

        public char m_DigitalHorizontalSyncPositive;//	public char	1
        public char DigitalHorizontalSyncPositive
        {
            get { return m_DigitalHorizontalSyncPositive; }
            set { m_DigitalHorizontalSyncPositive = value; }
        }


        public string m_DisplayProductSerialNumberDescriptorTagNumber;//	varpublic char	15
        public string DisplayProductSerialNumberDescriptorTagNumber
        {
            get { return m_DisplayProductSerialNumberDescriptorTagNumber; }
            set { m_DisplayProductSerialNumberDescriptorTagNumber = value; }
        }

        public string m_StringDescriptorTagnumber;//	varpublic char	15
        public string StringDescriptorTagnumber
        {
            get { return m_StringDescriptorTagnumber; }
            set { m_StringDescriptorTagnumber = value; }
        }


        public char m_MinVerticalRate_Flag;//	public char	1
        public char MinVerticalRate_Flag
        {
            get { return m_MinVerticalRate_Flag; }
            set { m_MinVerticalRate_Flag = value; }
        }


        public char m_MaxVerticalRate_Flag;//	public char	1
        public char MaxVerticalRate_Flag
        {
            get { return m_MaxVerticalRate_Flag; }
            set { m_MaxVerticalRate_Flag = value; }
        }

        public char m_MinHorizontalRate_flag;//	public char	1
        public char MinHorizontalRate_flag
        {
            get { return m_MinHorizontalRate_flag; }
            set { m_MinHorizontalRate_flag = value; }
        }

        public char m_MaxHorizontalRate_flag;//	public char	1
        public char MaxHorizontalRate_flag
        {
            get { return m_MaxHorizontalRate_flag; }
            set { m_MaxHorizontalRate_flag = value; }
        }



        public UInt32 m_MinVerticalRate;//	int	4
        public UInt32 MinVerticalRate
        {
            get { return m_MinVerticalRate; }
            set { m_MinVerticalRate = value; }
        }



        public UInt32 m_MaxVerticalRate;//	int	4
        public UInt32 MaxVerticalRate
        {
            get { return m_MaxVerticalRate; }
            set { m_MaxVerticalRate = value; }
        }


        public UInt32 m_MinHorizontalRate;//	int	4
        public UInt32 MinHorizontalRate
        {
            get { return m_MinHorizontalRate; }
            set { m_MinHorizontalRate = value; }
        }


        public UInt32 m_MaxHorizontalRate;//	int	4
        public UInt32 MaxHorizontalRate
        {
            get { return m_MaxHorizontalRate; }
            set { m_MaxHorizontalRate = value; }
        }

        public UInt32 m_Maxpixelclock;//	int	4
        public UInt32 Maxpixelclock
        {
            get { return m_Maxpixelclock; }
            set { m_Maxpixelclock = value; }
        }

        public char m_DefaultGTFsupported;//	public char	1
        public char DefaultGTFsupported
        {
            get { return m_DefaultGTFsupported; }
            set { m_DefaultGTFsupported = value; }
        }

        public char m_RangeLimits;//	public char	1
        public char RangeLimits
        {
            get { return m_RangeLimits; }
            set { m_RangeLimits = value; }
        }

        public char m_SecondaryGTFsupported;//	public char	1
        public char SecondaryGTFsupported
        {
            get { return m_SecondaryGTFsupported; }
            set { m_SecondaryGTFsupported = value; }
        }

        public char m_LineFeed;//	public char	1
        public char LineFeed
        {
            get { return m_LineFeed; }
            set { m_LineFeed = value; }
        }

        public char m_GTFSecondaryCurvesupported;//	public char	1
        public char GTFSecondaryCurvesupported
        {
            get { return m_GTFSecondaryCurvesupported; }
            set { m_GTFSecondaryCurvesupported = value; }
        }
        public UInt32 m_Startbreakfrequency;//	int	4
        public UInt32 Startbreakfrequency
        {
            get { return m_Startbreakfrequency; }
            set { m_Startbreakfrequency = value; }
        }
        public UInt32 m_CX2;//	int	4
        public UInt32 CX2
        {
            get { return m_CX2; }
            set { m_CX2 = value; }
        }
        public UInt32 m_M;//	int	4
        public UInt32 M
        {
            get { return m_M; }
            set { m_M = value; }
        }
        public UInt32 m_K;//	int	4
        public UInt32 K
        {
            get { return m_K; }
            set { m_K = value; }
        }
        public UInt32 m_J;//	int	4
        public UInt32 J
        {
            get { return m_J; }
            set { m_J = value; }
        }
        public char m_CVTsupported;//	public char	1 
        public char CVTsupported
        {
            get { return m_CVTsupported; }
            set { m_CVTsupported = value; }
        }
        public string m_CVTStandardVersionNumber;//	varpublic char	20
        public string CVTStandardVersionNumber
        {
            get { return m_CVTStandardVersionNumber; }
            set { m_CVTStandardVersionNumber = value; }
        }
        public UInt32 m_AdditionalPixelClockPrecision;//	int	4
        public UInt32 AdditionalPixelClockPrecision
        {
            get { return m_AdditionalPixelClockPrecision; }
            set { m_AdditionalPixelClockPrecision = value; }
        }
        public UInt32 m_MaximumActivePixelsperLine_MSB;//	int	4
        public UInt32 MaximumActivePixelsperLine_MSB
        {
            get { return m_MaximumActivePixelsperLine_MSB; }
            set { m_MaximumActivePixelsperLine_MSB = value; }
        }
        public UInt32 m_MaximumActivePixelsperLine_LSB;//	int	4
        public UInt32 MaximumActivePixelsperLine_LSB
        {
            get { return m_MaximumActivePixelsperLine_LSB; }
            set { m_MaximumActivePixelsperLine_LSB = value; }
        }
        public char m_AR_4_3SupportedAspectRatio;//	public char	1
        public char AR_4_3SupportedAspectRatio
        {
            get { return m_AR_4_3SupportedAspectRatio; }
            set { m_AR_4_3SupportedAspectRatio = value; }
        }
        public char m_AR_16_9SupportedAspectRatio;//	public char	1
        public char AR_16_9SupportedAspectRatio
        {
            get { return m_AR_16_9SupportedAspectRatio; }
            set { m_AR_16_9SupportedAspectRatio = value; }
        }
        public char m_AR_16_10SupportedAspectRatio;//	public char	1
        public char AR_16_10SupportedAspectRatio
        {
            get { return m_AR_16_10SupportedAspectRatio; }
            set { m_AR_16_10SupportedAspectRatio = value; }
        }
        public char m_AR_5_4SupportedAspectRatio;//	public char	1
        public char AR_5_4SupportedAspectRatio
        {
            get { return m_AR_5_4SupportedAspectRatio; }
            set { m_AR_5_4SupportedAspectRatio = value; }
        }
        public char m_AR_15_9SupportedAspectRatio;//	public char	1
        public char AR_15_9SupportedAspectRatio
        {
            get { return m_AR_15_9SupportedAspectRatio; }
            set { m_AR_15_9SupportedAspectRatio = value; }
        }
        public char m_AR_4_3PreferedAspectRatio;//	public char	1
        public char AR_4_3PreferedAspectRatio
        {
            get { return m_AR_4_3PreferedAspectRatio; }
            set { m_AR_4_3PreferedAspectRatio = value; }
        }
        public char m_AR_16_9PreferedAspectRatio;//	public char	1
        public char AR_16_9PreferedAspectRatio
        {
            get { return m_AR_16_9PreferedAspectRatio; }
            set { m_AR_16_9PreferedAspectRatio = value; }
        }
        public char m_AR_16_10PreferedAspectRatio;//	public char	1
        public char AR_16_10PreferedAspectRatio
        {
            get { return m_AR_16_10PreferedAspectRatio; }
            set { m_AR_16_10PreferedAspectRatio = value; }
        }
        public char m_AR_5_4PreferedAspectRatio;//	public char	1
        public char AR_5_4PreferedAspectRatio
        {
            get { return m_AR_5_4PreferedAspectRatio; }
            set { m_AR_5_4PreferedAspectRatio = value; }
        }
        public string m_AR_15_9PreferedAspectRatio;//	varpublic char	3
        public string AR_15_9PreferedAspectRatio
        {
            get { return m_AR_15_9PreferedAspectRatio; }
            set { m_AR_15_9PreferedAspectRatio = value; }
        }
        public char m_StandardCVTBlankingSupport;//	public char	1
        public char StandardCVTBlankingSupport
        {
            get { return m_StandardCVTBlankingSupport; }
            set { m_StandardCVTBlankingSupport = value; }
        }
        public char m_ReducedCVTBlankingSupport;//	public char	1
        public char ReducedCVTBlankingSupport
        {
            get { return m_ReducedCVTBlankingSupport; }
            set { m_ReducedCVTBlankingSupport = value; }
        }
        public char m_HorizontalShrinkSupport;//	public char	1
        public char HorizontalShrinkSupport
        {
            get { return m_HorizontalShrinkSupport; }
            set { m_HorizontalShrinkSupport = value; }
        }
        public char m_HorizontalStretch;//	public char	1
        public char HorizontalStretch
        {
            get { return m_HorizontalStretch; }
            set { m_HorizontalStretch = value; }
        }
        public char m_VerticalShrink;//	public char	1
        public char VerticalShrink
        {
            get { return m_VerticalShrink; }
            set { m_VerticalShrink = value; }
        }
        public char m_VerticalStretch;//	public char	1
        public char VerticalStretch
        {
            get { return m_VerticalStretch; }
            set { m_VerticalStretch = value; }
        }
        public UInt32 m_PreferredVerticalRefreshRate;//	int	4
        public UInt32 PreferredVerticalRefreshRate
        {
            get { return m_PreferredVerticalRefreshRate; }
            set { m_PreferredVerticalRefreshRate = value; }
        }
        public string m_ColorPointDescriptorTagNumber;//	varpublic char	3
        public string ColorPointDescriptorTagNumber
        {
            get { return m_ColorPointDescriptorTagNumber; }
            set { m_ColorPointDescriptorTagNumber = value; }
        }
        public UInt32 m_WhitePointIndexNumber;//	int	4
        public UInt32 WhitePointIndexNumber
        {
            get { return m_WhitePointIndexNumber; }
            set { m_WhitePointIndexNumber = value; }
        }
        public decimal m_White_xy;//	public decimal	5
        public decimal White_xy
        {
            get { return m_White_xy; }
            set { m_White_xy = value; }
        }
        public decimal m_White_x;//	public decimal	5
        public decimal White_x
        {
            get { return m_White_x; }
            set { m_White_x = value; }
        }
        public decimal m_White_y;//	public decimal	5
        public decimal White_y
        {
            get { return m_White_y; }
            set { m_White_y = value; }
        }
        public decimal m_ColorPointDescriptorValueStored;//	public decimal	5
        public decimal ColorPointDescriptorValueStored
        {
            get { return m_ColorPointDescriptorValueStored; }
            set { m_ColorPointDescriptorValueStored = value; }
        }
        public string m_MonitorName;
        public String MonitorName
        {
            get { return m_MonitorName; }
            set { m_MonitorName = value; }
        }
        private String m_PhysicalAddress = String.Empty;
        [ReadOnly(true)]
        [Description("CEC Physical Address")]
        [Category("Vendor Specific Data Block")]
        [DisplayName("CEC Physical Address")]
        public String PhysicalAddress
        {
            get { return m_PhysicalAddress; }
            set { m_PhysicalAddress = value; }
        }
    }
}
