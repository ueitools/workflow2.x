﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace UEI.EDID
{
    public class BrandTranslationData
    {
        private String m_MainDevice = String.Empty;
        public String MainDevice
        {
            get { return m_MainDevice; }
            set { m_MainDevice = value; }
        }
        private String m_SubDevice = String.Empty;
        public String SubDevice
        {
            get { return m_SubDevice; }
            set { m_SubDevice = value; }
        }
        private String m_Brand = String.Empty;
        public String Brand
        {
            get { return m_Brand; }
            set { m_Brand = value; }
        }
        private String m_ManufacturerCode = String.Empty;
        public String ManufacturerCode
        {
            get { return m_ManufacturerCode; }
            set { m_ManufacturerCode = value; }
        }
    }
    public class BrandTranslationCollection:Collection<BrandTranslationData>
    {
        public BrandTranslationData GetBrand(String strMainDevice, String strManufacturerCode)
        {            
            if (this.Items != null)
            {
                foreach (BrandTranslationData item in this.Items)
                {
                    if ((item.MainDevice.Trim().ToLower() == strMainDevice.Trim().ToLower()) && ((item.ManufacturerCode.Trim().ToLower() == strManufacturerCode.Trim().ToLower())))
                    {
                        return item;                       
                    }
                }
            }
            return null;
        }
        public BrandTranslationData GetBrand(String strManufacturerCode)
        {
            if (this.Items != null)
            {
                foreach (BrandTranslationData item in this.Items)
                {
                    if (item.ManufacturerCode.Trim().ToLower() == strManufacturerCode.Trim().ToLower())
                    {
                        return item;
                    }
                }
            }
            return null;
        }
    }
}
