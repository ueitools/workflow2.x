using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;

namespace IDView
{
    public partial class IDHeaderView : Form
    {
        private int LastLinePrinted;
        public void resetLastLinePrinted()
        {
            LastLinePrinted = 0;
        }
        public IDHeaderView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Set IDHeaderData to the GridView DataSource for display
        /// The logData is part of the HeaderData so the database call is done at a higher level
        /// and passed to the Header view but the Log view gets only the LogData
        /// </summary>
        /// <param name="IDHeaderData"></param>
        public void LoadID(IDHeader IDHeaderData)
        {
            LastLinePrinted = 0;
            iDHeaderBindingSource.DataSource = IDHeaderData;
            prefixDataGridView.DataSource = IDHeaderData.PrefixList;

            StatusQ.Checked = (IDHeaderData.Status == "Q")?true:false;
            StatusM.Checked = (IDHeaderData.Status == "M")?true:false;
            StatusE.Checked = (IDHeaderData.Status == "E")?true:false;
            StatusP.Checked = (IDHeaderData.Status == "P")?true:false;
        }
        /// <summary>
        /// format a line of text for a header line;
        /// </summary>
        /// <returns></returns>
        public string GetReportHeader()
        {
            return string.Format("Header Section");
        }
        /// <summary>
        /// Format the specified row of data from the table for printing.
        /// The data is returned in the order it was last viewed. 
        /// if the data sort order was changed the data will be returned in that order.
        /// </summary>
        /// <param name="row">the zero based row of data requested.</param>
        /// <returns>a string sutible for printing</returns>
        public string GetReportLine()
        {
            int row = LastLinePrinted++;
            string data = null;
            IDHeader Header = (IDHeader)iDHeaderBindingSource.DataSource;

            switch (row)
            {
                case 0: 
                    data = string.Format("Executor: {0}             Is Inversed Prefix: {1}", 
                                        Header.Executor_Code, Header.IsInversedPrefix);
                    break;
                case 1:
                    data = string.Format("Is External Prefix: {0}   Is Restricted: {1}",
                                        Header.IsExternalPrefix, Header.IsRestricted);
                    break;
                case 2:
                    data = string.Format("Is Frequency Data: {0}    Parent ID: {1}",
                                        Header.IsFrequencyData, Header.Parent_ID);
                    break;
                case 3:
                    data = string.Format("Is Hex Format: {0}        Status: {1}",
                                        Header.IsFrequencyData, Header.Status);
                    break;
                case 4:
                    data = string.Format("Is Inversd Data: {0}      Text: {1}",
                                        Header.IsInversedData, Header.Text);
                    break;
                default:
                    row -= 5;
                    if (row < Header.PrefixList.Count)
                    {
                        string d = Header.PrefixList[row].Data;
                        string t = Header.PrefixList[row].Description;
                        data = string.Format("{0,-36} {1}", d, t);
                    }
                    break;
            }
            return data;
        }
    }
}