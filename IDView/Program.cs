using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace IDView
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            int TN = 0;
            string ID = "";

            if (args.Length > 0)
            {
                GetArg(args[0], ref TN, ref ID);
                if (args.Length > 1)
                    GetArg(args[1], ref TN, ref ID);
            }

            Application.EnableVisualStyles();
            Application.Run(new IDFormView(ID, TN));
        }

        private static void GetArg(string arg, ref int TN, ref string ID)
        {
            if (char.IsNumber(arg[0]))
                TN = int.Parse(arg);
            else
                ID = arg.ToString();
        }
    }
}