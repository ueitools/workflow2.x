using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using UEI.Workflow2010.Report.Service;
namespace IDView
{
    public partial class IDBrandView : Form
    {
        #region Variables
        private int LastLinePrinted;
        private TreeNode m_SelectedTreeNode = null;
        #endregion

        #region Constrcutor
        public IDBrandView()
        {
            InitializeComponent();
            treeLocations.AfterSelect += new TreeViewEventHandler(treeLocations_AfterSelect);
            treeLocations.BeforeSelect += new TreeViewCancelEventHandler(treeLocations_BeforeSelect);
            treeDeviceTypes.AfterSelect += new TreeViewEventHandler(treeDeviceTypes_AfterSelect);
            treeDeviceTypes.BeforeSelect += new TreeViewCancelEventHandler(treeDeviceTypes_BeforeSelect);
        }
        void treeDeviceTypes_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Action == TreeViewAction.Unknown)
                e.Cancel = true;
        }
        void treeLocations_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Action == TreeViewAction.Unknown)
                e.Cancel = true;
        }
        #endregion

        #region ID Selected
        public void LoadID(String SelectedID)
        {
            m_ShowUnknownLocationNode = false;
            m_ShowIDBasedDevicesNode = false;
            m_SelectedTreeNode = null;
            LastLinePrinted = 0;
            DeviceCollection IDDevices = DAOFactory.Device().SelectID(SelectedID);
            ShowUnknownLocationandIDBasedDevice(IDDevices);
            deviceCollectionDataGridView.DataSource = IDDevices;
            FindDistinctRegions(IDDevices);
            FindDistinctDeviceTypes(IDDevices);            
        }
        #endregion

        #region Unknown Location and IDBased Devices Node Display
        private Boolean m_ShowUnknownLocationNode = false;
        private Boolean m_ShowIDBasedDevicesNode = false;
        private void ShowUnknownLocationandIDBasedDevice(DeviceCollection IDDevices)
        {
            if (IDDevices != null)
            {
                foreach (Device m_Item in IDDevices)
                {
                    if (m_Item.DeviceTypeList.Count == 0)
                        m_ShowIDBasedDevicesNode = true;
                    if (m_Item.CountryList.Count == 0)
                        m_ShowUnknownLocationNode = true;
                }
            }
        }
        #endregion

        #region Location Selection
        private void FindDistinctRegions(DeviceCollection IDDevices)
        {
            //Regionlist.Items.Clear();
            if (treeLocations.Nodes != null)
                treeLocations.Nodes.Clear();
            //Get all Regions
            List<UEI.Workflow2010.Report.Model.Region> m_Regions = new List<UEI.Workflow2010.Report.Model.Region>();
            foreach (Device device in IDDevices)
            {                
                for (Int32 Countries = 0; Countries < device.CountryList.Count; Countries++)
                {
                    String regionName = device.CountryList[Countries].Region.ToString();
                    if (regionName != "")
                    {
                        UEI.Workflow2010.Report.Model.Region region = GetRegion(regionName, m_Regions);
                        if (region == null)
                        {
                            region = new UEI.Workflow2010.Report.Model.Region(regionName, regionName);
                            m_Regions.Add(region);
                        }
                        String subregionName = device.CountryList[Countries].SubRegion.ToString();
                        if (subregionName != "" && !isSubRegionExists(regionName, subregionName, m_Regions))
                        {
                            if (region.SubRegions == null)
                                region.SubRegions = new List<UEI.Workflow2010.Report.Model.Region>();
                            region.SubRegions.Add(new UEI.Workflow2010.Report.Model.Region(subregionName, subregionName));
                        }
                        String countryName = device.CountryList[Countries].Name.ToString();
                        if (countryName != "" && !isCountryExists(regionName, countryName, subregionName, m_Regions))
                        {
                            if (region.Countries == null)
                                region.Countries = new List<UEI.Workflow2010.Report.Model.Country>();
                            region.Countries.Add(new UEI.Workflow2010.Report.Model.Country(countryName, countryName,subregionName,subregionName));
                        }                        
                    }
                }
            }
            if (m_Regions.Count > 0)
            {                      
                UEI.Workflow2010.Report.Model.RegionComparer m_RegionComprer = new UEI.Workflow2010.Report.Model.RegionComparer();
                m_Regions.Sort(m_RegionComprer);
                foreach (UEI.Workflow2010.Report.Model.Region region in m_Regions)
                {
                    treeLocations.Nodes.Add(region.Name, region.Name);
                    treeLocations.Nodes[region.Name].Tag = "Region";
                    if (region.SubRegions != null)
                    {
                        region.SubRegions.Sort(m_RegionComprer);          
                        foreach (UEI.Workflow2010.Report.Model.Region subregion in region.SubRegions)
                        {
                            treeLocations.Nodes[region.Name].Nodes.Add(subregion.Name, subregion.Name);
                            treeLocations.Nodes[region.Name].Nodes[subregion.Name].Tag = "SubRegion";
                        }
                    }
                }                
            }
            if (this.deviceCollectionDataGridView.Rows != null && m_ShowUnknownLocationNode == true)
            {
                treeLocations.Nodes.Add("Unknown Locations", "Unknown Locations");
                treeLocations.Nodes["Unknown Locations"].Tag = "Region";
            }
        }
        private UEI.Workflow2010.Report.Model.Region GetRegion(String regionName, List<UEI.Workflow2010.Report.Model.Region> _regions)
        {
            UEI.Workflow2010.Report.Model.Region m_Region = null;
            if (_regions.Count > 0)
            {
                foreach (UEI.Workflow2010.Report.Model.Region item in _regions)
                {
                    if (item.Name == regionName)
                    {
                        return item;
                    }
                }
            }
            return m_Region;
        }
        private Boolean isCountryExists(String regionName, String countryName, String subregionName, List<UEI.Workflow2010.Report.Model.Region> _regions)
        {
            Boolean isExists = false;
            if (_regions.Count > 0)
            {
                foreach (UEI.Workflow2010.Report.Model.Region item in _regions)
                {
                    if (item.Name == regionName)
                    {
                        if (item.Countries != null)
                        {
                            foreach (UEI.Workflow2010.Report.Model.Country item1 in item.Countries)
                            {
                                if (item1.Name == countryName && item1.SubRegion == subregionName)
                                {
                                    isExists = true;
                                    break;
                                }
                            }
                        }                        
                    }
                }
            }
            return isExists;
        }
        private Boolean isSubRegionExists(String regionName, String subregionName, List<UEI.Workflow2010.Report.Model.Region> _regions)
        {
            Boolean isExists = false;
            if (_regions.Count > 0)
            {
                foreach (UEI.Workflow2010.Report.Model.Region item in _regions)
                {
                    if (item.Name == regionName)
                    {
                        if (item.SubRegions != null)
                        {
                            foreach (UEI.Workflow2010.Report.Model.Region item1 in item.SubRegions)
                            {
                                if (item1.Name == subregionName)
                                {
                                    isExists = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return isExists;
        }
        private void treeLocations_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {                                
                TreeNode node = ((TreeNode)e.Node);
                if (node != null)
                {
                    treeDeviceTypes.SelectedNode = null;
                    m_SelectedTreeNode = node;
                    BindDataGridView();                                    
                }
            }
        }
        #endregion

        #region Device Selection
        private void FindDistinctDeviceTypes(DeviceCollection IDDevices)
        {
            if (treeDeviceTypes.Nodes != null)
                treeDeviceTypes.Nodes.Clear();
            List<UEI.Workflow2010.Report.Model.DeviceType> m_DeviceTypes = new List<UEI.Workflow2010.Report.Model.DeviceType>();
            //Get all Main Devices       
            foreach (Device device in IDDevices)
            {
                for (Int32 Devices = 0; Devices < device.DeviceTypeList.Count; Devices++)
                {
                    String maindeviceName = device.DeviceTypeList[Devices].MainDevice.ToString();                    
                    if (maindeviceName != "")
                    {
                        UEI.Workflow2010.Report.Model.DeviceType devicetype = GetMainDevice(maindeviceName, m_DeviceTypes);
                        if (devicetype == null)
                        {
                            devicetype = new UEI.Workflow2010.Report.Model.DeviceType(maindeviceName, maindeviceName);
                            m_DeviceTypes.Add(devicetype);
                        }                        
                        //Get Sub Devices
                        String subdeviceName = device.DeviceTypeList[Devices].Name.ToString();
                        if (subdeviceName != "" && !isSubDeviceExists(maindeviceName,subdeviceName, m_DeviceTypes))
                        {
                            //Get Sub Devices                                                        
                            if (devicetype.SubDevices == null)
                                devicetype.SubDevices = new List<UEI.Workflow2010.Report.Model.DeviceType>();
                            devicetype.SubDevices.Add(new UEI.Workflow2010.Report.Model.DeviceType(subdeviceName, subdeviceName));
                        }
                        //Get Components
                        String componentName = device.DeviceTypeList[Devices].Component.ToString();
                        if (componentName != "" && !isComponentExists(maindeviceName, componentName, m_DeviceTypes))
                        {
                            if (devicetype.Components == null)
                                devicetype.Components = new List<UEI.Workflow2010.Report.Model.Component>();
                            devicetype.Components.Add(new UEI.Workflow2010.Report.Model.Component(componentName,componentName, subdeviceName, subdeviceName));
                        }                        
                    }                    
                }
            }
            if (m_DeviceTypes.Count > 0)
            {                
                UEI.Workflow2010.Report.Model.DeviceTypeComparer m_DeviceTypeComparer = new UEI.Workflow2010.Report.Model.DeviceTypeComparer();
                m_DeviceTypes.Sort(m_DeviceTypeComparer);
                foreach (UEI.Workflow2010.Report.Model.DeviceType maindevice in m_DeviceTypes)
                {
                    treeDeviceTypes.Nodes.Add(maindevice.Name, maindevice.Name);
                    treeDeviceTypes.Nodes[maindevice.Name].Tag = "MainDevice";
                    if (maindevice.SubDevices != null)
                    {
                        maindevice.SubDevices.Sort(m_DeviceTypeComparer);
                        foreach (UEI.Workflow2010.Report.Model.DeviceType subdevice in maindevice.SubDevices)
                        {
                            treeDeviceTypes.Nodes[maindevice.Name].Nodes.Add(subdevice.Name, subdevice.Name);
                            treeDeviceTypes.Nodes[maindevice.Name].Nodes[subdevice.Name].Tag = "SubDevice";
                        }
                    }
                }                
            }
            if (this.deviceCollectionDataGridView.Rows != null && m_ShowIDBasedDevicesNode == true)
            {
                treeDeviceTypes.Nodes.Add("IDBased_Devices", "IDBased_Devices");
                treeDeviceTypes.Nodes["IDBased_Devices"].Tag = "MainDevice";
            }
        }        
        private void treeDeviceTypes_AfterSelect(object sender, TreeViewEventArgs e)
        {            
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeNode node = ((TreeNode)e.Node);
                if (node != null)
                {
                    treeLocations.SelectedNode = null;
                    m_SelectedTreeNode = node;
                    BindDataGridView();
                }
            }
        }
        private UEI.Workflow2010.Report.Model.DeviceType GetMainDevice(String maindeviceName, List<UEI.Workflow2010.Report.Model.DeviceType> _devices)
        {
            UEI.Workflow2010.Report.Model.DeviceType m_devicetype = null;
            if (_devices.Count > 0)
            {
                foreach (UEI.Workflow2010.Report.Model.DeviceType item in _devices)
                {
                    if (item.Name == maindeviceName)
                        return item;
                }
            }           
            return m_devicetype;
        }        
        private Boolean isSubDeviceExists(String maindeviceName,String subdeviceName, List<UEI.Workflow2010.Report.Model.DeviceType> _devices)
        {
            Boolean isExists = false;
            if (_devices != null)
            {
                foreach (UEI.Workflow2010.Report.Model.DeviceType mainDevice in _devices)
                {
                    if (mainDevice.Name == maindeviceName)
                    {
                        if (mainDevice.SubDevices != null)
                        {
                            foreach (UEI.Workflow2010.Report.Model.DeviceType subDevice in mainDevice.SubDevices)
                            {
                                if (subDevice.Name == subdeviceName)
                                {
                                    isExists = true;
                                    break;
                                }
                            }
                        }                        
                    }
                }
            }
            return isExists;
        }
        private Boolean isComponentExists(String maindeviceName, String componentName, List<UEI.Workflow2010.Report.Model.DeviceType> _devices)
        {
            Boolean isExists = false;
            if (_devices != null)
            {
                foreach (UEI.Workflow2010.Report.Model.DeviceType mainDevice in _devices)
                {
                    if (mainDevice.Name == maindeviceName)
                    {
                        if (mainDevice.Components != null)
                        {
                            foreach (UEI.Workflow2010.Report.Model.Component component in mainDevice.Components)
                            {
                                if (component.Name == componentName)
                                {
                                    isExists = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return isExists;
        }
        #endregion

        #region Formatting Report Header
        public void resetLastLinePrinted()
        {
            LastLinePrinted = 0;
        }        
        public String GetReportLine()
        {
            String data = null;
            String isTarget = "";
            int row = LastLinePrinted++;
            if (row >= 0 && row < deviceCollectionDataGridView.Rows.Count)
            {
                Device IDDeviceData = (Device)deviceCollectionDataGridView.Rows[row].DataBoundItem;
                if (IDDeviceData.IsTarget == "Y")
                    isTarget = "T";
                else
                    isTarget = "R";

                data = String.Format("TN{0,-06:D}  {1,-20} {2,-25}  {3,-5}  {4,-6}  {5,-6}  {6,-8}  {7,-5}",
                    IDDeviceData.TN, IDDeviceData.Brand,
                    IDDeviceData.Model, IDDeviceData.Branch,
                    IDDeviceData.IsRetail, isTarget,
                    IDDeviceData.IsCodebook, IDDeviceData.IsPartNumber);
            }
            if (data == null)
                LastLinePrinted = 0;
            return data;
        }
        public String GetReportHeader()
        {
            String head = String.Format("{0,-08:D}  {1,-20} {2,-25}  {3,-5} {4,-6}  {5,-6}  {6,-8}  {7,-5}", "TN", "Brand",
                                         "Model", "Branch", "Retail", "Target", "Codebook", "Part#") + "\r\n";
            return head;
        }
        #endregion

        #region Bind DataGridView
        private void BindDataGridView()
        {
            if (m_SelectedTreeNode != null)
            {
                String SelectedItem = m_SelectedTreeNode.Text;
                Device IDDeviceRow = null;

                #region Main Device Selected
                if (m_SelectedTreeNode.Tag.ToString() == "MainDevice")
                {                    
                    foreach (DataGridViewRow row in deviceCollectionDataGridView.Rows)
                    {
                        row.DefaultCellStyle.BackColor = Color.White;
                        IDDeviceRow = (Device)row.DataBoundItem;
                        if (SelectedItem == "IDBased_Devices" && IDDeviceRow.DeviceTypeList.Count == 0)
                        {
                            row.DefaultCellStyle.BackColor = Color.Yellow;
                        }
                        for (int Dev = 0; Dev < IDDeviceRow.DeviceTypeList.Count; Dev++)
                        {
                            String DevName              = String.Empty;
                            String m_ComponentName      = String.Empty;
                            String m_ComponentNameTemp  = String.Empty;
                            DevName                     = IDDeviceRow.DeviceTypeList[Dev].MainDevice.ToString();
                            m_ComponentName             = IDDeviceRow.DeviceTypeList[Dev].Component.ToString();
                            m_ComponentNameTemp         = IDDeviceRow.Component.ToString();

                            if (DevName == SelectedItem && m_ComponentNameTemp == m_ComponentName)
                                row.DefaultCellStyle.BackColor = Color.LightGreen;
                        }
                    }
                }
                #endregion

                #region Sub Devices Selected
                if (m_SelectedTreeNode.Tag.ToString() == "SubDevice")
                {                    
                    foreach (DataGridViewRow row in deviceCollectionDataGridView.Rows)
                    {
                        row.DefaultCellStyle.BackColor = Color.White;
                        IDDeviceRow = (Device)row.DataBoundItem;
                        for (int Dev = 0; Dev < IDDeviceRow.DeviceTypeList.Count; Dev++)
                        {
                            String DevName              = String.Empty;
                            String SubDevName           = String.Empty;
                            String m_ComponentName      = String.Empty;
                            String m_ComponentNameTemp  = String.Empty;
                            DevName                     = IDDeviceRow.DeviceTypeList[Dev].MainDevice.ToString();
                            SubDevName                  = IDDeviceRow.DeviceTypeList[Dev].Name.ToString();
                            m_ComponentName             = IDDeviceRow.DeviceTypeList[Dev].Component.ToString();
                            m_ComponentNameTemp         = IDDeviceRow.Component.ToString();                        
    
                            if (SubDevName == SelectedItem && DevName == m_SelectedTreeNode.Parent.Text && m_ComponentNameTemp == m_ComponentName)
                                row.DefaultCellStyle.BackColor = Color.LightGreen;
                        }
                    }
                }
                #endregion                

                #region Region Selected
                if (m_SelectedTreeNode.Tag.ToString() == "Region")
                {
                    foreach (DataGridViewRow row in deviceCollectionDataGridView.Rows)
                    {
                        row.DefaultCellStyle.BackColor = Color.White;
                        IDDeviceRow = (Device)row.DataBoundItem;
                        if (SelectedItem == "Unknown Locations" && IDDeviceRow.CountryList.Count == 0)
                        {
                            row.DefaultCellStyle.BackColor = Color.Red;
                        }
                        else
                        {
                            for (int Countries = 0; Countries < IDDeviceRow.CountryList.Count; Countries++)
                            {
                                String m_region = String.Empty;
                                String m_country = String.Empty;
                                String m_countrytemp = String.Empty;
                                m_region = IDDeviceRow.CountryList[Countries].Region.ToString();
                                m_country = IDDeviceRow.CountryList[Countries].Name.ToString();
                                m_countrytemp = IDDeviceRow.Country.ToString();
                                if (m_region == SelectedItem && m_countrytemp == m_country)
                                    row.DefaultCellStyle.BackColor = Color.LightGreen;
                            }
                        }
                    }
                }
                #endregion

                #region Sub Region Selected
                if (m_SelectedTreeNode.Tag.ToString() == "SubRegion")
                {
                    foreach (DataGridViewRow row in deviceCollectionDataGridView.Rows)
                    {
                        row.DefaultCellStyle.BackColor = Color.White;
                        IDDeviceRow = (Device)row.DataBoundItem;
                        for (int Countries = 0; Countries < IDDeviceRow.CountryList.Count; Countries++)
                        {
                            String m_region         = String.Empty;
                            String m_subregion      = String.Empty;
                            String m_country        = String.Empty;
                            String m_countrytemp    = String.Empty;
                            m_region                = IDDeviceRow.CountryList[Countries].Region.ToString();
                            m_subregion             = IDDeviceRow.CountryList[Countries].SubRegion.ToString();                                                        
                            m_country               = IDDeviceRow.CountryList[Countries].Name.ToString();
                            m_countrytemp           = IDDeviceRow.Country.ToString();                                                        
                            if (m_region == m_SelectedTreeNode.Parent.Text && m_subregion == SelectedItem && m_countrytemp == m_country)
                                row.DefaultCellStyle.BackColor = Color.LightGreen;
                        }
                    }
                }
                #endregion                
            }
        }
        #endregion

        #region Data Grid View Sorted
        private void deviceCollectionDataGridView_Sorted(object sender, EventArgs e)
        {
            BindDataGridView();
        }
        #endregion
    }
}