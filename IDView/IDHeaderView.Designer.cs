namespace IDView
{
    partial class IDHeaderView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label iDLabel1;
            this.executor_CodeLabel = new System.Windows.Forms.Label();
            this.isExternalPrefixLabel = new System.Windows.Forms.Label();
            this.isFrequencyDataLabel = new System.Windows.Forms.Label();
            this.isHexFormatLabel = new System.Windows.Forms.Label();
            this.isInversedDataLabel = new System.Windows.Forms.Label();
            this.isInversedPrefixLabel = new System.Windows.Forms.Label();
            this.isRestrictedLabel = new System.Windows.Forms.Label();
            this.parent_IDLabel = new System.Windows.Forms.Label();
            this.statusLabel = new System.Windows.Forms.Label();
            this.textLabel = new System.Windows.Forms.Label();
            this.prefixDataGridView = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prefixListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.executor_CodeTextBox = new System.Windows.Forms.TextBox();
            this.iDHeaderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.isExternalPrefixTextBox = new System.Windows.Forms.TextBox();
            this.isFrequencyDataTextBox = new System.Windows.Forms.TextBox();
            this.isHexFormatTextBox = new System.Windows.Forms.TextBox();
            this.isInversedDataTextBox = new System.Windows.Forms.TextBox();
            this.isInversedPrefixTextBox = new System.Windows.Forms.TextBox();
            this.isRestrictedTextBox = new System.Windows.Forms.TextBox();
            this.parent_IDTextBox = new System.Windows.Forms.TextBox();
            this.statusTextBox = new System.Windows.Forms.TextBox();
            this.textTextBox = new System.Windows.Forms.TextBox();
            this.iDTextBox1 = new System.Windows.Forms.TextBox();
            this.StatusBox = new System.Windows.Forms.GroupBox();
            this.StatusQ = new System.Windows.Forms.RadioButton();
            this.StatusP = new System.Windows.Forms.RadioButton();
            this.StatusM = new System.Windows.Forms.RadioButton();
            this.StatusE = new System.Windows.Forms.RadioButton();
            iDLabel1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.prefixDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prefixListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iDHeaderBindingSource)).BeginInit();
            this.StatusBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // iDLabel1
            // 
            iDLabel1.AutoSize = true;
            iDLabel1.Location = new System.Drawing.Point(42, 39);
            iDLabel1.Name = "iDLabel1";
            iDLabel1.Size = new System.Drawing.Size(21, 13);
            iDLabel1.TabIndex = 24;
            iDLabel1.Text = "ID:";
            // 
            // executor_CodeLabel
            // 
            this.executor_CodeLabel.AutoSize = true;
            this.executor_CodeLabel.Location = new System.Drawing.Point(42, 79);
            this.executor_CodeLabel.Name = "executor_CodeLabel";
            this.executor_CodeLabel.Size = new System.Drawing.Size(52, 13);
            this.executor_CodeLabel.TabIndex = 1;
            this.executor_CodeLabel.Text = "Executor:";
            // 
            // isExternalPrefixLabel
            // 
            this.isExternalPrefixLabel.AutoSize = true;
            this.isExternalPrefixLabel.Location = new System.Drawing.Point(325, 72);
            this.isExternalPrefixLabel.Name = "isExternalPrefixLabel";
            this.isExternalPrefixLabel.Size = new System.Drawing.Size(77, 13);
            this.isExternalPrefixLabel.TabIndex = 5;
            this.isExternalPrefixLabel.Text = "External Prefix:";
            // 
            // isFrequencyDataLabel
            // 
            this.isFrequencyDataLabel.AutoSize = true;
            this.isFrequencyDataLabel.Location = new System.Drawing.Point(325, 138);
            this.isFrequencyDataLabel.Name = "isFrequencyDataLabel";
            this.isFrequencyDataLabel.Size = new System.Drawing.Size(86, 13);
            this.isFrequencyDataLabel.TabIndex = 7;
            this.isFrequencyDataLabel.Text = "Frequency Data:";
            // 
            // isHexFormatLabel
            // 
            this.isHexFormatLabel.AutoSize = true;
            this.isHexFormatLabel.Location = new System.Drawing.Point(325, 171);
            this.isHexFormatLabel.Name = "isHexFormatLabel";
            this.isHexFormatLabel.Size = new System.Drawing.Size(64, 13);
            this.isHexFormatLabel.TabIndex = 9;
            this.isHexFormatLabel.Text = "Hex Format:";
            // 
            // isInversedDataLabel
            // 
            this.isInversedDataLabel.AutoSize = true;
            this.isInversedDataLabel.Location = new System.Drawing.Point(325, 203);
            this.isInversedDataLabel.Name = "isInversedDataLabel";
            this.isInversedDataLabel.Size = new System.Drawing.Size(77, 13);
            this.isInversedDataLabel.TabIndex = 11;
            this.isInversedDataLabel.Text = "Inversed Data:";
            // 
            // isInversedPrefixLabel
            // 
            this.isInversedPrefixLabel.AutoSize = true;
            this.isInversedPrefixLabel.Location = new System.Drawing.Point(325, 39);
            this.isInversedPrefixLabel.Name = "isInversedPrefixLabel";
            this.isInversedPrefixLabel.Size = new System.Drawing.Size(80, 13);
            this.isInversedPrefixLabel.TabIndex = 13;
            this.isInversedPrefixLabel.Text = "Inversed Prefix:";
            // 
            // isRestrictedLabel
            // 
            this.isRestrictedLabel.AutoSize = true;
            this.isRestrictedLabel.Location = new System.Drawing.Point(325, 105);
            this.isRestrictedLabel.Name = "isRestrictedLabel";
            this.isRestrictedLabel.Size = new System.Drawing.Size(58, 13);
            this.isRestrictedLabel.TabIndex = 15;
            this.isRestrictedLabel.Text = "Restricted:";
            // 
            // parent_IDLabel
            // 
            this.parent_IDLabel.AutoSize = true;
            this.parent_IDLabel.Location = new System.Drawing.Point(42, 119);
            this.parent_IDLabel.Name = "parent_IDLabel";
            this.parent_IDLabel.Size = new System.Drawing.Size(55, 13);
            this.parent_IDLabel.TabIndex = 17;
            this.parent_IDLabel.Text = "Parent ID:";
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(42, 159);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(40, 13);
            this.statusLabel.TabIndex = 19;
            this.statusLabel.Text = "Status:";
            this.statusLabel.Visible = false;
            // 
            // textLabel
            // 
            this.textLabel.AutoSize = true;
            this.textLabel.Location = new System.Drawing.Point(42, 203);
            this.textLabel.Name = "textLabel";
            this.textLabel.Size = new System.Drawing.Size(31, 13);
            this.textLabel.TabIndex = 21;
            this.textLabel.Text = "Text:";
            // 
            // prefixDataGridView
            // 
            this.prefixDataGridView.AllowUserToAddRows = false;
            this.prefixDataGridView.AllowUserToDeleteRows = false;
            this.prefixDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.prefixDataGridView.AutoGenerateColumns = false;
            this.prefixDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.prefixDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Data,
            this.Description,
            this.RID});
            this.prefixDataGridView.DataSource = this.prefixListBindingSource;
            this.prefixDataGridView.Location = new System.Drawing.Point(0, 275);
            this.prefixDataGridView.Name = "prefixDataGridView";
            this.prefixDataGridView.ReadOnly = true;
            this.prefixDataGridView.Size = new System.Drawing.Size(766, 217);
            this.prefixDataGridView.TabIndex = 0;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // Data
            // 
            this.Data.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Data.DataPropertyName = "Data";
            this.Data.FillWeight = 52F;
            this.Data.HeaderText = "Prefix";
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            // 
            // Description
            // 
            this.Description.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Description.DataPropertyName = "Description";
            this.Description.FillWeight = 97.71573F;
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            // 
            // RID
            // 
            this.RID.DataPropertyName = "RID";
            this.RID.HeaderText = "RID";
            this.RID.Name = "RID";
            this.RID.ReadOnly = true;
            this.RID.Visible = false;
            // 
            // prefixListBindingSource
            // 
            this.prefixListBindingSource.DataSource = typeof(BusinessObject.Prefix);
            // 
            // executor_CodeTextBox
            // 
            this.executor_CodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "Executor_Code", true));
            this.executor_CodeTextBox.Location = new System.Drawing.Point(118, 76);
            this.executor_CodeTextBox.Name = "executor_CodeTextBox";
            this.executor_CodeTextBox.ReadOnly = true;
            this.executor_CodeTextBox.Size = new System.Drawing.Size(100, 20);
            this.executor_CodeTextBox.TabIndex = 2;
            // 
            // iDHeaderBindingSource
            // 
            this.iDHeaderBindingSource.DataSource = typeof(BusinessObject.IDHeader);
            // 
            // isExternalPrefixTextBox
            // 
            this.isExternalPrefixTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "IsExternalPrefix", true));
            this.isExternalPrefixTextBox.Location = new System.Drawing.Point(427, 69);
            this.isExternalPrefixTextBox.Name = "isExternalPrefixTextBox";
            this.isExternalPrefixTextBox.ReadOnly = true;
            this.isExternalPrefixTextBox.Size = new System.Drawing.Size(37, 20);
            this.isExternalPrefixTextBox.TabIndex = 6;
            // 
            // isFrequencyDataTextBox
            // 
            this.isFrequencyDataTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "IsFrequencyData", true));
            this.isFrequencyDataTextBox.Location = new System.Drawing.Point(427, 135);
            this.isFrequencyDataTextBox.Name = "isFrequencyDataTextBox";
            this.isFrequencyDataTextBox.ReadOnly = true;
            this.isFrequencyDataTextBox.Size = new System.Drawing.Size(37, 20);
            this.isFrequencyDataTextBox.TabIndex = 8;
            // 
            // isHexFormatTextBox
            // 
            this.isHexFormatTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "IsHexFormat", true));
            this.isHexFormatTextBox.Location = new System.Drawing.Point(427, 168);
            this.isHexFormatTextBox.Name = "isHexFormatTextBox";
            this.isHexFormatTextBox.ReadOnly = true;
            this.isHexFormatTextBox.Size = new System.Drawing.Size(37, 20);
            this.isHexFormatTextBox.TabIndex = 10;
            // 
            // isInversedDataTextBox
            // 
            this.isInversedDataTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "IsInversedData", true));
            this.isInversedDataTextBox.Location = new System.Drawing.Point(427, 200);
            this.isInversedDataTextBox.Name = "isInversedDataTextBox";
            this.isInversedDataTextBox.ReadOnly = true;
            this.isInversedDataTextBox.Size = new System.Drawing.Size(37, 20);
            this.isInversedDataTextBox.TabIndex = 12;
            // 
            // isInversedPrefixTextBox
            // 
            this.isInversedPrefixTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "IsInversedPrefix", true));
            this.isInversedPrefixTextBox.Location = new System.Drawing.Point(427, 36);
            this.isInversedPrefixTextBox.Name = "isInversedPrefixTextBox";
            this.isInversedPrefixTextBox.ReadOnly = true;
            this.isInversedPrefixTextBox.Size = new System.Drawing.Size(37, 20);
            this.isInversedPrefixTextBox.TabIndex = 14;
            // 
            // isRestrictedTextBox
            // 
            this.isRestrictedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "IsRestricted", true));
            this.isRestrictedTextBox.Location = new System.Drawing.Point(427, 102);
            this.isRestrictedTextBox.Name = "isRestrictedTextBox";
            this.isRestrictedTextBox.ReadOnly = true;
            this.isRestrictedTextBox.Size = new System.Drawing.Size(37, 20);
            this.isRestrictedTextBox.TabIndex = 16;
            // 
            // parent_IDTextBox
            // 
            this.parent_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "Parent_ID", true));
            this.parent_IDTextBox.Location = new System.Drawing.Point(118, 116);
            this.parent_IDTextBox.Name = "parent_IDTextBox";
            this.parent_IDTextBox.ReadOnly = true;
            this.parent_IDTextBox.Size = new System.Drawing.Size(100, 20);
            this.parent_IDTextBox.TabIndex = 18;
            // 
            // statusTextBox
            // 
            this.statusTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "Status", true));
            this.statusTextBox.Location = new System.Drawing.Point(118, 156);
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.ReadOnly = true;
            this.statusTextBox.Size = new System.Drawing.Size(100, 20);
            this.statusTextBox.TabIndex = 20;
            this.statusTextBox.Visible = false;
            // 
            // textTextBox
            // 
            this.textTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "Text", true));
            this.textTextBox.Location = new System.Drawing.Point(118, 196);
            this.textTextBox.Name = "textTextBox";
            this.textTextBox.ReadOnly = true;
            this.textTextBox.Size = new System.Drawing.Size(100, 20);
            this.textTextBox.TabIndex = 22;
            // 
            // iDTextBox1
            // 
            this.iDTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "ID", true));
            this.iDTextBox1.Location = new System.Drawing.Point(118, 36);
            this.iDTextBox1.Name = "iDTextBox1";
            this.iDTextBox1.ReadOnly = true;
            this.iDTextBox1.Size = new System.Drawing.Size(100, 20);
            this.iDTextBox1.TabIndex = 25;
            // 
            // StatusBox
            // 
            this.StatusBox.Controls.Add(this.StatusQ);
            this.StatusBox.Controls.Add(this.StatusP);
            this.StatusBox.Controls.Add(this.StatusM);
            this.StatusBox.Controls.Add(this.StatusE);
            this.StatusBox.Enabled = false;
            this.StatusBox.Location = new System.Drawing.Point(509, 36);
            this.StatusBox.Name = "StatusBox";
            this.StatusBox.Size = new System.Drawing.Size(121, 136);
            this.StatusBox.TabIndex = 61;
            this.StatusBox.TabStop = false;
            this.StatusBox.Text = "ID Status";
            // 
            // StatusQ
            // 
            this.StatusQ.AutoCheck = false;
            this.StatusQ.AutoSize = true;
            this.StatusQ.Location = new System.Drawing.Point(21, 19);
            this.StatusQ.Name = "StatusQ";
            this.StatusQ.Size = new System.Drawing.Size(78, 17);
            this.StatusQ.TabIndex = 56;
            this.StatusQ.Tag = "Q";
            this.StatusQ.Text = "Passed QA";
            // 
            // StatusP
            // 
            this.StatusP.AutoCheck = false;
            this.StatusP.AutoSize = true;
            this.StatusP.Location = new System.Drawing.Point(21, 112);
            this.StatusP.Name = "StatusP";
            this.StatusP.Size = new System.Drawing.Size(83, 17);
            this.StatusP.TabIndex = 59;
            this.StatusP.Tag = "P";
            this.StatusP.Text = "Project Hold";
            // 
            // StatusM
            // 
            this.StatusM.AutoCheck = false;
            this.StatusM.AutoSize = true;
            this.StatusM.Location = new System.Drawing.Point(21, 50);
            this.StatusM.Name = "StatusM";
            this.StatusM.Size = new System.Drawing.Size(65, 17);
            this.StatusM.TabIndex = 57;
            this.StatusM.Tag = "M";
            this.StatusM.Text = "Modified";
            // 
            // StatusE
            // 
            this.StatusE.AutoCheck = false;
            this.StatusE.AutoSize = true;
            this.StatusE.Location = new System.Drawing.Point(21, 81);
            this.StatusE.Name = "StatusE";
            this.StatusE.Size = new System.Drawing.Size(74, 17);
            this.StatusE.TabIndex = 58;
            this.StatusE.Tag = "E";
            this.StatusE.Text = "Exec Hold";
            // 
            // IDHeaderView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 489);
            this.ControlBox = false;
            this.Controls.Add(this.StatusBox);
            this.Controls.Add(iDLabel1);
            this.Controls.Add(this.iDTextBox1);
            this.Controls.Add(this.prefixDataGridView);
            this.Controls.Add(this.executor_CodeLabel);
            this.Controls.Add(this.executor_CodeTextBox);
            this.Controls.Add(this.textTextBox);
            this.Controls.Add(this.textLabel);
            this.Controls.Add(this.statusTextBox);
            this.Controls.Add(this.isExternalPrefixLabel);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.isExternalPrefixTextBox);
            this.Controls.Add(this.parent_IDTextBox);
            this.Controls.Add(this.isFrequencyDataLabel);
            this.Controls.Add(this.parent_IDLabel);
            this.Controls.Add(this.isFrequencyDataTextBox);
            this.Controls.Add(this.isRestrictedTextBox);
            this.Controls.Add(this.isHexFormatLabel);
            this.Controls.Add(this.isRestrictedLabel);
            this.Controls.Add(this.isHexFormatTextBox);
            this.Controls.Add(this.isInversedPrefixTextBox);
            this.Controls.Add(this.isInversedDataLabel);
            this.Controls.Add(this.isInversedPrefixLabel);
            this.Controls.Add(this.isInversedDataTextBox);
            this.Name = "IDHeaderView";
            this.Text = "Header";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.prefixDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prefixListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iDHeaderBindingSource)).EndInit();
            this.StatusBox.ResumeLayout(false);
            this.StatusBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource prefixListBindingSource;
        private System.Windows.Forms.DataGridView prefixDataGridView;
        private System.Windows.Forms.TextBox executor_CodeTextBox;
        private System.Windows.Forms.BindingSource iDHeaderBindingSource;
        private System.Windows.Forms.TextBox isExternalPrefixTextBox;
        private System.Windows.Forms.TextBox isFrequencyDataTextBox;
        private System.Windows.Forms.TextBox isHexFormatTextBox;
        private System.Windows.Forms.TextBox isInversedDataTextBox;
        private System.Windows.Forms.TextBox isInversedPrefixTextBox;
        private System.Windows.Forms.TextBox isRestrictedTextBox;
        private System.Windows.Forms.TextBox parent_IDTextBox;
        private System.Windows.Forms.TextBox statusTextBox;
        private System.Windows.Forms.TextBox textTextBox;
        private System.Windows.Forms.Label executor_CodeLabel;
        private System.Windows.Forms.Label isExternalPrefixLabel;
        private System.Windows.Forms.Label isFrequencyDataLabel;
        private System.Windows.Forms.Label isHexFormatLabel;
        private System.Windows.Forms.Label isInversedDataLabel;
        private System.Windows.Forms.Label isInversedPrefixLabel;
        private System.Windows.Forms.Label isRestrictedLabel;
        private System.Windows.Forms.Label parent_IDLabel;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Label textLabel;
        private System.Windows.Forms.TextBox iDTextBox1;
        private System.Windows.Forms.GroupBox StatusBox;
        private System.Windows.Forms.RadioButton StatusQ;
        private System.Windows.Forms.RadioButton StatusP;
        private System.Windows.Forms.RadioButton StatusM;
        private System.Windows.Forms.RadioButton StatusE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn RID;
    }
}