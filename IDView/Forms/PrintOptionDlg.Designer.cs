namespace IDView
{
    partial class PrintOptionDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logChkBox = new System.Windows.Forms.CheckBox();
            this.bmChkBox = new System.Windows.Forms.CheckBox();
            this.PrintPagesLabel = new System.Windows.Forms.Label();
            this.headChkBox = new System.Windows.Forms.CheckBox();
            this.funcChkBox = new System.Windows.Forms.CheckBox();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // logChkBox
            // 
            this.logChkBox.AutoSize = true;
            this.logChkBox.Checked = true;
            this.logChkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.logChkBox.Location = new System.Drawing.Point(17, 48);
            this.logChkBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.logChkBox.Name = "logChkBox";
            this.logChkBox.Size = new System.Drawing.Size(54, 21);
            this.logChkBox.TabIndex = 0;
            this.logChkBox.Text = "Log";
            this.logChkBox.UseVisualStyleBackColor = true;
            // 
            // bmChkBox
            // 
            this.bmChkBox.AutoSize = true;
            this.bmChkBox.Checked = true;
            this.bmChkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.bmChkBox.Location = new System.Drawing.Point(16, 76);
            this.bmChkBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bmChkBox.Name = "bmChkBox";
            this.bmChkBox.Size = new System.Drawing.Size(110, 21);
            this.bmChkBox.TabIndex = 1;
            this.bmChkBox.Text = "Brand/Model";
            this.bmChkBox.UseVisualStyleBackColor = true;
            // 
            // PrintPagesLabel
            // 
            this.PrintPagesLabel.AutoSize = true;
            this.PrintPagesLabel.Location = new System.Drawing.Point(17, 16);
            this.PrintPagesLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.PrintPagesLabel.Name = "PrintPagesLabel";
            this.PrintPagesLabel.Size = new System.Drawing.Size(97, 17);
            this.PrintPagesLabel.TabIndex = 2;
            this.PrintPagesLabel.Text = "Pages to Print";
            // 
            // headChkBox
            // 
            this.headChkBox.AutoSize = true;
            this.headChkBox.Checked = true;
            this.headChkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.headChkBox.Location = new System.Drawing.Point(16, 106);
            this.headChkBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.headChkBox.Name = "headChkBox";
            this.headChkBox.Size = new System.Drawing.Size(77, 21);
            this.headChkBox.TabIndex = 3;
            this.headChkBox.Text = "Header";
            this.headChkBox.UseVisualStyleBackColor = true;
            // 
            // funcChkBox
            // 
            this.funcChkBox.AutoSize = true;
            this.funcChkBox.Checked = true;
            this.funcChkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.funcChkBox.Location = new System.Drawing.Point(16, 135);
            this.funcChkBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.funcChkBox.Name = "funcChkBox";
            this.funcChkBox.Size = new System.Drawing.Size(84, 21);
            this.funcChkBox.TabIndex = 4;
            this.funcChkBox.Text = "Function";
            this.funcChkBox.UseVisualStyleBackColor = true;
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(208, 48);
            this.okButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(100, 28);
            this.okButton.TabIndex = 5;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(208, 106);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(100, 28);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // PrintOptionDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(359, 190);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.funcChkBox);
            this.Controls.Add(this.headChkBox);
            this.Controls.Add(this.PrintPagesLabel);
            this.Controls.Add(this.bmChkBox);
            this.Controls.Add(this.logChkBox);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "PrintOptionDlg";
            this.Text = "PrintDlg";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox logChkBox;
        private System.Windows.Forms.CheckBox bmChkBox;
        private System.Windows.Forms.Label PrintPagesLabel;
        private System.Windows.Forms.CheckBox headChkBox;
        private System.Windows.Forms.CheckBox funcChkBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
    }
}