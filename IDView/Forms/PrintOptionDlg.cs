using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IDView
{
    public partial class PrintOptionDlg : Form
    {
        #region Private Fields
        private bool _printLog;
        private bool _printBM;
        private bool _printHead;
        private bool _printFunc;
        #endregion

        #region Public flag access
        public bool PrintLog { get { return _printLog; ; } }
        public bool PrintBM  { get { return _printBM; } }
        public bool PrintHead { get { return _printHead; ; } }
        public bool PrintFunc { get { return _printFunc; } }
        #endregion


        public PrintOptionDlg()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this._printLog = this.logChkBox.Checked;
            this._printBM = this.bmChkBox.Checked;
            this._printHead = this.headChkBox.Checked;
            this._printFunc = this.funcChkBox.Checked;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}