namespace IDView
{
    partial class IDFormView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IDFormView));
            this.ViewSelector = new System.Windows.Forms.TreeView();
            this.functionDAOBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.spacer2 = new System.Windows.Forms.ToolStripLabel();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.PrevModeBtn = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.PrevIDBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.IDComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.ChangeIDBtn = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.NextIDBtn = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.NextModeBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.CB_DatabaseSelector = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.ModeChangeBtn = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.setupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.defaultDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editDatabaseIndiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.duplicateIntronsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutIDViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.functionDAOBindingNavigator)).BeginInit();
            this.functionDAOBindingNavigator.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ViewSelector
            // 
            this.ViewSelector.Dock = System.Windows.Forms.DockStyle.Left;
            this.ViewSelector.Location = new System.Drawing.Point(0, 49);
            this.ViewSelector.Name = "ViewSelector";
            this.ViewSelector.Size = new System.Drawing.Size(134, 404);
            this.ViewSelector.TabIndex = 1;
            this.ViewSelector.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.ViewSelector_AfterSelect);
            // 
            // functionDAOBindingNavigator
            // 
            this.functionDAOBindingNavigator.AddNewItem = null;
            this.functionDAOBindingNavigator.CountItem = null;
            this.functionDAOBindingNavigator.DeleteItem = null;
            this.functionDAOBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spacer2,
            this.printToolStripButton,
            this.toolStripSeparator2,
            this.PrevModeBtn,
            this.bindingNavigatorSeparator,
            this.PrevIDBtn,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.IDComboBox,
            this.ChangeIDBtn,
            this.bindingNavigatorSeparator1,
            this.NextIDBtn,
            this.bindingNavigatorSeparator2,
            this.NextModeBtn,
            this.toolStripSeparator3,
            this.toolStripLabel2,
            this.CB_DatabaseSelector,
            this.toolStripSeparator4,
            this.ModeChangeBtn});
            this.functionDAOBindingNavigator.Location = new System.Drawing.Point(0, 24);
            this.functionDAOBindingNavigator.MoveFirstItem = null;
            this.functionDAOBindingNavigator.MoveLastItem = null;
            this.functionDAOBindingNavigator.MoveNextItem = null;
            this.functionDAOBindingNavigator.MovePreviousItem = null;
            this.functionDAOBindingNavigator.Name = "functionDAOBindingNavigator";
            this.functionDAOBindingNavigator.PositionItem = null;
            this.functionDAOBindingNavigator.Size = new System.Drawing.Size(757, 25);
            this.functionDAOBindingNavigator.TabIndex = 24;
            this.functionDAOBindingNavigator.Text = "bindingNavigator1";
            // 
            // spacer2
            // 
            this.spacer2.Name = "spacer2";
            this.spacer2.Size = new System.Drawing.Size(19, 22);
            this.spacer2.Text = "    ";
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripButton.Image")));
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printToolStripButton.Text = "&Print";
            this.printToolStripButton.Click += new System.EventHandler(this.printToolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // PrevModeBtn
            // 
            this.PrevModeBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PrevModeBtn.Image = ((System.Drawing.Image)(resources.GetObject("PrevModeBtn.Image")));
            this.PrevModeBtn.Name = "PrevModeBtn";
            this.PrevModeBtn.Size = new System.Drawing.Size(23, 22);
            this.PrevModeBtn.Text = "Move first";
            this.PrevModeBtn.Click += new System.EventHandler(this.PrevModeBtn_Click);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // PrevIDBtn
            // 
            this.PrevIDBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PrevIDBtn.Image = ((System.Drawing.Image)(resources.GetObject("PrevIDBtn.Image")));
            this.PrevIDBtn.Name = "PrevIDBtn";
            this.PrevIDBtn.Size = new System.Drawing.Size(23, 22);
            this.PrevIDBtn.Text = "Move previous";
            this.PrevIDBtn.Click += new System.EventHandler(this.PrevIDBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(18, 22);
            this.toolStripLabel1.Text = "ID";
            // 
            // IDComboBox
            // 
            this.IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.IDComboBox.DropDownHeight = 200;
            this.IDComboBox.DropDownWidth = 95;
            this.IDComboBox.IntegralHeight = false;
            this.IDComboBox.MaxDropDownItems = 10;
            this.IDComboBox.Name = "IDComboBox";
            this.IDComboBox.Size = new System.Drawing.Size(100, 25);
            this.IDComboBox.SelectedIndexChanged += new System.EventHandler(this.IDComboBox_SelectedIndexChanged);
            this.IDComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IDComboBox_KeyPress);
            // 
            // ChangeIDBtn
            // 
            this.ChangeIDBtn.BackColor = System.Drawing.SystemColors.Control;
            this.ChangeIDBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ChangeIDBtn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ChangeIDBtn.Image = ((System.Drawing.Image)(resources.GetObject("ChangeIDBtn.Image")));
            this.ChangeIDBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ChangeIDBtn.Name = "ChangeIDBtn";
            this.ChangeIDBtn.Size = new System.Drawing.Size(33, 22);
            this.ChangeIDBtn.Text = "View";
            this.ChangeIDBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.ChangeIDBtn.Click += new System.EventHandler(this.ChangeIDBtn_Click);
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // NextIDBtn
            // 
            this.NextIDBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.NextIDBtn.Image = ((System.Drawing.Image)(resources.GetObject("NextIDBtn.Image")));
            this.NextIDBtn.Name = "NextIDBtn";
            this.NextIDBtn.Size = new System.Drawing.Size(23, 22);
            this.NextIDBtn.Text = "Move next";
            this.NextIDBtn.Click += new System.EventHandler(this.NextIDBtn_Click);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // NextModeBtn
            // 
            this.NextModeBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.NextModeBtn.Image = ((System.Drawing.Image)(resources.GetObject("NextModeBtn.Image")));
            this.NextModeBtn.Name = "NextModeBtn";
            this.NextModeBtn.Size = new System.Drawing.Size(23, 22);
            this.NextModeBtn.Text = "Move last";
            this.NextModeBtn.Click += new System.EventHandler(this.NextModeBtn_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(43, 22);
            this.toolStripLabel2.Text = "Viewing";
            // 
            // CB_DatabaseSelector
            // 
            this.CB_DatabaseSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_DatabaseSelector.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.CB_DatabaseSelector.Name = "CB_DatabaseSelector";
            this.CB_DatabaseSelector.Size = new System.Drawing.Size(150, 25);
            this.CB_DatabaseSelector.SelectedIndexChanged += new System.EventHandler(this.CB_DatabaseSelector_SelectedIndexChanged);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // ModeChangeBtn
            // 
            this.ModeChangeBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ModeChangeBtn.Image = ((System.Drawing.Image)(resources.GetObject("ModeChangeBtn.Image")));
            this.ModeChangeBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ModeChangeBtn.Name = "ModeChangeBtn";
            this.ModeChangeBtn.Size = new System.Drawing.Size(105, 22);
            this.ModeChangeBtn.Text = "Using Viewing Mode";
            this.ModeChangeBtn.Click += new System.EventHandler(this.ModeChangeBtn_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setupToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(757, 24);
            this.menuStrip1.TabIndex = 26;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // setupToolStripMenuItem
            // 
            this.setupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.defaultDatabaseToolStripMenuItem});
            this.setupToolStripMenuItem.Name = "setupToolStripMenuItem";
            this.setupToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.setupToolStripMenuItem.Text = "Settings";
            // 
            // defaultDatabaseToolStripMenuItem
            // 
            this.defaultDatabaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainDatabaseToolStripMenuItem,
            this.editDatabaseToolStripMenuItem,
            this.editDatabaseIndiaToolStripMenuItem});
            this.defaultDatabaseToolStripMenuItem.Name = "defaultDatabaseToolStripMenuItem";
            this.defaultDatabaseToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.defaultDatabaseToolStripMenuItem.Text = "Default Database";
            // 
            // mainDatabaseToolStripMenuItem
            // 
            this.mainDatabaseToolStripMenuItem.Name = "mainDatabaseToolStripMenuItem";
            this.mainDatabaseToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.mainDatabaseToolStripMenuItem.Text = "Main";
            this.mainDatabaseToolStripMenuItem.Click += new System.EventHandler(this.mainDatabaseToolStripMenuItem_Click);
            // 
            // editDatabaseToolStripMenuItem
            // 
            this.editDatabaseToolStripMenuItem.Enabled = false;
            this.editDatabaseToolStripMenuItem.Name = "editDatabaseToolStripMenuItem";
            this.editDatabaseToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.editDatabaseToolStripMenuItem.Text = "Edit";
            this.editDatabaseToolStripMenuItem.Click += new System.EventHandler(this.editDatabaseToolStripMenuItem_Click);
            // 
            // editDatabaseIndiaToolStripMenuItem
            // 
            this.editDatabaseIndiaToolStripMenuItem.Enabled = false;
            this.editDatabaseIndiaToolStripMenuItem.Name = "editDatabaseIndiaToolStripMenuItem";
            this.editDatabaseIndiaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.editDatabaseIndiaToolStripMenuItem.Text = "India";
            this.editDatabaseIndiaToolStripMenuItem.Click += new System.EventHandler(this.editDatabaseIndiaToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.duplicateIntronsToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // duplicateIntronsToolStripMenuItem
            // 
            this.duplicateIntronsToolStripMenuItem.Name = "duplicateIntronsToolStripMenuItem";
            this.duplicateIntronsToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.duplicateIntronsToolStripMenuItem.Text = "Duplicate Introns";
            this.duplicateIntronsToolStripMenuItem.Click += new System.EventHandler(this.duplicateIntronsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutIDViewToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutIDViewToolStripMenuItem
            // 
            this.aboutIDViewToolStripMenuItem.Name = "aboutIDViewToolStripMenuItem";
            this.aboutIDViewToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.aboutIDViewToolStripMenuItem.Text = "About ID View";
            this.aboutIDViewToolStripMenuItem.Click += new System.EventHandler(this.aboutIDViewToolStripMenuItem_Click);
            // 
            // IDFormView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 453);
            this.Controls.Add(this.ViewSelector);
            this.Controls.Add(this.functionDAOBindingNavigator);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "IDFormView";
            this.Text = "ID View";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.IDFormView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.functionDAOBindingNavigator)).EndInit();
            this.functionDAOBindingNavigator.ResumeLayout(false);
            this.functionDAOBindingNavigator.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView ViewSelector;
        private System.Windows.Forms.BindingNavigator functionDAOBindingNavigator;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox IDComboBox;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripLabel spacer2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton printToolStripButton;
        private System.Windows.Forms.ToolStripButton PrevModeBtn;
        private System.Windows.Forms.ToolStripButton PrevIDBtn;
        private System.Windows.Forms.ToolStripButton ChangeIDBtn;
        private System.Windows.Forms.ToolStripButton NextIDBtn;
        private System.Windows.Forms.ToolStripButton NextModeBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem setupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem defaultDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mainDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem duplicateIntronsToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton ModeChangeBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripComboBox CB_DatabaseSelector;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripMenuItem editDatabaseIndiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutIDViewToolStripMenuItem;

    }
}