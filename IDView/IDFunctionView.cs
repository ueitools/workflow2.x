using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;

namespace IDView
{
    public partial class IDFunctionView : Form
    {
        string UnlinkedItems = "Not Linked";    // a label for the TNListBox to allow highlighting data that has no TN references
        int LastSortColumn;                     // used for clearing the initial highlighting of previouse sorts
        string FunctionData;                    // used for data matching highlights
        private int LastLinePrinted;            // for keeping track of lines printed

        private int PrevSelectedTn;             // The Tnlistbox needs to keep track of selections
        private int PrevSelectedTnValue = 0;
        private string TNStatusFlags;           // For highlighting TNs that are not QA status  
        private Color NormalText = Color.Black;
        private Color GrayText = Color.LightGray;
        private Color BkgndColr = Color.White;
        private Color highlight = Color.LightGreen;
        private Color warning = Color.LightCoral;
        private Color Selected = Color.RoyalBlue;
        private bool isCopyMode;

        public void setMode(bool mode)
        {
            isCopyMode = mode;
        }

        public void resetLastLinePrinted()
        {
            LastLinePrinted = 0;
        }

        public IDFunctionView()
        {
            InitializeComponent();
        }

        public int getPrevSelectedTNValue()
        {
            return PrevSelectedTnValue;
        }

        /// <summary>
        /// format a line of text for a header line;
        /// </summary>
        /// <returns></returns>
        public string GetReportHeader()
        {
            return string.Format("Synth   Data                    Label                Comment                      Intron    IntronPriority");
        }
        /// <summary>
        /// Format the specified row of data from the table for printing.
        /// The data is returned in the order it was last viewed. 
        /// if the data sort order was changed the data will be returned in that order.
        /// </summary>
        /// <param name="row">the zero based row of data requested.</param>
        /// <returns>a string sutible for printing</returns>
        public string GetReportLine()
        {
            int row = LastLinePrinted++;
            string data = null;
            if (row >= 0 && row < iDFunctionCollectionDataGridView.Rows.Count)
            {
                IDFunction IDFunctionRow = (IDFunction)iDFunctionCollectionDataGridView.Rows[row].DataBoundItem;
                
                data += string.Format("{0,-7}{1,-25}{2,-20}{3,-30}{4,-10}{5}",
                    IDFunctionRow.Synth, IDFunctionRow.FormattedData,
                    IDFunctionRow.Label, IDFunctionRow.Comment,
                    IDFunctionRow.Intron, IDFunctionRow.IntronPriority);
            }
            return data;
        }

        /// <summary>
        /// Search the function table and highlight all entries that reference the selected TN
        /// </summary>
        /// <param name="TNHighlight">Number of the TN to highlight</param>
        public void HighlightTn(int TNHighlight)
        {
            int index = TNListbox.FindString(string.Format("TN{0:D5}", TNHighlight));
            if (index >= 0)
            {
                TNListbox.SetSelected(index, true);
                HighlightDataFromTn();
            }
        }

        /// <summary>
        /// Load the data for the selected ID and populate the views
        /// </summary>
        /// <param name="SelectedID">The name of the ID to load, example "T0000"</param>
        public void LoadID(string SelectedID)
        {
            LastLinePrinted = 0;
            IDFunctionCollection IDFunctionData = DAOFactory.ReadOnly().IDFunctionSelect(SelectedID);
            iDFunctionCollectionDataGridView.DataSource = IDFunctionData;
            iDFunctionCollectionDataGridView.Sort(iDFunctionCollectionDataGridView.Columns["Label"], 
                                                    ListSortDirection.Ascending);
            GetIDTnList(SelectedID);
        }

        /// <summary>
        /// Load a list of all TNs that reference this ID.
        /// Store them in the TNListbox for the user to select
        /// </summary>
        /// <param name="ID">Currently selected ID name such as "T0000"</param>
        private void GetIDTnList(string id)
        {
            PrevSelectedTn = -1;
            TNListbox.Items.Clear();
            TNStatusFlags = "Q";

            IntegerCollection list = DBFunctions.GetTNList(id);

            string TN = UnlinkedItems;
            TNListbox.Items.Add(TN);

            foreach (int item in list)
            {
                TN = string.Format("TN{0:D5}", item);
                TNListbox.Items.Add(TN);

                TNHeader TNHeaderData = DAOFactory.TNHeader().Select(item);
                TNStatusFlags += TNHeaderData.Status[0];
            }
        }

        /// <summary>
        /// Intercept all main grid cell clicks to enable highlighting of rows with data 
        /// matching the selected row.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void iDFunctionCollectionDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)        // Sorting?
                return;

            
            TNListbox.SelectedItem = null;
            GetSelectedRowData();
            if (!isCopyMode)
                HighlightMatchingDataRows();
        }

        /// <summary>
        /// highlight all DataGrid rows with data that match the "FunctionData" from the last selected row.
        /// NOTE: this is also called after sorting when the last selected row may not be selected anymore
        /// </summary>
        private void HighlightMatchingDataRows()
        {
            IDFunction IDFunctionRow;
            foreach (DataGridViewRow row in iDFunctionCollectionDataGridView.Rows)
            {
                row.DefaultCellStyle.BackColor = BkgndColr;
                IDFunctionRow = (IDFunction)row.DataBoundItem;

                if (IDFunctionRow.Data.ToString() == FunctionData)
                    row.DefaultCellStyle.BackColor = highlight;
            }
        }

        /// <summary>
        /// highlight all duplicate Intron/priority pairs
        /// </summary>
        public void HighlightMatchingIntronRows()
        {
            IDFunction fncrow;
            string SearchIntron;
            string TestIntron;

            // Set background to a known state.
            foreach (DataGridViewRow row in iDFunctionCollectionDataGridView.Rows)
                row.DefaultCellStyle.BackColor = BkgndColr;

            foreach (DataGridViewRow irow in iDFunctionCollectionDataGridView.Rows)
            {
                fncrow = (IDFunction)irow.DataBoundItem;
                SearchIntron = string.Format("{0}.{1}", fncrow.Intron, fncrow.IntronPriority);
                if (SearchIntron.Equals("."))
                    continue;

                foreach (DataGridViewRow jrow in iDFunctionCollectionDataGridView.Rows)
                {
                    if (irow == jrow)
                        continue;
                    fncrow = (IDFunction)jrow.DataBoundItem;
                    TestIntron = string.Format("{0}.{1}", fncrow.Intron, fncrow.IntronPriority);
                    if (SearchIntron == TestIntron)
                    {
                        irow.DefaultCellStyle.BackColor = warning;
                        jrow.DefaultCellStyle.BackColor = warning;
                    }
                }
            }
        }

        /// <summary>
        /// Store the Data value of the currently selected row into "FunctionData" for later use.
        /// </summary>
        private void GetSelectedRowData()
        {
            int RowIndex = iDFunctionCollectionDataGridView.CurrentCell.RowIndex;

            if (RowIndex >= 0 && RowIndex < iDFunctionCollectionDataGridView.Rows.Count)
            {
                DataGridViewRow d1 = iDFunctionCollectionDataGridView.Rows[RowIndex];
                IDFunction IDFunctionRow = (IDFunction)d1.DataBoundItem;
                FunctionData = IDFunctionRow.Data.ToString();
            }
        }

        /// <summary>
        /// Highlight all Function Table rows that contain a referenct to the TN selected in the TNListbox
        /// </summary>
        private void HighlightDataFromTn()
        {
            string SelectedTnText;
            int TnNumber;

            if (TNListbox.SelectedItem == null)
                return;

            SelectedTnText = TNListbox.SelectedItem.ToString();
            if (SelectedTnText == UnlinkedItems)
                TnNumber = -1;
            else
                TnNumber = int.Parse(SelectedTnText.Substring(2));

            foreach (DataGridViewRow row in iDFunctionCollectionDataGridView.Rows)
            {
                row.DefaultCellStyle.BackColor = BkgndColr;
                IDFunction IDFunctionRow = (IDFunction)row.DataBoundItem;
                IDTNCaptureCollection TnList = IDFunctionRow.IDTNCaptureList;

                if (TnNumber == -1)
                {
                    if (TnList.Count == 1 && TnList[0].TN == 0)
                        row.DefaultCellStyle.BackColor = highlight;
                }
                else
                {
                    foreach (IDTNCapture TNRow in TnList)
                    {
                        if (TNRow.TN == TnNumber)
                        {
                            row.DefaultCellStyle.BackColor = highlight;
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Called after a sort has occured to 
        /// 1. re-highlight any Data or TN references that may have existed
        /// 2. setup initial highlighting on the sorted column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void iDFunctionCollectionDataGridView_Sorted(object sender, EventArgs e)
        {
            HighlightMatchingDataRows();
            HighlightDataFromTn();
            InitialHighlightColumn((DataGridView)sender);
        }

        /// <summary>
        /// After sorting, the "Sort by" column will be "Initial Highlighted" 
        /// that is, the first item in the list will be normal black and 
        /// any following duplicates will be light grey
        /// </summary>
        /// <param name="sender"></param>
        private void InitialHighlightColumn(DataGridView sender)
        {
            int SortColumn = (sender).SortedColumn.Index;
            string HeaderText = (sender).SortedColumn.HeaderText;
            string CurrentValue;
            string LastValue = "";

            foreach (DataGridViewRow row in iDFunctionCollectionDataGridView.Rows)
            {
                // reset all cells in this row to default black
                row.Cells[LastSortColumn].Style.ForeColor = NormalText;

                if (row.Cells[SortColumn].Value == null)
                    continue;

                // get the text for this cell
                CurrentValue = row.Cells[SortColumn].Value.ToString();

                if (CurrentValue == LastValue)
                    row.Cells[SortColumn].Style.ForeColor = GrayText;

                LastValue = CurrentValue;
            }
            LastSortColumn = SortColumn;
        }

        /// <summary>
        /// When the user changes the selected TN, clear any grid selection and 
        /// highlight the selected TN references
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TNListbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            iDFunctionCollectionDataGridView.ClearSelection();
            HighlightDataFromTn();

            if (TNListbox.SelectedIndex > 0)
                PrevSelectedTnValue = Int32.Parse(TNListbox.SelectedItem.ToString().Substring(2));
        }

        private void TNListbox_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (TNListbox.Items.Count > 0)
            {
                DrawOneTNListboxItem(e, PrevSelectedTn);
                DrawOneTNListboxItem(e, e.Index);

                PrevSelectedTn = TNListbox.SelectedIndex;
            }
        }

        private void DrawOneTNListboxItem(DrawItemEventArgs e, int item)
        {
            // Don't attempt to draw with bad index values
            if (item < 0 || item > TNListbox.Items.Count)
                return;

            Brush bkbrush = Brushes.White;
            Brush txtbrush = Brushes.Black;

            // If it is a non-QA set the special background color
            if (TNStatusFlags[item] != 'Q')
                bkbrush = Brushes.LightCoral;

            // If it's selected change both background and text colors
            if (TNListbox.SelectedIndex == item)
            {
                bkbrush = Brushes.RoyalBlue;
                txtbrush = Brushes.White;
            }
            // Draw the box then text for the current item
            e.Graphics.FillRectangle(bkbrush, TNListbox.GetItemRectangle(item));
            e.Graphics.DrawString(TNListbox.Items[item].ToString(), e.Font, txtbrush,
                                    TNListbox.GetItemRectangle(item),
                                    StringFormat.GenericDefault);
        }

        /// <summary>
        /// to restore the special background color when loosing focus
        /// we need to cause the ListBox to redraw without the selected index.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TNListbox_Leave(object sender, EventArgs e)
        {
            TNListbox.SelectedIndex = -1;
            TNListbox.Invalidate();
        }

        public void SetMultiSelect(bool multiSelVal)
        {
            iDFunctionCollectionDataGridView.MultiSelect = multiSelVal;
        }
    }
}