namespace IDView
{
    partial class IDBrandView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.deviceTypeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deviceCollectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.countryListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deviceCollectionDataGridView = new System.Windows.Forms.DataGridView();
            this.opInfoListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.treeLocations = new System.Windows.Forms.TreeView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.treeDeviceTypes = new System.Windows.Forms.TreeView();
            this.TN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Brand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AliasBrand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AliasType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsTarget = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Model = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Branch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Component = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsRetail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsCodebook = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsPartNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.deviceTypeListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceCollectionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countryListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceCollectionDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.opInfoListBindingSource)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // deviceTypeListBindingSource
            // 
            this.deviceTypeListBindingSource.DataMember = "DeviceTypeList";
            this.deviceTypeListBindingSource.DataSource = this.deviceCollectionBindingSource;
            // 
            // deviceCollectionBindingSource
            // 
            this.deviceCollectionBindingSource.DataSource = typeof(BusinessObject.DeviceCollection);
            // 
            // countryListBindingSource
            // 
            this.countryListBindingSource.DataMember = "CountryList";
            this.countryListBindingSource.DataSource = this.deviceCollectionBindingSource;
            // 
            // deviceCollectionDataGridView
            // 
            this.deviceCollectionDataGridView.AllowUserToAddRows = false;
            this.deviceCollectionDataGridView.AllowUserToDeleteRows = false;
            this.deviceCollectionDataGridView.AutoGenerateColumns = false;
            this.deviceCollectionDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.deviceCollectionDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TN,
            this.Brand,
            this.AliasBrand,
            this.AliasType,
            this.IsTarget,
            this.Model,
            this.Branch,
            this.Country,
            this.Component,
            this.IsRetail,
            this.IsCodebook,
            this.IsPartNumber});
            this.deviceCollectionDataGridView.DataSource = this.deviceCollectionBindingSource;
            this.deviceCollectionDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deviceCollectionDataGridView.Location = new System.Drawing.Point(0, 0);
            this.deviceCollectionDataGridView.Name = "deviceCollectionDataGridView";
            this.deviceCollectionDataGridView.ReadOnly = true;
            this.deviceCollectionDataGridView.Size = new System.Drawing.Size(656, 467);
            this.deviceCollectionDataGridView.TabIndex = 24;
            this.deviceCollectionDataGridView.Sorted += new System.EventHandler(this.deviceCollectionDataGridView_Sorted);
            // 
            // opInfoListBindingSource
            // 
            this.opInfoListBindingSource.DataMember = "OpInfoList";
            this.opInfoListBindingSource.DataSource = this.deviceCollectionBindingSource;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.deviceCollectionDataGridView);
            this.splitContainer1.Size = new System.Drawing.Size(775, 467);
            this.splitContainer1.SplitterDistance = 115;
            this.splitContainer1.TabIndex = 25;
            this.splitContainer1.Text = "splitContainer1";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer2.Size = new System.Drawing.Size(115, 467);
            this.splitContainer2.SplitterDistance = 234;
            this.splitContainer2.TabIndex = 2;
            this.splitContainer2.Text = "splitContainer2";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.treeLocations);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(115, 234);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Regions";
            // 
            // treeLocations
            // 
            this.treeLocations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeLocations.HideSelection = false;
            this.treeLocations.Location = new System.Drawing.Point(3, 16);
            this.treeLocations.Name = "treeLocations";
            this.treeLocations.Size = new System.Drawing.Size(109, 215);
            this.treeLocations.TabIndex = 0;
            this.treeLocations.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeLocations_AfterSelect);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.treeDeviceTypes);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(115, 229);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Main and Sub Devices";
            // 
            // treeDeviceTypes
            // 
            this.treeDeviceTypes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeDeviceTypes.HideSelection = false;
            this.treeDeviceTypes.Location = new System.Drawing.Point(3, 16);
            this.treeDeviceTypes.Name = "treeDeviceTypes";
            this.treeDeviceTypes.Size = new System.Drawing.Size(109, 210);
            this.treeDeviceTypes.TabIndex = 1;
            this.treeDeviceTypes.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeDeviceTypes_AfterSelect);
            // 
            // TN
            // 
            this.TN.DataPropertyName = "TN";
            this.TN.HeaderText = "TN";
            this.TN.Name = "TN";
            this.TN.ReadOnly = true;
            // 
            // Brand
            // 
            this.Brand.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Brand.DataPropertyName = "Brand";
            this.Brand.HeaderText = "Brand";
            this.Brand.Name = "Brand";
            this.Brand.ReadOnly = true;
            // 
            // AliasBrand
            // 
            this.AliasBrand.DataPropertyName = "AliasBrand";
            this.AliasBrand.HeaderText = "AliasBrand";
            this.AliasBrand.Name = "AliasBrand";
            this.AliasBrand.ReadOnly = true;
            // 
            // AliasType
            // 
            this.AliasType.DataPropertyName = "AliasType";
            this.AliasType.HeaderText = "AliasType";
            this.AliasType.Name = "AliasType";
            this.AliasType.ReadOnly = true;
            // 
            // IsTarget
            // 
            this.IsTarget.DataPropertyName = "IsTarget";
            this.IsTarget.HeaderText = "Target";
            this.IsTarget.Name = "IsTarget";
            this.IsTarget.ReadOnly = true;
            this.IsTarget.Width = 50;
            // 
            // Model
            // 
            this.Model.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Model.DataPropertyName = "Model";
            this.Model.HeaderText = "Model";
            this.Model.Name = "Model";
            this.Model.ReadOnly = true;
            // 
            // Branch
            // 
            this.Branch.DataPropertyName = "Branch";
            this.Branch.HeaderText = "Branch";
            this.Branch.Name = "Branch";
            this.Branch.ReadOnly = true;
            this.Branch.Width = 50;
            // 
            // Country
            // 
            this.Country.DataPropertyName = "Country";
            this.Country.HeaderText = "Country";
            this.Country.Name = "Country";
            this.Country.ReadOnly = true;
            // 
            // Component
            // 
            this.Component.DataPropertyName = "Component";
            this.Component.HeaderText = "Component";
            this.Component.Name = "Component";
            this.Component.ReadOnly = true;
            // 
            // IsRetail
            // 
            this.IsRetail.DataPropertyName = "IsRetail";
            this.IsRetail.HeaderText = "Retail";
            this.IsRetail.Name = "IsRetail";
            this.IsRetail.ReadOnly = true;
            this.IsRetail.Width = 50;
            // 
            // IsCodebook
            // 
            this.IsCodebook.DataPropertyName = "IsCodebook";
            this.IsCodebook.HeaderText = "Codebook";
            this.IsCodebook.Name = "IsCodebook";
            this.IsCodebook.ReadOnly = true;
            this.IsCodebook.Width = 58;
            // 
            // IsPartNumber
            // 
            this.IsPartNumber.DataPropertyName = "IsPartNumber";
            this.IsPartNumber.HeaderText = "Part #";
            this.IsPartNumber.Name = "IsPartNumber";
            this.IsPartNumber.ReadOnly = true;
            this.IsPartNumber.Width = 50;
            // 
            // IDBrandView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 467);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer1);
            this.Name = "IDBrandView";
            this.Text = "Brand";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.deviceTypeListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceCollectionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countryListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceCollectionDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.opInfoListBindingSource)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource countryListBindingSource;
        private System.Windows.Forms.BindingSource deviceCollectionBindingSource;
        private System.Windows.Forms.BindingSource opInfoListBindingSource;
        private System.Windows.Forms.BindingSource deviceTypeListBindingSource;
        private System.Windows.Forms.DataGridView deviceCollectionDataGridView;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TreeView treeLocations;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TreeView treeDeviceTypes;
        private System.Windows.Forms.DataGridViewTextBoxColumn TN;
        private System.Windows.Forms.DataGridViewTextBoxColumn Brand;
        private System.Windows.Forms.DataGridViewTextBoxColumn AliasBrand;
        private System.Windows.Forms.DataGridViewTextBoxColumn AliasType;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsTarget;
        private System.Windows.Forms.DataGridViewTextBoxColumn Model;
        private System.Windows.Forms.DataGridViewTextBoxColumn Branch;
        private System.Windows.Forms.DataGridViewTextBoxColumn Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn Component;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsRetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsCodebook;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsPartNumber;
    }
}