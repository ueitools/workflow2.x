using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using System.Configuration;
using System.IO;

namespace IDView
{
    public partial class RemotePictureFormView : Form
    {
        public RemotePictureFormView()
        {
            InitializeComponent();
        }

        public void LoadID(string SelectedID)
        {
            GetIDTnList(SelectedID);
        }

        private void GetIDTnList(string id)
        {
            string TN = "";
            this.lbTN.Items.Clear();
            IntegerCollection list = DBFunctions.GetTNList(id);
           
            foreach (int item in list)
            {
                TN = string.Format("TN{0:D5}", item);
                lbTN.Items.Add(TN);
            }
            lbTN.SelectedIndex = -1;
        }

        private void lbTN_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.boxTNPic.ImageLocation = RemotePicturePath(
            //    Int32.Parse(lbTN.SelectedItem.ToString().Substring(2)));
            ViewRemotePicture(Int32.Parse(lbTN.SelectedItem.ToString().Substring(2)));
        }

        private string RemotePicturePath(int _tn)
        {
            /// use windows picture and fax viewer 
            /// to open the picture locate in
            /// G:\Lib_Capture\Img\PICTURES 
            string TNpic_path = ConfigurationSettings.
                AppSettings["TNPICPATH"];
            string strTN = "";

            if (_tn >= 90000)
            {
                strTN = "Tn" + (_tn).ToString();
                //TNpic_path += "TN90000-\\" + strTN + ".jpg";
                TNpic_path += "TN90000-\\";
            }
            else if (_tn >= 10000)
            {
                strTN = "Tn" + (_tn).ToString();
                int tmp = _tn % 1000;
                string tmp_str = "TN" +
                    (_tn - tmp).ToString()
                    + "-TN" + (_tn - tmp + 999).ToString();
                //TNpic_path +=  tmp_str+ "\\" + strTN + ".jpg";
                TNpic_path += tmp_str + "\\";
            }
            else if (_tn >= 1000)
            {
                strTN = "Tn0" + (_tn).ToString();
                int tmp = _tn % 1000;
                string tmp_str = "TN0" + (_tn - tmp).ToString()
                    + "-TN0" + (_tn - tmp + 999).ToString();
                //TNpic_path += tmp_str + "\\" + strTN + ".jpg";
                TNpic_path += tmp_str + "\\";
            }
            else
            {
                strTN = "Tn00" + (_tn).ToString();
                int tmp = _tn % 1000;
                string tmp_str = "TN00000" 
                    + "-TN00" + (_tn - tmp + 999).ToString();
                //TNpic_path += tmp_str + "\\" + strTN + ".jpg";
                TNpic_path += tmp_str + "\\";
            }

            List<string> file_list = FindFiles(TNpic_path, strTN);
            if (file_list.Count != 0)
                return file_list[0];
            else
                return string.Empty;
        }

        private List<string> FindFiles(string dir, string strTN)
        {
            List<string> file_list = new List<string>();
            DirectoryInfo tn_dir = new DirectoryInfo(dir);
            foreach (FileInfo fi in tn_dir.GetFiles(strTN + "*.jpg"))
            {
                file_list.Add(fi.FullName);
            }
            return file_list;
        }

        private void ViewRemotePicture(int _tn)
        {
            /// use windows picture and fax viewer 
            /// to open the picture locate in
            /// G:\Lib_Capture\Img\PICTURES 
            string TNpic_path = ConfigurationSettings.
                AppSettings["TNPICPATH"];
            string strTN = "";

            if (_tn >= 90000)
            {
                strTN = "Tn" + (_tn).ToString();
                TNpic_path += "TN90000-\\";
            }
            else if (_tn >= 10000)
            {
                strTN = "Tn" + (_tn).ToString();
                int tmp = _tn % 1000;
                string tmp_str = "TN" +
                    (_tn - tmp).ToString()
                    + "-TN" + (_tn - tmp + 999).ToString();
                TNpic_path += tmp_str + "\\";
            }
            else if (_tn >= 1000)
            {
                strTN = "Tn0" + (_tn).ToString();
                int tmp = _tn % 1000;
                string tmp_str = "TN0" + (_tn - tmp).ToString()
                    + "-TN0" + (_tn - tmp + 999).ToString();
                TNpic_path += tmp_str + "\\";
            }
            else
            {
                strTN = "Tn00" + (_tn).ToString();
                int tmp = _tn % 1000;
                string tmp_str = "TN00000"
                    + "-TN00" + (_tn - tmp + 999).ToString();
                TNpic_path += tmp_str + "\\";
            }

            Process myProcess = new Process(); 
            string IMGview_path = ConfigurationSettings.
                AppSettings["IMGVIEWPATH"];
            try
            {
//                List<string> file_list = FindFiles(TNpic_path, strTN);
                string[] images = Directory.GetFiles(TNpic_path,strTN + ".*", 
                                                SearchOption.AllDirectories);

                myProcess.StartInfo.FileName = images[0];
                myProcess.StartInfo.Verb = "";
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.Start();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }
    }
}