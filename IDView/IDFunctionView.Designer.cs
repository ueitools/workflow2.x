namespace IDView
{
    partial class IDFunctionView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.TNListbox = new System.Windows.Forms.ListBox();
            this.iDFunctionCollectionDataGridView = new System.Windows.Forms.DataGridView();
            this.Synth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Label = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Intron = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IntronPriority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDFunctionCollectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.functionDAOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.functionCollectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iDFunctionCollectionDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iDFunctionCollectionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.functionDAOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.functionCollectionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.AutoScroll = true;
            this.splitContainer2.Panel1.Controls.Add(this.TNListbox);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.iDFunctionCollectionDataGridView);
            this.splitContainer2.Size = new System.Drawing.Size(722, 427);
            this.splitContainer2.SplitterDistance = 67;
            this.splitContainer2.TabIndex = 1;
            this.splitContainer2.Text = "splitContainer2";
            // 
            // TNListbox
            // 
            this.TNListbox.DisplayMember = "TN";
            this.TNListbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TNListbox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.TNListbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TNListbox.FormattingEnabled = true;
            this.TNListbox.ItemHeight = 16;
            this.TNListbox.Location = new System.Drawing.Point(0, 0);
            this.TNListbox.Name = "TNListbox";
            this.TNListbox.Size = new System.Drawing.Size(67, 427);
            this.TNListbox.TabIndex = 0;
            this.TNListbox.ValueMember = "TN";
            this.TNListbox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.TNListbox_DrawItem);
            this.TNListbox.SelectedIndexChanged += new System.EventHandler(this.TNListbox_SelectedIndexChanged);
            this.TNListbox.Leave += new System.EventHandler(this.TNListbox_Leave);
            // 
            // iDFunctionCollectionDataGridView
            // 
            this.iDFunctionCollectionDataGridView.AllowUserToAddRows = false;
            this.iDFunctionCollectionDataGridView.AllowUserToDeleteRows = false;
            this.iDFunctionCollectionDataGridView.AutoGenerateColumns = false;
            this.iDFunctionCollectionDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.iDFunctionCollectionDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Synth,
            this.Data,
            this.Label,
            this.Comment,
            this.Intron,
            this.IntronPriority});
            this.iDFunctionCollectionDataGridView.DataSource = this.iDFunctionCollectionBindingSource;
            this.iDFunctionCollectionDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iDFunctionCollectionDataGridView.Location = new System.Drawing.Point(0, 0);
            this.iDFunctionCollectionDataGridView.MultiSelect = false;
            this.iDFunctionCollectionDataGridView.Name = "iDFunctionCollectionDataGridView";
            this.iDFunctionCollectionDataGridView.ReadOnly = true;
            this.iDFunctionCollectionDataGridView.Size = new System.Drawing.Size(651, 427);
            this.iDFunctionCollectionDataGridView.TabIndex = 0;
            this.iDFunctionCollectionDataGridView.Sorted += new System.EventHandler(this.iDFunctionCollectionDataGridView_Sorted);
            this.iDFunctionCollectionDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.iDFunctionCollectionDataGridView_CellClick);
            // 
            // Synth
            // 
            this.Synth.DataPropertyName = "Synth";
            this.Synth.HeaderText = "Synth";
            this.Synth.Name = "Synth";
            this.Synth.ReadOnly = true;
            this.Synth.Width = 50;
            // 
            // Data
            // 
            this.Data.DataPropertyName = "FormattedData";
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            this.Data.Width = 250;
            // 
            // Label
            // 
            this.Label.DataPropertyName = "Label";
            this.Label.HeaderText = "Label";
            this.Label.Name = "Label";
            this.Label.ReadOnly = true;
            this.Label.Width = 250;
            // 
            // Comment
            // 
            this.Comment.DataPropertyName = "Comment";
            this.Comment.HeaderText = "Comment";
            this.Comment.Name = "Comment";
            this.Comment.ReadOnly = true;
            this.Comment.Width = 101;
            // 
            // Intron
            // 
            this.Intron.DataPropertyName = "Intron";
            this.Intron.HeaderText = "Intron";
            this.Intron.Name = "Intron";
            this.Intron.ReadOnly = true;
            // 
            // IntronPriority
            // 
            this.IntronPriority.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.IntronPriority.DataPropertyName = "IntronPriority";
            this.IntronPriority.HeaderText = "P";
            this.IntronPriority.Name = "IntronPriority";
            this.IntronPriority.ReadOnly = true;
            // 
            // iDFunctionCollectionBindingSource
            // 
            this.iDFunctionCollectionBindingSource.DataSource = typeof(BusinessObject.IDFunctionCollection);
            // 
            // functionDAOBindingSource
            // 
            this.functionDAOBindingSource.DataSource = typeof(BusinessObject.FunctionDAO);
            // 
            // functionCollectionBindingSource
            // 
            this.functionCollectionBindingSource.DataSource = typeof(BusinessObject.FunctionCollection);
            // 
            // IDFunctionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 427);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer2);
            this.Name = "IDFunctionView";
            this.Text = "Function";
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iDFunctionCollectionDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iDFunctionCollectionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.functionDAOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.functionCollectionBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource functionDAOBindingSource;
        private System.Windows.Forms.BindingSource functionCollectionBindingSource;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ListBox TNListbox;
        private System.Windows.Forms.DataGridView iDFunctionCollectionDataGridView;
        private System.Windows.Forms.BindingSource iDFunctionCollectionBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn Synth;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Label;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comment;
        private System.Windows.Forms.DataGridViewTextBoxColumn Intron;
        private System.Windows.Forms.DataGridViewTextBoxColumn IntronPriority;
    }
}

