using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Win32;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using System.IO;
using System.Configuration;
//using DBEditor;

namespace IDView
{
    public partial class IDFormView : Form
    {
        /// <summary>
        /// All the tree view names
        /// </summary>
        public const string TreeHeader = "ID View";
        public const string LogViewNode = "Log";
        public const string ModelViewNode = "Brand/Model";
        public const string HeaderViewNode = "Header";
        public const string FunctionViewNode = "Function";
        public const string RemotePictureViewNode = "RemotePicture";

        string ViewID = "";
        int TNHighlight = 0;

        const string _mainDB = "Main Database";
        const string _tempDB = "Temp Database";
        const string _indiaDB = "Temp Database - India";
        static string SelectedDB = _mainDB;

        IDLogView _idLogForm = new IDLogView();
        IDBrandView _idBrandForm = new IDBrandView();
        IDHeaderView _idHeaderForm = new IDHeaderView();
        IDFunctionView _idFunctionForm = new IDFunctionView();
        RemotePictureFormView _remotePictureForm = new RemotePictureFormView();
        bool isCopyMode = false;
        int CurrentViewForm = 3; // function view form



        /// <summary>
        /// Store the requested ID and TN and setup all the child views.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tn"></param>
        public IDFormView(string id, int tn)
        {
            TNHighlight = tn;
            if (!string.IsNullOrEmpty(id))
                ViewID = id;

            InitializeComponent();
            _idLogForm.MdiParent = this;
            _idBrandForm.MdiParent = this;
            _idHeaderForm.MdiParent = this;
            _idFunctionForm.MdiParent = this;
            _remotePictureForm.MdiParent = this;

            CB_DatabaseSelector.Items.Add(_mainDB);

            // disable until databases operational
            CB_DatabaseSelector.Items.Add(_tempDB);
            if (SystemInformation.UserDomainName == WorkflowConfig.GetItem("CypressDomain"))
                CB_DatabaseSelector.Items.Add(_indiaDB);

            CB_DatabaseSelector.SelectedIndex = 0;

            mainDatabaseToolStripMenuItem.Text = _mainDB;
            editDatabaseToolStripMenuItem.Text = _tempDB;
            editDatabaseIndiaToolStripMenuItem.Text = _indiaDB;
        }

        /// <summary>
        /// Setup the tree list, load the requested ID and highlight the indicated TN
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IDFormView_Load(object sender, EventArgs e)
        {
            ViewSelector.Nodes.Add(TreeHeader);
            ViewSelector.Nodes[0].Nodes.Add(TreeHeader, LogViewNode);
            ViewSelector.Nodes[0].Nodes.Add(TreeHeader, ModelViewNode);
            ViewSelector.Nodes[0].Nodes.Add(TreeHeader, HeaderViewNode);
            ViewSelector.Nodes[0].Nodes.Add(TreeHeader, FunctionViewNode);
            ViewSelector.Nodes[0].Nodes.Add(TreeHeader, RemotePictureViewNode);
            ViewSelector.Nodes[0].Expand();

            string defaultDatabase = _tempDB;
            defaultDatabase = (string)Registry.GetValue(
                Reg.PROJ_ROOT, Reg.DEFAULTDB, defaultDatabase);

            CB_DatabaseSelector.SelectedItem = defaultDatabase;
            SelectedDB = defaultDatabase;

            SetDefaultDBCheckboxes(defaultDatabase);

            LoadFormView(ViewID);
        }

        private void LoadFormView(string usingID)
        {
            if (!string.IsNullOrEmpty(usingID))
                GetIDList(usingID);

            this.Show();

            _idFunctionForm.Show();
            _idFunctionForm.setMode(isCopyMode);
            // When a menu was added this became necessary, otherwise the window was small
            _idFunctionForm.WindowState = FormWindowState.Maximized;
            
            SetSelectedMode();
            
            if (!isCopyMode && TNHighlight > 0)
            {
                _idFunctionForm.HighlightTn(TNHighlight);
            }
            
        }

        /// <summary>
        /// When the user selects a different view, show the vew and make sure it's in front
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewSelector_AfterSelect(object sender, TreeViewEventArgs e)
        {
            switch(e.Node.Text)
            {
                case LogViewNode:
                    CurrentViewForm = 0;
                    isCopyMode = false;
                    SetSelectedMode();
                    _idLogForm.Show();
                    _idLogForm.BringToFront();
                    break;
                case ModelViewNode:
                    CurrentViewForm = 1;
                    isCopyMode = false;
                    SetSelectedMode();
                    _idBrandForm.Show();
                    _idBrandForm.BringToFront();
                    break;
                case HeaderViewNode:
                    CurrentViewForm = 2;
                    isCopyMode = false;
                    SetSelectedMode();
                    _idHeaderForm.Show();
                    _idHeaderForm.BringToFront();
                    break;
                case FunctionViewNode:
                    CurrentViewForm = 3;
                    isCopyMode = false;
                    SetSelectedMode();
                    _idFunctionForm.Show();
                    _idFunctionForm.BringToFront();
                    break;
                case RemotePictureViewNode:
                    isCopyMode = false;
                    _remotePictureForm.Show();
                    _remotePictureForm.BringToFront();
                    break;
                default: break;
            }
        }

        private void ViewRemotePicture(int _tn)
        {
            /// use windows picture and fax viewer 
            /// to open the picture locate in
            /// G:\Lib_Capture\Img\PICTURES 
            string TNpic_path = ConfigurationSettings.
                AppSettings["TNPICPATH"];
            string strTN = "";

            if (_tn >= 90000)
            {
                strTN = "Tn" + (_tn).ToString();
                //TNpic_path += "TN90000-\\" + strTN + ".jpg";
                TNpic_path += "TN90000-\\";
            }
            else if (_tn >= 10000)
            {
                strTN = "Tn" + (_tn).ToString();
                int tmp = _tn % 1000;
                string tmp_str = "TN" +
                    (_tn - tmp).ToString()
                    + "-TN" + (_tn - tmp + 999).ToString();
                //TNpic_path +=  tmp_str+ "\\" + strTN + ".jpg";
                TNpic_path += tmp_str + "\\";
            }
            else
            {
                strTN = "Tn0" + (_tn).ToString();
                int tmp = _tn % 1000;
                string tmp_str = "TN0" + (_tn - tmp).ToString()
                    + "-TN0" + (_tn - tmp + 999).ToString();
                //TNpic_path += tmp_str + "\\" + strTN + ".jpg";
                TNpic_path += tmp_str + "\\";
            }


            string IMGview_path = ConfigurationSettings.
                AppSettings["IMGVIEWPATH"];
            try
            {
                List<string> file_list = FindFiles(TNpic_path, strTN);
                string args = IMGview_path + " " + file_list[0];
                System.Diagnostics.Process result =
                    System.Diagnostics.Process.Start(
                    "rundll32.exe", args);
                if (result.HasExited)
                    MessageBox.Show("The Picture File wasn't found.");
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        private List<string> FindFiles(string dir, string strTN)
        {
            List<string> file_list = new List<string>();
            DirectoryInfo tn_dir = new DirectoryInfo(dir);
            foreach (FileInfo fi in tn_dir.GetFiles(strTN + "*.jpg"))
            {
                file_list.Add(fi.FullName);
            }
            return file_list;
        }

        /// <summary>
        /// Load a list of ALL IDs in the database into the IDComboBox
        /// </summary>
        private void GetIDList(string usingID)
        {
            SetSelectedDB();
            StringCollection list = DBFunctions.NonBlockingGetAllIDList();
            IDComboBox.ComboBox.DataSource = list;

            int index = IDComboBox.ComboBox.FindString(usingID);
            if (index >= 0)
                IDComboBox.SelectedIndex = index;

            ResetSelectedDB();
        }

        private void SetSelectedDB()
        {
            switch (SelectedDB)
            {
                case _mainDB:
                    DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
                    break;
                case _tempDB:
                    DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);
                    break;
                case _indiaDB:
                    DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP_INDIA);
                    break;
            }
        }

        private void ResetSelectedDB()
        {
            if (SelectedDB == _mainDB)
            {
                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
            }
        }

        private void SetSelectedMode()
        {
            string modeName = "";
            if (!isCopyMode) // Hightlighting/Viewing Mode
            {
                modeName = "Viewing Mode";
                this._idFunctionForm.SetMultiSelect(false);
            }
            else
            {
                modeName = "Copy Mode";
                this._idFunctionForm.SetMultiSelect(true);
            }

             this.ModeChangeBtn.Text = string.Format(
                "Using {0}", modeName);
        }

        /// <summary>
        /// When the selected index in the IDComboBox changes load all the views with the new ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IDComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IDComboBox.SelectedIndex >= 0)
            {
                SetSelectedDB();
                string SelectedID = IDComboBox.Text.ToString();

                IDHeader IDHeaderData = DAOFactory.IDHeader().Select(SelectedID);
                _idLogForm.LoadID(IDHeaderData.LogList);
                _idHeaderForm.LoadID(IDHeaderData);

                _idBrandForm.LoadID(SelectedID);
                _idFunctionForm.LoadID(SelectedID);
                _remotePictureForm.LoadID(SelectedID);

                this.Text = "ID Viewer: " + SelectedID;

                ResetSelectedDB();
            }
        }

        /// <summary>
        /// If an ID was hand entered, clicking on the "view" button will select the
        /// entered ID if it's in the list.  Otherwise does nothing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeIDBtn_Click(object sender, EventArgs e)
        {
            GetIDList(IDComboBox.Text.ToString());
            int index = IDComboBox.ComboBox.FindString(IDComboBox.Text.ToString());
            if (index >= 0)
                IDComboBox.SelectedIndex = index;
        }

        /// <summary>
        /// Change the selected ID to the next id in the IDComboBox list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextIDBtn_Click(object sender, EventArgs e)
        {
            if (IDComboBox.SelectedIndex < IDComboBox.Items.Count - 1)
                IDComboBox.SelectedIndex++;
        }

        /// <summary>
        /// Change the selected ID to the previous id in the IDComboBox list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrevIDBtn_Click(object sender, EventArgs e)
        {
            if (IDComboBox.SelectedIndex > 0)
                IDComboBox.SelectedIndex--;
        }

        /// <summary>
        /// Change the selected ID forward to the first occurance of a mode change in the IDComboBox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextModeBtn_Click(object sender, EventArgs e)
        {
            char Mode = IDComboBox.Text.ToString()[0];
            StringCollection IDArray = (StringCollection)IDComboBox.ComboBox.DataSource;
            int index = IDComboBox.SelectedIndex;

            while (index < IDComboBox.Items.Count - 1)
            {
                if (Mode != IDArray[++index].ToString()[0])
                    break;
            }
            IDComboBox.SelectedIndex = index;
        }

        /// <summary>
        /// Change the selected ID back to the first occurance of a mode change in the IDComboBox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrevModeBtn_Click(object sender, EventArgs e)
        {
            char Mode = IDComboBox.Text.ToString()[0];
            StringCollection IDArray = (StringCollection)IDComboBox.ComboBox.DataSource;
            int index = IDComboBox.SelectedIndex;

            while (index > 0)
            {
                if (Mode != IDArray[--index].ToString()[0])
                    break;
            }
            IDComboBox.SelectedIndex = index;
        }

        #region variables for printing
        private Font printFont;
        private string _PrintPagesWatch;
        #endregion
        /// <summary>
        /// 1st attempt at a print routine for the IDs. 
        /// I'm not happy with this but the object at this point was to get print on paper.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// Modified by Jimmy Pan on 12/29/05 to use a newly added dialog to make user able to
        /// print different sections of the ID 
        private void printToolStripButton_Click(object sender, EventArgs e)
        {
            PrintOptionDlg PrintOption = new PrintOptionDlg();
            DialogResult res = PrintOption.ShowDialog();
            PrintOption.Close();
            if (res == DialogResult.OK)
            {
                _PrintPagesWatch =  PrintOption.PrintLog ? "L" : "";
                _PrintPagesWatch += PrintOption.PrintBM ? "B" : "";
                _PrintPagesWatch += PrintOption.PrintHead ? "H" : "";
                _PrintPagesWatch += PrintOption.PrintFunc ? "F" : "";
            }

            PrintDocument PrintID = new PrintDocument();
            PrintDialog pdlg = new PrintDialog();
            pdlg.Document = PrintID;
            pdlg.AllowPrintToFile = false;

            // Use Portrait mode
            PrintID.DefaultPageSettings.Landscape = false;

            DialogResult result = pdlg.ShowDialog();
            if (result != DialogResult.OK)
                return;
 
            printFont = new Font("Courier New", 8.5f);
            PrintID.DocumentName = IDComboBox.Text.ToString();
            PrintID.PrintPage += new PrintPageEventHandler(this.pid_PagePrint);

            PrintID.Print(); 
         }

        // The PrintPage event is raised for each page to be printed.
        /// <summary>
        /// Break up prints that span multiple pages
        /// I'm not happy with this but the object at this point was to get print on paper.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        /// 
        /// Modified by Jimmy Pan on 12/29/05 to print different part of the MDI document base
        /// on user's selection
        private void pid_PagePrint(object sender, PrintPageEventArgs ev)
        {
            float linesPerPage = 0;
            int count = 0;
            float leftMargin = 10;
            float topMargin = 50;
            string line = null;

            // Calculate the number of lines per page.
            linesPerPage = ev.MarginBounds.Height /
               printFont.GetHeight(ev.Graphics);

            // Print Header of the file
            PrintReportHeaderToGraphics(ev.Graphics, topMargin, leftMargin);
            count++;

            // Print each line of the file.
            while (count < linesPerPage && (line = GetReportLine()) != null)
            {
                float startTopPos = topMargin + count*printFont.GetHeight(ev.Graphics);
                int printedLines = PrintReportLineToGraphics(line, 
                    ev.Graphics, startTopPos,leftMargin);

                count += printedLines;
            }

            // User's selection for pages to print
            SelectPageToPrint(line, ref ev);
        }

        private void SelectPageToPrint(string line, ref PrintPageEventArgs ev)
        {
            if (line == null) // finished printing a part 
            {
                ResetLastLinePrinted();
                if (_PrintPagesWatch.Length > 1)
                {
                    _PrintPagesWatch = _PrintPagesWatch.Remove(0, 1);
                    ev.HasMorePages = true;
                }
                else
                {
                    _PrintPagesWatch = _PrintPagesWatch.Remove(0, 1); 
                    ev.HasMorePages = false;
                }
            }
            else
                ev.HasMorePages = true;
        }

        private void ResetLastLinePrinted()
        {
            switch (_PrintPagesWatch[0])
            { 
                case 'L':
                    _idLogForm.resetLastLinePrinted();
                    break;
                case 'B':
                    _idBrandForm.resetLastLinePrinted();
                    break;
                case 'H':
                    _idHeaderForm.resetLastLinePrinted();
                    break;
                case 'F':
                    _idFunctionForm.resetLastLinePrinted();
                    break;
                default:
                    break;
            }
        }
        private void PrintReportHeaderToGraphics(Graphics g,float TopMargin,
            float leftMargin)
        {
            string head = GetReportHeader();
            g.DrawString(head, printFont, Brushes.Black,
                    leftMargin, TopMargin, new StringFormat());
        }

        private int PrintReportLineToGraphics(string line, Graphics g,
            float startTopPos, float leftMargin)
        {
            StringFormat cellformat = new StringFormat();

            string lineToPrint = line;
            int printedLinesCount = 0;
            float yPos = startTopPos;
            // width of the whole datagridview is about 110 characters
            int maxCharsToPrint = 110;
            
            while (lineToPrint.Length > maxCharsToPrint)
            {
                int ww_index = lineToPrint.Substring(0, maxCharsToPrint).LastIndexOf(' ');
                yPos += printFont.GetHeight(g);
              
                g.DrawString(lineToPrint.Substring(0, ww_index), printFont, Brushes.Black, 
                    leftMargin, yPos, cellformat);

                lineToPrint = lineToPrint.Substring(ww_index+1);
                printedLinesCount++;

                // from the 2nd line, we print only the content field so need
                // to reset leftMargin and maximun number of characters to 
                // print
                leftMargin = 245;
                maxCharsToPrint = 75;
            }

            yPos += printFont.GetHeight(g);
            g.DrawString(lineToPrint, printFont, Brushes.Black, leftMargin,
                yPos, cellformat);
            return ++printedLinesCount;
        }

        /// <summary>
        /// A method to call each section in turn
        /// </summary>
        /// <returns></returns>
        private string GetReportLine()
        {
            string line = null;
            switch(_PrintPagesWatch[0])
            {
                case 'L': 
                    line = _idLogForm.GetReportLine(); 
                    break;
                case 'B': 
                    line = _idBrandForm.GetReportLine(); 
                    break;
                case 'H': 
                    line = _idHeaderForm.GetReportLine(); 
                    break;
                case 'F': 
                    line = _idFunctionForm.GetReportLine(); 
                    break;
                default: break;
            }
            return line;
        }

        private string GetReportHeader()
        {
            string line = null;
            switch (_PrintPagesWatch[0])
            {
                case 'L':
                    line = _idLogForm.GetReportHeader();
                    break;
                case 'B':
                    line = _idBrandForm.GetReportHeader();
                    break;
                case 'H':
                    line = _idHeaderForm.GetReportHeader();
                    break;
                case 'F':
                    line = _idFunctionForm.GetReportHeader();
                    break;
                default: break;
            }
            return line;
        }

        private void CB_DatabaseSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CurrentID = IDComboBox.Text.ToString();
            SelectedDB = CB_DatabaseSelector.SelectedItem.ToString();
            LoadFormView(CurrentID);
        }

        private void IDComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                ChangeIDBtn_Click(sender, e);
        }

        private void mainDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetDefaultDBCheckboxes(_mainDB);
            Registry.SetValue(Reg.PROJ_ROOT, Reg.DEFAULTDB, _mainDB);
        }

        private void editDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetDefaultDBCheckboxes(_tempDB);
            Registry.SetValue(Reg.PROJ_ROOT, Reg.DEFAULTDB, _tempDB);
        }

        private void editDatabaseIndiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetDefaultDBCheckboxes(_indiaDB);
            Registry.SetValue(Reg.PROJ_ROOT, Reg.DEFAULTDB, _indiaDB);
        }

        private void SetDefaultDBCheckboxes(string defaultDatabase)
        {
            mainDatabaseToolStripMenuItem.Checked = defaultDatabase == _mainDB;
            editDatabaseToolStripMenuItem.Checked = defaultDatabase == _tempDB;
            editDatabaseIndiaToolStripMenuItem.Checked = defaultDatabase == _indiaDB;
        }

        private void duplicateIntronsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _idFunctionForm.Show();
            _idFunctionForm.BringToFront();
            _idFunctionForm.HighlightMatchingIntronRows();
        }

        private void ModeChangeBtn_Click(object sender, EventArgs e)
        {
            if (CurrentViewForm != 3) // not function view form
                return;

            isCopyMode = !isCopyMode;
            LoadFormView(ViewID);
        }

        private void aboutIDViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileInfo fileInfo = new FileInfo(assembly.Location);
            DateTime lastModified = fileInfo.LastWriteTime;

            string CopyrightMsg = "Copyright � 2010 Universal Electronics Inc.";
            string VersionString = "ID View BETA - Last Updated " + lastModified.ToString();
            string HelpMessage = VersionString + "\n\n";
            HelpMessage += CopyrightMsg;
            MessageBox.Show(this, HelpMessage, "About ID View", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}