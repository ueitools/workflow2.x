using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;

namespace IDView
{
    public partial class IDLogView : Form
    {
        private int LastLinePrinted;
        
        public IDLogView()
        {
            InitializeComponent();
        }

        public void resetLastLinePrinted()
        {
            LastLinePrinted = 0;
        }

        /// <summary>
        /// Set IDLogData to the GridView DataSource for display
        /// The logData is part of the HeaderData so the database call is done at a higher level
        /// and passed to the Header view but the Log view gets only the LogData
        /// </summary>
        /// <param name="IDLogData">The Log portion of the Header Data</param>
        public void LoadID(LogCollection IDLogData)
        {
            LastLinePrinted = 0;
            logDataGridView.DataSource = IDLogData;
            logDataGridView.Sort(logDataGridView.Columns["Time"],
                                                    ListSortDirection.Ascending);
        }

        /// <summary>
        /// crude report formatter (needs lots of work)
        /// </summary>
        /// <returns>a single formatted string from the screen data</returns>
        public string GetReportLine()
        {
            string data = null;
            int row = LastLinePrinted++;
            if (row >= 0 && row < logDataGridView.Rows.Count)
            {
                Log IDLogData = (Log)logDataGridView.Rows[row].DataBoundItem;
                data = string.Format("{0,-20:g} {1,-10} {2}",
                    IDLogData.Time, 
                    IDLogData.Operator, IDLogData.Content);
            }
            return data;
        }

        public string GetReportHeader()
        {
            string head = string.Format("{0,-20} {1,-10} {2}",
                    "Time",  "Operator", "Content");
            return head;
        }

     }
}