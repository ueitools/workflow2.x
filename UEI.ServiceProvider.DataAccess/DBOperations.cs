﻿using System;
using System.Collections.Generic;
//using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
namespace UEI.ServiceProvider
{
    public class DBOperations : IDisposable
    {
        /// <summary> For checking the Transaction is in process. </summary>
        private bool blnTransactionInProcess = false;

        /// <summary> For checking the Connection status. </summary>
        private bool blnConnectionOpen = false;

        /// <summary> Reference for connection object. </summary>		
        private IDbConnection objConnection;

        /// <summary> Reference for Transaction object. </summary>
        private IDbTransaction objTransaction;

        /// <summary> Hold the Connection string for the database. </summary>
        private string strConnectionString;

        /// <summary> Holds the DBProvider value. value will be sqlClient or oledb. </summary>
        private string strDBProvider;

        /// <summary> holds the starting character for the command parameter. </summary>
        private string strParameterStartsWith;

        /// <summary> 
        /// Default constuctor for DBOperation. 
        /// </summary>
        public DBOperations()
        {
            //strConnectionString =   ConfigurationSettings.AppSettings["ConnectionString"];
            //strDBProvider           = "sqlclient"; //ConfigurationSettings.AppSettings["DBProvider"];
            //strParameterStartsWith  = "@";// ConfigurationSettings.AppSettings["ParameterBeginsWith"];
        }

        public DBOperations(String connectionString)
        {
            strConnectionString = connectionString; //ConfigurationSettings.AppSettings["ConnectionString"];
            strDBProvider = "sqlclient"; //ConfigurationSettings.AppSettings["DBProvider"];
            strParameterStartsWith = "@";// ConfigurationSettings.AppSettings["ParameterBeginsWith"];
        }
        /// <summary> 
        /// Creates a new SQlCommand or OleDbCommand object based on the provider. 
        /// </summary>
        /// <returns> Command object will be returned.</returns>
        public IDbCommand CreateCommand()
        {
            if (strDBProvider.ToLower() == "oledb")
                return new System.Data.OleDb.OleDbCommand();
            else
                return new System.Data.SqlClient.SqlCommand();
        }

        /// <summary> 
        /// Creates a new SQLParameter or OleDBParameter object based on the provider. 
        /// </summary>
        /// <returns> Parameter object will be returned.</returns>
        public object CreateParameter(string strParameterName, DbType cdbType, System.Data.ParameterDirection prmDirection, int intSize, object objValue)
        {
            if (strDBProvider.ToLower() == "oledb")
                return CreateOleParameter(strParameterName, cdbType, prmDirection, intSize, 0, objValue);
            else
                return CreateSQLParameter(strParameterName, cdbType, prmDirection, intSize, 0, objValue);
        }

        /// <summary> 
        /// Creates a new SQLParameter or OleDBParameter object based on the provider. 
        /// </summary>
        /// <returns> Parameter object will be returned.</returns>
        public object CreateParameter(string strParameterName, DbType cdbType, System.Data.ParameterDirection prmDirection, int intSize, byte bytPrecision, object objValue)
        {
            if (strDBProvider.ToLower() == "oledb")
                return CreateOleParameter(strParameterName, cdbType, prmDirection, intSize, bytPrecision, objValue);
            else
                return CreateSQLParameter(strParameterName, cdbType, prmDirection, intSize, bytPrecision, objValue);
        }

        /// <summary> 
        /// Creates a new SQLParameter . 
        /// </summary>
        /// <returns> SqlParameter object will be returned.</returns>
        private System.Data.SqlClient.SqlParameter CreateSQLParameter(string strParameterName, DbType cdbType, System.Data.ParameterDirection prmDirection, int intSize, byte bytPrecision, object objValue)
        {
            System.Data.SqlClient.SqlParameter objSqlParameter = new SqlParameter();
            objSqlParameter.ParameterName = strParameterName;
            objSqlParameter.Direction = prmDirection;
            objSqlParameter.DbType = cdbType;
            if (bytPrecision != 0)
                objSqlParameter.Precision = bytPrecision;
            objSqlParameter.Size = intSize;
            objSqlParameter.Value = objValue;
            return objSqlParameter;
        }

        /// <summary> 
        /// Creates a new OleDbParameter. 
        /// </summary>
        /// <returns> OleDbParameter object will be returned.</returns>
        private System.Data.OleDb.OleDbParameter CreateOleParameter(string strParameterName, DbType cdbType, System.Data.ParameterDirection prmDirection, int intSize, byte bytPrecision, object objValue)
        {
            System.Data.OleDb.OleDbParameter objOleParameter = new System.Data.OleDb.OleDbParameter();
            objOleParameter.ParameterName = strParameterName.Replace("@", strParameterStartsWith);
            objOleParameter.Direction = prmDirection;
            objOleParameter.DbType = cdbType;
            if (bytPrecision != 0)
                objOleParameter.Precision = bytPrecision;
            objOleParameter.Size = intSize;
            objOleParameter.Value = objValue;
            return objOleParameter;
        }

        /// <summary> 
        /// Gets the SQLParameter or OleDBParameter value. 
        /// </summary>
        /// <param name="obj"></param> 
        /// <returns> object will be returned.</returns>
        public object GetParameterValue(object obj)
        {
            if (strDBProvider.ToLower() == "oledb")
                return ((System.Data.OleDb.OleDbParameter)obj).Value;
            else
                return ((System.Data.SqlClient.SqlParameter)obj).Value;
        }

        /// <summary> 
        /// If the Connection is not open the a new Connection object 
        /// will be created and it will be opened. 
        /// </summary>
        private void OpenConnection()
        {
            try
            {
                if (!blnConnectionOpen)
                {
                    if (strDBProvider.ToLower() == "oledb")
                        objConnection = new System.Data.OleDb.OleDbConnection(strConnectionString);
                    else
                        objConnection = new System.Data.SqlClient.SqlConnection(strConnectionString);
                    objConnection.Open();
                    blnConnectionOpen = true;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary> 
        /// If the connection is not closed the connection will be closed. 
        /// also if the Transaction is in process it be Rolledback. 
        /// </summary>
        private void CloseConnection()
        {
            try
            {
                if (blnTransactionInProcess)
                    RollbackTransaction();

                if (blnConnectionOpen)
                    if (objConnection.State == ConnectionState.Open)
                        objConnection.Close();
            }
            catch
            {
                throw;
            }
            finally
            {
                blnConnectionOpen = false;
                blnTransactionInProcess = false;
            }
        }

        /// <summary> 
        /// If the Transaction is not in process then a new Transaction object 
        /// will be created. 
        /// </summary>
        private void BeginTransaction()
        {
            try
            {
                if (!blnTransactionInProcess)
                {
                    if (!blnConnectionOpen)
                        OpenConnection();
                    objTransaction = objConnection.BeginTransaction();
                    blnTransactionInProcess = true;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary> 
        /// If the Transaction is in process then the Transaction will be RolledBack. 
        /// </summary>
        private void RollbackTransaction()
        {
            try
            {
                if (blnTransactionInProcess)
                {
                    objTransaction.Rollback();
                    blnTransactionInProcess = false;
                }
            }
            catch
            {
                CloseConnection();
                throw;
            }
        }

        /// <summary> 
        /// If the Transaction is in process then the Transaction will commited. 
        /// </summary>
        private void CommitTransaction()
        {
            try
            {
                if (blnTransactionInProcess)
                {
                    objTransaction.Commit();
                    blnTransactionInProcess = false;
                }
            }
            catch
            {
                CloseConnection();
                throw;
            }
        }

        /// <summary>
        /// Executes a Transaction for a command object. If the Transaction has 
        /// already begun then begin Transaction wont be called.
        /// </summary>
        /// <param name="objCommand">Command object.</param>
        /// <returns>rows affected.</returns>
        public int ExecuteTransaction(IDbCommand objCommand)
        {
            int intRowAffected = 0;

            try
            {
                if (!blnTransactionInProcess)
                    BeginTransaction();

                objCommand.Connection = objConnection;
                objCommand.Transaction = objTransaction;
                intRowAffected = objCommand.ExecuteNonQuery();
                return intRowAffected;
            }
            catch
            {
                RollbackTransaction();
                CloseConnection();
                throw;
            }
        }

        /// <summary>
        /// Executes a Transaction for a command object. If the CommitTrans is equal to true 
        /// then the Transaction will be commited.
        /// </summary>
        /// <param name="objCommand">Command object.</param>
        /// <param name="CommitTrans">boolean variable.</param>
        /// <returns>rows affected</returns>
        public int ExecuteTransaction(IDbCommand objCommand, bool CommitTrans)
        {
            int intRowAffected = 0;

            try
            {
                intRowAffected = ExecuteTransaction(objCommand);

                if (CommitTrans)
                    CommitTransaction();

                return intRowAffected;
            }
            catch
            {
                RollbackTransaction();
                CloseConnection();
                throw;
            }
        }

        /// <summary>
        /// Executes a command object which has reference for stored procedure. 
        /// </summary>
        /// <param name="objCommand">Command object.</param>
        /// <returns>rows affcted.</returns>
        public int ExecuteSP(IDbCommand objCommand)
        {
            int intRowAffected = 0;

            try
            {
                if (!blnConnectionOpen)
                    OpenConnection();

                objCommand.Connection = objConnection;
                objCommand.CommandType = CommandType.StoredProcedure;
                intRowAffected = objCommand.ExecuteNonQuery();
                return intRowAffected;
            }
            catch
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        /// <summary>
        /// Executes a command object which has reference for stored procedure. If
        /// the KeepConnectionOpen is true then the connection wont be closed. 
        /// </summary>
        /// <param name="objCommand">Command object.</param>
        /// <param name="KeepConnectionOpen">bool</param> 
        /// <returns>rows affcted.</returns>
        public int ExecuteSP(IDbCommand objCommand, bool KeepConnectionOpen)
        {
            try
            {
                return ExecuteSP(objCommand);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (!KeepConnectionOpen)
                    CloseConnection();
            }
        }

        /// <summary>
        /// Executes a Simple Text Query.
        /// </summary>
        /// <param name="objCommand">Command object</param>
        /// <returns>rows affected.</returns>
        public int ExecuteTextQuery(IDbCommand objCommand)
        {
            int intRowAffected = 0;

            try
            {
                if (!blnConnectionOpen)
                    OpenConnection();

                objCommand.Connection = objConnection;
                objCommand.CommandType = CommandType.Text;
                intRowAffected = objCommand.ExecuteNonQuery();
                return intRowAffected;
            }
            catch
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        /// <summary>
        /// Executes a Simple Text Query. 
        /// If KeepConnectionOpen is true then connection wont be closed.
        /// </summary>
        /// <param name="objCommand">Command object</param>
        /// <param name="KeepConnectionOpen">bool</param>
        /// <returns>rows affected.</returns>
        public int ExecuteTextQuery(IDbCommand objCommand, bool KeepConnectionOpen)
        {
            try
            {
                return ExecuteTextQuery(objCommand);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (!KeepConnectionOpen)
                    CloseConnection();
            }
        }

        /// <summary>
        /// Executes a command object which has reference for a stored procedure.
        /// </summary>
        /// <param name="objCommand">Command object</param>
        /// <returns>SqlReader or OleDBReader will be returned based on provider.</returns>
        public IDataReader ExecuteSPReader(IDbCommand objCommand)
        {
            try
            {
                IDataReader objReader;
                OpenConnection();
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.Connection = objConnection;
                objReader = objCommand.ExecuteReader();
                return objReader;
            }
            catch
            {
                CloseConnection();
                throw;
            }
        }

        /// <summary>
        /// Executes a Simple Text Query (TextReader).
        /// </summary>
        /// <param name="objCommand">Commmand object.</param>
        /// <returns>SqlReader or OleDBReader will be returned based on provider.</returns>
        public IDataReader ExecuteTextReader(IDbCommand objCommand)
        {
            try
            {
                IDataReader objReader;
                OpenConnection();
                objCommand.CommandType = CommandType.Text;
                objCommand.Connection = objConnection;
                if (blnTransactionInProcess)
                    objCommand.Transaction = objTransaction;
                objReader = objCommand.ExecuteReader();
                return objReader;
            }
            catch
            {
                CloseConnection();
                throw;
            }
        }

        /// <summary>
        /// Executes a commmand object and DataSet will be returned.
        /// </summary>
        /// <param name="objCommand">Command object.</param>
        /// <returns>Dataset</returns>
        public DataSet ExecuteDataSet(IDbCommand objCommand)
        {
            try
            {
                DataSet objDS = new DataSet();
                OpenConnection();
                objCommand.Connection = objConnection;
                IDataAdapter objDataAdapter;
                if (strDBProvider.ToLower() == "oledb")
                    objDataAdapter = new System.Data.OleDb.OleDbDataAdapter((System.Data.OleDb.OleDbCommand)objCommand);
                else
                    objDataAdapter = new System.Data.SqlClient.SqlDataAdapter((System.Data.SqlClient.SqlCommand)objCommand);
                objDataAdapter.Fill(objDS);
                return objDS;
            }
            catch
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        /// <summary>
        /// Executes a commmand object and Datatable will be returned.
        /// </summary>
        /// <param name="objCommand">Command object.</param>
        /// <returns>DataTable</returns>
        public DataTable ExecuteDataTable(IDbCommand objCommand)
        {
            try
            {
                DataTable objDT = new DataTable();
                OpenConnection();
                objCommand.Connection = objConnection;
                IDataAdapter objDataAdapter;
                if (strDBProvider.ToLower() == "oledb")
                {
                    objDataAdapter = new System.Data.OleDb.OleDbDataAdapter((System.Data.OleDb.OleDbCommand)objCommand);
                    ((System.Data.OleDb.OleDbDataAdapter)objDataAdapter).Fill(objDT);
                }
                else
                {
                    objDataAdapter = new System.Data.SqlClient.SqlDataAdapter((System.Data.SqlClient.SqlCommand)objCommand);
                    ((System.Data.SqlClient.SqlDataAdapter)objDataAdapter).Fill(objDT);
                }
                return objDT;
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Executes a Command Object and returns DataTable
        /// </summary>
        /// <param name="objCommand">Command Objects</param>
        /// <param name="intStartRecord">Starting Record Searial No</param>
        /// <param name="intNoOfRecords">No Of Records</param>
        /// <returns></returns>

        public DataTable ExecuteDataTable(IDbCommand objCommand, int intStartRecord, int intNoOfRecords)
        {
            try
            {
                DataTable objDT = new DataTable();
                OpenConnection();
                objCommand.Connection = objConnection;
                IDataAdapter objDataAdapter;
                System.Data.DataSet objDs = null;
                if (strDBProvider.ToLower() == "oledb")
                {
                    objDataAdapter = new System.Data.OleDb.OleDbDataAdapter((System.Data.OleDb.OleDbCommand)objCommand);
                    ((System.Data.OleDb.OleDbDataAdapter)objDataAdapter).Fill(objDT);
                }
                else
                {
                    objDataAdapter = new System.Data.SqlClient.SqlDataAdapter((System.Data.SqlClient.SqlCommand)objCommand);
                    objDs = new System.Data.DataSet();
                    ((System.Data.SqlClient.SqlDataAdapter)objDataAdapter).Fill(objDs, intStartRecord, intNoOfRecords, "xyz");
                    objDT = objDs.Tables[0];
                }
                return objDT;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Executes a commmand object and DataTable  schema will be returned.
        /// </summary>
        /// <param name="objCommand">Command Object.</param>
        /// <returns>DataTable</returns>
        public void ExecuteDataTableSchema(IDbCommand objCommand, DataTable dt)
        {
            try
            {
                OpenConnection();
                objCommand.Connection = objConnection;
                IDataAdapter objDataAdapter;
                if (strDBProvider.ToLower() == "oledb")
                {
                    objDataAdapter = new System.Data.OleDb.OleDbDataAdapter((System.Data.OleDb.OleDbCommand)objCommand);
                    ((System.Data.OleDb.OleDbDataAdapter)objDataAdapter).FillSchema(dt, SchemaType.Source);
                }
                else
                {
                    objDataAdapter = new System.Data.SqlClient.SqlDataAdapter((System.Data.SqlClient.SqlCommand)objCommand);
                    ((System.Data.SqlClient.SqlDataAdapter)objDataAdapter).FillSchema(dt, SchemaType.Source);
                }
                //return dt;
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Closes The Connection.
        /// </summary>		
        public void Dispose()
        {
            CloseConnection();
        }

        /// <summary>
        /// Closes The Connection.
        /// </summary>		
        public void Dispose(bool blnClose)
        {
            if (blnClose)
            {
                CloseConnection();
            }
        }
    }
}