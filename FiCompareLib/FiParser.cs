using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
namespace FiCompareLib
{
    public class FiParser
    {
        FrameElements RawFrameelements = null;
        internal class FrameElements
        {
            public List<Element> StartframeElements;
            public List<Element> RepeatFrameElements;
            public List<Element> EndofFrameElements;
        }
        public FiParser()
        {
            RawFrameelements = new FrameElements();
            FiInfo = new List<Element>();
            Mark_tolerance = 0.06;
            Space_tolerance = 0.06;
        }
        #region Properties
        private SignalFrame _fisignalframes;
        public SignalFrame FiSignalFrames
        {
            get { return _fisignalframes; }
            set { _fisignalframes = value; }
        }
        private List<Element> _FiInfo;
        /// <summary>
        /// Data Structure to store Fi Signal
        /// </summary>
        public List<Element> FiInfo
        {
            get { return _FiInfo; }
            set { _FiInfo = value; }
        }
        
        private double _mark_tolerance;
        /// <summary>
        /// Tolerance value for Mark
        /// </summary>
        public double Mark_tolerance
{
  get { return _mark_tolerance; }
  set { _mark_tolerance = value; }
}
        
        private double _space_tolerance;
        /// <summary>
        /// Tolerance value for Space
        /// </summary>
        public double Space_tolerance
        {
            get { return _space_tolerance; }
            set { _space_tolerance = value; }
        }
        private Dictionary<int, int> _unique_MarksList;
        //private List<int> _unique_MarksList;
        /// <summary>
        /// List of unique Mark Values
        /// </summary>
        public Dictionary<int, int> Unique_MarksList
        {
            get { return _unique_MarksList; }
            set { _unique_MarksList = value; }
        }
        private Dictionary<int, int> _unique_SpacesList;
        //private List<int> _unique_SpacesList;
        /// <summary>
        /// List of Unique Space Values
        /// </summary>
        public Dictionary<int, int> Unique_SpacesList
        {
            get { return _unique_SpacesList; }
            set { _unique_SpacesList = value; }
        }
        private int _codegapValue;
        /// <summary>
        /// The frame seperator value ie. Codegap value
        /// </summary>
        public int CodegapValue
        {
            get { return _codegapValue; }
            set { _codegapValue = value; }
        }
        #endregion
                
        #region Parse and Create Fi Signal Information Structure
        /// <summary>
        /// Reads Fi File and creates a datastructure of Fi
        /// </summary>
        /// <param name="FiFilePath"></param>
        public void GetProcessedFiInfo(string FiFilePath)
        {
            FiInfo = new List<Element>();
            PaseFiFile(FiFilePath);
            if (RawFrameelements.StartframeElements.Count > 0)
            {
                CreateFiInfo(RawFrameelements.StartframeElements);
            }
            if (RawFrameelements.RepeatFrameElements.Count > 0)
            {
                CreateFiInfo(RawFrameelements.RepeatFrameElements);
            }
            if (RawFrameelements.EndofFrameElements.Count > 0)
            {
                CreateFiInfo(RawFrameelements.EndofFrameElements);
            }
       }
        public void GetProcessedFiInfoWithFrames(string FiFilepath)
        {
            GetProcessedFiInfo(FiFilepath);
            CreateFrameElements(FiInfo);
            for (int framecounter = 0; framecounter < FiSignalFrames.SignalFrames.Count; framecounter++)
            {
                RemoveStartSpaceFromFrame(FiSignalFrames.SignalFrames[framecounter]);
            }
        }
        private void PaseFiFile(string FiFilePath)
        {
            Element e = new Element();
            RawFrameelements.StartframeElements = new List<Element>();
            RawFrameelements.RepeatFrameElements = new List<Element>();
            RawFrameelements.EndofFrameElements = new List<Element>();
            string linedata = string.Empty;
            char[] sep ={ ' ' };
            string[] data = null;
            using (StreamReader sr = new StreamReader(FiFilePath))
            {
                while (!sr.EndOfStream)
                {
                    linedata = sr.ReadLine();
                    if (!linedata.Contains("[SOF R]"))
                    {
                        data = linedata.Split(sep);
                        if (data.Length == 3)
                        {
                            e = new Element();
                            e.Frequency = Convert.ToDouble(data[0]);
                            e.Duration = Convert.ToInt32(data[1]);
                            e.Duty = Convert.ToInt16(data[2]);
                            RawFrameelements.StartframeElements.Add(e);
                        }
                    }
                    else if (linedata.Contains("[SOF R]"))
                    {
                        while (!linedata.Contains("[EOF R]"))
                        {
                            data = linedata.Split(sep);
                            if (data.Length == 3)
                            {
                                e = new Element();
                                e.Frequency = Convert.ToDouble(data[0]);
                                e.Duration = Convert.ToInt32(data[1]);
                                e.Duty = Convert.ToInt16(data[2]);
                                RawFrameelements.RepeatFrameElements.Add(e);
                            }
                            linedata = sr.ReadLine();
                        }
                        if (linedata.Contains("[EOF R]"))
                        {
                            while (linedata != null)
                            {
                                data = linedata.Split(sep);
                                if (data.Length == 3)
                                {
                                    e = new Element();
                                    e.Frequency = Convert.ToDouble(data[0]);
                                    e.Duration = Convert.ToInt32(data[1]);
                                    e.Duty = Convert.ToInt16(data[2]);
                                    RawFrameelements.EndofFrameElements.Add(e);
                                }
                                linedata = sr.ReadLine();
                            }
                        }
                    }
                }
            }
        }
        private List<Element> NormalizeFrameElements(List<Element> e)
        {
            Nullable<double> PrevFreqElement = null;
            Nullable<int> PrevDuty = null;
            Element _element = new Element();
            List<Element> NewFrameElements = new List<Element>();
            Element newelement = new Element();
            foreach (Element tempE in e)
            {
                if (PrevFreqElement == null)
                {
                    _element = new Element();
                    _element = tempE;
                    PrevFreqElement = tempE.Frequency;
                    PrevDuty = tempE.Duty;
                }
                else if (PrevFreqElement != tempE.Frequency)
                {
                    //if ((PrevFreqElement == 0.0 && tempE.Frequency == 1.0) || (PrevFreqElement == 1.0 && tempE.Frequency == 0.0))
                    //{
                    //    _element.Frequency = (_element.Frequency + tempE.Frequency) / 2;
                    //    _element.Duration += tempE.Duration;
                    //    _element.Duty = (_element.Duty + tempE.Duty) / 2;
                    //}
                    //else
                    //{
                        newelement = _element;
                        NewFrameElements.Add(newelement);
                        newelement = new Element();
                        _element = tempE;
                        PrevFreqElement = tempE.Frequency;
                        PrevDuty = tempE.Duty;
                    //}
                }
                else if (PrevFreqElement == tempE.Frequency)
                {
                    if (PrevDuty == tempE.Duty )
                    {
                        _element.Frequency = (_element.Frequency + tempE.Frequency) / 2;
                        _element.Duration += tempE.Duration;
                        _element.Duty = (_element.Duty + tempE.Duty) / 2;
                    }
                    else
                    {
                        newelement = _element;
                        NewFrameElements.Add(newelement);
                        newelement = new Element();
                        _element = tempE;
                        PrevFreqElement = tempE.Frequency;
                        PrevDuty = tempE.Duty;
                    }
                }
            }
            if (_element != null)
            {
                NewFrameElements.Add(_element);
                                
            }
            return NewFrameElements;
        }
        private void CreateFiInfo(List<Element> _Elements)
        {
            List<Element> tempElements = new List<Element>();
            tempElements = NormalizeFrameElements(_Elements);
            foreach (Element e in tempElements)
            {
                FiInfo.Add(e);
            }
            
            
        }
        #endregion
        
        #region Creates Frame Elements from an Fi Signal
        /// <summary>
        /// Create Unique Frame Elements from an Fi Signal
        /// </summary>
        /// <param name="FiSignal"></param>
        public void CreateFrameElements(List<Element> FiSignal)
        {
            FiSignalFrames = new SignalFrame();
            List<Element> frameelements = new List<Element>();
            FiSignalFrames.SignalFrames = new List<List<Element>>();
            RemoveStartSpaceFromStartFrame();
            GetAllUniqueMarks(FiSignal);
            GetAllUniqueSpaces(FiSignal);
            GetCodeGapValue(Unique_SpacesList);
            Int32 positivetolerance, negativetolerance;
            positivetolerance = (Int32)((Space_tolerance * CodegapValue) + CodegapValue);
            negativetolerance=(Int32)(CodegapValue-(Space_tolerance*CodegapValue));
            foreach (Element e in FiSignal)
            {
                if ((e.Duration>=negativetolerance)&&(e.Duration<=positivetolerance))
                {
                    frameelements.Add(e);
                    FiSignalFrames.SignalFrames.Add(frameelements);
                    frameelements = new List<Element>();
                }
                else
                {
                    frameelements.Add(e);
                }
            }
            
        }
        private int GetCodeGapValue(Dictionary<int,int> Values)
        {
            int length = Values.Count;
            int counter = 0;
            bool isValid = false;
            int tempVal=0;
            int actualval=0;
            List<int> Spacevals = new List<int>();
            Dictionary<int, int>.KeyCollection _uniquespace = Values.Keys;

            
            foreach (int spaceval in _uniquespace)
            {
                Spacevals.Add(spaceval);
            }

            Values.TryGetValue(Spacevals[length-1], out tempVal);
            
            if (tempVal >= 1)
                {
                CodegapValue = Spacevals[length - 1];
            }
            else if (length - 2 > 1)
            {
                CodegapValue = Spacevals[length - 2];
            }
            else
            {
                CodegapValue = Spacevals[length - 1];
            }
            
            

            //for (int count = length - 1; count >= 0; count--)
            //{
            //    Values.TryGetValue(Spacevals[count], out tempVal);
            //    if (tempVal > 1)
            //    {
            //        CodegapValue = Spacevals[count];
            //        break;
            //    }
                
            //}
            
            //foreach (KeyValuePair<int, int> data in Values)
            //{
            //    //if (counter == (length-1) && data.Value == 1)
            //    //{
            //    //    isValid = false;
            //    //    tempVal = data.Key;
            //    //}
            //    //else if (counter == length - 2)
            //    //{
            //    //    actualval = data.Key; 
            //    //}
            //    //else
            //    //{
            //    //    isValid = true;
            //    //    tempVal = data.Key;
            //    //}
            //    //counter++;

            //    if (data.Value > 1)
            //    {
            //        CodegapValue = data.Key;
            //        break;
            //    }
            //}
            ////if (isValid == true)
            ////{
            ////    CodegapValue = tempVal;
            ////}
            ////else
            ////{
            ////    CodegapValue=actualval;
                
            ////}
            
            return CodegapValue;
        }
        #endregion
        #region Extract Unique Marks and Space values from an Fi Signal
        private void GetAllUniqueSpaces(List<Element> FiSignal)
        {
            List<Element> Spaces = FiSignal.FindAll(FindSpace);
            List<int> UniqueSpaces = new List<int>();
            Spaces.Sort(CompareValues);
            Unique_SpacesList = new Dictionary<int,int>();
            Unique_SpacesList = ExtractUniqueValues(Spaces, Space_tolerance);
            
        }
        private void GetAllUniqueMarks(List<Element> FiSignal)
        {
            List<Element> Marks = FiSignal.FindAll(FindMark);
            Marks.Sort(CompareValues);
            Unique_MarksList = new Dictionary<int, int>();
            Unique_MarksList = ExtractUniqueValues(Marks, Mark_tolerance);
        }
        #endregion
                
        #region Get the average Frequency and Duty Cycle
        public double GetAverageFrequency(List<Element> FiSignal)
        {
            double AverageFrequency = 0.0;
            int FrequencyElements = 0;
            double FrequencySum = 0.0;
            foreach (Element e in FiSignal)
            {
                //if ((e.Frequency == 0.0 && e.Duty > 0) || (e.Frequency == 1.0 && e.Duty > 0))
                if (e.Duty > 0.0 && e.Frequency > 1.0)
                {
                    FrequencyElements++;
                    FrequencySum += e.Frequency;
                }
            }
            if (FrequencyElements > 0)
            {
                AverageFrequency = FrequencySum / FrequencyElements;
            }
            return AverageFrequency;
        }
        public int GetAverageDutyCycle(List<Element> FiSignal)
        {
            int AverageDuty = 0;
            int DutyElements = 0;
            int DutySum = 0;
            foreach (Element e in FiSignal)
            {
                if (e.Duty>0)
                {
                    DutyElements++;
                    DutySum += e.Duty;
                }
            }
            if (DutyElements > 0)
            {
                AverageDuty = DutySum / DutyElements;
            }
            return AverageDuty;
        }
        #endregion
        #region Finds a Mark or space element from an Fi Signal
        private bool FindMark(Element e)
        {
            //Normally mark values begin with a non-zero value, but there are cases
            //where mark values start with a zero value in cuch case the duration is a non-zero value
            if ((e.Frequency >= 0.0)&&(e.Duty > 0))
                return true;
            else
                return false;
        }
        private bool FindSpace(Element e)
        {
            if ((e.Frequency ==0.0 && e.Duty==0)||(e.Frequency ==1.0 && e.Duty==0))
                return true;
            else
                return false;
        }
        #endregion
        #region Miscelleneous Functions
        private int CompareValues(Element e1, Element e2)
        {
            if (e1.Duration > e2.Duration)
                return 1;
            else if (e1.Duration == e2.Duration)
                return 0;
            else
                return -1;
        }
        private Dictionary<int, int> ExtractUniqueValues(List<Element> elements, double tolerance)
        {
            Dictionary<int,int> uniqueelements = new Dictionary<int,int>();
            int prevelement = 0;
            int counter = 0;
            int positivetolerance, negativetolerance;
            foreach (Element e in elements)
            {
                positivetolerance = (int)((tolerance * e.Duration) + e.Duration);
                negativetolerance = (int)(e.Duration - (tolerance * e.Duration));
                if (prevelement == 0)
                {
                    prevelement = e.Duration;
                    counter++;
                    
                }
                else if (prevelement == e.Duration)
                {
                    prevelement = e.Duration;
                    counter++;
                }
                else if ((prevelement != e.Duration))
                {
                    if (prevelement >= negativetolerance && prevelement <= positivetolerance)
                    {
                        prevelement = e.Duration;
                        counter++;
                    }
                    else
                    {
                        uniqueelements.Add(prevelement, counter);
                        prevelement = e.Duration;
                        
                        counter = 1;
                        
                    }
                }
            }
            if (prevelement != 0)
            {
                uniqueelements.Add(prevelement, counter);
            }
            return uniqueelements;
        }
        private void RemoveStartSpaceFromStartFrame()
        {
            if ((FiInfo[0].Frequency == 0.0 && FiInfo[0].Duty == 0) || (FiInfo[0].Frequency == 1.0 && FiInfo[0].Duty == 0))
            {
                FiInfo.RemoveAt(0);
            }
        }

        private List<Element> RemoveStartSpaceFromFrame(List<Element> elements)
        {
            if ((elements[0].Frequency == 0.0 && elements[0].Duty == 0) || (elements[0].Frequency == 1.0 && elements[0].Duty == 0))
            {
                elements.RemoveAt(0);
            }
            return elements;
        }
        #endregion
        
    }
        #region Data Structure to hold Frame and Signal Elements
    /// <summary>
    /// Data Structure that holds the element values that contain Frequency, Duration & Duty Cycle Values
    /// </summary>
    public class Element
    {
        public double Frequency;
        public int Duration;
        public int Duty;
    }
    /// <summary>
    /// Signal Frame that holds all the frame elements
    /// </summary>
    public class SignalFrame
    {
        public List<List<Element>> SignalFrames;
    }
    #endregion
}
