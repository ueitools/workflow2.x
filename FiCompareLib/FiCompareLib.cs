using System;
using System.Collections.Generic;
using System.Text;
using CommonForms;

namespace FiCompareLib
{
    public class FiCompareLib
    {
        private readonly StringBuilder outputstream;
        
        #region Properties

        private double _dutytolerance;
        private double _frequencytolerance;
        private double _marktolerance;
        private double _spacetolerance;
        private bool _bCmpFrames;

        public double Frequencytolerance
        {
            get { return _frequencytolerance; }
            set { _frequencytolerance = value; }
        }

        public double Dutytolerance
        {
            get { return _dutytolerance; }
            set { _dutytolerance = value; }
        }

        public double Marktolerance
        {
            get { return _marktolerance; }
            set { _marktolerance = value; }
        }

        public double Spacetolerance
        {
            get { return _spacetolerance; }
            set { _spacetolerance = value; }
        }

        #endregion

        #region Public Methods

        public bool DoCompare(FiParser Tx, FiParser Rx, out List<string> warningMessages, out List<string> errorMessages)
        {
            warningMessages = new List<string>();
            errorMessages = new List<string>();
            bool bMatched = false;

            double TxFrequency = Tx.GetAverageFrequency(Tx.FiInfo);
            double TxDuty = Tx.GetAverageDutyCycle(Tx.FiInfo);
            double RxFrequency = Rx.GetAverageFrequency(Rx.FiInfo);
            double RxDuty = Rx.GetAverageDutyCycle(Rx.FiInfo);
            CompareFrequency(TxFrequency, RxFrequency, ref errorMessages);

            bool ChkDutyCycle = Convert.ToBoolean(UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CMPDUTYCHKBOX, 0));
            if(ChkDutyCycle)
                CompareDutyCycle(TxDuty, RxDuty, ref errorMessages);

            bMatched = CompareFrameElements(Tx.FiSignalFrames, Rx.FiSignalFrames, ref errorMessages);

            return bMatched;
        }

        #endregion

        #region Private Methods

        private void CompareFrequency(double tx, double rx, ref List<string> errorMessages)
        {
            double deviation = CalculateDev(tx, rx, Frequencytolerance);
            if (deviation > 0)
            {
                errorMessages.Add(String.Format(
                       "Frequency not matched:  "
                       + "Frequency(Capture)= {0:00.00}, "
                       + "Frequency(DB)= {1:00.00}, dev = {2:0.00} "
                       + " > {3:0.00}", rx, tx, deviation, Frequencytolerance));
            
            }
        }

        private void CompareDutyCycle(double tx, double rx, ref List<string> errorMessages)
        {
            double deviation = CalculateDev(tx, rx, Dutytolerance);
            if (deviation > 0)
            {
                errorMessages.Add(String.Format(
                       "Duty Cycle not matched:  "
                       + "Duty Cycle(Capture)= {0:00.00}, "
                       + "Duty Cycle(DB)= {1:00.00}, dev = {2:0.00} "
                       + " > {3:0.00}", rx, tx, deviation, Dutytolerance));

            }
        }


        public double CalculateDev(double tx, double rx,double dTolerance)
        {
            double diff = Math.Abs(rx - tx);
            double min = Math.Min(rx, tx);

            if (diff > dTolerance * min)
            {
                if (min == 0)
                    return double.MaxValue;

                return diff / min;
            }

            return 0;
        }

        private bool CompareFrameElements(SignalFrame tx, SignalFrame rx, ref List<string> errorMessages)
        {
            SignalFrame src = new SignalFrame();
            SignalFrame trg = new SignalFrame();
            bool bMatched = true;
            double positivetolerance;
            double negativetolerance;
            double deviation;
            try
            {

                src = tx;
                trg = rx;

                if (_bCmpFrames == false)
                {
                    int nLastFrame = tx.SignalFrames.Count;
                    if (rx.SignalFrames.Count < nLastFrame)
                    {
                        nLastFrame = rx.SignalFrames.Count;
                    }

                    List<Element> srcframe = new List<Element>();
                    List<Element> trgframe = new List<Element>();

                    #region Comparing First Frame
                    {
                        int framecounter = 0;
                        if (src.SignalFrames[framecounter].Count != trg.SignalFrames[framecounter].Count)
                        {
                            if (src.SignalFrames[framecounter].Count > trg.SignalFrames[framecounter].Count)
                            {
                                srcframe = trg.SignalFrames[framecounter];
                                trgframe = src.SignalFrames[framecounter];
                            }
                            else
                            {
                                srcframe = src.SignalFrames[framecounter];
                                trgframe = trg.SignalFrames[framecounter];
                            }                            
                        }
                        else
                        {
                            srcframe = src.SignalFrames[framecounter];
                            trgframe = trg.SignalFrames[framecounter];
                        }

                        for (int elementcounter = 0; elementcounter < srcframe.Count -1; elementcounter++)
                        {
                            if (src.SignalFrames[framecounter][elementcounter].Frequency == 0)
                            {
                                positivetolerance = srcframe[elementcounter].Duration +
                                                    (Spacetolerance * srcframe[elementcounter].Duration);
                                negativetolerance = srcframe[elementcounter].Duration -
                                                    (Spacetolerance * srcframe[elementcounter].Duration);
                                if (trgframe[elementcounter].Duration >= negativetolerance &&
                                    trgframe[elementcounter].Duration <= positivetolerance)
                                {
                                }
                                else
                                {
                                    deviation =
                                        Math.Abs(src.SignalFrames[framecounter][elementcounter].Duration -
                                                 trg.SignalFrames[framecounter][elementcounter].Duration);
                                    deviation = ((100.00 * deviation) / src.SignalFrames[framecounter][elementcounter].Duration);
                                    outputstream.AppendLine(
                                        string.Format(
                                            "Timing Mismatch at Frame {0}, Element {1} [DB] Space= {2}, [Capture] Space= {3} Deviation {4}% exceeds {5}%",
                                            framecounter, elementcounter,
                                            src.SignalFrames[framecounter][elementcounter].Duration,
                                            trg.SignalFrames[framecounter][elementcounter].Duration, deviation,
                                            Spacetolerance * 100));
                                    errorMessages.Add(
                                        string.Format(
                                            "Timing Mismatch at Frame {0}, Element {1} [DB] Space= {2}, [Capture] Space= {3} Deviation {4}% exceeds {5}%",
                                            framecounter + 1, elementcounter + 1,
                                            src.SignalFrames[framecounter][elementcounter].Duration,
                                            trg.SignalFrames[framecounter][elementcounter].Duration, deviation,
                                            Spacetolerance * 100));
                                    //bMatched = false;
                                }
                            }
                            else
                            {
                                positivetolerance = src.SignalFrames[framecounter][elementcounter].Duration +
                                                    (Marktolerance * src.SignalFrames[framecounter][elementcounter].Duration);
                                negativetolerance = src.SignalFrames[framecounter][elementcounter].Duration -
                                                    (Marktolerance * src.SignalFrames[framecounter][elementcounter].Duration);
                                if (trgframe[elementcounter].Duration >= negativetolerance &&
                                    trgframe[elementcounter].Duration <= positivetolerance)
                                {
                                }
                                else
                                {
                                    deviation =
                                        Math.Abs(src.SignalFrames[framecounter][elementcounter].Duration -
                                                 trg.SignalFrames[framecounter][elementcounter].Duration);
                                    deviation = ((100.00 * deviation) / src.SignalFrames[framecounter][elementcounter].Duration);
                                    outputstream.AppendLine(
                                        string.Format(
                                            "Timing Mismatch at Frame {0}, Element {1} [DB] Space= {2}, [Capture] Space= {3} Deviation {4}% exceeds {5}%",
                                            framecounter, elementcounter,
                                            src.SignalFrames[framecounter][elementcounter].Duration,
                                            trg.SignalFrames[framecounter][elementcounter].Duration, deviation,
                                            Marktolerance * 100));
                                    errorMessages.Add(
                                        string.Format(
                                            "Timing Mismatch at Frame {0}, Element {1} [DB] Space= {2}, [Capture] Space= {3} Deviation {4}% exceeds {5}%",
                                            framecounter + 1, elementcounter + 1,
                                            src.SignalFrames[framecounter][elementcounter].Duration,
                                            trg.SignalFrames[framecounter][elementcounter].Duration, deviation,
                                            Marktolerance * 100));
                                    //bMatched = false;
                                }
                            }
                        }
                    }
                    #endregion
                    if (src.SignalFrames.Count > 1 && trg.SignalFrames.Count > 1)
                    {
                        #region Comparing End or Last Frame
                        {
                            int srcFrameCounter = src.SignalFrames.Count - 1;
                            int trgFrameCounter = trg.SignalFrames.Count - 1;

                            srcframe = src.SignalFrames[srcFrameCounter];
                            trgframe = trg.SignalFrames[trgFrameCounter];


                            for (int elementcounter = 0; elementcounter < srcframe.Count - 1; elementcounter++)
                            {
                                //srcframe.Count - 1 is to avoid last copdegap comparision
                                if (src.SignalFrames[srcFrameCounter][elementcounter].Frequency == 0)
                                {
                                    positivetolerance = srcframe[elementcounter].Duration +
                                                        (Spacetolerance * srcframe[elementcounter].Duration);
                                    negativetolerance = srcframe[elementcounter].Duration -
                                                        (Spacetolerance * srcframe[elementcounter].Duration);
                                    if (trgframe[elementcounter].Duration >= negativetolerance &&
                                        trgframe[elementcounter].Duration <= positivetolerance)
                                    {
                                    }
                                    else
                                    {
                                        deviation =
                                            Math.Abs(src.SignalFrames[srcFrameCounter][elementcounter].Duration -
                                                     trg.SignalFrames[trgFrameCounter][elementcounter].Duration);
                                        deviation = ((100.00 * deviation) / src.SignalFrames[srcFrameCounter][elementcounter].Duration);
                                        outputstream.AppendLine(
                                            string.Format(
                                                "Timing Mismatch at Frame {0}, Element {1} [DB] Space= {2}, [Capture] Space= {3} Deviation {4}% exceeds {5}%",
                                                srcFrameCounter, elementcounter,
                                                src.SignalFrames[srcFrameCounter][elementcounter].Duration,
                                                trg.SignalFrames[trgFrameCounter][elementcounter].Duration, deviation,
                                                Spacetolerance * 100));
                                        errorMessages.Add(
                                            string.Format(
                                                "Timing Mismatch at Frame {0}, Element {1} [DB] Space= {2}, [Capture] Space= {3} Deviation {4}% exceeds {5}%",
                                                srcFrameCounter + 1, elementcounter + 1,
                                                src.SignalFrames[srcFrameCounter][elementcounter].Duration,
                                                trg.SignalFrames[trgFrameCounter][elementcounter].Duration, deviation,
                                                Spacetolerance * 100));
                                        //bMatched = false;
                                    }
                                }
                                else
                                {
                                    positivetolerance = src.SignalFrames[srcFrameCounter][elementcounter].Duration +
                                                        (Marktolerance * src.SignalFrames[srcFrameCounter][elementcounter].Duration);
                                    negativetolerance = src.SignalFrames[srcFrameCounter][elementcounter].Duration -
                                                        (Marktolerance * src.SignalFrames[srcFrameCounter][elementcounter].Duration);
                                    if (trgframe[elementcounter].Duration >= negativetolerance &&
                                        trgframe[elementcounter].Duration <= positivetolerance)
                                    {
                                    }
                                    else
                                    {
                                        deviation =
                                            Math.Abs(src.SignalFrames[srcFrameCounter][elementcounter].Duration -
                                                     trg.SignalFrames[trgFrameCounter][elementcounter].Duration);
                                        deviation = ((100.00 * deviation) / src.SignalFrames[srcFrameCounter][elementcounter].Duration);
                                        outputstream.AppendLine(
                                            string.Format(
                                                "Timing Mismatch at Frame {0}, Element {1} [DB] Space= {2}, [Capture] Space= {3} Deviation {4}% exceeds {5}%",
                                                srcFrameCounter, elementcounter,
                                                src.SignalFrames[srcFrameCounter][elementcounter].Duration,
                                                trg.SignalFrames[trgFrameCounter][elementcounter].Duration, deviation,
                                                Marktolerance * 100));
                                        errorMessages.Add(
                                            string.Format(
                                                "Timing Mismatch at Frame {0}, Element {1} [DB] Space= {2}, [Capture] Space= {3} Deviation {4}% exceeds {5}%",
                                                srcFrameCounter + 1, elementcounter + 1,
                                                src.SignalFrames[srcFrameCounter][elementcounter].Duration,
                                                trg.SignalFrames[trgFrameCounter][elementcounter].Duration, deviation,
                                                Marktolerance * 100));
                                        //bMatched = false;
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                }
                else //Compare Frames
                {
                    int nCountFrames = tx.SignalFrames.Count;
                    if (nCountFrames > rx.SignalFrames.Count)
                    {
                        nCountFrames = rx.SignalFrames.Count;
                    }

                    List<Element> srcframe = new List<Element>();
                    List<Element> trgframe = new List<Element>();

                    for (int framecounter = 0; framecounter < nCountFrames; framecounter++)
                    {
                        if (src.SignalFrames[framecounter].Count != trg.SignalFrames[framecounter].Count)
                        {
                            if (src.SignalFrames[framecounter].Count > trg.SignalFrames[framecounter].Count)
                            {
                                srcframe = trg.SignalFrames[framecounter];
                                trgframe = src.SignalFrames[framecounter];
                            }
                            else
                            {
                                srcframe = src.SignalFrames[framecounter];
                                trgframe = trg.SignalFrames[framecounter];
                            }
                            //errorMessages.Add(String.Format("Tx has {0} frameelements & Rx has {1} frameelements, hence framelements mismatch", src.SignalFrames[framecounter].Count, trg.SignalFrames[framecounter].Count));
                            //bMatched = false;
                        }
                        else
                        {
                            srcframe = src.SignalFrames[framecounter];
                            trgframe = trg.SignalFrames[framecounter];
                        }

                        //Skip compare last frame space to avoid compare with long code gap
                        int nCurrentFrameCount = srcframe.Count;
                        if (framecounter == nCountFrames - 1)
                        {
                            nCurrentFrameCount--;
                        }
                        //

                        for (int elementcounter = 0; elementcounter < nCurrentFrameCount; elementcounter++)
                        {
                            if (src.SignalFrames[framecounter][elementcounter].Frequency == 0)
                            {
                                positivetolerance = srcframe[elementcounter].Duration +
                                                    (Spacetolerance * srcframe[elementcounter].Duration);
                                negativetolerance = srcframe[elementcounter].Duration -
                                                    (Spacetolerance * srcframe[elementcounter].Duration);
                                if (trgframe[elementcounter].Duration >= negativetolerance &&
                                    trgframe[elementcounter].Duration <= positivetolerance)
                                {
                                }
                                else
                                {
                                    deviation =
                                        Math.Abs(src.SignalFrames[framecounter][elementcounter].Duration -
                                                 trg.SignalFrames[framecounter][elementcounter].Duration);
                                    deviation = ((100.00 * deviation) / src.SignalFrames[framecounter][elementcounter].Duration);
                                    outputstream.AppendLine(
                                        string.Format(
                                            "Timing Mismatch at Frame {0}, Element {1} [DB] Space= {2}, [Capture] Space= {3} Deviation {4}% exceeds {5}%",
                                            framecounter, elementcounter,
                                            src.SignalFrames[framecounter][elementcounter].Duration,
                                            trg.SignalFrames[framecounter][elementcounter].Duration, deviation,
                                            Spacetolerance * 100));
                                    errorMessages.Add(
                                        string.Format(
                                            "Timing Mismatch at Frame {0}, Element {1} [DB] Space= {2}, [Capture] Space= {3} Deviation {4}% exceeds {5}%",
                                            framecounter + 1, elementcounter + 1,
                                            src.SignalFrames[framecounter][elementcounter].Duration,
                                            trg.SignalFrames[framecounter][elementcounter].Duration, deviation,
                                            Spacetolerance * 100));
                                    //bMatched = false;
                                }
                            }
                            else
                            {
                                positivetolerance = src.SignalFrames[framecounter][elementcounter].Duration +
                                                    (Marktolerance * src.SignalFrames[framecounter][elementcounter].Duration);
                                negativetolerance = src.SignalFrames[framecounter][elementcounter].Duration -
                                                    (Marktolerance * src.SignalFrames[framecounter][elementcounter].Duration);
                                if (trgframe[elementcounter].Duration >= negativetolerance &&
                                    trgframe[elementcounter].Duration <= positivetolerance)
                                {
                                }
                                else
                                {
                                    deviation =
                                        Math.Abs(src.SignalFrames[framecounter][elementcounter].Duration -
                                                 trg.SignalFrames[framecounter][elementcounter].Duration);
                                    deviation = ((100.00 * deviation) / src.SignalFrames[framecounter][elementcounter].Duration);
                                    outputstream.AppendLine(
                                        string.Format(
                                            "Timing Mismatch at Frame {0}, Element {1} [DB] Space= {2}, [Capture] Space= {3} Deviation {4}% exceeds {5}%",
                                            framecounter, elementcounter,
                                            src.SignalFrames[framecounter][elementcounter].Duration,
                                            trg.SignalFrames[framecounter][elementcounter].Duration, deviation,
                                            Marktolerance * 100));
                                    errorMessages.Add(
                                        string.Format(
                                            "Timing Mismatch at Frame {0}, Element {1} [DB] Space= {2}, [Capture] Space= {3} Deviation {4}% exceeds {5}%",
                                            framecounter + 1, elementcounter + 1,
                                            src.SignalFrames[framecounter][elementcounter].Duration,
                                            trg.SignalFrames[framecounter][elementcounter].Duration, deviation,
                                            Marktolerance * 100));
                                    //bMatched = false;
                                }
                            }
                        }
                    }

                }

            }
            catch
            {
                return false;
            }
            return bMatched;
        }

        #endregion

        public FiCompareLib()
        {
            _frequencytolerance = 0.06;
            _dutytolerance = 0.6;
            _marktolerance = 0.06;
            _spacetolerance = 0.06;
            outputstream = new StringBuilder();


            int devPulse = 6;
            int devDelay = 6;
            int devDelayPulse = 6;
            int devFreq = 10;
            int devCodeGap = 6;
            int devDutyCycle = 60;
            _bCmpFrames = false;

            devFreq = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.FREQ_DEV, 10);
            devPulse = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.PULSE_DEV, 6);
            devDelay = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.DELAY_DEV, 6);
            devCodeGap = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CODEGAP_DEV, 6);
            devDelayPulse = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.DELAYPULSE_DEV, 6);
            devDutyCycle = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.DUTYCYCLE_DEV, 60);
            _bCmpFrames = Convert.ToBoolean(UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.TICMPFRAMES, 0));

            _frequencytolerance = (double) devFreq/100;
            _dutytolerance = (double) devDutyCycle/100;
            _marktolerance = (double) devPulse/100;
            _spacetolerance = (double) devDelay/100;
        }
    }
}