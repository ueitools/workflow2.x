using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DBReports.Forms
{
    public partial class DisconnectedTNsDlg : Form
    {
        #region Private Fields

        int _first = 0;
        int _last = -1;
        bool _useMainDB = false;

        #endregion

        #region Public flag access

        public int FirstTn { get { return _first; } }
        public int LastTn  { get { return _last;  } }
        public bool UseMainDB { get { return _useMainDB; } }

        #endregion

        public DisconnectedTNsDlg()
        {
            InitializeComponent();
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            if (FirstTNEdit.Text.Length > 0)
                _first = int.Parse(FirstTNEdit.Text.ToString());

            if (LastTNEdit.Text.Length > 0)
                _last = int.Parse(LastTNEdit.Text.ToString());

            _useMainDB = MainDBRb.Checked;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}