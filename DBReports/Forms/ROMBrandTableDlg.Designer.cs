namespace DBReports.Forms
{
    partial class ROMBrandTableDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LV_Regions = new System.Windows.Forms.ListView();
            this.Btn_IDList = new System.Windows.Forms.Button();
            this.TB_IDListFilePath = new System.Windows.Forms.TextBox();
            this.TB_BrandListFilePath = new System.Windows.Forms.TextBox();
            this.Btn_BrandList = new System.Windows.Forms.Button();
            this.OK_Btn = new System.Windows.Forms.Button();
            this.Cancel_Btn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TB_OutputFilePath = new System.Windows.Forms.TextBox();
            this.Btn_Output = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LV_Regions
            // 
            this.LV_Regions.CheckBoxes = true;
            this.LV_Regions.Location = new System.Drawing.Point(63, 169);
            this.LV_Regions.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LV_Regions.Name = "LV_Regions";
            this.LV_Regions.Size = new System.Drawing.Size(543, 78);
            this.LV_Regions.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.LV_Regions.TabIndex = 0;
            this.LV_Regions.UseCompatibleStateImageBehavior = false;
            this.LV_Regions.View = System.Windows.Forms.View.List;
            // 
            // Btn_IDList
            // 
            this.Btn_IDList.Location = new System.Drawing.Point(507, 38);
            this.Btn_IDList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Btn_IDList.Name = "Btn_IDList";
            this.Btn_IDList.Size = new System.Drawing.Size(100, 28);
            this.Btn_IDList.TabIndex = 1;
            this.Btn_IDList.Text = "Browse";
            this.Btn_IDList.UseVisualStyleBackColor = true;
            this.Btn_IDList.Click += new System.EventHandler(this.Btn_IDList_Click);
            // 
            // TB_IDListFilePath
            // 
            this.TB_IDListFilePath.Location = new System.Drawing.Point(63, 39);
            this.TB_IDListFilePath.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TB_IDListFilePath.Name = "TB_IDListFilePath";
            this.TB_IDListFilePath.Size = new System.Drawing.Size(435, 22);
            this.TB_IDListFilePath.TabIndex = 2;
            // 
            // TB_BrandListFilePath
            // 
            this.TB_BrandListFilePath.Location = new System.Drawing.Point(63, 98);
            this.TB_BrandListFilePath.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TB_BrandListFilePath.Name = "TB_BrandListFilePath";
            this.TB_BrandListFilePath.Size = new System.Drawing.Size(435, 22);
            this.TB_BrandListFilePath.TabIndex = 4;
            // 
            // Btn_BrandList
            // 
            this.Btn_BrandList.Location = new System.Drawing.Point(507, 97);
            this.Btn_BrandList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Btn_BrandList.Name = "Btn_BrandList";
            this.Btn_BrandList.Size = new System.Drawing.Size(100, 28);
            this.Btn_BrandList.TabIndex = 3;
            this.Btn_BrandList.Text = "Browse";
            this.Btn_BrandList.UseVisualStyleBackColor = true;
            this.Btn_BrandList.Click += new System.EventHandler(this.Btn_BrandList_Click);
            // 
            // OK_Btn
            // 
            this.OK_Btn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.OK_Btn.Location = new System.Drawing.Point(204, 353);
            this.OK_Btn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OK_Btn.Name = "OK_Btn";
            this.OK_Btn.Size = new System.Drawing.Size(100, 28);
            this.OK_Btn.TabIndex = 5;
            this.OK_Btn.Text = "OK";
            this.OK_Btn.UseVisualStyleBackColor = true;
            this.OK_Btn.Click += new System.EventHandler(this.OKBtn_Click);
            // 
            // Cancel_Btn
            // 
            this.Cancel_Btn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Cancel_Btn.Location = new System.Drawing.Point(365, 353);
            this.Cancel_Btn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Cancel_Btn.Name = "Cancel_Btn";
            this.Cancel_Btn.Size = new System.Drawing.Size(100, 28);
            this.Cancel_Btn.TabIndex = 6;
            this.Cancel_Btn.Text = "Cancel";
            this.Cancel_Btn.UseVisualStyleBackColor = true;
            this.Cancel_Btn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "ID List Input File";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 78);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Brand List Input File";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 148);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Region Selection";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 262);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "Output File";
            // 
            // TB_OutputFilePath
            // 
            this.TB_OutputFilePath.Location = new System.Drawing.Point(63, 283);
            this.TB_OutputFilePath.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TB_OutputFilePath.Name = "TB_OutputFilePath";
            this.TB_OutputFilePath.Size = new System.Drawing.Size(435, 22);
            this.TB_OutputFilePath.TabIndex = 11;
            // 
            // Btn_Output
            // 
            this.Btn_Output.Location = new System.Drawing.Point(507, 282);
            this.Btn_Output.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Btn_Output.Name = "Btn_Output";
            this.Btn_Output.Size = new System.Drawing.Size(100, 28);
            this.Btn_Output.TabIndex = 10;
            this.Btn_Output.Text = "Browse";
            this.Btn_Output.UseVisualStyleBackColor = true;
            this.Btn_Output.Click += new System.EventHandler(this.Btn_Output_Click);
            // 
            // ROMBrandTableDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(669, 410);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TB_OutputFilePath);
            this.Controls.Add(this.Btn_Output);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Cancel_Btn);
            this.Controls.Add(this.OK_Btn);
            this.Controls.Add(this.TB_BrandListFilePath);
            this.Controls.Add(this.Btn_BrandList);
            this.Controls.Add(this.TB_IDListFilePath);
            this.Controls.Add(this.Btn_IDList);
            this.Controls.Add(this.LV_Regions);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ROMBrandTableDlg";
            this.Text = "Generate Brand Table for ROM";
            this.Load += new System.EventHandler(this.ROMBrandTable_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView LV_Regions;
        private System.Windows.Forms.Button Btn_IDList;
        private System.Windows.Forms.TextBox TB_IDListFilePath;
        private System.Windows.Forms.Button Btn_BrandList;
        private System.Windows.Forms.Button OK_Btn;
        private System.Windows.Forms.Button Cancel_Btn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TB_BrandListFilePath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TB_OutputFilePath;
        private System.Windows.Forms.Button Btn_Output;
    }
}