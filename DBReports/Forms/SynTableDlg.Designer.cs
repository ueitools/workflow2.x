namespace DBReports
{
    partial class SynTableDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Button buttonCancel;
            this.buttonOK = new System.Windows.Forms.Button();
            this.includeIntron = new System.Windows.Forms.RadioButton();
            this.noIntron = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBoxIntron = new System.Windows.Forms.GroupBox();
            this.groupBoxDB = new System.Windows.Forms.GroupBox();
            this.UEI_Temp_DB = new System.Windows.Forms.RadioButton();
            this.UEI_DB = new System.Windows.Forms.RadioButton();
            buttonCancel = new System.Windows.Forms.Button();
            this.groupBoxIntron.SuspendLayout();
            this.groupBoxDB.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            buttonCancel.Location = new System.Drawing.Point(327, 114);
            buttonCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            buttonCancel.Name = "buttonCancel";
            buttonCancel.Size = new System.Drawing.Size(100, 28);
            buttonCancel.TabIndex = 1;
            buttonCancel.Text = "Cancel";
            buttonCancel.UseVisualStyleBackColor = true;
            buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(327, 54);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(100, 28);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // includeIntron
            // 
            this.includeIntron.AutoSize = true;
            this.includeIntron.Checked = true;
            this.includeIntron.Location = new System.Drawing.Point(20, 50);
            this.includeIntron.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.includeIntron.Name = "includeIntron";
            this.includeIntron.Size = new System.Drawing.Size(121, 21);
            this.includeIntron.TabIndex = 3;
            this.includeIntron.TabStop = true;
            this.includeIntron.Text = "Include Introns";
            this.includeIntron.UseVisualStyleBackColor = true;
            // 
            // noIntron
            // 
            this.noIntron.AutoSize = true;
            this.noIntron.Location = new System.Drawing.Point(20, 78);
            this.noIntron.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.noIntron.Name = "noIntron";
            this.noIntron.Size = new System.Drawing.Size(94, 21);
            this.noIntron.TabIndex = 4;
            this.noIntron.Text = "No Introns";
            this.noIntron.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Database";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Intron";
            // 
            // groupBoxIntron
            // 
            this.groupBoxIntron.Controls.Add(this.includeIntron);
            this.groupBoxIntron.Controls.Add(this.label2);
            this.groupBoxIntron.Controls.Add(this.noIntron);
            this.groupBoxIntron.Location = new System.Drawing.Point(0, 101);
            this.groupBoxIntron.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxIntron.Name = "groupBoxIntron";
            this.groupBoxIntron.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxIntron.Size = new System.Drawing.Size(267, 111);
            this.groupBoxIntron.TabIndex = 7;
            this.groupBoxIntron.TabStop = false;
            // 
            // groupBoxDB
            // 
            this.groupBoxDB.Controls.Add(this.UEI_Temp_DB);
            this.groupBoxDB.Controls.Add(this.UEI_DB);
            this.groupBoxDB.Controls.Add(this.label1);
            this.groupBoxDB.Location = new System.Drawing.Point(0, -5);
            this.groupBoxDB.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxDB.Name = "groupBoxDB";
            this.groupBoxDB.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxDB.Size = new System.Drawing.Size(267, 110);
            this.groupBoxDB.TabIndex = 8;
            this.groupBoxDB.TabStop = false;
            // 
            // UEI_Temp_DB
            // 
            this.UEI_Temp_DB.AutoSize = true;
            this.UEI_Temp_DB.Location = new System.Drawing.Point(20, 78);
            this.UEI_Temp_DB.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.UEI_Temp_DB.Name = "UEI_Temp_DB";
            this.UEI_Temp_DB.Size = new System.Drawing.Size(114, 21);
            this.UEI_Temp_DB.TabIndex = 7;
            this.UEI_Temp_DB.Text = "UEI Temp DB";
            this.UEI_Temp_DB.UseVisualStyleBackColor = true;
            // 
            // UEI_DB
            // 
            this.UEI_DB.AutoSize = true;
            this.UEI_DB.Checked = true;
            this.UEI_DB.Location = new System.Drawing.Point(20, 49);
            this.UEI_DB.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.UEI_DB.Name = "UEI_DB";
            this.UEI_DB.Size = new System.Drawing.Size(74, 21);
            this.UEI_DB.TabIndex = 6;
            this.UEI_DB.TabStop = true;
            this.UEI_DB.Text = "UEI DB";
            this.UEI_DB.UseVisualStyleBackColor = true;
            // 
            // SynTableDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(461, 214);
            this.Controls.Add(this.groupBoxDB);
            this.Controls.Add(this.groupBoxIntron);
            this.Controls.Add(buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SynTableDlg";
            this.Text = "SynTableDlg";
            this.groupBoxIntron.ResumeLayout(false);
            this.groupBoxIntron.PerformLayout();
            this.groupBoxDB.ResumeLayout(false);
            this.groupBoxDB.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.RadioButton includeIntron;
        private System.Windows.Forms.RadioButton noIntron;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxIntron;
        private System.Windows.Forms.GroupBox groupBoxDB;
        private System.Windows.Forms.RadioButton UEI_Temp_DB;
        private System.Windows.Forms.RadioButton UEI_DB;
    }
}