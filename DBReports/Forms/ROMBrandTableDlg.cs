using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using CommonForms;

namespace DBReports.Forms
{
    public partial class ROMBrandTableDlg : Form
    {
        public string IDListFilePath;
        public string BrandListPath;
        public string OutputFilePath;

        public ROMBrandTableDlg()
        {
            InitializeComponent();
        }

        private void ROMBrandTable_Load(object sender, EventArgs e)
        {
            CountryCollection CountryList = DBFunctions.GetAllCountryList();
            IList<string> tmp = new List<string>();
            foreach (Country item in CountryList)
            {
                if (tmp.Contains(item.Region))
                    continue;
                LV_Regions.Items.Add(item.Region);
                tmp.Add(item.Region);
            }
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Btn_IDList_Click(object sender, EventArgs e)
        {
            TB_IDListFilePath.Text = LocateFile(TB_IDListFilePath.Text, true);
        }

        private void Btn_BrandList_Click(object sender, EventArgs e)
        {
            TB_BrandListFilePath.Text = LocateFile(TB_BrandListFilePath.Text, true);
        }

        private void Btn_Output_Click(object sender, EventArgs e)
        {
            TB_OutputFilePath.Text = LocateFile(TB_OutputFilePath.Text, false);
        }

        private string LocateFile(string StartPoint, bool MustExist)
        {
            FileDialog dlg;
            if (MustExist)
            {
                dlg = new OpenFileDialog();
                dlg.Filter = "Lst files (*.lst)|*.lst|Txt files (*.txt)|*.txt|All files (*.*)|*.*";
                dlg.DefaultExt = ".txt";
            }
            else
            {
                dlg = new SaveFileDialog();
                dlg.Filter = "Src files (*.src)|*.src|Txt files (*.txt)|*.txt|All files (*.*)|*.*";
                dlg.DefaultExt = ".src";
            }
            if (string.IsNullOrEmpty(StartPoint))
                dlg.InitialDirectory = Configuration.GetWorkingDirectory();
            else
                dlg.InitialDirectory = StartPoint;

            dlg.CheckFileExists = MustExist;

            if (dlg.ShowDialog() == DialogResult.OK)
                return dlg.FileName;

            return StartPoint;
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            bool ready = true;
            if (string.IsNullOrEmpty(TB_IDListFilePath.Text))
                ready = false;
            if (string.IsNullOrEmpty(TB_BrandListFilePath.Text))
                ready = false;
            if (string.IsNullOrEmpty(TB_OutputFilePath.Text))
                ready = false;
            if (LV_Regions.CheckedIndices.Count == 0)
                ready = false;
            if (!ready)
            {
                MessageBox.Show("All fields need to be entered", "Error");
                return;
            }

            BrandListPath = TB_BrandListFilePath.Text;
            IDListFilePath = TB_IDListFilePath.Text;
            OutputFilePath = TB_OutputFilePath.Text;

            DialogResult = DialogResult.OK;
            Close();
        }

        public string BuildMarketList()
        {
            string MarketList = "All " + LV_Regions.CheckedItems[0].Text;
            foreach (ListViewItem item in LV_Regions.CheckedItems)
                if (!MarketList.Contains(item.Text))
                    MarketList += "|All " + item.Text;

            return MarketList;
        }
    }
}