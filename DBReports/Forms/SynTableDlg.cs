using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DBReports
{
    public partial class SynTableDlg : Form
    {
        #region Private Fields
        private bool _includeIntron;
        private bool _useUEIDB;
        #endregion

        #region Public flag access
        public bool IncludeIntron { get { return _includeIntron; } }
        public bool UseUEIDB { get { return _useUEIDB; } }
        #endregion

        public SynTableDlg()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this._includeIntron = this.includeIntron.Checked;
            this._useUEIDB = this.UEI_DB.Checked;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

 
    }
}