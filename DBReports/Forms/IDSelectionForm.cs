using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DBReports
{
    public enum SelType { RANGE, MODE, FILE };

    public struct SelectType
    {
        public SelType SelectionType;
        public string Result;
    };

    public partial class IDSelectionForm : Form
    {
        public SelectType Choice = new SelectType();

        public IDSelectionForm()
        {
            InitializeComponent();
            ModeRdoBtn.Checked = true;
            AllModes.Checked = true;
        }

        private void RangeRdoBtn_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = true;
            groupBox2.Enabled = false;
            groupBox3.Enabled = false;
            Choice.SelectionType = SelType.RANGE;
        }

        private void ModeRdoBtn_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = false;
            groupBox2.Enabled = true;
            groupBox3.Enabled = false;
            Choice.SelectionType = SelType.MODE;
        }

        private void FileRdoBtn_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = false;
            groupBox2.Enabled = false;
            groupBox3.Enabled = true;
            Choice.SelectionType = SelType.FILE;

            if (FileRdoBtn.Checked)
                GetIDInputFile();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            bool result = false;

            switch (Choice.SelectionType)
            {
                case SelType.RANGE:
                    result = CheckIDRange();
                    break;
                case SelType.MODE:
                    result = CheckModeSelection();
                    break;
                case SelType.FILE:
                    if (InputFile.Text.Length > 0)
                    {
                        Choice.Result = InputFile.Text;
                        result = true;
                    }
                    break;
            }

            if (!result)
            {
                MessageBox.Show("You haven't completed your selection.", "ERROR");
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// verify that at least one ID was entered
        /// </summary>
        /// <returns></returns>
        private bool CheckIDRange()
        {
            bool result = false;
            string IDName;

            IDName = IsValidIdName(FirstID.Text);
            if (IDName != null)
            {
                Choice.Result = IDName;
                result = true;
            }

            IDName = IsValidIdName(LastID.Text);
            if (IDName != null)
            {
                if (Choice.Result.Length > 0)
                    Choice.Result += "|";
                Choice.Result += IDName;
                result = true;
            }
            return result;
        }

        private string IsValidIdName(string ID)
        {
            string result = null;
            int IDNumber;

            if (ID.Length > 1 && ID.Length < 6)
            {
                if (Char.IsLetter(ID[0]) &&
                     int.TryParse(ID.Substring(1), out IDNumber))
                {
                    result = string.Format("{0}{1:D4}", Char.ToUpper(ID[0]), IDNumber);
                }
            }
            return result;
        }

        /// <summary>
        /// Check for valid mode selections
        /// </summary>
        /// <returns></returns>
        private bool CheckModeSelection()
        {
            bool result = false;
            if (AllModes.Checked == true)
            {
                Choice.Result = "";
                result = true;
            }
            else
            {
                string modes = GetSelectedModes();
                if (modes.Length > 0)
                {
                    Choice.Result = modes;
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// display an open file dialog to select an ID List file
        /// </summary>
        private void GetIDInputFile()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Text file (*.txt)|*.txt";
            dlg.Filter += "|List File (*.lst)|*.LST";
            dlg.Filter += "|ID List FIle (*.ids)|*.ids";
            dlg.Filter += "|All File Types (*.*)|*.*";
            dlg.DefaultExt = "txt";
            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                InputFile.Text = dlg.FileName;
            }
        }

        /// <summary>
        /// if AllModes is checked, clear all single mode boxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AllMode_CheckedChanged(object sender, EventArgs e)
        {
            if (AllModes.Checked == true)
            {
                ResetAllModes();
                AllModes.Checked = true;
            }
        }

        /// <summary>
        /// Called if any mode checkbox state changes except AllModes.
        /// Check if all mode boxes are checked, if true reset all checks and set AllModes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModeSelection_CheckedChanged(object sender, EventArgs e)
        {
            AllModes.Checked = false;
            if (AllModesSelected())
            {
                ResetAllModes();
                AllModes.Checked = true;
            }
        }

        /// <summary>
        /// Return true if all mode check boxes are checked return false if any are not checked
        ///  except AllModes
        /// </summary>
        /// <returns></returns>
        private bool AllModesSelected()
        {
            foreach (CheckBox box in groupBox2.Controls)
                if (box.Checked == false && box.Text.Length == 1)
                    return false;

            return true;
        }

        /// <summary>
        /// Reset all mode check boxes
        ///  except AllModes
        /// </summary>
        private void ResetAllModes()
        {
            foreach (CheckBox box in groupBox2.Controls)
                if (box.Text.Length == 1)
                    box.Checked = false;
        }

        /// <summary>
        /// Add the mode letter to the result string for each selected mode
        ///  except AllModes
        /// </summary>
        /// <returns></returns>
        private string GetSelectedModes()
        {
            string modes = "";

            foreach (CheckBox Box in groupBox2.Controls)
                if (Box.Checked && Box.Text.Length == 1)
                    modes += Box.Text;

            return modes;
        }
    }
}