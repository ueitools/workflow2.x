namespace DBReports
{
    partial class IDSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.InputFile = new System.Windows.Forms.TextBox();
            this.AllModes = new System.Windows.Forms.CheckBox();
            this.ModeZ = new System.Windows.Forms.CheckBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.ModeY = new System.Windows.Forms.CheckBox();
            this.ModeX = new System.Windows.Forms.CheckBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.ModeW = new System.Windows.Forms.CheckBox();
            this.ModeV = new System.Windows.Forms.CheckBox();
            this.ModeU = new System.Windows.Forms.CheckBox();
            this.ModeT = new System.Windows.Forms.CheckBox();
            this.ModeS = new System.Windows.Forms.CheckBox();
            this.ModeR = new System.Windows.Forms.CheckBox();
            this.ModeQ = new System.Windows.Forms.CheckBox();
            this.ModeP = new System.Windows.Forms.CheckBox();
            this.ModeO = new System.Windows.Forms.CheckBox();
            this.ModeN = new System.Windows.Forms.CheckBox();
            this.ModeM = new System.Windows.Forms.CheckBox();
            this.ModeL = new System.Windows.Forms.CheckBox();
            this.ModeK = new System.Windows.Forms.CheckBox();
            this.ModeJ = new System.Windows.Forms.CheckBox();
            this.ModeI = new System.Windows.Forms.CheckBox();
            this.ModeH = new System.Windows.Forms.CheckBox();
            this.ModeG = new System.Windows.Forms.CheckBox();
            this.ModeF = new System.Windows.Forms.CheckBox();
            this.ModeE = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ModeD = new System.Windows.Forms.CheckBox();
            this.ModeC = new System.Windows.Forms.CheckBox();
            this.ModeB = new System.Windows.Forms.CheckBox();
            this.ModeA = new System.Windows.Forms.CheckBox();
            this.ModeRdoBtn = new System.Windows.Forms.RadioButton();
            this.FileRdoBtn = new System.Windows.Forms.RadioButton();
            this.FirstID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LastID = new System.Windows.Forms.TextBox();
            this.RangeRdoBtn = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.InputFile);
            this.groupBox3.Location = new System.Drawing.Point(124, 258);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(384, 64);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Input File";
            // 
            // InputFile
            // 
            this.InputFile.Location = new System.Drawing.Point(20, 23);
            this.InputFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.InputFile.Name = "InputFile";
            this.InputFile.Size = new System.Drawing.Size(340, 22);
            this.InputFile.TabIndex = 1;
            // 
            // AllModes
            // 
            this.AllModes.AutoSize = true;
            this.AllModes.Location = new System.Drawing.Point(8, 137);
            this.AllModes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AllModes.Name = "AllModes";
            this.AllModes.Size = new System.Drawing.Size(91, 21);
            this.AllModes.TabIndex = 27;
            this.AllModes.Text = "All Modes";
            this.AllModes.CheckedChanged += new System.EventHandler(this.AllMode_CheckedChanged);
            // 
            // ModeZ
            // 
            this.ModeZ.AutoSize = true;
            this.ModeZ.Location = new System.Drawing.Point(228, 108);
            this.ModeZ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeZ.Name = "ModeZ";
            this.ModeZ.Size = new System.Drawing.Size(39, 21);
            this.ModeZ.TabIndex = 26;
            this.ModeZ.Text = "Z";
            this.ModeZ.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.buttonCancel.Location = new System.Drawing.Point(300, 330);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(100, 28);
            this.buttonCancel.TabIndex = 9;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // ModeY
            // 
            this.ModeY.AutoSize = true;
            this.ModeY.Location = new System.Drawing.Point(172, 108);
            this.ModeY.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeY.Name = "ModeY";
            this.ModeY.Size = new System.Drawing.Size(39, 21);
            this.ModeY.TabIndex = 25;
            this.ModeY.Text = "Y";
            this.ModeY.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeX
            // 
            this.ModeX.AutoSize = true;
            this.ModeX.Location = new System.Drawing.Point(117, 108);
            this.ModeX.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeX.Name = "ModeX";
            this.ModeX.Size = new System.Drawing.Size(39, 21);
            this.ModeX.TabIndex = 24;
            this.ModeX.Text = "X";
            this.ModeX.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.buttonOK.Location = new System.Drawing.Point(107, 330);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(100, 28);
            this.buttonOK.TabIndex = 8;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // ModeW
            // 
            this.ModeW.AutoSize = true;
            this.ModeW.Location = new System.Drawing.Point(63, 108);
            this.ModeW.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeW.Name = "ModeW";
            this.ModeW.Size = new System.Drawing.Size(43, 21);
            this.ModeW.TabIndex = 23;
            this.ModeW.Text = "W";
            this.ModeW.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeV
            // 
            this.ModeV.AutoSize = true;
            this.ModeV.Location = new System.Drawing.Point(8, 108);
            this.ModeV.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeV.Name = "ModeV";
            this.ModeV.Size = new System.Drawing.Size(39, 21);
            this.ModeV.TabIndex = 22;
            this.ModeV.Text = "V";
            this.ModeV.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeU
            // 
            this.ModeU.AutoSize = true;
            this.ModeU.Location = new System.Drawing.Point(336, 80);
            this.ModeU.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeU.Name = "ModeU";
            this.ModeU.Size = new System.Drawing.Size(40, 21);
            this.ModeU.TabIndex = 21;
            this.ModeU.Text = "U";
            this.ModeU.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeT
            // 
            this.ModeT.AutoSize = true;
            this.ModeT.Location = new System.Drawing.Point(283, 80);
            this.ModeT.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeT.Name = "ModeT";
            this.ModeT.Size = new System.Drawing.Size(39, 21);
            this.ModeT.TabIndex = 20;
            this.ModeT.Text = "T";
            this.ModeT.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeS
            // 
            this.ModeS.AutoSize = true;
            this.ModeS.Location = new System.Drawing.Point(228, 80);
            this.ModeS.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeS.Name = "ModeS";
            this.ModeS.Size = new System.Drawing.Size(39, 21);
            this.ModeS.TabIndex = 19;
            this.ModeS.Text = "S";
            this.ModeS.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeR
            // 
            this.ModeR.AutoSize = true;
            this.ModeR.Location = new System.Drawing.Point(172, 80);
            this.ModeR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeR.Name = "ModeR";
            this.ModeR.Size = new System.Drawing.Size(40, 21);
            this.ModeR.TabIndex = 18;
            this.ModeR.Text = "R";
            this.ModeR.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeQ
            // 
            this.ModeQ.AutoSize = true;
            this.ModeQ.Location = new System.Drawing.Point(117, 80);
            this.ModeQ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeQ.Name = "ModeQ";
            this.ModeQ.Size = new System.Drawing.Size(41, 21);
            this.ModeQ.TabIndex = 17;
            this.ModeQ.Text = "Q";
            this.ModeQ.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeP
            // 
            this.ModeP.AutoSize = true;
            this.ModeP.Location = new System.Drawing.Point(63, 80);
            this.ModeP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeP.Name = "ModeP";
            this.ModeP.Size = new System.Drawing.Size(39, 21);
            this.ModeP.TabIndex = 16;
            this.ModeP.Text = "P";
            this.ModeP.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeO
            // 
            this.ModeO.AutoSize = true;
            this.ModeO.Location = new System.Drawing.Point(8, 80);
            this.ModeO.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeO.Name = "ModeO";
            this.ModeO.Size = new System.Drawing.Size(41, 21);
            this.ModeO.TabIndex = 15;
            this.ModeO.Text = "O";
            this.ModeO.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeN
            // 
            this.ModeN.AutoSize = true;
            this.ModeN.Location = new System.Drawing.Point(336, 52);
            this.ModeN.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeN.Name = "ModeN";
            this.ModeN.Size = new System.Drawing.Size(40, 21);
            this.ModeN.TabIndex = 14;
            this.ModeN.Text = "N";
            this.ModeN.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeM
            // 
            this.ModeM.AutoSize = true;
            this.ModeM.Location = new System.Drawing.Point(283, 52);
            this.ModeM.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeM.Name = "ModeM";
            this.ModeM.Size = new System.Drawing.Size(41, 21);
            this.ModeM.TabIndex = 13;
            this.ModeM.Text = "M";
            this.ModeM.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeL
            // 
            this.ModeL.AutoSize = true;
            this.ModeL.Location = new System.Drawing.Point(228, 52);
            this.ModeL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeL.Name = "ModeL";
            this.ModeL.Size = new System.Drawing.Size(38, 21);
            this.ModeL.TabIndex = 12;
            this.ModeL.Text = "L";
            this.ModeL.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeK
            // 
            this.ModeK.AutoSize = true;
            this.ModeK.Location = new System.Drawing.Point(172, 52);
            this.ModeK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeK.Name = "ModeK";
            this.ModeK.Size = new System.Drawing.Size(39, 21);
            this.ModeK.TabIndex = 11;
            this.ModeK.Text = "K";
            this.ModeK.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeJ
            // 
            this.ModeJ.AutoSize = true;
            this.ModeJ.Location = new System.Drawing.Point(117, 52);
            this.ModeJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeJ.Name = "ModeJ";
            this.ModeJ.Size = new System.Drawing.Size(37, 21);
            this.ModeJ.TabIndex = 10;
            this.ModeJ.Text = "J";
            this.ModeJ.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeI
            // 
            this.ModeI.AutoSize = true;
            this.ModeI.Location = new System.Drawing.Point(63, 52);
            this.ModeI.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeI.Name = "ModeI";
            this.ModeI.Size = new System.Drawing.Size(33, 21);
            this.ModeI.TabIndex = 9;
            this.ModeI.Text = "I";
            this.ModeI.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeH
            // 
            this.ModeH.AutoSize = true;
            this.ModeH.Location = new System.Drawing.Point(8, 52);
            this.ModeH.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeH.Name = "ModeH";
            this.ModeH.Size = new System.Drawing.Size(40, 21);
            this.ModeH.TabIndex = 8;
            this.ModeH.Text = "H";
            this.ModeH.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeG
            // 
            this.ModeG.AutoSize = true;
            this.ModeG.Location = new System.Drawing.Point(336, 23);
            this.ModeG.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeG.Name = "ModeG";
            this.ModeG.Size = new System.Drawing.Size(41, 21);
            this.ModeG.TabIndex = 7;
            this.ModeG.Text = "G";
            this.ModeG.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeF
            // 
            this.ModeF.AutoSize = true;
            this.ModeF.Location = new System.Drawing.Point(283, 23);
            this.ModeF.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeF.Name = "ModeF";
            this.ModeF.Size = new System.Drawing.Size(38, 21);
            this.ModeF.TabIndex = 6;
            this.ModeF.Text = "F";
            this.ModeF.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeE
            // 
            this.ModeE.AutoSize = true;
            this.ModeE.Location = new System.Drawing.Point(228, 23);
            this.ModeE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeE.Name = "ModeE";
            this.ModeE.Size = new System.Drawing.Size(39, 21);
            this.ModeE.TabIndex = 5;
            this.ModeE.Text = "E";
            this.ModeE.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.AllModes);
            this.groupBox2.Controls.Add(this.ModeZ);
            this.groupBox2.Controls.Add(this.ModeY);
            this.groupBox2.Controls.Add(this.ModeX);
            this.groupBox2.Controls.Add(this.ModeW);
            this.groupBox2.Controls.Add(this.ModeV);
            this.groupBox2.Controls.Add(this.ModeU);
            this.groupBox2.Controls.Add(this.ModeT);
            this.groupBox2.Controls.Add(this.ModeS);
            this.groupBox2.Controls.Add(this.ModeR);
            this.groupBox2.Controls.Add(this.ModeQ);
            this.groupBox2.Controls.Add(this.ModeP);
            this.groupBox2.Controls.Add(this.ModeO);
            this.groupBox2.Controls.Add(this.ModeN);
            this.groupBox2.Controls.Add(this.ModeM);
            this.groupBox2.Controls.Add(this.ModeL);
            this.groupBox2.Controls.Add(this.ModeK);
            this.groupBox2.Controls.Add(this.ModeJ);
            this.groupBox2.Controls.Add(this.ModeI);
            this.groupBox2.Controls.Add(this.ModeH);
            this.groupBox2.Controls.Add(this.ModeG);
            this.groupBox2.Controls.Add(this.ModeF);
            this.groupBox2.Controls.Add(this.ModeE);
            this.groupBox2.Controls.Add(this.ModeD);
            this.groupBox2.Controls.Add(this.ModeC);
            this.groupBox2.Controls.Add(this.ModeB);
            this.groupBox2.Controls.Add(this.ModeA);
            this.groupBox2.Location = new System.Drawing.Point(123, 82);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(384, 169);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Modes";
            // 
            // ModeD
            // 
            this.ModeD.AutoSize = true;
            this.ModeD.Location = new System.Drawing.Point(172, 23);
            this.ModeD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeD.Name = "ModeD";
            this.ModeD.Size = new System.Drawing.Size(40, 21);
            this.ModeD.TabIndex = 4;
            this.ModeD.Text = "D";
            this.ModeD.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeC
            // 
            this.ModeC.AutoSize = true;
            this.ModeC.Location = new System.Drawing.Point(117, 23);
            this.ModeC.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeC.Name = "ModeC";
            this.ModeC.Size = new System.Drawing.Size(39, 21);
            this.ModeC.TabIndex = 3;
            this.ModeC.Text = "C";
            this.ModeC.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeB
            // 
            this.ModeB.AutoSize = true;
            this.ModeB.Location = new System.Drawing.Point(63, 23);
            this.ModeB.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeB.Name = "ModeB";
            this.ModeB.Size = new System.Drawing.Size(39, 21);
            this.ModeB.TabIndex = 2;
            this.ModeB.Text = "B";
            this.ModeB.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeA
            // 
            this.ModeA.AutoSize = true;
            this.ModeA.Location = new System.Drawing.Point(8, 23);
            this.ModeA.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeA.Name = "ModeA";
            this.ModeA.Size = new System.Drawing.Size(39, 21);
            this.ModeA.TabIndex = 1;
            this.ModeA.Text = "A";
            this.ModeA.CheckedChanged += new System.EventHandler(this.ModeSelection_CheckedChanged);
            // 
            // ModeRdoBtn
            // 
            this.ModeRdoBtn.AutoSize = true;
            this.ModeRdoBtn.Location = new System.Drawing.Point(24, 105);
            this.ModeRdoBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ModeRdoBtn.Name = "ModeRdoBtn";
            this.ModeRdoBtn.Size = new System.Drawing.Size(84, 21);
            this.ModeRdoBtn.TabIndex = 29;
            this.ModeRdoBtn.Text = "By Mode";
            this.ModeRdoBtn.CheckedChanged += new System.EventHandler(this.ModeRdoBtn_CheckedChanged);
            // 
            // FileRdoBtn
            // 
            this.FileRdoBtn.AutoSize = true;
            this.FileRdoBtn.Location = new System.Drawing.Point(24, 283);
            this.FileRdoBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FileRdoBtn.Name = "FileRdoBtn";
            this.FileRdoBtn.Size = new System.Drawing.Size(71, 21);
            this.FileRdoBtn.TabIndex = 30;
            this.FileRdoBtn.Text = "By File";
            this.FileRdoBtn.CheckedChanged += new System.EventHandler(this.FileRdoBtn_CheckedChanged);
            // 
            // FirstID
            // 
            this.FirstID.Location = new System.Drawing.Point(68, 23);
            this.FirstID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FirstID.Name = "FirstID";
            this.FirstID.Size = new System.Drawing.Size(88, 22);
            this.FirstID.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "From";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(227, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "To";
            // 
            // LastID
            // 
            this.LastID.Location = new System.Drawing.Point(256, 23);
            this.LastID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LastID.Name = "LastID";
            this.LastID.Size = new System.Drawing.Size(88, 22);
            this.LastID.TabIndex = 4;
            // 
            // RangeRdoBtn
            // 
            this.RangeRdoBtn.AutoSize = true;
            this.RangeRdoBtn.Location = new System.Drawing.Point(24, 39);
            this.RangeRdoBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.RangeRdoBtn.Name = "RangeRdoBtn";
            this.RangeRdoBtn.Size = new System.Drawing.Size(91, 21);
            this.RangeRdoBtn.TabIndex = 28;
            this.RangeRdoBtn.Text = "By Range";
            this.RangeRdoBtn.CheckedChanged += new System.EventHandler(this.RangeRdoBtn_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.LastID);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.FirstID);
            this.groupBox1.Location = new System.Drawing.Point(124, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(384, 60);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ID Range";
            // 
            // IDSelectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(524, 382);
            this.Controls.Add(this.FileRdoBtn);
            this.Controls.Add(this.ModeRdoBtn);
            this.Controls.Add(this.RangeRdoBtn);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "IDSelectionForm";
            this.Text = "IDSelectionForm";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox InputFile;
        private System.Windows.Forms.CheckBox AllModes;
        private System.Windows.Forms.CheckBox ModeZ;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.CheckBox ModeY;
        private System.Windows.Forms.CheckBox ModeX;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.CheckBox ModeW;
        private System.Windows.Forms.CheckBox ModeV;
        private System.Windows.Forms.CheckBox ModeU;
        private System.Windows.Forms.CheckBox ModeT;
        private System.Windows.Forms.CheckBox ModeS;
        private System.Windows.Forms.CheckBox ModeR;
        private System.Windows.Forms.CheckBox ModeQ;
        private System.Windows.Forms.CheckBox ModeP;
        private System.Windows.Forms.CheckBox ModeO;
        private System.Windows.Forms.CheckBox ModeN;
        private System.Windows.Forms.CheckBox ModeM;
        private System.Windows.Forms.CheckBox ModeL;
        private System.Windows.Forms.CheckBox ModeK;
        private System.Windows.Forms.CheckBox ModeJ;
        private System.Windows.Forms.CheckBox ModeI;
        private System.Windows.Forms.CheckBox ModeH;
        private System.Windows.Forms.CheckBox ModeG;
        private System.Windows.Forms.CheckBox ModeF;
        private System.Windows.Forms.CheckBox ModeE;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox ModeD;
        private System.Windows.Forms.CheckBox ModeC;
        private System.Windows.Forms.CheckBox ModeB;
        private System.Windows.Forms.CheckBox ModeA;
        private System.Windows.Forms.RadioButton ModeRdoBtn;
        private System.Windows.Forms.RadioButton FileRdoBtn;
        private System.Windows.Forms.TextBox FirstID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox LastID;
        private System.Windows.Forms.RadioButton RangeRdoBtn;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}