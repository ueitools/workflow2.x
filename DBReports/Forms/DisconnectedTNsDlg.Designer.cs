namespace DBReports.Forms
{
    partial class DisconnectedTNsDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FirstTNEdit = new System.Windows.Forms.TextBox();
            this.LastTNEdit = new System.Windows.Forms.TextBox();
            this.OKBtn = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.MainDBRb = new System.Windows.Forms.RadioButton();
            this.TempDBRb = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // FirstTNEdit
            // 
            this.FirstTNEdit.Location = new System.Drawing.Point(67, 33);
            this.FirstTNEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FirstTNEdit.Name = "FirstTNEdit";
            this.FirstTNEdit.Size = new System.Drawing.Size(132, 22);
            this.FirstTNEdit.TabIndex = 2;
            // 
            // LastTNEdit
            // 
            this.LastTNEdit.Location = new System.Drawing.Point(67, 65);
            this.LastTNEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LastTNEdit.Name = "LastTNEdit";
            this.LastTNEdit.Size = new System.Drawing.Size(132, 22);
            this.LastTNEdit.TabIndex = 3;
            // 
            // OKBtn
            // 
            this.OKBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.OKBtn.Location = new System.Drawing.Point(139, 208);
            this.OKBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OKBtn.Name = "OKBtn";
            this.OKBtn.Size = new System.Drawing.Size(100, 28);
            this.OKBtn.TabIndex = 4;
            this.OKBtn.Text = "OK";
            this.OKBtn.UseVisualStyleBackColor = true;
            this.OKBtn.Click += new System.EventHandler(this.OKBtn_Click);
            // 
            // CancelBtn
            // 
            this.CancelBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.CancelBtn.Location = new System.Drawing.Point(295, 208);
            this.CancelBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(100, 28);
            this.CancelBtn.TabIndex = 5;
            this.CancelBtn.Text = "Cancel";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "First";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 69);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Last";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.FirstTNEdit);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.LastTNEdit);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(36, 34);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(220, 123);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TN Search Range";
            // 
            // MainDBRb
            // 
            this.MainDBRb.AutoSize = true;
            this.MainDBRb.Location = new System.Drawing.Point(28, 33);
            this.MainDBRb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MainDBRb.Name = "MainDBRb";
            this.MainDBRb.Size = new System.Drawing.Size(124, 21);
            this.MainDBRb.TabIndex = 9;
            this.MainDBRb.Text = "Main Database";
            this.MainDBRb.UseVisualStyleBackColor = true;
            // 
            // TempDBRb
            // 
            this.TempDBRb.AutoSize = true;
            this.TempDBRb.Checked = true;
            this.TempDBRb.Location = new System.Drawing.Point(28, 70);
            this.TempDBRb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TempDBRb.Name = "TempDBRb";
            this.TempDBRb.Size = new System.Drawing.Size(130, 21);
            this.TempDBRb.TabIndex = 10;
            this.TempDBRb.TabStop = true;
            this.TempDBRb.Text = "Temp Database";
            this.TempDBRb.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.MainDBRb);
            this.groupBox2.Controls.Add(this.TempDBRb);
            this.groupBox2.Location = new System.Drawing.Point(308, 34);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(191, 123);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Database";
            // 
            // DisconnectedTNsDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(535, 251);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.OKBtn);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "DisconnectedTNsDlg";
            this.Text = "DisconnectedTNsDlg";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox FirstTNEdit;
        private System.Windows.Forms.TextBox LastTNEdit;
        private System.Windows.Forms.Button OKBtn;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton MainDBRb;
        private System.Windows.Forms.RadioButton TempDBRb;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}