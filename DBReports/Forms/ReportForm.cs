using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DBReports
{
    public enum ReportType
    {
        GENERIC,
        CHECK_IN,
        QASTATUS,
        DISCONNECTED_TNS, 
        DUPLICATE_INTRONS,
        CHECKOUT_CONFLICT, 
        SYNTHESIZER_TABLE,
        RESTRICTED_ID,
        NONEMPTY_ID,
        EMPTY_ID,
        UNIQUE_FUNC_IR,
        BRAND_TABLE_ROM
    }

    public partial class ReportForm : Form
    {
        private ReportType Report;
        private string _FilePath;

        public ReportForm(ReportType Type)
        {
            InitializeComponent();
            Report = Type;
        }

        private void ReportForm_Load(object sender, EventArgs e)
        {
            switch (Report)
            {
                case ReportType.DISCONNECTED_TNS:
                    this.Text = "Find Disconnected TNs";
                    DisconnectedTNs DTNrpt = new DisconnectedTNs();
                    DTNrpt.DoReport(ReportTB);
                    break;
                case ReportType.QASTATUS:
                    this.Text = "QA Status Report";
                    QAStatus QArpt = new QAStatus();
                    QArpt.DoReport(ReportTB);
                    break;
                case ReportType.CHECK_IN:
                    this.Text = "Bulk Check-in Report";
                    break;
                case ReportType.GENERIC:
                    break;
                case ReportType.DUPLICATE_INTRONS:
                    DuplicateIntrons DupIntrs = new DuplicateIntrons();
                    this.Text = "Duplicate Introns Report";
                    DupIntrs.DoReport(ReportTB);
                    break;
                case ReportType.CHECKOUT_CONFLICT:
                    this.Text = "ID Check Out Conflict Report";
                    IDCheckoutConflict IDrpt = new IDCheckoutConflict();
                    IDrpt.DoReport(ReportTB);
                    break;
                case ReportType.SYNTHESIZER_TABLE:
                    this.Text = "Synthesizer Table Report";
                    SynthesizerTable SynTable = new SynthesizerTable();
                    SynTable.DoReport(ReportTB);
                    break;
                case ReportType.RESTRICTED_ID:
                    this.Text = "Restricted ID Report";
                    RestrictedID RestID = new RestrictedID();
                    RestID.DoReport();
                    break;
                case ReportType.NONEMPTY_ID:
                    this.Text = "Non empty ID Report";
                    NonEmptyID NonemptyID = new NonEmptyID();
                    NonemptyID.DoReport();
                    break;
                case ReportType.EMPTY_ID:
                    this.Text = "Empty ID Report";
                    EmptyID emptyID = new EmptyID();
                    emptyID.DoReport();
                    break;
                case ReportType.UNIQUE_FUNC_IR:
                    this.Text = "Unique Function/IR Report";
                    UniqueFuncIRCount UFIC = new UniqueFuncIRCount();
                    UFIC.DoReport();
                    break;
                case ReportType.BRAND_TABLE_ROM:
                    this.Text = "Generate Brand Table for ROM";
                    ROMBrandTable RBT = new ROMBrandTable();
                    RBT.DoReport();
                    break;
                default: break;
            }
           // this.Show();
            this.Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_FilePath == null)
            {
                saveAsToolStripMenuItem_Click(sender, e);
                return;
            }
            if (ConfirmOverwrite() == DialogResult.OK)
            {
                ReportTB.SaveFile(_FilePath, RichTextBoxStreamType.PlainText);
            }
        }

        private static DialogResult ConfirmOverwrite()
        {
            string message = "Overwrite Existing File?";
            string caption = "Confirm";
            return MessageBox.Show(message, caption, MessageBoxButtons.OKCancel);
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Text file (*.txt)|*.txt";
            dlg.Filter += "|List File (*.lst)|*.LST";
            dlg.Filter += "|ID List FIle (*.ids)|*.ids";
            dlg.Filter += "|All File Types (*.*)|*.*";
            dlg.DefaultExt = "txt";
            dlg.OverwritePrompt = true;

            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                _FilePath = dlg.FileName;
                ReportTB.SaveFile(_FilePath, RichTextBoxStreamType.PlainText);
            }
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}