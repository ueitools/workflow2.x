using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DBReports
{
    public partial class ProgressDlg : Form
    {
        public ProgressDlg()
        {
            InitializeComponent();
        }

        public void SetupProgressBar(int Start, int End, int Step)
        {
            ProgressIndicator.Minimum = Start;
            ProgressIndicator.Maximum = End;
            ProgressIndicator.Step = Step;
        }

        public void UpdateProgressBar()
        {
            ProgressIndicator.PerformStep();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            throw new Exception("Process Canceled by User");
        }
    }
}