namespace DBReports
{
    partial class StatusReportSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StatusBox = new System.Windows.Forms.GroupBox();
            this.FlagPcb = new System.Windows.Forms.CheckBox();
            this.FlagQcb = new System.Windows.Forms.CheckBox();
            this.FlagEcb = new System.Windows.Forms.CheckBox();
            this.FlagMcb = new System.Windows.Forms.CheckBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TNStatusCb = new System.Windows.Forms.CheckBox();
            this.VerboseBtn = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.StatusBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // StatusBox
            // 
            this.StatusBox.Controls.Add(this.FlagPcb);
            this.StatusBox.Controls.Add(this.FlagQcb);
            this.StatusBox.Controls.Add(this.FlagEcb);
            this.StatusBox.Controls.Add(this.FlagMcb);
            this.StatusBox.Location = new System.Drawing.Point(49, 75);
            this.StatusBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.StatusBox.Name = "StatusBox";
            this.StatusBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.StatusBox.Size = new System.Drawing.Size(161, 167);
            this.StatusBox.TabIndex = 62;
            this.StatusBox.TabStop = false;
            this.StatusBox.Text = "ID Status";
            // 
            // FlagPcb
            // 
            this.FlagPcb.AutoSize = true;
            this.FlagPcb.Location = new System.Drawing.Point(8, 138);
            this.FlagPcb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FlagPcb.Name = "FlagPcb";
            this.FlagPcb.Size = new System.Drawing.Size(107, 21);
            this.FlagPcb.TabIndex = 66;
            this.FlagPcb.Text = "Project Hold";
            // 
            // FlagQcb
            // 
            this.FlagQcb.AutoSize = true;
            this.FlagQcb.Checked = true;
            this.FlagQcb.CheckState = System.Windows.Forms.CheckState.Checked;
            this.FlagQcb.Location = new System.Drawing.Point(8, 23);
            this.FlagQcb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FlagQcb.Name = "FlagQcb";
            this.FlagQcb.Size = new System.Drawing.Size(101, 21);
            this.FlagQcb.TabIndex = 63;
            this.FlagQcb.Text = "Passed QA";
            // 
            // FlagEcb
            // 
            this.FlagEcb.AutoSize = true;
            this.FlagEcb.Location = new System.Drawing.Point(8, 100);
            this.FlagEcb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FlagEcb.Name = "FlagEcb";
            this.FlagEcb.Size = new System.Drawing.Size(93, 21);
            this.FlagEcb.TabIndex = 65;
            this.FlagEcb.Text = "Exec Hold";
            // 
            // FlagMcb
            // 
            this.FlagMcb.AutoSize = true;
            this.FlagMcb.Location = new System.Drawing.Point(8, 62);
            this.FlagMcb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FlagMcb.Name = "FlagMcb";
            this.FlagMcb.Size = new System.Drawing.Size(83, 21);
            this.FlagMcb.TabIndex = 64;
            this.FlagMcb.Text = "Modified";
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOK.Location = new System.Drawing.Point(79, 283);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(100, 28);
            this.buttonOK.TabIndex = 63;
            this.buttonOK.Text = "Start";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(284, 283);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(100, 28);
            this.buttonCancel.TabIndex = 64;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(73, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(310, 20);
            this.label1.TabIndex = 65;
            this.label1.Text = "Select the Status flags you want to track";
            // 
            // TNStatusCb
            // 
            this.TNStatusCb.AutoSize = true;
            this.TNStatusCb.Location = new System.Drawing.Point(21, 100);
            this.TNStatusCb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TNStatusCb.Name = "TNStatusCb";
            this.TNStatusCb.Size = new System.Drawing.Size(113, 21);
            this.TNStatusCb.TabIndex = 66;
            this.TNStatusCb.Text = "Modified TNs";
            // 
            // VerboseBtn
            // 
            this.VerboseBtn.AutoSize = true;
            this.VerboseBtn.Location = new System.Drawing.Point(8, 62);
            this.VerboseBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.VerboseBtn.Name = "VerboseBtn";
            this.VerboseBtn.Size = new System.Drawing.Size(129, 21);
            this.VerboseBtn.TabIndex = 67;
            this.VerboseBtn.Text = "Verbose Report";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Checked = true;
            this.radioButton2.Location = new System.Drawing.Point(8, 23);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(101, 21);
            this.radioButton2.TabIndex = 68;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "ID List Only";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.TNStatusCb);
            this.groupBox1.Controls.Add(this.VerboseBtn);
            this.groupBox1.Location = new System.Drawing.Point(251, 75);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(161, 167);
            this.groupBox1.TabIndex = 69;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Report Options";
            // 
            // StatusReportSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(461, 326);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.StatusBox);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "StatusReportSetup";
            this.Text = "QA Report Setup";
            this.StatusBox.ResumeLayout(false);
            this.StatusBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox StatusBox;
        private System.Windows.Forms.CheckBox FlagPcb;
        private System.Windows.Forms.CheckBox FlagQcb;
        private System.Windows.Forms.CheckBox FlagEcb;
        private System.Windows.Forms.CheckBox FlagMcb;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox TNStatusCb;
        private System.Windows.Forms.RadioButton VerboseBtn;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

