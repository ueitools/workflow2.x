using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using System.IO;

namespace DBReports
{
    class QAStatus
    {
        #region Private Fields

        private bool _TnStatus;
        private bool _Verbose;
        private string _IDStatusWatch;
        private RichTextBox ReportTB;
        static string _traceFileName =
           CommonForms.Configuration.GetWorkingDirectory() +
           "\\QAStatusReport.txt";
        #endregion

        public DialogResult DoReport(RichTextBox RTB)
        {
            ReportTB = RTB;

            StatusReportSetup QASetup = new StatusReportSetup();
            DialogResult result = QASetup.ShowDialog();
            QASetup.Close();

            if (result == DialogResult.OK)
            {
                _TnStatus = QASetup.TnStatus;
                _Verbose = QASetup.Verbose;

                _IDStatusWatch = (QASetup.FlagQ) ? StatusFlag.PASSED_QA : "";
                _IDStatusWatch += (QASetup.FlagM) ? StatusFlag.MODIFIED : "";
                _IDStatusWatch += (QASetup.FlagE) ? StatusFlag.EXEC_HOLD : "";
                _IDStatusWatch += (QASetup.FlagP) ? StatusFlag.PROJECT_HOLD : "";

                ProgressDlg Progress = new ProgressDlg();
                Progress.Show();
                GenerateStatusReport(Progress);
                Progress.Close();
            }
            return result;
        }

        private void GenerateStatusReport(ProgressDlg Progress)
        {
            File.Delete(_traceFileName);
            StringCollection list = DBFunctions.NonBlockingGetAllIDList();
            Progress.SetupProgressBar(0, list.Count, 1);

            try
            {
                foreach (string ID in list)
                {
                    Progress.UpdateProgressBar();
                    CheckIdStatus(ID);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            // open the file
            string notepad_path = "notepad.exe";
            System.Diagnostics.Process.Start(notepad_path, _traceFileName);
        }

        private void CheckIdStatus(string ID)
        {
            IDHeader IDHeaderData = DAOFactory.IDHeader().Select(ID);
            if (_IDStatusWatch.Contains(IDHeaderData.Status))
            {
                if (_Verbose)
                {
                    //ReportTB.Text += string.Format("\r ID {0} - Current status is: {1}\r",
                    //                        ID, StatusName(IDHeaderData.Status));
                    using (StreamWriter sw = File.AppendText(_traceFileName))
                    {
                        sw.WriteLine(string.Format(" ID {0} - Current status is: {1}",
                                            ID, StatusName(IDHeaderData.Status)));
                        sw.Close();
                    }
                    if (_TnStatus)
                        CheckTnStatus(ID);
                }
                else
                {
                    //ReportTB.Text += string.Format("{0}\r", ID);
                    using (StreamWriter sw = File.AppendText(_traceFileName))
                    {
                        sw.WriteLine(" "+ID);
                        sw.Close();
                    }
                }
            }
        }

        private void CheckTnStatus(string id)
        {
            IntegerCollection TNList = DBFunctions.GetTNList(id);
            foreach (int tn in TNList)
            {
                TNHeader TNHeaderData = DAOFactory.TNHeader().Select(tn);
                if (TNHeaderData.Status != "Q")
                {
                    //ReportTB.Text +=
                    //    string.Format("\tTN {0} has not Passed QA\r", tn);
                    using (StreamWriter sw = File.AppendText(_traceFileName))
                    {
                        sw.WriteLine(string.Format("\tTN {0} has not Passed QA", tn));
                        sw.Close();
                    }
                }
            }
        }

        public string StatusName(string Flag)
        {
            switch (Flag)
            {
                case StatusFlag.SHADOW: return "Shadow ID";
                case StatusFlag.MODIFIED: return "Modified";
                case StatusFlag.PASSED_QA: return "Passed QA";
                case StatusFlag.EXEC_HOLD: return "Exec Hold";
                case StatusFlag.PROJECT_HOLD: return "Project Hold";
                default: return "Undefined Flag";
            }
        }
    }
}
