using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using System.IO;
using System.Configuration;
using CommonForms;

namespace DBReports
{
    class NonEmptyID
    {
        #region Private Fields
        private CommonForms.IDSelectionForm IDSel;
        #endregion

        public DialogResult DoReport()
        {
            // IDSelectionForm from CommonForms Project
            IDSel = new CommonForms.IDSelectionForm();
            DialogResult res = IDSel.ShowDialog();
            IDSel.Close();

            if (res == DialogResult.OK)
            {
                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
                NonEmptyIDRpt Progress = new NonEmptyIDRpt();

                // Get the list of IDs that we're gonna process
                StringCollection allIDList = DBFunctions.NonBlockingGetAllIDList();
                List<string> IDList = IDSel.GetSelectedIDList(allIDList);

                Progress.Show();
                Progress.GenerateReport(IDList);
                Progress.Close();
            }

            IDSel.Dispose();
            return res;
        }

        class NonEmptyIDRpt : ProgressDlg
        {
            #region Private Fields
            static string _traceFileName =
                CommonForms.Configuration.GetWorkingDirectory() +
                "\\NonEmptyIDReport.txt";
            #endregion

            public void GenerateReport(List<string> IDList)
            {
                SetupProgressBar(0, IDList.Count, 1);
                File.Delete(_traceFileName);

                try
                {
                    foreach (string ID in IDList)
                    {
                        UpdateProgressBar();
                        TextMessage.Text = string.Format("Processing ID {0}", ID);
                        this.Update();
                        ProcessID(ID);
                    }
                    string notepad_path = "notepad.exe";
                    // open the file
                    System.Diagnostics.Process.Start(notepad_path, _traceFileName);
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }

            }


            /// <summary>
            /// process groups of functions with duplicate Introns
            /// </summary>
            /// <param name="ID"></param>
            private void ProcessID(string ID)
            {
                Hashtable paramList = new Hashtable();
                paramList.Add("ID", ID);
                FunctionCollection functions = DAOFactory.Function().SelectIDFunc(paramList);
                if (functions == null)
                    return;

                foreach (Function func in functions)
                {
                    if (func.Data != null && func.Data != "")
                    {
                        using (StreamWriter sw = File.AppendText(_traceFileName))
                        {
                            sw.WriteLine(ID);
                            sw.Close();
                        }
                        break;
                    }
                }
            }
        }
    }
}
