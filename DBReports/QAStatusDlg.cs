using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace DBReports
{
    public partial class StatusReportSetup : Form
    {
        #region Private Fields

        private bool _flag_Q;
        private bool _flag_M;
        private bool _flag_E;
        private bool _flag_P;
        private bool _tnStatus;
        private bool _verbose;

        #endregion

        #region Public flag access

        public bool FlagQ { get { return _flag_Q; } }
        public bool FlagM { get { return _flag_M; } }
        public bool FlagE { get { return _flag_E; } }
        public bool FlagP { get { return _flag_P; } }
        public bool TnStatus { get { return _tnStatus; } }
        public bool Verbose { get { return _verbose; } }

        #endregion

        public StatusReportSetup()
        {
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            _flag_Q = FlagQcb.Checked;
            _flag_M = FlagMcb.Checked;
            _flag_E = FlagEcb.Checked;
            _flag_P = FlagPcb.Checked;
            _tnStatus = TNStatusCb.Checked;
            _verbose = VerboseBtn.Checked;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}