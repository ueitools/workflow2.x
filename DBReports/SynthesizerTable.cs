using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using System.IO;
using System.Configuration;
using CommonForms;
using UEI;

namespace DBReports
{
    class SynthesizerTable
    {
        #region Private Fields
        private RichTextBox ReportTB;
        private CommonForms.IDSelectionForm IDSel;
        #endregion

        public DialogResult DoReport(RichTextBox RTB)
        {
            ReportTB = RTB;

            // IDSelectionForm from CommonForms Project
            IDSel = new CommonForms.IDSelectionForm();
            DialogResult res = IDSel.ShowDialog();
            IDSel.Close();

            if (res == DialogResult.OK)
            {
                // the Sort method selection form show up
                SynTableDlg SynTableSetup = new SynTableDlg();
                DialogResult result = SynTableSetup.ShowDialog();
                SynTableSetup.Close();
                if (result == DialogResult.OK)
                {
                    if (SynTableSetup.UseUEIDB)
                        DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
                    else
                        DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);
                    
                    SynthesizerTableRpt Progress = new SynthesizerTableRpt();
                  
                    Progress.IncludeIntron = SynTableSetup.IncludeIntron;

                    StringCollection allIDList = DBFunctions.NonBlockingGetAllIDList();
                    List<string> IDList = IDSel.GetSelectedIDList(allIDList);
                    
                    Progress.Show();
                    Progress.GenerateReport(IDList);
                    Progress.Close();
                }
            }

            IDSel.Dispose();
            return res;
        }

    }

    class SynthesizerTableRpt : ProgressDlg
    {
        #region Private Fields
        static string _traceFileName =
            CommonForms.Configuration.GetWorkingDirectory() +
            "\\SynthesizerTableReport.txt";
      
        private CommonFunctions commonFunc = new CommonFunctions();
        private bool _includeIntron;
        #endregion

        #region Public flag access
        public bool IncludeIntron { 
            get { return _includeIntron; }
            set { _includeIntron = value; }
        }
        #endregion

        public void GenerateReport(List<string> IDList)
        {
            SetupProgressBar(0, IDList.Count, 1);
            File.Delete(_traceFileName);

            try
            {
                using (StreamWriter sw = File.AppendText(_traceFileName))
                {
                    sw.WriteLine("************* Synthesizer Table *************");
                    sw.Close();
                }
                foreach (string ID in IDList)
                {
                    UpdateProgressBar();
                    TextMessage.Text = string.Format("Processing ID {0}", ID);
                    this.Update();
                    ProcessID(ID);
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

            // open the file
            string notepad_path = "notepad.exe";
            System.Diagnostics.Process.Start(notepad_path, _traceFileName);
      
        }

        private void ProcessID(string ID)
        {
            bool isInversedData = false;
            string syn;
            string zero_str_16 = "0000000000000000";
            Byte byte_data;
            Int16 int16_data;
            string data_str;

            CommonFunctions commonFunc = new CommonFunctions();
            FunctionCollection functions;
            string output_text = "";


            // Check whether this ID includes Inverse Data or not
            IDHeader IDHeaderData = DAOFactory.IDHeader().Select(ID);
            if (IDHeaderData.IsInversedData == "Y")
                isInversedData = true;
            

            if (this._includeIntron)
            {
                functions = DAOFactory.Function().SelectSynTableFunc(ID);

                if (functions == null)
                    return;
                else
                    output_text += "______________________________________________________\r\n";
               
                foreach (Function functionData in functions)
                {
                    if (functionData.Data.Length != 8 && functionData.Data.Length != 16)
                        continue;
                    else if (functionData.Data.Length == 8)
                    {
                        if (isInversedData)
                            byte_data = (Byte)~(Convert.ToByte(functionData.Data, 2));
                        else 
                            byte_data = Convert.ToByte(functionData.Data, 2);
                        data_str = Convert.ToString(byte_data, 2);
                        
                        if (data_str.Length < 8)
                            data_str = String.Concat(zero_str_16.Substring(0,8-data_str.Length),data_str);
                    }
                    else
                    {
                        if (isInversedData)
                            int16_data = (Int16)~(Convert.ToInt16(functionData.Data, 2));
                        else
                            int16_data = Convert.ToInt16(functionData.Data, 2);
                        data_str = Convert.ToString(int16_data, 2);
                        if (data_str.Length < 16)
                            data_str = String.Concat(zero_str_16.Substring(0, 16 - data_str.Length), data_str);
                    }
                    
                    syn = commonFunc.GetSynthesizer(data_str);
                    output_text += string.Format("{0,-8}{1,-16}{2,-8}{3,-16}\r\n",
                                functionData.ID, functionData.Label, syn, functionData.Intron);
                }
            }
            else // no intron
            {
                functions = DAOFactory.Function().SelectSynTableFuncNoIntron(ID);
                if (functions == null)
                    return;
                else
                    output_text += "______________________________________________________\r\n";
                foreach (Function functionData in functions)
                {
                    if (functionData.Data.Length != 8 && functionData.Data.Length != 16)
                        continue;

                    else if (functionData.Data.Length == 8)
                    {
                        if (isInversedData)
                            byte_data = (Byte)~(Convert.ToByte(functionData.Data, 2));
                        else
                            byte_data = Convert.ToByte(functionData.Data, 2);
                        data_str = Convert.ToString(byte_data, 2);
                        if (data_str.Length < 8)
                            data_str = String.Concat(zero_str_16.Substring(0, 8 - data_str.Length), data_str);
                    }
                    else
                    {
                        if (isInversedData)
                            int16_data = (Int16)~(Convert.ToInt16(functionData.Data, 2));
                        else
                            int16_data = Convert.ToInt16(functionData.Data, 2);
                        data_str = Convert.ToString(int16_data, 2);
                        if (data_str.Length < 16)
                            data_str = String.Concat(zero_str_16.Substring(0, 16 - data_str.Length), data_str);
                    }

                    syn = commonFunc.GetSynthesizer(data_str);
                    syn = commonFunc.GetSynthesizer(data_str);
                    output_text += string.Format("{0,-8}{1,-8}{2,-16}\r\n",
                                functionData.ID, syn, functionData.Label);
                }
            }

            if (output_text == "")
               output_text += "\r\n";
            StoreToFile(ref output_text);
        }

        private void StoreToFile(ref string output_holdback)
        {
            using (StreamWriter sw = File.AppendText(_traceFileName))
            {
                sw.WriteLine(output_holdback);
                sw.Close();
            }
            output_holdback = "";
        }

    }
}
