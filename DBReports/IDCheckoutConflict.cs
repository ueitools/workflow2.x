using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using DBTransfer;
using CommonForms;
using System.IO;

namespace DBReports
{
    public class IDCheckoutConflict
    {
        static string _traceFileName =
                CommonForms.Configuration.GetWorkingDirectory() +
                "\\IDCheckoutConflictReport.txt";

        public void DoReport(RichTextBox RTB)
        {
            CommonForms.IDSelectionForm slectDlg = new CommonForms.IDSelectionForm();
            if (slectDlg.ShowDialog() == DialogResult.OK)
            {
                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
                StringCollection allIDList = DBFunctions.NonBlockingGetAllIDList();
                List<string> idList = slectDlg.GetSelectedIDList(allIDList);
                GenerateReport(idList, RTB);
            }
        }

        public void GenerateReport(List<string> idList, RichTextBox ReportTB)
        {
            File.Delete(_traceFileName);
            ProgressDlg Progress = new ProgressDlg();
            Progress.Show();
            Progress.SetupProgressBar(0, idList.Count, 1);


            DataReport ConflictReport = new DataReport(DBConnectionString.UEIPUBLIC);
            try
            {
                string idReport = "";
                foreach (string id in idList)
                {
                    Progress.UpdateProgressBar();
                    Progress.BringToFront();
                    idReport = ConflictReport.reportIDConflict(id);

                    if (!string.IsNullOrEmpty(idReport))
                    {
                        //ReportTB.Text += string.Format("\r\nConflicts for ID {0}\r\n", id);
                        //ReportTB.Text += idReport;
                        using (StreamWriter sw = File.AppendText(_traceFileName))
                        {
                            sw.Write(string.Format("\r\nConflicts for ID {0}\r\n", id));
                            sw.Write(idReport);
                            sw.Close();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
            Progress.Close();

            // open the file
            string notepad_path = "notepad.exe";
            System.Diagnostics.Process.Start(notepad_path, _traceFileName);
        }
    }
}
