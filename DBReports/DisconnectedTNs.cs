using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using DBReports.Forms;
using System.IO;

namespace DBReports
{
    class DisconnectedTNs
    {
        private RichTextBox ReportTB;
        static string _traceFileName =
            CommonForms.Configuration.GetWorkingDirectory() +
            "\\DisconnectedTNsReport.txt";

        public void DoReport(RichTextBox RTB)
        {
            ReportTB = RTB;

            DisconnectedTNsDlg dlg = new DisconnectedTNsDlg();
            DialogResult result = dlg.ShowDialog();

            if (result == DialogResult.OK)
            {
                int FirstTn = dlg.FirstTn;
                int LastTn = dlg.LastTn;

                SetDatabase(dlg.UseMainDB);
                CreateReport(FirstTn, ref LastTn);
            }
        }

        private void CreateReport(int FirstTn, ref int LastTn)
        {
            ProgressDlg progress = new ProgressDlg();
            progress.Show();

            FunctionDAO TNConnection = new FunctionDAO();
            IList list = TNConnection.FindDisconnectedTNs();

            if (LastTn == -1)
                LastTn = (int)list[list.Count - 1];

            progress.SetupProgressBar(FirstTn, LastTn, 1);

            try
            {
                for (int TN = FirstTn; TN <= LastTn; TN++)
                {
                    if (list.Contains(TN))
                    {
                        //ReportTB.Text += string.Format("TN{0:D5} ", TN);
                        using (StreamWriter sw = File.AppendText(_traceFileName))
                        {
                            sw.WriteLine(string.Format("TN{0:D5} ", TN));
                            sw.Close();
                        }
                    }

                    progress.UpdateProgressBar();
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            finally
            {
                progress.Close();
                SetDatabase(false);
            }

            // open the file
            string notepad_path = "notepad.exe";
            System.Diagnostics.Process.Start(notepad_path, _traceFileName);
        }

        private static void SetDatabase(bool UseMainDB)
        {
            if (UseMainDB)
                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
            else
                DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);
        }
    }
}
