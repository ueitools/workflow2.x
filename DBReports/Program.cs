using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DBReports
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
//            Application.Run(new ReportForm(ReportType.UNIQUE_FUNC_IR));
            Application.Run(new ReportForm(ReportType.BRAND_TABLE_ROM));
        }
    }
}
