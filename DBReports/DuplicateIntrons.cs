using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using System.IO;
using System.Configuration;
using CommonForms;

namespace DBReports
{
    class DuplicateIntrons
    {
        #region Private Fields
        private RichTextBox ReportTB;
        private CommonForms.IDSelectionForm IDSel;
        #endregion

        public DialogResult DoReport(RichTextBox RTB)
        {
            ReportTB = RTB;

            // IDSelectionForm from CommonForms Project
            IDSel = new CommonForms.IDSelectionForm();
            DialogResult res = IDSel.ShowDialog();
            IDSel.Close();
            

            if (res == DialogResult.OK)
            {
                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
                DuplicateIntronRpt Progress = new DuplicateIntronRpt();

                // Get the list of IDs that we're gonna process
                StringCollection allIDList = DBFunctions.NonBlockingGetAllIDList();
                List<string> IDList = IDSel.GetSelectedIDList(allIDList);

                Progress.Show();
                Progress.GenerateReport(IDList);
                Progress.Close();
            }

            IDSel.Dispose();
            return res;
        }
    }

    class DuplicateIntronRpt : ProgressDlg
    {
        #region Private Fields
        static string _traceFileName =
            CommonForms.Configuration.GetWorkingDirectory() +
            "\\DuplicateIntronReport.txt";
        #endregion

        public void GenerateReport(List<string> IDList)
        {
            SetupProgressBar(0, IDList.Count, 1);
            File.Delete(_traceFileName);

            try
            {
                foreach (string ID in IDList)
                {
                    UpdateProgressBar();
                    TextMessage.Text = string.Format("Processing ID {0}", ID);
                    this.Update();
                    ProcessID(ID);
                }
                //string notepad_path = ConfigurationSettings.AppSettings["NOTEPAD_PATH"];
                string notepad_path = "notepad.exe";
                // open the file
                System.Diagnostics.Process.Start(notepad_path, _traceFileName);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        /*----------------------------------------------------------------------
         * ProcessID
         * 
         * To process groups of functions with duplicate Introns where
         * 1. None of them among the group have IntronPriority
         *    ==> Suggest IntronPriority by RID order, from 0
         * 2. Some of them among the group have IntronPriority (say up to x)
         *    ==> Suggest IntronPriority by RID order, from x+1
         * ----------------------------------------------------------------------*/
        /// <summary>
        /// process groups of functions with duplicate Introns
        /// </summary>
        /// <param name="ID"></param>
        private void ProcessID(string ID)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ID", ID);
            FunctionCollection functions = DAOFactory.Function().SelectIDFunc(paramList);
            if (functions == null)
                return;

            bool need_output = false;
            bool print_header = true;
            bool start = false;
            string output_holdback = "";
            int priority = 0;

            for (int it = 0; it < functions.Count; it++)
            {
                if (it < functions.Count - 1)
                {
                    if (functions[it].IntronPriority == null || functions[it + 1].IntronPriority == null)
                        continue;

                    if (functions[it].Intron.Equals(functions[it+1].Intron) &&
                        (functions[it].IntronPriority.Equals(functions[it+1].IntronPriority)) && 
                        (!functions[it].Data.Equals(functions[it+1].Data)))
                        need_output = true;
                    if (!start && functions[it].Intron.Equals(functions[it + 1].Intron))
                    {
                        start = true;
                    }
                }
                if (start)
                {
                    output_holdback = StoreToString(functions[it], output_holdback);//, ref priority);
                    if (need_output)
                        StoreToFile(ID, ref print_header, ref output_holdback);
                }
                if (it < functions.Count - 1)
                {
                    if (!functions[it].Intron.Equals(functions[it + 1].Intron) || !functions[it].IntronPriority.Equals(functions[it + 1].IntronPriority) ||
                        functions[it].Data.Equals(functions[it+1].Data))
                    {
                        start = false;
                        need_output = false;
                        output_holdback = "";
                    }
                }
            }
        }

        private string StoreToString(Function functions, string output_holdback)//, ref int priority)
        {
            if (!string.IsNullOrEmpty(functions.IntronPriority))
            {
                if (!string.IsNullOrEmpty(output_holdback))
                    output_holdback += "\n";
                output_holdback += ReportLine(functions, functions.IntronPriority);
            }
            else
            {
                if (!string.IsNullOrEmpty(output_holdback))
                    output_holdback += "\n";
                output_holdback += ReportLine(functions, "None");//, priority.ToString());
            }
            return output_holdback;
        }

        private void StoreToFile(string ID, ref bool print_header, ref string output_holdback)
        {
            using (StreamWriter sw = File.AppendText(_traceFileName))
            {
                if (print_header)
                {
                    sw.WriteLine(PrintHeader(ID));
                    print_header = false;
                }
                sw.WriteLine(output_holdback);
                sw.Close();
            }
            output_holdback = "";
        }

        private string PrintHeader(string ID)
        {
            string line;
            // Output the Header for ambiguous functions of current ID
            line = string.Format("\r\n ID: {0} \r\n", ID);
            line += string.Format(" {0,-16}   {1,-20}   {2,-12}   {3,-16}",
                                "Data", "Label", "Intron", "Priority");
            return line;
        }        

        private string ReportLine(Function functionData, 
                                string OriginalPriority)
        {
            string line;
            line = string.Format(
              " {0,-16}   {1,-20}   {2,-12}   {3,-16}",
              functionData.Data, functionData.Label,
              functionData.Intron, OriginalPriority);
            return line;
        }
    }
}
