using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using System.IO;

namespace DBReports
{
    class RestrictedID
    {
        #region Private Fields
        static string _traceFileName =
            CommonForms.Configuration.GetWorkingDirectory() +
            "\\RestrictedIDReport.txt";
        #endregion

        public DialogResult DoReport()
        {
            ProgressDlg Progress = new ProgressDlg();
            Progress.Show();
            GenerateStatusReport(Progress);
            Progress.Close();

            return DialogResult.OK;
          
        }

        private void GenerateStatusReport(ProgressDlg Progress)
        {
            // run report on public DB
            DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
        
            StringCollection list = DBFunctions.GetRestrictedIDList();
            Progress.SetupProgressBar(0, list.Count, 1);
            File.Delete(_traceFileName);
            try
            {
                foreach (string ID in list)
                {
                    Progress.UpdateProgressBar();
                    using (StreamWriter sw = File.AppendText(_traceFileName))
                    {
                        sw.WriteLine(ID);
                        sw.Close();
                    }
                }
                string notepad_path = "notepad.exe";
                // open the file
                System.Diagnostics.Process.Start(notepad_path, _traceFileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                // set back to temp DB
                DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);
            }
        }
    }
}
