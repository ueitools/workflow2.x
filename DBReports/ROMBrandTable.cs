using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using CommonForms;

namespace DBReports
{
    class BrandItem
    {
        public string Brand;
        public int BrandNum;
        public IList<string> IDPriority;
        public IList<string> IDList;
    }

    class ModeBrandItem
    {
        public string Mode;
        public IList<BrandItem> BrandInfo;
    }

    class ROMBrandTable
    {

        private IList<string> IDList = new List<string>();
        private IList<string> FromIDList = new List<string>();

        private IList<ModeBrandItem> BrandList = new List<ModeBrandItem>();

        private string IDListFilePath;
        private string BrandListPath;
        private string OutputFilePath;
        private string MarketList;

        public void DoReport()
        {
            Forms.ROMBrandTableDlg dlg = new DBReports.Forms.ROMBrandTableDlg();

            dlg.ShowDialog();
            if (dlg.DialogResult != DialogResult.OK)
                return;

            IDListFilePath = dlg.IDListFilePath;
            BrandListPath = dlg.BrandListPath;
            OutputFilePath = dlg.OutputFilePath;

            ReadIDList();
            MarketList = dlg.BuildMarketList();

            ReadBrandList();
            LookupBrandNums();
            GetBrandIDs();

            if (File.Exists(OutputFilePath))
                File.Delete(OutputFilePath);
            StreamWriter rpt = File.AppendText(OutputFilePath);

            WriteTable1(rpt);
            WriteTable2(rpt);
            WriteTable3(rpt);
            WriteTable4(rpt);
            rpt.Close();
        }

        private void ReadIDList()
        {
            char[] delim = { ' ', '-', ',', '\r', '\n', '\t' };
            string[] FileList = File.ReadAllLines(IDListFilePath);
            string ID;

            foreach (string line in FileList)
            {
                if (string.IsNullOrEmpty(line))
                    continue;

                string[] item = line.Split(delim, StringSplitOptions.RemoveEmptyEntries);

                ID = ValidateIDName(item[0]);
                if (string.IsNullOrEmpty(ID))
                    continue;
                else
                    FromIDList.Add(ID);

                if (item.Length > 1)
                    ID = ValidateIDName(item[1]);
                if (!string.IsNullOrEmpty(ID))
                    IDList.Add(ID);
            }
        }

        private void ReadBrandList()
        {
            char[] delim = { '-', ',', '\r', '\n', '\t', ' ' };
            string[] BrandFile = File.ReadAllLines(BrandListPath);

            ModeBrandItem MI = new ModeBrandItem();
            MI.BrandInfo = new List<BrandItem>();
            BrandItem BI = new BrandItem();

            foreach (string line in BrandFile)
            {
                if (string.IsNullOrEmpty(line))
                    continue;
                if (line.Trim()[0] == ':')
                {
                    if (!string.IsNullOrEmpty(MI.Mode))
                    {
                        MI.BrandInfo.Add(BI);
                        BI = new BrandItem();
                        BrandList.Add(MI);
                        MI = new ModeBrandItem();
                        MI.BrandInfo = new List<BrandItem>();
                    }
                    MI.Mode = line.Trim().Substring(1, 1);
                    continue;
                }
                if (line.Trim()[0] == '-')
                {
                    string[] LineItems = line.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string item in LineItems)
                    {
                        string ID = ValidateIDName(item);
                        if (!string.IsNullOrEmpty(ID))
                        {
                            if (BI.IDPriority == null)
                                BI.IDPriority = new List<string>();
                            BI.IDPriority.Add(ID);
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(BI.Brand))
                    {
                        MI.BrandInfo.Add(BI);
                        BI = new BrandItem();
                    }
                    BI.Brand = line.Trim();
                }
            }

            BrandList.Add(MI);
        }

        private string ValidateIDName(string ID)
        {
            ID = ID.Trim().ToUpper();
            if (!string.IsNullOrEmpty(ID) &&
                char.IsLetter(ID[0]) &&
                char.IsDigit(ID[1]))
            {
                int idnum = int.Parse(ID.Substring(1));
                return string.Format("{0}{1:D4}", ID[0], idnum);
            }
            return null;
        }

        private void LookupBrandNums()
        {
            BrandCollection AllBrands = DBFunctions.GetAllBrandList();

            IList<string> ABNames = new List<string>();
            foreach (Brand item in AllBrands)
                ABNames.Add(item.Name);

            foreach (ModeBrandItem list in BrandList)
            {
                for (int bli = 0; bli < list.BrandInfo.Count; bli++)
                {
                    if (ABNames.Contains(list.BrandInfo[bli].Brand))
                    {
                        int abi = ABNames.IndexOf(list.BrandInfo[bli].Brand);
                        list.BrandInfo[bli].BrandNum = AllBrands[abi].Code;
                    }
                    //else
                    // log error
                }
            }
        }

        private void GetBrandIDs()
        {
            elcinsql_Model.ModelWS WebBMS = new DBReports.elcinsql_Model.ModelWS();
            WebBMS.Url = CommonForms.WorkflowConfig.GetCfg("ModelWS");
            elcinsql_Model.UEIDeviceCodeListEx result;

            foreach (ModeBrandItem list in BrandList)
            {
                foreach (BrandItem item in list.BrandInfo)
                {
                    result = WebBMS.GetBSCList(item.Brand, list.Mode, MarketList, "CA|CY|EU", 5000, 0);
                    IList<string> List = new List<string>();
                    foreach (elcinsql_Model.UEIDeviceCode id in result.codeList)
                    {
                        if (!FromIDList.Contains(id.code))
                            continue;
                        if (!List.Contains(id.code))
                            List.Add(id.code);
                    }
                    // prioritise the ID list if given
                    if (item.IDPriority != null && item.IDPriority.Count > 0)
                        for (int idx = item.IDPriority.Count - 1; idx >= 0; idx-- )
                            if (List.Contains(item.IDPriority[idx]))
                            {
                                List.Remove(item.IDPriority[idx]);
                                List.Insert(0, item.IDPriority[idx]);
                            }

                    list.BrandInfo[list.BrandInfo.IndexOf(item)].IDList = List;
                }
            }
        }

        private void WriteTable1(StreamWriter rpt)
        {
            rpt.WriteLine(";***********************************************************************;");
            rpt.WriteLine(";         Manufacturer code look up table                               ;");
            rpt.WriteLine(";***********************************************************************;");
            rpt.WriteLine(" ");
            rpt.WriteLine("          public DEVBrandTable          ;Device Brand Table");
            rpt.WriteLine("DEVBrandTable:                          ;");
            rpt.WriteLine(string.Format("          db     {0,-2}                     ;", BrandList.Count));
            foreach (ModeBrandItem mode in BrandList)
            {
                rpt.WriteLine(string.Format("          dw     {0}_Mode                 ;", mode.Mode));
                rpt.WriteLine(string.Format("          dw     {0}_Brand                ;", mode.Mode));
            }
            rpt.WriteLine(" ");
        }

        private void WriteTable2(StreamWriter rpt)
        {
            rpt.WriteLine(";***********************************************************************;");
            rpt.WriteLine(" ");

            foreach (ModeBrandItem mode in BrandList)
            {
                int brandcount = 0;
                foreach (BrandItem item in mode.BrandInfo)
                    if (item.IDList.Count > 0) brandcount++;

                rpt.WriteLine(string.Format("{0}_Brand:                                ;", mode.Mode));
                rpt.WriteLine(string.Format("          db     {0,-2}                     ;", brandcount));
                foreach (BrandItem item in mode.BrandInfo)
                {
                    if (item.IDList.Count == 0)
                        continue;
                    rpt.WriteLine(
                        string.Format("          dw     {0:D4}                   ;{1}", 
                                                        item.BrandNum, item.Brand));
                }
                rpt.WriteLine(" ");
                foreach (BrandItem item in mode.BrandInfo)
                {
                    if (item.IDList.Count == 0)
                        continue;
                    rpt.WriteLine(
                        string.Format("          dw     B{0}{1:D4}                 ;{2}",
                                                        mode.Mode, item.BrandNum, item.Brand));
                }
            }
            rpt.WriteLine(" ");
        }

        private void WriteTable3(StreamWriter rpt)
        {
            IList<int> BNum = new List<int>();
            IList<string> BName = new List<string>();

            foreach (ModeBrandItem mode in BrandList)
            {
                foreach (BrandItem brand in mode.BrandInfo)
                {
                    if (brand.IDList.Count == 0)
                        continue;
                    if (!BNum.Contains(brand.BrandNum))
                    {
                        BNum.Add(brand.BrandNum);
                        BName.Add(brand.Brand);
                    }
                }
            }

            rpt.WriteLine(";***********************************************************************;");
            rpt.WriteLine(" ");
            rpt.WriteLine("          public BrandTable             ;Manufacture table");
            rpt.WriteLine("BrandTable:");
            rpt.WriteLine(string.Format("          dw     {0}", BNum.Count));
            for (int idx = 0; idx < BNum.Count; idx++)
            {
                rpt.WriteLine(
                    string.Format("          dw     {0:D4}                   ;{1}", BNum[idx], BName[idx]));
            }
            rpt.WriteLine(" ");
            for (int idx = 0; idx < BNum.Count; idx++)
            {
                rpt.WriteLine(
                    string.Format("          dw     BName{0:D4}              ;{1}", BNum[idx], BName[idx]));
            }
            rpt.WriteLine(" ");
            rpt.WriteLine("BrandTableEnd::");
            rpt.WriteLine(" ");
            for (int idx = 0; idx < BNum.Count; idx++)
            {
                rpt.WriteLine(
                    string.Format("BName{0:D4}:       .asciz            \"{1}\"", BNum[idx], BName[idx]));
            }
            rpt.WriteLine(" ");
        }

        private void WriteTable4(StreamWriter rpt)
        {
            rpt.WriteLine(";***********************************************************************;");
            rpt.WriteLine(" ");
            foreach (ModeBrandItem mode in BrandList)
            {
                foreach (BrandItem item in mode.BrandInfo)
                {
                    if (item.IDList.Count == 0)
                        continue;

                    rpt.WriteLine(
                        string.Format("B{0}{1:D4}:                                 ;    {2}",
                        mode.Mode, item.BrandNum, item.Brand));
                    rpt.WriteLine(string.Format("          db     {0}", item.IDList.Count));
                    foreach (string id in item.IDList)
                    {
                        rpt.WriteLine(
                            string.Format("          dw     {0}+{1}_Mode            ;{2}",
                            id.Substring(1),
                            id[0],
                            id));
                    }
                    rpt.WriteLine(" ");
                }
            }
        }
    }
}
