using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using System.IO;
using System.Configuration;
using CommonForms;

namespace DBReports
{
    class UniqueFuncIRCount
    {
        ProgressDlg Progress = new ProgressDlg();
            
        static string _traceFileName =
               CommonForms.Configuration.GetWorkingDirectory() +
               "\\UniqueFuncIRCntReport.txt";

        string[] Modes = {"A","B","C","D","E","F","G","H",
                              "I","J","K","L","M","N","O","P",
                              "Q","R","S","T","U","V","W","X",
                              "Y","Z"};
        int cntFunc = 0;
        int cntIR = 0;
        int totalFunc = 0;
        int totalIR = 0;
        public void DoReport()
        {
            DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
          
            File.Delete(_traceFileName);
            Progress.SetupProgressBar(0, Modes.Length, 1);
            Progress.Show();
            WriteHeader();
            foreach (string mode in Modes)
            {
                Progress.UpdateProgressBar();
                Progress.Text = string.Format(
                    "Processing Mode {0}", mode);
                Work(mode);
                WriteRecord(mode,cntFunc,cntIR);
            }
            WriteBottomLine();
            Progress.Close();

            string notepad_path = "notepad.exe";
            // open the file
            System.Diagnostics.Process.Start(notepad_path, _traceFileName);
            return;
        }

        void Work(string mode)
        {
            cntFunc = DAOFactory.Function().SelectUniqueFuncCount(mode);
            cntIR = DAOFactory.Function().SelectUniqueIRCount(mode);
            totalFunc += cntFunc;
            totalIR += cntIR;
           
        }

        void WriteRecord(string mode, int cntFunc, int cntIR)
        {
            if (cntFunc > 0)
            {
                using (StreamWriter sw = File.AppendText(_traceFileName))
                {
                    sw.WriteLine("{0,-5}  {1,-15}  {2,-15}", mode, cntFunc,
                        cntIR);
                    sw.Close();
                }
            }
        }

        void WriteHeader()
        {
            using (StreamWriter sw = File.AppendText(_traceFileName))
            {
                sw.WriteLine("{0,-5}  {1,-15}  {2,-15}", "Mode",
                    "Total Functions", "Total IR codes\r\n");
                sw.Close();
            }
        }

        void WriteBottomLine()
        {
            using (StreamWriter sw = File.AppendText(_traceFileName))
            {
                sw.WriteLine("\r\n{0,-5}  {1,-15}  {2,-15}", "Total",
                    totalFunc, totalIR);
                sw.Close();
            }
        }
    }
}
