﻿using BusinessObject;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace UEI.EDID.DataAccess
{
    public class EDIDDataAccess
    {
        #region Variables
        private DBOperations objDb = null;
        #endregion

        #region Properties
        public String ErrorMessage { get; set; }       
        #endregion

        #region Constrcuctor
        public EDIDDataAccess()
        {            
        }
        #endregion  


        #region Methods
        public void UpdateCapturedEDID(String RID, String base64EdId, String osd, String strCustomFP, String strCutomFPOSD, String str128FP, String str128FPOSD, String strOSDFP, String strBytePosition, String strCustomFingerPrint0, String strCustomFingerPrint1, String strCustomFingerPrint2, String strCustomFingerPrint3, String strPhysicalAddress)
        {
            this.ErrorMessage = String.Empty;
            try
            {
                this.objDb = new DBOperations(DBConnectionString.EdId);

                //Update EDID Table
                IDbCommand objCmd = objDb.CreateCommand();
                //objCmd.CommandType = CommandType.Text;
                //objCmd.CommandText = @"Update dbo.EDID set EDID = '" + base64EdId + "' where EDID_RID = " + RID;
                //objDb.ExecuteTextQuery(objCmd);


                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "EDID_Sp_TmpMigrate";
                objCmd.Parameters.Add(objDb.CreateParameter("@RID", DbType.Int64, ParameterDirection.Input, RID.Length, RID));
                if(!String.IsNullOrEmpty(osd))
                objCmd.Parameters.Add(objDb.CreateParameter("@OSD", DbType.String, ParameterDirection.Input, osd.Length, osd));
                if (!String.IsNullOrEmpty(strCustomFP))
                objCmd.Parameters.Add(objDb.CreateParameter("@CustomFP", DbType.String, ParameterDirection.Input, strCustomFP.Length, strCustomFP));
                if (!String.IsNullOrEmpty(strCutomFPOSD))
                objCmd.Parameters.Add(objDb.CreateParameter("@CustomFPOSD", DbType.String, ParameterDirection.Input, strCutomFPOSD.Length, strCutomFPOSD));
                if (!String.IsNullOrEmpty(str128FP))
                objCmd.Parameters.Add(objDb.CreateParameter("@128FP", DbType.String, ParameterDirection.Input, str128FP.Length, str128FP));
                if (!String.IsNullOrEmpty(str128FPOSD))
                objCmd.Parameters.Add(objDb.CreateParameter("@128FPOSD", DbType.String, ParameterDirection.Input, str128FPOSD.Length, str128FPOSD));
                if (!String.IsNullOrEmpty(strOSDFP))
                objCmd.Parameters.Add(objDb.CreateParameter("@OSDFP", DbType.String, ParameterDirection.Input, strOSDFP.Length, strOSDFP));
                if (!String.IsNullOrEmpty(strBytePosition))
                objCmd.Parameters.Add(objDb.CreateParameter("@BytePosition", DbType.String, ParameterDirection.Input, strBytePosition.Length, strBytePosition));

                if (!String.IsNullOrEmpty(strCustomFingerPrint0))
                    objCmd.Parameters.Add(objDb.CreateParameter("@Type0", DbType.String, ParameterDirection.Input, strCustomFingerPrint0.Length, strCustomFingerPrint0));

                if (!String.IsNullOrEmpty(strCustomFingerPrint1))
                    objCmd.Parameters.Add(objDb.CreateParameter("@Type1", DbType.String, ParameterDirection.Input, strCustomFingerPrint1.Length, strCustomFingerPrint1));

                if (!String.IsNullOrEmpty(strCustomFingerPrint2))
                    objCmd.Parameters.Add(objDb.CreateParameter("@Type2", DbType.String, ParameterDirection.Input, strCustomFingerPrint2.Length, strCustomFingerPrint2));

                if (!String.IsNullOrEmpty(strCustomFingerPrint3))
                    objCmd.Parameters.Add(objDb.CreateParameter("@Type3", DbType.String, ParameterDirection.Input, strCustomFingerPrint3.Length, strCustomFingerPrint3));

                if (!String.IsNullOrEmpty(strCustomFingerPrint3))
                    objCmd.Parameters.Add(objDb.CreateParameter("@PhysicalAddress", DbType.String, ParameterDirection.Input, strCustomFingerPrint3.Length, strPhysicalAddress));

                objDb.ExecuteSP(objCmd);

                //Insert into Fingerprint History
//                objCmd.CommandText = @"insert into dbo.FingerPrintHistory(FK_EDID_RID,CustomFingerPrint,[128FP],CustomFPplusOSD,[128FPplusOSD],BytePosition,OSDdata,OSDFP) 
//                                        values(" + RID + "," + strCustomFP + "," + str128FP + "," + strCutomFPOSD + "," + str128FPOSD + "," + strBytePosition + "," + osd + "," + strOSDFP + ")";                
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
        }
        public DataTable GetCapturedEDID()
        {
            DataTable dtResult = null;
            this.ErrorMessage = String.Empty;
            try
            {
                this.objDb = new DBOperations(DBConnectionString.EdId);

                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.Text;
                objCmd.CommandText = @"select	[RID]		= T1.EDID_RID,
		                                        [EDID]		= T1.EDID,
		                                        [FileStructure]	= T1.EDID_Uei2Bin
		                                         from dbo.EDID T1
                                        inner join dbo.DataSources T2
                                        on T1.FK_Source_RID = T2.Source_RID
                                        where T2.Name like 'Capture EDID'";
                dtResult = objDb.ExecuteDataTable(objCmd);                
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dtResult;
        }
        public DataTable GetGapsInEdIdTable()
        {
            this.ErrorMessage = String.Empty;
            DataTable dtResult = null;
            try
            {
                this.objDb = new DBOperations(DBConnectionString.EdId);
                //SystemFlag : 3 - Redecoding existing EDID
                //System Flag: 0 - New EDID Decoding
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.Text;
                objCmd.CommandText = @"SELECT DISTINCT 
                                        T1.EDID_RID,
                                        T1.EDID 
                                        FROM dbo.EDID T1 
                                        LEFT JOIN DecodedEDIDData T2
                                        ON T1.EDID_RID = T2.FK_EDID_RID
		                                INNER JOIN DBO.DataSources T3
		                                ON T1.FK_Source_RID = T3.Source_RID
                                        WHERE  ISNULL(T2.FK_EDID_RID,'')=''
                                        AND T1.LK_StandardEDID_RID IS NULL 
                                        AND T1.SystemFlags = 0
		                                AND T3.Name = 'Capture EDID'
                                UNION
                                SELECT DISTINCT 
                                        T1.EDID_RID,
                                        T1.EDID 
                                        FROM dbo.EDID T1 
		                                INNER JOIN DBO.DataSources T3
		                                ON T1.FK_Source_RID = T3.Source_RID
                                        WHERE  T1.LK_StandardEDID_RID IS NULL 
                                        AND T1.SystemFlags = 3
		                                AND T3.Name = 'Capture EDID'
                                        ORDER BY T1.EDID_RID";
                dtResult = objDb.ExecuteDataTable(objCmd);
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dtResult;
        }
        public DataTable CreateDecodedEDIDDataTable()
        {
            this.ErrorMessage = String.Empty;
            DataTable dtScemaDataTable = new DataTable();            
            try
            {
                SqlConnection objCon = new SqlConnection(DBConnectionString.EdId);
                objCon.Open();
                SqlCommand objCmd = new SqlCommand(@"DECLARE @sample [dbo].[DecodedEDID];
                                                    SELECT * FROM @sample;", objCon);
                SqlDataReader objReader = objCmd.ExecuteReader();
                DataTable dtResult = objReader.GetSchemaTable();
                objReader.Close();
                objCon.Close();
                if (dtResult != null)
                {
                    foreach (DataRow schemaRow in dtResult.Rows)
                    {
                        String ColumnName = schemaRow["ColumnName"].ToString();
                        if (ColumnName == "RID")
                            continue;
                        if (ColumnName == "MonitorNameExt")
                            continue;                        
                        Type DataType = (Type)(schemaRow["DataType"]);
                        var col = new DataColumn(ColumnName, DataType);
                        dtScemaDataTable.Columns.Add(col);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dtScemaDataTable;
        }
        public Int32 SetEdIdValidity(Int64 RID, Int32 status)
        {
            Int32 rowsAffected = 0;
            this.ErrorMessage = String.Empty;            
            try
            {
                this.objDb = new DBOperations(DBConnectionString.EdId);
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "[dbo].[EDID_Sp_SetEDIDValidity]";
                objCmd.Parameters.Add(objDb.CreateParameter("@nEDID_RID", DbType.Int64, ParameterDirection.Input, RID.ToString().Length, RID));
                objCmd.Parameters.Add(objDb.CreateParameter("@nSystemFlags", DbType.UInt32, ParameterDirection.Input, status.ToString().Length, status));
                rowsAffected = objDb.ExecuteSP(objCmd);                
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return rowsAffected;
        }
        public Int32 UpdateDecodedEdId(DataTable decodedEdIdTable)
        {
            Int32 rowsAffected = 0;
            this.ErrorMessage = String.Empty;
            try
            {
                this.objDb = new DBOperations(DBConnectionString.EdId);
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "[dbo].[EDID_Sp_UpdateDecodedEDID]";
                objCmd.Parameters.Add(objDb.CreateParameter("@InsertDecodedEDID", SqlDbType.Structured, ParameterDirection.Input, decodedEdIdTable));
                rowsAffected = objDb.ExecuteSP(objCmd);
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return rowsAffected;
        }
        public Int32 UpdateCorrectedEdId(Int64 RID, String correctedEDID)
        {
            Int32 rowsAffected = 0;
            this.ErrorMessage = String.Empty;
            try
            {
                this.objDb = new DBOperations(DBConnectionString.EdId);
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "[dbo].[EDID_Sp_UpdateCorrectedEDID]";
                objCmd.Parameters.Add(objDb.CreateParameter("@nEDID_RID", DbType.UInt64, ParameterDirection.Input,RID.ToString().Length, RID));
                objCmd.Parameters.Add(objDb.CreateParameter("@vcharEDID", DbType.String, ParameterDirection.Input, correctedEDID.Length, correctedEDID));
                rowsAffected = objDb.ExecuteSP(objCmd);
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return rowsAffected;
        }
        public IList<Hashtable> GetXBoxEdIDDataEx()
        {
            String m_Message = String.Empty;
            IList<Hashtable> edidList = null;
            using (new ConnectionSwitcher())
            {
                //DAOFactory.ResetDBConnection(DBConnectionString.XBox);
                DAOFactory.ResetDBConnection(DBConnectionString.EdId);
                IDbFunctionsWrapper m_Wrapper = new DbFunctionsWrapper();
                edidList = m_Wrapper.GetXBoxEdIDDataEx();
            }
            return edidList;
        }
        public String UpdateEdIdXBoxData(Int32 edidRID, Byte[] m_FileStructure, String customFingerPrint, String customFPPlusOSD, String str128FP, String str128FPPlusOSD, String osdData, String osdFP, String bytePosition, String strCustomFingerPrint0, String strCustomFingerPrint1, String strCustomFingerPrint2, String strCustomFingerPrint3, String strPhysicalAddress)
        {
            String m_Message = String.Empty;
            using (new ConnectionSwitcher())
            {
                DAOFactory.ResetDBConnection(DBConnectionString.EdId);
                IDbFunctionsWrapper m_Wrapper = new DbFunctionsWrapper();
                m_Message = m_Wrapper.UpdateEdIdXBoxData(edidRID, m_FileStructure, customFingerPrint, customFPPlusOSD, str128FP, str128FPPlusOSD, osdData, osdFP, bytePosition,strCustomFingerPrint0,strCustomFingerPrint1,strCustomFingerPrint2,strCustomFingerPrint3,strPhysicalAddress);
            }
            return m_Message;
        }
        public String UpdateEDIDData(String m_MainDevice, String m_SubDevice, String m_Component, String m_Brand, String m_Model, String m_Countries, Byte[] m_FileStructure, String m_Description, Int32 m_Flag, String Base64EDID, String CustomFingerPrint, String CustomFPPlusOSD, String Only128FP, String Only128FPPlusOSD, String OSDData, String OSDFP, String BytePosition, String strCustomFingerPrint0, String strCustomFingerPrint1, String strCustomFingerPrint2, String strCustomFingerPrint3)
        {
            String m_Message = String.Empty;
            using (new ConnectionSwitcher())
            {
                DAOFactory.ResetDBConnection(DBConnectionString.EdId);
                IDbFunctionsWrapper m_Wrapper = new DbFunctionsWrapper();
                m_Message = m_Wrapper.UpdateEdIdData(m_MainDevice, m_SubDevice, m_Component, m_Brand, m_Model, m_Countries, m_FileStructure, m_Description, m_Flag, Base64EDID, CustomFingerPrint, CustomFPPlusOSD, Only128FP, Only128FPPlusOSD, OSDData, OSDFP, BytePosition,strCustomFingerPrint0,strCustomFingerPrint1,strCustomFingerPrint2,strCustomFingerPrint3);
            }
            return m_Message;
        }
        public HashtableCollection GetIDIDData(IDSearchParameter parameter)
        {
            HashtableCollection edidList = null;
            using (new ConnectionSwitcher())
            {
                DAOFactory.ResetDBConnection(DBConnectionString.EdId);
                IDbFunctionsWrapper m_Wrapper = new DbFunctionsWrapper();
                edidList = m_Wrapper.GetEdIdData(parameter);
            }
            return edidList;
        }
        #endregion
    }
    #region Data Base Connection
    internal class ConnectionSwitcher : IDisposable
    {
        private string _lastConn;
        public ConnectionSwitcher()
        {
            _lastConn = DAOFactory.GetDBConnectionString();
            if (_lastConn != DBConnectionString.UEIPUBLIC &&
                _lastConn != DBConnectionString.UEITEMP &&
                _lastConn != DBConnectionString.UEITEMP_INDIA)
                DAOFactory.ResetDBConnection(DBConnectionString.EdId);
        }        
        public void Dispose()
        {
            DAOFactory.ResetDBConnection(_lastConn);
        }
    }
    #endregion
}