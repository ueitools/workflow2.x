﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.ServiceProvider.Forms
{
    public partial class SubLocationEditorForm : Form
    {
        public enum ProceeeTypes
        {            
            ByServiceProvider,
            ByLocation,
            Attach,
            Detach
        }
        #region Variables
        private ProgressForm m_Progress = null;
        private BackgroundWorker m_BackgroundWorker = null;
        private SubLocationEditor m_SubLocationService = null;        
        #endregion

        #region Properties
        public ServiceProviders         SelectedServiceProvider { get; set; }
        public SubLocationModel         SubLocationsByServiceProvider { get; set; }
        public SubLocationModel         SubLocationsByLocation { get; set; }
        public ProceeeTypes             ProcessType { get; set; }
        public TreeNode                 SelectedLocationTreeNode { get; set; }
        public SubLocation              SelectedSubLocation { get; set; }
        #endregion

        #region Constructor
        private SubLocationEditorForm()
        {
            InitializeComponent();
        }
        public SubLocationEditorForm(ServiceProviders serviceProvider)
            : this()
        {
            this.SelectedServiceProvider = serviceProvider;
            this.Load += SubLocationEditorForm_Load;
        }
        void SubLocationEditorForm_Load(object sender, EventArgs e)
        {
            this.m_SubLocationService                   = new SubLocationEditor();
            this.m_BackgroundWorker                     = new BackgroundWorker();
            this.m_BackgroundWorker.DoWork              += m_BackgroundWorker_DoWork;
            this.m_BackgroundWorker.RunWorkerCompleted  += m_BackgroundWorker_RunWorkerCompleted;
            this.Timer.Tick                             += Timer_Tick;
            this.treeViewLocation.AfterSelect           += treeViewLocation_AfterSelect;
            this.availableSubLocationsDataGridView.MouseDown += DataGridViewMouseDown;
            this.subLocationsDataGridView.MouseDown     += DataGridViewMouseDown;
            this.txtSearch1.KeyUp                       += txtSearch1_KeyUp;
            this.txtSearch2.KeyUp                       += txtSearch2_KeyUp;          
            this.ProcessType                            = ProceeeTypes.ByServiceProvider;
            ResetTimer();            
            BindLocations();
        }                        
        #endregion

        #region Location Selected
        void treeViewLocation_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //if (e.Action != TreeViewAction.Unknown)
            //{
                TreeNode node = ((TreeNode)e.Node);
                if (node != null)
                {                    
                    SelectedLocationTreeNode = node;
                    this.ProcessType = ProceeeTypes.ByLocation;
                    ResetTimer();
                }
            //}
        }
        #endregion

        #region Background Progress
        private String m_Message = "Please Wait...";
        private void ResetTimer()
        {
            if (m_BackgroundWorker != null || m_BackgroundWorker.IsBusy == false)
                m_BackgroundWorker.RunWorkerAsync();

            if (m_Progress == null)
            {
                m_Progress = new ProgressForm();
                m_Progress.StartPosition = FormStartPosition.CenterParent;
            }
            m_Progress.SetMessage(m_Message);
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();
            Timer.Start();
        }
        private void ProgressMessage()
        {
            if (this.m_Progress != null)
                m_Progress.SetMessage(m_Message, "Please Wait...");
        }
        void Timer_Tick(object sender, EventArgs e)
        {
            if (this.m_Progress != null)
                ProgressMessage();
        }
        void m_BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Timer.Stop();
            m_Progress.DialogResult = DialogResult.OK;
            m_Progress.Close();
            if (!String.IsNullOrEmpty(this.m_SubLocationService.ErrorMessage))
            {
                MessageBox.Show(this.m_SubLocationService.ErrorMessage, "Service Provider", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                switch (ProcessType)
                {
                    case ProceeeTypes.ByServiceProvider:
                        BindSubLocationByServiceProvider();
                        break;
                    case ProceeeTypes.ByLocation:
                        BindSubLocationByLocation();
                        break;
                    case ProceeeTypes.Attach:
                        if (!String.IsNullOrEmpty(m_SubLocationService.ErrorMessage))
                            MessageBox.Show(m_SubLocationService.ErrorMessage, "Service Provider", MessageBoxButtons.OK, MessageBoxIcon.Error);                        
                        break;
                    case ProceeeTypes.Detach:
                        if (!String.IsNullOrEmpty(m_SubLocationService.ErrorMessage))
                            MessageBox.Show(m_SubLocationService.ErrorMessage, "Service Provider", MessageBoxButtons.OK, MessageBoxIcon.Error);                       
                        break;                   
                }
            } 
        }
        void m_BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (ProcessType)
            {
                case ProceeeTypes.ByServiceProvider:                    
                    LoadSubLocationByServiceProvider();
                    break;
                case ProceeeTypes.ByLocation:
                    LoadSubLocationByLocation();
                    break;     
                case ProceeeTypes.Attach:
                    Attach();                    
                    break;
                case ProceeeTypes.Detach:
                    Detach();                    
                    break;                
            }                                   
        }
        #endregion

        #region Event

        void txtSearch2_KeyUp(object sender, KeyEventArgs e)
        {
            //if ((e.KeyCode != Keys.Back) && (e.KeyCode != Keys.Delete))
            //{
                if ((this.SubLocationsByLocation != null) && (!String.IsNullOrEmpty(txtSearch2.Text)))
                {
                    this.subLocationsDataGridView.ClearSelection();
                    SubLocationModel m_SearchResult = this.SubLocationsByLocation.GetSublocationByZipCode(txtSearch2.Text);
                    if (m_SearchResult != null)
                    {
                        this.subLocationsDataGridView.DataSource = null;
                        this.subLocationsDataGridView.DataSource = m_SearchResult;
                        this.subLocationsDataGridView.Rows[0].Cells[1].Selected = false;
                        //this.subLocationsDataGridView.Columns["SubLocationRID"].Visible = false;
                    }
                    else
                    {
                        this.subLocationsDataGridView.DataSource = null;
                    }
                }
            //}
            if (String.IsNullOrEmpty(txtSearch2.Text))
            {
                BindSubLocationByLocation();
            }
        }
        void txtSearch1_KeyUp(object sender, KeyEventArgs e)
        {
            //if ((e.KeyCode != Keys.Back) && (e.KeyCode != Keys.Delete))
            //{
                if ((this.SubLocationsByServiceProvider != null) && (!String.IsNullOrEmpty(txtSearch1.Text)))
                {
                    this.availableSubLocationsDataGridView.ClearSelection();
                    SubLocationModel m_SearchResult = this.SubLocationsByServiceProvider.GetSublocationByZipCode(txtSearch1.Text);
                    if (m_SearchResult != null)
                    {
                        this.availableSubLocationsDataGridView.DataSource = null;
                        this.availableSubLocationsDataGridView.DataSource = m_SearchResult;
                        this.availableSubLocationsDataGridView.Rows[0].Cells[1].Selected = false;
                        //this.availableSubLocationsDataGridView.Columns["SubLocationRID"].Visible = false;
                    }
                    else
                    {
                        this.availableSubLocationsDataGridView.DataSource = null;
                    }
                }
            //}
            if (String.IsNullOrEmpty(txtSearch1.Text))
            {
                BindSubLocationByServiceProvider();
            }
        }
        #endregion

        #region Locations
        void BindLocations()
        {
            if (this.SelectedServiceProvider != null)
            {
                if (this.SelectedServiceProvider.Location != null)
                {
                    this.treeViewLocation.Nodes.Clear();
                    foreach (Locations m_Location in this.SelectedServiceProvider.Location)
                    {
                        if (!this.treeViewLocation.Nodes.ContainsKey(m_Location.Region))
                        {
                            this.treeViewLocation.Nodes.Add(m_Location.Region, m_Location.Region);
                            this.treeViewLocation.Nodes[m_Location.Region].Tag = "Region";
                        }
                        if (!String.IsNullOrEmpty(m_Location.Country))
                        {
                            if (!this.treeViewLocation.Nodes[m_Location.Region].Nodes.ContainsKey(m_Location.Country))
                            {
                                this.treeViewLocation.Nodes[m_Location.Region].Nodes.Add(m_Location.Country, m_Location.Country);
                                this.treeViewLocation.Nodes[m_Location.Region].Nodes[m_Location.Country].Tag = "Country";
                            }
                        }
                        if (!String.IsNullOrEmpty(m_Location.SubLocation))
                        {
                            if (!this.treeViewLocation.Nodes[m_Location.Region].Nodes[m_Location.Country].Nodes.ContainsKey(m_Location.SubLocation))
                            {
                                this.treeViewLocation.Nodes[m_Location.Region].Nodes[m_Location.Country].Nodes.Add(m_Location.SubLocation, m_Location.SubLocation);
                                this.treeViewLocation.Nodes[m_Location.Region].Nodes[m_Location.Country].Nodes[m_Location.SubLocation].Tag = "SubLocation";
                            }
                        }
                    }
                    if (this.treeViewLocation.Nodes.Count > 0)
                        if (this.treeViewLocation.Nodes[0].Nodes.Count > 0)
                            if (this.treeViewLocation.Nodes[0].Nodes[0].Nodes.Count > 0)
                                this.treeViewLocation.SelectedNode = this.treeViewLocation.Nodes[0].Nodes[0].Nodes[0];
                            else
                                this.treeViewLocation.SelectedNode = this.treeViewLocation.Nodes[0].Nodes[0];
                }
            }
        }
        #endregion

        #region Sub Locations By Service Provider
        void LoadSubLocationByServiceProvider()
        {
            try
            {
                this.m_Message = "Loading Province, City and ZIP code based on selected Service Provider";
                m_SubLocationService = new SubLocationEditor();                
                if(this.SelectedServiceProvider != null)
                {
                    //this.SubLocationsByServiceProvider = new SubLocationModel();
                    this.SubLocationsByServiceProvider = m_SubLocationService.GetAvailableSubLocationsByServiceProvider(this.SelectedServiceProvider.ServiceProviderRID);
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Service Provider", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void BindSubLocationByServiceProvider()
        {
            if (this.availableSubLocationsDataGridView.Rows.Count > 0)
            {
                this.availableSubLocationsDataGridView.ClearSelection();
                this.availableSubLocationsDataGridView.DataSource = null;
            }
            if (this.SubLocationsByServiceProvider != null)
            {
                this.availableSubLocationsDataGridView.DataSource = this.SubLocationsByServiceProvider;
                if (this.SubLocationsByServiceProvider.Count > 0)
                {
                    this.availableSubLocationsDataGridView.Columns["SubLocationRID"].Visible = false;
                    this.availableSubLocationsDataGridView.Rows[0].Cells[1].Selected = false;
                }
            }
            else
            {
                MessageBox.Show("No Province, City, ZIP Code found for this Service Provider", "Service Provider", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        #endregion       

        #region Sub Locations By Locations
        void LoadSubLocationByLocation()
        {
            try
            {
                this.m_Message = "Loading Province, City and ZIP code based on selected Location";
                //m_SubLocationService = new SubLocationEditor();
                if (this.SelectedServiceProvider != null && SelectedLocationTreeNode != null)
                {
                    String strRegion = String.Empty;
                    String strCountry = String.Empty;
                    String strSubLocation = String.Empty;

                    String SelectedItem = SelectedLocationTreeNode.Text;
                    switch (SelectedLocationTreeNode.Tag.ToString())
                    {
                        case "Region":
                            strRegion = SelectedItem;
                            break;
                        case "Country":
                            strRegion = SelectedLocationTreeNode.Parent.Text;
                            strCountry = SelectedItem;
                            break;
                        case "SubLocation":
                            strRegion = SelectedLocationTreeNode.Parent.Parent.Text;
                            strCountry = SelectedLocationTreeNode.Parent.Text;
                            strSubLocation = SelectedItem;
                            break;
                    }
                    this.SubLocationsByLocation = m_SubLocationService.GetAvailableSubLocationsByLocation(strRegion,strCountry,strSubLocation);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Service Provider", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void BindSubLocationByLocation()
        {
            if (this.subLocationsDataGridView.Rows.Count > 0)
            {
                this.subLocationsDataGridView.ClearSelection();
                this.subLocationsDataGridView.DataSource = null;
            }
            if (this.SubLocationsByLocation != null)
            {
                this.subLocationsDataGridView.DataSource = this.SubLocationsByLocation;
                if (this.SubLocationsByLocation.Count > 0)
                {                    
                    this.subLocationsDataGridView.Columns["SubLocationRID"].Visible = false;                    
                }
            }
            else
            {
                MessageBox.Show("No Province, City, ZIP Code found for this Location", "Service Provider", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        #endregion       

        #region DataGridView Mouse Click
        private void DataGridViewMouseDown(Object Sender, MouseEventArgs e)
        {            
            DataGridView dataGridView = (DataGridView)Sender;
            if (dataGridView != null)
            {
                DataGridView.HitTestInfo hitTestInfo = dataGridView.HitTest(e.X, e.Y);
                if (hitTestInfo.RowIndex != -1 && hitTestInfo.ColumnIndex == -1)
                {                                   
                    dataGridView.ClearSelection();
                    if (dataGridView.Rows[hitTestInfo.RowIndex].Cells["SubLocationRID"].Value.ToString() != String.Empty)
                    {
                        dataGridView.Rows[hitTestInfo.RowIndex].Selected = true;
                        this.SelectedSubLocation = new SubLocation();
                        this.SelectedSubLocation = (SubLocation)dataGridView.Rows[hitTestInfo.RowIndex].DataBoundItem;
                        if (dataGridView.Name.Contains("availableSubLocationsDataGridView"))
                            contextMenuDetach.Show(dataGridView, new Point(e.X, e.Y));
                        else if (dataGridView.Name.Contains("subLocationsDataGridView"))
                            contextMenuAttach.Show(dataGridView, new Point(e.X, e.Y));
                    }
                }
            }
        }
        #endregion

        #region Attach\Detach Province, City, ZIP Code
        private void attachToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ProcessType = ProceeeTypes.Attach;
            ResetTimer();
            if (String.IsNullOrEmpty(m_SubLocationService.ErrorMessage))                
            {                
                this.ProcessType = ProceeeTypes.ByServiceProvider;
                ResetTimer();                
            }
        }
        private void detachToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ProcessType = ProceeeTypes.Detach;
            ResetTimer();
            if (String.IsNullOrEmpty(m_SubLocationService.ErrorMessage))
            {                
                this.ProcessType = ProceeeTypes.ByServiceProvider;
                ResetTimer();                
            }
        }
        void Attach()
        {
            try
            {
                this.m_Message = "Attach Province, City and ZIP code for selected Service Provider";
                //m_SubLocationService = new SubLocationEditor();
                if (this.SelectedServiceProvider != null && this.SelectedSubLocation != null)
                {
                    m_SubLocationService.AttachSubLocation(this.SelectedServiceProvider.ServiceProviderRID.ToString(), this.SelectedSubLocation.SubLocationRID.ToString());                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Service Provider", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void Detach()
        {
            try
            {
                this.m_Message = "Detach Province, City and ZIP code for selected Service Provider";
                //m_SubLocationService = new SubLocationEditor();
                if (this.SelectedServiceProvider != null && this.SelectedSubLocation != null)
                {
                    m_SubLocationService.DetachSubLocation(this.SelectedServiceProvider.ServiceProviderRID.ToString(), this.SelectedSubLocation.SubLocationRID.ToString());                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Service Provider", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
    }
}