﻿using CommonForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.ServiceProvider.Forms
{
    public partial class ServiceProviderView : Form
    {
        #region Variables
        private ProgressForm m_Progress                         = null;
        private BackgroundWorker m_BackgroundWorker             = null;
        private SubLocationEditorForm m_SubLocationEditor       = null;
        private ServiceProviderViewer m_ServiceProviderServices = null;
        private ModelInfoForm m_ModelInfo                       = null;
        #endregion

        #region Properties
        public ServiceProviderModel ServiceProviders { get; set; }
        public ServiceProviderModel SelectedServiceProviders { get; set; }        
        public TreeNode             SelectedLocationTreeNode { get; set; }
        public TreeNode             SelectedDeviceTypeTreeNode { get; set; }
        public ServiceProviders     SelectedServiceProvider { get; set; }
        public MainForm             ParentWindow { get; set; }
        public IList<String>        SelectedLoad { get; set; }
        #endregion

        #region Constructor
        public ServiceProviderView()
        {
            InitializeComponent();           
            this.Load += ServiceProviderView_Load;
        }
        void ServiceProviderView_Load(object sender, EventArgs e)
        {
            this.m_BackgroundWorker                     = new BackgroundWorker();
            this.m_BackgroundWorker.DoWork              += m_BackgroundWorker_DoWork;
            this.m_BackgroundWorker.RunWorkerCompleted  += m_BackgroundWorker_RunWorkerCompleted;
            this.Timer.Tick                             += Timer_Tick;
            this.treeViewLocation.BeforeSelect          += treeViewLocation_BeforeSelect;
            this.treeViewLocation.AfterSelect           += treeViewLocation_AfterSelect;
            this.treeViewDeviceTypes.BeforeSelect       += treeViewDeviceTypes_BeforeSelect;
            this.treeViewDeviceTypes.AfterSelect        += treeViewDeviceTypes_AfterSelect;
            ResetTimer();
        }                        
        #endregion

        #region Background Progress
        private String m_Message = "Please Wait...";
        private void ResetTimer()
        {
            if (m_BackgroundWorker != null || m_BackgroundWorker.IsBusy == false)
                m_BackgroundWorker.RunWorkerAsync();

            if (m_Progress == null)
            {
                m_Progress = new ProgressForm();
                m_Progress.StartPosition = FormStartPosition.CenterParent;
            }
            m_Progress.SetMessage(m_Message);
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();
            Timer.Start();
        }
        private void ProgressMessage()
        {
            if (this.m_Progress != null)
                m_Progress.SetMessage(m_Message, "Please Wait...");
        }
        void Timer_Tick(object sender, EventArgs e)
        {
            if (this.m_Progress != null)
                ProgressMessage();
        }
        void m_BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Timer.Stop();
            m_Progress.DialogResult = DialogResult.OK;
            m_Progress.Close();
            ParentWindow.lblStatus.Text = String.Empty;
            if (this.m_ServiceProviderServices != null && (String.IsNullOrEmpty(m_ServiceProviderServices.ErrorMessage)))
            {
                if (this.SelectedLocationTreeNode != null || this.SelectedDeviceTypeTreeNode != null || this.SelectedLoad != null)
                {
                    if (this.SelectedServiceProviders != null)
                    {
                        BindFilteredServiceProviders();
                    }
                    else
                    {
                        this.dataGridView.DataSource = null;
                        ParentWindow.lblStatus.Text = "No Records Found";
                        MessageBox.Show("No Data Found", "Service Providers", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    if (this.ServiceProviders != null)
                    {
                        BindServiceProviders();
                        BindLocations();
                        BindDeviceTypes();
                    }
                    else
                    {
                        ParentWindow.lblStatus.Text = "No Records Found";
                        this.dataGridView.DataSource = null;
                        MessageBox.Show("No Data Found", "Service Providers", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            else
            {
                MessageBox.Show(m_ServiceProviderServices.ErrorMessage, "Service Providers", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void m_BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            LoadServiceProviders();
        }
        #endregion

        #region Device Type Tree View Selected
        void treeViewDeviceTypes_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeNode node = ((TreeNode)e.Node);
                if (node != null)
                {
                    this.SelectedLoad = null;
                    this.treeViewLocation.SelectedNode = null;
                    this.SelectedLocationTreeNode = null;
                    SelectedDeviceTypeTreeNode = node;                    
                    ResetTimer();
                }
            }
        }
        void treeViewDeviceTypes_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Action == TreeViewAction.Unknown)
                e.Cancel = true;
        }
        #endregion

        #region Location Tree View Selected
        void treeViewLocation_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeNode node = ((TreeNode)e.Node);
                if (node != null)
                {
                    this.SelectedLoad = null;
                    this.treeViewDeviceTypes.SelectedNode = null;
                    this.SelectedDeviceTypeTreeNode = null;
                    SelectedLocationTreeNode = node;                    
                    ResetTimer();
                }
            }
        }
        void treeViewLocation_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Action == TreeViewAction.Unknown)
                e.Cancel = true;
        }
        #endregion

        #region Methods
        void BindServiceProviders()
        {
            ParentWindow.lblStatus.Text = String.Empty;
            this.dataGridView.DataSource = null;
            if (this.ServiceProviders != null)
            {
                this.dataGridView.DataSource = this.ServiceProviders;
                if (this.ServiceProviders.Count > 0)
                {
                    this.dataGridView.Columns[0].Visible = false;
                    ParentWindow.lblStatus.Text = "Records Found: " + this.ServiceProviders.Count.ToString();
                }
                this.dataGridView.MouseDown += new MouseEventHandler(DataGridViewMouseDown);
            }
        }
        void BindFilteredServiceProviders()
        {
            ParentWindow.lblStatus.Text = String.Empty;
            this.dataGridView.DataSource = null;
            if (this.SelectedServiceProviders != null)
            {
                this.dataGridView.DataSource = this.SelectedServiceProviders;
                if (this.SelectedServiceProviders.Count > 0)
                {
                    this.dataGridView.Columns[0].Visible = false;
                    ParentWindow.lblStatus.Text = "Records Found: " + this.SelectedServiceProviders.Count.ToString();
                }
                this.dataGridView.MouseDown += new MouseEventHandler(DataGridViewMouseDown);
            }
        }        
        void BindLocations()
        {
            if (this.m_ServiceProviderServices.ServiceProviderLocations != null)
            {
                Dictionary<String, Dictionary<String, List<String>>> allLocations = this.m_ServiceProviderServices.ServiceProviderLocations.GetAllLocations();
                if (allLocations != null)
                {
                    this.treeViewLocation.Nodes.Clear();
                    foreach (String m_Region in allLocations.Keys)
                    {
                        if (!this.treeViewLocation.Nodes.ContainsKey(m_Region))
                        {
                            this.treeViewLocation.Nodes.Add(m_Region, m_Region);
                            this.treeViewLocation.Nodes[m_Region].Tag = "Region";
                        }
                        foreach (String m_Country in allLocations[m_Region].Keys)
                        {
                            if (!String.IsNullOrEmpty(m_Country))
                            {
                                if (!this.treeViewLocation.Nodes[m_Region].Nodes.ContainsKey(m_Country))
                                {
                                    this.treeViewLocation.Nodes[m_Region].Nodes.Add(m_Country, m_Country);
                                    this.treeViewLocation.Nodes[m_Region].Nodes[m_Country].Tag = "Country";
                                }
                            }
                            foreach (String m_SubLocation in allLocations[m_Region][m_Country])
                            {
                                if (!String.IsNullOrEmpty(m_SubLocation))
                                {
                                    if (!this.treeViewLocation.Nodes[m_Region].Nodes[m_Country].Nodes.ContainsKey(m_SubLocation))
                                    {
                                        this.treeViewLocation.Nodes[m_Region].Nodes[m_Country].Nodes.Add(m_SubLocation, m_SubLocation);
                                        this.treeViewLocation.Nodes[m_Region].Nodes[m_Country].Nodes[m_SubLocation].Tag = "SubLocation";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        void BindDeviceTypes()
        {
            if (this.m_ServiceProviderServices.ServiceProviderDeviceTypes != null)
            {
                Dictionary<String, Dictionary<String, List<String>>> allDeviceTypes = this.m_ServiceProviderServices.ServiceProviderDeviceTypes.GetAllDeviceTypes();
                if (allDeviceTypes != null)
                {
                    this.treeViewDeviceTypes.Nodes.Clear();
                    foreach (String m_MainDevice in allDeviceTypes.Keys)
                    {
                        if (!this.treeViewDeviceTypes.Nodes.ContainsKey(m_MainDevice))
                        {
                            this.treeViewDeviceTypes.Nodes.Add(m_MainDevice, m_MainDevice);
                            this.treeViewDeviceTypes.Nodes[m_MainDevice].Tag = "MainDeviceType";
                        }
                        foreach (String m_SubDeviceType in allDeviceTypes[m_MainDevice].Keys)
                        {
                            if (!String.IsNullOrEmpty(m_SubDeviceType))
                            {
                                if (!this.treeViewDeviceTypes.Nodes[m_MainDevice].Nodes.ContainsKey(m_SubDeviceType))
                                {
                                    this.treeViewDeviceTypes.Nodes[m_MainDevice].Nodes.Add(m_SubDeviceType, m_SubDeviceType);
                                    this.treeViewDeviceTypes.Nodes[m_MainDevice].Nodes[m_SubDeviceType].Tag = "SubDeviceType";
                                }
                            }
                            foreach (String m_SubLocation in allDeviceTypes[m_MainDevice][m_SubDeviceType])
                            {
                                if (!String.IsNullOrEmpty(m_SubLocation))
                                {
                                    if (!this.treeViewDeviceTypes.Nodes[m_MainDevice].Nodes[m_SubDeviceType].Nodes.ContainsKey(m_SubLocation))
                                    {
                                        this.treeViewDeviceTypes.Nodes[m_MainDevice].Nodes[m_SubDeviceType].Nodes.Add(m_SubLocation, m_SubLocation);
                                        this.treeViewDeviceTypes.Nodes[m_MainDevice].Nodes[m_SubDeviceType].Nodes[m_SubLocation].Tag = "SubLocation";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        void FilterServiceProviders()
        {
            this.SelectedServiceProviders = null;
            if (this.SelectedLoad != null)
            {
                foreach (ServiceProviders item in this.ServiceProviders)
                {                    
                    if (!String.IsNullOrEmpty(item.ID) && this.SelectedLoad.Contains(item.ID))
                    {
                        if (this.SelectedServiceProviders == null)
                            this.SelectedServiceProviders = new ServiceProviderModel();
                        this.SelectedServiceProviders.Add(item);
                    }                    
                }
            }
            if (this.SelectedLocationTreeNode != null)
            {
                String SelectedItem = SelectedLocationTreeNode.Text;
                Dictionary<String, List<String>> m_Location = null;
                switch (SelectedLocationTreeNode.Tag.ToString())
                {
                    case "Region":
                        foreach (ServiceProviders item in this.ServiceProviders)
                        {
                            m_Location = item.GetServiceProviderByRegion(SelectedItem);
                            if (m_Location != null)
                            {
                                if (this.SelectedServiceProviders == null)
                                    this.SelectedServiceProviders = new ServiceProviderModel();
                                this.SelectedServiceProviders.Add(item);
                            }
                            m_Location = null;
                        }
                        break;
                    case "Country":
                        foreach (ServiceProviders item in this.ServiceProviders)
                        {
                            m_Location = item.GetServiceProviderByCountry(SelectedLocationTreeNode.Parent.Text, SelectedItem);
                            if (m_Location != null)
                            {
                                if (this.SelectedServiceProviders == null)
                                    this.SelectedServiceProviders = new ServiceProviderModel();
                                this.SelectedServiceProviders.Add(item);
                            }
                            m_Location = null;
                        }
                        break;
                    case "SubLocation":
                        foreach (ServiceProviders item in this.ServiceProviders)
                        {
                            m_Location = item.GetServiceProviderBySubLocation(SelectedLocationTreeNode.Parent.Parent.Text,SelectedLocationTreeNode.Parent.Text, SelectedItem);
                            if (m_Location != null)
                            {
                                if (this.SelectedServiceProviders == null)
                                    this.SelectedServiceProviders = new ServiceProviderModel();
                                this.SelectedServiceProviders.Add(item);
                            }
                            m_Location = null;
                        }
                        break;
                }
            }
            if (this.SelectedDeviceTypeTreeNode != null)
            {
                String SelectedItem = SelectedDeviceTypeTreeNode.Text;
                Dictionary<String, List<String>> m_DeviceType = null;
                switch (SelectedDeviceTypeTreeNode.Tag.ToString())
                {
                    case "MainDeviceType":
                        foreach (ServiceProviders item in this.ServiceProviders)
                        {
                            m_DeviceType = item.GetServiceProviderByMainDeviceType(SelectedItem);
                            if (m_DeviceType != null)
                            {
                                if (this.SelectedServiceProviders == null)
                                    this.SelectedServiceProviders = new ServiceProviderModel();
                                this.SelectedServiceProviders.Add(item);
                            }
                            m_DeviceType = null;
                        }
                        break;
                    case "SubDeviceType":
                        foreach (ServiceProviders item in this.ServiceProviders)
                        {
                            m_DeviceType = item.GetServiceProviderBySubDeviceType(SelectedItem);
                            if (m_DeviceType != null)
                            {
                                if (this.SelectedServiceProviders == null)
                                    this.SelectedServiceProviders = new ServiceProviderModel();
                                this.SelectedServiceProviders.Add(item);
                            }
                            m_DeviceType = null;
                        }
                        break;
                    case "SubLocation":
                        foreach (ServiceProviders item in this.ServiceProviders)
                        {
                            m_DeviceType = item.GetServiceProviderBySubLocation1(SelectedItem);
                            if (m_DeviceType != null)
                            {
                                if (this.SelectedServiceProviders == null)
                                    this.SelectedServiceProviders = new ServiceProviderModel();
                                this.SelectedServiceProviders.Add(item);
                            }
                            m_DeviceType = null;
                        }
                        break;
                }
            }
        }
        void LoadServiceProviders()
        {
            try
            {
                this.m_Message = "Loading Service Providers";
                if (this.SelectedLocationTreeNode != null || this.SelectedDeviceTypeTreeNode != null || this.SelectedLoad != null)
                {
                    FilterServiceProviders();
                }
                else
                {
                    m_ServiceProviderServices = new ServiceProviderViewer();
                    this.ServiceProviders = m_ServiceProviderServices.GetServiceProviders();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Service Provider", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region DataGridView Mouse Click
        private void DataGridViewMouseDown(Object Sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hitTestInfo = this.dataGridView.HitTest(e.X, e.Y);
            if (hitTestInfo.RowIndex != -1 && hitTestInfo.ColumnIndex == -1)
            {
                //if (this.dataGridView.Rows[hitTestInfo.RowIndex].Cells["EdIdRawData"].Value.ToString() != String.Empty)                
                this.dataGridView.ClearSelection();
                this.dataGridView.Rows[hitTestInfo.RowIndex].Selected = true;
                this.SelectedServiceProvider = (ServiceProviders)this.dataGridView.Rows[hitTestInfo.RowIndex].DataBoundItem;
                this.contextMenu.Show(this.dataGridView, new Point(e.X, e.Y));
            }
        }
        #endregion

        #region Sub Location Editor
        private void subLocationEditorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.SelectedServiceProvider != null)
            {
                m_SubLocationEditor = new SubLocationEditorForm(this.SelectedServiceProvider);
                //m_SubLocationEditor.ServiceProviderServices = this.m_ServiceProviderServices;
                m_SubLocationEditor.ShowDialog();
            }            
        }
        #endregion

        #region Filter
        private void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                this.treeViewDeviceTypes.SelectedNode = null;
                this.treeViewLocation.SelectedNode = null;
                this.SelectedLocationTreeNode = null;
                this.SelectedDeviceTypeTreeNode = null;
                this.SelectedLoad = null;
                IList<String> idlList = GetIDs();
                if (idlList != null && idlList.Count > 0)
                {
                    SelectedLoad = idlList;
                    ResetTimer();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Service Provider", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } 
        }        
        private IList<String> GetIDs()
        {
            IList<string> idList = null;
            IDSelectionForm idSelectionForm = null;
            try
            {
                //Use id selection form object
                idSelectionForm = new IDSelectionForm();
                DialogResult result = idSelectionForm.ShowDialog();
                if (result == DialogResult.OK)
                {                    
                    idList = idSelectionForm.SelectedIDList;
                    if (idList == null || idList.Count == 0)
                        MessageBox.Show("No ids retrieved for the selection.");
                }
                return idList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idSelectionForm != null)
                {
                    idSelectionForm.Dispose();
                }
            }
        }        
        #endregion

        #region Model Info
        private void modelInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.SelectedServiceProvider != null)
            {
                if (!String.IsNullOrEmpty(this.SelectedServiceProvider.ID))
                {
                    m_ModelInfo = new ModelInfoForm();
                    m_ModelInfo.SelectedID = this.SelectedServiceProvider.ID;
                    m_ModelInfo.SelectedBrand = this.SelectedServiceProvider.Brand;
                    m_ModelInfo.SelectedModel = this.SelectedServiceProvider.Model;
                    m_ModelInfo.ShowDialog();
                }
                else
                {
                    MessageBox.Show("ID is not available", "Service Provider", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        #endregion        
    }
}
