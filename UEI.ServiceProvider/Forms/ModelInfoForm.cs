﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.ServiceProvider.Forms
{
    public partial class ModelInfoForm : Form
    {
        #region Variables
        private ProgressForm m_Progress = null;
        private BackgroundWorker m_BackgroundWorker = null;        
        #endregion

        #region Properties
        public String SelectedID { get; set; }
        public String StatusMessage { get; set; }
        public DeviceCollection ModelInfo { get; set; }
        public String SelectedModel { get; set; }
        public String SelectedBrand { get; set; }
        #endregion

        #region Constrcutor
        public ModelInfoForm()
        {
            InitializeComponent();
            this.Load += ModelInfoForm_Load;
        }
        void ModelInfoForm_Load(object sender, EventArgs e)
        {
            if ((!String.IsNullOrEmpty(this.SelectedBrand)) && (!String.IsNullOrEmpty(this.SelectedModel)))
                this.Text = this.Text + (" for ID: " + this.SelectedID + ", Brand: " + this.SelectedBrand + ", Model: " + this.SelectedModel);
            else if (!String.IsNullOrEmpty(this.SelectedBrand))
                this.Text = this.Text + (" for ID: " + this.SelectedID + ", Brand: " + this.SelectedBrand);
            else if (!String.IsNullOrEmpty(this.SelectedModel))
                this.Text = this.Text + (" for ID: " + this.SelectedID + ", Model: " + this.SelectedModel);
            else
                this.Text = this.Text + (" for ID: " + this.SelectedID);
            this.m_BackgroundWorker = new BackgroundWorker();
            this.m_BackgroundWorker.DoWork += m_BackgroundWorker_DoWork;
            this.m_BackgroundWorker.RunWorkerCompleted += m_BackgroundWorker_RunWorkerCompleted;
            this.Timer.Tick += Timer_Tick;
            ResetTimer();
        }        
        #endregion

        #region Background Progress
        private String m_Message = "Please Wait...";
        private void ResetTimer()
        {
            if (m_BackgroundWorker != null || m_BackgroundWorker.IsBusy == false)
                m_BackgroundWorker.RunWorkerAsync();

            if (m_Progress == null)
            {
                m_Progress = new ProgressForm();
                m_Progress.StartPosition = FormStartPosition.CenterParent;
            }
            m_Progress.SetMessage(m_Message);
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();
            Timer.Start();
        }
        private void ProgressMessage()
        {
            if (this.m_Progress != null)
                m_Progress.SetMessage(m_Message, "Please Wait...");
        }
        void Timer_Tick(object sender, EventArgs e)
        {
            if (this.m_Progress != null)
                ProgressMessage();
        }
        void m_BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Timer.Stop();
            m_Progress.DialogResult = DialogResult.OK;
            m_Progress.Close();
            BindModelInfo();
        }
        void m_BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!String.IsNullOrEmpty(this.SelectedID))
                LoadModelInfo();
        }
        #endregion

        #region Method
        void LoadModelInfo()
        {
            try
            {
                m_Message = "Loading Model Info for ID: "+ SelectedID;
                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
                this.ModelInfo = DAOFactory.Device().SelectID(SelectedID);
            }
            catch (Exception ex)
            {
                this.StatusMessage = ex.Message;
            }
            
        }
        void BindModelInfo()
        {
            this.dataGridView.DataSource = null;
            if (this.ModelInfo != null)
            {
                this.dataGridView.DataSource = this.ModelInfo;
                if (!String.IsNullOrEmpty(this.SelectedModel))
                {
                    Device IDDeviceRow = null;
                    foreach (DataGridViewRow row in this.dataGridView.Rows)
                    {
                        IDDeviceRow = (Device)row.DataBoundItem;
                        if (IDDeviceRow.Model.Contains(this.SelectedModel) && IDDeviceRow.Brand.Contains(this.SelectedBrand))
                            row.DefaultCellStyle.BackColor = Color.LightGreen;
                    }
                }
                if (this.dataGridView.Columns.Contains("ID"))
                    this.dataGridView.Columns["ID"].Visible = false;
                if (this.dataGridView.Columns.Contains("RID"))
                    this.dataGridView.Columns["RID"].Visible = false;
                if (this.dataGridView.Columns.Contains("Brand_Code"))
                    this.dataGridView.Columns["Brand_Code"].Visible = false;
                if (this.dataGridView.Columns.Contains("Manufacturer"))
                    this.dataGridView.Columns["Manufacturer"].Visible = false;
                if (this.dataGridView.Columns.Contains("Manufacturer_Code"))
                    this.dataGridView.Columns["Manufacturer_Code"].Visible = false;
                if (this.dataGridView.Columns.Contains("UEIProduct"))
                    this.dataGridView.Columns["UEIProduct"].Visible = false;
                if (this.dataGridView.Columns.Contains("UEIProduct_Code"))
                    this.dataGridView.Columns["UEIProduct_Code"].Visible = false;
                if (this.dataGridView.Columns.Contains("Date"))
                    this.dataGridView.Columns["Date"].Visible = false;
                if (this.dataGridView.Columns.Contains("Collector"))
                    this.dataGridView.Columns["Collector"].Visible = false;
                if (this.dataGridView.Columns.Contains("PurchaseYear"))
                    this.dataGridView.Columns["PurchaseYear"].Visible = false;
                if (this.dataGridView.Columns.Contains("PriceInDigit"))
                    this.dataGridView.Columns["PriceInDigit"].Visible = false;
                if (this.dataGridView.Columns.Contains("ProductManual"))
                    this.dataGridView.Columns["ProductManual"].Visible = false;
                if (this.dataGridView.Columns.Contains("Source_RID"))
                    this.dataGridView.Columns["Source_RID"].Visible = false;
                if (this.dataGridView.Columns.Contains("Text"))
                    this.dataGridView.Columns["Text"].Visible = false;
            }            
        }
        #endregion
    }
}