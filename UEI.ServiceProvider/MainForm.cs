﻿using CommonForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using UEI.ServiceProvider.Forms;

namespace UEI.ServiceProvider
{
    public partial class MainForm : Form
    {
        #region Variables
        private ServiceProviderView m_ServiceProviderView = null;
        #endregion

        #region Properties
        
        #endregion

        #region Form Load
        public MainForm()
        {
            InitializeComponent();
            this.Load += MainForm_Load;
        }
        void MainForm_Load(object sender, EventArgs e)
        {
            this.lblCopyRights.Text = String.Format("Copyright ©{0} UEIC", DateTime.Now.Year);
            if (this.m_ServiceProviderView == null)
                this.m_ServiceProviderView = new ServiceProviderView();
            this.m_ServiceProviderView.MdiParent = this;
            this.m_ServiceProviderView.ParentWindow = this;
            this.m_ServiceProviderView.WindowState = FormWindowState.Maximized;
            this.m_ServiceProviderView.Show();
        }
        #endregion

        #region Exit
        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Set Working Directory
        private void workingDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Configuration.SetWorkingDirectory();
        }
        #endregion
    }
}