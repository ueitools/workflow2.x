using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using BusinessObject;

namespace DBTransfer
{
    /// <summary>
    /// delete files if equal 
    /// </summary>
    public static class Comparer
    {
        public static bool IsEqual(HashtableCollection list1,
            HashtableCollection list2)
        {
            string file1 = "list1.xml";
            string file2 = "list2.xml";

            file1 = FileToWorkingDirectory(file1);
            file2 = FileToWorkingDirectory(file2);

            list1.ToXML(file1);
            list2.ToXML(file2);

            bool isEqual = FileComparer.Compare(file1, file2);
            if (isEqual == true)
            {
                File.Delete(file1);
                File.Delete(file2);
            }

            return isEqual;
        }

        public static bool IsEqual(PartialID oID1, PartialID oID2)
        {
            string file1 = String.Format("{0}_1.xml", oID1.Header.ID);
            string file2 = String.Format("{0}_2.xml", oID2.Header.ID);

            file1 = FileToWorkingDirectory(file1);
            file2 = FileToWorkingDirectory(file2);

            oID1.ToXML(file1);
            oID2.ToXML(file2);

            bool isEqual = FileComparer.Compare(file1, file2);
            if (isEqual == true)
            {
                File.Delete(file1);
                File.Delete(file2);
            }

            return isEqual;
        }

        public static bool IsEqual(TN oTN1, TN oTN2)
        {
            string file1 = String.Format("{0}_1.xml", oTN1.Header.TN);
            string file2 = String.Format("{0}_2.xml", oTN2.Header.TN);

            file1 = FileToWorkingDirectory(file1);
            file2 = FileToWorkingDirectory(file2);

            oTN1.ToXML(file1);
            oTN2.ToXML(file2);

            bool isEqual = FileComparer.Compare(file1, file2);
            if (isEqual == true)
            {
                File.Delete(file1);
                File.Delete(file2);
            }

            return isEqual;
        }

        public static string FileToWorkingDirectory(string file)
        {
            string workDir = CommonForms.Configuration.GetWorkingDirectory();
            return workDir + file;
        }
    }
}
