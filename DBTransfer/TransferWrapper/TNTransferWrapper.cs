using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BusinessObject;
using CommonForms;

namespace DBTransfer
{
    static class TNTransferWrapper
    {
        public static string UEITempConnectionString = DBConnectionString.UEITEMP;

        /// <summary>
        /// TN related check out
        /// </summary>
        public static void CheckOut()
        {
            List<int> tnList = GetTNListFromUser("Check Out TN");
            if (tnList.Count == 0)
                return;

            ITransfer transfer = new TNTransfer(tnList, UEITempConnectionString);
            try
            {
                transfer.CheckOut();
                MessageBox.Show("Check Out finished successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void CheckIn()
        {
            List<int> tnList = GetTNListFromUser("Check In TN");
            if (tnList.Count == 0)
                return;

            ITransfer transfer = new TNTransfer(tnList, UEITempConnectionString);
            try
            {
                transfer.CheckIn();
                MessageBox.Show("Check In finished successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        #region Private Methods
        private static List<int> GetTNListFromUser(string title)
        {
            List<int> tnList = new List<int>();

            InputForm dlg = new InputForm();
            dlg.SetLabel("Enter TN:");
            dlg.SetTitle(title);

            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                try
                {
                    int tn = Int32.Parse(dlg.Value);
                    tnList.Add(tn);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error! Operation Aborted");
                }
            }

            return tnList;
        }
        #endregion
    }
}
