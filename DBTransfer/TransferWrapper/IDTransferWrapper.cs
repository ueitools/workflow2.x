using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Transactions;
using System.IO;

using BusinessObject;
using CommonForms;

namespace DBTransfer
{
    public static class IDTransferWrapper
    {
        static bool _isBatchProcessing = false;
        static string _traceFileName = 
            CommonForms.Configuration.GetWorkingDirectory() + 
            "batchTrace.txt";
        public static string UEITempConnectionString = DBConnectionString.UEITEMP;

        /// <summary>
        /// Respect TN Lock
        /// </summary>
        public static void CheckOut()
        {
            List<string> idList = GetIDListFromUser("Check Out ID", false);
            if (idList.Count > 0)
                _CheckOut(idList);
        }

        /// <summary>
        /// 
        /// </summary>
        public static void CheckIn()
        {
            AccessRestrictForm AccessRestrict = new AccessRestrictForm();
            DialogResult result = AccessRestrict.ShowDialog();
            AccessRestrict.Close();
            if (result == DialogResult.OK)
            {
                List<string> idList = GetIDListFromUser("Check In ID", true);
                if (idList.Count > 0)
                    _CheckIn(idList);
            }
            else if (result == DialogResult.No)
            { 
                MessageBox.Show("Password Incorrect.", 
                    "Password Error", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void Create()
        {
            NewIDForm dlg = new NewIDForm();
            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.Cancel)
                return;

            IDHeader idHeader = null;
            if (dlg.BaseID != "") {
                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
                idHeader = DAOFactory.IDHeader().Select(dlg.BaseID);
            }

            DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);
            DataStore tempDataStore = new DataStore(DBConnectionString.UEITEMP);
            try
            {
                DAOFactory.BeginTransaction();
                string newID = dlg.NewID;
                
                PartialID partialID = null;
                try {
                    partialID = tempDataStore.Load(newID);
                } catch {}
                
                if (partialID != null && partialID.Header != null) {
                    tempDataStore.EraseIDAndShadow(newID);
                }

                tempDataStore.Create(newID, idHeader, dlg.Exec);
                DAOFactory.CommitTransaction();

                /// now check out
                List<string> idList = new List<string>();
                idList.Add(newID);

                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
                PartialID.Lock(newID);

                MessageBox.Show(string.Format("{0} created successfully.", newID));
            }
            catch (Exception ex)
            {
                DAOFactory.RollBackTransaction();
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void Export()
        {
            ExportIDForm dlg = new ExportIDForm();
            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.Cancel)
                return;

            List<string> idList = new List<string>();
            idList.Add(dlg.ID);
            ITransferEx transfer = new IDTransfer(idList, UEITempConnectionString);
            int timeOut = transfer.GetTimeOut();

            try
            {
                transfer.Export(dlg.DestConnStr, dlg.SrcConnStr);

                MessageBox.Show("Export ID finished successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void RevertEditsAndUnlock()
        {
            List<string> idList = GetIDListFromUser("Revert and Unlock ID", false);
            if (idList.Count == 0)
                return;

            IDTransfer transfer = new IDTransfer(idList, UEITempConnectionString);
            int timeOut = transfer.GetTimeOut();

            try
            {
                transfer.Erase();

                WarnTNConflict(idList);
                MessageBox.Show("Revert and Unlock ID " + 
                    "finished successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// get idList from IDSelectionForm and proceed with idList
        /// each check in treated as independent 
        /// set _isBatchProcessing to true and restore back to false
        /// reset trace file every time
        /// </summary>
        public static void BatchCheckIn()
        {
            AccessRestrictForm AccessRestrict = new AccessRestrictForm();
            DialogResult res = AccessRestrict.ShowDialog();
            AccessRestrict.Close();
            if (res == DialogResult.OK)
            {
                IDSelectionForm dlg = new IDSelectionForm();
                DialogResult result = dlg.ShowDialog();
                if (result == DialogResult.Cancel) {
                    return;
                }

                DBSelectionForm dbSelectionForm = new DBSelectionForm();
                dbSelectionForm.SetLabel("Select the database to check in from");
                dbSelectionForm.Text = "Database Selection";
                if (dbSelectionForm.ShowDialog() == DialogResult.Cancel) {
                    return;
                }
                UEITempConnectionString = dbSelectionForm.ConnectionString;

                DataStore tempDS = new DataStore(dbSelectionForm.ConnectionString);
                List<string> batchIDList = dlg.GetSelectedIDList(tempDS.GetAllIDList());

                _BatchCheckIn(batchIDList);

                TextReportViewerForm viewer = new TextReportViewerForm();
                viewer.Open(_traceFileName);
            }
            else if (res == DialogResult.No)
            {
                MessageBox.Show("Password Incorrect.", "Password Error", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// get idList from IDSelectionForm and proceed with idList
        /// each check out treated as independent 
        /// set _isBatchProcessing to true and restore back to false
        /// reset trace file every time
        /// </summary>
        public static void BatchCheckOut()
        {
            IDSelectionForm dlg = new IDSelectionForm();
            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.Cancel)
                return;

            TextReportViewerForm viewer = new TextReportViewerForm();

            DataStore publicDS = new DataStore(DBConnectionString.UEIPUBLIC);
            List<string> batchIDList = dlg.GetSelectedIDList(
                publicDS.GetAllIDList());

            _BatchCheckOut(batchIDList);
            viewer.Open(_traceFileName);
        }

        public static void Delete()
        {
            List<string> idList = GetIDListFromUser("Delete ID from Main DB", false);
            if (idList.Count == 0)
                return;

            IDTransfer transfer = new IDTransfer(idList, UEITempConnectionString);
            int timeOut = transfer.GetTimeOut();

            try
            {
                transfer.EraseIdFromMainDB();
                MessageBox.Show("Delete ID finished successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        #region Private Methods
        public static void _BatchCheckIn(List<string> batchIDList)
        {
            _isBatchProcessing = true;
            File.CreateText(_traceFileName);

            foreach (string id in batchIDList)
            {
                List<string> idList = new List<string>();
                idList.Add(id);
                _CheckIn(idList);
            }
            _isBatchProcessing = false;
            string notepad_path = "notepad.exe";
            System.Diagnostics.Process.Start(notepad_path, _traceFileName);
        }

        public static void _BatchCheckOut(List<string> batchIDList)
        {
            _isBatchProcessing = true;
            File.CreateText(_traceFileName);

            foreach (string id in batchIDList)
            {
                List<string> idList = new List<string>();
                idList.Add(id);
                _CheckOut(idList);
            }
            _isBatchProcessing = false;
        }

        private static void _CheckOut(List<string> idList)
        {
            ITransferEx transfer = new IDTransfer(idList, UEITempConnectionString);
            int timeOut = transfer.GetTimeOut();

            try
            {
                transfer.CheckOut();
                Trace(ListToString(idList) + " checked out successfully");
            }
            catch (Exception ex)
            {
                RichTextBox RTB = new RichTextBox();
                DBReports.IDCheckoutConflict Report = new DBReports.IDCheckoutConflict();
                RTB.Text += string.Format("{0} not checked out - {1}\n",
                                                    ListToString(idList),
                                                    ex.Message);
                Report.GenerateReport(idList, RTB);
                Trace(RTB.Text);
            }
        }

        /// <summary>
        /// batch ready
        /// </summary>
        /// <param name="idList"></param>
        private static void _CheckIn(List<string> idList)
        {
            IDTransfer transfer = new IDTransfer(idList, UEITempConnectionString);
            int timeOut = transfer.GetTimeOut();

            try
            {
                transfer.CheckIn();

                WarnTNConflict(idList);
                Trace(ListToString(idList) + 
                    " checked in successfully");
            }
            catch (Exception ex)
            {
                string msg = ListToString(idList) +
                    " not checked in - " + ex.Message;
                Trace(msg);
            }
        }

        private static List<string> GetIDListFromUser(string title, bool showDbSelection)
        {
            List<string> idList = new List<string>();

            TransferInputForm dlg = new TransferInputForm();
            dlg.SetLabel("Enter ID:");
            dlg.SetTitle(title);
            dlg.ShowDbSelection(showDbSelection);

            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                idList.Add(dlg.Value);
                UEITempConnectionString = dlg.DbConnectionString;
            }

            return idList;
        }

        private static void WarnTNConflict(IList<string> idList)
        {
            DataReport publicDR = new DataReport(DBConnectionString.UEIPUBLIC);
            string message = publicDR.reportTNConflict(idList);

            if (message.Length > 0)
            {
                message = "Conflicted TNs are found!\r\n\r\n" + message;
                MessageBox.Show(message, "Warning");
            }
        }

        private static string ListToString(IList<string> list)
        {
            string returnStr = "";
            foreach (string str in list)
            {
                if (returnStr != "")
                    returnStr += ", ";

                returnStr += str; 
            }

            return returnStr;
        }

        private static void Trace(string msg)
        {
            if (_isBatchProcessing == true)
            {
                using (StreamWriter sw = File.AppendText(_traceFileName))
                {
                    sw.WriteLine(msg);
                    sw.Close();
                }
            }
            else
            {
                MessageBox.Show(msg);
            }
        }
        #endregion
    }
}
