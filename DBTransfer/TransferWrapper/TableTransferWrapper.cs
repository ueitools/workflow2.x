using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using BusinessObject;

namespace DBTransfer
{
    class TableTransferWrapper
    {
        /// <summary>
        ///
        /// </summary>
        public static void Synchronize()
        {
            SynchronizeTablesForm dlg = new SynchronizeTablesForm();
            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.Cancel)
                return;

            try
            {
                DAOFactory.BeginTransaction();

                TableTransfer transfer = new TableTransfer();
                transfer.Synchronize(dlg.TableName,
                    dlg.DestDBName, dlg.SrcDBName);

                DAOFactory.CommitTransaction();

                MessageBox.Show(
                    String.Format("{0} is synchronized from {1} to {2}",
                    dlg.TableName, dlg.SrcDBName, dlg.DestDBName));
            }
            catch (Exception ex)
            {
                DAOFactory.RollBackTransaction();
                MessageBox.Show(ex.Message);
            }
        }
    }
}
