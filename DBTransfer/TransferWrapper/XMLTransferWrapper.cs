using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace DBTransfer
{
    static class XMLTransferWrapper
    {
        public static void ImportPartialID()
        {
            XMLImportForm dlg = new XMLImportForm();
            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                try
                {
                    XMLTransfer.ImportPartialID(dlg.XMLFile, dlg.ConnStr);
                    MessageBox.Show(dlg.XMLFile +
                        " is imported successfully");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
