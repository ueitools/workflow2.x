using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Transactions;
using BusinessObject;
using CommonForms;

namespace DBTransfer
{
    static class ExecutorTransferWrapper
    {
        public static string UEITempConnectionString = DBConnectionString.UEITEMP;

        /// <summary>
        /// executor mode
        /// </summary>
        public static void CheckOut()
        {
            int executor = GetExecutorFromUser(
                "Check Out All IDs For Executor");
            if (executor < 0)
                return;

            ITransfer transfer = new ExecutorTransfer(executor, UEITempConnectionString);
            int timeOut = transfer.GetTimeOut();

            try
            {
//                using (TransactionScope oTranScope =
//                    DTCHelper.GetTransactionScope(timeOut))
//                {
                    transfer.CheckOut();
//                    oTranScope.Complete();
//                }

                WarnTNConflict(executor);
                MessageBox.Show("Check Out finished successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void CheckIn()
        {
            int executor = GetExecutorFromUser(
                "Check In All IDs For Executor");
            if (executor < 0)
                return;

            ExecutorTransfer transfer = new ExecutorTransfer(executor, UEITempConnectionString);
            int timeOut = transfer.GetTimeOut();

            try
            {
//                using (TransactionScope oTranScope =
//                    DTCHelper.GetTransactionScope(timeOut))
//                {
                    transfer.CheckIn();
//                    oTranScope.Complete();
//                }

                WarnTNConflict(executor);
                MessageBox.Show("Check In finished successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        #region Private Methods
        /// <summary>
        /// TryParse set executor to 0 if parsing fails
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        private static int GetExecutorFromUser(string title)
        {
            InputForm dlg = new InputForm();
            dlg.SetLabel("Enter Executor:");
            dlg.SetTitle(title);

            DialogResult result = dlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                try
                {
                    int executor = Int32.Parse(dlg.Value);
                    return executor;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error! Operation Aborted");
                    return -1;
                }
            }
            else
            {
                return -1;
            }
        }

        private static void WarnTNConflict(int executor)
        {
            DataStore publicDS = new DataStore(DBConnectionString.UEIPUBLIC);
            IList<string> idList = publicDS.GetIDListByExecutor(executor);

            DataReport publicDR = new DataReport(DBConnectionString.UEIPUBLIC);
            string message = publicDR.reportTNConflict(idList);

            if (message.Length > 0)
            {
                message = "Conflicted TNs are found!\r\n\r\n" + message;
                MessageBox.Show(message, "Warning");
            }
        }
        #endregion
    }
}
