using BusinessObject;

namespace DBTransfer
{
    public class ExecutorTransfer : ITransfer
    {
        int _MAX_TIMEOUT = 500; /// 100 * 5 (id * avg time per id)
        int _executor = -1;
        private readonly string _tempDBConnectionString;

        public ExecutorTransfer(int executor, string tempDBConnectionString)
        {
            _executor = executor;
            _tempDBConnectionString = tempDBConnectionString;
        }

        /// <summary>
        /// Public --> Temp
        /// Ignore TN Lock 
        /// </summary>
        /// <param name="idList"></param>
        public void CheckOut()
        {
            DataStore publicDS = new DataStore(
                DBConnectionString.UEIPUBLIC);
            StringCollection idList =
                publicDS.GetIDListByExecutor(_executor);

            TransferUnit transferUnit = DTCHelper.BuildTransferUnit(idList,
                _tempDBConnectionString, DBConnectionString.UEIPUBLIC);

            transferUnit.IgnoreTNLock = true;
            transferUnit.Copy();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="executor"></param>
        public void CheckIn()
        {
            DataStore tempDS = new DataStore(
                _tempDBConnectionString);
            StringCollection idList =
                tempDS.GetIDListByExecutor(_executor);

            TransferUnit transferUnit = DTCHelper.BuildTransferUnit(idList,
                DBConnectionString.UEIPUBLIC, _tempDBConnectionString);
            transferUnit.Move();
        }

        /// <summary>
        /// assume max id count 
        /// </summary>
        /// <returns></returns>
        public int GetTimeOut()
        {
            return _MAX_TIMEOUT;
        }
    }
}
