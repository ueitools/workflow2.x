using System;
using System.Collections.Generic;
using BusinessObject;

namespace DBTransfer
{
    /// <summary>
    /// 1. Both normal and special Check In skip 
    ///    �Unlock� and �Clear� if TN is shared. 
    /// 2. ID Lock conflict aborts the process always. 
    /// 3. Max check out count is set to 99. 
    /// 
    /// TN Conflict Matrix
    /// 
    ///                 Normal              Special
    /// 
    /// Check Out       abort and report    report
    /// Check In        report              reprot
    /// 
    public class IDTransfer : ITransferEx
    {
        #region Private Fields
        IList<string> _idList = new List<string>();
        private readonly string _tempDBConnectionString;
        const int _TIMEOUTPERID = 15;
        #endregion

        public IDTransfer(IList<string> idList, string tempDBConnectionString)
        {
            _idList = idList;
            _tempDBConnectionString = tempDBConnectionString;
        }

        /// <summary>
        /// Respect TN Lock conflict
        /// Public --> Temp
        /// </summary>
        /// <param name="idList"></param>
        public void CheckOut()
        {
            TransferUnit transferUnit = DTCHelper.BuildTransferUnit(_idList,
                _tempDBConnectionString, DBConnectionString.UEIPUBLIC);

            transferUnit.Copy();
        }

       
        /// <summary>
        /// Temp --> Public
        /// do not ignore TN Lock (src is temp)
        /// </summary>
        /// <param name="idList"></param>
        public void CheckIn()
        {
            TransferUnit transferUnit = DTCHelper.BuildTransferUnit(_idList,
               DBConnectionString.UEIPUBLIC, _tempDBConnectionString);

            transferUnit.Move();
        }


        /// <summary>
        /// Ignore ID and TN Lock for Export
        /// </summary>
        /// <param name="idList"></param>
        /// <param name="connStrTodestDB"></param>
        /// <param name="connStrTosrcDB"></param>
        public void Export(string destConnStr, string srcConnStr)
        {
            TransferUnit transferUnit = DTCHelper.BuildTransferUnit(
                _idList, destConnStr, srcConnStr);
            transferUnit.IgnoreIDLock = true;
            transferUnit.IgnoreTNLock = true;

            transferUnit.Export();
        }

        /// <summary>
        /// Public --> Temp
        /// Do not ignore TN Lock, if somebody is editing the TN, fail
        /// </summary>
        /// <param name="idList"></param>
        public void Erase()
        {
            TransferUnit transferUnit = DTCHelper.BuildTransferUnit(_idList,
               DBConnectionString.UEIPUBLIC, _tempDBConnectionString);

            transferUnit.Erase();
        }

        public void EraseIdFromMainDB()
        {
            TransferUnit transferUnit = DTCHelper.BuildTransferUnit(_idList,
               DBConnectionString.UEIPUBLIC, DBConnectionString.UEIPUBLIC);

            if (transferUnit.TNList.Count > 0)
                throw new Exception(string.Format("ID {0} has linked TNs", transferUnit.IDList[0]));

            transferUnit.EraseIDListAndShadow();
        }

        public int GetTimeOut()
        {
            return _idList.Count * _TIMEOUTPERID;
        }
    }
}
