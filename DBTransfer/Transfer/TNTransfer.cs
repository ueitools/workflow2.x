using System;
using System.Collections.Generic;
using BusinessObject;

namespace DBTransfer
{
    /// <summary>
    /// assume only one tn
    /// </summary>
    public class TNTransfer : ITransfer
    {
        readonly IList<int> _tnList = new List<int>();
        private readonly string _tempConnectionString;

        public TNTransfer(IList<int> tnList, string tempConnectionString)
        {
            _tnList = tnList;
            _tempConnectionString = tempConnectionString;
        }

        /// <summary>
        /// make sure tn is not coonected to any id
        /// </summary>
        public void CheckOut()
        {
            /// The original intent was changed.  
            /// The code to block checking out connected TNs has been removed.

            TransferUnit transferUnit = DTCHelper.BuildTransferUnit(_tnList,
                _tempConnectionString, DBConnectionString.UEIPUBLIC);
            transferUnit.Copy();
        }

        public void CheckIn()
        {
            StringCollection idList = GetIDsLinkedToTN(_tnList[0], _tempConnectionString);
            if (idList.Count > 0)
            {
                string msg = GetConnectedTNErrorMsg(_tnList[0], idList);
                throw new Exception(msg);
            }

            TransferUnit transferUnit = DTCHelper.BuildTransferUnit(_tnList,
                DBConnectionString.UEIPUBLIC, _tempConnectionString);
            transferUnit.Move();
        }

        public int GetTimeOut()
        {
            return _tnList.Count;
        }

        #region private method
        private StringCollection GetIDsLinkedToTN(
            int tn, string connStrTosrcDB)
        {
            DataStore srcDS = new DataStore(connStrTosrcDB);
            StringCollection idList = srcDS.GetIDList(tn);
            return idList;
        }

        private string GetConnectedTNErrorMsg(int tn, IList<string> idList)
        {
            string msg = string.Format("TN{0} is linked to ", tn);
            foreach (string id in idList)
                msg += string.Format("{0} ", id);

            return msg;
        }
        #endregion
    }
}
