using System;
using System.Collections.Generic;
using System.Text;
using BusinessObject;

namespace DBTransfer
{
    public class XMLTransfer
    {
        /// <summary>
        /// user interaction layer
        /// </summary>
        /// <param name="xmlFile"></param>
        /// <param name="connStr"></param>
        public static void ImportPartialID(string xmlFile, string connStr)
        {
            try
            {
                DAOFactory.ResetDBConnection(connStr);
                DAOFactory.BeginTransaction();
                ImportPartialID(xmlFile);
                DAOFactory.CommitTransaction();
            }
            catch
            {
                DAOFactory.RollBackTransaction();
                throw;
            }
        }

        /// <summary>
        /// testing layer
        /// verifiy write
        /// </summary>
        /// <param name="xmlFile"></param>
        public static void ImportPartialID(string xmlFile)
        {
            PartialID oID = new PartialID();
            oID.LoadFromXML(xmlFile);
            oID.Save();

            PartialID oReadBackID = new PartialID();
            oReadBackID.Load(oID.Header.ID);

            oID.FunctionList = FilterOutConnectionRecord(oID.FunctionList);
            Adjust(oID.DeviceList);
            if (Comparer.IsEqual(oID, oReadBackID) != true)
            {
                throw new Exception("Import operation for "
                    + oID.Header.ID + " can't be verified!");
            }
        }

        #region Private
        /// <summary>
        /// remove connection record from functionList
        /// for compatible xml generation for verification
        /// </summary>
        /// <param name="functionList"></param>
        private static FunctionCollection FilterOutConnectionRecord(
            FunctionCollection functionList)
        {
            FunctionCollection list = new FunctionCollection();

            foreach (Function function in functionList)
            {
                if (function.IsConnection == BooleanFlag.NO)
                    list.Add(function);
            }

            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceList"></param>
        private static void Adjust(DeviceCollection deviceList)
        {
            foreach (Device device in deviceList)
            {
                device.CountryList = Redirect(device.CountryList);
                device.CountryList.SortByDefault();
            }
        }

        private static CountryCollection Redirect(
            CountryCollection countryList)
        {
            CountryCollection list = new CountryCollection();
            Country curCountry = new Country();
            foreach (Country country in countryList)
            {
                curCountry = DBFunctions.Redirect(country);
                curCountry.Code = 0;

                list.Add(curCountry);
            }

            return list;
        }

        #endregion
    }
}
