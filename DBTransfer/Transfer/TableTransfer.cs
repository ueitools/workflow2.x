using System;
using System.Collections.Generic;
using System.Text;
using BusinessObject;

namespace DBTransfer
{
    public class TableTransfer
    {
        /// <summary>
        /// 1. read src
        /// 2. synchronize
        /// 3. compare src to src and src to dest
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="destDBName"></param>
        /// <param name="srcDBName"></param>
        public void Synchronize(string tableName, 
            string destDBName, string srcDBName)
        {
            ///
            /// before
            ///
            HashtableCollection srcListBefore = ReplicationFunctions.
                SelectAll(tableName, srcDBName);

            ReplicationFunctions.Transfer(tableName, destDBName, srcDBName);

            ///
            /// after
            ///
            HashtableCollection srcListAfter = ReplicationFunctions.
                SelectAll(tableName, srcDBName);

            HashtableCollection destList = ReplicationFunctions.
                SelectAll(tableName, destDBName);

            ///
            /// verifiy
            ///
            bool IsSrcVerifiedOK = Comparer.IsEqual(srcListBefore, srcListAfter);
            bool IsDestVerifiedOK = Comparer.IsEqual(srcListBefore, destList);
            if (IsSrcVerifiedOK != true || IsDestVerifiedOK != true)
            {
                throw new Exception(
                    "Table Synchronization can't be verified!");
            }
        }
    }
}
