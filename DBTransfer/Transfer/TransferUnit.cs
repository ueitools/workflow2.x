using System;
using System.Collections.Generic;
using BusinessObject;
using UEIException;

namespace DBTransfer
{
    /// <summary>
    /// idList and tnList synchronization is client's responsibility
    /// This class just handles transfer as unit.
    /// </summary>
    public class TransferUnit
    {
        #region Private Fields

        private const int _MAX_IDCOUNT = 100;
        private IList<string> _idList = new StringCollection();
        private IList<int> _tnList = new IntegerCollection();
        private DataStore _dest;
        private DataStore _src;
        private bool _ignoreTNLock = false;
        private bool _ignoreIDLock = false;
        #endregion

        #region Business Properties and Methods

        public IList<string> IDList
        {
            get { return _idList; }
            set { _idList = value; }
        }

        public IList<int> TNList
        {
            get { return _tnList; }
            set { _tnList = value; }
        }

        public DataStore Dest
        {
            get { return _dest; }
            set { _dest = value; }
        }

        public DataStore Src
        {
            get { return _src; }
            set { _src = value; }
        }
       
        public bool IgnoreTNLock
        {
            get { return _ignoreTNLock; }
            set { _ignoreTNLock = value; }
        }

        public bool IgnoreIDLock
        {
            get { return _ignoreIDLock; }
            set { _ignoreIDLock = value; }
        }

        #endregion

        /// <summary>
        /// dest first and src second
        /// </summary>
        /// <param name="dest"></param>
        /// <param name="src"></param>
        public TransferUnit(DataStore dest, DataStore src)
        {
            _dest = dest;
            _src = src;
        }

        /// <summary>
        /// copying order: ID --> TN
        /// </summary>
        /// <param name="dest"></param>
        /// <param name="src"></param>
        public void Copy() {
            CopyIDList();
            CopyTNList();

            LockIdsAndTns();

            try
            {
                UnLockTNList();
                UnLockIDList();
            } catch (Exception) {
                RevertLockTNList();
                RevertLockIDList();
                throw;
            }
        }

        private void LockIdsAndTns()
        {
            if (_src.ConnStr == DBConnectionString.UEIPUBLIC)
            {
                List<string> idList = new List<string>(_idList);
                List<int> tnList = new List<int>(_tnList);
                _src.LockIdsAndTns(idList, tnList, _ignoreTNLock);
            }
            else
            {
                LockTNList();
                try
                {
                    LockIDList();
                }
                catch (Exception)
                {
                    RevertLockTNList();
                    throw;
                }
            }
        }

        /// <summary>
        /// no locking is needed
        /// </summary>
        public void Export()
        {
            CopyIDList();
            CopyTNList();
        }

        /// <summary>
        /// Move if only it is ready -- Check In
        /// </summary>
        public void Move()
        {
            LockTNList();
            try {
                LockIDList();
            } catch (Exception) {
                RevertLockTNList();
                throw;
            }

            try {
                CopyIDListWithReadyCheck();
                CopyTNListWithReadyCheck();

                UnLockTNListWithTNSharedCheck();
                UnLockIDList();
            } catch (Exception) {
                RevertLockTNList();
                RevertLockIDList();
                throw;
            }
        }

        /// <summary>
        /// Erase ID and TN from Temp DB and release locks from Main DB
        /// </summary>
        public void Erase()
        {
            LockTNList();
            LockIDList();

            UnLockTNListWithTNSharedCheck();
            UnLockIDList();

            EraseTNList();
            EraseIDList();
        }

        public void EraseIDListAndShadow()
        {
            foreach (string id in _idList)
                _src.EraseIDAndShadow(id);
        }

        #region Private Methods
        /////////////////////////////////////////////////////////////////////
        ////
        //// ID
        ////

        /// <summary>
        /// Check Max ID Limit
        /// </summary>
        private void CopyIDList()
        {
            if (_idList.Count > _MAX_IDCOUNT)
            {
                throw new Exception("Too many IDs: " + _idList.Count
                    + " (Limit to " + _MAX_IDCOUNT + ")");
            }

            foreach (string id in _idList)
            {
                PartialID oID = _src.Load(id);
                _dest.Save(oID);
            }
        }

        /// <summary>
        /// this is for check in
        /// </summary>
        private void CopyIDListWithReadyCheck()
        {
            foreach (string id in _idList)
            {
                PartialID oID = _src.Load(id);
                if (oID.IsReady() == true)
                {
                    Log log = Log.Create(id, "Checked In");
                    oID.Header.LogList.Add(log);

                    _dest.Save(oID);
                }
                else
                {
                    throw new Exception("ID " + id +
                        " has not passed QA");
                }
            }
        }

        private void LockIDList()
        {
            if (_src.ConnStr == DBConnectionString.UEITEMP_INDIA) {
                return;
            }

            for (int idListIndex = 0; idListIndex < _idList.Count; idListIndex++) {
                string id = _idList[idListIndex];
                try {
                    _src.Lock(id);
                } catch (Exception) {
                    for (int idRevertIndex = 0; idRevertIndex < idListIndex; idRevertIndex++) {
                        string revertId = _idList[idRevertIndex];
                        _src.UnLock(revertId);
                    }
                    throw;
                }
            }
        }

        private void UnLockIDList()
        {
            foreach (string id in _idList)
            {
                try
                {
                    if (_src.ConnStr == DBConnectionString.UEITEMP_INDIA) {
                        _dest.ForceUnLock(id);
                    } else {
                        _dest.UnLock(id);
                    }
                }
                catch (IDLockException)
                {
                    if (_ignoreIDLock != true)
                        throw;
                }
            }
        }

        private void EraseIDList()
        {
            foreach (string id in _idList)
            {
                _src.UnLock(id);
            }
        }
                
        /////////////////////////////////////////////////////////////////////
        ////
        //// TN
        ////
        //// 1. Lock skips if _ignoreTNLock is set to true
        //// 2. If TN is shared UnLock and Erase are skipped
        ////
        private void CopyTNList()
        {
            foreach (int tn in _tnList)
            {
                TN oTN = _src.Load(tn);
                _dest.Save(oTN);
            }
        }

        /// <summary>
        /// this is for check in
        /// </summary>
        private void CopyTNListWithReadyCheck()
        {
            foreach (int tn in _tnList)
            {
                TN oTN = _src.Load(tn);
                if (oTN.IsReady() == true)
                {
                    _dest.Save(oTN);
                }
                else
                {
                    throw new Exception("TN " + tn +
                        " has not passed QA");
                }
            }
        }

        /// <summary>
        /// if _ignoreTNLock is set to true TNLockException will be ignored
        /// </summary>
        private void LockTNList()
        {
            if (_src.ConnStr == DBConnectionString.UEITEMP_INDIA) {
                return;
            }

            for (int tnListIndex = 0; tnListIndex < _tnList.Count; tnListIndex++) {
                int tn = _tnList[tnListIndex];
                try
                {
                    _src.Lock(tn);
                }
                catch (Exception)
                {
                    if (_ignoreTNLock == false) {
                        for (int tnRevertIndex = 0; tnRevertIndex < tnListIndex; tnRevertIndex++) {
                            int revertTn = _tnList[tnRevertIndex];
                            _src.UnLock(revertTn);
                        }
                        throw;
                    }
                }
            }
        }

        private void RevertLockIDList() {
            foreach (string id in _idList) {
                _src.UnLock(id);
            }
        }

        private void RevertLockTNList() {
            foreach (int tn in _tnList) {
                _src.UnLock(tn);
            }
        }

        /// <summary>
        /// check src and take action on dest
        /// </summary>
        private void UnLockTNList()
        {
            foreach (int tn in _tnList)
            {   
                _dest.UnLock(tn);
            }
        }

        private void UnLockTNListWithTNSharedCheck()
        {
            foreach (int tn in _tnList)
            {
                if (_src.IsTNShared(tn)) {
                    if (_src.ConnStr != DBConnectionString.UEITEMP_INDIA)
                    {
                        _src.UnLock(tn);
                    }
                } else {
                    if (_src.ConnStr == DBConnectionString.UEITEMP_INDIA) {
                        _dest.ForceUnlock(tn);
                    } else {
                        _dest.UnLock(tn);
                    }
                }
            }
        }

        /// <summary>
        /// src should be unlocked even if it is shared
        /// </summary>
        private void EraseTNList()
        {
            foreach (int tn in _tnList)
            {
                _src.UnLock(tn);
            }
        }
        #endregion
    }
}
