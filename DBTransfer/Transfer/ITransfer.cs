namespace DBTransfer
{
    public interface ITransfer
    {
        void CheckOut();
        void CheckIn();
        int GetTimeOut();
    }

    public interface ITransferEx : ITransfer
    {
        void Export(string destConnStr, string srcConnStr);
        void Erase();
    }
}
