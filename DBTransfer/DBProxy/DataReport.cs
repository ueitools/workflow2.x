using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using BusinessObject;

namespace DBTransfer
{
    /// <summary>
    /// used as the return struct in GetIDConflict
    /// </summary>
    public struct IDTNMap
    {
        public int tn;
        public string id;
    };

    /// <summary>
    /// 
    /// </summary>
    public class DataReport
    {
        private string _connStr = null;

        public DataReport(string conn)
        {
            _connStr = conn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="executor"></param>
        /// <returns></returns>
        public string reportTNConflict(int executor)
        {
            DAOFactory.ResetDBConnection(_connStr);

            IList<string> idList = DBFunctions.GetIDListByExecutor(executor);
            return reportTNConflict(idList);
        }

        public string reportIDConflict(int executor)
        {
            DAOFactory.ResetDBConnection(_connStr);

            IList<string> idList = DBFunctions.GetIDListByExecutor(executor);
            return reportIDConflict(idList);
        }

        public string reportTNConflict(IList<string> idList)
        {
            DAOFactory.ResetDBConnection(_connStr);

            string message = "";
            foreach (string id in idList)
            {
                message += reportTNConflict(id);
            }

            return message;
        }

        public string reportIDConflict(string id)
        {
            DAOFactory.ResetDBConnection(_connStr);

            string message = "";
            message += reportTNConflict(id);
            return message;
        }

        public string reportIDConflict(IList<string> idList)
        {
            DAOFactory.ResetDBConnection(_connStr);

            string message = "";
            IList<string> lockedIDList = GetIDConflict(idList);
            foreach (string id in lockedIDList)
            {
                message += id + " is locked\r\n";
            }

            return message;
        }

        /////////////////////////////////////////////////////////////////////
        ////
        //// private
        ////
        /// <summary>
        /// targets Public DB
        /// </summary>
        /// <param name="idList"></param>
        /// <returns></returns>
        private IList<string> GetIDConflict(IList<string> idList)
        {
            IDHeader header = null;
            List<string> lockedIDList = new List<string>();

            foreach (string id in idList)
            {
                header = DAOFactory.IDHeader().Select(id);
                if (header.IsLocked == BooleanFlag.YES)
                {
                    lockedIDList.Add(id);
                }
            }

            return lockedIDList;
        }

        /// <summary>
        /// targets Public DB
        /// 1. For a given ID return a list of the locked TN 
        /// 2. For each locked TN find locked parent ID list 
        /// </summary>
        /// <param name="IDToCheckOut"></param>
        /// <returns></returns>
        private List<IDTNMap> GetTNConflict(string idToCheckOut)
        {
            List<IDTNMap> conflictList = new List<IDTNMap>();
            List<string> idList = null;
            IDTNMap map;

            IntegerCollection tnList = DBFunctions.GetTNList(idToCheckOut);
            foreach (int tn in tnList)
            {
                if (TN.IsLocked(tn))
                {
                    idList = FindLockedParentIDList(tn);
                    foreach (string id in idList)
                    {
                        map = new IDTNMap();
                        map.id = id;
                        map.tn = tn;

                        conflictList.Add(map);
                    }
                }
            }

            return conflictList;
        }

        private string reportTNConflict(string id)
        {
            string message = "";
            List<IDTNMap> mapList = GetTNConflict(id);
            foreach (IDTNMap map in mapList)
            {
                message += "TN" + map.tn + " is locked by "
                    + map.id + "\r\n";
            }

            if (message != "")
                message += "\r\n";

            return message;
        }

        /////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Check each ID realated to the given TN and for each
        /// ID that is locked, add an entry to the ConflictedList.
        /// </summary>
        /// <param name="tn"></param>
        /// <returns></returns>
        private List<string> FindLockedParentIDList(int tn)
        {
            IDHeader header = null;

            List<string> lockedIDList = new List<string>();
            StringCollection idListForTN = DBFunctions.GetIDList(tn);
            foreach (string id in idListForTN)
            {
                if (PartialID.IsLocked(id))
                {
                    lockedIDList.Add(id);
                }
            }

            return lockedIDList;
        }
    }
}
