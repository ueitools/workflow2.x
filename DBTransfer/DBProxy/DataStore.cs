using System;
using System.Collections.Generic;
using BusinessObject;

namespace DBTransfer
{
    /// <summary>
    /// Database switching is only allowed context of this class
    /// for DBEditor project
    /// 
    /// every public method must start with calling
    /// DAOFactory.ResetDBConnection(_connStr) 
    /// </summary>
    public class DataStore
    {
        private string _connStr = null;

        public DataStore(string conn)
        {
            _connStr = conn;
        }

        public string ConnStr {
            get { return _connStr; }
        }

        /////////////////////////////////////////////////////////////////////
        ////
        //// ID
        ////
        public PartialID Load(string id)
        {
            DAOFactory.ResetDBConnection(_connStr);

            PartialID oID = new PartialID();
            oID.Load(id);
            return oID;
        }

        /// <summary>
        /// Check if id has been changed before save
        /// 1. not changed --> skip 
        /// 2. changed --> save and verify
        /// </summary>
        /// <param name="oID"></param>
        public void Save(PartialID oID)
        {
            DAOFactory.ResetDBConnection(_connStr);

            PartialID oReadBackID = new PartialID();
            if (DoesExist(oID.Header.ID) == true)
            {
                oReadBackID.Load(oID.Header.ID);
                if (Comparer.IsEqual(oID, oReadBackID) == true)
                    return;
            }

            /// 
            oID.Save();
            oReadBackID.Load(oID.Header.ID);
            if (Comparer.IsEqual(oID, oReadBackID) != true)
            {
                throw new Exception("Save operation for "
                    + oID.Header.ID + " can't be verified!");
            }
        }

        /// <summary>
        /// verifiy erase
        /// </summary>
        /// <param name="id"></param>
        public void Erase(string id)
        {
            DAOFactory.ResetDBConnection(_connStr);
            PartialID.Erase(id);

            if (DBFunctions.DoesExist(id) == true)
            {
                throw new Exception("Erase operation for "
                    + id + " can't be verified!");
            }
        }

        public void EraseIDAndShadow(string id)
        {
            DAOFactory.ResetDBConnection(_connStr);
            PartialID.EraseIDAndShadow(id);

            if (DBFunctions.DoesExist(id) == true)
            {
                throw new Exception("Erase operation for "
                    + id + " can't be verified!");
            }
        }

        public void Lock(string id)
        {
            DAOFactory.ResetDBConnection(_connStr);
            PartialID.Lock(id);
        }

        public void UnLock(string id)
        {
            DAOFactory.ResetDBConnection(_connStr);
            PartialID.UnLock(id);
        }

        public void ForceUnLock(string id)
        {
            DAOFactory.ResetDBConnection(_connStr);
            PartialID.ForceUnLock(id);
        }

        public void Create(string newID, string baseID, string exec)
        {
            DAOFactory.ResetDBConnection(_connStr);
            PartialID oID = new PartialID();
            oID.Initialize(newID, baseID, exec);
            oID.Insert();
        }

        public void Create(string newID, IDHeader baseID, string exec)
        {
            DAOFactory.ResetDBConnection(_connStr);
            PartialID oID = new PartialID();
            oID.Initialize(newID, baseID, exec);
            oID.Insert();
        }

        /////////////////////////////////////////////////////////////////////
        ////
        //// TN
        ////
        public TN Load(int tn)
        {
            DAOFactory.ResetDBConnection(_connStr);

            TN oTN = new TN();
            oTN.Load(tn);
            return oTN;   
        }

        /// <summary>
        /// Check if tn has been changed before save
        /// 1. not changed --> skip 
        /// 2. changed --> save and verify
        /// </summary>
        /// <param name="oTN"></param>
        public void Save(TN oTN)
        {
            DAOFactory.ResetDBConnection(_connStr);

            TN oReadBackTN = new TN();
            if (DoesExist(oTN.Header.TN) == true)
            {
                oReadBackTN.Load(oTN.Header.TN);
                if (Comparer.IsEqual(oTN, oReadBackTN) == true)
                    return;
            }

            ///
            oTN.Save();
            oReadBackTN.Load(oTN.Header.TN);
            if (Comparer.IsEqual(oTN, oReadBackTN) != true)
            {
                throw new Exception("Save operation for "
                    + oTN.Header.TN + " can't be verified!");
            }
        }

        public void Erase(int tn)
        {
            DAOFactory.ResetDBConnection(_connStr);
            TN.Erase(tn);

            if (DBFunctions.DoesExist(tn) == true)
            {
                throw new Exception("Erase operation for "
                    + tn + " can't be verified!");
            }
        }

        public void Lock(int tn)
        {
            DAOFactory.ResetDBConnection(_connStr);
            TN.Lock(tn);
        }

        public void UnLock(int tn)
        {
            DAOFactory.ResetDBConnection(_connStr);
            TN.UnLock(tn);
        }

        public void ForceUnlock(int tn)
        {
            DAOFactory.ResetDBConnection(_connStr);
            TN.ForceUnLock(tn);
        }

        /////////////////////////////////////////////////////////////////////
        ////
        //// Target-Aware DB Query
        ////

        /// <summary>
        /// check if more than 1 ID are related to TN 
        /// </summary>
        /// <param name="tn"></param>
        /// <returns></returns>
        public bool IsTNShared(int tn)
        {
            DAOFactory.ResetDBConnection(_connStr);
            return DBFunctions.IsTNShared(tn);
        }

        public IntegerCollection GetAvailableCodeList(string deviceGroup)
        {
            DAOFactory.ResetDBConnection(_connStr);
            return DBFunctions.GetAvailableCodeList(deviceGroup);
        }

        public StringCollection GetAllIDList()
        {
            DAOFactory.ResetDBConnection(_connStr);
            return DBFunctions.GetAllIDList();
        }

        public IntegerCollection GetTNList(IList<string> idList)
        {
            DAOFactory.ResetDBConnection(_connStr);
            return DBFunctions.GetTNList(idList);
        }

        public StringCollection GetIDList(int tn)
        {
            DAOFactory.ResetDBConnection(_connStr);
            return DBFunctions.GetIDList(tn);
        }

        public StringCollection GetIDListByExecutor(int executor)
        {
            DAOFactory.ResetDBConnection(_connStr);
            return DBFunctions.GetIDListByExecutor(executor);
        }

        public bool DoesExist(int tn)
        {
            DAOFactory.ResetDBConnection(_connStr);
            return DBFunctions.DoesExist(tn);
        }

        public bool DoesExist(string id)
        {
            DAOFactory.ResetDBConnection(_connStr);
            return DBFunctions.DoesExist(id);
        }

        public void LockIdsAndTns(List<string> idList, List<int> tnList, bool _ignoreTnLock)
        {
            DAOFactory.ResetDBConnection(_connStr);
            DBFunctions.LockIdsAndTns(idList, tnList, _ignoreTnLock);
        }
    }
}
