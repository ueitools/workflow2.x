using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DBTransfer
{
    public partial class SynchronizeTablesForm : Form
    {
        #region Private Fields
        string _tableName;
        string _srcDBName;
        string _destDBName;
        #endregion

        #region Puplic Properties
        public string TableName
        {
            get { return _tableName; }
        }

        public string SrcDBName
        {
            get { return _srcDBName; }
        }

        public string DestDBName
        {
            get { return _destDBName; }
        }
        #endregion

        public SynchronizeTablesForm()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            _tableName = this.comboBoxTable.SelectedItem.ToString();
            _destDBName = this.comboBoxDestDB.SelectedItem.ToString();
            _srcDBName = this.comboBoxSrcDB.SelectedItem.ToString();
            
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void SynchronizeTablesForm_Load(object sender, EventArgs e)
        {
            this.comboBoxDestDB.SelectedIndex = 0;
            this.comboBoxSrcDB.SelectedIndex = 0;
        }
    }
}