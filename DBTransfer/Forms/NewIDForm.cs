using System;
using System.Windows.Forms;
using BusinessObject;

namespace DBTransfer
{
    public partial class NewIDForm : Form
    {
        #region Private Fields
        string _newID;
        string _baseID;
        string _exec;
        #endregion

        #region Puplic Properties
        public string NewID
        {
            get { return _newID; }
        }

        public string BaseID
        {
            get { return _baseID; }
        }

        public string Exec
        {
            get { return _exec; }
        }
        #endregion

        public NewIDForm()
        {
            InitializeComponent();
        }

        private void NewIDForm_Load(object sender, EventArgs e)
        {
            comboBoxUEIDeviceGroup.DataSource = 
                DBFunctions.GetDeviceGroupList();

            LoadExecList();
        }

        /// <summary>
        /// After creation checkout new id 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            string mode = comboBoxUEIDeviceMode.Text;
            int code = Int32.Parse(comboBoxUEIDeviceCode.Text);
            _newID = String.Format(mode + "{0:D4}", code);
            _baseID = textBoxCopyFromID.Text;

            if (cbboxExec.SelectedItem == null) {
                MessageBox.Show(this, "Invalid exec entered.");
                cbboxExec.Select();
                return;
            }
            _exec = cbboxExec.SelectedItem.ToString();

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Get Available Code info from Public Database
        /// </summary>
        /// <param name="deviceGroup"></param>
        private void BindModeNCode(string deviceGroup)
        {
            StringCollection modeListForDeviceGroup = DBFunctions.GetModeListForDeviceGroup(deviceGroup);
            comboBoxUEIDeviceMode.DataSource = modeListForDeviceGroup;

            DataStore publicDS = new DataStore(DBConnectionString.UEIPUBLIC);
            IntegerCollection publicCodes = publicDS.GetAvailableCodeList(deviceGroup);

            LockStatusFunctions lsf = new LockStatusFunctions();
            StringCollection allLockedIds = lsf.GetAllCheckedOutIds("");
            IntegerCollection filteredIdCodes = FilterIdsByModeToCodeOnly(allLockedIds, modeListForDeviceGroup);

            comboBoxUEIDeviceCode.DataSource = GetCollectionRemoveCommon(publicCodes, filteredIdCodes);
        }

        private IntegerCollection FilterIdsByModeToCodeOnly(StringCollection idList, StringCollection modeListForDeviceGroup) {
            IntegerCollection filteredIds = new IntegerCollection();
            foreach (string id in idList) {
                if (modeListForDeviceGroup.Contains(id.Substring(0,1))) {
                    filteredIds.Add(int.Parse(id.Substring(1)));
                }
            }
            return filteredIds;
        }

        private IntegerCollection GetCollectionRemoveCommon(IntegerCollection sourceCollection, IntegerCollection removeCollection) {
            IntegerCollection resultCollection = new IntegerCollection();
            resultCollection.Append(sourceCollection);
            foreach (int removeCode in removeCollection) {
                if (resultCollection.Contains(removeCode)) {
                    resultCollection.Remove(removeCode);
                }
            }

            return resultCollection;
        }

        private void comboBoxUEIDeviceGroup_SelectedIndexChanged(
            object sender, EventArgs e)
        {
            BindModeNCode(comboBoxUEIDeviceGroup.Text);
        }

        /// <summary>
        /// binding is faster than individual add
        /// </summary>
        private void LoadExecList()
        {
            StringCollection list = new StringCollection();
            for (int i = 0; i <= 2048; i++)
            {
                list.Add(i.ToString());
            }

            cbboxExec.DataSource = list;
        }
    }
}