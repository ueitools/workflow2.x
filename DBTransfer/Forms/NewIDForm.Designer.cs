namespace DBTransfer
{
    partial class NewIDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxUEIDeviceGroup = new System.Windows.Forms.ComboBox();
            this.comboBoxUEIDeviceMode = new System.Windows.Forms.ComboBox();
            this.comboBoxUEIDeviceCode = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbboxExec = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxCopyFromID = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxUEIDeviceGroup
            // 
            this.comboBoxUEIDeviceGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUEIDeviceGroup.FormattingEnabled = true;
            this.comboBoxUEIDeviceGroup.Location = new System.Drawing.Point(186, 25);
            this.comboBoxUEIDeviceGroup.Name = "comboBoxUEIDeviceGroup";
            this.comboBoxUEIDeviceGroup.Size = new System.Drawing.Size(121, 21);
            this.comboBoxUEIDeviceGroup.TabIndex = 0;
            this.comboBoxUEIDeviceGroup.SelectedIndexChanged += new System.EventHandler(this.comboBoxUEIDeviceGroup_SelectedIndexChanged);
            // 
            // comboBoxUEIDeviceMode
            // 
            this.comboBoxUEIDeviceMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUEIDeviceMode.FormattingEnabled = true;
            this.comboBoxUEIDeviceMode.Location = new System.Drawing.Point(172, 24);
            this.comboBoxUEIDeviceMode.Name = "comboBoxUEIDeviceMode";
            this.comboBoxUEIDeviceMode.Size = new System.Drawing.Size(121, 21);
            this.comboBoxUEIDeviceMode.TabIndex = 1;
            // 
            // comboBoxUEIDeviceCode
            // 
            this.comboBoxUEIDeviceCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUEIDeviceCode.FormattingEnabled = true;
            this.comboBoxUEIDeviceCode.Location = new System.Drawing.Point(172, 54);
            this.comboBoxUEIDeviceCode.Name = "comboBoxUEIDeviceCode";
            this.comboBoxUEIDeviceCode.Size = new System.Drawing.Size(121, 21);
            this.comboBoxUEIDeviceCode.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Select UEI Device Group";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Available UEI Device Mode";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Available UEI Device Code";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbboxExec);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxCopyFromID);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBoxUEIDeviceMode);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboBoxUEIDeviceCode);
            this.groupBox1.Location = new System.Drawing.Point(14, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(318, 185);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Mode and Code";
            // 
            // cbboxExec
            // 
            this.cbboxExec.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbboxExec.FormattingEnabled = true;
            this.cbboxExec.Location = new System.Drawing.Point(172, 147);
            this.cbboxExec.Name = "cbboxExec";
            this.cbboxExec.Size = new System.Drawing.Size(121, 21);
            this.cbboxExec.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(98, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Executor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(95, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Copy From ID";
            // 
            // textBoxCopyFromID
            // 
            this.textBoxCopyFromID.Location = new System.Drawing.Point(172, 110);
            this.textBoxCopyFromID.Name = "textBoxCopyFromID";
            this.textBoxCopyFromID.Size = new System.Drawing.Size(121, 20);
            this.textBoxCopyFromID.TabIndex = 7;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(76, 280);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 9;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(186, 280);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 10;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // NewIDForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 322);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxUEIDeviceGroup);
            this.Name = "NewIDForm";
            this.Text = "Create a New ID Header";
            this.Load += new System.EventHandler(this.NewIDForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxUEIDeviceGroup;
        private System.Windows.Forms.ComboBox comboBoxUEIDeviceMode;
        private System.Windows.Forms.ComboBox comboBoxUEIDeviceCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxCopyFromID;
        private System.Windows.Forms.ComboBox cbboxExec;
        private System.Windows.Forms.Label label5;
    }
}