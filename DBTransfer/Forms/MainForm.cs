using System;
using System.Windows.Forms;
using System.Configuration;

namespace DBTransfer
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            string adminList = ConfigurationManager.
                AppSettings["DBADMINISTRATOR"].ToUpper();
            string user = SystemInformation.UserName.ToUpper();

            string domain = SystemInformation.UserDomainName;
            if (domain != "UEIC") {
                checkInIDToolStripMenuItem.Enabled = false;
                batchCheckInToolStripMenuItem.Enabled = false;
                checkInTNToolStripMenuItem.Enabled = false;
                importFromXMLToolStripMenuItem.Enabled = false;
                deleteIDFromMainDBToolStripMenuItem.Enabled = false;
                tableExportToolStripMenuItem.Enabled = false; // Sync
                checkInAllIDsForExecutorToolStripMenuItem1.Enabled = false;
            }

            if (adminList.Contains(user))
                deleteIDFromMainDBToolStripMenuItem.Visible = true;
        }

        /// <summary>
        /// ID related
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkOutIDToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            IDTransferWrapper.CheckOut();
        }

        private void checkInIDToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            IDTransferWrapper.CheckIn();
        }

        private void batchIDCheckOutToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            IDTransferWrapper.BatchCheckOut();
        }

        private void batchIDCheckInToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            IDTransferWrapper.BatchCheckIn();
        }

        private void createIDToolStripMenuItem_Click(
           object sender, EventArgs e)
        {
            IDTransferWrapper.Create();
        }

        private void exportIDToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            IDTransferWrapper.Export();
        }

        private void revertEditsAndUnlockIDToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            IDTransferWrapper.RevertEditsAndUnlock();
        }

        /// <summary>
        /// executor related
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkOutAllIDsForExecutorToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            ExecutorTransferWrapper.CheckOut();
        }

        private void checkInAllIDsForExecutorToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            ExecutorTransferWrapper.CheckIn();
        }

        /// <summary>
        /// TN related
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkOutTNToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            TNTransferWrapper.CheckOut();
        }

        private void checkInTNToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            TNTransferWrapper.CheckIn();
        }

        private void deleteIDFromMainDBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IDTransferWrapper.Delete();
        }

        private void tableExportToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            TableTransferWrapper.Synchronize();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void importFromXMLToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            XMLTransferWrapper.ImportPartialID();
        }
    }
}
