namespace DBTransfer
{
    partial class SynchronizeTablesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxDestDB = new System.Windows.Forms.ComboBox();
            this.comboBoxSrcDB = new System.Windows.Forms.ComboBox();
            this.comboBoxTable = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Table To Synchronize";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(212, 82);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 13;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(109, 82);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 12;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(274, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Destination DB";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(146, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Source DB";
            // 
            // comboBoxDestDB
            // 
            this.comboBoxDestDB.DisplayMember = "Display";
            this.comboBoxDestDB.FormattingEnabled = true;
            this.comboBoxDestDB.Items.AddRange(new object[] {
            "uei"});
            this.comboBoxDestDB.Location = new System.Drawing.Point(275, 41);
            this.comboBoxDestDB.Name = "comboBoxDestDB";
            this.comboBoxDestDB.Size = new System.Drawing.Size(106, 21);
            this.comboBoxDestDB.TabIndex = 9;
            this.comboBoxDestDB.ValueMember = "Value";
            // 
            // comboBoxSrcDB
            // 
            this.comboBoxSrcDB.DisplayMember = "Display";
            this.comboBoxSrcDB.FormattingEnabled = true;
            this.comboBoxSrcDB.Items.AddRange(new object[] {
            "ueiTemp"});
            this.comboBoxSrcDB.Location = new System.Drawing.Point(147, 41);
            this.comboBoxSrcDB.Name = "comboBoxSrcDB";
            this.comboBoxSrcDB.Size = new System.Drawing.Size(106, 21);
            this.comboBoxSrcDB.TabIndex = 8;
            this.comboBoxSrcDB.ValueMember = "Value";
            // 
            // comboBoxTable
            // 
            this.comboBoxTable.DisplayMember = "Display";
            this.comboBoxTable.FormattingEnabled = true;
            this.comboBoxTable.Items.AddRange(new object[] {
            "Intron",
            "IntronDictionary",
            "PDL_Template",
            "Protocol_Priority",
            "Nop_Data"});
            this.comboBoxTable.Location = new System.Drawing.Point(23, 41);
            this.comboBoxTable.Name = "comboBoxTable";
            this.comboBoxTable.Size = new System.Drawing.Size(106, 21);
            this.comboBoxTable.TabIndex = 16;
            this.comboBoxTable.ValueMember = "Value";
            // 
            // SynchronizeTablesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 125);
            this.Controls.Add(this.comboBoxTable);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxDestDB);
            this.Controls.Add(this.comboBoxSrcDB);
            this.Name = "SynchronizeTablesForm";
            this.Text = "Synchronize Tables";
            this.Load += new System.EventHandler(this.SynchronizeTablesForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxDestDB;
        private System.Windows.Forms.ComboBox comboBoxSrcDB;
        private System.Windows.Forms.ComboBox comboBoxTable;
    }
}