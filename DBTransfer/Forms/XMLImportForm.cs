using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;

namespace DBTransfer
{
    public partial class XMLImportForm : Form
    {
        #region Private Fields
        string _xmlFile;
        string _connStr;
        #endregion

        #region Puplic Properties
        public string XMLFile
        {
            get { return _xmlFile; }
        }

        public string ConnStr
        {
            get { return _connStr; }
        }
        #endregion

        public XMLImportForm()
        {
            InitializeComponent();
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                this.textBoxXMLFile.Text = dlg.FileName;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this._xmlFile = this.textBoxXMLFile.Text;
            this._connStr = this.comboBoxDBNameList.SelectedValue.ToString();

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void XMLImportForm_Load(object sender, EventArgs e)
        {
            ComboBoxItem item = null;

            ComboBoxItemCollection comboBoxItemList =
                new ComboBoxItemCollection();

            item = new ComboBoxItem();
            item.Display = "UEI Public";
            item.Value = DBConnectionString.UEIPUBLIC;
            comboBoxItemList.Add(item);

            item = new ComboBoxItem();
            item.Display = "FAST 4.1";
            item.Value = DBConnectionString.FAST4_1;
            comboBoxItemList.Add(item);

            this.comboBoxItemCollectionBindingSource.DataSource
                = comboBoxItemList;
            this.comboBoxDBNameList.SelectedIndex = 0;
        }
    }
}