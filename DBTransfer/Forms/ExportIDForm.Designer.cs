namespace DBTransfer
{
    partial class ExportIDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBoxSrcDB = new System.Windows.Forms.ComboBox();
            this.srcComboBoxItemCollectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboBoxDestDB = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.destComboBoxItemCollectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.srcComboBoxItemCollectionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.destComboBoxItemCollectionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxSrcDB
            // 
            this.comboBoxSrcDB.DataSource = this.srcComboBoxItemCollectionBindingSource;
            this.comboBoxSrcDB.DisplayMember = "Display";
            this.comboBoxSrcDB.FormattingEnabled = true;
            this.comboBoxSrcDB.Location = new System.Drawing.Point(148, 40);
            this.comboBoxSrcDB.Name = "comboBoxSrcDB";
            this.comboBoxSrcDB.Size = new System.Drawing.Size(106, 21);
            this.comboBoxSrcDB.TabIndex = 0;
            this.comboBoxSrcDB.ValueMember = "Value";
            // 
            // srcComboBoxItemCollectionBindingSource
            // 
            this.srcComboBoxItemCollectionBindingSource.DataSource = typeof(BusinessObject.ComboBoxItemCollection);
            // 
            // comboBoxDestDB
            // 
            this.comboBoxDestDB.DataSource = this.destComboBoxItemCollectionBindingSource;
            this.comboBoxDestDB.DisplayMember = "Display";
            this.comboBoxDestDB.FormattingEnabled = true;
            this.comboBoxDestDB.Location = new System.Drawing.Point(276, 40);
            this.comboBoxDestDB.Name = "comboBoxDestDB";
            this.comboBoxDestDB.Size = new System.Drawing.Size(106, 21);
            this.comboBoxDestDB.TabIndex = 1;
            this.comboBoxDestDB.ValueMember = "Value";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Source DB";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(275, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Destination DB";
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(110, 81);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 4;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(213, 81);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // textBoxID
            // 
            this.textBoxID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxID.Location = new System.Drawing.Point(20, 40);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(106, 20);
            this.textBoxID.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "ID To Export";
            // 
            // destComboBoxItemCollectionBindingSource
            // 
            this.destComboBoxItemCollectionBindingSource.DataSource = typeof(BusinessObject.ComboBoxItemCollection);
            // 
            // ExportIDForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 125);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxDestDB);
            this.Controls.Add(this.comboBoxSrcDB);
            this.Name = "ExportIDForm";
            this.Text = "Export ID";
            this.Load += new System.EventHandler(this.ExportIDForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.srcComboBoxItemCollectionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.destComboBoxItemCollectionBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxSrcDB;
        private System.Windows.Forms.ComboBox comboBoxDestDB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.BindingSource srcComboBoxItemCollectionBindingSource;
        private System.Windows.Forms.BindingSource destComboBoxItemCollectionBindingSource;
    }
}