namespace DBTransfer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.actionByTNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkOutIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkInIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.createToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importFromXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.batchCheckOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.batchCheckInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteIDFromMainDBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableExportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.iDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.revertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iDToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.executorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkOutAllIDsForExecutorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.checkInAllIDsForExecutorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.actionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkOutTNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkInTNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actionByTNToolStripMenuItem,
            this.tableToolStripMenuItem,
            this.createToolStripMenuItem1,
            this.revertToolStripMenuItem,
            this.executorToolStripMenuItem,
            this.actionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(541, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // actionByTNToolStripMenuItem
            // 
            this.actionByTNToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkOutIDToolStripMenuItem,
            this.checkInIDToolStripMenuItem,
            this.toolStripSeparator6,
            this.createToolStripMenuItem,
            this.importFromXMLToolStripMenuItem,
            this.toolStripSeparator7,
            this.batchCheckOutToolStripMenuItem,
            this.batchCheckInToolStripMenuItem,
            this.toolStripSeparator1,
            this.deleteIDFromMainDBToolStripMenuItem});
            this.actionByTNToolStripMenuItem.Name = "actionByTNToolStripMenuItem";
            this.actionByTNToolStripMenuItem.Size = new System.Drawing.Size(30, 20);
            this.actionByTNToolStripMenuItem.Text = "ID";
            // 
            // checkOutIDToolStripMenuItem
            // 
            this.checkOutIDToolStripMenuItem.Name = "checkOutIDToolStripMenuItem";
            this.checkOutIDToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.checkOutIDToolStripMenuItem.Text = "Check Out";
            this.checkOutIDToolStripMenuItem.Click += new System.EventHandler(this.checkOutIDToolStripMenuItem_Click);
            // 
            // checkInIDToolStripMenuItem
            // 
            this.checkInIDToolStripMenuItem.Name = "checkInIDToolStripMenuItem";
            this.checkInIDToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.checkInIDToolStripMenuItem.Text = "Check In";
            this.checkInIDToolStripMenuItem.Click += new System.EventHandler(this.checkInIDToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(195, 6);
            // 
            // createToolStripMenuItem
            // 
            this.createToolStripMenuItem.Name = "createToolStripMenuItem";
            this.createToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.createToolStripMenuItem.Text = "Export";
            this.createToolStripMenuItem.Click += new System.EventHandler(this.exportIDToolStripMenuItem_Click);
            // 
            // importFromXMLToolStripMenuItem
            // 
            this.importFromXMLToolStripMenuItem.Name = "importFromXMLToolStripMenuItem";
            this.importFromXMLToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.importFromXMLToolStripMenuItem.Text = "Import From XML";
            this.importFromXMLToolStripMenuItem.Click += new System.EventHandler(this.importFromXMLToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(195, 6);
            // 
            // batchCheckOutToolStripMenuItem
            // 
            this.batchCheckOutToolStripMenuItem.Name = "batchCheckOutToolStripMenuItem";
            this.batchCheckOutToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.batchCheckOutToolStripMenuItem.Text = "Batch Check Out";
            this.batchCheckOutToolStripMenuItem.Click += new System.EventHandler(this.batchIDCheckOutToolStripMenuItem_Click);
            // 
            // batchCheckInToolStripMenuItem
            // 
            this.batchCheckInToolStripMenuItem.Name = "batchCheckInToolStripMenuItem";
            this.batchCheckInToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.batchCheckInToolStripMenuItem.Text = "Batch Check In";
            this.batchCheckInToolStripMenuItem.Click += new System.EventHandler(this.batchIDCheckInToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(195, 6);
            // 
            // deleteIDFromMainDBToolStripMenuItem
            // 
            this.deleteIDFromMainDBToolStripMenuItem.Name = "deleteIDFromMainDBToolStripMenuItem";
            this.deleteIDFromMainDBToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.deleteIDFromMainDBToolStripMenuItem.Text = "Delete ID From Main DB";
            this.deleteIDFromMainDBToolStripMenuItem.Visible = false;
            this.deleteIDFromMainDBToolStripMenuItem.Click += new System.EventHandler(this.deleteIDFromMainDBToolStripMenuItem_Click);
            // 
            // tableToolStripMenuItem
            // 
            this.tableToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tableExportToolStripMenuItem});
            this.tableToolStripMenuItem.Name = "tableToolStripMenuItem";
            this.tableToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.tableToolStripMenuItem.Text = "Table";
            // 
            // tableExportToolStripMenuItem
            // 
            this.tableExportToolStripMenuItem.Name = "tableExportToolStripMenuItem";
            this.tableExportToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.tableExportToolStripMenuItem.Text = "Synchronize";
            this.tableExportToolStripMenuItem.Click += new System.EventHandler(this.tableExportToolStripMenuItem_Click);
            // 
            // createToolStripMenuItem1
            // 
            this.createToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iDToolStripMenuItem});
            this.createToolStripMenuItem1.Name = "createToolStripMenuItem1";
            this.createToolStripMenuItem1.Size = new System.Drawing.Size(52, 20);
            this.createToolStripMenuItem1.Text = "Create";
            // 
            // iDToolStripMenuItem
            // 
            this.iDToolStripMenuItem.Name = "iDToolStripMenuItem";
            this.iDToolStripMenuItem.Size = new System.Drawing.Size(96, 22);
            this.iDToolStripMenuItem.Text = "ID";
            this.iDToolStripMenuItem.Click += new System.EventHandler(this.createIDToolStripMenuItem_Click);
            // 
            // revertToolStripMenuItem
            // 
            this.revertToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iDToolStripMenuItem1});
            this.revertToolStripMenuItem.Name = "revertToolStripMenuItem";
            this.revertToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.revertToolStripMenuItem.Text = "Revert";
            // 
            // iDToolStripMenuItem1
            // 
            this.iDToolStripMenuItem1.Name = "iDToolStripMenuItem1";
            this.iDToolStripMenuItem1.Size = new System.Drawing.Size(96, 22);
            this.iDToolStripMenuItem1.Text = "ID";
            this.iDToolStripMenuItem1.Click += new System.EventHandler(this.revertEditsAndUnlockIDToolStripMenuItem_Click);
            // 
            // executorToolStripMenuItem
            // 
            this.executorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkOutAllIDsForExecutorToolStripMenuItem1,
            this.checkInAllIDsForExecutorToolStripMenuItem1});
            this.executorToolStripMenuItem.Name = "executorToolStripMenuItem";
            this.executorToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.executorToolStripMenuItem.Text = "Executor";
            // 
            // checkOutAllIDsForExecutorToolStripMenuItem1
            // 
            this.checkOutAllIDsForExecutorToolStripMenuItem1.Name = "checkOutAllIDsForExecutorToolStripMenuItem1";
            this.checkOutAllIDsForExecutorToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.checkOutAllIDsForExecutorToolStripMenuItem1.Text = "Check Out ";
            this.checkOutAllIDsForExecutorToolStripMenuItem1.Click += new System.EventHandler(this.checkOutAllIDsForExecutorToolStripMenuItem_Click);
            // 
            // checkInAllIDsForExecutorToolStripMenuItem1
            // 
            this.checkInAllIDsForExecutorToolStripMenuItem1.Name = "checkInAllIDsForExecutorToolStripMenuItem1";
            this.checkInAllIDsForExecutorToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.checkInAllIDsForExecutorToolStripMenuItem1.Text = "Check In";
            this.checkInAllIDsForExecutorToolStripMenuItem1.Click += new System.EventHandler(this.checkInAllIDsForExecutorToolStripMenuItem_Click);
            // 
            // actionToolStripMenuItem
            // 
            this.actionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkOutTNToolStripMenuItem,
            this.checkInTNToolStripMenuItem});
            this.actionToolStripMenuItem.Name = "actionToolStripMenuItem";
            this.actionToolStripMenuItem.Size = new System.Drawing.Size(32, 20);
            this.actionToolStripMenuItem.Text = "TN";
            // 
            // checkOutTNToolStripMenuItem
            // 
            this.checkOutTNToolStripMenuItem.Name = "checkOutTNToolStripMenuItem";
            this.checkOutTNToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.checkOutTNToolStripMenuItem.Text = "Check Out ";
            this.checkOutTNToolStripMenuItem.Click += new System.EventHandler(this.checkOutTNToolStripMenuItem_Click);
            // 
            // checkInTNToolStripMenuItem
            // 
            this.checkInTNToolStripMenuItem.Name = "checkInTNToolStripMenuItem";
            this.checkInTNToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.checkInTNToolStripMenuItem.Text = "Check In";
            this.checkInTNToolStripMenuItem.Click += new System.EventHandler(this.checkInTNToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 356);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "DBTransfer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem actionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkOutTNToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkInTNToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actionByTNToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkOutIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkInIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem executorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tableToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem batchCheckInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem batchCheckOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkOutAllIDsForExecutorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem checkInAllIDsForExecutorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem iDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem revertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iDToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tableExportToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem deleteIDFromMainDBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importFromXMLToolStripMenuItem;
    }
}