using System;
using System.Windows.Forms;
using BusinessObject;

namespace DBTransfer {
    public partial class TransferInputForm : Form {
        private string _value;
        private string _connectionString;

        public string Value {
            get { return _value; }
            set { _value = value; }
        }

        public string DbConnectionString {
            get { return _connectionString; }
        }

        public TransferInputForm() {
            InitializeComponent();

            dbComboBox.Items.Add(new TempDBComboBoxItem("ueiTemp", DBConnectionString.UEITEMP));
            string domain = SystemInformation.UserDomainName;
            if (domain == "UEIC") {
                dbComboBox.Items.Add(new TempDBComboBoxItem("ueiTemp_India", DBConnectionString.UEITEMP_INDIA));
            }
            dbComboBox.SelectedIndex = 0;
        }

        private void buttonOK_Click(object sender, EventArgs e) {
            _value = textBoxValue.Text;
            _connectionString = ((TempDBComboBoxItem)dbComboBox.SelectedItem).ConnectionString;
        }

        public void SetLabel(string label) {
            labelValue.Text = label;
        }

        public void SetTitle(string title) {
            Text = title;
        }

        public void ShowDbSelection(bool showDBSelection) {
            dbComboBox.Visible = showDBSelection;
        }

        private class TempDBComboBoxItem {
            private string _caption;
            private string _connectionString;

            public TempDBComboBoxItem(string caption, string connectionString) {
                _caption = caption;
                _connectionString = connectionString;
            }

            public string ConnectionString {
                get { return _connectionString; }
            }

            public override string ToString() {
                return _caption;
            }
        }
    }
}