using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;

namespace DBTransfer
{
    public partial class ExportIDForm : Form
    {
        #region Private Fields
        string _id;
        string _srcConnStr;
        string _destConnStr;
        #endregion

        #region Puplic Properties
        public string ID
        {
            get { return _id; }
        }

        public string SrcConnStr
        {
            get { return _srcConnStr; }
        }

        public string DestConnStr
        {
            get { return _destConnStr; }
        }
        #endregion


        public ExportIDForm()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            _id = this.textBoxID.Text;
            _destConnStr = this.comboBoxDestDB.SelectedValue.ToString();
            _srcConnStr = this.comboBoxSrcDB.SelectedValue.ToString();
           
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void ExportIDForm_Load(object sender, EventArgs e)
        {
            ComboBoxItem item = null;

            /// src
            ComboBoxItemCollection srcComboBoxItemList =
                new ComboBoxItemCollection();

            item = new ComboBoxItem();
            item.Display = "UEI";
            item.Value = DBConnectionString.UEIPUBLIC;
            srcComboBoxItemList.Add(item);

            this.srcComboBoxItemCollectionBindingSource.DataSource
                = srcComboBoxItemList;
            this.comboBoxSrcDB.SelectedIndex = 0;

            /// dest
            ComboBoxItemCollection destComboBoxItemList =
                new ComboBoxItemCollection();

            item = new ComboBoxItem();
            item.Display = "FAST 4.1";
            item.Value = DBConnectionString.FAST4_1;
            destComboBoxItemList.Add(item);

            item = new ComboBoxItem();
            item.Display = "FAST 4.2";
            item.Value = DBConnectionString.FAST4_2;
            destComboBoxItemList.Add(item);

            item = new ComboBoxItem();
            item.Display = "FAST 4.3";
            item.Value = DBConnectionString.FAST4_3;
            destComboBoxItemList.Add(item);

            this.destComboBoxItemCollectionBindingSource.DataSource
                = destComboBoxItemList;
            this.comboBoxDestDB.SelectedIndex = 0;
        }
    }
}