using System;
using System.Windows.Forms;
using BusinessObject;

namespace DBTransfer {
    public partial class DBSelectionForm : Form {
        private string _connectionString;

        public DBSelectionForm() {
            InitializeComponent();

            dbComboBox.Items.Add(new TempDBComboBoxItem("ueiTemp", DBConnectionString.UEITEMP));
            string domain = SystemInformation.UserDomainName;
            if (domain == "UEIC") {
                dbComboBox.Items.Add(new TempDBComboBoxItem("ueiTemp_India", DBConnectionString.UEITEMP_INDIA));
            }
            dbComboBox.SelectedIndex = 0;
        }

        public string ConnectionString {
            get { return _connectionString; }
        }

        public void SetLabel(string label) {
            labelValue.Text = label;
        }

        private void buttonOK_Click(object sender, System.EventArgs e) {
            _connectionString = ((TempDBComboBoxItem)dbComboBox.SelectedItem).ConnectionString;
        }

        private class TempDBComboBoxItem {
            private string _caption;
            private string _connectionString;

            public TempDBComboBoxItem(string caption, string connectionString) {
                _caption = caption;
                _connectionString = connectionString;
            }

            public string ConnectionString {
                get { return _connectionString; }
            }

            public override string ToString() {
                return _caption;
            }
        }
    }
}