using System;
using System.Collections.Generic;
using System.Transactions;
using BusinessObject;

namespace DBTransfer
{
   /// <summary>
   /// 
   /// </summary>
    public static class DTCHelper
    {
        public static TransactionScope GetTransactionScope(int timeout)
        {
            TransactionOptions oTranOpt = new TransactionOptions();
            TimeSpan oTime = new TimeSpan(0, timeout, 0);
            oTranOpt.Timeout = oTime;

            return new TransactionScope(
                TransactionScopeOption.Required, oTranOpt);
        }
        
        /// <summary>
        /// TN list should be based on both Src and Dest
        /// 
        /// Src (TN1, TN2, TN3) for ID 1
        /// Dest (TN1, TN3, TN4) for ID 1
        /// 
        /// TN2 is added in Src
        /// TN4 is removed from ID 1 in Src (include if this exist in Src)
        /// 
        /// TransferUnit (ID1, TN1, TN2, TN3, TN4)
        /// </summary>
        /// <param name="idList"></param>
        /// <param name="connStrTodestDB"></param>
        /// <param name="connStrTosrcDB"></param>
        /// <returns></returns>
        public static TransferUnit BuildTransferUnit(IList<string> idList,
            string connStrTodestDB, string connStrTosrcDB)
        {
            DataStore destDS = new DataStore(connStrTodestDB);
            DataStore srcDS = new DataStore(connStrTosrcDB);

            TransferUnit transferUnit = new TransferUnit(destDS, srcDS);
            transferUnit.IDList = idList;

            transferUnit.TNList = srcDS.GetTNList(idList);

            IntegerCollection tnList = destDS.GetTNList(idList);
            foreach (int tn in tnList)
            {
                if (transferUnit.TNList.Contains(tn) != true &&
                    srcDS.DoesExist(tn) == true)
                {
                    transferUnit.TNList.Add(tn);
                }
            }

            return transferUnit;
        }

        /// <summary>
        /// Builds a transfer unit for TN transfers. \n
        /// This function does not search for any other 
        /// TNs to put in the list.
        /// </summary>
        /// <param name="tnList"></param>
        /// <param name="connStrTodestDB"></param>
        /// <param name="connStrTosrcDB"></param>
        /// <returns></returns>
        public static TransferUnit BuildTransferUnit(IList<int> tnList,
            string connStrTodestDB, string connStrTosrcDB)
        {
            DataStore destDS = new DataStore(connStrTodestDB);
            DataStore srcDS = new DataStore(connStrTosrcDB);

            TransferUnit transferUnit = new TransferUnit(destDS, srcDS);
            transferUnit.TNList = tnList;

            return transferUnit;
        }
    }
}
