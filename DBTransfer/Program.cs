using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Reflection;

namespace DBTransfer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                OperateInCommandLineMode(args[0], args[1]);
            }
            else
            {
                Application.EnableVisualStyles();
                Application.Run(new MainForm());
            }
        }

        /// <summary>
        /// using reflection to redirect
        /// protect from crashing (try/catch)
        /// </summary>
        /// <param name="command"></param>
        private static void OperateInCommandLineMode(string mode, string command)
        {
            string typeName = "DBTransfer." + mode + "TransferWrapper";
            Type type = Type.GetType(typeName);

            MethodInfo methodInfo = type.GetMethod(command);
            if (methodInfo == null)
            {
                MessageBox.Show(methodInfo.Name + " not supported");
            }
            else
            {
                try
                {
                    methodInfo.Invoke(type, null);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}