using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using CommonForms;

namespace DBSearch
{
    public partial class IntronLabelSearchForm : Form
    {
        public DataSet result;
        IDSelectionForm IDSelection;
        string type;

        public IntronLabelSearchForm(string type, 
            IDSelectionForm IDSelection)
        {
            InitializeComponent();
            this.IDSelection = IDSelection;
            this.type = type;
            this.Text = this.Text.Replace("Intron", type);
            label1.Text = label1.Text.Replace("intron",type.ToLower());
            result = new DataSet("Functions");
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            string conditions = GetConditions();
            FunctionCollection functions = null;

            try
            {
                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
                DAOFactory.BeginTransaction();
                functions = ProductDBFunctions.GetAllFunc(conditions);
                DAOFactory.CommitTransaction();
            }
            catch (Exception ex)
            {
                DAOFactory.RollBackTransaction();
                MessageBox.Show(ex.Message);
            }

            if (functions == null)
            {
                Cursor.Current = Cursors.Default;
                this.DialogResult = DialogResult.OK;
                return;
            }

            CommonFunctions helper = new CommonFunctions();

            result.Tables.Add("Table");
            result.Tables["Table"].Columns.Add("ID");
            result.Tables["Table"].Columns.Add("DATA");
            result.Tables["Table"].Columns.Add("SYNTH");
            result.Tables["Table"].Columns.Add("LABEL");
            result.Tables["Table"].Columns.Add("INTRON");
            result.Tables["Table"].Columns.Add("PRIORITY");

            foreach (Function func in functions)
            {
                result.Tables["Table"].Rows.Add(func.ID, 
                    CommonFunctions.SingleQuote(func.Data),
                    CommonFunctions.SingleQuote(func.Synth),
                    func.Label, func.Intron,func.IntronPriority);
            }

            Cursor.Current = Cursors.Default;
            this.DialogResult = DialogResult.OK;  
        }

        string GetConditions()
        {
            string conditions = IDSelection.GetQueryConditions();

            if (conditions != "")
            {
                conditions = "(" + conditions + ") ";
                conditions = conditions.Replace("ID", "[function].ID");
            }

            string intronOrLabelCond = GetIntronOrLabelConditions();

            if (intronOrLabelCond != "")
                intronOrLabelCond = "(" + intronOrLabelCond + ") ";

            if (conditions != "" && intronOrLabelCond != "")
                conditions += " AND " + intronOrLabelCond;
            else if (intronOrLabelCond != "")
                conditions += intronOrLabelCond;

            return conditions;
        }

        string GetIntronOrLabelConditions()
        {
            string conidtions = "";

            string text = Intron1.Text;
            GetCond(ref conidtions, text);

            text = Intron2.Text;
            GetCond(ref conidtions, text);

            text = Intron3.Text;
            GetCond(ref conidtions, text);

            return conidtions;
        }

        private void GetCond(ref string conidtions, string text)
        {
            if (text != null)
            {
                char[] trimChars = { '*' };
                text = text.Trim();
                if (text.Length > 0)
                {
                    if (conidtions != "")
                        conidtions += " OR ";

                    conidtions += "[function]." + type + " LIKE ";
                    conidtions += "('";

                    if (text[0] == '*')
                    {
                        conidtions += "%";
                    }

                    conidtions += text.Trim(trimChars);

                    if (text.Length > 1 && text[text.Length - 1] == '*')
                    {
                        conidtions += "%";
                    }
                    conidtions += "')";
                }
            }
        }
    }
}