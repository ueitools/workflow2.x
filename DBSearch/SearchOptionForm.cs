using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using UEI.Workflow2010.Report;
using UEI.Workflow2010.Report.Model;
using UEI.Workflow2010.Report.Service;
using UEI.Workflow2010.Report.DataAccess;

namespace DBSearch
{
    public enum SelType {ENTRY, FILE };

    public partial class SearchOptionForm : Form
    {
        public SelectType Choice = new SelectType();
        //
        private IDBAccess _DBAccess = null;
        private IList<DataSources> _dataSources = null;
        public IList<DataSources> DataSources
        {
            get { return _dataSources; }
        }
        //
        public SearchOptionForm()
        {
            InitializeComponent();

            // add auto complete items for tbBrand
            BrandCollection allBrands = DBFunctions.GetAllBrandList();
            foreach (Brand brand in allBrands)
            {
                if (brand.Name != null)
                    this.tbBrand.AutoCompleteCustomSource.Add(brand.Name);
            }
            _dataSources = new List<DataSources>();
            LoadDataSources();
        }

        #region Load Data Source Location
        private void LoadDataSources()
        {
            Reports _Report = new Reports();
            IList<DataSources> _dataSourcesList = _Report.GetAllDataSources();
            if (_dataSourcesList.Count > 0)
            {
                treeDataSources.Nodes.Add("DataSources", "DataSources");
                treeDataSources.Nodes["DataSources"].Tag = "All";
                treeDataSources.Nodes["DataSources"].Checked = true;
                foreach (DataSources item in _dataSourcesList)
                {
                    if (item.Name.Contains("CF"))
                    {
                        if (item.Name.Contains(":"))
                        {
                            String[] _SubDataSource = item.Name.Split(':');
                            if (treeDataSources.Nodes["DataSources"].Nodes[_SubDataSource[0]] == null)
                            {
                                treeDataSources.Nodes["DataSources"].Nodes.Add(_SubDataSource[0], _SubDataSource[0]);
                                treeDataSources.Nodes["DataSources"].Nodes[_SubDataSource[0]].Tag = "DataSource";
                                treeDataSources.Nodes["DataSources"].Nodes[_SubDataSource[0]].Checked = true;
                            }

                            treeDataSources.Nodes["DataSources"].Nodes[_SubDataSource[0]].Nodes.Add(_SubDataSource[1], _SubDataSource[1]);
                            treeDataSources.Nodes["DataSources"].Nodes[_SubDataSource[0]].Nodes[_SubDataSource[1]].Tag = "SubDataSource";
                            treeDataSources.Nodes["DataSources"].Nodes[_SubDataSource[0]].Nodes[_SubDataSource[1]].Checked = true;
                        }
                        else
                        {
                            treeDataSources.Nodes["DataSources"].Nodes.Add(item.Name, item.Name);
                            treeDataSources.Nodes["DataSources"].Nodes[item.Name].Tag = "DataSource";
                            treeDataSources.Nodes["DataSources"].Nodes[item.Name].Checked = true;

                            treeDataSources.Nodes["DataSources"].Nodes[item.Name].Nodes.Add(item.Name, item.Name);
                            treeDataSources.Nodes["DataSources"].Nodes[item.Name].Nodes[item.Name].Tag = "SubDataSource";
                            treeDataSources.Nodes["DataSources"].Nodes[item.Name].Nodes[item.Name].Checked = true;
                        }
                    }
                    else
                    {
                        treeDataSources.Nodes["DataSources"].Nodes.Add(item.Name, item.Name);
                        treeDataSources.Nodes["DataSources"].Nodes[item.Name].Tag = "DataSource";
                        treeDataSources.Nodes["DataSources"].Nodes[item.Name].Checked = true;
                    }
                    _dataSources.Add(new DataSources(item.Name));
                }
                treeDataSources.Nodes["DataSources"].Expand();
            }
        }
        #endregion

        public string getChoiceResult()
        {
            return Choice.Result.ToString();
        }

        private void rdobtnInput_CheckedChanged(object sender, EventArgs e)
        {
            this.groupBox1.Enabled = true;
            this.groupBox2.Enabled = false;
            Choice.SelectionType = SelType.ENTRY;
        }

        private void rdobtnFile_CheckedChanged(object sender, EventArgs e)
        {
            this.groupBox1.Enabled = false;
            this.groupBox2.Enabled = true;
            Choice.SelectionType = SelType.FILE;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool result = false;

            Choice.Branch = MakeBranchString();
            Choice.ComboFlag = MakeComboString();
            switch (Choice.SelectionType)
            {
                case SelType.ENTRY:
                    Choice.Result = this.tbMode.Text.ToString() + "|" + this.tbBrand.Text.ToString() +
                        "|"+this.tbModel.Text.ToString();
                    result = true;
                    break;
                case SelType.FILE:
                    if (tbInputFile.Text.Length > 0)
                    {
                        Choice.Result = this.tbInputFile.Text;
                        result = true;
                    }
                    break;
            }

            if (!result)
            {
                MessageBox.Show("You haven't completed your selection.", "ERROR");
                return;
            }

            this.DialogResult = DialogResult.OK;
        }

        private string MakeBranchString()
        {
            List<string> branchList = new List<string>();

            //if (caCheckBox.Checked)
            //    branchList.Add("CA");
            //if (cyCheckBox.Checked)
            //    branchList.Add("CY");
            //if (euCheckBox.Checked)
            //    branchList.Add("EU");
            //if (blCheckBox.Checked)
            //    branchList.Add("BL");
            //SelectedDataSources();

            foreach (DataSources item in _dataSources)
            {
                branchList.Add(item.Name);
            }
            return String.Join("|", branchList.ToArray());
        }

        private string MakeComboString()
        {
            if (fuzzyRadio.Checked)
                return (string)fuzzyRadio.Tag;
            else if (exactRadio.Checked)
                return (string)exactRadio.Tag;
            return (string)comboRadio.Tag;                
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// display an open file dialog to select an Input file
        /// </summary>
        private void GetInputFile()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Excel file (*.xls)|*.xls";
            dlg.Filter += "|Text file (*.txt)|*.txt";
            dlg.Filter += "|All File Types (*.*)|*.*";
            dlg.DefaultExt = "xls";
            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                this.tbInputFile.Text = dlg.FileName;
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            rdobtnFile.Checked = true;
            GetInputFile();
        }

        #region DataSource
        private void treeDataSources_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.Unknown)
            {
                TreeNode node = ((TreeNode)e.Node);
                if (node != null)
                {
                    switch (node.Tag.ToString())
                    {
                        case "All":
                            checkNodes(node, node.Checked);
                            break;
                        case "DataSource":
                            if (node.Text == "CF")
                            {
                                checkNodes(node, node.Checked);
                            }
                            if (node.Parent.Nodes.Count == GetCheckedNodes(node.Parent))
                            {
                                node.Parent.Checked = true;
                            }
                            else
                            {
                                node.Parent.Checked = false;
                            }
                            break;
                        case "SubDataSource":
                            if (node.Parent.Nodes.Count == GetCheckedNodes(node.Parent))
                            {
                                node.Parent.Checked = true;
                            }
                            else
                            {
                                node.Parent.Checked = false;
                            }
                            if (node.Parent.Parent.Nodes.Count == GetCheckedNodes(node.Parent.Parent))
                            {
                                node.Parent.Parent.Checked = true;
                            }
                            else
                            {
                                node.Parent.Parent.Checked = false;
                            }
                            break;
                    }
                    SelectedDataSources();
                }
            }
        }

        private void checkNodes(TreeNode node, bool check)
        {
            if (node != null)
                foreach (TreeNode child in node.Nodes)
                {
                    child.Checked = check;
                    checkNodes(child, check);
                }
        }
        private Int32 GetCheckedNodes(TreeNode node)
        {
            Int32 checkedNodes = 0;
            if (node != null)
                foreach (TreeNode child in node.Nodes)
                {
                    if (child.Checked)
                        checkedNodes++;
                }
            return checkedNodes;
        }
        private void SelectedDataSources()
        {
            if (treeDataSources.Nodes != null)
            {
               // _LoadDevicesFlag = true;
                _dataSources = new List<DataSources>();
                foreach (TreeNode mainNode in treeDataSources.Nodes)
                {
                    if (mainNode.Nodes != null)
                    {
                        foreach (TreeNode datasourceNode in mainNode.Nodes)
                        {
                            if (datasourceNode.Checked == true)
                            {
                                if (datasourceNode.Text != "CF")
                                    _dataSources.Add(new DataSources(datasourceNode.Text));
                            }
                            if (datasourceNode.Nodes != null)
                            {
                                foreach (TreeNode subdatasourceNode in datasourceNode.Nodes)
                                {
                                    if (subdatasourceNode.Checked == true)
                                    {
                                        if (subdatasourceNode.Text != "CF")
                                            _dataSources.Add(new DataSources(datasourceNode.Text + ":" + subdatasourceNode.Text));
                                        else
                                            _dataSources.Add(new DataSources(subdatasourceNode.Text));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
#endregion
     }

    public class SelectType
    {
        public SelType SelectionType;
        public string Result;

        private string _branch;
        public string Branch
        {
            get { return _branch; }
            set { _branch = value; }
        }
        private string _comboSwitch;
        public string ComboFlag
        {
            get { return _comboSwitch; }
            set { _comboSwitch = value; }
        }
    };
}