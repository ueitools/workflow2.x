using System;
using System.Collections.Generic;
//using System.Text;
using System.Windows.Forms;

namespace DBSearch
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();

            if (args != null && args.Length > 0)
            {
                SearchUEIDevice SE = new SearchUEIDevice(args[0]);
                SE.BeginSearch();
            }
            else
            {
                SearchUEIDevice SE = new SearchUEIDevice();
                SE.BeginSearch();
            }
        }
    }
}
