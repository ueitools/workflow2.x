using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using IOWrapper;
using System.Windows.Forms;
using DBSearch.elcinsql; // for web service
//using DBSearch.ModelWS;
using CommonForms;

namespace DBSearch
{
    class SearchUEIDevice
    {
        private string type;

        private IOWrapper.ExcelIOWrapper ExcelIO;
        private ModelWS ws = new ModelWS();
        //private Model ws = new Model();
        private string strMode, strBrand, strModel, strCombo, strBranch;
        private UEIDeviceCode deviceCode = new UEIDeviceCode();
        private ProgressDlg Progress = new ProgressDlg();

        public SearchUEIDevice()
        {
            this.type = "";
            AssignWSAddress();
        }

        public SearchUEIDevice(string type)
        {
            this.type = type;
            AssignWSAddress();
        }

        private void AssignWSAddress()
        {
            ws.Url = WorkflowConfig.GetCfg("ModelWSv2");
        }

        public void BeginSearch()
        {
            if(type == "" || type == "Model")
                BeginModelNumberSearch();
            else BeginIntronLabelSearch();
        }

        private void BeginIntronLabelSearch()
        {
            ExcelIO = new ExcelIOWrapper("functions.xsl");
            IDSelectionForm IDSelection = new IDSelectionForm();
            DialogResult res = IDSelection.ShowDialog();
            IDSelection.Close();

            if(res == DialogResult.Cancel)
            {
                return;
            }

            IntronLabelSearchForm IntronLabelSearch
                = new IntronLabelSearchForm(type, IDSelection);
            res = IntronLabelSearch.ShowDialog();
            IntronLabelSearch.Close();

            if(res == DialogResult.Cancel)
            {
                return;
            }

            ExcelIO.DataSetToExcel(IntronLabelSearch.result);
            IntronLabelSearch.Dispose();
        }

        private void BeginModelNumberSearch()
        {
            ExcelIO = new ExcelIOWrapper();
            SearchOptionForm SearchOption = new SearchOptionForm();

            DialogResult res = SearchOption.ShowDialog();
            SearchOption.Close();


            if(res == DialogResult.OK)
            {
                Progress.Show();

                DataSet dsOutput = new DataSet();
                switch(SearchOption.Choice.SelectionType)
                {
                    case SelType.ENTRY:
                        string[] results = SearchOption.Choice.Result.Split('|');
                        strMode = results[0];
                        strBrand = results[1];
                        strModel = results[2];
                        strCombo = SearchOption.Choice.ComboFlag;
                        strBranch = SearchOption.Choice.Branch;
                        dsOutput = SearchByEntry(strMode, strBrand, strModel, strBranch, strCombo);
                        break;
                    case SelType.FILE:
                        // Get Excel Data from input excel file and parse it
                        // into a DataSet
                        string filepath = SearchOption.getChoiceResult();
                        strCombo = SearchOption.Choice.ComboFlag;
                        strBranch = SearchOption.Choice.Branch;
                        try
                        {
                            DataSet dsInput = ExcelIO.ExcelToDataSet(filepath,
                                "Sheet1");
                            dsOutput = SearchByFile(dsInput, strCombo, strBranch);

                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        break;
                }
                // Call IOWapper again to send the DataSet to Excel
                ExcelIO.DataSetToExcel(dsOutput);
            }

            Progress.Close();

            SearchOption.Dispose();
        }

        DataSet SearchByEntry(string mode, string brand, string model, string branch, string combo)
        {
            DataSet dsOutput = new DataSet("Devices");
            dsOutput.Tables.Add("Table");
            dsOutput.Tables[0].Columns.Add("Device_Search");
            dsOutput.Tables[0].Columns.Add("Brand");
            dsOutput.Tables[0].Columns.Add("Device_Model");
            dsOutput.Tables[0].Columns.Add("Mode_DB");
            dsOutput.Tables[0].Columns.Add("Model_DB");
            dsOutput.Tables[0].Columns.Add("ID");
            dsOutput.Tables[0].Columns.Add("Fuzzy_Factor");

            const string _USERNAME = "test";
            const string _TOKEN = "vbgfjllk0010asml";
            const string AllMarket = "";
            const int MINFUZZY = 50;
            //UEIDeviceCodeListEx result = ws.GetUEIDeviceCodeComboV2(
            //    _USERNAME, _TOKEN, "WORKFOLOW",
            //    mode, brand, model, AllMarket, MINFUZZY, combo, branch);

            //UEIDeviceCodeListEx result = ws.GetUEIDeviceCodeComboV2(
            //    _USERNAME, _TOKEN, "WORKFOLOW",
            //    mode, brand, model, AllMarket, MINFUZZY, combo, branch);
            UEIDeviceCodeListEx result = ws.GetUEIDeviceCodeComboV2(
               _USERNAME,
               _TOKEN,
               "WORKFOLOW",
               mode,
               string.Empty,
               string.Empty,
               string.Empty,
               string.Empty,
               brand,
               model,
               branch,
               AllMarket,
               string.Empty,
               string.Empty,
               MINFUZZY,
               combo);

            if(result.error_code == 0)
            {
                Progress.SetupProgressBar(0, result.codeList.Length, 1);
                foreach(UEIDeviceCode device_code in result.codeList)
                {
                    Progress.UpdateProgressBar();
                    dsOutput.Tables[0].Rows.Add(mode, brand, model, mode,
                      device_code.model, device_code.code, device_code.fuzzyFactor);
                }
            }
            return dsOutput;
        }

        DataSet SearchByFile(DataSet dsInput, string combo, string branch)
        {
            Progress.SetupProgressBar(0, dsInput.Tables[0].Rows.Count, 1);

            DataSet dsOutput = dsInput.Copy();
            string strMode_DB = ""; // the mode we used to search
            dsOutput.Tables[0].Columns.Add("Mode_DB");
            dsOutput.Tables[0].Columns.Add("Model_DB");
            dsOutput.Tables[0].Columns.Add("ID");
            dsOutput.Tables[0].Columns.Add("Fuzzy_Factor");

            // process the input data 
            foreach(DataRow row in dsOutput.Tables[0].Rows)
            {
                Progress.UpdateProgressBar();

                strMode_DB = strMode = row["Device_Search"].ToString();
                strBrand = row["Brand"].ToString();
                strModel = row["Device_Model"].ToString();

                // Call Webservice with Mode, Brand, Model
                deviceCode =
                GetUEIDeviceCodeFromWS(ws, strMode, strBrand, strModel, combo, branch);

                // check fuzzy factor
                if(deviceCode == null ||
                    Int32.Parse(deviceCode.fuzzyFactor) < 50)
                // 50 is the given fuzzyFactor threshold
                {
                    strMode_DB = string.Empty;
                    deviceCode = GetUEIDeviceCodeFromWS(ws,
                        "", strBrand, strModel, combo, branch);
                }

                // put deviceCode into DataSet
                if(deviceCode != null)
                {
                    row["Mode_DB"] = strMode_DB;
                    row["Model_DB"] = deviceCode.model;
                    row["ID"] = deviceCode.code;
                    row["Fuzzy_Factor"] = deviceCode.fuzzyFactor;
                }
                else
                {
                    row["Mode_DB"] = string.Empty;
                    row["Model_DB"] = string.Empty;
                    row["ID"] = string.Empty;
                    row["Fuzzy_Factor"] = string.Empty;
                }
            }
            return dsOutput;
        }

        /// <summary>
        /// Only first non-minus-one record from web service
        /// </summary>
        /// <param name="ws"></param>
        /// <param name="mode"></param>
        /// <param name="brand"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        static UEIDeviceCode GetUEIDeviceCodeFromWS(ModelWS ws,
            string mode, string brand, string model, string combo, string branch)
        {
            int return_index = 0;
            const string _USERNAME = "test";
            const string _TOKEN = "vbgfjllk0010asml";
            const string ANYMARKET = "";
            const int DEFAULTMINFUZZY = 50;
            //UEIDeviceCodeListEx result = ws.GetUEIDeviceCodeComboV2(
            //    _USERNAME, _TOKEN, "WORKFOLOW", 
            //    mode, brand, model, ANYMARKET, DEFAULTMINFUZZY, combo, branch);

            //UEIDeviceCodeListEx result = ws.GetUEIDeviceCodeComboV2(
            //    _USERNAME, _TOKEN, "WORKFOLOW",
            //    mode, brand, model, ANYMARKET, DEFAULTMINFUZZY, combo, branch);

            UEIDeviceCodeListEx result = ws.GetUEIDeviceCodeComboV2(
          _USERNAME,
          _TOKEN,
          "WORKFOLOW",
          mode,
          string.Empty,
          string.Empty,
          string.Empty,
          string.Empty,
          brand,
          model,
          branch,
          ANYMARKET,
          string.Empty,
          string.Empty,
          DEFAULTMINFUZZY,
          combo);


            if(result.error_code == 0 && result.codeList.Length > 0)
            {
                // try to get first code with non -1 fuzzy factor 
                for(; return_index < result.codeList.Length - 1;
                    return_index++)
                {
                    if(result.codeList[return_index].fuzzyFactor != "-1")
                        break;
                }

                if(result.codeList[return_index].fuzzyFactor != "-1")
                    return result.codeList[return_index];
                else
                    return null;
            }
            else
                return null;
        }
    }
}
