<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
    <HTML>
      <HEAD>
        <STYLE>
          .HDR { background-color:bisque;font-weight:bold }
        </STYLE>
      </HEAD>
      <BODY>
        <TABLE border="1" bordercolor="#CCCCCC">
          <COLGROUP WIDTH="100" ALIGN="LEFT"></COLGROUP>
          <COLGROUP WIDTH="100" ALIGN="LEFT"></COLGROUP>
          <COLGROUP WIDTH="200" ALIGN="LEFT"></COLGROUP>
	  <COLGROUP WIDTH="100" ALIGN="LEFT"></COLGROUP>
          <COLGROUP WIDTH="200" ALIGN="LEFT"></COLGROUP>
          <COLGROUP WIDTH="100" ALIGN="LEFT"></COLGROUP>
      	  <COLGROUP WIDTH="100" ALIGN="LEFT"></COLGROUP>
          <TD CLASS="">Device_Search</TD>
          <TD CLASS="">Brand</TD>
          <TD CLASS="">Device_Model</TD>
	  <TD CLASS="">Mode_DB</TD>
	  <TD CLASS="">Model_DB</TD>
	  <TD CLASS="">ID</TD>	  
	  <TD CLASS="">Fuzzy_Factor</TD>
          <xsl:for-each select="Devices/Table">
            <TR>
              <TD><xsl:value-of select="Device_Search"/></TD>
              <TD><xsl:value-of select="Brand"/></TD>
              <TD><xsl:value-of select="Device_Model"/></TD>
              <TD><xsl:value-of select="Mode_DB"/></TD>
	      <TD><xsl:value-of select="Model_DB"/></TD>
	      <TD><xsl:value-of select="ID"/></TD>
	      <TD><xsl:value-of select="Fuzzy_Factor"/></TD>
            </TR>
          </xsl:for-each>
        </TABLE>
      </BODY>
    </HTML>
  </xsl:template>
</xsl:stylesheet>