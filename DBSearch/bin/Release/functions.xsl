<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
    <HTML>
      <HEAD>
        <STYLE>
          .HDR { background-color:bisque;font-weight:bold }
        </STYLE>
      </HEAD>
      <BODY>
        <TABLE border="1" bordercolor="#CCCCCC">
          <COLGROUP WIDTH="100" ALIGN="LEFT"></COLGROUP>
          <COLGROUP WIDTH="100" ALIGN="LEFT"></COLGROUP>
          <COLGROUP WIDTH="200" ALIGN="LEFT"></COLGROUP>
	  <COLGROUP WIDTH="100" ALIGN="LEFT"></COLGROUP>
          <COLGROUP WIDTH="200" ALIGN="LEFT"></COLGROUP>
          <COLGROUP WIDTH="100" ALIGN="LEFT"></COLGROUP>
          <TD CLASS="">ID</TD>
          <TD CLASS="">DATA</TD>
          <TD CLASS="">SYNTH</TD>
	  <TD CLASS="">LABEL</TD>
	  <TD CLASS="">INTRON</TD>
	  <TD CLASS="">PRIORITY</TD>
          <xsl:for-each select="Functions/Table">
            <TR>
              <TD><xsl:value-of select="ID"/></TD>
              <TD><xsl:value-of select="DATA"/></TD>
              <TD><xsl:value-of select="SYNTH"/></TD>
              <TD><xsl:value-of select="LABEL"/></TD>
	      <TD><xsl:value-of select="INTRON"/></TD>
	      <TD><xsl:value-of select="PRIORITY"/></TD>
            </TR>
          </xsl:for-each>
        </TABLE>
      </BODY>
    </HTML>
  </xsl:template>
</xsl:stylesheet>