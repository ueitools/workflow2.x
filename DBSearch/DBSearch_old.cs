using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using IOWrapper;
using System.Windows.Forms;
using DBSearch.elcinsql; // for web service
using CommonForms;

namespace DBSearch
{   
    class SearchUEIDevice
    {
        private SearchOptionForm SearchOption = new SearchOptionForm();
      
        //private ProgressDlg Progress = new ProgressDlg();

        public void BeginSearch()
        {
            DialogResult res = SearchOption.ShowDialog();
            SearchOption.Close();

            if (res == DialogResult.OK)
            {
                SearchDeviceRpt Progress = new SearchDeviceRpt();
                Progress.SearchType = SearchOption.Choice.SelectionType;
                Progress.SearchInput = SearchOption.Choice.Result;
                Progress.Show();
                Progress.DoSearch();
                Progress.Close();
            }

             SearchOption.Dispose();
        }

    
    }

    class SearchDeviceRpt : ProgressDlg
    {
        public SelType SearchType;
        public string SearchInput;
        private IOWrapper.ExcelIOWrapper ExcelIO = new ExcelIOWrapper();
        private ModelWS ws = new ModelWS();
        private string strMode, strBrand, strModel;
        private UEIDeviceCode deviceCode = new UEIDeviceCode();

        public void DoSearch()
        {
            DataSet dsOutput = new DataSet();
            switch (SearchType)
            {
                case SelType.TEXT:
                    string[] results = SearchInput.Split('|');
                    strMode = results[0];
                    strBrand = results[1];
                    strModel = results[2];
                    dsOutput = SearchByText(strMode, strBrand, strModel);
                    break;
                case SelType.FILE:
                    // Get Excel Data from input excel file and parse it
                    // into a DataSet
                    string filepath = SearchInput;
                    try
                    {
                        DataSet dsInput = ExcelIO.ExcelToDataSet(filepath,
                            "All Brands");
                        dsOutput = SearchByFile(dsInput);

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    break;
            }
            // Call IOWapper again to send the DataSet to Excel
            ExcelIO.DataSetToXML(dsOutput, "C:\\dbsearch.xml",
                "C:\\devices.xsl");
            ExcelIO.XMLToExcel("C:\\dbsearch.xml",
                "C:\\dbsearch.htm", "C:\\devices.xsl");
        }

        DataSet SearchByText(string mode, string brand, string model)
        {
            DataSet dsOutput = new DataSet("Devices");
            dsOutput.Tables.Add("Table");
            dsOutput.Tables[0].Columns.Add("Device_Search");
            dsOutput.Tables[0].Columns.Add("Brand");
            dsOutput.Tables[0].Columns.Add("Device_Model");
            dsOutput.Tables[0].Columns.Add("Mode_DB");
            dsOutput.Tables[0].Columns.Add("Model_DB");
            dsOutput.Tables[0].Columns.Add("ID");
            dsOutput.Tables[0].Columns.Add("Fuzzy_Factor");

            const string _USERNAME = "test";
            const string _TOKEN = "vbgfjllk0010asml";
            UEIDeviceCodeListEx result = ws.GetUEIDeviceCodeCombo(
            _USERNAME, _TOKEN, "WORKFOLOW",
            mode, brand, model, "", 200, 0);

            foreach (UEIDeviceCode device_code in result.codeList)
            {
                dsOutput.Tables[0].Rows.Add(mode, brand, model, mode, device_code.model,
                    device_code.code, device_code.fuzzyFactor);
            }

            return dsOutput;
        }

        DataSet SearchByFile(DataSet dsInput)
        {
            //Progress.SetupProgressBar(0, dsInput.Tables[0].Rows.Count, 1);

            DataSet dsOutput = dsInput.Copy();
            string strMode_DB = ""; // the mode we used to search
            dsOutput.Tables[0].Columns.Add("Mode_DB");
            dsOutput.Tables[0].Columns.Add("Model_DB");
            dsOutput.Tables[0].Columns.Add("ID");
            dsOutput.Tables[0].Columns.Add("Fuzzy_Factor");
            foreach (DataRow row in dsOutput.Tables[0].Rows)
            {
                strMode_DB = strMode = row["Device_Search"].ToString();
                strBrand = row["Brand"].ToString();
                strModel = row["Device_Model"].ToString();

                // Call Webservice with Mode, Brand, Model
                deviceCode =
                GetUEIDeviceCodeFromWS(ws, strMode, strBrand, strModel);

                // check fuzzy factor
                if (deviceCode == null ||
                    Int32.Parse(deviceCode.fuzzyFactor) < 50)
                // 50 is the given fuzzyFactor threshold
                {
                    strMode_DB = string.Empty;
                    deviceCode = GetUEIDeviceCodeFromWS(ws,
                        "", strBrand, strModel);
                }

                // put deviceCode into DataSet
                if (deviceCode != null)
                {
                    row["Mode_DB"] = strMode_DB;
                    row["Model_DB"] = deviceCode.model;
                    row["ID"] = deviceCode.code;
                    row["Fuzzy_Factor"] = deviceCode.fuzzyFactor;
                }
                else
                {
                    row["Mode_DB"] = string.Empty;
                    row["Model_DB"] = string.Empty;
                    row["ID"] = string.Empty;
                    row["Fuzzy_Factor"] = string.Empty;
                }

                //Progress.UpdateProgressBar();
            }
            return dsOutput;
        }

        /// <summary>
        /// Only first non-minus-one record from web service
        /// </summary>
        /// <param name="ws"></param>
        /// <param name="mode"></param>
        /// <param name="brand"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        static UEIDeviceCode GetUEIDeviceCodeFromWS(ModelWS ws,
            string mode, string brand, string model)
        {
            int return_index = 0;
            const string _USERNAME = "test";
            const string _TOKEN = "vbgfjllk0010asml";
            UEIDeviceCodeListEx result = ws.GetUEIDeviceCodeCombo(
                _USERNAME, _TOKEN, "WORKFOLOW",
                mode, brand, model, "", 25, 0);

            if (result.error_code == 0 && result.codeList.Length > 0)
            {
                // try to get first code with non -1 fuzzy factor 
                for (; return_index < result.codeList.Length - 1;
                    return_index++)
                {
                    if (result.codeList[return_index].fuzzyFactor != "-1")
                        break;
                }
                if (result.codeList[return_index].fuzzyFactor != "-1")
                    return result.codeList[return_index];
                else
                    return null;
            }
            else
                return null;
        }
    }
}
