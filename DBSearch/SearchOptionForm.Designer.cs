namespace DBSearch
{
    partial class SearchOptionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdobtnInput = new System.Windows.Forms.RadioButton();
            this.rdobtnFile = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.tbMode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbBrand = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbModel = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.tbInputFile = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.exactRadio = new System.Windows.Forms.RadioButton();
            this.fuzzyRadio = new System.Windows.Forms.RadioButton();
            this.comboRadio = new System.Windows.Forms.RadioButton();
            this.blCheckBox = new System.Windows.Forms.CheckBox();
            this.euCheckBox = new System.Windows.Forms.CheckBox();
            this.cyCheckBox = new System.Windows.Forms.CheckBox();
            this.caCheckBox = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.treeDataSources = new System.Windows.Forms.TreeView();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // rdobtnInput
            // 
            this.rdobtnInput.AutoSize = true;
            this.rdobtnInput.Location = new System.Drawing.Point(8, 81);
            this.rdobtnInput.Name = "rdobtnInput";
            this.rdobtnInput.Size = new System.Drawing.Size(80, 17);
            this.rdobtnInput.TabIndex = 0;
            this.rdobtnInput.Text = "Single entry";
            this.rdobtnInput.UseVisualStyleBackColor = true;
            this.rdobtnInput.CheckedChanged += new System.EventHandler(this.rdobtnInput_CheckedChanged);
            // 
            // rdobtnFile
            // 
            this.rdobtnFile.AutoSize = true;
            this.rdobtnFile.Location = new System.Drawing.Point(8, 186);
            this.rdobtnFile.Name = "rdobtnFile";
            this.rdobtnFile.Size = new System.Drawing.Size(56, 17);
            this.rdobtnFile.TabIndex = 1;
            this.rdobtnFile.Text = "By File";
            this.rdobtnFile.UseVisualStyleBackColor = true;
            this.rdobtnFile.CheckedChanged += new System.EventHandler(this.rdobtnFile_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Mode";
            // 
            // tbMode
            // 
            this.tbMode.Location = new System.Drawing.Point(12, 35);
            this.tbMode.Name = "tbMode";
            this.tbMode.Size = new System.Drawing.Size(100, 20);
            this.tbMode.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Brand";
            // 
            // tbBrand
            // 
            this.tbBrand.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tbBrand.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.tbBrand.Location = new System.Drawing.Point(12, 85);
            this.tbBrand.Name = "tbBrand";
            this.tbBrand.Size = new System.Drawing.Size(129, 20);
            this.tbBrand.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(144, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Model";
            // 
            // tbModel
            // 
            this.tbModel.Location = new System.Drawing.Point(147, 85);
            this.tbModel.Name = "tbModel";
            this.tbModel.Size = new System.Drawing.Size(155, 20);
            this.tbModel.TabIndex = 7;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnBrowse);
            this.groupBox2.Controls.Add(this.tbInputFile);
            this.groupBox2.Location = new System.Drawing.Point(94, 167);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(315, 48);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Input File";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(246, 16);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(56, 23);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // tbInputFile
            // 
            this.tbInputFile.Location = new System.Drawing.Point(15, 19);
            this.tbInputFile.Name = "tbInputFile";
            this.tbInputFile.Size = new System.Drawing.Size(228, 20);
            this.tbInputFile.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbMode);
            this.groupBox1.Controls.Add(this.tbBrand);
            this.groupBox1.Controls.Add(this.tbModel);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(94, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(315, 138);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mode/Brand/Model";
            // 
            // exactRadio
            // 
            this.exactRadio.AutoSize = true;
            this.exactRadio.Location = new System.Drawing.Point(10, 67);
            this.exactRadio.Name = "exactRadio";
            this.exactRadio.Size = new System.Drawing.Size(91, 17);
            this.exactRadio.TabIndex = 14;
            this.exactRadio.Tag = "exactOnly";
            this.exactRadio.Text = "Substring only";
            this.exactRadio.UseVisualStyleBackColor = true;
            // 
            // fuzzyRadio
            // 
            this.fuzzyRadio.AutoSize = true;
            this.fuzzyRadio.Location = new System.Drawing.Point(10, 43);
            this.fuzzyRadio.Name = "fuzzyRadio";
            this.fuzzyRadio.Size = new System.Drawing.Size(74, 17);
            this.fuzzyRadio.TabIndex = 13;
            this.fuzzyRadio.Tag = "fuzzyOnly";
            this.fuzzyRadio.Text = "Fuzzy only";
            this.fuzzyRadio.UseVisualStyleBackColor = true;
            // 
            // comboRadio
            // 
            this.comboRadio.AutoSize = true;
            this.comboRadio.Checked = true;
            this.comboRadio.Location = new System.Drawing.Point(10, 19);
            this.comboRadio.Name = "comboRadio";
            this.comboRadio.Size = new System.Drawing.Size(120, 17);
            this.comboRadio.TabIndex = 12;
            this.comboRadio.TabStop = true;
            this.comboRadio.Tag = "combo";
            this.comboRadio.Text = "Default Combination";
            this.comboRadio.UseVisualStyleBackColor = true;
            // 
            // blCheckBox
            // 
            this.blCheckBox.AutoSize = true;
            this.blCheckBox.Checked = true;
            this.blCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.blCheckBox.Location = new System.Drawing.Point(21, 94);
            this.blCheckBox.Name = "blCheckBox";
            this.blCheckBox.Size = new System.Drawing.Size(76, 17);
            this.blCheckBox.TabIndex = 11;
            this.blCheckBox.Tag = "BL";
            this.blCheckBox.Text = "BL Branch";
            this.blCheckBox.UseVisualStyleBackColor = true;
            // 
            // euCheckBox
            // 
            this.euCheckBox.AutoSize = true;
            this.euCheckBox.Checked = true;
            this.euCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.euCheckBox.Location = new System.Drawing.Point(21, 70);
            this.euCheckBox.Name = "euCheckBox";
            this.euCheckBox.Size = new System.Drawing.Size(78, 17);
            this.euCheckBox.TabIndex = 10;
            this.euCheckBox.Tag = "EU";
            this.euCheckBox.Text = "EU Branch";
            this.euCheckBox.UseVisualStyleBackColor = true;
            // 
            // cyCheckBox
            // 
            this.cyCheckBox.AutoSize = true;
            this.cyCheckBox.Checked = true;
            this.cyCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cyCheckBox.Location = new System.Drawing.Point(21, 46);
            this.cyCheckBox.Name = "cyCheckBox";
            this.cyCheckBox.Size = new System.Drawing.Size(77, 17);
            this.cyCheckBox.TabIndex = 9;
            this.cyCheckBox.Tag = "CY";
            this.cyCheckBox.Text = "CY Branch";
            this.cyCheckBox.UseVisualStyleBackColor = true;
            // 
            // caCheckBox
            // 
            this.caCheckBox.AutoSize = true;
            this.caCheckBox.Checked = true;
            this.caCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.caCheckBox.Location = new System.Drawing.Point(21, 22);
            this.caCheckBox.Name = "caCheckBox";
            this.caCheckBox.Size = new System.Drawing.Size(77, 17);
            this.caCheckBox.TabIndex = 8;
            this.caCheckBox.Tag = "CA";
            this.caCheckBox.Text = "CA Branch";
            this.caCheckBox.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(182, 274);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 10;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(314, 274);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.caCheckBox);
            this.groupBox3.Controls.Add(this.cyCheckBox);
            this.groupBox3.Controls.Add(this.euCheckBox);
            this.groupBox3.Controls.Add(this.blCheckBox);
            this.groupBox3.Location = new System.Drawing.Point(29, 249);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(136, 120);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Branch Selection";
            this.groupBox3.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.exactRadio);
            this.groupBox4.Controls.Add(this.comboRadio);
            this.groupBox4.Controls.Add(this.fuzzyRadio);
            this.groupBox4.Location = new System.Drawing.Point(450, 221);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(136, 100);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Search Method";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox1);
            this.groupBox5.Controls.Add(this.rdobtnInput);
            this.groupBox5.Controls.Add(this.rdobtnFile);
            this.groupBox5.Controls.Add(this.groupBox2);
            this.groupBox5.Location = new System.Drawing.Point(12, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(420, 231);
            this.groupBox5.TabIndex = 14;
            this.groupBox5.TabStop = false;
            // 
            // treeDataSources
            // 
            this.treeDataSources.CheckBoxes = true;
            this.treeDataSources.Location = new System.Drawing.Point(6, 14);
            this.treeDataSources.Name = "treeDataSources";
            this.treeDataSources.Size = new System.Drawing.Size(242, 181);
            this.treeDataSources.TabIndex = 15;
            this.treeDataSources.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeDataSources_AfterCheck);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.treeDataSources);
            this.groupBox6.Location = new System.Drawing.Point(444, 17);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(254, 201);
            this.groupBox6.TabIndex = 13;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Branch Selection";
            // 
            // SearchOptionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(710, 324);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Name = "SearchOptionForm";
            this.Text = "DBSearch";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton rdobtnInput;
        private System.Windows.Forms.RadioButton rdobtnFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbMode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbBrand;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbModel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbInputFile;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.CheckBox blCheckBox;
        private System.Windows.Forms.CheckBox euCheckBox;
        private System.Windows.Forms.CheckBox cyCheckBox;
        private System.Windows.Forms.CheckBox caCheckBox;
        private System.Windows.Forms.RadioButton exactRadio;
        private System.Windows.Forms.RadioButton fuzzyRadio;
        private System.Windows.Forms.RadioButton comboRadio;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TreeView treeDataSources;
        private System.Windows.Forms.GroupBox groupBox6;
    }
}

