using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;

namespace DBSearch
{
    public enum SelType {ENTRY, FILE };

    public partial class SearchOptionForm : Form
    {
        public SelectType Choice = new SelectType();

        public SearchOptionForm()
        {
            InitializeComponent();

            // add auto complete items for tbBrand
            BrandCollection allBrands = DBFunctions.GetAllBrandList();
            foreach (Brand brand in allBrands)
            {
                if (brand.Name != null)
                    this.tbBrand.AutoCompleteCustomSource.Add(brand.Name);
            }
        }

        public string getChoiceResult()
        {
            return Choice.Result.ToString();
        }

        private void rdobtnInput_CheckedChanged(object sender, EventArgs e)
        {
            this.groupBox1.Enabled = true;
            this.groupBox2.Enabled = false;
            Choice.SelectionType = SelType.ENTRY;
        }

        private void rdobtnFile_CheckedChanged(object sender, EventArgs e)
        {
            this.groupBox1.Enabled = false;
            this.groupBox2.Enabled = true;
            Choice.SelectionType = SelType.FILE;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool result = false;

            Choice.Branch = MakeBranchString();
            Choice.ComboFlag = MakeComboString();
            switch (Choice.SelectionType)
            {
                case SelType.ENTRY:
                    Choice.Result = this.tbMode.Text.ToString() + "|" + this.tbBrand.Text.ToString() +
                        "|"+this.tbModel.Text.ToString();
                    result = true;
                    break;
                case SelType.FILE:
                    if (tbInputFile.Text.Length > 0)
                    {
                        Choice.Result = this.tbInputFile.Text;
                        result = true;
                    }
                    break;
            }

            if (!result)
            {
                MessageBox.Show("You haven't completed your selection.", "ERROR");
                return;
            }

            this.DialogResult = DialogResult.OK;
        }

        private string MakeBranchString()
        {
            List<string> branchList = new List<string>();

            if (caCheckBox.Checked)
                branchList.Add("CA");
            if (cyCheckBox.Checked)
                branchList.Add("CY");
            if (euCheckBox.Checked)
                branchList.Add("EU");
            if (blCheckBox.Checked)
                branchList.Add("BL");

            return String.Join("|", branchList.ToArray());
        }

        private string MakeComboString()
        {
            if (fuzzyRadio.Checked)
                return (string)fuzzyRadio.Tag;
            else if (exactRadio.Checked)
                return (string)exactRadio.Tag;
            return (string)comboRadio.Tag;                
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// display an open file dialog to select an Input file
        /// </summary>
        private void GetInputFile()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Excel file (*.xls)|*.xls";
            dlg.Filter += "|Text file (*.txt)|*.txt";
            dlg.Filter += "|All File Types (*.*)|*.*";
            dlg.DefaultExt = "xls";
            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                this.tbInputFile.Text = dlg.FileName;
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            rdobtnFile.Checked = true;
            GetInputFile();
        }

     }

    public class SelectType
    {
        public SelType SelectionType;
        public string Result;

        private string _branch;
        public string Branch
        {
            get { return _branch; }
            set { _branch = value; }
        }
        private string _comboSwitch;
        public string ComboFlag
        {
            get { return _comboSwitch; }
            set { _comboSwitch = value; }
        }
    };
}