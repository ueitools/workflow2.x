using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Checksums;

namespace SplitCap
{
    public class ZipHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SourceDir"></param>
        /// <param name="TargetName"></param>
        /// <param name="DestinationDir"></param>
        /// <param name="KeepOriginal"></param>
        public static void ZipProject(string SourceDir, string TargetName, string DestinationDir, bool KeepOriginal)
        {
            string WorkPath = DestinationDir;
            if (string.IsNullOrEmpty(WorkPath)) {
                WorkPath = Config.GetDirectory(DirTypes.Working);
            }

            if (!WorkPath.EndsWith("\\"))
                WorkPath += "\\";

            string TargetDir = WorkPath + TargetName;
            if (!TargetDir.ToLower().EndsWith(".zip"))
                TargetDir += ".zip";

            ZipOutputStream ZipStream = new ZipOutputStream(File.Create(TargetDir));

            ZipStream.SetLevel(6);          // 0=store only - 9=best compression
            string[] FileList = Directory.GetFiles(SourceDir);
            foreach (string item in FileList)
            {
                AddFileToZip(ZipStream, item);
            }

            ZipStream.Finish();
            ZipStream.Close();

            if (!KeepOriginal)
                Directory.Delete(SourceDir, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ZipStream"></param>
        /// <param name="FilePath"></param>
        private static void AddFileToZip(ZipOutputStream ZipStream, string FilePath)
        {
            Crc32 crc = new Crc32();

            string file = Path.GetFileName(FilePath);
            if (string.IsNullOrEmpty(file))
                return;

            FileStream fs = File.OpenRead(FilePath);
            byte[] buffer = new byte[fs.Length];
            fs.Read(buffer, 0, buffer.Length);
            ZipEntry entry = new ZipEntry(file);
            entry.DateTime = File.GetLastWriteTime(FilePath);
            entry.Size = fs.Length;         // set Size and the crc
            fs.Close();
            crc.Reset();
            crc.Update(buffer);
            entry.Crc = crc.Value;
            ZipStream.PutNextEntry(entry);
            ZipStream.Write(buffer, 0, buffer.Length);
        }

        public static string OpenZipFile(string WorkingDir)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = WorkingDir;
            dlg.Filter = "Zip files (*.zip)|*.zip|All files (*.*)|*.*";
            dlg.DefaultExt = ".zip";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string filePath = dlg.FileName; // "C:\working\capt32\test.zip"
                ZipHelper.UnZipProject(filePath);

                return filePath.Substring(0, filePath.LastIndexOf('.')) + "\\";
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zipArchive"></param>
        /// <returns></returns>
        public static string[] UnZipProject(string zipArchive)
        {
            string[] Files;

            string filePath = zipArchive.Substring(0, zipArchive.LastIndexOf('.'));
            filePath += "\\";

            if (Directory.Exists(filePath))
                Directory.Delete(filePath, true);

            Directory.CreateDirectory(filePath);

            ZipInputStream zipInputStream =
                new ZipInputStream(File.OpenRead(zipArchive));

            ZipEntry entry = null;
            string curFileName = null;
            FileStream streamWriter = null;

            while ((entry = zipInputStream.GetNextEntry()) != null)
            {
                curFileName = Path.GetFileName(entry.Name);
                streamWriter = File.Create(filePath + curFileName);
                StreamCopy(streamWriter, zipInputStream);
                streamWriter.Close();
                File.SetCreationTime(filePath + curFileName, entry.DateTime);
                File.SetLastWriteTime(filePath + curFileName, entry.DateTime);
            }

            Files = Directory.GetFiles(filePath);
            return Files;
        }


        /// <summary>
        /// buffered write
        /// </summary>
        /// <param name="zipInputStream"></param>
        /// <param name="streamWriter"></param>
        private static void StreamCopy(Stream destStream,
            Stream srcStream)
        {
            int size = 2048;
            byte[] data = new byte[2048];
            while (true)
            {
                size = srcStream.Read(data, 0, data.Length);
                if (size > 0)
                {
                    destStream.Write(data, 0, size);
                }
                else
                {
                    break;
                }
            }
        }
    }
}
