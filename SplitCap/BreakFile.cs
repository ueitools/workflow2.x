using System;
using System.IO;

namespace SplitCap
{
    public class BreakFile
    {
        private int READ_BLOCK_SIZE = 1024 * 16; // 16k block
        public void SetSplitTime (int timeinms, bool oneMhz)
        {
            // get blocksize from time
            if ( oneMhz )
            {
                this.READ_BLOCK_SIZE = timeinms * 1000 / 8;
            }
            else
            {
                this.READ_BLOCK_SIZE = timeinms * 1000 / 11;
            }
        }

        public string Breakitapart(string FilePath, string destinationPath)
        {
            if (!File.Exists(FilePath))
            {
                return null;
            }

            InitConfig(FilePath);

            string filename = "TN" + Path.GetFileNameWithoutExtension(FilePath);
            bool oneMHz = Path.GetExtension(FilePath) == ".U2";
            byte[] Capture = File.ReadAllBytes(FilePath);

            ChopItUp(Config.TargetPath, filename, Capture, oneMHz);
            ReadbackPrj prj = new ReadbackPrj();
            prj.CreatePrjFile(filename, filename, "");
            ZipHelper.ZipProject(Config.TargetPath, filename, destinationPath, false);
            return filename + ".zip";
        }

        private static void InitConfig(string FilePath)
        {
            Config.ProjList.Clear();
            string subdir = Path.GetFileNameWithoutExtension(Path.GetRandomFileName());
            Config.TargetPath = Path.GetDirectoryName(FilePath) + "\\" + subdir + "\\";
            Directory.CreateDirectory(Config.TargetPath);
        }

        public bool BreakItAndZipIt(string SourceFile)
        {
            Breakitapart(SourceFile, "");
            ZipHelper.ZipProject("SourceDir", SourceFile + ".zip", "", false);
            return true;
        }

        private bool ChopItUp(string path, string FileName, byte[] Capture, bool oneMHz)
        {
            bool InGap = false;
            int FileNum = 0;
            byte[] CurBlock = new byte[READ_BLOCK_SIZE];
            
            string extension = "U1";
            if (oneMHz) {
                extension = "U2";
            }
            string CurrentFile = string.Format("!{0}{1:X2}.{2}", FileName, FileNum, extension);

            FileStream of = File.Open(path + CurrentFile, FileMode.Create);

            for (int offset = 0; offset < Capture.Length; offset += READ_BLOCK_SIZE)
            {
                int len = (offset + READ_BLOCK_SIZE < Capture.Length) ? READ_BLOCK_SIZE : Capture.Length - offset;
                Buffer.BlockCopy(Capture, offset, CurBlock, 0, len);
                if (isnull(CurBlock))
                {
                    if (!InGap)
                    {
                        of.Write(CurBlock, 0, CurBlock.Length);
                        of.Close();
                        FileNum++;
                        CurrentFile = string.Format("!{0}{1:X2}.{2}", FileName, FileNum, extension);
                        of = File.Open(path + CurrentFile, FileMode.Create);
                        InGap = true;
                    }
                }
                else
                {
                    of.Write(CurBlock, 0, CurBlock.Length);
                    InGap = false;
                    AddToPrj(CurrentFile, FileNum);
                }
            }
            if (of.Length == 0)
            {
                of.Close();
                File.Delete(path + CurrentFile);
            }
            else
                of.Close();
            return true;
        }

        private void AddToPrj(string CurrentFile, int FileNum)
        {
            foreach (CaptureInfo c in Config.ProjList)
                if (c.FileName == CurrentFile)
                    return;
            CaptureInfo ci = new CaptureInfo();
            ci.FileName = CurrentFile;
            ci.KeyLabel = string.Format("Key-{0}", FileNum + 1);
            Config.ProjList.Add(ci);
        }

        private bool isnull(byte[] CBlock)
        {
            int value = 0;
            foreach (byte b in CBlock)
                value += b;
            return value == 0;
        }

//        private bool ChopItUp(string path, string FileName, byte[] Capture)
//        {
//            List<byte> runningBytes = new List<byte>();
//            int fileNum = 0;
//            bool inGap = true;
//            int gapStartIndex = -1;
//
//            for (int byteIndex = 0; byteIndex < Capture.Length; byteIndex++) {
//                byte currentByte = Capture[byteIndex];
//
//                if (currentByte == 0 && gapStartIndex == -1) {
//                    gapStartIndex = byteIndex;
//                    continue;
//                }
//
//                if (currentByte != 0) {
//                    if (gapStartIndex == -1) {}
//                    if (byteIndex - gapStartIndex > BREAK_BYTE_SIZE && runningBytes.Count > 0) {
//                        runningBytes.Clear();
//                    }
//                    continue;
//                }
//
//                inGap = currentByte == 0;
//                if (currentByte != 0) {
//                    runningBytes.Add(currentByte);
//                }
//            }
//
//            if (isnull(CurBlock)) {
//                if (!inGap) {
//                    string CurrentFile = string.Format("!{0}{1:X2}.U1", FileName, fileNum);
//                    FileStream of = File.Open(path + CurrentFile, FileMode.Create);
//                    of.Write(runningBytes.ToArray(), 0, runningBytes.Count);
//                    of.Close();
//
//                    fileNum++;
//                    inGap = true;
//                }
//            }
//
//            return true;
//        }
    }
}
