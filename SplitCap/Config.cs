using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Xml.Serialization;
using Microsoft.Win32;

namespace SplitCap
{
    enum DirTypes
    {
        Working,
        KeyFile
    }

     class Config
    {
        const string _UEITOOLSROOT = 
            "HKEY_CURRENT_USER\\Software\\UEITools\\capt32USB\\Settings\\";
        const string _LONG_CAP_TIMEOUT = "LongCapTimeout";
        const string _WORKINGDIR = "WorkDir";
        const string _KEYFILEDIR = "KeyFileDir";
        const string _KEYFILENAME = "KeyFileName";
        const string _DEFAULT_WORKINGDIR = "C:\\Working\\";
        const string _DEFAULT_KEYFILEDIR = "C:\\Working\\";
        const string _DEFAULT_KEYFILENAME = "capture_keys.txt";

        public static string WorkingDir = _DEFAULT_WORKINGDIR;
        public static string KeyFileDir = _DEFAULT_KEYFILEDIR;
        public static string TargetPath = _DEFAULT_WORKINGDIR;
        public static string KeyFileName = _DEFAULT_KEYFILENAME;

        public static int CapTimeout = 4;   // default value

        public static bool ChangesMade = false;
        public static IList<CaptureInfo> ProjList = new List<CaptureInfo>();

        public static void LoadDefaultConfig()
        {
            WorkingDir = GetDirectory(DirTypes.Working);
            KeyFileDir = GetDirectory(DirTypes.KeyFile);

            CapTimeout = int.Parse(GetRegistry(_LONG_CAP_TIMEOUT, 
                                        CapTimeout.ToString()));
            KeyFileDir = GetRegistry(_KEYFILEDIR, _DEFAULT_KEYFILEDIR);
            KeyFileName = GetRegistry(_KEYFILENAME, _DEFAULT_KEYFILENAME);
        }

        public static bool LocateKeyFile()
        {
            string msg = "Please locate the Key Label File.";
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = msg;
            dlg.InitialDirectory = Config.KeyFileDir;
            dlg.Filter = "List files (*.lst)|*.lst|Txt files (*.txt)|*.txt|All files (*.*)|*.*";
            dlg.DefaultExt = ".lst";
            DialogResult DlgResult = dlg.ShowDialog();
            bool result = false;

            switch(DlgResult)
            {
                case DialogResult.OK:
                    {
                        KeyFileDir = Path.GetDirectoryName(dlg.FileName) + "\\";
                        KeyFileName = Path.GetFileName(dlg.FileName);
                        SetRegistry(_KEYFILEDIR, KeyFileDir);
                        SetRegistry(_KEYFILENAME, KeyFileName);
                        result = true;
                        break;
                    }
                case DialogResult.Cancel:
                        break;
                default:
                    MessageBox.Show("Unable to locate the Key Label File", "Error");
                    break;
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directory"></param>
        public static void SetupDirectory(DirTypes type)
        {
            string DirName = type.ToString();
            string RegistryName = GetRegistryName(type);
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.Description = string.Format("Choose your {0} directory", DirName);
            dlg.SelectedPath = GetDirectory(type);

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                if (dlg.SelectedPath.EndsWith("\\") == false)
                    dlg.SelectedPath += "\\";

                SetRegistry(RegistryName, dlg.SelectedPath);
                switch (type)
                {
                    case DirTypes.Working: WorkingDir = dlg.SelectedPath; break;
                    case DirTypes.KeyFile: KeyFileDir = dlg.SelectedPath; break;
                }
            }
        }

        public static string GetDirectory(DirTypes type)
        {
            string Directory = GetRegistryName(type);
            return GetRegistry(Directory, _DEFAULT_WORKINGDIR);
        }

        private static string GetRegistryName(DirTypes type)
        {
            string Directory;
            switch (type)
            {
                case DirTypes.Working: Directory = _WORKINGDIR; break;
                case DirTypes.KeyFile: Directory = _KEYFILEDIR; break;
                default: Directory = ""; break;
            }
            return Directory;
        }

        private static void SetRegistry(string RegistryName, string value)
        {
            Registry.SetValue(_UEITOOLSROOT, RegistryName,
                value, RegistryValueKind.String);
        }

        /// <summary>
        /// Read a previously set value from the registry
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static string GetRegistry(string RegistryName, string DefaultValue)
        {
            string result = (string)Registry.GetValue(_UEITOOLSROOT, RegistryName, DefaultValue);
            if (string.IsNullOrEmpty(result))
                result = DefaultValue;
            return result;
        }
    }
}
