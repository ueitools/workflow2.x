using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Windows.Forms;

namespace SplitCap
{
    class ReadbackPrj
    {
        string PrjFile = "";
        string FullPath = "";
        //private const string _NOFILE = "-----------";

        public bool CreatePrjFile(string PrjFileName, string TN, string ID)
        {
            FullPath = Config.TargetPath;
            string OutFileName = PrjFileName;

            if (PrjFileName.StartsWith("TN"))
                OutFileName = PrjFileName.Substring(2);

            /// If file was renamed we need to delete the old prj file.
            string[] files = Directory.GetFiles(FullPath);
            foreach (string item in files)
                if (item.EndsWith(PrjFileName + ".prj"))
                    File.Delete(item);

            PrjFile = "L" + OutFileName + ".prj";
            IList<string> prjHeader = CreatePrjHeader(TN, ID);
            foreach (string text in prjHeader)
                File.AppendAllText(FullPath + PrjFile, text);

            return AddCaptureList(OutFileName);
        }

        private bool AddCaptureList(string Prefix)
        {
            string line;
            for (int index = 0; index < Config.ProjList.Count; index++)
            {
                string sourceFilename = Config.ProjList[index].FileName;
                string extension = Path.GetExtension(sourceFilename);

                string destinationFilename = string.Format("!{0}{1:X2}{2}", Prefix, index, extension);
                if (string.IsNullOrEmpty(sourceFilename))
                {
                    continue;
                    //line = string.Format("{0}: {1}\r\n", _NOFILE,
                    //                    Config.ProjList[index].KeyLabel);
                }
                else
                {
                    line = string.Format("{0}: {1}\r\n", destinationFilename,
                                        Config.ProjList[index].KeyLabel);
                    try
                    {
                        File.Move(FullPath + sourceFilename,
                                    FullPath + destinationFilename);
                        Config.ProjList[index].FileName = destinationFilename;
                    }
                    catch
                    {
                        MessageBox.Show("Failed Updating file name", "Error");
                        return false;
                    }
                }

                AddLine(line);
            }
            return true;
        }

        private void AddLine(string text)
        {
            File.AppendAllText(FullPath + PrjFile, text);
        }

        public IList<string> CreatePrjHeader(string TN, string ID)
        {
            IList<string> report = new List<string>();

            report.Add("REMOTE CONTROL CAPTURE ----------------------------------------\r\n\r\n");

            report.Add("Copyright 2008 Universal Electronics, Inc.  All Rights Reserved\r\n\r\n");

            report.Add("Tracking Number: " + TN + "\r\n");
            report.Add("Primary Number : 0\r\n\r\n");

            report.Add("Id             : " + ID + "\r\n\r\n");

            report.Add("CUSTOMER-------------------------------------------------------\r\n\r\n");

            report.Add("Customer Name  : READBACK -UEI\r\n\r\n");

            report.Add("CAPTURE LIST---------------------------------------------------\r\n\r\n");

            return report;
        }
    }
}
