using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SplitCap
{
    public partial class SplitCapView : Form
    {
        public SplitCapView()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            tbInputFile.Text = Common.BrowseForFile("U1", "");
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            BreakFile breakit = new BreakFile();
            string Ofn = breakit.Breakitapart(tbInputFile.Text, "");
            if (string.IsNullOrEmpty(Ofn))
                MessageBox.Show("Input file was not found", "Error");
            else
                MessageBox.Show("File " + Ofn + " Created", "Complete");
        }
    }
}