using System;
using System.Windows.Forms;
using System.IO;

namespace SplitCap {
    internal static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args) {
            if (args.Length == 1) {
                string FileName = args[0];
                if (File.Exists(FileName)) {
                    BreakFile bk = new BreakFile();
                    bk.Breakitapart(FileName, "");
                }
            } else if (args.Length > 1) {
                string inFileName = args[0];
                string destinationPath = args[1];
                if (File.Exists(inFileName)) {
                    BreakFile bk = new BreakFile();
                    bk.Breakitapart(inFileName, destinationPath);
                }
            } else {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new SplitCapView());
            }
        }
    }
}