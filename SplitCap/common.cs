using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Text;
using System.Windows.Forms;

namespace SplitCap
{
    public class CaptureInfo
    {
        public string KeyLabel;
        public string FileName;
    }

    class KeyLists
    {
        public IList<string> Header = new List<string>();
        public IList<List<string>> Labels = new List<List<string>>();
    }

    class Common
    {
        public static string ProjectName = "SplitCap";
        public static string ProjectVersion = "v1.00";
        public static string CopyrightMsg = 
            "Copyright � 2008 Universal Electronics Inc. \r\nAll rights reserved.";

        public static string WorkingDirectory = "C:\\working";
        public static string[] AllKeyLables;
        public static string TestTnNumber = "";
        public static string ImportIDName = "";


        public static KeyLists Keys = new KeyLists();

        public static bool LoadModeLables()
        {
            if (!File.Exists(Config.KeyFileDir + Config.KeyFileName))
                if (!Config.LocateKeyFile())
                    return false;

            AllKeyLables = File.ReadAllLines(Config.KeyFileDir +
                                             Config.KeyFileName);

            int listnum = -1;
            Keys.Header.Clear();
            Keys.Labels.Clear();
            foreach (string text in AllKeyLables)
            {
                if (string.IsNullOrEmpty(text))
                    continue;
                    
                if (!text.StartsWith("  "))
                {
                    Keys.Header.Add(text);
                    listnum++;
                    Keys.Labels.Add(new List<string>());
                }
                else 
                {
                    Keys.Labels[listnum].Add(text.Trim());
                }
            }
            return true;
        }

        public static string BrowseForFile(string fileType, string targetDir)
        {
            string msg = "Please locate the File.";
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = msg;
            //dlg.InitialDirectory = Config.KeyFileDir;
            dlg.Filter = string.Format("{0} files (*.{0})|*.{0}|All files (*.*)|*.*", fileType);
            dlg.DefaultExt = "." + fileType;
            DialogResult DlgResult = dlg.ShowDialog();

            if (!string.IsNullOrEmpty(targetDir) && DlgResult == DialogResult.OK)
                File.Copy(dlg.FileName, targetDir + Path.GetFileName(dlg.FileName));

            return dlg.FileName;
        }

        public static bool Confirmed(string text)
        {
            return MessageBox.Show(text, 
                "Confirm?", MessageBoxButtons.YesNo) == DialogResult.Yes;
        }

        public static string MakeTargetDirectory(string WorkDir)
        {
            string TargetDir = Path.Combine(WorkDir, Path.GetRandomFileName());
            TargetDir = TargetDir.Substring(0, TargetDir.Length - 4);

            while (Directory.Exists(TargetDir))
            {
                TargetDir = Path.Combine(WorkDir, Path.GetRandomFileName());
                TargetDir = TargetDir.Substring(0, TargetDir.Length - 4);
            }

            TargetDir += '\\';
            Directory.CreateDirectory(TargetDir);

            return TargetDir;
        }

        public static bool VerifyDeleteTargetDir()
        {
            if (Config.ChangesMade)
            {
                string Msg = "Are you sure you want to discard your changes?";
                if (!Confirmed(Msg))
                {
                    return false;
                }
            }

            if (!string.IsNullOrEmpty(Config.TargetPath) &&
                    Directory.Exists(Config.TargetPath))
                Directory.Delete(Config.TargetPath, true);
            return true;
        }

        public static bool SaveProject(bool TestCapture)
        {
            string FileName = "";
            ReadbackPrj PrjFile = new ReadbackPrj();
            FileName = "TestCapture";
            if (!string.IsNullOrEmpty(TestTnNumber))
                FileName = TestTnNumber;
            if (!string.IsNullOrEmpty(ImportIDName))
                FileName = ImportIDName;
            PrjFile.CreatePrjFile(FileName, TestTnNumber, ImportIDName);
            if (!string.IsNullOrEmpty(ImportIDName))
                FileName = "RB!" + FileName;

            ZipHelper.ZipProject(Config.TargetPath, FileName, "", true);
            return true;
        }

        public static void ClearTnId()
        {
            TestTnNumber = "";
            ImportIDName = "";
        }
    }
}
