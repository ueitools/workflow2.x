using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using UEIException;

namespace DBEditor
{
    static class DBEditorWrapper
    {
        /// <summary>
        /// PartialID
        /// </summary>
        public static void EditPartialID()
        {
            SelectionForm dlg = new SelectionForm(ListType.IDLIST);
            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                EditPartialID(dlg.Selected, dlg.ConnStr);
            }
        }

        public static void EditPartialID(string id)
        {
            EditPartialID(id, DBConnectionString.UEITEMP);
        }

        public static void EditPartialID(string id, string connStr)
        {
            if (connStr == DBConnectionString.UEITEMP)
            {
                try {
                    bool error = false;
                    bool enableEdit = true;
                    string statusMessage = string.Empty;
                    try {
                        if (PartialID.Lock(id) == 1) {
                            statusMessage = "ID is checked out by another user.";
                        }
                    } catch (TNLockException ex) {
                        enableEdit = false;
                        error = true;
                        statusMessage = "ID is only be available for viewing.";
                        MessageBox.Show(string.Format("Error locking {0}. ID will only be available for viewing.\n\n{1} ", id,
                                                      ex.Message));
                    }

                    PartialIDForm dlg = new PartialIDForm(id);
                    dlg.SetStatus(statusMessage, error);
                    if (enableEdit) {
                        dlg.EnableEdit();
                        dlg.FormClosed += delegate { PartialID.UnLock(id); };
                    }
                    dlg.ShowDialog();  
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                PartialIDForm dlg = new PartialIDForm(id);
                dlg.ShowDialog();
            }
        }

        /// <summary>
        /// TN
        /// </summary>
        public static void EditTN()
        {
            SelectionForm dlg = new SelectionForm(ListType.TNLIST);
            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                EditTN(Int32.Parse(dlg.Selected), dlg.ConnStr);
            }
        }

        public static void EditTN(int tn)
        {
            EditTN(tn, DBConnectionString.UEITEMP);
        }

        public static void EditTN(int tn, string connStr)
        {
            if (connStr == DBConnectionString.UEITEMP)
            {
                try {
                    bool enableSave = true;
                    string statusMessage = string.Empty;
                    bool error = false;
                    try {
                        if (TN.Lock(tn) == 1) {
                            statusMessage = "TN is checked out by another user.";
                        }
                    } catch (TNLockException ex) {
                        enableSave = false;
                        error = true;
                        statusMessage = "TN is only be available for viewing.";
                        MessageBox.Show(string.Format("Error locking TN{0}. TN will only be available for viewing.\n\n{1} ", tn,
                                                      ex.Message));
                    }

                    TNForm dlg = new TNForm(tn);
                    dlg.SetStatus(statusMessage, error);
                    if (enableSave) {
                        dlg.EnableSave();
                        dlg.FormClosed += delegate { TN.UnLock(tn); };
                    }
                    dlg.ShowDialog();
                } catch (Exception ex) {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                TNForm dlg = new TNForm(tn);
                dlg.ShowDialog();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void EditData()
        {
            IDLevelEditForm dlg = new IDLevelEditForm(
                IDLevelEditType.DATA);
            dlg.ShowDialog();
        }

        public static void EditIntron()
        {
            IDLevelEditForm dlg = new IDLevelEditForm(
                IDLevelEditType.INTRON);
            dlg.ShowDialog();
        }

        public static void EditLabel()
        {
            IDLevelEditForm dlg = new IDLevelEditForm(
                IDLevelEditType.LABEL);
            dlg.ShowDialog();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void UnLockPartialID()
        {
            ReleaseLockForm dlg = new ReleaseLockForm(
                ReleaseLockMode.ID);
            dlg.ShowDialog();
        }

        public static void UnLockTN()
        {
            ReleaseLockForm dlg = new ReleaseLockForm(
                ReleaseLockMode.TN);
            dlg.ShowDialog();
        }

        public static void RenameID()
        {
            IDRenameForm dlg = new IDRenameForm();

            dlg.ShowDialog();
        }

        public static void AddNewRemote() {
            LockStatusFunctions lockStatusFunctions = new LockStatusFunctions();

            try {
                lockStatusFunctions.LockStatusCounters(Environment.UserDomainName, Environment.UserName);
            } catch (Exception ex) {
                MessageBox.Show(string.Format("Another user is logging in a new TN, please try again later.\n\n{0}", ex.Message));
                return;
            }

            string message = string.Empty;
            string messageTitle = string.Empty;
            MessageBoxIcon messageBoxIcon = MessageBoxIcon.None;
            try {
                LogTNForm dlg = new LogTNForm();
                dlg.ShowDialog();
            } catch (TimeoutException ex) {
                message = string.Format("{0}", ex.Message);
                messageTitle = "Timeout";
                messageBoxIcon = MessageBoxIcon.Information;
            } catch (Exception ex) {
                message = string.Format("{0}", ex.Message);
                messageTitle = "Error logging new remote";
                messageBoxIcon = MessageBoxIcon.Error;
            } finally {
                lockStatusFunctions.UnlockStatusCounters(Environment.UserDomainName, Environment.UserName);                
            }

            if (message != string.Empty) {
                MessageBox.Show(message, messageTitle, MessageBoxButtons.OK, messageBoxIcon);                
            }
        }
    }
}
