using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using BusinessObject;
using System.Drawing;
using IRUSBLib;
using UsbNi;

namespace DBEditor
{
    public class CaptureFunctions
    {
        public enum CaptureMode {
            NonUSB,
            USB,
            USBee
        }

//        private static int NORMAL_CAPTURE_BLOCK_COUNT = 2;
        private static int LONG_CAPTURE_BLOCK_COUNT = 32;
        static USBRamni Ramni = new USBRamni();
        private static CaptureMode __CaptureMode = CaptureMode.NonUSB;

        public static CaptureMode USBCaptureMode {
            get { return __CaptureMode; }
            set { __CaptureMode = value; }
        }

        public static IList<string> ImportTnLabels()
        {
            string CurrentDBConnStr = DAOFactory.GetDBConnectionString();
            IList<string> result = new List<string>();

            try
            {
                SelectionForm dlg = new SelectionForm(ListType.TNLIST);                
                DialogResult res = dlg.ShowDialog();
                if (res == DialogResult.OK)
                {
                    DAOFactory.ResetDBConnection(dlg.ConnStr);
                    int tnNum = int.Parse(dlg.Selected);
                    TN tnSource = new TN();
                    tnSource.Load(tnNum);
                    foreach (Function item in tnSource.FunctionList)
                        if (!string.IsNullOrEmpty(item.Label))
                            result.Add(item.Label);
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            finally
            {
                DAOFactory.ResetDBConnection(CurrentDBConnStr);
            }
            return result;
        }

        public static void ViewCaptureFileForSelectedGridCell(DataGridView Grid)
        {
            string filename;
            int tn = -1;
            string workingDirectory = Common.GetWorkPath();
            foreach (DataGridViewCell cell in Grid.SelectedCells)
            {
                if (cell.ColumnIndex == 6)// it's a File cell
                {
                    // open the file by cas32
                    try
                    {
                        filename = cell.Value.ToString();
                        string cas32_path = Common.GetConfig("CASAPP");
                        object objTN = Grid.Rows[cell.RowIndex].Cells["TN"].Value;
                        if (objTN != null)
                        {
                            tn = Int32.Parse(objTN.ToString());
                        }
                        string file_path = workingDirectory + 
                                            TNHelper.TNUnzipDir(tn) + 
                                            "\\" + filename;
                        System.Diagnostics.Process.Start(cas32_path, file_path);
                    }
                    catch (Exception ex)
                    {
                        string error = ex.Message;
                    }
                }
            }
        }

        /// <summary>
        /// Capture or re-capture one or more U1 files and update the TN Zip file
        /// </summary>
        /// <param name="Grid"></param>
        public static void CaptureNewU1FileForSelectedRows(DataGridView Grid)
        {
            int tn = FindTnNumber(Grid);
            if (tn == 0)
                return;

            string filename;
            string workPath = Common.GetWorkPath();
            string zipdir = workPath + TNHelper.TNUnzipDir(tn);
            bool captureAll = HighlightedOrAll(Grid);
            bool renumber = false;
            Hashtable Entry;

            foreach (DataGridViewRow row in Grid.Rows)
            {
                if (row.Index >= Grid.Rows.Count - 1)
                    continue;
                if (row.Selected == true || captureAll)
                {
                    if (row.Cells["TN"].Value == null)
                    {
                        MessageBox.Show(string.Format("Row {0} is not valid", row.Index));
                        continue;
                    }
                    row.Cells["FileName"].Style.Font = new Font(DataGridView.DefaultFont, FontStyle.Bold);
                    Entry = CaptureRow(tn, row.Index, row);
                    filename = Entry["FileName"].ToString();
                    if (!File.Exists(zipdir + filename))
                    {
                        MessageBox.Show("Capture failed\r\nthe capture file was not created");
                        row.Cells["FileName"].Style.Font = new Font(DataGridView.DefaultFont, FontStyle.Regular);
                        break;
                    }
                    FileInfo test = new FileInfo(zipdir + filename);
                    if (test.Length == 0)
                    {
                        row.Cells["FileName"].Style.Font = new Font(DataGridView.DefaultFont, FontStyle.Regular);
                        ZipHelper.ExtractFile(workPath + TNHelper.TNZipName(tn), filename, zipdir);
                        break;
                    }
                    if (row.Cells["FileName"].Value == null || filename.StartsWith("#"))
                        renumber = true;
                    row.Cells["FileName"].Value = filename;
                    EditFunctions.LogOperation(tn, "Captured - " + filename + "   : ", row);
                    //TNHelper.Log(tn, "Captured - " + filename + "   : " +
                    //                            row.Cells["Label"].Value.ToString());
                }
            }
            HashtableCollection FileEntries = GetGridFileList(Grid);
            if (renumber)
                RenumberTnCaptures(tn, FileEntries, Grid);

            FileEntries = GetGridFileList(Grid);
            LogDifferenceList(tn,
                PrjFileHelper.UpdatePrjAndReportDiff(tn, FileEntries));
            TNHelper.UpdateTNZip(tn, CaptureFileList(Grid));
        }

        public static void UpdateChangesToTnFiles(DataGridView Grid)
        {
            int tn = FindTnNumber(Grid);
            if (tn == 0)
                return;
            HashtableCollection FileEntries = GetGridFileList(Grid);
            RenumberTnCaptures(tn, FileEntries, Grid);

            LogDifferenceList(tn, 
                PrjFileHelper.UpdatePrjAndReportDiff(tn, GetGridFileList(Grid)));

            TNHelper.UpdateTNZip(tn, CaptureFileList(Grid));
        }

        private static void LogDifferenceList(int tn, StringCollection diff)
        {
            foreach (string line in diff)
            {
                TNHelper.Log(tn, line);
                //MessageBox.Show(line);
            }
        }

        private static Hashtable CaptureRow(int tn, int FileCount, DataGridViewRow row)
        {
            string filename;
            string label = GetRowLabel(row);
            string zipdir = Common.GetWorkPath() + TNHelper.TNUnzipDir(tn);
            Hashtable Entry = new Hashtable();

            filename = GetOrAssignCaptureName(row.Cells["Filename"].Value, tn, FileCount, __CaptureMode);

            Entry.Add("FileName", filename);
            Entry.Add("Label", label);

            if (label.IndexOf('/') >= 0)
                label = label.Replace('/', '|');

            if (__CaptureMode == CaptureMode.USB)
            {
                USBCapture(zipdir + filename, label);
            }
            else if (__CaptureMode == CaptureMode.NonUSB)
            {
                string parameters = string.Format("/f={0} /l={1} /b={2}",
                                        zipdir + filename, label,
                                        LONG_CAPTURE_BLOCK_COUNT);
                System.Diagnostics.Process CaptureProcess;
                CaptureProcess = System.Diagnostics.Process.Start("ramni06.exe", parameters);
                CaptureProcess.WaitForExit();
                CaptureProcess.Close();
            }
            else if (__CaptureMode == CaptureMode.USBee) {
                UsbeeTask usbeeTask = new UsbeeTask();
                usbeeTask.DoCapture(zipdir + filename, label);
            }

            return Entry;
        }

        private static bool USBCapture(string FilePath, string DialogTitle)
        {
            DialogProperties dp = new DialogProperties();
            TaskProperties tp = new TaskProperties();            
            Point Window = Form.ActiveForm.Location;

            dp.Title = DialogTitle;
            dp.CenterDialog = true;
            tp.CapBlocks = 20;
            tp.timeout = 20000;
            tp.MaxZeroBlocks = 4; // Config.CapTimeout;
            tp.FileName = FilePath;
            tp.saveGapFile = false;
            return Ramni.DoCapture(dp, tp);
        }

        private static void RenumberTnCaptures(int tn,
                                    HashtableCollection FileEntries,
                                    DataGridView Grid)
        {
            string filename;
            Hashtable RenameTable = TNHelper.RenumberCaptureFiles(tn, FileEntries);
            foreach (DataGridViewRow row in Grid.Rows)
            {
                if (row.Index >= Grid.Rows.Count - 1)
                    continue;
                if (row.Cells["FileName"].Value == null)
                    continue;
                filename = row.Cells["FileName"].Value.ToString();
                row.Cells["FileName"].Value = RenameTable[filename];
            }
        }

        /// <summary>
        /// Collect all the capture file names and labels from the grid
        /// </summary>
        /// <param name="Grid"></param>
        /// <returns>A collection of Hashtable items containing "FileName" and "Label"</returns>
        public static HashtableCollection GetGridFileList(DataGridView Grid)
        {
            HashtableCollection FileEntries = new HashtableCollection();
            foreach (DataGridViewRow row in Grid.Rows)
            {
                if (row.Cells["FileName"].Value == null)
                    continue;
                Hashtable Entry = new Hashtable();
                Entry.Add("FileName", row.Cells["FileName"].Value.ToString());
                Entry.Add("Label", GetRowLabel(row));
                FileEntries.Add(Entry);
            }
            return FileEntries;
        }

        /// <summary>
        /// If nothing is highlighted ask the user if they want to capture ALL
        /// </summary>
        /// <param name="Grid"></param>
        /// <returns></returns>
        private static bool HighlightedOrAll(DataGridView Grid)
        {
            bool captureAll = false;
            if (Grid.SelectedRows.Count == 0)
            {
                string msg = "No rows are selected, Do you want to capture ALL functions?";
                DialogResult res = MessageBox.Show(msg, "Error", MessageBoxButtons.YesNo);
                if (res == DialogResult.Yes)
                    captureAll = true;
            }
            return captureAll;
        }

        /// <summary>
        /// Search for the TN number in the grid
        /// </summary>
        /// <param name="Grid"></param>
        /// <returns></returns>
        private static int FindTnNumber(DataGridView Grid)
        {
            if (Grid.RowCount == 0)
                return 0;
            string tn = "0";
            foreach (DataGridViewRow item in Grid.Rows)
                if (item.Cells["TN"].Value != null &&
                    item.Index < Grid.Rows.Count - 1)
                    tn = item.Cells["TN"].Value.ToString();
            return int.Parse(tn);
        }

        /// <summary>
        /// Extract the "LegacyLabel" or if empty, "Label" from the given row
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private static string GetRowLabel(DataGridViewRow row)
        {
            string label = "";

            if (row.Cells["Label"].Value != null)
                label = row.Cells["Label"].Value.ToString();

            if (!string.IsNullOrEmpty(label))
                return label;

            if (row.Cells["LegacyLabel"].Value != null)
                label = row.Cells["LegacyLabel"].Value.ToString();
            return label;
        }

        private static string GetOrAssignCaptureName(object filecol,
                                                     int tn, int EntryNum, CaptureMode captureMode)
        {
            string filename;
            string extension;

            if (captureMode == CaptureMode.USBee) {
                extension = "U2";
            } else {
                extension = "U1";
            }

            if (string.IsNullOrEmpty((string)filecol))
            {
                filename = "#" + PrjFileHelper.GetNewCapName(tn, EntryNum, extension);
            }
            else {
                string columnValue = filecol.ToString();
                filename = Path.GetFileNameWithoutExtension(columnValue) + "." + extension;
            }
            return filename;
        }

        public static StringCollection CaptureFileList(DataGridView Grid)
        {
            StringCollection FileEntries = new StringCollection();
            foreach (DataGridViewRow row in Grid.Rows)
            {
                if (row.Cells["FileName"].Value != null)
                    FileEntries.Add(row.Cells["FileName"].Value.ToString());
            }
            return FileEntries;
        }

        private class UsbeeTask {
            //Method to aquire data from USBee capture pod
            public bool DoCapture(string FilePath, string KeyLabel) {
                object pData = new object();
                int pStatus = 0;
                string prompt = KeyLabel;

                try {
                    IrUSBCaptureClass capture = new IrUSBCaptureClass();
                    capture.AquireKey(prompt, ref pData, ref pStatus);

                    bool capturestatus = false;
                    if (pStatus == 0) {
                        capturestatus = true;
                        byte[] data = (byte[])pData;
                        StoreBinaryData(data, FilePath);
                    } else {
                        capturestatus = false;
                    }
                    return capturestatus;
                } catch (Exception) {
                    MessageBox.Show("Error in dll or Dll not registered");
                    return false;
                }
            }

            //Method that stores the aquired binary data into a binary file
            private void StoreBinaryData(byte[] capturedata, string filePath) {
                using (FileStream binFile = File.Open(filePath, FileMode.Create, FileAccess.Write)) {
                    for (int len = 0; len < capturedata.Length; len++) {
                        binFile.WriteByte(capturedata[len]);
                    }

                    binFile.Close();
                }
            }
        }
    }
}
