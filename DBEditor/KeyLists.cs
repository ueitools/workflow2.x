using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace DBEditor
{
    public class KeyLists
    {
        public IList<string> Header = new List<string>();
        public IList<List<string>> Labels = new List<List<string>>();

        public KeyLists(string FileName)
        {
            string[] labels = File.ReadAllLines(FileName);
            int listnum = -1;
            Header.Clear();
            Labels.Clear();
            foreach (string item in labels)
            {
                if (string.IsNullOrEmpty(item))
                    continue;
                if (item.StartsWith(" "))
                    Labels[listnum].Add(item.Trim());
                else
                {
                    Header.Add(item.Trim());
                    listnum++;
                    Labels.Add(new List<string>());
                }
            }
        }

        public void FillGroupSelectionComboBox(ComboBox cb)
        {
            foreach (string item in Header)
                cb.Items.Add(item);
            cb.SelectedIndex = 0;
        }

        public void FillLabelSelectionListBox(ListBox lb, int list)
        {
            lb.Items.Clear();
            foreach (string item in Labels[list])
                lb.Items.Add(item);
        }
    }
}
