using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using BusinessObject;

namespace DBEditor
{
    public enum IDLevelEditType { DATA, LABEL, INTRON }; 

    public class IDLevelEdit
    {
        #region Private Fields

        private string _id;
        private IDLevelEditType _type;

        private string _currentData;
        private string _newData;

        private string _currentLabel;
        private string _newLabel;

        private string _currentIntron;
        private string _newIntron;
        private string _newPriority;

        #endregion

        #region Business Properties and Methods

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public IDLevelEditType Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public string CurrentData
        {
            get { return _currentData; }
            set { _currentData = value; }
        }

        public string NewData
        {
            get { return _newData; }
            set { _newData = value; }
        }

        public string CurrentLabel
        {
            get { return _currentLabel; }
            set { _currentLabel = value; }
        }

        public string NewLabel
        {
            get { return _newLabel; }
            set { _newLabel = value; }
        }

        public string CurrentIntron
        {
            get { return _currentIntron; }
            set { _currentIntron = value; }
        }

        public string NewIntron
        {
            get { return _newIntron; }
            set { _newIntron = value; }
        }

        public string NewPriority
        {
            get { return _newPriority; }
            set { _newPriority = value; }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public void Preview()
        {
            FunctionCollection functionList = GetAffectedFunction();
            IDLevelEditPreviewForm form = new IDLevelEditPreviewForm();
            form.Bind(functionList);

            string summary = this.GetSummary();
            form.SetSummary(summary);
            form.Show();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Commit()
        {
            switch (this._type)
            {
                case IDLevelEditType.LABEL:
                    DAOFactory.Function().UpdateIDLabel(this._id,
                        this._currentData, this._currentLabel,
                        this._newLabel);
                    break;

                case IDLevelEditType.INTRON:
                    DAOFactory.Function().UpdateIDIntron(this._id,
                        this._currentLabel, this._currentIntron,
                        this._newIntron, this._newPriority);
                    break;

                case IDLevelEditType.DATA:
                default:
                    DAOFactory.Function().UpdateIDData(this._id,
                        this._currentData, this._newData);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private FunctionCollection GetAffectedFunction()
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ID", _id);

            switch (this._type)
            {
                case IDLevelEditType.LABEL:
                    paramList.Add("Data", _currentData);
                    paramList.Add("Label", _currentLabel);
                    break;

                case IDLevelEditType.INTRON:
                    paramList.Add("Label", _currentLabel);
                    paramList.Add("Intron", _currentIntron);
                    break;

                case IDLevelEditType.DATA:
                default:
                    paramList.Add("Data", _currentData);
                    break;
            }

            FunctionCollection functionList = 
                DAOFactory.Function().SelectID(paramList);

            return functionList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetSummary()
        {
            string summary = "";
            switch (this._type)
            {
                case IDLevelEditType.LABEL:
                    summary = String.Format(
                        "All Labels \"{0}\" will be updated into \"{1}\" " +
                        "for ID \"{2}\" with Data \"{3}\"", this._currentLabel, 
                        this._newLabel, this._id, this._currentData);
                    break;

                case IDLevelEditType.INTRON:
                    summary = String.Format(
                        "All Introns \"{0}\" will be updated into \"{1} {2}\" " +
                        " for ID \"{3}\" with Label \"{4}\"",
                        this._currentIntron, this._newIntron, this._newPriority,
                        this._id, this._currentLabel);
                    break;

                default: // IDLevelEditType.DATA
                    summary = String.Format(
                        "All Data \"{0}\" will be updated into \"{1}\" " +
                        "for ID \"{2}\" ",
                        this._currentData, this._newData, this._id);
                    break;
            }

            return summary;
        }
    }
}
