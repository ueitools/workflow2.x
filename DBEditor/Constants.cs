using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.Win32;
using System.Text;
using CommonForms;

namespace DBEditor
{
    public static class Reg
    {
        public const string PROJ_ROOT = 
            "HKEY_CURRENT_USER\\Software\\UEITools\\Workflow";
        public const string DEFAULTDB = "Default_Database";
        public const string WORKING_DIR = "Working_Directory";
    }
    public class Common
    {
        /// <summary>
        /// Common call since this is referring to an  
        /// obsolite function and we may want to update it
        /// </summary>
        /// <param name="Setting"></param>
        /// <returns></returns>
        public static string GetConfig(string Setting)
        {
            return ConfigurationSettings.AppSettings[Setting];
        }

        /// <summary>
        /// Shorter call and adds \\ at the end if not there
        /// </summary>
        /// <returns></returns>
        public static string GetWorkPath()
        {
            string workpath = CommonForms.Configuration.GetWorkingDirectory();
            if (!string.IsNullOrEmpty(workpath) && !workpath.EndsWith("\\"))
                workpath += "\\";

            return workpath;
        }
    }
}
