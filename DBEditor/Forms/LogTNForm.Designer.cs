namespace DBEditor {
    partial class LogTNForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.BoxNumberTextBox = new System.Windows.Forms.TextBox();
            this.CustomerNameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CompanyNameTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.AddressTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.PhoneTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.CommentsTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RestrictedTNLabel = new System.Windows.Forms.Label();
            this.UnrestrictedTNLabel = new System.Windows.Forms.Label();
            this.RestrictedRadioButton = new System.Windows.Forms.RadioButton();
            this.UnrestrictedRadioButton = new System.Windows.Forms.RadioButton();
            this.AddNewRemoteButton = new System.Windows.Forms.Button();
            this.DoneButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Box Number";
            // 
            // BoxNumberTextBox
            // 
            this.BoxNumberTextBox.Location = new System.Drawing.Point(114, 12);
            this.BoxNumberTextBox.Name = "BoxNumberTextBox";
            this.BoxNumberTextBox.Size = new System.Drawing.Size(100, 20);
            this.BoxNumberTextBox.TabIndex = 1;
            // 
            // CustomerNameTextBox
            // 
            this.CustomerNameTextBox.Location = new System.Drawing.Point(114, 38);
            this.CustomerNameTextBox.Name = "CustomerNameTextBox";
            this.CustomerNameTextBox.Size = new System.Drawing.Size(182, 20);
            this.CustomerNameTextBox.TabIndex = 5;
            this.CustomerNameTextBox.Enter += new System.EventHandler(this.TextBoxField_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Customer Name";
            // 
            // CompanyNameTextBox
            // 
            this.CompanyNameTextBox.Location = new System.Drawing.Point(114, 64);
            this.CompanyNameTextBox.Name = "CompanyNameTextBox";
            this.CompanyNameTextBox.Size = new System.Drawing.Size(182, 20);
            this.CompanyNameTextBox.TabIndex = 7;
            this.CompanyNameTextBox.Enter += new System.EventHandler(this.TextBoxField_Enter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Company Name";
            // 
            // AddressTextBox
            // 
            this.AddressTextBox.Location = new System.Drawing.Point(114, 90);
            this.AddressTextBox.Multiline = true;
            this.AddressTextBox.Name = "AddressTextBox";
            this.AddressTextBox.Size = new System.Drawing.Size(182, 67);
            this.AddressTextBox.TabIndex = 9;
            this.AddressTextBox.Enter += new System.EventHandler(this.TextBoxField_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Address";
            // 
            // PhoneTextBox
            // 
            this.PhoneTextBox.Location = new System.Drawing.Point(114, 163);
            this.PhoneTextBox.Name = "PhoneTextBox";
            this.PhoneTextBox.Size = new System.Drawing.Size(182, 20);
            this.PhoneTextBox.TabIndex = 11;
            this.PhoneTextBox.Enter += new System.EventHandler(this.TextBoxField_Enter);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(61, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Phone";
            // 
            // CommentsTextBox
            // 
            this.CommentsTextBox.Location = new System.Drawing.Point(114, 189);
            this.CommentsTextBox.Multiline = true;
            this.CommentsTextBox.Name = "CommentsTextBox";
            this.CommentsTextBox.Size = new System.Drawing.Size(182, 137);
            this.CommentsTextBox.TabIndex = 13;
            this.CommentsTextBox.Enter += new System.EventHandler(this.TextBoxField_Enter);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(43, 192);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Comments";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RestrictedTNLabel);
            this.groupBox1.Controls.Add(this.UnrestrictedTNLabel);
            this.groupBox1.Controls.Add(this.RestrictedRadioButton);
            this.groupBox1.Controls.Add(this.UnrestrictedRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(56, 332);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(224, 66);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TN Number";
            // 
            // RestrictedTNLabel
            // 
            this.RestrictedTNLabel.AutoSize = true;
            this.RestrictedTNLabel.Location = new System.Drawing.Point(145, 39);
            this.RestrictedTNLabel.Name = "RestrictedTNLabel";
            this.RestrictedTNLabel.Size = new System.Drawing.Size(35, 13);
            this.RestrictedTNLabel.TabIndex = 3;
            this.RestrictedTNLabel.Text = "label9";
            // 
            // UnrestrictedTNLabel
            // 
            this.UnrestrictedTNLabel.AutoSize = true;
            this.UnrestrictedTNLabel.Location = new System.Drawing.Point(29, 39);
            this.UnrestrictedTNLabel.Name = "UnrestrictedTNLabel";
            this.UnrestrictedTNLabel.Size = new System.Drawing.Size(35, 13);
            this.UnrestrictedTNLabel.TabIndex = 2;
            this.UnrestrictedTNLabel.Text = "label8";
            // 
            // RestrictedRadioButton
            // 
            this.RestrictedRadioButton.AutoSize = true;
            this.RestrictedRadioButton.Location = new System.Drawing.Point(125, 19);
            this.RestrictedRadioButton.Name = "RestrictedRadioButton";
            this.RestrictedRadioButton.Size = new System.Drawing.Size(73, 17);
            this.RestrictedRadioButton.TabIndex = 1;
            this.RestrictedRadioButton.TabStop = true;
            this.RestrictedRadioButton.Text = "Restricted";
            this.RestrictedRadioButton.UseVisualStyleBackColor = true;
            // 
            // UnrestrictedRadioButton
            // 
            this.UnrestrictedRadioButton.AutoSize = true;
            this.UnrestrictedRadioButton.Location = new System.Drawing.Point(11, 19);
            this.UnrestrictedRadioButton.Name = "UnrestrictedRadioButton";
            this.UnrestrictedRadioButton.Size = new System.Drawing.Size(82, 17);
            this.UnrestrictedRadioButton.TabIndex = 0;
            this.UnrestrictedRadioButton.TabStop = true;
            this.UnrestrictedRadioButton.Text = "Unrestricted";
            this.UnrestrictedRadioButton.UseVisualStyleBackColor = true;
            // 
            // AddNewRemoteButton
            // 
            this.AddNewRemoteButton.Location = new System.Drawing.Point(67, 404);
            this.AddNewRemoteButton.Name = "AddNewRemoteButton";
            this.AddNewRemoteButton.Size = new System.Drawing.Size(123, 23);
            this.AddNewRemoteButton.TabIndex = 15;
            this.AddNewRemoteButton.Text = "Add New Remote";
            this.AddNewRemoteButton.UseVisualStyleBackColor = true;
            this.AddNewRemoteButton.Click += new System.EventHandler(this.AddNewRemoteButton_Click);
            // 
            // DoneButton
            // 
            this.DoneButton.Location = new System.Drawing.Point(195, 404);
            this.DoneButton.Name = "DoneButton";
            this.DoneButton.Size = new System.Drawing.Size(75, 23);
            this.DoneButton.TabIndex = 16;
            this.DoneButton.Text = "Close";
            this.DoneButton.UseVisualStyleBackColor = true;
            this.DoneButton.Click += new System.EventHandler(this.DoneButton_Click);
            // 
            // LogTNForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 444);
            this.Controls.Add(this.DoneButton);
            this.Controls.Add(this.AddNewRemoteButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CommentsTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.PhoneTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.AddressTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CompanyNameTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CustomerNameTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BoxNumberTextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "LogTNForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add New Remote";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox BoxNumberTextBox;
        private System.Windows.Forms.TextBox CustomerNameTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox CompanyNameTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox AddressTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox PhoneTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox CommentsTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label RestrictedTNLabel;
        private System.Windows.Forms.Label UnrestrictedTNLabel;
        private System.Windows.Forms.RadioButton RestrictedRadioButton;
        private System.Windows.Forms.RadioButton UnrestrictedRadioButton;
        private System.Windows.Forms.Button AddNewRemoteButton;
        private System.Windows.Forms.Button DoneButton;
    }
}