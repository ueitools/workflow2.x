using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
//using CommonLogicFunctions;

namespace DBEditor
{
    public partial class IDLevelEditForm : Form
    {
        private IDLevelEdit _oIDLevelEdit 
            = new IDLevelEdit();

        public IDLevelEdit OIDLevelEdit
        {
            get { return _oIDLevelEdit; }
            set { _oIDLevelEdit = value; }
        }

        public IDLevelEditForm(IDLevelEditType type)
        {
            InitializeComponent();
            this._oIDLevelEdit.Type = type;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IDLevelEditForm_Load(object sender, EventArgs e)
        {
            GetIDList("");
            IDDataGridView.Columns["iPriority"].Visible = false;
            switch (this._oIDLevelEdit.Type)
            {
                case IDLevelEditType.LABEL:
                    IDDataGridView.Columns["Label"].DefaultCellStyle.BackColor = 
                        Color.LightGreen;
                    break;

                case IDLevelEditType.INTRON:
                    IDDataGridView.Columns["iPriority"].Visible = true;
                    IDDataGridView.Columns["Intron"].DefaultCellStyle.BackColor = 
                        Color.LightGreen;
                    IDDataGridView.Columns["IntronPriority"].DefaultCellStyle.BackColor =
                        Color.LightGreen;
                    break;

                default: // IDLevelEditType.DATA
                    IDDataGridView.Columns["FormattedData"].DefaultCellStyle.BackColor = 
                        Color.LightGreen;
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCommit_Click(object sender, EventArgs e)
        {
            string distinct = "";

            foreach (DataGridViewRow row in IDDataGridView.Rows)
            {
                if (row.Cells["NewValue"].Value == null
                    && row.Cells["iPriority"].Value == null)
                    continue;

                switch (_oIDLevelEdit.Type)
                {
                    case IDLevelEditType.DATA:
                        if (distinct.Contains(TableData(
                            row.Index, "Data")))
                            continue;
                        distinct += TableData(row.Index, "Data");
                        break;
                    case IDLevelEditType.LABEL:
                        if (distinct.Contains(TableData(
                            row.Index, "Label")))
                            continue;
                        distinct += TableData(row.Index, "Label");
                        break;
                    case IDLevelEditType.INTRON:
                        if (distinct.Contains(TableData(row.Index, "Label")
                            + TableData(row.Index, "Intron") 
                            + TableData(row.Index, "iPriority")))
                            continue;
                        distinct += TableData(row.Index, "Label")
                            + TableData(row.Index, "Intron")
                            + TableData(row.Index, "iPriority");
                        break;
                }

                UpdateData(row.Index);
                _oIDLevelEdit.Commit();
            }

            string logComment = tbLog.Text.ToString();
            logComment = logComment.Trim();

            if (!String.IsNullOrEmpty(logComment))
            {
                try
                {
                    DAOFactory.BeginTransaction();
                    /* insert a log record if it's not empty */
                    // first get the loglist of the id
                    string ID = IDComboBox.SelectedItem.ToString();

                    LogCollection loglist = DAOFactory.Log().Select(ID);

                    // create the new log from id and log textbox
                    Log newlog = Log.Create(ID, logComment);
                    loglist.Add(newlog);

                    // ReInsert() the updated loglist
                    DAOFactory.Log().ReInsert(ID, loglist);
                    DAOFactory.CommitTransaction();
                }
                catch (Exception ex)
                {
                    DAOFactory.RollBackTransaction();
                    MessageBox.Show(ex.Message);
                    return;
                }
            }

            Close();
        }

        private void buttonPreview_Click(object sender, EventArgs e)
        {
            UpdateData(IDDataGridView.CurrentCell.RowIndex);
            this._oIDLevelEdit.Preview();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateData(int row)
        {
            _oIDLevelEdit.ID = IDComboBox.SelectedItem.ToString();
            _oIDLevelEdit.CurrentData = TableData(row, "Data");
            _oIDLevelEdit.CurrentLabel = TableData(row, "Label");
            _oIDLevelEdit.CurrentIntron = TableData(row, "Intron");

            switch (_oIDLevelEdit.Type)
            {
                case IDLevelEditType.LABEL:
                    _oIDLevelEdit.NewLabel = TableData(row, "NewValue");
                    break;

                case IDLevelEditType.INTRON:
                    if (  TableData(row, "NewValue") != "" )
                        _oIDLevelEdit.NewIntron = TableData(row, "NewValue");
                    else _oIDLevelEdit.NewIntron = _oIDLevelEdit.CurrentIntron;

                    if (  TableData(row, "iPriority") != "" )
                        _oIDLevelEdit.NewPriority = TableData(row, "iPriority");
                    else _oIDLevelEdit.NewPriority = TableData(row, "IntronPriority");
                    break;

                case IDLevelEditType.DATA:
                default:
                    _oIDLevelEdit.NewData = TableData(row, "NewValue");
                    break;
            }
        }

        private string TableData(int row, string column)
        {
            if (IDDataGridView[column, row].Value == null)
                return "";
            return IDDataGridView[column, row].Value.ToString();
        }

        /// <summary>
        /// Load a list of ALL IDs in the database into the IDComboBox
        /// </summary>
        private void GetIDList(string usingID)
        {
            StringCollection list = DBFunctions.NonBlockingGetAllIDList();
            IDComboBox.ComboBox.DataSource = list;

            int index = IDComboBox.ComboBox.FindString(usingID);
            if (index >= 0)
                IDComboBox.SelectedIndex = index;
            else
                IDComboBox.SelectedIndex = 1;

        }

        /// <summary>
        /// When the selected index in the IDComboBox changes load all the views with the new ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IDComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = IDComboBox.ComboBox.FindString(IDComboBox.Text.ToString());
            if (index >= 0)
                IDComboBox.SelectedIndex = index;

            if (IDComboBox.SelectedIndex >= 0)
            {
                string SelectedID = IDComboBox.Text.ToString();
                IDFunctionCollection IDFunctionData = DAOFactory.ReadOnly().IDFunctionSelect(SelectedID);

                IDDataGridView.DataSource = IDFunctionData;

                this.Text = "Viewing " + SelectedID;

            }
        }

        /// <summary>
        /// Change the selected ID to the next id in the IDComboBox list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextIDBtn_Click(object sender, EventArgs e)
        {
            if (IDComboBox.SelectedIndex < IDComboBox.Items.Count - 1)
                IDComboBox.SelectedIndex++;
        }

        /// <summary>
        /// Change the selected ID to the previous id in the IDComboBox list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrevIDBtn_Click(object sender, EventArgs e)
        {
            if (IDComboBox.SelectedIndex > 0)
                IDComboBox.SelectedIndex--;
        }

        /// <summary>
        /// Change the selected ID forward to the first occurance of a mode change in the IDComboBox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextModeBtn_Click(object sender, EventArgs e)
        {
            char Mode = IDComboBox.Text.ToString()[0];
            StringCollection IDArray = (StringCollection)IDComboBox.ComboBox.DataSource;
            int index = IDComboBox.SelectedIndex;

            while (index < IDComboBox.Items.Count - 1)
            {
                if (Mode != IDArray[++index].ToString()[0])
                    break;
            }
            IDComboBox.SelectedIndex = index;
        }

        /// <summary>
        /// Change the selected ID back to the first occurance of a mode change in the IDComboBox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrevModeBtn_Click(object sender, EventArgs e)
        {
            char Mode = IDComboBox.Text.ToString()[0];
            StringCollection IDArray = (StringCollection)IDComboBox.ComboBox.DataSource;
            int index = IDComboBox.SelectedIndex;

            while (index > 0)
            {
                if (Mode != IDArray[--index].ToString()[0])
                    break;
            }
            IDComboBox.SelectedIndex = index;
        }

        /// <summary>
        /// Make sure there are no repeats of the data column being edited
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private IDFunctionCollection Distinct(IDFunctionCollection data)
        {
            string distinct = "";
            IDFunctionCollection final = new IDFunctionCollection();

            foreach (IDFunction row in data)
            {
                switch (_oIDLevelEdit.Type)
                {
                    case IDLevelEditType.DATA:
                        if (distinct.Contains(row.Data))
                            continue;
                        distinct += row.Data;
                        break;
                    case IDLevelEditType.LABEL:
                        if (distinct.Contains(row.Label))
                            continue;
                        distinct += row.Label;
                        break;
                    case IDLevelEditType.INTRON:
                        if (row.Intron.Length == 0)
                            continue;
                        if (distinct.Contains(row.Label + row.Intron))
                            continue;
                        distinct += row.Label + row.Intron;
                        break;
                }
                final.Add(row);
            }
            return final;
        }

        private void IDDataGridView_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu ClickMenu = EditFunctions.CommonEditPopup(RightClickMenuSelection);
                ClickMenu.MenuItems.Add("Preview", new EventHandler(RightClickMenuSelection));
                ClickMenu.Show(IDDataGridView, e.Location);                
            }
        }

        private void RightClickMenuSelection(object sender, EventArgs e)
        {
            string Selection = ((MenuItem)sender).Text;
            if (Selection.Equals("Preview"))
                buttonPreview_Click(sender, e);
            else
                EditFunctions.EditMenuFunctionsHandler(Selection, IDDataGridView, 
                    functionCollectionBindingSource);

        }

        private void IDDataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            EditFunctions.KeyCheck(sender, e, IDDataGridView,
                                    functionCollectionBindingSource);
        }

        private void IDDataGridView_CellEndEdit(object sender, 
            DataGridViewCellEventArgs e)
        {
            string value = (string)IDDataGridView[e.ColumnIndex,
                e.RowIndex].Value;
            
            if (value != null && value.Trim() == "")
            {
                value = null;
                IDDataGridView[e.ColumnIndex, e.RowIndex].Value
                    = null;
            }

            switch (_oIDLevelEdit.Type)
            {
                case IDLevelEditType.DATA:
                    string data = TableData(e.RowIndex, "Data");

                    for (int i = 0; i < IDDataGridView.RowCount; i++)
                    {
                        if (data == TableData(i, "Data"))
                        {
                            IDDataGridView[e.ColumnIndex, i].Value
                                = value;
                        }
                    }
                    break;
                case IDLevelEditType.LABEL:
                    string label = TableData(e.RowIndex, "Label");

                    for (int i = 0; i < IDDataGridView.RowCount; i++)
                    {
                        if (label == TableData(i, "Label"))
                        {
                            IDDataGridView[e.ColumnIndex, i].Value
                                = value;
                        }
                    }
                    break;
                case IDLevelEditType.INTRON:
                    string intronLabel = TableData(e.RowIndex, "Label")
                        + TableData(e.RowIndex, "Intron");

                    for (int i = 0; i < IDDataGridView.RowCount; i++)
                    {
                        if (intronLabel ==
                            (TableData(i, "Label")
                            + TableData(i, "Intron")))
                        {
                            IDDataGridView[e.ColumnIndex, i].Value
                                = value;
                        }
                    }
                    break;
            }
        }
    }
}