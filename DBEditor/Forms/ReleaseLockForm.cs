using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using CommonForms;
using BusinessObject;

namespace DBEditor
{
    public enum ReleaseLockMode { ID, TN }

    /// <summary>
    /// 
    /// </summary>
    public partial class ReleaseLockForm : Form
    {
        private ReleaseLockMode _mode;

        public ReleaseLockForm(ReleaseLockMode mode)
        {
            this._mode = mode;
            InitializeComponent();
            string adminList = Common.GetConfig("DBADMINISTRATOR").ToUpper();
            string user = SystemInformation.UserName.ToUpper();

            if (adminList.Contains(user))
                MainDbOption.Visible = true;

            MainDbOption.Checked = false;

            if (this._mode == ReleaseLockMode.ID)
            {
                this.Text = "Release Lock for ID";
                this.labelValue.Text = "Enter ID to unlock: ";
            }
            else
            {
                this.Text = "Release Lock for TN";
                this.labelValue.Text = "Enter TN number to unlock: ";
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (this.textBoxValue.Text == "")
                return;

            if (this.MainDbOption.Checked)
            {
                AccessRestrictForm AccessRestrict = new AccessRestrictForm();
                DialogResult res = AccessRestrict.ShowDialog();
                AccessRestrict.Close();
                if (res == DialogResult.OK)
                {
                    if (this._mode == ReleaseLockMode.ID)
                    {
                        ReleaseLock(textBoxValue.Text);
                    }
                    else
                    {
                        int tn = Int32.Parse(textBoxValue.Text);
                        ReleaseLock(tn);
                    }
                }
                else if (res == DialogResult.No)
                {
                    MessageBox.Show("Password Incorrect.",
                        "Password Error", MessageBoxButtons.OK);
                }
            }
            else
            {
                if (this._mode == ReleaseLockMode.ID)
                {
                    ReleaseLock(textBoxValue.Text);
                }
                else
                {
                    int tn = Int32.Parse(textBoxValue.Text);
                    ReleaseLock(tn);
                }
            }

            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        { 
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        private void ReleaseLock(string id)
        {
            if (MainDbOption.Checked)
                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
            try
            {
                DAOFactory.BeginTransaction();

//                PartialID oID = new PartialID();
//                oID.Load(id);
//                if (oID.Header.IsLocked != BooleanFlag.YES)
//                    throw new Exception("The ID is not locked");
                if (PartialID.IsLocked(id) == false)
                    throw new Exception("The ID is not locked");
                PartialID.UnLock(id);

                DAOFactory.CommitTransaction();
            }
            catch (Exception ex)
            {
                DAOFactory.RollBackTransaction();
                string msg = string.Format("Releasing Lock for ID: {0} failed.\n\n{1}", 
                                            id, ex.Message);
                MessageBox.Show(msg, "Error");
            }
            finally
            {
                DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);
            }
        }

        private void ReleaseLock(int tn)
        {
            if (MainDbOption.Checked)
                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
            try
            {
                DAOFactory.BeginTransaction();

//                TN oTN = new TN();
//                oTN.Load(tn);
//                if (oTN.Header.IsLocked != BooleanFlag.YES)
//                    throw new Exception("The TN is not locked");
                if (TN.IsLocked(tn) == false)
                    throw new Exception("The TN is not locked");
                TN.UnLock(tn);

                DAOFactory.CommitTransaction();
            }
            catch (Exception ex)
            {
                DAOFactory.RollBackTransaction();
                string msg = string.Format("Releasing Lock for TN: {0} failed.\n\n{1}",
                                            tn, ex.Message);
                MessageBox.Show(msg, "Error");
            }
            finally
            {
                DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);
            }
        }
    }
}