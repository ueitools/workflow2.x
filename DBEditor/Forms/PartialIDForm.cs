using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using BusinessObject;

namespace DBEditor
{
    /// <summary>
    /// current ID will be used to unlock ID properly
    /// </summary>
    public partial class PartialIDForm : Form
    {
        PartialID _oID = new PartialID();
        IDHeaderForm _idHeaderForm = new IDHeaderForm();
        FunctionForm _functionForm = new FunctionForm();
        DeviceForm _deviceForm = new DeviceForm();
        LogForm _LogForm = new LogForm();
        string _id = null;

        public PartialIDForm(string id)
        {
            InitializeComponent();

            _id = id;
            _idHeaderForm.MdiParent = this;
            _functionForm.MdiParent = this;
            _deviceForm.MdiParent = this;
            _LogForm.MdiParent = this;
        }

        public void EnableEdit()
        {
            this.commitToolStripButton.Enabled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditorTreeView_AfterSelect(
            object sender, TreeViewEventArgs e)
        {
            if (e.Node.Name == "IDHeader" && _idHeaderForm != null)
            {
                _idHeaderForm.Show();
                _idHeaderForm.BringToFront();
            }
            else if (e.Node.Name == "Function" && _functionForm != null)
            {
                _functionForm.Show();
                _functionForm.BringToFront();
            }
            else if (e.Node.Name == "Device" && _deviceForm != null)
            {
                _deviceForm.Show();
                _deviceForm.BringToFront();
            }
            else if (e.Node.Name == "Log" && _LogForm != null)
            {
                _LogForm.Show();
                _LogForm.BringToFront();
            }
        }

        private void IDForm_Load(object sender, EventArgs e)
        {
            _functionForm.DisableEntryHelper();

            _functionForm.ShowTNColumn();
            _deviceForm.ShowTNColumn();
            EditorTreeView.ExpandAll();
            _functionForm.WindowState = FormWindowState.Maximized;

            Bind();
        }

        private void Bind()
        {
            try
            {
                string strDBname = "";
                DAOFactory.BeginTransaction();
                _oID.Load(_id);

                _idHeaderForm.Bind(_oID.Header);
                _functionForm.Bind(_oID.FunctionList);
                _deviceForm.Bind(_oID.DeviceList);
                _deviceForm.FillComboBoxTNList(_id, _oID.FunctionList);
                _LogForm.Bind(_oID.Header.LogList);

                if (DAOFactory.GetDBConnectionString() ==
                 DBConnectionString.UEITEMP)
                    strDBname = "UEI Temp DB";
                else
                    strDBname = "UEI Public DB";

                this.Text = "Editing ID: " + _id + " in " + strDBname;

                //this.Text = "Editing " + _id + " in " +
                //    DAOFactory.GetDBConnectionString();
                _functionForm.Show();
                _functionForm.BringToFront();

                DAOFactory.CommitTransaction();
            }
            catch (Exception ex)
            {
                DAOFactory.RollBackTransaction();
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Make sure you are writing to Temp DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void commitToolStripButton_Click(object sender, EventArgs e)
        {
            if (DAOFactory.GetDBConnectionString() !=
                DBConnectionString.UEITEMP)
            {
                throw new ApplicationException(
                    "Committing to Public DB: " +
                    DAOFactory.GetDBConnectionString());
            }
            
            string comment = this._idHeaderForm.LogCommentTextBox.Text;
            if (!string.IsNullOrEmpty(comment))
            {
                Log log = Log.Create(_oID.Header.ID, comment);
                _oID.Header.LogList.Add(log);
                this._idHeaderForm.LogCommentTextBox.Clear();
            }

            try
            {
                DAOFactory.BeginTransaction();

                this._idHeaderForm.Validate();
                this._functionForm.Validate();
                this._deviceForm.Validate();
                this._LogForm.Validate();
                _oID.Update();
   
                DAOFactory.CommitTransaction();
                MessageBox.Show("ID has been saved to " +
                    "the database", "Success");
            }
            catch (Exception ex)
            {
                DAOFactory.RollBackTransaction();
                MessageBox.Show(ex.Message, "Save Failed");
            }
        }

        public void SetStatus(string statusMessage, bool error) {
            if (statusMessage != string.Empty) {
                toolStripStatusLabel.Text = statusMessage;
                toolStripStatusLabel.BackColor = Color.Tan;
                if (error) {
                    toolStripStatusLabel.BackColor = Color.Red;                    
                }
            }
        }
    }
}
