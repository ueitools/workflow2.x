using System;
using System.Threading;
using System.Windows.Forms;
using BusinessObject;
using Timer=System.Windows.Forms.Timer;

namespace DBEditor {
    public partial class LogTNForm : Form {
        private LockStatusFunctions _lockStatusFunctions;
        private Timer _releaseLockTimer;

        public LogTNForm() {
            DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);

            InitializeComponent();
            InitializeLockService();
            InitializeControls();
        }

        private void InitializeLockService() {
            _lockStatusFunctions = new LockStatusFunctions();
        }

        private void InitializeControls() {
            InitializeLockServiceNumbers();

            CustomerNameTextBox.Text = string.Empty;
            CompanyNameTextBox.Text = string.Empty;
            AddressTextBox.Text = string.Empty;
            PhoneTextBox.Text = string.Empty;
            CommentsTextBox.Text = string.Empty;

            _releaseLockTimer = new Timer();
            _releaseLockTimer.Interval = 300000;
            _releaseLockTimer.Tick += ReleaseLockTimer_Tick;
        }

        private void InitializeLockServiceNumbers() {
            BoxNumberTextBox.Text = _lockStatusFunctions.PreviewNextBoxNumber().ToString();
            RestrictedTNLabel.Text = _lockStatusFunctions.PreviewNextRestrictedTnNumber().ToString();
            UnrestrictedTNLabel.Text = _lockStatusFunctions.PreviewNextTnNumber().ToString();
        }

        private void AddNewRemoteButton_Click(object sender, System.EventArgs e) {
            int tnNumber = 0;
            if (RestrictedRadioButton.Checked) {
                tnNumber = int.Parse(RestrictedTNLabel.Text);
            } else if (UnrestrictedRadioButton.Checked) {
                tnNumber = int.Parse(UnrestrictedTNLabel.Text);
            }

            try {
                TN tn = new TN();
                tn.Header.TN = tnNumber;

                if (tn.Header.TN > 0) {
                    tn.Header.Box = BoxNumberTextBox.Text;
                    tn.Header.Sender_Name = CustomerNameTextBox.Text;
                    tn.Header.Sender_Company = CompanyNameTextBox.Text;
                    tn.Header.Sender_Address = AddressTextBox.Text;
                    tn.Header.Sender_Phone = PhoneTextBox.Text;
                    tn.Header.Text = CommentsTextBox.Text;
                    
                    Log logItem = new Log();
                    logItem.TN = tn.Header.TN;
                    logItem.Time = DateTime.Now;
                    logItem.Operator = Environment.UserName;
                    logItem.Content = "IN";
                    tn.Header.LogList.Add(logItem);

                    tn.Save();

                    _lockStatusFunctions.IncrementBoxNumber(Environment.UserDomainName, Environment.UserName);
                    if (RestrictedRadioButton.Checked) {
                        _lockStatusFunctions.IncrementRestrictedTnNumber(Environment.UserDomainName, Environment.UserName);
                    } else {
                        _lockStatusFunctions.IncrementTnNumber(Environment.UserDomainName, Environment.UserName);
                    }

                    _lockStatusFunctions.LockExistingTN(tn.Header.TN, Environment.UserDomainName, Environment.UserName);

                    MessageBox.Show(string.Format("TN{0} has been successfully created.", tnNumber));

                    InitializeLockServiceNumbers();
                } else {
                    MessageBox.Show("TN number must be selected.");
                }
            } catch (Exception ex) {
                MessageBox.Show(string.Format("Error: TN{0} was not added.\n\n{1}", tnNumber, ex.Message), "TN Creation Error");
            }
        }

        private void DoneButton_Click(object sender, System.EventArgs e) {
            Close();
        }

        private void TextBoxField_Enter(object sender, EventArgs e) {
            TextBox tb = sender as TextBox;

            if (tb != null) {
                tb.SelectAll();
            }
        }

        protected override void OnClosed(EventArgs e) {
            _releaseLockTimer.Stop();
        }

        private void ReleaseLockTimer_Tick(object sender, EventArgs e) {
            _releaseLockTimer.Tick -= ReleaseLockTimer_Tick;
            Close();
            throw new TimeoutException("Log New Remote operation was closed due to inactivity.");
        }

        protected override void WndProc(ref Message m) {
            const int WM_TIMER = 275;
            if (m.Msg != WM_TIMER) {
                _releaseLockTimer.Stop();
                _releaseLockTimer.Start();
            }

            base.WndProc(ref m);
        }
    }
}