using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;

namespace DBEditor
{
    public partial class DeviceForm : Form
    {
        private OpInfoCollection _opInfoList = 
            DBFunctions.GetAllOpInfoList();

        private BrandCollection _brandList = 
            DBFunctions.GetAllBrandList();

        private DeviceTypeCollection _deviceTypeList =
            DBFunctions.GetAllDeviceTypeList();

        private CountryCollection _countryList =
            DBFunctions.GetAllCountryList();

      
        public DeviceForm()
        {
            InitializeComponent(); 
        }

        public void Bind(DeviceCollection deviceList)
        {
            deviceCollectionBindingSource.DataSource = deviceList;

            this.Brand.DataSource = _brandList;
            this.Brand.DisplayMember = "Name";

            this.Operation.DataSource = GetOperationList(_opInfoList);
            this.Information.DataSource = _opInfoList;
            this.Information.DisplayMember = "Information";

            this.DeviceName.DataSource = _deviceTypeList;
            this.DeviceName.DisplayMember = "Name";

            this.Region.DataSource = GetRegionList(_countryList);
            this.CountryName.DataSource = _countryList;
            this.CountryName.DisplayMember = "Name";
        }

        public void ShowIDColumn()
        {
            this.ComboBoxTNList.Visible = false;
            this.ComboBoxIDList.Visible = true;
        }

        public void ShowTNColumn()
        {
            this.ComboBoxTNList.Visible = true;
            this.ComboBoxIDList.Visible = false;
        }

        /// <summary>
        /// REFACTOR
        /// </summary>
        /// <param name="_tn"></param>
        public void FillComboBoxIDList(
            int tn, FunctionCollection functionList)
        {
            StringCollection idList = DBFunctions.GetIDList(tn);

            this.ComboBoxIDList.Items.Clear();
            this.ComboBoxIDList.Items.Add("--");

            foreach (string id in idList)
            {
                this.ComboBoxIDList.Items.Add(id);
            }

            /// due to incomplete TN info in the current DB we need to
            /// supplement the list with ID actually in use at this time.
            DeviceCollection deviceList =
                (DeviceCollection)deviceCollectionBindingSource.DataSource;
            foreach (Device device in deviceList)
            {
                if (string.IsNullOrEmpty(device.ID))
                    continue;

                if (this.ComboBoxIDList.Items.Contains(device.ID) == false)
                    this.ComboBoxIDList.Items.Add(device.ID);
            }

            foreach (Function function in functionList)
            {
                if (string.IsNullOrEmpty(function.ID))
                    continue;

                if (this.ComboBoxIDList.Items.Contains(function.ID) == false)
                    this.ComboBoxIDList.Items.Add(function.ID);
            }
        }

        /// <summary>
        /// Load a list of all TNs that reference this ID.
        /// Store them in the comboBoxTNList for the user to select
        /// </summary>
        /// <param name="ID">Currently selected ID name such as "T0000"</param>
        public void FillComboBoxTNList(string id, FunctionCollection functionList)
        {
            this.ComboBoxTNList.Items.Clear();
            this.ComboBoxTNList.Items.Add(0);

            IntegerCollection tnList = DBFunctions.GetTNList(id);

            foreach (int tn in tnList)
            {
                string strTN = string.Format("TN{0:D5}", tn);
                if (!ComboBoxTNList.Items.Contains(strTN))
                    ComboBoxTNList.Items.Add(strTN);
            }

            DeviceCollection deviceList =
               (DeviceCollection)deviceCollectionBindingSource.DataSource;
            foreach (Device device in deviceList)
            {
                if (string.IsNullOrEmpty(device.TN.ToString()))
                    continue;

                if (this.ComboBoxTNList.Items.Contains(device.TN) == false)
                    this.ComboBoxTNList.Items.Add(device.TN);
            }
        }


        /////////////////////////////////////////////////////////////////////
        ////
        //// private
        ////

        private void deviceCollectionDataGridView_DataError(
            object sender, DataGridViewDataErrorEventArgs e)
        {
            handleError((DataGridView)sender, e);
        }

        private void countryDataGridView_DataError(
            object sender, DataGridViewDataErrorEventArgs e)
        {
            handleError((DataGridView)sender, e);
        }

        private void deviceTypeDataGridView_DataError(
            object sender, DataGridViewDataErrorEventArgs e)
        {
            handleError((DataGridView)sender, e);
        }

        private void opInfoDataGridView_DataError(
            object sender, DataGridViewDataErrorEventArgs e)
        {
            handleError((DataGridView)sender, e);
        }

        private void handleError(
            DataGridView view, DataGridViewDataErrorEventArgs e)
        {
            view.Rows[e.RowIndex].ErrorText = e.Exception.Message;
            view.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = 
                e.Exception.Message;

            e.ThrowException = false;
        }
    }
}