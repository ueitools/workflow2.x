using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Printing;

namespace DBEditor
{
    /// <summary>
    /// remove this protion to seprate class later
    /// </summary>
    partial class TNForm
    {
        #region variables for printing
        private Font printFont;
        private string _PrintPagesWatch;
        #endregion

        /// 
        /// Coupling TNFrom to FunctionForm 
        /// Is this only way?
        /// 
        #region Key Event Handling

        private void cutToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            if (this.ActiveMdiChild.Equals(_functionForm))
                _functionForm.ToolStripMenuItem_Relay("Cut");
        }

        private void copyToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            if (this.ActiveMdiChild.Equals(_functionForm))
                _functionForm.ToolStripMenuItem_Relay("Copy");
        }

        private void pasteToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            if (this.ActiveMdiChild.Equals(_functionForm))
                _functionForm.ToolStripMenuItem_Relay("Paste");
        }

        private void deleteToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            if (this.ActiveMdiChild.Equals(_functionForm))
                _functionForm.ToolStripMenuItem_Relay("Delete");
        }

        private void viewFileToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            if (this.ActiveMdiChild.Equals(_functionForm))
                _functionForm.ToolStripMenuItem_Relay("ViewFile");
        }
        #endregion

        /// 
        /// remove this out of this file
        /// 
        #region Print
        private void printToolStripButton_Click(object sender, EventArgs e)
        {
            PrintOptionDlg PrintOption = new PrintOptionDlg();
            DialogResult res = PrintOption.ShowDialog();
            PrintOption.Close();
            if (res == DialogResult.OK)
            {
                _PrintPagesWatch = PrintOption.PrintDevice ? "D" : "";
                _PrintPagesWatch += PrintOption.PrintHead ? "H" : "";
                _PrintPagesWatch += PrintOption.PrintFunc ? "F" : "";
            }

            PrintDocument PrintID = new PrintDocument();
            PrintDialog pdlg = new PrintDialog();
            pdlg.Document = PrintID;
            pdlg.AllowPrintToFile = false;

            DialogResult result = pdlg.ShowDialog();
            if (result != DialogResult.OK)
                return;

            //CurrentForm = 0;
            printFont = new Font("Courier New", 8.5f);
            PrintID.DocumentName = _tn.ToString();
            PrintID.PrintPage += new PrintPageEventHandler(this.pid_PagePrint);

            PrintID.Print();
        }// end of printToolStripButton_Click

        private void pid_PagePrint(object sender, PrintPageEventArgs ev)
        {
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;

            float leftMargin = 10;
            float topMargin = 50;
            string line = null;

            // Calculate the number of lines per page.
            linesPerPage = ev.MarginBounds.Height /
               printFont.GetHeight(ev.Graphics);

            // Print Header of the file
            PrintReportHeaderToGraphics(ev.Graphics, topMargin, leftMargin);
            count++;

            // Print each line of the file.
            while (count < linesPerPage && (line = GetReportLine()) != null)
            {
                float startTopPos = topMargin + count * printFont.GetHeight(ev.Graphics);
                int printedLines = PrintReportLineToGraphics(line,
                    ev.Graphics, startTopPos, leftMargin);

                count += printedLines;
            }

            // User's selection for pages to print
            SelectPageToPrint(line, ref ev);

        }// end of pid_PagePrint

        private void SelectPageToPrint(string line, ref PrintPageEventArgs ev)
        {
            if (line == null) // finished printing a part 
            {
                ResetLastLinePrinted();
                if (_PrintPagesWatch.Length > 1)
                {
                    _PrintPagesWatch = _PrintPagesWatch.Remove(0, 1);
                    ev.HasMorePages = true;
                }
                else
                {
                    _PrintPagesWatch = _PrintPagesWatch.Remove(0, 1);
                    ev.HasMorePages = false;
                }
            }
            else
                ev.HasMorePages = true;
        }

        private void ResetLastLinePrinted()
        {
            switch (_PrintPagesWatch[0])
            {
                case 'D':
                    _deviceForm.resetLastLinePrinted();
                    break;
                case 'F':
                    _functionForm.resetLastLinePrinted();
                    break;
                case 'H':
                    _tnHeaderForm.resetLastLinePrinted();
                    break;
                default:
                    break;
            }
        }
        private void PrintReportHeaderToGraphics(Graphics g, float TopMargin,
            float leftMargin)
        {
            string head = GetReportHeader(_tn.ToString());
            g.DrawString(head, printFont, Brushes.Black,
                    leftMargin, TopMargin, new StringFormat());
        }

        private int PrintReportLineToGraphics(string line, Graphics g,
            float startTopPos, float leftMargin)
        {
            StringFormat cellformat = new StringFormat();

            string lineToPrint = line;
            int printedLinesCount = 0;
            float yPos = startTopPos;
            // width of the whole datagridview is about 110 characters
            int maxCharsToPrint = 110;

            while (lineToPrint.Length > maxCharsToPrint)
            {
                int ww_index = lineToPrint.Substring(0, maxCharsToPrint).LastIndexOf(' ');
                yPos += printFont.GetHeight(g);

                g.DrawString(lineToPrint.Substring(0, ww_index), printFont, Brushes.Black,
                    leftMargin, yPos, cellformat);

                lineToPrint = lineToPrint.Substring(ww_index + 1);
                printedLinesCount++;

                // from the 2nd line, we print only the content field so need
                // to reset leftMargin and maximun number of characters to 
                // print
                leftMargin = 245;
                maxCharsToPrint = 75;
            }

            yPos += printFont.GetHeight(g);
            g.DrawString(lineToPrint, printFont, Brushes.Black, leftMargin,
                yPos, cellformat);
            return ++printedLinesCount;
        }

        /// <summary>
        /// A method to call each section in turn
        /// </summary>
        /// <returns></returns>
        private string GetReportLine()
        {
            string line = null;
            switch (_PrintPagesWatch[0])
            {
                case 'F':
                    line = this._functionForm.GetTNReportLine();
                    break;
                case 'D':
                    line = this._deviceForm.GetTNReportLine();
                    break;
                case 'H':
                    line = this._tnHeaderForm.GetReportLine();
                    break;
                default: break;
            }
            return line;
        }

        private string GetReportHeader(string TN)
        {
            string line = null;
            switch (_PrintPagesWatch[0])
            {
                case 'F':
                    line = this._functionForm.GetTNReportHeader(TN);
                    break;
                case 'D':
                    line = this._deviceForm.GetTNReportHeader(TN);
                    break;
                case 'H':
                    line = this._tnHeaderForm.GetReportHeader(TN);
                    break;
                default: break;
            }
            return line;
        }
        #endregion
    }
}
