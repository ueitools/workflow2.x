using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using BusinessObject;

namespace DBEditor
{
    public partial class IDHeaderForm : Form
    {
        private int LastLinePrinted;
        public void resetLastLinePrinted()
        {
            LastLinePrinted = 0;
        }

        public IDHeaderForm()
        {
            InitializeComponent();
        }

        public void Bind(IDHeader idHeader)
        {
            if (idHeader != null)
                iDHeaderBindingSource.DataSource = idHeader;

            StatusQ.Checked = (idHeader.Status == "Q") ? true : false;
            StatusM.Checked = (idHeader.Status == "M") ? true : false;
            StatusE.Checked = (idHeader.Status == "E") ? true : false;
            StatusP.Checked = (idHeader.Status == "P") ? true : false;
        }

        private void StatusChanged(object sender, EventArgs e)
        {
            IDHeader idHeader = (IDHeader)iDHeaderBindingSource.DataSource;
            foreach(RadioButton btn in StatusBox.Controls)
            {
                if (btn.Checked)
                    idHeader.Status = btn.Tag.ToString();
            }
        }

        public string GetReportHeader(string ID)
        {
            return string.Format("ID: {0} \r\nHeader Section\r\n", ID);
        }
        /// <summary>
        /// Format the specified row of data from the table for printing.
        /// The data is returned in the order it was last viewed. 
        /// if the data sort order was changed the data will be returned in that order.
        /// </summary>
        /// <param name="row">the zero based row of data requested.</param>
        /// <returns>a string sutible for printing</returns>
        public string GetReportLine()
        {
            int row = LastLinePrinted++;
            string data = null;
            IDHeader Header = (IDHeader)iDHeaderBindingSource.DataSource;

            switch (row)
            {
                case 0:
                    data = string.Format("Executor: {0}", Header.Executor_Code);
                    break;
                case 1:
                    data = string.Format("Is Inversed Prefix: {0}", Header.IsInversedPrefix);
                    break;
                case 2:
                    data = string.Format("Is External Prefix: {0}",Header.IsExternalPrefix);
                    break;
                case 3:
                    data = string.Format("Is Restricted: {0}",Header.IsRestricted);
                    break;
                case 4:
                    data = string.Format("Is Frequency Data: {0}",Header.IsFrequencyData); 
                    break;
                case 5:
                    data = string.Format("Parent ID: {0}",Header.Parent_ID);
                    break;
                case 6:
                    data = string.Format("Is Hex Format: {0}",Header.IsFrequencyData);
                    break;
                case 7:
                    data = string.Format("Status: {0}",Header.Status);
                    break;
                case 8:
                    data = string.Format("Is Inversd Data: {0}",Header.IsInversedData); 
                    break;
                case 9:
                    data = string.Format("Text: {0}",Header.Text);
                    break;
                default:
                    row -= 5;
                    if (row < Header.PrefixList.Count)
                    {
                        string d = Header.PrefixList[row].Data;
                        string t = Header.PrefixList[row].Description;
                        data = string.Format("{0,-36} {1}", d, t);
                    }
                    break;
            }
            return data;
        }

        private void prefixDataGridView_DataError(
            object sender, DataGridViewDataErrorEventArgs e)
        {
            handleError((DataGridView)sender, e);
        }

        private void handleError(
            DataGridView view, DataGridViewDataErrorEventArgs e)
        {
            view.Rows[e.RowIndex].ErrorText = e.Exception.Message;
            view.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText =
                e.Exception.Message;

            e.ThrowException = false;
        }
    }
}