namespace DBEditor
{
    partial class IDLevelEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IDLevelEditForm));
            this.buttonCommit = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.IDDataGridView = new System.Windows.Forms.DataGridView();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FormattedData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Label = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Intron = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IntronPriority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NewValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iPriority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.functionCollectionBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.functionDAOBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.spacer2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.PrevModeBtn = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.PrevIDBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.IDComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.ChangeIDBtn = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.NextIDBtn = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.NextModeBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.functionCollectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelLog = new System.Windows.Forms.Label();
            this.tbLog = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.IDDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.functionCollectionBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.functionDAOBindingNavigator)).BeginInit();
            this.functionDAOBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.functionCollectionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCommit
            // 
            this.buttonCommit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonCommit.Location = new System.Drawing.Point(381, 527);
            this.buttonCommit.Name = "buttonCommit";
            this.buttonCommit.Size = new System.Drawing.Size(78, 23);
            this.buttonCommit.TabIndex = 15;
            this.buttonCommit.Text = "Commit";
            this.buttonCommit.Click += new System.EventHandler(this.buttonCommit_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonCancel.Location = new System.Drawing.Point(271, 527);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(80, 23);
            this.buttonCancel.TabIndex = 16;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // IDDataGridView
            // 
            this.IDDataGridView.AllowUserToAddRows = false;
            this.IDDataGridView.AllowUserToDeleteRows = false;
            this.IDDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.IDDataGridView.AutoGenerateColumns = false;
            this.IDDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.IDDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Data,
            this.FormattedData,
            this.Label,
            this.Intron,
            this.IntronPriority,
            this.NewValue,
            this.iPriority});
            this.IDDataGridView.DataSource = this.functionCollectionBindingSource1;
            this.IDDataGridView.Location = new System.Drawing.Point(0, 28);
            this.IDDataGridView.Name = "IDDataGridView";
            this.IDDataGridView.Size = new System.Drawing.Size(755, 428);
            this.IDDataGridView.TabIndex = 19;
            this.IDDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IDDataGridView_KeyDown);
            this.IDDataGridView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.IDDataGridView_MouseClick);
            this.IDDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.IDDataGridView_CellEndEdit);
            // 
            // Data
            // 
            this.Data.DataPropertyName = "Data";
            this.Data.HeaderText = "";
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            this.Data.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Data.Visible = false;
            // 
            // FormattedData
            // 
            this.FormattedData.DataPropertyName = "FormattedData";
            this.FormattedData.FillWeight = 3.261109F;
            this.FormattedData.HeaderText = "Data";
            this.FormattedData.Name = "FormattedData";
            this.FormattedData.ReadOnly = true;
            this.FormattedData.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FormattedData.Width = 200;
            // 
            // Label
            // 
            this.Label.DataPropertyName = "Label";
            this.Label.FillWeight = 3.261109F;
            this.Label.HeaderText = "Label";
            this.Label.Name = "Label";
            this.Label.ReadOnly = true;
            this.Label.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Label.Width = 200;
            // 
            // Intron
            // 
            this.Intron.DataPropertyName = "Intron";
            this.Intron.FillWeight = 1.10359F;
            this.Intron.HeaderText = "Intron";
            this.Intron.Name = "Intron";
            this.Intron.ReadOnly = true;
            this.Intron.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // IntronPriority
            // 
            this.IntronPriority.DataPropertyName = "IntronPriority";
            this.IntronPriority.DividerWidth = 1;
            this.IntronPriority.FillWeight = 25F;
            this.IntronPriority.HeaderText = "P";
            this.IntronPriority.Name = "IntronPriority";
            this.IntronPriority.ReadOnly = true;
            this.IntronPriority.Width = 25;
            // 
            // NewValue
            // 
            this.NewValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NewValue.FillWeight = 92.52647F;
            this.NewValue.HeaderText = "New Value";
            this.NewValue.Name = "NewValue";
            this.NewValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // iPriority
            // 
            this.iPriority.FillWeight = 25F;
            this.iPriority.HeaderText = "P";
            this.iPriority.Name = "iPriority";
            this.iPriority.Width = 25;
            // 
            // functionCollectionBindingSource1
            // 
            this.functionCollectionBindingSource1.DataSource = typeof(BusinessObject.FunctionCollection);
            // 
            // functionDAOBindingNavigator
            // 
            this.functionDAOBindingNavigator.AddNewItem = null;
            this.functionDAOBindingNavigator.CountItem = null;
            this.functionDAOBindingNavigator.DeleteItem = null;
            this.functionDAOBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spacer2,
            this.toolStripSeparator2,
            this.PrevModeBtn,
            this.bindingNavigatorSeparator,
            this.PrevIDBtn,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.IDComboBox,
            this.ChangeIDBtn,
            this.bindingNavigatorSeparator1,
            this.NextIDBtn,
            this.bindingNavigatorSeparator2,
            this.NextModeBtn,
            this.toolStripSeparator3});
            this.functionDAOBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.functionDAOBindingNavigator.MoveFirstItem = null;
            this.functionDAOBindingNavigator.MoveLastItem = null;
            this.functionDAOBindingNavigator.MoveNextItem = null;
            this.functionDAOBindingNavigator.MovePreviousItem = null;
            this.functionDAOBindingNavigator.Name = "functionDAOBindingNavigator";
            this.functionDAOBindingNavigator.PositionItem = null;
            this.functionDAOBindingNavigator.Size = new System.Drawing.Size(755, 25);
            this.functionDAOBindingNavigator.TabIndex = 25;
            this.functionDAOBindingNavigator.Text = "bindingNavigator1";
            // 
            // spacer2
            // 
            this.spacer2.Name = "spacer2";
            this.spacer2.Size = new System.Drawing.Size(19, 22);
            this.spacer2.Text = "    ";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // PrevModeBtn
            // 
            this.PrevModeBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PrevModeBtn.Image = ((System.Drawing.Image)(resources.GetObject("PrevModeBtn.Image")));
            this.PrevModeBtn.Name = "PrevModeBtn";
            this.PrevModeBtn.Size = new System.Drawing.Size(23, 22);
            this.PrevModeBtn.Text = "Move first";
            this.PrevModeBtn.Click += new System.EventHandler(this.PrevModeBtn_Click);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // PrevIDBtn
            // 
            this.PrevIDBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PrevIDBtn.Image = ((System.Drawing.Image)(resources.GetObject("PrevIDBtn.Image")));
            this.PrevIDBtn.Name = "PrevIDBtn";
            this.PrevIDBtn.Size = new System.Drawing.Size(23, 22);
            this.PrevIDBtn.Text = "Move previous";
            this.PrevIDBtn.Click += new System.EventHandler(this.PrevIDBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(18, 22);
            this.toolStripLabel1.Text = "ID";
            // 
            // IDComboBox
            // 
            this.IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.IDComboBox.DropDownHeight = 200;
            this.IDComboBox.DropDownWidth = 95;
            this.IDComboBox.IntegralHeight = false;
            this.IDComboBox.MaxDropDownItems = 10;
            this.IDComboBox.Name = "IDComboBox";
            this.IDComboBox.Size = new System.Drawing.Size(100, 25);
            this.IDComboBox.SelectedIndexChanged += new System.EventHandler(this.IDComboBox_SelectedIndexChanged);
            // 
            // ChangeIDBtn
            // 
            this.ChangeIDBtn.BackColor = System.Drawing.SystemColors.Control;
            this.ChangeIDBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ChangeIDBtn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ChangeIDBtn.Image = ((System.Drawing.Image)(resources.GetObject("ChangeIDBtn.Image")));
            this.ChangeIDBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ChangeIDBtn.Name = "ChangeIDBtn";
            this.ChangeIDBtn.Size = new System.Drawing.Size(33, 22);
            this.ChangeIDBtn.Text = "View";
            this.ChangeIDBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.ChangeIDBtn.Click += new System.EventHandler(this.IDComboBox_SelectedIndexChanged);
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // NextIDBtn
            // 
            this.NextIDBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.NextIDBtn.Image = ((System.Drawing.Image)(resources.GetObject("NextIDBtn.Image")));
            this.NextIDBtn.Name = "NextIDBtn";
            this.NextIDBtn.Size = new System.Drawing.Size(23, 22);
            this.NextIDBtn.Text = "Move next";
            this.NextIDBtn.Click += new System.EventHandler(this.NextIDBtn_Click);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // NextModeBtn
            // 
            this.NextModeBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.NextModeBtn.Image = ((System.Drawing.Image)(resources.GetObject("NextModeBtn.Image")));
            this.NextModeBtn.Name = "NextModeBtn";
            this.NextModeBtn.Size = new System.Drawing.Size(23, 22);
            this.NextModeBtn.Text = "Move last";
            this.NextModeBtn.Click += new System.EventHandler(this.NextModeBtn_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // functionCollectionBindingSource
            // 
            this.functionCollectionBindingSource.DataSource = typeof(BusinessObject.IDFunctionCollection);
            // 
            // labelLog
            // 
            this.labelLog.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labelLog.AutoSize = true;
            this.labelLog.Location = new System.Drawing.Point(24, 483);
            this.labelLog.Name = "labelLog";
            this.labelLog.Size = new System.Drawing.Size(25, 13);
            this.labelLog.TabIndex = 26;
            this.labelLog.Text = "Log";
            // 
            // tbLog
            // 
            this.tbLog.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.tbLog.Location = new System.Drawing.Point(55, 480);
            this.tbLog.Name = "tbLog";
            this.tbLog.Size = new System.Drawing.Size(688, 20);
            this.tbLog.TabIndex = 27;
            // 
            // IDLevelEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(755, 578);
            this.Controls.Add(this.tbLog);
            this.Controls.Add(this.labelLog);
            this.Controls.Add(this.functionDAOBindingNavigator);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.IDDataGridView);
            this.Controls.Add(this.buttonCommit);
            this.Name = "IDLevelEditForm";
            this.Load += new System.EventHandler(this.IDLevelEditForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.IDDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.functionCollectionBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.functionDAOBindingNavigator)).EndInit();
            this.functionDAOBindingNavigator.ResumeLayout(false);
            this.functionDAOBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.functionCollectionBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCommit;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.DataGridView IDDataGridView;
        private System.Windows.Forms.BindingNavigator functionDAOBindingNavigator;
        private System.Windows.Forms.ToolStripLabel spacer2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton PrevModeBtn;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripButton PrevIDBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox IDComboBox;
        private System.Windows.Forms.ToolStripButton ChangeIDBtn;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton NextIDBtn;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton NextModeBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.BindingSource functionCollectionBindingSource;
        private System.Windows.Forms.BindingSource functionCollectionBindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn FormattedData;
        private System.Windows.Forms.DataGridViewTextBoxColumn Label;
        private System.Windows.Forms.DataGridViewTextBoxColumn Intron;
        private System.Windows.Forms.DataGridViewTextBoxColumn IntronPriority;
        private System.Windows.Forms.DataGridViewTextBoxColumn NewValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn iPriority;
        private System.Windows.Forms.Label labelLog;
        private System.Windows.Forms.TextBox tbLog;
    }
}