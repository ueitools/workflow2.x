namespace DBEditor
{
    partial class IDRenameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CancelBtn = new System.Windows.Forms.Button();
            this.OKBtn = new System.Windows.Forms.Button();
            this.IDComboBox = new System.Windows.Forms.ComboBox();
            this.IDLabel = new System.Windows.Forms.Label();
            this.NextModeBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.UEIDeviceModeComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.UEIDeviceCodeComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.UEIDeviceGroupComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PreviousModeBtn = new System.Windows.Forms.Button();
            this.ViewIDs = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // CancelBtn
            // 
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.Location = new System.Drawing.Point(247, 277);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(75, 23);
            this.CancelBtn.TabIndex = 5;
            this.CancelBtn.Text = "Cancel";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // OKBtn
            // 
            this.OKBtn.Location = new System.Drawing.Point(60, 277);
            this.OKBtn.Name = "OKBtn";
            this.OKBtn.Size = new System.Drawing.Size(75, 23);
            this.OKBtn.TabIndex = 4;
            this.OKBtn.Text = "OK";
            this.OKBtn.UseVisualStyleBackColor = true;
            this.OKBtn.Click += new System.EventHandler(this.OKBtn_Click);
            // 
            // IDComboBox
            // 
            this.IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.IDComboBox.FormattingEnabled = true;
            this.IDComboBox.Location = new System.Drawing.Point(56, 18);
            this.IDComboBox.Name = "IDComboBox";
            this.IDComboBox.Size = new System.Drawing.Size(116, 21);
            this.IDComboBox.TabIndex = 0;
            this.IDComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IDComboBox_KeyPress);
            // 
            // IDLabel
            // 
            this.IDLabel.AutoSize = true;
            this.IDLabel.Location = new System.Drawing.Point(29, 21);
            this.IDLabel.Name = "IDLabel";
            this.IDLabel.Size = new System.Drawing.Size(21, 13);
            this.IDLabel.TabIndex = 3;
            this.IDLabel.Text = "ID:";
            // 
            // NextModeBtn
            // 
            this.NextModeBtn.Enabled = false;
            this.NextModeBtn.Location = new System.Drawing.Point(314, 16);
            this.NextModeBtn.Name = "NextModeBtn";
            this.NextModeBtn.Size = new System.Drawing.Size(28, 23);
            this.NextModeBtn.TabIndex = 8;
            this.NextModeBtn.Text = ">";
            this.NextModeBtn.UseVisualStyleBackColor = true;
            this.NextModeBtn.Click += new System.EventHandler(this.NextModeBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Available UEI Device Mode";
            // 
            // UEIDeviceModeComboBox
            // 
            this.UEIDeviceModeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.UEIDeviceModeComboBox.FormattingEnabled = true;
            this.UEIDeviceModeComboBox.Location = new System.Drawing.Point(171, 75);
            this.UEIDeviceModeComboBox.Name = "UEIDeviceModeComboBox";
            this.UEIDeviceModeComboBox.Size = new System.Drawing.Size(167, 21);
            this.UEIDeviceModeComboBox.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Available UEI Device Code";
            // 
            // UEIDeviceCodeComboBox
            // 
            this.UEIDeviceCodeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.UEIDeviceCodeComboBox.FormattingEnabled = true;
            this.UEIDeviceCodeComboBox.Location = new System.Drawing.Point(171, 122);
            this.UEIDeviceCodeComboBox.Name = "UEIDeviceCodeComboBox";
            this.UEIDeviceCodeComboBox.Size = new System.Drawing.Size(167, 21);
            this.UEIDeviceCodeComboBox.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.UEIDeviceGroupComboBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.UEIDeviceModeComboBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.UEIDeviceCodeComboBox);
            this.groupBox1.Location = new System.Drawing.Point(16, 68);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(355, 178);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "New Mode and Code";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Select UEI Device Group";
            // 
            // UEIDeviceGroupComboBox
            // 
            this.UEIDeviceGroupComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.UEIDeviceGroupComboBox.FormattingEnabled = true;
            this.UEIDeviceGroupComboBox.Location = new System.Drawing.Point(171, 33);
            this.UEIDeviceGroupComboBox.Name = "UEIDeviceGroupComboBox";
            this.UEIDeviceGroupComboBox.Size = new System.Drawing.Size(167, 21);
            this.UEIDeviceGroupComboBox.TabIndex = 1;
            this.UEIDeviceGroupComboBox.SelectedIndexChanged += new System.EventHandler(this.UEIDeviceGroupComboBox_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PreviousModeBtn);
            this.groupBox2.Controls.Add(this.ViewIDs);
            this.groupBox2.Controls.Add(this.IDLabel);
            this.groupBox2.Controls.Add(this.NextModeBtn);
            this.groupBox2.Controls.Add(this.IDComboBox);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(359, 50);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Original ID";
            // 
            // PreviousModeBtn
            // 
            this.PreviousModeBtn.Enabled = false;
            this.PreviousModeBtn.Location = new System.Drawing.Point(279, 16);
            this.PreviousModeBtn.Name = "PreviousModeBtn";
            this.PreviousModeBtn.Size = new System.Drawing.Size(29, 23);
            this.PreviousModeBtn.TabIndex = 10;
            this.PreviousModeBtn.Text = "<";
            this.PreviousModeBtn.UseVisualStyleBackColor = true;
            this.PreviousModeBtn.Click += new System.EventHandler(this.PreviousModeBtn_Click);
            // 
            // ViewIDs
            // 
            this.ViewIDs.Location = new System.Drawing.Point(188, 16);
            this.ViewIDs.Name = "ViewIDs";
            this.ViewIDs.Size = new System.Drawing.Size(75, 23);
            this.ViewIDs.TabIndex = 9;
            this.ViewIDs.Text = "View IDs";
            this.ViewIDs.UseVisualStyleBackColor = true;
            this.ViewIDs.Click += new System.EventHandler(this.ViewIDs_Click);
            // 
            // IDRenameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelBtn;
            this.ClientSize = new System.Drawing.Size(391, 335);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.OKBtn);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.groupBox2);
            this.Name = "IDRenameForm";
            this.Text = "Update ID";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.IDRenameForm_FormClosed);
            this.Load += new System.EventHandler(this.IDRenameForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button OKBtn;
        private System.Windows.Forms.ComboBox IDComboBox;
        private System.Windows.Forms.Label IDLabel;
        private System.Windows.Forms.Button NextModeBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox UEIDeviceModeComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox UEIDeviceCodeComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox UEIDeviceGroupComboBox;
        private System.Windows.Forms.Button ViewIDs;
        private System.Windows.Forms.Button PreviousModeBtn;
    }
}