namespace DBEditor
{
    partial class IDLevelEditPreviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.functionCollectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.functionCollectionDataGridView = new System.Windows.Forms.DataGridView();
            this.labelSummary = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.TN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Intron = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IntronPriority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Label = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LegacyLabel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Filename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.functionCollectionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.functionCollectionDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // functionCollectionBindingSource
            // 
            this.functionCollectionBindingSource.DataSource = typeof(BusinessObject.FunctionCollection);
            // 
            // functionCollectionDataGridView
            // 
            this.functionCollectionDataGridView.AllowUserToAddRows = false;
            this.functionCollectionDataGridView.AllowUserToDeleteRows = false;
            this.functionCollectionDataGridView.AutoGenerateColumns = false;
            this.functionCollectionDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.functionCollectionDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TN,
            this.Data,
            this.Intron,
            this.IntronPriority,
            this.Label,
            this.LegacyLabel,
            this.Filename,
            this.Comment});
            this.functionCollectionDataGridView.DataSource = this.functionCollectionBindingSource;
            this.functionCollectionDataGridView.Location = new System.Drawing.Point(29, 55);
            this.functionCollectionDataGridView.Name = "functionCollectionDataGridView";
            this.functionCollectionDataGridView.ReadOnly = true;
            this.functionCollectionDataGridView.Size = new System.Drawing.Size(703, 335);
            this.functionCollectionDataGridView.TabIndex = 0;
            // 
            // labelSummary
            // 
            this.labelSummary.AutoSize = true;
            this.labelSummary.Location = new System.Drawing.Point(28, 23);
            this.labelSummary.Name = "labelSummary";
            this.labelSummary.Size = new System.Drawing.Size(71, 13);
            this.labelSummary.TabIndex = 1;
            this.labelSummary.Text = "Edit Summary";
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(347, 412);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // TN
            // 
            this.TN.DataPropertyName = "TN";
            this.TN.HeaderText = "TN";
            this.TN.Name = "TN";
            this.TN.ReadOnly = true;
            this.TN.Width = 50;
            // 
            // Data
            // 
            this.Data.DataPropertyName = "Data";
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            // 
            // Intron
            // 
            this.Intron.DataPropertyName = "Intron";
            this.Intron.HeaderText = "Intron";
            this.Intron.Name = "Intron";
            this.Intron.ReadOnly = true;
            this.Intron.Width = 50;
            // 
            // IntronPriority
            // 
            this.IntronPriority.DataPropertyName = "IntronPriority";
            this.IntronPriority.HeaderText = "Priority";
            this.IntronPriority.Name = "IntronPriority";
            this.IntronPriority.ReadOnly = true;
            this.IntronPriority.Width = 50;
            // 
            // Label
            // 
            this.Label.DataPropertyName = "Label";
            this.Label.HeaderText = "Label";
            this.Label.Name = "Label";
            this.Label.ReadOnly = true;
            // 
            // LegacyLabel
            // 
            this.LegacyLabel.DataPropertyName = "LegacyLabel";
            this.LegacyLabel.HeaderText = "LegacyLabel";
            this.LegacyLabel.Name = "LegacyLabel";
            this.LegacyLabel.ReadOnly = true;
            // 
            // Filename
            // 
            this.Filename.DataPropertyName = "Filename";
            this.Filename.HeaderText = "Filename";
            this.Filename.Name = "Filename";
            this.Filename.ReadOnly = true;
            this.Filename.Width = 80;
            // 
            // Comment
            // 
            this.Comment.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Comment.DataPropertyName = "Comment";
            this.Comment.HeaderText = "Comment";
            this.Comment.Name = "Comment";
            this.Comment.ReadOnly = true;
            // 
            // IDLevelEditPreviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 447);
            this.ControlBox = false;
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.labelSummary);
            this.Controls.Add(this.functionCollectionDataGridView);
            this.Name = "IDLevelEditPreviewForm";
            this.Text = "Preview";
            ((System.ComponentModel.ISupportInitialize)(this.functionCollectionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.functionCollectionDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource functionCollectionBindingSource;
        private System.Windows.Forms.DataGridView functionCollectionDataGridView;
        private System.Windows.Forms.Label labelSummary;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.DataGridViewTextBoxColumn TN;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Intron;
        private System.Windows.Forms.DataGridViewTextBoxColumn IntronPriority;
        private System.Windows.Forms.DataGridViewTextBoxColumn Label;
        private System.Windows.Forms.DataGridViewTextBoxColumn LegacyLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Filename;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comment;

    }
}