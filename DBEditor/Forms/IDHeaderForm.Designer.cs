namespace DBEditor
{
    partial class IDHeaderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label executor_CodeLabel;
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label parent_IDLabel;
            System.Windows.Forms.Label statusLabel;
            System.Windows.Forms.Label textLabel;
            System.Windows.Forms.Label isExternalPrefixLabel;
            System.Windows.Forms.Label isFrequencyDataLabel;
            System.Windows.Forms.Label isInversedDataLabel;
            System.Windows.Forms.Label isRestrictedLabel;
            System.Windows.Forms.Label isInversedPrefixLabel;
            System.Windows.Forms.Label isHexFormatLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.executor_CodeTextBox = new System.Windows.Forms.TextBox();
            this.iDHeaderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.iDTextBox = new System.Windows.Forms.TextBox();
            this.parent_IDTextBox = new System.Windows.Forms.TextBox();
            this.statusTextBox = new System.Windows.Forms.TextBox();
            this.textTextBox = new System.Windows.Forms.TextBox();
            this.prefixListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.prefixDataGridView = new System.Windows.Forms.DataGridView();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isExternalPrefixComboBox = new System.Windows.Forms.ComboBox();
            this.isFrequencyDataComboBox = new System.Windows.Forms.ComboBox();
            this.isInversedDataComboBox = new System.Windows.Forms.ComboBox();
            this.isRestrictedComboBox = new System.Windows.Forms.ComboBox();
            this.isInversedPrefixComboBox = new System.Windows.Forms.ComboBox();
            this.isHexFormatComboBox = new System.Windows.Forms.ComboBox();
            this.LogCommentTextBox = new System.Windows.Forms.TextBox();
            this.Loglabel = new System.Windows.Forms.Label();
            this.StatusQ = new System.Windows.Forms.RadioButton();
            this.StatusM = new System.Windows.Forms.RadioButton();
            this.StatusE = new System.Windows.Forms.RadioButton();
            this.StatusP = new System.Windows.Forms.RadioButton();
            this.StatusBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            executor_CodeLabel = new System.Windows.Forms.Label();
            iDLabel = new System.Windows.Forms.Label();
            parent_IDLabel = new System.Windows.Forms.Label();
            statusLabel = new System.Windows.Forms.Label();
            textLabel = new System.Windows.Forms.Label();
            isExternalPrefixLabel = new System.Windows.Forms.Label();
            isFrequencyDataLabel = new System.Windows.Forms.Label();
            isInversedDataLabel = new System.Windows.Forms.Label();
            isRestrictedLabel = new System.Windows.Forms.Label();
            isInversedPrefixLabel = new System.Windows.Forms.Label();
            isHexFormatLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.iDHeaderBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prefixListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prefixDataGridView)).BeginInit();
            this.StatusBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // executor_CodeLabel
            // 
            executor_CodeLabel.AutoSize = true;
            executor_CodeLabel.Location = new System.Drawing.Point(22, 110);
            executor_CodeLabel.Name = "executor_CodeLabel";
            executor_CodeLabel.Size = new System.Drawing.Size(52, 13);
            executor_CodeLabel.TabIndex = 26;
            executor_CodeLabel.Text = "Executor:";
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Location = new System.Drawing.Point(22, 43);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(21, 13);
            iDLabel.TabIndex = 28;
            iDLabel.Text = "ID:";
            // 
            // parent_IDLabel
            // 
            parent_IDLabel.AutoSize = true;
            parent_IDLabel.Location = new System.Drawing.Point(22, 79);
            parent_IDLabel.Name = "parent_IDLabel";
            parent_IDLabel.Size = new System.Drawing.Size(55, 13);
            parent_IDLabel.TabIndex = 42;
            parent_IDLabel.Text = "Parent ID:";
            // 
            // statusLabel
            // 
            statusLabel.AutoSize = true;
            statusLabel.Location = new System.Drawing.Point(22, 145);
            statusLabel.Name = "statusLabel";
            statusLabel.Size = new System.Drawing.Size(40, 13);
            statusLabel.TabIndex = 44;
            statusLabel.Text = "Status:";
            statusLabel.Visible = false;
            // 
            // textLabel
            // 
            textLabel.AutoSize = true;
            textLabel.Location = new System.Drawing.Point(22, 179);
            textLabel.Name = "textLabel";
            textLabel.Size = new System.Drawing.Size(31, 13);
            textLabel.TabIndex = 46;
            textLabel.Text = "Text:";
            // 
            // isExternalPrefixLabel
            // 
            isExternalPrefixLabel.AutoSize = true;
            isExternalPrefixLabel.Location = new System.Drawing.Point(307, 74);
            isExternalPrefixLabel.Name = "isExternalPrefixLabel";
            isExternalPrefixLabel.Size = new System.Drawing.Size(77, 13);
            isExternalPrefixLabel.TabIndex = 47;
            isExternalPrefixLabel.Text = "External Prefix:";
            // 
            // isFrequencyDataLabel
            // 
            isFrequencyDataLabel.AutoSize = true;
            isFrequencyDataLabel.Location = new System.Drawing.Point(307, 143);
            isFrequencyDataLabel.Name = "isFrequencyDataLabel";
            isFrequencyDataLabel.Size = new System.Drawing.Size(86, 13);
            isFrequencyDataLabel.TabIndex = 48;
            isFrequencyDataLabel.Text = "Frequency Data:";
            // 
            // isInversedDataLabel
            // 
            isInversedDataLabel.AutoSize = true;
            isInversedDataLabel.Location = new System.Drawing.Point(307, 40);
            isInversedDataLabel.Name = "isInversedDataLabel";
            isInversedDataLabel.Size = new System.Drawing.Size(77, 13);
            isInversedDataLabel.TabIndex = 49;
            isInversedDataLabel.Text = "Inversed Data:";
            // 
            // isRestrictedLabel
            // 
            isRestrictedLabel.AutoSize = true;
            isRestrictedLabel.Location = new System.Drawing.Point(307, 109);
            isRestrictedLabel.Name = "isRestrictedLabel";
            isRestrictedLabel.Size = new System.Drawing.Size(58, 13);
            isRestrictedLabel.TabIndex = 50;
            isRestrictedLabel.Text = "Restricted:";
            // 
            // isInversedPrefixLabel
            // 
            isInversedPrefixLabel.AutoSize = true;
            isInversedPrefixLabel.Location = new System.Drawing.Point(307, 179);
            isInversedPrefixLabel.Name = "isInversedPrefixLabel";
            isInversedPrefixLabel.Size = new System.Drawing.Size(80, 13);
            isInversedPrefixLabel.TabIndex = 51;
            isInversedPrefixLabel.Text = "Inversed Prefix:";
            // 
            // isHexFormatLabel
            // 
            isHexFormatLabel.AutoSize = true;
            isHexFormatLabel.Location = new System.Drawing.Point(307, 215);
            isHexFormatLabel.Name = "isHexFormatLabel";
            isHexFormatLabel.Size = new System.Drawing.Size(64, 13);
            isHexFormatLabel.TabIndex = 52;
            isHexFormatLabel.Text = "Hex Format:";
            // 
            // executor_CodeTextBox
            // 
            this.executor_CodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "Executor_Code", true));
            this.executor_CodeTextBox.Location = new System.Drawing.Point(122, 107);
            this.executor_CodeTextBox.Name = "executor_CodeTextBox";
            this.executor_CodeTextBox.Size = new System.Drawing.Size(104, 20);
            this.executor_CodeTextBox.TabIndex = 27;
            // 
            // iDHeaderBindingSource
            // 
            this.iDHeaderBindingSource.DataSource = typeof(BusinessObject.IDHeader);
            // 
            // iDTextBox
            // 
            this.iDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "ID", true));
            this.iDTextBox.Location = new System.Drawing.Point(122, 40);
            this.iDTextBox.Name = "iDTextBox";
            this.iDTextBox.Size = new System.Drawing.Size(104, 20);
            this.iDTextBox.TabIndex = 29;
            // 
            // parent_IDTextBox
            // 
            this.parent_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "Parent_ID", true));
            this.parent_IDTextBox.Location = new System.Drawing.Point(122, 76);
            this.parent_IDTextBox.Name = "parent_IDTextBox";
            this.parent_IDTextBox.Size = new System.Drawing.Size(104, 20);
            this.parent_IDTextBox.TabIndex = 43;
            // 
            // statusTextBox
            // 
            this.statusTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "Status", true));
            this.statusTextBox.Location = new System.Drawing.Point(122, 142);
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.ReadOnly = true;
            this.statusTextBox.Size = new System.Drawing.Size(104, 20);
            this.statusTextBox.TabIndex = 45;
            this.statusTextBox.Visible = false;
            // 
            // textTextBox
            // 
            this.textTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "Text", true));
            this.textTextBox.Location = new System.Drawing.Point(122, 176);
            this.textTextBox.Name = "textTextBox";
            this.textTextBox.Size = new System.Drawing.Size(104, 20);
            this.textTextBox.TabIndex = 47;
            // 
            // prefixListBindingSource
            // 
            this.prefixListBindingSource.DataMember = "PrefixList";
            this.prefixListBindingSource.DataSource = this.iDHeaderBindingSource;
            // 
            // prefixDataGridView
            // 
            this.prefixDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.prefixDataGridView.AutoGenerateColumns = false;
            this.prefixDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.prefixDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Data,
            this.Description});
            this.prefixDataGridView.DataSource = this.prefixListBindingSource;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.FormatProvider = new System.Globalization.CultureInfo("en-US");
            dataGridViewCellStyle1.NullValue = "null";
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.prefixDataGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this.prefixDataGridView.Location = new System.Drawing.Point(25, 306);
            this.prefixDataGridView.Name = "prefixDataGridView";
            this.prefixDataGridView.Size = new System.Drawing.Size(605, 258);
            this.prefixDataGridView.TabIndex = 47;
            this.prefixDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.prefixDataGridView_DataError);
            // 
            // Data
            // 
            this.Data.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Data.DataPropertyName = "Data";
            this.Data.HeaderText = "Prefix";
            this.Data.Name = "Data";
            this.Data.Width = 150;
            // 
            // Description
            // 
            this.Description.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Description.DataPropertyName = "Description";
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            // 
            // isExternalPrefixComboBox
            // 
            this.isExternalPrefixComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "IsExternalPrefix", true));
            this.isExternalPrefixComboBox.FormattingEnabled = true;
            this.isExternalPrefixComboBox.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.isExternalPrefixComboBox.Location = new System.Drawing.Point(410, 74);
            this.isExternalPrefixComboBox.Name = "isExternalPrefixComboBox";
            this.isExternalPrefixComboBox.Size = new System.Drawing.Size(44, 21);
            this.isExternalPrefixComboBox.TabIndex = 48;
            // 
            // isFrequencyDataComboBox
            // 
            this.isFrequencyDataComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "IsFrequencyData", true));
            this.isFrequencyDataComboBox.FormattingEnabled = true;
            this.isFrequencyDataComboBox.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.isFrequencyDataComboBox.Location = new System.Drawing.Point(410, 143);
            this.isFrequencyDataComboBox.Name = "isFrequencyDataComboBox";
            this.isFrequencyDataComboBox.Size = new System.Drawing.Size(44, 21);
            this.isFrequencyDataComboBox.TabIndex = 49;
            // 
            // isInversedDataComboBox
            // 
            this.isInversedDataComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "IsInversedData", true));
            this.isInversedDataComboBox.FormattingEnabled = true;
            this.isInversedDataComboBox.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.isInversedDataComboBox.Location = new System.Drawing.Point(410, 40);
            this.isInversedDataComboBox.Name = "isInversedDataComboBox";
            this.isInversedDataComboBox.Size = new System.Drawing.Size(44, 21);
            this.isInversedDataComboBox.TabIndex = 50;
            // 
            // isRestrictedComboBox
            // 
            this.isRestrictedComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "IsRestricted", true));
            this.isRestrictedComboBox.FormattingEnabled = true;
            this.isRestrictedComboBox.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.isRestrictedComboBox.Location = new System.Drawing.Point(410, 109);
            this.isRestrictedComboBox.Name = "isRestrictedComboBox";
            this.isRestrictedComboBox.Size = new System.Drawing.Size(44, 21);
            this.isRestrictedComboBox.TabIndex = 51;
            // 
            // isInversedPrefixComboBox
            // 
            this.isInversedPrefixComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "IsInversedPrefix", true));
            this.isInversedPrefixComboBox.FormattingEnabled = true;
            this.isInversedPrefixComboBox.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.isInversedPrefixComboBox.Location = new System.Drawing.Point(410, 179);
            this.isInversedPrefixComboBox.Name = "isInversedPrefixComboBox";
            this.isInversedPrefixComboBox.Size = new System.Drawing.Size(44, 21);
            this.isInversedPrefixComboBox.TabIndex = 52;
            // 
            // isHexFormatComboBox
            // 
            this.isHexFormatComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iDHeaderBindingSource, "IsHexFormat", true));
            this.isHexFormatComboBox.FormattingEnabled = true;
            this.isHexFormatComboBox.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.isHexFormatComboBox.Location = new System.Drawing.Point(410, 212);
            this.isHexFormatComboBox.Name = "isHexFormatComboBox";
            this.isHexFormatComboBox.Size = new System.Drawing.Size(44, 21);
            this.isHexFormatComboBox.TabIndex = 53;
            // 
            // LogCommentTextBox
            // 
            this.LogCommentTextBox.Location = new System.Drawing.Point(124, 258);
            this.LogCommentTextBox.Name = "LogCommentTextBox";
            this.LogCommentTextBox.Size = new System.Drawing.Size(332, 20);
            this.LogCommentTextBox.TabIndex = 54;
            // 
            // Loglabel
            // 
            this.Loglabel.AutoSize = true;
            this.Loglabel.Location = new System.Drawing.Point(22, 261);
            this.Loglabel.Name = "Loglabel";
            this.Loglabel.Size = new System.Drawing.Size(97, 13);
            this.Loglabel.TabIndex = 55;
            this.Loglabel.Text = "Add Log Comment:";
            // 
            // StatusQ
            // 
            this.StatusQ.AutoSize = true;
            this.StatusQ.Location = new System.Drawing.Point(21, 19);
            this.StatusQ.Name = "StatusQ";
            this.StatusQ.Size = new System.Drawing.Size(78, 17);
            this.StatusQ.TabIndex = 56;
            this.StatusQ.Tag = "Q";
            this.StatusQ.Text = "Passed QA";
            this.StatusQ.CheckedChanged += new System.EventHandler(this.StatusChanged);
            // 
            // StatusM
            // 
            this.StatusM.AutoSize = true;
            this.StatusM.Location = new System.Drawing.Point(21, 50);
            this.StatusM.Name = "StatusM";
            this.StatusM.Size = new System.Drawing.Size(65, 17);
            this.StatusM.TabIndex = 57;
            this.StatusM.Tag = "M";
            this.StatusM.Text = "Modified";
            this.StatusM.CheckedChanged += new System.EventHandler(this.StatusChanged);
            // 
            // StatusE
            // 
            this.StatusE.AutoSize = true;
            this.StatusE.Location = new System.Drawing.Point(21, 81);
            this.StatusE.Name = "StatusE";
            this.StatusE.Size = new System.Drawing.Size(74, 17);
            this.StatusE.TabIndex = 58;
            this.StatusE.Tag = "E";
            this.StatusE.Text = "Exec Hold";
            this.StatusE.CheckedChanged += new System.EventHandler(this.StatusChanged);
            // 
            // StatusP
            // 
            this.StatusP.AutoSize = true;
            this.StatusP.Location = new System.Drawing.Point(21, 112);
            this.StatusP.Name = "StatusP";
            this.StatusP.Size = new System.Drawing.Size(83, 17);
            this.StatusP.TabIndex = 59;
            this.StatusP.Tag = "P";
            this.StatusP.Text = "Project Hold";
            this.StatusP.CheckedChanged += new System.EventHandler(this.StatusChanged);
            // 
            // StatusBox
            // 
            this.StatusBox.Controls.Add(this.StatusQ);
            this.StatusBox.Controls.Add(this.StatusP);
            this.StatusBox.Controls.Add(this.StatusM);
            this.StatusBox.Controls.Add(this.StatusE);
            this.StatusBox.Location = new System.Drawing.Point(509, 40);
            this.StatusBox.Name = "StatusBox";
            this.StatusBox.Size = new System.Drawing.Size(121, 136);
            this.StatusBox.TabIndex = 60;
            this.StatusBox.TabStop = false;
            this.StatusBox.Text = "ID Status";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 288);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 61;
            this.label1.Text = "Prefix Information";
            // 
            // IDHeaderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(654, 576);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.StatusBox);
            this.Controls.Add(this.Loglabel);
            this.Controls.Add(this.LogCommentTextBox);
            this.Controls.Add(isHexFormatLabel);
            this.Controls.Add(this.isHexFormatComboBox);
            this.Controls.Add(isInversedPrefixLabel);
            this.Controls.Add(this.isInversedPrefixComboBox);
            this.Controls.Add(isRestrictedLabel);
            this.Controls.Add(this.isRestrictedComboBox);
            this.Controls.Add(isInversedDataLabel);
            this.Controls.Add(this.isInversedDataComboBox);
            this.Controls.Add(isFrequencyDataLabel);
            this.Controls.Add(this.isFrequencyDataComboBox);
            this.Controls.Add(isExternalPrefixLabel);
            this.Controls.Add(this.isExternalPrefixComboBox);
            this.Controls.Add(this.prefixDataGridView);
            this.Controls.Add(executor_CodeLabel);
            this.Controls.Add(this.executor_CodeTextBox);
            this.Controls.Add(iDLabel);
            this.Controls.Add(this.iDTextBox);
            this.Controls.Add(parent_IDLabel);
            this.Controls.Add(this.parent_IDTextBox);
            this.Controls.Add(statusLabel);
            this.Controls.Add(this.statusTextBox);
            this.Controls.Add(textLabel);
            this.Controls.Add(this.textTextBox);
            this.MinimizeBox = false;
            this.Name = "IDHeaderForm";
            this.Text = "IDHeader";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.iDHeaderBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prefixListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prefixDataGridView)).EndInit();
            this.StatusBox.ResumeLayout(false);
            this.StatusBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox executor_CodeTextBox;
        private System.Windows.Forms.TextBox iDTextBox;
        private System.Windows.Forms.TextBox parent_IDTextBox;
        private System.Windows.Forms.TextBox statusTextBox;
        private System.Windows.Forms.TextBox textTextBox;
        private System.Windows.Forms.BindingSource prefixListBindingSource;
        private System.Windows.Forms.DataGridView prefixDataGridView;
        private System.Windows.Forms.ComboBox isExternalPrefixComboBox;
        private System.Windows.Forms.ComboBox isFrequencyDataComboBox;
        private System.Windows.Forms.ComboBox isInversedDataComboBox;
        private System.Windows.Forms.ComboBox isRestrictedComboBox;
        private System.Windows.Forms.ComboBox isInversedPrefixComboBox;
        private System.Windows.Forms.ComboBox isHexFormatComboBox;
        private System.Windows.Forms.Label Loglabel;
        public System.Windows.Forms.TextBox LogCommentTextBox;
        private System.Windows.Forms.BindingSource iDHeaderBindingSource;
        private System.Windows.Forms.RadioButton StatusQ;
        private System.Windows.Forms.RadioButton StatusM;
        private System.Windows.Forms.RadioButton StatusE;
        private System.Windows.Forms.RadioButton StatusP;
        private System.Windows.Forms.GroupBox StatusBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.Label label1;

    }
}

