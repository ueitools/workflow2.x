namespace DBEditor
{
    partial class FunctionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.functionCollectionDataGridView = new System.Windows.Forms.DataGridView();
            this.Synth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Label = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LegacyLabel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Intron = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IntronPriority = new Microsoft.Samples.Windows.Forms.DataGridViewCustomColumn.MaskedTextBoxColumn();
            this.Filename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.functionCollectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.buttonMap = new System.Windows.Forms.Button();
            this.textBoxLinkedID = new System.Windows.Forms.TextBox();
            this.buttonImport = new System.Windows.Forms.Button();
            this.labelLinkedID = new System.Windows.Forms.Label();
            this.buttonAssignIntron = new System.Windows.Forms.Button();
            this.ExecOverrideBox = new System.Windows.Forms.CheckBox();
            this.ByteCol1 = new System.Windows.Forms.TextBox();
            this.ByteCol2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ColumnGroup = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CopyBtn = new System.Windows.Forms.Button();
            this.InsertBtn = new System.Windows.Forms.Button();
            this.DeleteAllBtn = new System.Windows.Forms.Button();
            this.ButtonBox = new System.Windows.Forms.GroupBox();
            this.ImportBtn = new System.Windows.Forms.Button();
            this.LabelGrpCB = new System.Windows.Forms.ComboBox();
            this.LabelLB = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.functionCollectionDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.functionCollectionBindingSource)).BeginInit();
            this.ColumnGroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.ButtonBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // functionCollectionDataGridView
            // 
            this.functionCollectionDataGridView.AllowDrop = true;
            this.functionCollectionDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.functionCollectionDataGridView.AutoGenerateColumns = false;
            this.functionCollectionDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.functionCollectionDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.functionCollectionDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Synth,
            this.Data,
            this.Label,
            this.LegacyLabel,
            this.Intron,
            this.IntronPriority,
            this.Filename,
            this.ID,
            this.TN,
            this.Comment});
            this.functionCollectionDataGridView.DataSource = this.functionCollectionBindingSource;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.FormatProvider = new System.Globalization.CultureInfo("en-US");
            dataGridViewCellStyle3.NullValue = "null";
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.functionCollectionDataGridView.DefaultCellStyle = dataGridViewCellStyle3;
            this.functionCollectionDataGridView.Location = new System.Drawing.Point(-1, 114);
            this.functionCollectionDataGridView.Name = "functionCollectionDataGridView";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.functionCollectionDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.functionCollectionDataGridView.Size = new System.Drawing.Size(1193, 462);
            this.functionCollectionDataGridView.TabIndex = 1;
            this.functionCollectionDataGridView.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.functionCollectionDataGridView_UserDeletingRow);
            this.functionCollectionDataGridView.Sorted += new System.EventHandler(this.functionCollectionDataGridView_Sorted);
            this.functionCollectionDataGridView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.functionCollectionDataGridView_MouseClick);
            this.functionCollectionDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.functionCollectionDataGridView_CellEndEdit);
            this.functionCollectionDataGridView.DefaultValuesNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.functionCollectionDataGridView_DefaultValuesNeeded);
            this.functionCollectionDataGridView.DragEnter += new System.Windows.Forms.DragEventHandler(this.functionCollectionDataGridView_DragEnter);
            this.functionCollectionDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.functionCollectionDataGridView_KeyDown);
            this.functionCollectionDataGridView.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.functionCollectionDataGridView_RowsRemoved);
            this.functionCollectionDataGridView.DragDrop += new System.Windows.Forms.DragEventHandler(this.functionCollectionDataGridView_DragDrop);
            // 
            // Synth
            // 
            this.Synth.DataPropertyName = "Synth";
            dataGridViewCellStyle2.FormatProvider = new System.Globalization.CultureInfo("en-US");
            dataGridViewCellStyle2.NullValue = "0";
            this.Synth.DefaultCellStyle = dataGridViewCellStyle2;
            this.Synth.HeaderText = "Synth";
            this.Synth.Name = "Synth";
            this.Synth.Width = 40;
            // 
            // Data
            // 
            this.Data.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Data.DataPropertyName = "FormattedData";
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            this.Data.Width = 200;
            // 
            // Label
            // 
            this.Label.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Label.DataPropertyName = "Label";
            this.Label.HeaderText = "Label";
            this.Label.Name = "Label";
            this.Label.Width = 150;
            // 
            // LegacyLabel
            // 
            this.LegacyLabel.DataPropertyName = "LegacyLabel";
            this.LegacyLabel.HeaderText = "LegacyLabel";
            this.LegacyLabel.Name = "LegacyLabel";
            // 
            // Intron
            // 
            this.Intron.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Intron.DataPropertyName = "Intron";
            this.Intron.HeaderText = "Intron";
            this.Intron.Name = "Intron";
            this.Intron.Width = 75;
            // 
            // IntronPriority
            // 
            this.IntronPriority.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IntronPriority.DataPropertyName = "IntronPriority";
            this.IntronPriority.HeaderText = "P";
            this.IntronPriority.IncludeLiterals = false;
            this.IntronPriority.IncludePrompt = false;
            this.IntronPriority.Mask = "0";
            this.IntronPriority.Name = "IntronPriority";
            this.IntronPriority.PromptChar = '\0';
            this.IntronPriority.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IntronPriority.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.IntronPriority.ValidatingType = null;
            this.IntronPriority.Width = 20;
            // 
            // Filename
            // 
            this.Filename.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Filename.DataPropertyName = "Filename";
            this.Filename.HeaderText = "Filename";
            this.Filename.Name = "Filename";
            // 
            // ID
            // 
            this.ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Width = 40;
            // 
            // TN
            // 
            this.TN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TN.DataPropertyName = "TN";
            this.TN.HeaderText = "TN";
            this.TN.MaxInputLength = 6;
            this.TN.Name = "TN";
            // 
            // Comment
            // 
            this.Comment.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Comment.DataPropertyName = "Comment";
            this.Comment.HeaderText = "Comment";
            this.Comment.Name = "Comment";
            // 
            // functionCollectionBindingSource
            // 
            this.functionCollectionBindingSource.DataSource = typeof(BusinessObject.FunctionCollection);
            // 
            // buttonMap
            // 
            this.buttonMap.Location = new System.Drawing.Point(204, 39);
            this.buttonMap.Name = "buttonMap";
            this.buttonMap.Size = new System.Drawing.Size(75, 23);
            this.buttonMap.TabIndex = 2;
            this.buttonMap.Text = "Assign data";
            this.buttonMap.Click += new System.EventHandler(this.buttonMap_Click);
            // 
            // textBoxLinkedID
            // 
            this.textBoxLinkedID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxLinkedID.Location = new System.Drawing.Point(74, 40);
            this.textBoxLinkedID.Name = "textBoxLinkedID";
            this.textBoxLinkedID.Size = new System.Drawing.Size(100, 20);
            this.textBoxLinkedID.TabIndex = 3;
            // 
            // buttonImport
            // 
            this.buttonImport.Location = new System.Drawing.Point(9, 15);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(75, 23);
            this.buttonImport.TabIndex = 4;
            this.buttonImport.Text = "Import Data";
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // labelLinkedID
            // 
            this.labelLinkedID.AutoSize = true;
            this.labelLinkedID.Location = new System.Drawing.Point(12, 46);
            this.labelLinkedID.Name = "labelLinkedID";
            this.labelLinkedID.Size = new System.Drawing.Size(59, 13);
            this.labelLinkedID.TabIndex = 5;
            this.labelLinkedID.Text = "Linked ID :";
            // 
            // buttonAssignIntron
            // 
            this.buttonAssignIntron.Location = new System.Drawing.Point(309, 39);
            this.buttonAssignIntron.Name = "buttonAssignIntron";
            this.buttonAssignIntron.Size = new System.Drawing.Size(81, 23);
            this.buttonAssignIntron.TabIndex = 6;
            this.buttonAssignIntron.Text = "Assign Intron";
            this.buttonAssignIntron.Click += new System.EventHandler(this.buttonAssignIntron_Click);
            // 
            // ExecOverrideBox
            // 
            this.ExecOverrideBox.AutoSize = true;
            this.ExecOverrideBox.Location = new System.Drawing.Point(90, 18);
            this.ExecOverrideBox.Name = "ExecOverrideBox";
            this.ExecOverrideBox.Size = new System.Drawing.Size(93, 17);
            this.ExecOverrideBox.TabIndex = 7;
            this.ExecOverrideBox.Text = "Override Exec";
            this.ExecOverrideBox.UseVisualStyleBackColor = true;
            this.ExecOverrideBox.CheckedChanged += new System.EventHandler(this.ExecOverrideBox_CheckedChanged);
            // 
            // ByteCol1
            // 
            this.ByteCol1.Location = new System.Drawing.Point(43, 18);
            this.ByteCol1.MaxLength = 2;
            this.ByteCol1.Name = "ByteCol1";
            this.ByteCol1.Size = new System.Drawing.Size(34, 20);
            this.ByteCol1.TabIndex = 8;
            // 
            // ByteCol2
            // 
            this.ByteCol2.Location = new System.Drawing.Point(124, 18);
            this.ByteCol2.MaxLength = 2;
            this.ByteCol2.Name = "ByteCol2";
            this.ByteCol2.Size = new System.Drawing.Size(34, 20);
            this.ByteCol2.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Col 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(87, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Col 2";
            // 
            // ColumnGroup
            // 
            this.ColumnGroup.Controls.Add(this.label2);
            this.ColumnGroup.Controls.Add(this.label1);
            this.ColumnGroup.Controls.Add(this.ByteCol2);
            this.ColumnGroup.Controls.Add(this.ByteCol1);
            this.ColumnGroup.Location = new System.Drawing.Point(606, 24);
            this.ColumnGroup.Name = "ColumnGroup";
            this.ColumnGroup.Size = new System.Drawing.Size(170, 53);
            this.ColumnGroup.TabIndex = 12;
            this.ColumnGroup.TabStop = false;
            this.ColumnGroup.Text = "Import Columns";
            this.ColumnGroup.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ExecOverrideBox);
            this.groupBox1.Controls.Add(this.buttonImport);
            this.groupBox1.Location = new System.Drawing.Point(411, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(189, 53);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            // 
            // CopyBtn
            // 
            this.CopyBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CopyBtn.Location = new System.Drawing.Point(98, 15);
            this.CopyBtn.Name = "CopyBtn";
            this.CopyBtn.Size = new System.Drawing.Size(67, 23);
            this.CopyBtn.TabIndex = 14;
            this.CopyBtn.Text = "Copy";
            this.CopyBtn.UseVisualStyleBackColor = true;
            this.CopyBtn.Click += new System.EventHandler(this.CopyBtn_Click);
            // 
            // InsertBtn
            // 
            this.InsertBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.InsertBtn.Location = new System.Drawing.Point(171, 15);
            this.InsertBtn.Name = "InsertBtn";
            this.InsertBtn.Size = new System.Drawing.Size(67, 23);
            this.InsertBtn.TabIndex = 16;
            this.InsertBtn.Text = "Insert";
            this.InsertBtn.UseVisualStyleBackColor = true;
            this.InsertBtn.Click += new System.EventHandler(this.InsertBtn_Click);
            // 
            // DeleteAllBtn
            // 
            this.DeleteAllBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DeleteAllBtn.Location = new System.Drawing.Point(244, 15);
            this.DeleteAllBtn.Name = "DeleteAllBtn";
            this.DeleteAllBtn.Size = new System.Drawing.Size(67, 23);
            this.DeleteAllBtn.TabIndex = 18;
            this.DeleteAllBtn.Text = "Delete All";
            this.DeleteAllBtn.UseVisualStyleBackColor = true;
            this.DeleteAllBtn.Click += new System.EventHandler(this.DeleteAllBtn_Click);
            // 
            // ButtonBox
            // 
            this.ButtonBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonBox.Controls.Add(this.ImportBtn);
            this.ButtonBox.Controls.Add(this.CopyBtn);
            this.ButtonBox.Controls.Add(this.InsertBtn);
            this.ButtonBox.Controls.Add(this.DeleteAllBtn);
            this.ButtonBox.Location = new System.Drawing.Point(850, 24);
            this.ButtonBox.Name = "ButtonBox";
            this.ButtonBox.Size = new System.Drawing.Size(329, 53);
            this.ButtonBox.TabIndex = 19;
            this.ButtonBox.TabStop = false;
            this.ButtonBox.Visible = false;
            // 
            // ImportBtn
            // 
            this.ImportBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ImportBtn.Location = new System.Drawing.Point(17, 15);
            this.ImportBtn.Name = "ImportBtn";
            this.ImportBtn.Size = new System.Drawing.Size(75, 23);
            this.ImportBtn.TabIndex = 19;
            this.ImportBtn.Text = "Import";
            this.ImportBtn.UseVisualStyleBackColor = true;
            this.ImportBtn.Click += new System.EventHandler(this.ImportBtn_Click);
            // 
            // LabelGrpCB
            // 
            this.LabelGrpCB.FormattingEnabled = true;
            this.LabelGrpCB.Location = new System.Drawing.Point(0, 114);
            this.LabelGrpCB.Name = "LabelGrpCB";
            this.LabelGrpCB.Size = new System.Drawing.Size(140, 21);
            this.LabelGrpCB.TabIndex = 21;
            this.LabelGrpCB.Visible = false;
            this.LabelGrpCB.SelectedIndexChanged += new System.EventHandler(this.LabelGrpCB_SelectedIndexChanged);
            // 
            // LabelLB
            // 
            this.LabelLB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.LabelLB.FormattingEnabled = true;
            this.LabelLB.IntegralHeight = false;
            this.LabelLB.Location = new System.Drawing.Point(0, 133);
            this.LabelLB.Name = "LabelLB";
            this.LabelLB.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.LabelLB.Size = new System.Drawing.Size(140, 443);
            this.LabelLB.TabIndex = 20;
            this.LabelLB.Visible = false;
            this.LabelLB.DoubleClick += new System.EventHandler(this.LabelLB_DoubleClick);
            this.LabelLB.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LabelLB_MouseMove);
            this.LabelLB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LabelLB_MouseDown);
            // 
            // FunctionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1191, 576);
            this.ControlBox = false;
            this.Controls.Add(this.ButtonBox);
            this.Controls.Add(this.LabelGrpCB);
            this.Controls.Add(this.LabelLB);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ColumnGroup);
            this.Controls.Add(this.buttonAssignIntron);
            this.Controls.Add(this.labelLinkedID);
            this.Controls.Add(this.textBoxLinkedID);
            this.Controls.Add(this.buttonMap);
            this.Controls.Add(this.functionCollectionDataGridView);
            this.MinimizeBox = false;
            this.Name = "FunctionForm";
            this.Text = "Function";
            this.Load += new System.EventHandler(this.FunctionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.functionCollectionDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.functionCollectionBindingSource)).EndInit();
            this.ColumnGroup.ResumeLayout(false);
            this.ColumnGroup.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ButtonBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView functionCollectionDataGridView;
        private System.Windows.Forms.Button buttonMap;
        private System.Windows.Forms.TextBox textBoxLinkedID;
        private System.Windows.Forms.Button buttonImport;
        private System.Windows.Forms.Label labelLinkedID;
        private System.Windows.Forms.Button buttonAssignIntron;
        private System.Windows.Forms.BindingSource functionCollectionBindingSource;
        private System.Windows.Forms.CheckBox ExecOverrideBox;
        private System.Windows.Forms.TextBox ByteCol1;
        private System.Windows.Forms.TextBox ByteCol2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox ColumnGroup;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Synth;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Label;
        private System.Windows.Forms.DataGridViewTextBoxColumn LegacyLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Intron;
        private Microsoft.Samples.Windows.Forms.DataGridViewCustomColumn.MaskedTextBoxColumn IntronPriority;
        private System.Windows.Forms.DataGridViewTextBoxColumn Filename;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn TN;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comment;
        private System.Windows.Forms.Button CopyBtn;
        private System.Windows.Forms.Button InsertBtn;
        private System.Windows.Forms.Button DeleteAllBtn;
        private System.Windows.Forms.GroupBox ButtonBox;
        private System.Windows.Forms.ComboBox LabelGrpCB;
        private System.Windows.Forms.ListBox LabelLB;
        private System.Windows.Forms.Button ImportBtn;
    }
}