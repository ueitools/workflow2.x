using System;
using System.Windows.Forms;

namespace DBEditor
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            string domain = SystemInformation.UserDomainName;
            if (domain != "UEIC") {
                updateIDNameToolStripMenuItem.Enabled = false;
                unLockTNToolStripMenuItem.Enabled = false;
                unLockPartialIDToolStripMenuItem.Enabled = false;
                dataToolStripMenuItem.Enabled = false;
                intronToolStripMenuItem.Enabled = false;
                labelToolStripMenuItem.Enabled = false;
            }
        }

        private void tNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DBEditorWrapper.EditTN();
        }

        private void iDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DBEditorWrapper.EditPartialID();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void intronToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            DBEditorWrapper.EditIntron();
        }

        private void dataToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            DBEditorWrapper.EditData();
        }

        private void labelToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            DBEditorWrapper.EditLabel();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void unLockTNToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            DBEditorWrapper.UnLockTN();
        }

        private void unLockPartialIDToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            DBEditorWrapper.UnLockPartialID();
        }

        private void updateIDNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DBEditorWrapper.RenameID();
        }

        private void logNewRemoteToolStripMenuItem_Click(object sender, EventArgs e) {
            DBEditorWrapper.AddNewRemote();
        }
    }
}