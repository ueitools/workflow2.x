namespace DBEditor
{
    partial class PrintOptionDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.chkBoxFunc = new System.Windows.Forms.CheckBox();
            this.ckhBoxDevice = new System.Windows.Forms.CheckBox();
            this.chkHead = new System.Windows.Forms.CheckBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pages to Print:";
            // 
            // chkBoxFunc
            // 
            this.chkBoxFunc.AutoSize = true;
            this.chkBoxFunc.Checked = true;
            this.chkBoxFunc.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBoxFunc.Location = new System.Drawing.Point(13, 46);
            this.chkBoxFunc.Name = "chkBoxFunc";
            this.chkBoxFunc.Size = new System.Drawing.Size(67, 17);
            this.chkBoxFunc.TabIndex = 1;
            this.chkBoxFunc.Text = "Function";
            this.chkBoxFunc.UseVisualStyleBackColor = true;
            // 
            // ckhBoxDevice
            // 
            this.ckhBoxDevice.AutoSize = true;
            this.ckhBoxDevice.Checked = true;
            this.ckhBoxDevice.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckhBoxDevice.Location = new System.Drawing.Point(13, 69);
            this.ckhBoxDevice.Name = "ckhBoxDevice";
            this.ckhBoxDevice.Size = new System.Drawing.Size(60, 17);
            this.ckhBoxDevice.TabIndex = 2;
            this.ckhBoxDevice.Text = "Device";
            this.ckhBoxDevice.UseVisualStyleBackColor = true;
            // 
            // chkHead
            // 
            this.chkHead.AutoSize = true;
            this.chkHead.Checked = true;
            this.chkHead.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkHead.Location = new System.Drawing.Point(13, 93);
            this.chkHead.Name = "chkHead";
            this.chkHead.Size = new System.Drawing.Size(61, 17);
            this.chkHead.TabIndex = 3;
            this.chkHead.Text = "Header";
            this.chkHead.UseVisualStyleBackColor = true;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(179, 31);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 4;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(179, 78);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // TNPrintOptionDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 130);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.chkHead);
            this.Controls.Add(this.ckhBoxDevice);
            this.Controls.Add(this.chkBoxFunc);
            this.Controls.Add(this.label1);
            this.Name = "TNPrintOptionDlg";
            this.Text = "PrintOptionDlg";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkBoxFunc;
        private System.Windows.Forms.CheckBox ckhBoxDevice;
        private System.Windows.Forms.CheckBox chkHead;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
    }
}