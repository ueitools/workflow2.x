using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using DBTransfer;

namespace DBEditor
{
    public partial class IDRenameForm : Form
    {
        string oldDBConnection = null;

        public IDRenameForm()
        {
            InitializeComponent();
        }

        private void IDRenameForm_Load(object sender, EventArgs e)
        {
            oldDBConnection = DAOFactory.GetDBConnectionString();
            UEIDeviceGroupComboBox.DataSource =
                DBFunctions.GetDeviceGroupList();
            IDComboBox.TabStop = true;
            IDComboBox.TabIndex = 1;
            IDComboBox.Focus(); // attempt to set focus to this combo box, fix this later
        }

        /// <summary>
        /// Load a list of ALL IDs in the database into the IDComboBox
        /// </summary>
        private void GetIDList(string usingID)
        {
            DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
            StringCollection list = DBFunctions.NonBlockingGetAllIDList();
            IDComboBox.DataSource = list;

            int index = IDComboBox.FindString(usingID);
            if (index >= 0)
                IDComboBox.SelectedIndex = index;
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            string mode = UEIDeviceModeComboBox.Text;
            int code = Int16.Parse(UEIDeviceCodeComboBox.Text);
            String destination = String.Format("{0}{1:D4}", mode, code);

            if (VerifyUserInfo(destination) != true)
                return;
            try
            {
                DAOFactory.BeginTransaction();            
                DAOFactory.IDHeader().UpdateName(IDComboBox.Text, destination);                
                DAOFactory.CommitTransaction();                
                Close();
            }
            catch (Exception ex)
            {
                DAOFactory.RollBackTransaction();
                MessageBox.Show(ex.Message);
            }
        }

        private void NextModeBtn_Click(object sender, EventArgs e)
        {
            StringCollection IDArray = (StringCollection)IDComboBox.DataSource;
            char Mode = IDArray[0].ToString()[0];

            if (IDComboBox.Text.ToString()!="")
                Mode = IDComboBox.Text.ToString()[0];

            int index = IDComboBox.SelectedIndex;
            while (index < IDComboBox.Items.Count - 1)
            {
                if (Mode != IDArray[++index].ToString()[0])
                    break;
            }
            if (index == IDComboBox.Items.Count - 1)
                index = 0;

            IDComboBox.SelectedIndex = index;            
        }

        /// <summary>
        /// Get Available Code info from Public Database
        /// </summary>
        /// <param name="deviceGroup"></param>
        private void BindModeNCode(string deviceGroup)
        {
            UEIDeviceModeComboBox.DataSource =
                DBFunctions.GetModeListForDeviceGroup(deviceGroup); ;

            DataStore publicDS = new DataStore(DBConnectionString.UEIPUBLIC);
            UEIDeviceCodeComboBox.DataSource =
                publicDS.GetAvailableCodeList(deviceGroup);
        }

        private void UEIDeviceGroupComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindModeNCode(UEIDeviceGroupComboBox.Text);
        }

        private void IDComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLower(e.KeyChar))
                e.KeyChar = Char.ToUpper(e.KeyChar);
            else if (!Char.IsLetterOrDigit(e.KeyChar) && e.KeyChar!=(char)Keys.Back)
                e.Handled = true;
        }

        private bool VerifyUserInfo(string destination)
        {
            bool checkID = false;

            if (IDComboBox.Items.Count > 0)
            {
                foreach (String item in IDComboBox.Items)
                    if (item == IDComboBox.Text)
                        checkID = true;
            }
            if (IDComboBox.Items.Count > 0 && checkID == false)
            {
                MessageBox.Show("Invalid original ID. Please verify ID was set properly.");
                return false;
            }
            String msg = "This will rename " + IDComboBox.Text + " to " + destination + ". Are you sure?";

            DialogResult result = MessageBox.Show(msg, "Are you sure", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
                return false;

            return true;
        }

        private void ViewIDs_Click(object sender, EventArgs e)
        {
            GetIDList(IDComboBox.Text);
            NextModeBtn.Enabled = true;
            PreviousModeBtn.Enabled = true;
        }

        private void PreviousModeBtn_Click(object sender, EventArgs e)
        {
            StringCollection IDArray = (StringCollection)IDComboBox.DataSource;
            char Mode = IDArray[0].ToString()[0];

            if (IDComboBox.Text.ToString() != "")
                Mode = IDComboBox.Text.ToString()[0];

            int index = IDComboBox.SelectedIndex;
            while (index >= 0)
            {
                if (Mode != IDArray[index].ToString()[0])
                    break;
                --index;
            }
            if (index == -1)
                index = IDComboBox.Items.Count - 1;

            IDComboBox.SelectedIndex = index;
        }

        private void IDRenameForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            DAOFactory.ResetDBConnection(oldDBConnection);
        }
    }
}