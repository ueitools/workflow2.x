namespace DBEditor
{
    partial class ReleaseLockForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxValue = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelValue = new System.Windows.Forms.Label();
            this.MainDbOption = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // textBoxValue
            // 
            this.textBoxValue.AcceptsReturn = true;
            this.textBoxValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxValue.Location = new System.Drawing.Point(12, 61);
            this.textBoxValue.Name = "textBoxValue";
            this.textBoxValue.Size = new System.Drawing.Size(156, 20);
            this.textBoxValue.TabIndex = 0;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(205, 59);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 1;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(205, 93);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelValue
            // 
            this.labelValue.AutoSize = true;
            this.labelValue.Location = new System.Drawing.Point(12, 32);
            this.labelValue.Name = "labelValue";
            this.labelValue.Size = new System.Drawing.Size(32, 13);
            this.labelValue.TabIndex = 3;
            this.labelValue.Text = "Enter";
            // 
            // MainDbOption
            // 
            this.MainDbOption.AutoSize = true;
            this.MainDbOption.Location = new System.Drawing.Point(12, 97);
            this.MainDbOption.Name = "MainDbOption";
            this.MainDbOption.Size = new System.Drawing.Size(104, 17);
            this.MainDbOption.TabIndex = 4;
            this.MainDbOption.Text = "Unlock Main DB";
            this.MainDbOption.UseVisualStyleBackColor = true;
            this.MainDbOption.Visible = false;
            // 
            // ReleaseLockForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 127);
            this.Controls.Add(this.MainDbOption);
            this.Controls.Add(this.labelValue);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.textBoxValue);
            this.Name = "ReleaseLockForm";
            this.Text = "Enter ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxValue;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelValue;
        private System.Windows.Forms.CheckBox MainDbOption;
    }
}