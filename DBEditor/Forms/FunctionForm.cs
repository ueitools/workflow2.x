using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using EntryHelper;
using Microsoft.Win32;
using BusinessObject;

namespace DBEditor
{
    public partial class FunctionForm : Form
    {
        public KeyLists _keys;
        private int LastLinePrinted;            // for keeping track of lines printed
        private int _tnNumber = 0;
        
        public void resetLastLinePrinted()
        {
            LastLinePrinted = 0;
        }
        
        public FunctionForm()
        {
            InitializeComponent();
            SetupCaptureMode(false);
        }

        private void FunctionForm_Load(object sender, EventArgs e)
        {
            if (_keys != null)
                _keys.FillGroupSelectionComboBox(LabelGrpCB);
            SetupCaptureMode(false);
        }

        public void CaptModeBtn_CheckedChanged(object sender, EventArgs e)
        {
            SetupCaptureMode(((ToolStripButton)sender).Checked);
        }

        private void SetupCaptureMode(bool CaptMode)
        {
            int left = (!CaptMode) ? 0 : LabelLB.Width;
            int top = functionCollectionDataGridView.Top;
            functionCollectionDataGridView.Left = left;
            functionCollectionDataGridView.Width = ClientRectangle.Width - left;
            functionCollectionDataGridView.Height = ClientRectangle.Height - top;

            LabelGrpCB.Visible = CaptMode;
            LabelLB.Visible = CaptMode;
            ButtonBox.Visible = CaptMode;
        }

        public void Bind(FunctionCollection functionList)
        {
            functionCollectionBindingSource.DataSource = functionList;

            functionCollectionDataGridView.Sort(functionCollectionDataGridView.Columns["Filename"],
                                                  ListSortDirection.Ascending);
            ColumnGroup.Visible = false;
        }

        public void SetRowColorsByID()
        {
            string cur_id = "";
            Color[] colors = { Color.Aquamarine, Color.Khaki, 
                Color.Aqua, Color.Lime, Color.SlateBlue,Color.Beige,
                Color.Gold, Color.Violet, Color.LemonChiffon,
                Color.MistyRose, Color.Peru, Color.SeaShell,Color.Silver,
                Color.Olive, Color.Orange, Color.Ivory, Color.OliveDrab,
                Color.DarkRed, Color.DarkSalmon, Color.DarkGray};
            int cur_color_ind = 0;

             // Set background to a known state.
            foreach (DataGridViewRow row in 
                functionCollectionDataGridView.Rows)
                row.DefaultCellStyle.BackColor = Color.White;

            foreach (DataGridViewRow irow in 
                functionCollectionDataGridView.Rows)
            {
                if (irow.Cells["ID"].Value == null || 
                    irow.DefaultCellStyle.BackColor != Color.White)
                    continue;
                irow.DefaultCellStyle.BackColor = colors[cur_color_ind];
                cur_id = irow.Cells["ID"].Value.ToString();

                foreach (DataGridViewRow jrow in 
                    functionCollectionDataGridView.Rows)
                {
                    if (irow == jrow || (jrow.Cells["ID"].Value == null))
                        continue;

                    if (jrow.Cells["ID"].Value.ToString() == cur_id)
                        jrow.DefaultCellStyle.BackColor = colors[cur_color_ind];
                }

                if (cur_color_ind < 19)
                    cur_color_ind++;
            }
            if (cur_color_ind == 1)
            {
                foreach (DataGridViewRow row in 
                    functionCollectionDataGridView.Rows)
                    row.DefaultCellStyle.BackColor = Color.White;
            }
        }

        public void ShowIDColumn(int tn)
        {
            ButtonBox.Show();
            _tnNumber = tn;
            functionCollectionDataGridView.Columns["TN"].Visible = false;
            functionCollectionDataGridView.Columns["ID"].Visible = true;
        }

        public void ShowTNColumn()
        {
            functionCollectionDataGridView.Columns["TN"].Visible = true;
            functionCollectionDataGridView.Columns["ID"].Visible = false;
        }

        public void DisableEntryHelper()
        {
            ButtonBox.Hide();
            functionCollectionDataGridView.Width = functionCollectionDataGridView.Parent.Width;
            //functionCollectionDataGridView.Left += 90;

            textBoxLinkedID.Visible = false;
            labelLinkedID.Visible = false;

            buttonMap.Visible = false;
            //buttonImport.Visible = false;
            buttonAssignIntron.Visible = false;

            groupBox1.Visible = false;
            //ExecOverrideBox.Visible = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMap_Click(object sender, EventArgs e)
        {
            string fileName = null;
            string data = null;
            string id = textBoxLinkedID.Text;
            bool noRowsSelected = true;
            int tn;
            string workingDirectory = (string)Registry.GetValue(Reg.PROJ_ROOT, Reg.WORKING_DIR, "C:\\Working");

            try
            {
                LockStatusFunctions lockStatusFunctions = new LockStatusFunctions();
                if (lockStatusFunctions.IsCheckedOut(id) == false) {
                    throw new Exception(string.Format("{0} isn't checked out.\n\nSelected 'Linked ID' needs to be checked out.", id));
                }

                foreach (DataGridViewRow row in functionCollectionDataGridView.Rows)
                {
                    if (row.Selected == true)
                    {
                        noRowsSelected = false;
                        object objFileName = row.Cells["Filename"].Value;
                        object objTN = row.Cells["TN"].Value;
                        if (objTN != null && objFileName != null)
                        {
                            tn = Int32.Parse(objTN.ToString());
                            fileName = objFileName.ToString();

                            data = EntryHelperWrapper.Map(tn, fileName, id, workingDirectory);

                            if (!string.IsNullOrEmpty(data))
                            {
                                row.Cells["Data"].Value = data;
                                row.Cells["ID"].Value = id;
                            }
                        }
                    }
                }
                if (noRowsSelected)
                    throw new Exception("You didn't select any rows to assign data.");
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, "Error");
            }
        }

        /// <summary>
        /// 1. executor code
        /// 2. $$$ file
        /// 3. import
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImport_Click(object sender, EventArgs e)
        {
            try
            {
                string id = textBoxLinkedID.Text;

                if (string.IsNullOrEmpty(id))
                    throw new Exception("You need to specify a linked ID");

                Nullable<int> executor_code =
                    EntryHelperWrapper.GetExecutor_Code(id);

                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Filter = "$$$ file (*.$$$)|*.$$$";
                dlg.Filter += "|All File Types (*.*)|*.*";
                dlg.DefaultExt = "$$$";

                if (dlg.ShowDialog() != DialogResult.OK)
                    return;

                ///
                /// passed executor and $$$ file check
                /// now start importing
                ///
                string data = null;
                int selectedRows = 0;
                TNDataImporter importer =
                    new TNDataImporter(executor_code.Value, dlg.FileName);
                CheckForExecOverride(importer);

                foreach (DataGridViewRow row 
                    in functionCollectionDataGridView.Rows)
                {
                    if (row.Selected == true)
                    {
                        selectedRows++;
                        object obj = row.Cells["Label"].Value;
                        if(obj != null)
                        {
                            data = importer.GetDataForLabel(obj.ToString());
                            if (!string.IsNullOrEmpty(data))
                            {
                                row.Cells["Data"].Value = data;
                                row.Cells["ID"].Value = id;
                            }
                        }
                    }
                }
                if (selectedRows == 0)
                {
                    string Title = "Nothing Done";
                    string Message = "No rows were selected.\nDo you want to import all data?";

                    DialogResult result = MessageBox.Show(Message, Title, MessageBoxButtons.YesNo);

                    if (result == DialogResult.Yes)
                    {
                        ImportAllData(importer.GetAllLabelsAndData());
                    }

                }
                else
                    MessageBox.Show(string.Format("{0} rows affected", selectedRows),
                                    "Success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void CheckForExecOverride(TNDataImporter importer)
        {
            if (ExecOverrideBox.Checked)
            {
                string Byte1 = (string)ByteCol1.Text;
                string Byte2 = (string)ByteCol2.Text;
                if (Byte1 == "")
                    Byte1 = ByteCol1.Text = "1";
                if (Byte2 == "")
                    Byte2 = ByteCol2.Text = "0";
                importer.OverrideExec(int.Parse(Byte1), int.Parse(Byte2));
            }
        }

        /// <summary>
        /// Import Data and Label from the given file into the TN
        /// </summary>
        /// <param name="Exec"></param>
        /// <param name="FileName"></param>
        private void ImportAllData(HashtableCollection ImportData)
        {
            string id = textBoxLinkedID.Text;
            foreach (Hashtable row in ImportData)
            {
                AddNewRow(id, row["Data"].ToString(), row["Label"].ToString()); 
            }
        }


        private void AddNewRow(string id, string Data, string Label)
        {
            ((FunctionCollection)functionCollectionBindingSource.DataSource).
                Add(BuildRow(id, Data, Label));
        }

        private void InsertRow(int before, string id, string Data, string Label)
        {
            ((FunctionCollection)functionCollectionBindingSource.DataSource).
                Insert(before, BuildRow(id, Data, Label));
        }

        private Function BuildRow(string id, string Data, string Label)
        {
            Function FunctionRow = new Function();

            if (!string.IsNullOrEmpty(id))
                FunctionRow.ID = id.Trim();

            if (!string.IsNullOrEmpty(Data))
                FunctionRow.Data = Data.Trim();

            if (!string.IsNullOrEmpty(Label))
                FunctionRow.Label = Label.Trim();

            FunctionRow.TN = _tnNumber;
            return FunctionRow;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAssignIntron_Click(object sender, EventArgs e)
        {
            string intron = null;

            foreach (DataGridViewRow row
               in functionCollectionDataGridView.Rows)
            {
                object oIntron = row.Cells["Intron"].Value;
                object label = row.Cells["Label"].Value;
                object id = row.Cells["ID"].Value;              // use Mode associated to this data
                if (rowMeetsRequirements(row.Selected, oIntron, id, label))
                {
                    intron = EntryHelperWrapper.GetIntron(
                        id.ToString().Substring(0, 1), label.ToString());

                    row.Cells["Intron"].Value = intron;
                }
            }
        }

        private static bool rowMeetsRequirements(bool Selected, object intron, object id, object label)
        {
            if (Selected == false)
                return false;
            if (intron != null && intron.ToString().Length > 0)   // if intron already exists
                return false;                                     // do nothing
            if (label == null || label.ToString().Length == 0)
                return false;
            if (id == null || id.ToString().Length == 0)
                return false;

            return true;
        }

        /// <summary>
        /// After a cell has been edited, 
        /// this is where you might do some extra data validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void functionCollectionDataGridView_CellEndEdit(
                            object sender, DataGridViewCellEventArgs e)
        {
            if (functionCollectionDataGridView.Rows[e.RowIndex].Cells["TN"].Value == null)
                return;

            string TNentry = functionCollectionDataGridView.Rows[e.RowIndex].Cells["TN"].Value.ToString();

            if (e.ColumnIndex == functionCollectionDataGridView.Columns["TN"].Index)
            {
                if (!EditFunctions.validateTN(int.Parse(TNentry)))
                    functionCollectionDataGridView.Rows[e.RowIndex].
                        Cells["TN"].Value = "0";
            }
            if (e.ColumnIndex == functionCollectionDataGridView.Columns["Label"].Index)
            {
                functionCollectionDataGridView["TN", e.RowIndex].Value = _tnNumber;
                if (_tnNumber > 0)
                {
                    PrjFileHelper.UpdatePrjAndReportDiff(_tnNumber,
                        CaptureFunctions.GetGridFileList(functionCollectionDataGridView));
                    TNHelper.Log(_tnNumber, string.Format("Edited [row {0}] label to \"{1}\"", e.RowIndex,
                        functionCollectionDataGridView["Label", e.RowIndex].Value.ToString()));
                }
            }
        }

        public void UpdateTnZip()
        {
            TNHelper.UpdateTNZip(_tnNumber,
                CaptureFunctions.CaptureFileList(functionCollectionDataGridView));
        }

        private void ExecOverrideBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ExecOverrideBox.Checked)
                ColumnGroup.Visible = true;
            else
                ColumnGroup.Visible = false;
        }

        public void ToolStripMenuItem_Relay(string Selection)
        {
            EditFunctions.EditMenuFunctionsHandler(Selection,
                                    functionCollectionDataGridView, 
                                    functionCollectionBindingSource);
        }

        private void functionCollectionDataGridView_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu ClickMenu = EditFunctions.CommonEditPopup(RightClickMenuSelection);
                ClickMenu.Show(functionCollectionDataGridView, e.Location);
            }
        }

        private void RightClickMenuSelection(object sender, EventArgs e)
        {
            EditFunctions._tnNumber = _tnNumber;
            string Selection = ((MenuItem)sender).Text;
            EditFunctions.EditMenuFunctionsHandler(Selection,
                                    functionCollectionDataGridView,
                                    functionCollectionBindingSource);
        }

        private void functionCollectionDataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            EditFunctions._tnNumber = _tnNumber;
            EditFunctions.KeyCheck(sender, e, functionCollectionDataGridView,
                                    functionCollectionBindingSource);
        }


        /// <summary>
        /// format a line of text for a header line;
        /// </summary>
        /// <returns></returns>
        public string GetTNReportHeader(string TN)
        {
            return string.Format("TN:{0} \r\nSynth   Data               Label             LegacyLabel       Intron    P  File        ID    Comment                      \r\n", TN);
        }
        /// <summary>
        /// Format the specified row of data from the table for printing.
        /// The data is returned in the order it was last viewed. 
        /// if the data sort order was changed the data will be returned in that order.
        /// </summary>
        /// <param name="row">the zero based row of data requested.</param>
        /// <returns>a string sutible for printing</returns>
        public string GetTNReportLine()
        {
            int row = LastLinePrinted++;
            string data = null;
             
            if (row >= 0 && row < functionCollectionDataGridView.Rows.Count)
            {
                Function FunctionRow = (Function)functionCollectionDataGridView.Rows[row].DataBoundItem;
                if (FunctionRow == null)
                    return null;
                data += string.Format("{0,-7}{1,-20}{2,-18}{3,-18}{4,-10}{5,-2} {6,-12}{7,-6}{8,-10}",
                    FunctionRow.Synth, FunctionRow.Data,
                    FunctionRow.Label, FunctionRow.LegacyLabel,
                    FunctionRow.Intron, FunctionRow.IntronPriority,
                    FunctionRow.Filename, FunctionRow.ID, FunctionRow.Comment);
            }
            return data;
        }

        public string GetIDReportHeader(string ID)
        {
            return string.Format("ID:{0} \r\nSynth   Data               Label             LegacyLabel       Intron    P  File        TN    Comment                      \r\n",ID);
        }
        /// <summary>
        /// Format the specified row of data from the table for printing.
        /// The data is returned in the order it was last viewed. 
        /// if the data sort order was changed the data will be returned in that order.
        /// </summary>
        /// <param name="row">the zero based row of data requested.</param>
        /// <returns>a string sutible for printing</returns>
        public string GetIDReportLine()
        {
            int row = LastLinePrinted++;
            string data = null;

            if (row >= 0 && row < functionCollectionDataGridView.Rows.Count)
            {
                Function FunctionRow = (Function)functionCollectionDataGridView.Rows[row].DataBoundItem;
                if (FunctionRow == null)
                    return null;
                data += string.Format("{0,-7}{1,-20}{2,-18}{3,-18}{4,-10}{5,-2} {6,-12}{7,-6}{8,-10}",
                    FunctionRow.Synth, FunctionRow.Data,
                    FunctionRow.Label, FunctionRow.LegacyLabel,
                    FunctionRow.Intron, FunctionRow.IntronPriority,
                    FunctionRow.Filename, FunctionRow.TN, FunctionRow.Comment);
            }
            return data;
        }

        private void functionCollectionDataGridView_Sorted(object sender, EventArgs e)
        {
            SetRowColorsByID();
        }

        private void functionCollectionDataGridView_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void functionCollectionDataGridView_DragDrop(object sender, DragEventArgs e)
        {
            string[] text = e.Data.GetFormats();
            AddNewRow(null, null, (string)e.Data.GetData(text[0]));
        }

        private void LabelGrpCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            _keys.FillLabelSelectionListBox(LabelLB, LabelGrpCB.SelectedIndex);
        }

        private bool ItemSelected = false;
        private Point ClickLocation = new Point(0, 0);

        private void LabelLB_MouseDown(object sender, MouseEventArgs e)
        {
            ItemSelected = true;
            ClickLocation = e.Location;
        }

        private void LabelLB_MouseMove(object sender, MouseEventArgs e)
        {
            if (ItemSelected && !ClickLocation.Equals(e.Location))
            {
                string label = LabelLB.SelectedItem.ToString();
                LabelLB.DoDragDrop(label, DragDropEffects.Copy);
            }
            ItemSelected = false;
        }

        private void LabelLB_DoubleClick(object sender, EventArgs e)
        {
            string label = LabelLB.SelectedItem.ToString();
            DataGridViewSelectedRowCollection rows = functionCollectionDataGridView.SelectedRows;
            if (rows.Count > 0)
                InsertRow(rows[0].Index, null, null, label);
            else
                AddNewRow(null, null, label);
        }

        private void CopyBtn_Click(object sender, EventArgs e)
        {
            foreach (Object item in LabelLB.SelectedItems)
                AddNewRow(null, null, item.ToString());
        }

        private void InsertBtn_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rows = functionCollectionDataGridView.SelectedRows;
            if (rows.Count > 0)
                foreach (Object item in LabelLB.SelectedItems)
                    InsertRow(rows[0].Index, null, null, item.ToString());
        }

        private void DeleteAllBtn_Click(object sender, EventArgs e)
        {
            if (CommonFunctions.WarningDlg("Are You Sure?", "Delete All Functions?") != DialogResult.Yes)
                return;
            //foreach (DataGridViewRow row in functionCollectionDataGridView.Rows)
            //    row.Cells["FileName"].Selected = true;
            //EditFunctions.EditMenuFunctionsHandler("Delete", functionCollectionDataGridView,
            //                        functionCollectionBindingSource);

            functionCollectionDataGridView.Rows.Clear();
            ((FunctionCollection)functionCollectionBindingSource.DataSource).Clear();
            EditFunctions.LogOperation(_tnNumber, "Delete All Rows", null);
        }

        private void ImportBtn_Click(object sender, EventArgs e)
        {
            IList<string> labels = CaptureFunctions.ImportTnLabels();
            foreach (string item in labels)
                AddNewRow(null, null, item);
        }

        private void functionCollectionDataGridView_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            //if (functionCollectionDataGridView.RowCount > e.RowIndex)
            //    CaptureFunctions.UpdateChangesToTnFiles(functionCollectionDataGridView);
        }

        private void functionCollectionDataGridView_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            e.Cancel = true;    // can't allow this!
        }

        private void functionCollectionDataGridView_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e) {
            e.Row.Cells["Intron"].Value = string.Empty;
            e.Row.Cells["IntronPriority"].Value = string.Empty;
            e.Row.Cells["LegacyLabel"].Value = string.Empty;
            e.Row.Cells["ID"].Value = string.Empty;
            e.Row.Cells["Comment"].Value = string.Empty;
            e.Row.Cells["Filename"].Value = string.Empty;
        }
    }
}
