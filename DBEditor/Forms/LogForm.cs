using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using System.IO;

namespace DBEditor
{
    public partial class LogForm : Form
    {
        public LogForm()
        {
            InitializeComponent();
        }

        public void Bind(LogCollection IDLogData)
        {
            //LastLinePrinted = 0;
            logDataGridView.DataSource = IDLogData;
            logDataGridView.Sort(logDataGridView.Columns["Time"],  // can't be null?
                                                    ListSortDirection.Ascending);
        }

        private void logDataGridView_MouseClick(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Right)
            //{
            //    ContextMenu ClickMenu = EditFunctions.CommonEditPopup(RightClickMenuSelection);
            //    ClickMenu.Show(logDataGridView, e.Location);
            //}
        }

        private void RightClickMenuSelection(object sender, EventArgs e)
        {
            //string Selection = ((MenuItem)sender).Text;
            //if (Selection != "Time" && Selection != "Operator")
            //    EditFunctions.EditMenuFunctionsHandler(Selection,
            //                        logDataGridView);
        }

    }
}