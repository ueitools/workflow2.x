namespace DBEditor
{
    partial class TNHeaderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label textLabel;
            System.Windows.Forms.Label statusLabel;
            System.Windows.Forms.Label sender_PhoneLabel;
            System.Windows.Forms.Label sender_NameLabel;
            System.Windows.Forms.Label sender_CompanyLabel;
            System.Windows.Forms.Label sender_AddressLabel;
            System.Windows.Forms.Label parent_TNLabel;
            System.Windows.Forms.Label legacyTargetModelLabel;
            System.Windows.Forms.Label legacyRemoteModelLabel;
            System.Windows.Forms.Label legacyMarketFlagLabel;
            System.Windows.Forms.Label legacyDeviceTypeLabel;
            System.Windows.Forms.Label legacyBrandLabel;
            System.Windows.Forms.Label imageLabel;
            System.Windows.Forms.Label homerLabel;
            System.Windows.Forms.Label dBSearchLogLabel;
            System.Windows.Forms.Label boxLabel;
            this.textTextBox = new System.Windows.Forms.TextBox();
            this.tnHeaderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.statusTextBox = new System.Windows.Forms.TextBox();
            this.sender_PhoneTextBox = new System.Windows.Forms.TextBox();
            this.sender_NameTextBox = new System.Windows.Forms.TextBox();
            this.sender_CompanyTextBox = new System.Windows.Forms.TextBox();
            this.sender_AddressTextBox = new System.Windows.Forms.TextBox();
            this.parent_TNTextBox = new System.Windows.Forms.TextBox();
            this.legacyTargetModelTextBox = new System.Windows.Forms.TextBox();
            this.legacyRemoteModelTextBox = new System.Windows.Forms.TextBox();
            this.legacyDeviceTypeTextBox = new System.Windows.Forms.TextBox();
            this.legacyBrandTextBox = new System.Windows.Forms.TextBox();
            this.imageTextBox = new System.Windows.Forms.TextBox();
            this.homerTextBox = new System.Windows.Forms.TextBox();
            this.dBSearchLogTextBox = new System.Windows.Forms.TextBox();
            this.boxTextBox = new System.Windows.Forms.TextBox();
            this.Loglabel = new System.Windows.Forms.Label();
            this.LogCommentTextBox = new System.Windows.Forms.TextBox();
            this.StatusBox = new System.Windows.Forms.GroupBox();
            this.StatusQ = new System.Windows.Forms.RadioButton();
            this.StatusM = new System.Windows.Forms.RadioButton();
            this.legacyMarketFlagTextBox = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.operatorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            textLabel = new System.Windows.Forms.Label();
            statusLabel = new System.Windows.Forms.Label();
            sender_PhoneLabel = new System.Windows.Forms.Label();
            sender_NameLabel = new System.Windows.Forms.Label();
            sender_CompanyLabel = new System.Windows.Forms.Label();
            sender_AddressLabel = new System.Windows.Forms.Label();
            parent_TNLabel = new System.Windows.Forms.Label();
            legacyTargetModelLabel = new System.Windows.Forms.Label();
            legacyRemoteModelLabel = new System.Windows.Forms.Label();
            legacyMarketFlagLabel = new System.Windows.Forms.Label();
            legacyDeviceTypeLabel = new System.Windows.Forms.Label();
            legacyBrandLabel = new System.Windows.Forms.Label();
            imageLabel = new System.Windows.Forms.Label();
            homerLabel = new System.Windows.Forms.Label();
            dBSearchLogLabel = new System.Windows.Forms.Label();
            boxLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tnHeaderBindingSource)).BeginInit();
            this.StatusBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logListBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // textLabel
            // 
            textLabel.AutoSize = true;
            textLabel.Location = new System.Drawing.Point(36, 351);
            textLabel.Name = "textLabel";
            textLabel.Size = new System.Drawing.Size(51, 13);
            textLabel.TabIndex = 31;
            textLabel.Text = "Comment";
            // 
            // statusLabel
            // 
            statusLabel.AutoSize = true;
            statusLabel.Location = new System.Drawing.Point(36, 61);
            statusLabel.Name = "statusLabel";
            statusLabel.Size = new System.Drawing.Size(40, 13);
            statusLabel.TabIndex = 29;
            statusLabel.Text = "Status:";
            statusLabel.Visible = false;
            // 
            // sender_PhoneLabel
            // 
            sender_PhoneLabel.AutoSize = true;
            sender_PhoneLabel.Location = new System.Drawing.Point(289, 87);
            sender_PhoneLabel.Name = "sender_PhoneLabel";
            sender_PhoneLabel.Size = new System.Drawing.Size(78, 13);
            sender_PhoneLabel.TabIndex = 27;
            sender_PhoneLabel.Text = "Sender Phone:";
            // 
            // sender_NameLabel
            // 
            sender_NameLabel.AutoSize = true;
            sender_NameLabel.Location = new System.Drawing.Point(289, 61);
            sender_NameLabel.Name = "sender_NameLabel";
            sender_NameLabel.Size = new System.Drawing.Size(75, 13);
            sender_NameLabel.TabIndex = 25;
            sender_NameLabel.Text = "Sender Name:";
            // 
            // sender_CompanyLabel
            // 
            sender_CompanyLabel.AutoSize = true;
            sender_CompanyLabel.Location = new System.Drawing.Point(289, 35);
            sender_CompanyLabel.Name = "sender_CompanyLabel";
            sender_CompanyLabel.Size = new System.Drawing.Size(91, 13);
            sender_CompanyLabel.TabIndex = 23;
            sender_CompanyLabel.Text = "Sender Company:";
            // 
            // sender_AddressLabel
            // 
            sender_AddressLabel.AutoSize = true;
            sender_AddressLabel.Location = new System.Drawing.Point(289, 113);
            sender_AddressLabel.Name = "sender_AddressLabel";
            sender_AddressLabel.Size = new System.Drawing.Size(85, 13);
            sender_AddressLabel.TabIndex = 21;
            sender_AddressLabel.Text = "Sender Address:";
            // 
            // parent_TNLabel
            // 
            parent_TNLabel.AutoSize = true;
            parent_TNLabel.Location = new System.Drawing.Point(36, 35);
            parent_TNLabel.Name = "parent_TNLabel";
            parent_TNLabel.Size = new System.Drawing.Size(59, 13);
            parent_TNLabel.TabIndex = 19;
            parent_TNLabel.Text = "Parent TN:";
            // 
            // legacyTargetModelLabel
            // 
            legacyTargetModelLabel.AutoSize = true;
            legacyTargetModelLabel.Location = new System.Drawing.Point(289, 306);
            legacyTargetModelLabel.Name = "legacyTargetModelLabel";
            legacyTargetModelLabel.Size = new System.Drawing.Size(111, 13);
            legacyTargetModelLabel.TabIndex = 17;
            legacyTargetModelLabel.Text = "Legacy Target Model:";
            // 
            // legacyRemoteModelLabel
            // 
            legacyRemoteModelLabel.AutoSize = true;
            legacyRemoteModelLabel.Location = new System.Drawing.Point(289, 279);
            legacyRemoteModelLabel.Name = "legacyRemoteModelLabel";
            legacyRemoteModelLabel.Size = new System.Drawing.Size(117, 13);
            legacyRemoteModelLabel.TabIndex = 15;
            legacyRemoteModelLabel.Text = "Legacy Remote Model:";
            // 
            // legacyMarketFlagLabel
            // 
            legacyMarketFlagLabel.AutoSize = true;
            legacyMarketFlagLabel.Location = new System.Drawing.Point(289, 252);
            legacyMarketFlagLabel.Name = "legacyMarketFlagLabel";
            legacyMarketFlagLabel.Size = new System.Drawing.Size(104, 13);
            legacyMarketFlagLabel.TabIndex = 13;
            legacyMarketFlagLabel.Text = "Legacy Market Flag:";
            // 
            // legacyDeviceTypeLabel
            // 
            legacyDeviceTypeLabel.AutoSize = true;
            legacyDeviceTypeLabel.Location = new System.Drawing.Point(289, 225);
            legacyDeviceTypeLabel.Name = "legacyDeviceTypeLabel";
            legacyDeviceTypeLabel.Size = new System.Drawing.Size(109, 13);
            legacyDeviceTypeLabel.TabIndex = 11;
            legacyDeviceTypeLabel.Text = "Legacy Device Type:";
            // 
            // legacyBrandLabel
            // 
            legacyBrandLabel.AutoSize = true;
            legacyBrandLabel.Location = new System.Drawing.Point(289, 198);
            legacyBrandLabel.Name = "legacyBrandLabel";
            legacyBrandLabel.Size = new System.Drawing.Size(76, 13);
            legacyBrandLabel.TabIndex = 9;
            legacyBrandLabel.Text = "Legacy Brand:";
            // 
            // imageLabel
            // 
            imageLabel.AutoSize = true;
            imageLabel.Location = new System.Drawing.Point(36, 191);
            imageLabel.Name = "imageLabel";
            imageLabel.Size = new System.Drawing.Size(39, 13);
            imageLabel.TabIndex = 7;
            imageLabel.Text = "Image:";
            // 
            // homerLabel
            // 
            homerLabel.AutoSize = true;
            homerLabel.Location = new System.Drawing.Point(36, 165);
            homerLabel.Name = "homerLabel";
            homerLabel.Size = new System.Drawing.Size(41, 13);
            homerLabel.TabIndex = 5;
            homerLabel.Text = "Homer:";
            // 
            // dBSearchLogLabel
            // 
            dBSearchLogLabel.AutoSize = true;
            dBSearchLogLabel.Location = new System.Drawing.Point(36, 139);
            dBSearchLogLabel.Name = "dBSearchLogLabel";
            dBSearchLogLabel.Size = new System.Drawing.Size(80, 13);
            dBSearchLogLabel.TabIndex = 3;
            dBSearchLogLabel.Text = "DBSearch Log:";
            // 
            // boxLabel
            // 
            boxLabel.AutoSize = true;
            boxLabel.Location = new System.Drawing.Point(36, 113);
            boxLabel.Name = "boxLabel";
            boxLabel.Size = new System.Drawing.Size(28, 13);
            boxLabel.TabIndex = 1;
            boxLabel.Text = "Box:";
            // 
            // textTextBox
            // 
            this.textTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "Text", true));
            this.textTextBox.Location = new System.Drawing.Point(125, 348);
            this.textTextBox.Multiline = true;
            this.textTextBox.Name = "textTextBox";
            this.textTextBox.Size = new System.Drawing.Size(471, 160);
            this.textTextBox.TabIndex = 32;
            // 
            // tnHeaderBindingSource
            // 
            this.tnHeaderBindingSource.DataSource = typeof(BusinessObject.TNHeader);
            // 
            // statusTextBox
            // 
            this.statusTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "Status", true));
            this.statusTextBox.Location = new System.Drawing.Point(125, 57);
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.ReadOnly = true;
            this.statusTextBox.Size = new System.Drawing.Size(100, 20);
            this.statusTextBox.TabIndex = 30;
            this.statusTextBox.Visible = false;
            // 
            // sender_PhoneTextBox
            // 
            this.sender_PhoneTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "Sender_Phone", true));
            this.sender_PhoneTextBox.Location = new System.Drawing.Point(409, 83);
            this.sender_PhoneTextBox.Name = "sender_PhoneTextBox";
            this.sender_PhoneTextBox.Size = new System.Drawing.Size(187, 20);
            this.sender_PhoneTextBox.TabIndex = 28;
            // 
            // sender_NameTextBox
            // 
            this.sender_NameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "Sender_Name", true));
            this.sender_NameTextBox.Location = new System.Drawing.Point(409, 57);
            this.sender_NameTextBox.Name = "sender_NameTextBox";
            this.sender_NameTextBox.Size = new System.Drawing.Size(187, 20);
            this.sender_NameTextBox.TabIndex = 26;
            // 
            // sender_CompanyTextBox
            // 
            this.sender_CompanyTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "Sender_Company", true));
            this.sender_CompanyTextBox.Location = new System.Drawing.Point(409, 31);
            this.sender_CompanyTextBox.Name = "sender_CompanyTextBox";
            this.sender_CompanyTextBox.Size = new System.Drawing.Size(187, 20);
            this.sender_CompanyTextBox.TabIndex = 24;
            // 
            // sender_AddressTextBox
            // 
            this.sender_AddressTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "Sender_Address", true));
            this.sender_AddressTextBox.Location = new System.Drawing.Point(409, 109);
            this.sender_AddressTextBox.Multiline = true;
            this.sender_AddressTextBox.Name = "sender_AddressTextBox";
            this.sender_AddressTextBox.Size = new System.Drawing.Size(187, 80);
            this.sender_AddressTextBox.TabIndex = 22;
            // 
            // parent_TNTextBox
            // 
            this.parent_TNTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "Parent_TN", true));
            this.parent_TNTextBox.Location = new System.Drawing.Point(125, 31);
            this.parent_TNTextBox.Name = "parent_TNTextBox";
            this.parent_TNTextBox.Size = new System.Drawing.Size(100, 20);
            this.parent_TNTextBox.TabIndex = 20;
            // 
            // legacyTargetModelTextBox
            // 
            this.legacyTargetModelTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "LegacyTargetModel", true));
            this.legacyTargetModelTextBox.Location = new System.Drawing.Point(409, 303);
            this.legacyTargetModelTextBox.Name = "legacyTargetModelTextBox";
            this.legacyTargetModelTextBox.Size = new System.Drawing.Size(187, 20);
            this.legacyTargetModelTextBox.TabIndex = 18;
            // 
            // legacyRemoteModelTextBox
            // 
            this.legacyRemoteModelTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "LegacyRemoteModel", true));
            this.legacyRemoteModelTextBox.Location = new System.Drawing.Point(409, 276);
            this.legacyRemoteModelTextBox.Name = "legacyRemoteModelTextBox";
            this.legacyRemoteModelTextBox.Size = new System.Drawing.Size(187, 20);
            this.legacyRemoteModelTextBox.TabIndex = 16;
            // 
            // legacyDeviceTypeTextBox
            // 
            this.legacyDeviceTypeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "LegacyDeviceType", true));
            this.legacyDeviceTypeTextBox.Location = new System.Drawing.Point(409, 222);
            this.legacyDeviceTypeTextBox.Name = "legacyDeviceTypeTextBox";
            this.legacyDeviceTypeTextBox.Size = new System.Drawing.Size(187, 20);
            this.legacyDeviceTypeTextBox.TabIndex = 12;
            // 
            // legacyBrandTextBox
            // 
            this.legacyBrandTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "LegacyBrand", true));
            this.legacyBrandTextBox.Location = new System.Drawing.Point(409, 195);
            this.legacyBrandTextBox.Name = "legacyBrandTextBox";
            this.legacyBrandTextBox.Size = new System.Drawing.Size(187, 20);
            this.legacyBrandTextBox.TabIndex = 10;
            // 
            // imageTextBox
            // 
            this.imageTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "Image", true));
            this.imageTextBox.Location = new System.Drawing.Point(125, 187);
            this.imageTextBox.Name = "imageTextBox";
            this.imageTextBox.Size = new System.Drawing.Size(100, 20);
            this.imageTextBox.TabIndex = 8;
            // 
            // homerTextBox
            // 
            this.homerTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "Homer", true));
            this.homerTextBox.Location = new System.Drawing.Point(125, 161);
            this.homerTextBox.Name = "homerTextBox";
            this.homerTextBox.Size = new System.Drawing.Size(100, 20);
            this.homerTextBox.TabIndex = 6;
            // 
            // dBSearchLogTextBox
            // 
            this.dBSearchLogTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "DBSearchLog", true));
            this.dBSearchLogTextBox.Location = new System.Drawing.Point(125, 135);
            this.dBSearchLogTextBox.Name = "dBSearchLogTextBox";
            this.dBSearchLogTextBox.Size = new System.Drawing.Size(100, 20);
            this.dBSearchLogTextBox.TabIndex = 4;
            // 
            // boxTextBox
            // 
            this.boxTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tnHeaderBindingSource, "Box", true));
            this.boxTextBox.Location = new System.Drawing.Point(125, 109);
            this.boxTextBox.Name = "boxTextBox";
            this.boxTextBox.Size = new System.Drawing.Size(100, 20);
            this.boxTextBox.TabIndex = 2;
            // 
            // Loglabel
            // 
            this.Loglabel.AutoSize = true;
            this.Loglabel.Location = new System.Drawing.Point(36, 517);
            this.Loglabel.Name = "Loglabel";
            this.Loglabel.Size = new System.Drawing.Size(75, 13);
            this.Loglabel.TabIndex = 57;
            this.Loglabel.Text = "Log Comment:";
            // 
            // LogCommentTextBox
            // 
            this.LogCommentTextBox.Location = new System.Drawing.Point(125, 514);
            this.LogCommentTextBox.Multiline = true;
            this.LogCommentTextBox.Name = "LogCommentTextBox";
            this.LogCommentTextBox.Size = new System.Drawing.Size(471, 40);
            this.LogCommentTextBox.TabIndex = 56;
            // 
            // StatusBox
            // 
            this.StatusBox.Controls.Add(this.StatusQ);
            this.StatusBox.Controls.Add(this.StatusM);
            this.StatusBox.Location = new System.Drawing.Point(643, 28);
            this.StatusBox.Name = "StatusBox";
            this.StatusBox.Size = new System.Drawing.Size(121, 74);
            this.StatusBox.TabIndex = 61;
            this.StatusBox.TabStop = false;
            this.StatusBox.Text = "TN Status";
            // 
            // StatusQ
            // 
            this.StatusQ.AutoSize = true;
            this.StatusQ.Location = new System.Drawing.Point(21, 19);
            this.StatusQ.Name = "StatusQ";
            this.StatusQ.Size = new System.Drawing.Size(78, 17);
            this.StatusQ.TabIndex = 56;
            this.StatusQ.Tag = "Q";
            this.StatusQ.Text = "Passed QA";
            this.StatusQ.CheckedChanged += new System.EventHandler(this.StatusChanged);
            // 
            // StatusM
            // 
            this.StatusM.AutoSize = true;
            this.StatusM.Location = new System.Drawing.Point(21, 50);
            this.StatusM.Name = "StatusM";
            this.StatusM.Size = new System.Drawing.Size(65, 17);
            this.StatusM.TabIndex = 57;
            this.StatusM.Tag = "M";
            this.StatusM.Text = "Modified";
            this.StatusM.CheckedChanged += new System.EventHandler(this.StatusChanged);
            // 
            // legacyMarketFlagTextBox
            // 
            this.legacyMarketFlagTextBox.Location = new System.Drawing.Point(409, 249);
            this.legacyMarketFlagTextBox.Name = "legacyMarketFlagTextBox";
            this.legacyMarketFlagTextBox.Size = new System.Drawing.Size(187, 20);
            this.legacyMarketFlagTextBox.TabIndex = 62;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.operatorDataGridViewTextBoxColumn,
            this.timeDataGridViewTextBoxColumn,
            this.contentDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.logListBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(39, 570);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(736, 160);
            this.dataGridView1.TabIndex = 63;
            // 
            // operatorDataGridViewTextBoxColumn
            // 
            this.operatorDataGridViewTextBoxColumn.DataPropertyName = "Operator";
            this.operatorDataGridViewTextBoxColumn.HeaderText = "Operator";
            this.operatorDataGridViewTextBoxColumn.Name = "operatorDataGridViewTextBoxColumn";
            this.operatorDataGridViewTextBoxColumn.ReadOnly = true;
            this.operatorDataGridViewTextBoxColumn.Width = 170;
            // 
            // timeDataGridViewTextBoxColumn
            // 
            this.timeDataGridViewTextBoxColumn.DataPropertyName = "Time";
            this.timeDataGridViewTextBoxColumn.HeaderText = "Time";
            this.timeDataGridViewTextBoxColumn.Name = "timeDataGridViewTextBoxColumn";
            this.timeDataGridViewTextBoxColumn.ReadOnly = true;
            this.timeDataGridViewTextBoxColumn.Width = 120;
            // 
            // contentDataGridViewTextBoxColumn
            // 
            this.contentDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.contentDataGridViewTextBoxColumn.DataPropertyName = "Content";
            this.contentDataGridViewTextBoxColumn.HeaderText = "Content";
            this.contentDataGridViewTextBoxColumn.Name = "contentDataGridViewTextBoxColumn";
            this.contentDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // logListBindingSource
            // 
            this.logListBindingSource.DataMember = "LogList";
            this.logListBindingSource.DataSource = this.tnHeaderBindingSource;
            // 
            // TNHeaderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 742);
            this.ControlBox = false;
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.legacyMarketFlagTextBox);
            this.Controls.Add(this.StatusBox);
            this.Controls.Add(this.Loglabel);
            this.Controls.Add(this.LogCommentTextBox);
            this.Controls.Add(boxLabel);
            this.Controls.Add(this.boxTextBox);
            this.Controls.Add(dBSearchLogLabel);
            this.Controls.Add(this.dBSearchLogTextBox);
            this.Controls.Add(homerLabel);
            this.Controls.Add(this.homerTextBox);
            this.Controls.Add(imageLabel);
            this.Controls.Add(this.imageTextBox);
            this.Controls.Add(legacyBrandLabel);
            this.Controls.Add(this.legacyBrandTextBox);
            this.Controls.Add(legacyDeviceTypeLabel);
            this.Controls.Add(this.legacyDeviceTypeTextBox);
            this.Controls.Add(legacyMarketFlagLabel);
            this.Controls.Add(legacyRemoteModelLabel);
            this.Controls.Add(this.legacyRemoteModelTextBox);
            this.Controls.Add(legacyTargetModelLabel);
            this.Controls.Add(this.legacyTargetModelTextBox);
            this.Controls.Add(parent_TNLabel);
            this.Controls.Add(this.parent_TNTextBox);
            this.Controls.Add(sender_AddressLabel);
            this.Controls.Add(this.sender_AddressTextBox);
            this.Controls.Add(sender_CompanyLabel);
            this.Controls.Add(this.sender_CompanyTextBox);
            this.Controls.Add(sender_NameLabel);
            this.Controls.Add(this.sender_NameTextBox);
            this.Controls.Add(sender_PhoneLabel);
            this.Controls.Add(this.sender_PhoneTextBox);
            this.Controls.Add(statusLabel);
            this.Controls.Add(this.statusTextBox);
            this.Controls.Add(textLabel);
            this.Controls.Add(this.textTextBox);
            this.Name = "TNHeaderForm";
            this.Text = "TN Header";
            ((System.ComponentModel.ISupportInitialize)(this.tnHeaderBindingSource)).EndInit();
            this.StatusBox.ResumeLayout(false);
            this.StatusBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logListBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textTextBox;
        private System.Windows.Forms.TextBox statusTextBox;
        private System.Windows.Forms.TextBox sender_PhoneTextBox;
        private System.Windows.Forms.TextBox sender_NameTextBox;
        private System.Windows.Forms.TextBox sender_CompanyTextBox;
        private System.Windows.Forms.TextBox sender_AddressTextBox;
        private System.Windows.Forms.TextBox parent_TNTextBox;
        private System.Windows.Forms.TextBox legacyTargetModelTextBox;
        private System.Windows.Forms.TextBox legacyRemoteModelTextBox;
        private System.Windows.Forms.TextBox legacyDeviceTypeTextBox;
        private System.Windows.Forms.TextBox legacyBrandTextBox;
        private System.Windows.Forms.TextBox imageTextBox;
        private System.Windows.Forms.TextBox homerTextBox;
        private System.Windows.Forms.TextBox dBSearchLogTextBox;
        private System.Windows.Forms.TextBox boxTextBox;
        private System.Windows.Forms.Label Loglabel;
        public System.Windows.Forms.TextBox LogCommentTextBox;
        private System.Windows.Forms.BindingSource tnHeaderBindingSource;
        private System.Windows.Forms.GroupBox StatusBox;
        private System.Windows.Forms.RadioButton StatusQ;
        private System.Windows.Forms.RadioButton StatusM;
        private System.Windows.Forms.TextBox legacyMarketFlagTextBox;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource logListBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn operatorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contentDataGridViewTextBoxColumn;

    }
}