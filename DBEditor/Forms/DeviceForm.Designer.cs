namespace DBEditor
{
    partial class DeviceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.deviceCollectionDataGridView = new System.Windows.Forms.DataGridView();
            this.deviceCollectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.opInfoDataGridView = new System.Windows.Forms.DataGridView();
            this.Operation = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Information = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.opInfoListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.countryDataGridView = new System.Windows.Forms.DataGridView();
            this.Region = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.CountryName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.countryListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deviceTypeDataGridView = new System.Windows.Forms.DataGridView();
            this.DeviceName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.deviceTypeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ComboBoxTNList = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ComboBoxIDList = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Brand = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Model = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Branch = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.IsRetail = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.IsTarget = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.IsCodebook = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.IsPartNumber = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.deviceCollectionDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceCollectionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.opInfoDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.opInfoListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countryDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countryListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceTypeDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceTypeListBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // deviceCollectionDataGridView
            // 
            this.deviceCollectionDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceCollectionDataGridView.AutoGenerateColumns = false;
            this.deviceCollectionDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.deviceCollectionDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.deviceCollectionDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ComboBoxTNList,
            this.ComboBoxIDList,
            this.Brand,
            this.Model,
            this.Branch,
            this.IsRetail,
            this.IsTarget,
            this.IsCodebook,
            this.IsPartNumber});
            this.deviceCollectionDataGridView.DataSource = this.deviceCollectionBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.FormatProvider = new System.Globalization.CultureInfo("en-US");
            dataGridViewCellStyle2.NullValue = "null";
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.deviceCollectionDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.deviceCollectionDataGridView.Location = new System.Drawing.Point(12, 54);
            this.deviceCollectionDataGridView.Name = "deviceCollectionDataGridView";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.deviceCollectionDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.deviceCollectionDataGridView.Size = new System.Drawing.Size(849, 355);
            this.deviceCollectionDataGridView.TabIndex = 1;
            this.deviceCollectionDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.deviceCollectionDataGridView_DataError);
            // 
            // deviceCollectionBindingSource
            // 
            this.deviceCollectionBindingSource.DataSource = typeof(BusinessObject.DeviceCollection);
            // 
            // opInfoDataGridView
            // 
            this.opInfoDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.opInfoDataGridView.AutoGenerateColumns = false;
            this.opInfoDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.opInfoDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.opInfoDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Operation,
            this.Information});
            this.opInfoDataGridView.DataSource = this.opInfoListBindingSource;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.opInfoDataGridView.DefaultCellStyle = dataGridViewCellStyle7;
            this.opInfoDataGridView.Location = new System.Drawing.Point(522, 459);
            this.opInfoDataGridView.Name = "opInfoDataGridView";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.opInfoDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.opInfoDataGridView.Size = new System.Drawing.Size(339, 135);
            this.opInfoDataGridView.TabIndex = 4;
            this.opInfoDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.opInfoDataGridView_CellBeginEdit);
            this.opInfoDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.opInfoDataGridView_CellEndEdit);
            this.opInfoDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.opInfoDataGridView_DataError);
            // 
            // Operation
            // 
            this.Operation.DataPropertyName = "Operation";
            dataGridViewCellStyle5.NullValue = "null";
            this.Operation.DefaultCellStyle = dataGridViewCellStyle5;
            this.Operation.HeaderText = "Operation";
            this.Operation.Name = "Operation";
            this.Operation.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Operation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Information
            // 
            this.Information.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Information.DataPropertyName = "Information";
            dataGridViewCellStyle6.NullValue = "null";
            this.Information.DefaultCellStyle = dataGridViewCellStyle6;
            this.Information.HeaderText = "Information";
            this.Information.Name = "Information";
            this.Information.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Information.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // opInfoListBindingSource
            // 
            this.opInfoListBindingSource.DataMember = "OpInfoList";
            this.opInfoListBindingSource.DataSource = this.deviceCollectionBindingSource;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 435);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Market Information";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(300, 435);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 37;
            this.label2.Text = "Device Type";
            // 
            // countryDataGridView
            // 
            this.countryDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.countryDataGridView.AutoGenerateColumns = false;
            this.countryDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.countryDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.countryDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Region,
            this.CountryName});
            this.countryDataGridView.DataSource = this.countryListBindingSource;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.countryDataGridView.DefaultCellStyle = dataGridViewCellStyle12;
            this.countryDataGridView.Location = new System.Drawing.Point(15, 459);
            this.countryDataGridView.Name = "countryDataGridView";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.countryDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.countryDataGridView.Size = new System.Drawing.Size(275, 135);
            this.countryDataGridView.TabIndex = 2;
            this.countryDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.countryDataGridView_CellBeginEdit);
            this.countryDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.countryDataGridView_CellEndEdit);
            this.countryDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.countryDataGridView_DataError);
            // 
            // Region
            // 
            this.Region.DataPropertyName = "Region";
            dataGridViewCellStyle10.NullValue = "null";
            this.Region.DefaultCellStyle = dataGridViewCellStyle10;
            this.Region.HeaderText = "Region";
            this.Region.Name = "Region";
            this.Region.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Region.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // CountryName
            // 
            this.CountryName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CountryName.DataPropertyName = "Name";
            dataGridViewCellStyle11.NullValue = "null";
            this.CountryName.DefaultCellStyle = dataGridViewCellStyle11;
            this.CountryName.HeaderText = "Name";
            this.CountryName.Name = "CountryName";
            this.CountryName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CountryName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // countryListBindingSource
            // 
            this.countryListBindingSource.DataMember = "CountryList";
            this.countryListBindingSource.DataSource = this.deviceCollectionBindingSource;
            // 
            // deviceTypeDataGridView
            // 
            this.deviceTypeDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceTypeDataGridView.AutoGenerateColumns = false;
            this.deviceTypeDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.deviceTypeDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.deviceTypeDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DeviceName});
            this.deviceTypeDataGridView.DataSource = this.deviceTypeListBindingSource;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.deviceTypeDataGridView.DefaultCellStyle = dataGridViewCellStyle16;
            this.deviceTypeDataGridView.Location = new System.Drawing.Point(303, 459);
            this.deviceTypeDataGridView.Name = "deviceTypeDataGridView";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.deviceTypeDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.deviceTypeDataGridView.Size = new System.Drawing.Size(202, 135);
            this.deviceTypeDataGridView.TabIndex = 3;
            this.deviceTypeDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.deviceTypeDataGridView_DataError);
            // 
            // DeviceName
            // 
            this.DeviceName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DeviceName.DataPropertyName = "Name";
            dataGridViewCellStyle15.NullValue = "null";
            this.DeviceName.DefaultCellStyle = dataGridViewCellStyle15;
            this.DeviceName.HeaderText = "Name";
            this.DeviceName.Name = "DeviceName";
            this.DeviceName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DeviceName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // deviceTypeListBindingSource
            // 
            this.deviceTypeListBindingSource.DataMember = "DeviceTypeList";
            this.deviceTypeListBindingSource.DataSource = this.deviceCollectionBindingSource;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(519, 435);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 13);
            this.label3.TabIndex = 38;
            this.label3.Text = "Operational Information";
            this.label3.UseMnemonic = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 13);
            this.label4.TabIndex = 39;
            this.label4.Text = "Brand Model Information";
            // 
            // ComboBoxTNList
            // 
            this.ComboBoxTNList.AutoComplete = false;
            this.ComboBoxTNList.DataPropertyName = "TN";
            this.ComboBoxTNList.HeaderText = "TN";
            this.ComboBoxTNList.Name = "ComboBoxTNList";
            this.ComboBoxTNList.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ComboBoxTNList.Sorted = true;
            // 
            // ComboBoxIDList
            // 
            this.ComboBoxIDList.AutoComplete = false;
            this.ComboBoxIDList.DataPropertyName = "ID";
            this.ComboBoxIDList.HeaderText = "ID";
            this.ComboBoxIDList.Name = "ComboBoxIDList";
            this.ComboBoxIDList.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ComboBoxIDList.Sorted = true;
            // 
            // Brand
            // 
            this.Brand.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Brand.DataPropertyName = "Brand";
            this.Brand.FillWeight = 50F;
            this.Brand.HeaderText = "Brand";
            this.Brand.MinimumWidth = 100;
            this.Brand.Name = "Brand";
            this.Brand.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Brand.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Model
            // 
            this.Model.DataPropertyName = "Model";
            this.Model.HeaderText = "Model";
            this.Model.MinimumWidth = 100;
            this.Model.Name = "Model";
            this.Model.Width = 128;
            // 
            // Branch
            // 
            this.Branch.DataPropertyName = "Branch";
            this.Branch.HeaderText = "Branch";
            this.Branch.Items.AddRange(new object[] {
            "CA",
            "EU",
            "BL",
            "CY"});
            this.Branch.MinimumWidth = 50;
            this.Branch.Name = "Branch";
            this.Branch.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Branch.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Branch.Width = 50;
            // 
            // IsRetail
            // 
            this.IsRetail.DataPropertyName = "IsRetail";
            this.IsRetail.HeaderText = "Retail";
            this.IsRetail.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.IsRetail.Name = "IsRetail";
            this.IsRetail.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsRetail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.IsRetail.Width = 50;
            // 
            // IsTarget
            // 
            this.IsTarget.DataPropertyName = "IsTarget";
            this.IsTarget.HeaderText = "Target";
            this.IsTarget.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.IsTarget.Name = "IsTarget";
            this.IsTarget.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsTarget.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.IsTarget.Width = 50;
            // 
            // IsCodebook
            // 
            this.IsCodebook.DataPropertyName = "IsCodebook";
            this.IsCodebook.HeaderText = "Codebook";
            this.IsCodebook.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.IsCodebook.Name = "IsCodebook";
            this.IsCodebook.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsCodebook.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // IsPartNumber
            // 
            this.IsPartNumber.DataPropertyName = "IsPartNumber";
            this.IsPartNumber.HeaderText = "PartNumber";
            this.IsPartNumber.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.IsPartNumber.Name = "IsPartNumber";
            this.IsPartNumber.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsPartNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // DeviceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(873, 630);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.opInfoDataGridView);
            this.Controls.Add(this.deviceTypeDataGridView);
            this.Controls.Add(this.countryDataGridView);
            this.Controls.Add(this.deviceCollectionDataGridView);
            this.Name = "DeviceForm";
            this.Text = "Device";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.deviceCollectionDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceCollectionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.opInfoDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.opInfoListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countryDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countryListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceTypeDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceTypeListBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView deviceCollectionDataGridView;
        private System.Windows.Forms.DataGridView opInfoDataGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView countryDataGridView;
        private System.Windows.Forms.DataGridView deviceTypeDataGridView;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.BindingSource deviceCollectionBindingSource;
        private System.Windows.Forms.BindingSource opInfoListBindingSource;
        private System.Windows.Forms.BindingSource countryListBindingSource;
        private System.Windows.Forms.BindingSource deviceTypeListBindingSource;
        private System.Windows.Forms.DataGridViewComboBoxColumn Operation;
        private System.Windows.Forms.DataGridViewComboBoxColumn Information;
        private System.Windows.Forms.DataGridViewComboBoxColumn Region;
        private System.Windows.Forms.DataGridViewComboBoxColumn CountryName;
        private System.Windows.Forms.DataGridViewComboBoxColumn DeviceName;
        private System.Windows.Forms.DataGridViewComboBoxColumn ComboBoxTNList;
        private System.Windows.Forms.DataGridViewComboBoxColumn ComboBoxIDList;
        private System.Windows.Forms.DataGridViewComboBoxColumn Brand;
        private System.Windows.Forms.DataGridViewTextBoxColumn Model;
        private System.Windows.Forms.DataGridViewComboBoxColumn Branch;
        private System.Windows.Forms.DataGridViewComboBoxColumn IsRetail;
        private System.Windows.Forms.DataGridViewComboBoxColumn IsTarget;
        private System.Windows.Forms.DataGridViewComboBoxColumn IsCodebook;
        private System.Windows.Forms.DataGridViewComboBoxColumn IsPartNumber;
    }
}