using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;

namespace DBEditor
{
    public partial class TNHeaderForm : Form
    {
        private int LastLinePrinted;            // for keeping track of lines printed

        public void resetLastLinePrinted()
        {
            LastLinePrinted = 0;
        }

        // The following defines the legacy markets bitmap
        [Flags]
        public enum LMarket
        {
            AUS = 0x0001,            // Australia
            CAN = 0x0002,            // Canada
            ENG = 0x0004,            // England
            EUR = 0x0008,            // Europe
            GER = 0x0010,            // Germany
            HKG = 0x0020,            // Hong Kong
            JAP = 0x0040,            // Japan
            KOR = 0x0080,            // Korea
            MEX = 0x0100,            // Mexico (Latin America)
            SAM = 0x0200,            // South America
            USA = 0x0400             // United States
        }

        public TNHeaderForm()
        {
            InitializeComponent();
        }

        public void Bind(TNHeader tnHeader)
        {
            if (tnHeader != null)
                tnHeaderBindingSource.DataSource = tnHeader;

            legacyMarketFlagTextBox.Text = GetLegacyMarketText(tnHeader.LegacyMarketFlag);

            StatusQ.Checked = (tnHeader.Status == "Q") ? true : false;
            StatusM.Checked = (tnHeader.Status == "M") ? true : false;
        }

        private void StatusChanged(object sender, EventArgs e)
        {
            TNHeader tnHeader = (TNHeader)tnHeaderBindingSource.DataSource;
            foreach (RadioButton btn in StatusBox.Controls)
            {
                if (btn.Checked)
                    tnHeader.Status = btn.Tag.ToString();
            }
        }

        string GetLegacyMarketText(int LMFlag)
        {
            string result = "";

            if (((LMarket)LMFlag & LMarket.AUS) > 0) AddMarket(ref result, LMarket.AUS);
            if (((LMarket)LMFlag & LMarket.CAN) > 0) AddMarket(ref result, LMarket.CAN);
            if (((LMarket)LMFlag & LMarket.ENG) > 0) AddMarket(ref result, LMarket.ENG);
            if (((LMarket)LMFlag & LMarket.EUR) > 0) AddMarket(ref result, LMarket.EUR);
            if (((LMarket)LMFlag & LMarket.GER) > 0) AddMarket(ref result, LMarket.GER);
            if (((LMarket)LMFlag & LMarket.HKG) > 0) AddMarket(ref result, LMarket.HKG);
            if (((LMarket)LMFlag & LMarket.JAP) > 0) AddMarket(ref result, LMarket.JAP);
            if (((LMarket)LMFlag & LMarket.KOR) > 0) AddMarket(ref result, LMarket.KOR);
            if (((LMarket)LMFlag & LMarket.MEX) > 0) AddMarket(ref result, LMarket.MEX);
            if (((LMarket)LMFlag & LMarket.SAM) > 0) AddMarket(ref result, LMarket.SAM);
            if (((LMarket)LMFlag & LMarket.USA) > 0) AddMarket(ref result, LMarket.USA);
            return result;
        }

        void AddMarket(ref string res, LMarket Mkt)
        {
            if (res.Length > 0)
                res += ",";
            res += Mkt.ToString();
        }

        public string GetReportHeader(string TN)
        {
            return string.Format("TN: {0} \r\nHeader Section\r\n", TN);
        }
        /// <summary>
        /// Format the specified row of data from the table for printing.
        /// The data is returned in the order it was last viewed. 
        /// if the data sort order was changed the data will be returned in that order.
        /// </summary>
        /// <param name="row">the zero based row of data requested.</param>
        /// <returns>a string sutible for printing</returns>
        public string GetReportLine()
        {
            int row = LastLinePrinted++;
            string data = null;
            TNHeader tnHeader = (TNHeader)tnHeaderBindingSource.DataSource;

            switch (row)
            {
                case 0:
                    data = string.Format("Parent TN: {0}",tnHeader.Parent_TN);
                    break;
                case 1:
                    data = string.Format("Box{0}",tnHeader.Box);
                    break;
                case 2:
                    data = string.Format("DB Search Log: {0}",tnHeader.DBSearchLog);
                    break;
                case 3:
                    data = string.Format("Homer: {0}",tnHeader.Homer);
                    break;
                case 4:
                    data = string.Format("Image: {0}",tnHeader.Image);
                    break;
                case 5:
                    data = string.Format("Status: {0}",tnHeader.Status);
                    break;
                case 6:
                    data = string.Format("Comment: {0}",tnHeader.Text);
                    break;
                case 7:
                    data = string.Format("Sender Name: {0}",
                                        tnHeader.Sender_Name);
                    break;
                case 8:
                    data = string.Format("Sender Phone: {0}",
                                         tnHeader.Sender_Phone);
                    break;
                case 9:
                    data = string.Format("Sender Address: {0}",
                                         tnHeader.Sender_Address);
                    break;
                case 10:
                    data = string.Format("Legacy Brand: {0}",
                                        tnHeader.LegacyBrand);
                    break;
                case 11:
                    data = string.Format("Legacy Device Type: {0}",
                                    tnHeader.LegacyDeviceType);
                    break;
                case 12:
                    data = string.Format("Legacy Market Flag: {0}",
                                    tnHeader.LegacyMarketFlag);
                    break;
                case 13:
                    data = string.Format("Legacy Remote Model: {0}",
                                    tnHeader.LegacyRemoteModel);
                    break;
                case 14:
                    data = string.Format("Legacy Target Model: {0}",
                                    tnHeader.LegacyTargetModel);
                    break;
                default:
                    row -= 15;
                    if (row < tnHeader.LogList.Count)
                    {
                        string o = tnHeader.LogList[row].Operator;
                        string t = tnHeader.LogList[row].Time.ToShortTimeString();
                        string c = tnHeader.LogList[row].Content;
                        data = string.Format("{0,-15} {1,-20} {2,-20}", o, t, c);
                    }
                    break;
            }
            return data;
        }
    }
}