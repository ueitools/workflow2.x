using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;

namespace DBEditor
{
    public partial class IDLevelEditPreviewForm : Form
    {
        public IDLevelEditPreviewForm()
        {
            InitializeComponent();
        }

        public void Bind(FunctionCollection functionList)
        {
            this.functionCollectionDataGridView.DataSource =
                functionList;
        }

        public void SetSummary(string summary)
        {
            this.labelSummary.Text = summary;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}