using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DBEditor
{
    public partial class PrintOptionDlg : Form
    {
        #region Private Fields
        private bool _printFunc;
        private bool _printDevice;
        private bool _printHead;
        #endregion

        #region Public flag access
        public bool PrintFunc { get { return _printFunc; } }
        public bool PrintDevice { get { return _printDevice; } }
        public bool PrintHead { get { return _printHead; ; } }
        #endregion

        public PrintOptionDlg()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this._printFunc = this.chkBoxFunc.Checked;
            this._printDevice = this.ckhBoxDevice.Checked;
            this._printHead = this.chkHead.Checked;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}