namespace DBEditor
{
    partial class SelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBoxSelectionList = new System.Windows.Forms.ComboBox();
            this.comboBoxDBList = new System.Windows.Forms.ComboBox();
            this.comboBoxItemCollectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxItemCollectionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxSelectionList
            // 
            this.comboBoxSelectionList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxSelectionList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxSelectionList.FormattingEnabled = true;
            this.comboBoxSelectionList.Location = new System.Drawing.Point(115, 21);
            this.comboBoxSelectionList.Name = "comboBoxSelectionList";
            this.comboBoxSelectionList.Size = new System.Drawing.Size(130, 21);
            this.comboBoxSelectionList.TabIndex = 0;
            this.comboBoxSelectionList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBoxSelectionList_KeyDown);
            // 
            // comboBoxDBList
            // 
            this.comboBoxDBList.DataSource = this.comboBoxItemCollectionBindingSource;
            this.comboBoxDBList.DisplayMember = "Display";
            this.comboBoxDBList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDBList.FormattingEnabled = true;
            this.comboBoxDBList.Location = new System.Drawing.Point(115, 48);
            this.comboBoxDBList.Name = "comboBoxDBList";
            this.comboBoxDBList.Size = new System.Drawing.Size(130, 21);
            this.comboBoxDBList.TabIndex = 1;
            this.comboBoxDBList.ValueMember = "Value";
            this.comboBoxDBList.SelectedIndexChanged += new System.EventHandler(this.comboBoxDBList_SelectedIndexChanged);
            // 
            // comboBoxItemCollectionBindingSource
            // 
            this.comboBoxItemCollectionBindingSource.DataSource = typeof(BusinessObject.ComboBoxItemCollection);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(45, 89);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(170, 89);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "ID/TN";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Database";
            // 
            // SelectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 127);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.comboBoxDBList);
            this.Controls.Add(this.comboBoxSelectionList);
            this.Name = "SelectionForm";
            this.Text = "Select ID/TN";
            this.Load += new System.EventHandler(this.SelectionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxItemCollectionBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxSelectionList;
        private System.Windows.Forms.ComboBox comboBoxDBList;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource comboBoxItemCollectionBindingSource;
    }
}