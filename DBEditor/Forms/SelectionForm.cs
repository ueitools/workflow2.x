using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;

namespace DBEditor
{
    public enum ListType {TNLIST, IDLIST};

    public partial class SelectionForm : Form
    {
        #region Proeprties
        private string _selected;
        private string _connStr;
        private ListType _listType;

        public string Selected
        {
            get{return _selected;}
        }

        public string ConnStr
        {
            get { return _connStr; }
        }
        #endregion

        public SelectionForm(ListType listType)
        {
            InitializeComponent();
            _listType = listType;
        }

        public void Bind()
        {
            if (_listType == ListType.TNLIST)
            {
                this.comboBoxSelectionList.DataSource =
                    DBFunctions.NonBlockingGetAllTNList();
            }
            else
            {
                this.comboBoxSelectionList.DataSource =
                    DBFunctions.NonBlockingGetAllIDList();
            }  
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            _selected = this.comboBoxSelectionList.Text;
            _connStr = this.comboBoxDBList.SelectedValue.ToString();

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void SelectionForm_Load(object sender, EventArgs e)
        {
            ComboBoxItem item = null;

            /// DB List
            ComboBoxItemCollection srcComboBoxItemList =
                new ComboBoxItemCollection();

            item = new ComboBoxItem();
            item.Display = "UEI Temp";
            item.Value = DBConnectionString.UEITEMP;
            srcComboBoxItemList.Add(item);

            item = new ComboBoxItem();
            item.Display = "UEI Public (Read Only)";
            item.Value = DBConnectionString.UEIPUBLIC;
            srcComboBoxItemList.Add(item);

            if (Environment.UserDomainName == "UEIC") {
                item = new ComboBoxItem();
                item.Display = "UEI Temp India (Read Only)";
                item.Value = DBConnectionString.UEITEMP_INDIA;
                srcComboBoxItemList.Add(item);
            }

            this.comboBoxItemCollectionBindingSource.DataSource
                = srcComboBoxItemList;
            this.comboBoxDBList.SelectedIndex = 0;
            Bind();
        }

        private void comboBoxDBList_SelectedIndexChanged(
            object sender, EventArgs e)
        {
            DAOFactory.ResetDBConnection(
                this.comboBoxDBList.SelectedValue.ToString());
            Bind();
        }

        // KeyPress didn't work.... (it works for textbox but not comboBox)
        // This might be a bug of VS 2005
        //private void comboBoxSelectionList_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    if (e.KeyChar == '\r')
        //        this.buttonOK_Click(sender, e);
        //}

        private void comboBoxSelectionList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13) // Enter key
                this.buttonOK_Click(sender, e);
        }
    }
}