namespace DBEditor
{
    partial class TNForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Function");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Device");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("TN Header");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("View Remote");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TNForm));
            this.EditorTreeView = new System.Windows.Forms.TreeView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.commitToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.CaptureStationButton = new System.Windows.Forms.ToolStripSplitButton();
            this.NonUsbCapture = new System.Windows.Forms.ToolStripMenuItem();
            this.UsbCapture = new System.Windows.Forms.ToolStripMenuItem();
            this.UsbeeCapture = new System.Windows.Forms.ToolStripMenuItem();
            this.USB_Btn = new System.Windows.Forms.ToolStripButton();
            this.CaptModeBtn = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.USBeeSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // EditorTreeView
            // 
            this.EditorTreeView.Dock = System.Windows.Forms.DockStyle.Left;
            this.EditorTreeView.Location = new System.Drawing.Point(0, 49);
            this.EditorTreeView.Name = "EditorTreeView";
            treeNode1.Name = "Function";
            treeNode1.Text = "Function";
            treeNode2.Name = "Device";
            treeNode2.Text = "Device";
            treeNode3.Name = "TNHeader";
            treeNode3.Text = "TN Header";
            treeNode4.Name = "ViewRemote";
            treeNode4.Text = "View Remote";
            treeNode4.ToolTipText = "View the Picture File";
            this.EditorTreeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4});
            this.EditorTreeView.Size = new System.Drawing.Size(140, 418);
            this.EditorTreeView.TabIndex = 2;
            this.EditorTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.EditorTreeView_AfterSelect);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator2,
            this.commitToolStripButton,
            this.toolStripSeparator3,
            this.printToolStripButton,
            this.toolStripSeparator,
            this.CaptureStationButton,
            this.USB_Btn,
            this.CaptModeBtn});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(642, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Enabled = false;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.newToolStripButton.Text = "&New";
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Enabled = false;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "&Open";
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Enabled = false;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "&Save";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // commitToolStripButton
            // 
            this.commitToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.commitToolStripButton.Enabled = false;
            this.commitToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("commitToolStripButton.Image")));
            this.commitToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.commitToolStripButton.Name = "commitToolStripButton";
            this.commitToolStripButton.Size = new System.Drawing.Size(64, 22);
            this.commitToolStripButton.Text = "Save to DB";
            this.commitToolStripButton.Click += new System.EventHandler(this.commitToolStripButton_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripButton.Image")));
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printToolStripButton.Text = "&Print";
            this.printToolStripButton.Click += new System.EventHandler(this.printToolStripButton_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // CaptureStationButton
            // 
            this.CaptureStationButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.CaptureStationButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.CaptureStationButton.DropDownButtonWidth = 0;
            this.CaptureStationButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NonUsbCapture,
            this.UsbCapture,
            this.UsbeeCapture});
            this.CaptureStationButton.Image = ((System.Drawing.Image)(resources.GetObject("CaptureStationButton.Image")));
            this.CaptureStationButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CaptureStationButton.Name = "CaptureStationButton";
            this.CaptureStationButton.Size = new System.Drawing.Size(120, 22);
            this.CaptureStationButton.Text = "Select Capture Station";
            this.CaptureStationButton.Click += new System.EventHandler(this.CaptureStationButton_Click);
            this.CaptureStationButton.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.CaptureStationButton_DropDownItemClicked);
            // 
            // NonUsbCapture
            // 
            this.NonUsbCapture.Name = "NonUsbCapture";
            this.NonUsbCapture.Size = new System.Drawing.Size(158, 22);
            this.NonUsbCapture.Text = "Non-USB";
            // 
            // UsbCapture
            // 
            this.UsbCapture.Name = "UsbCapture";
            this.UsbCapture.Size = new System.Drawing.Size(158, 22);
            this.UsbCapture.Text = "USB";
            // 
            // UsbeeCapture
            // 
            this.UsbeeCapture.Name = "UsbeeCapture";
            this.UsbeeCapture.Size = new System.Drawing.Size(158, 22);
            this.UsbeeCapture.Text = "USBee Capture";
            // 
            // USB_Btn
            // 
            this.USB_Btn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.USB_Btn.CheckOnClick = true;
            this.USB_Btn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.USB_Btn.Image = ((System.Drawing.Image)(resources.GetObject("USB_Btn.Image")));
            this.USB_Btn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.USB_Btn.Margin = new System.Windows.Forms.Padding(0, 1, 10, 2);
            this.USB_Btn.Name = "USB_Btn";
            this.USB_Btn.Size = new System.Drawing.Size(30, 22);
            this.USB_Btn.Text = "USB";
            this.USB_Btn.Visible = false;
            this.USB_Btn.Click += new System.EventHandler(this.USB_Btn_Click);
            // 
            // CaptModeBtn
            // 
            this.CaptModeBtn.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.CaptModeBtn.CheckOnClick = true;
            this.CaptModeBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.CaptModeBtn.Image = ((System.Drawing.Image)(resources.GetObject("CaptModeBtn.Image")));
            this.CaptModeBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CaptModeBtn.Margin = new System.Windows.Forms.Padding(0, 1, 10, 2);
            this.CaptModeBtn.Name = "CaptModeBtn";
            this.CaptModeBtn.Size = new System.Drawing.Size(79, 22);
            this.CaptModeBtn.Text = "Capture Mode";
            this.CaptModeBtn.CheckedChanged += new System.EventHandler(this.CaptModeBtn_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(642, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.viewFileToolStripMenuItem,
            this.toolStripSeparator1,
            this.USBeeSettings});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.cutToolStripMenuItem.Text = "Cut";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // viewFileToolStripMenuItem
            // 
            this.viewFileToolStripMenuItem.Name = "viewFileToolStripMenuItem";
            this.viewFileToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.viewFileToolStripMenuItem.Text = "View File";
            this.viewFileToolStripMenuItem.Click += new System.EventHandler(this.viewFileToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(155, 6);
            // 
            // USBeeSettings
            // 
            this.USBeeSettings.Name = "USBeeSettings";
            this.USBeeSettings.Size = new System.Drawing.Size(158, 22);
            this.USBeeSettings.Text = "USBee Settings";
            this.USBeeSettings.Click += new System.EventHandler(this.USBeeSettings_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(140, 445);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(502, 22);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // TNForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 467);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.EditorTreeView);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TNForm";
            this.Text = "TN Editor";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TNForm_FormClosing);
            this.Load += new System.EventHandler(this.TNForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView EditorTreeView;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton commitToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton printToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton CaptModeBtn;
        private System.Windows.Forms.ToolStripButton USB_Btn;
        private System.Windows.Forms.ToolStripSplitButton CaptureStationButton;
        private System.Windows.Forms.ToolStripMenuItem NonUsbCapture;
        private System.Windows.Forms.ToolStripMenuItem UsbCapture;
        private System.Windows.Forms.ToolStripMenuItem UsbeeCapture;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem USBeeSettings;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;

    }
}