using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BusinessObject;
using System.Configuration;
using System.IO;
using CommonForms;
using IRUSBLib;
using UsbNi;

namespace DBEditor
{
    public partial class TNForm : Form
    {
        TN _oTN = new TN(); 
        TNHeaderForm _tnHeaderForm = new TNHeaderForm();
        FunctionForm _functionForm = new FunctionForm();
        DeviceForm _deviceForm = new DeviceForm();
        int _tn;
//        public bool USBCaptureMode = false;
        public string FunctionKeyFile;
        private int _podNumber;

        /// <summary>
        /// 
        /// </summary>
        public TNForm(int tn)
        {
            InitializeComponent();
            FunctionKeyFile = ConfigurationSettings.AppSettings["FUNCTIONKEYS"];
            _functionForm._keys = new KeyLists(FunctionKeyFile);

            _tn = tn;
            _tnHeaderForm.MdiParent = this;
            _functionForm.MdiParent = this;
            _deviceForm.MdiParent = this;

            _podNumber = Configuration.GetConfigItem("DBEditor", "USBee Pod Number", 0);
        }

        public void EnableSave()
        {
            this.commitToolStripButton.Enabled = true;
        }

        /// <summary>
        /// TODOD: Please Refactor 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditorTreeView_AfterSelect(
            object sender, TreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "TNHeader":
                    ShowForm(_tnHeaderForm);
                    break;
                case "Function":
                    ShowForm(_functionForm);
                    break;
                case "Device":
                    _deviceForm.FillComboBoxIDList(_tn, _oTN.FunctionList);
                    ShowForm(_deviceForm);
                    break;
                case "ViewRemote":
                    ViewRemotePicture(_tn);
                    break;
            }
        }

        private void ShowForm(Form selected)
        {
            selected.BringToFront();
            selected.Visible = true;
        }

        private void ViewRemotePicture(int _tn)
        {
            /// open the picture locate in G:\Lib_Capture\Img\PICTURES 
            string TNpic_path = ConfigurationSettings.AppSettings["TNPICPATH"];

            string strTN = string.Format("Tn{0:D5}", _tn);
            int tnGrp = _tn - (_tn % 1000);

            if (_tn >= 90000)
                TNpic_path += "TN90000-\\";     // Highest numbered group (so far)
            else                                // else build the directory group name
                TNpic_path += string.Format("TN{0:D5}-TN{1:D5}\\", tnGrp, (tnGrp + 999));

            Process myProcess = new Process();
            try
            {
                string[] images = Directory.GetFiles(TNpic_path, strTN + ".*",
                                                SearchOption.AllDirectories);
                myProcess.StartInfo.FileName = images[0];
                myProcess.StartInfo.Verb = "";
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.Start();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        private List<string> FindFiles(string dir, string strTN)
        {
            List<string> file_list = new List<string>();
            DirectoryInfo tn_dir = new DirectoryInfo(dir);
            foreach (FileInfo fi in tn_dir.GetFiles(strTN + "*.jpg"))
            {
                file_list.Add(fi.FullName);   
            }
            return file_list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TNForm_Load(object sender, EventArgs e)
        {
            UpdateCaptureModeUI();

            CaptureFunctions.CaptureMode captureMode = CaptureFunctions.CaptureMode.NonUSB;
            if (UsbCapture.Visible) {
                captureMode = CaptureFunctions.CaptureMode.USB;
            }
            UpdateCaptureModeSelection(captureMode);
            CaptureFunctions.USBCaptureMode = captureMode;

            EditorTreeView.ExpandAll();
            _functionForm.ShowIDColumn(_tn);
            _deviceForm.ShowIDColumn();

            _functionForm.WindowState = FormWindowState.Maximized;
            Bind();
        }

        private void UpdateCaptureModeUI() {
            NonUsbCapture.Visible = true;
            UsbCapture.Visible = false;
            UsbeeCapture.Visible = false;

            try {
                USBRamni ramni = new USBRamni();
                bool usbCaptureFound = ramni.FindUSBCaptureDevice();
                if (usbCaptureFound) {
                    UsbCapture.Visible = true;
                }
            } catch {}

            try {
                int connectionStatus = 0;
                IrUSBCapture irUSBCapture = new IrUSBCaptureClass();
                irUSBCapture.IsPODConnected(_podNumber, ref connectionStatus);
                if (connectionStatus == 1) {
                    UsbeeCapture.Visible = true;
                }
            } catch {} 
        }

        private void UpdateCaptureModeSelection(CaptureFunctions.CaptureMode captureMode) {
            NonUsbCapture.Checked = captureMode == CaptureFunctions.CaptureMode.NonUSB;
            UsbCapture.Checked = captureMode == CaptureFunctions.CaptureMode.USB;
            UsbeeCapture.Checked = captureMode == CaptureFunctions.CaptureMode.USBee;

            string captureStationText = string.Empty;
            switch (captureMode) {
                case CaptureFunctions.CaptureMode.USB:
                    UsbCapture.Checked = true;
                    captureStationText = UsbCapture.Text;
                    break;
                case CaptureFunctions.CaptureMode.USBee:
                    UsbeeCapture.Checked = true;
                    captureStationText = UsbeeCapture.Text;
                    break;
                default:
                    NonUsbCapture.Checked = true;
                    captureStationText = NonUsbCapture.Text;
                    break;
            }
            CaptureStationButton.Text = captureStationText;
        }

        private void Bind()
        {
            try
            {
                string strDBname="";
                DAOFactory.BeginTransaction();
                _oTN.Load(_tn);

                _tnHeaderForm.Bind(_oTN.Header);
                _functionForm.Bind(_oTN.FunctionList);
                _deviceForm.Bind(_oTN.DeviceList);
                _deviceForm.ShowIDColumn(); 

                if (DAOFactory.GetDBConnectionString() == 
                    DBConnectionString.UEITEMP)
                    strDBname = "UEI Temp DB";
                else
                    strDBname = "UEI Public DB";

                this.Text = "Editing TN: " + _tn + " in " + strDBname;
                _functionForm.Show();
                _functionForm.BringToFront();

                TNHelper.CleanupTN(_tn);
                TNHelper.RetrieveTN(_tn);

                DAOFactory.CommitTransaction();
                _functionForm.SetRowColorsByID();
            }
            catch (Exception ex)
            {
                DAOFactory.RollBackTransaction();
                MessageBox.Show(ex.Message, "Database Checkout Canceled");
            }
        }

        /// <summary>
        /// Make sure you are writing to Temp DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void commitToolStripButton_Click(
            object sender, EventArgs e)
        {
            _functionForm.UpdateTnZip();
            if (DAOFactory.GetDBConnectionString() !=
                DBConnectionString.UEITEMP)
            {
                throw new ApplicationException(
                    "Committing to Public DB: " +
                    DAOFactory.GetDBConnectionString());
            }

            string comment = this._tnHeaderForm.LogCommentTextBox.Text;
            if (!string.IsNullOrEmpty(comment))
            {
                Log log = Log.Create(_oTN.Header.TN, comment);
                _oTN.Header.LogList.Add(log);
                this._tnHeaderForm.LogCommentTextBox.Clear();
            }

            try
            {
                DAOFactory.BeginTransaction();

                TNHelper.WriteTnToNetwork(_tn);
                this._tnHeaderForm.Validate();
                this._functionForm.Validate();
                this._deviceForm.Validate();

                this._oTN.Update();

                DAOFactory.CommitTransaction();

                MessageBox.Show("TN has been saved " +
                    "to the database", "Success");
            }
            catch (Exception ex)
            {
                DAOFactory.RollBackTransaction();
                MessageBox.Show(ex.Message, "Save Failed");
            }
        }

        private void TNForm_FormClosing(
            object sender, FormClosingEventArgs e)
        {
            TNHelper.CleanupTN(_tn);
        }

        private void CaptModeBtn_CheckedChanged(object sender, EventArgs e)
        {
            TreeNode[] item = EditorTreeView.Nodes.Find("Function", false);
            EditorTreeView.SelectedNode = item[0];
            EditorTreeView.Visible = !CaptModeBtn.Checked;
            _functionForm.CaptModeBtn_CheckedChanged(sender, e);
            ShowForm(_functionForm);
        }

        private void USB_Btn_Click(object sender, EventArgs e)
        {
            CaptureFunctions.USBCaptureMode = GetCaptureMode();
        }

        private CaptureFunctions.CaptureMode GetCaptureMode() {
            CaptureFunctions.CaptureMode captureMode = CaptureFunctions.CaptureMode.NonUSB;
            if (UsbCapture.Checked) {
                captureMode = CaptureFunctions.CaptureMode.USB;
            } else if (UsbeeCapture.Checked) {
                captureMode = CaptureFunctions.CaptureMode.USBee;
            }

            return captureMode;
        }

        private void CaptureStationButton_Click(object sender, EventArgs e) {
            CaptureStationButton.ShowDropDown();
        }

        private void CaptureStationButton_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e) {
            NonUsbCapture.Checked = false;
            UsbCapture.Checked = false;
            UsbeeCapture.Checked = false;
            ((ToolStripMenuItem)e.ClickedItem).Checked = true;

            CaptureFunctions.CaptureMode captureMode = GetCaptureMode();
            UpdateCaptureModeSelection(captureMode);
            CaptureFunctions.USBCaptureMode = captureMode;
        }

        private void USBeeSettings_Click(object sender, EventArgs e) {
            SimpleInputBox inputBox = new SimpleInputBox();
            inputBox.InputString = _podNumber.ToString();
            inputBox.Caption = "Enter in pod number of USBee unit.";
            if (inputBox.ShowDialog(this) == DialogResult.OK) {
                int result = 0;
                if (int.TryParse(inputBox.InputString, out result)) {
                    _podNumber = result;
                }

                Configuration.SetConfigItem("DBEditor", "USBee Pod Number", _podNumber);
                UpdateCaptureModeUI();
            }
        }

        public void SetStatus(string statusMessage, bool error) {
            if (statusMessage != string.Empty) {
                toolStripStatusLabel.Text = statusMessage;
                toolStripStatusLabel.BackColor = Color.Tan;
                if (error) {
                    toolStripStatusLabel.BackColor = Color.Red;
                }
            }
        }
    }
}
