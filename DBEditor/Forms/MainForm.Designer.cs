namespace DBEditor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.partialIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateIDNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unLockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unLockTNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unLockPartialIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iDLevelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intronToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.logNewRemoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.unLockToolStripMenuItem,
            this.iDLevelToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(566, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tNToolStripMenuItem,
            this.partialIDToolStripMenuItem,
            this.updateIDNameToolStripMenuItem,
            this.toolStripMenuItem1,
            this.logNewRemoteToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // tNToolStripMenuItem
            // 
            this.tNToolStripMenuItem.Name = "tNToolStripMenuItem";
            this.tNToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.tNToolStripMenuItem.Text = "TN";
            this.tNToolStripMenuItem.Click += new System.EventHandler(this.tNToolStripMenuItem_Click);
            // 
            // partialIDToolStripMenuItem
            // 
            this.partialIDToolStripMenuItem.Name = "partialIDToolStripMenuItem";
            this.partialIDToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.partialIDToolStripMenuItem.Text = "Partial ID";
            this.partialIDToolStripMenuItem.Click += new System.EventHandler(this.iDToolStripMenuItem_Click);
            // 
            // updateIDNameToolStripMenuItem
            // 
            this.updateIDNameToolStripMenuItem.Name = "updateIDNameToolStripMenuItem";
            this.updateIDNameToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.updateIDNameToolStripMenuItem.Text = "Update ID Name";
            this.updateIDNameToolStripMenuItem.Click += new System.EventHandler(this.updateIDNameToolStripMenuItem_Click);
            // 
            // unLockToolStripMenuItem
            // 
            this.unLockToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unLockTNToolStripMenuItem,
            this.unLockPartialIDToolStripMenuItem});
            this.unLockToolStripMenuItem.Name = "unLockToolStripMenuItem";
            this.unLockToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.unLockToolStripMenuItem.Text = "UnLock";
            // 
            // unLockTNToolStripMenuItem
            // 
            this.unLockTNToolStripMenuItem.Name = "unLockTNToolStripMenuItem";
            this.unLockTNToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.unLockTNToolStripMenuItem.Text = "TN";
            this.unLockTNToolStripMenuItem.Click += new System.EventHandler(this.unLockTNToolStripMenuItem_Click);
            // 
            // unLockPartialIDToolStripMenuItem
            // 
            this.unLockPartialIDToolStripMenuItem.Name = "unLockPartialIDToolStripMenuItem";
            this.unLockPartialIDToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.unLockPartialIDToolStripMenuItem.Text = "Partial ID";
            this.unLockPartialIDToolStripMenuItem.Click += new System.EventHandler(this.unLockPartialIDToolStripMenuItem_Click);
            // 
            // iDLevelToolStripMenuItem
            // 
            this.iDLevelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataToolStripMenuItem,
            this.intronToolStripMenuItem,
            this.labelToolStripMenuItem});
            this.iDLevelToolStripMenuItem.Name = "iDLevelToolStripMenuItem";
            this.iDLevelToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.iDLevelToolStripMenuItem.Text = "ID Level";
            // 
            // dataToolStripMenuItem
            // 
            this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
            this.dataToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.dataToolStripMenuItem.Text = "Data";
            this.dataToolStripMenuItem.Click += new System.EventHandler(this.dataToolStripMenuItem_Click);
            // 
            // intronToolStripMenuItem
            // 
            this.intronToolStripMenuItem.Name = "intronToolStripMenuItem";
            this.intronToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.intronToolStripMenuItem.Text = "Intron";
            this.intronToolStripMenuItem.Click += new System.EventHandler(this.intronToolStripMenuItem_Click);
            // 
            // labelToolStripMenuItem
            // 
            this.labelToolStripMenuItem.Name = "labelToolStripMenuItem";
            this.labelToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.labelToolStripMenuItem.Text = "Label";
            this.labelToolStripMenuItem.Click += new System.EventHandler(this.labelToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(175, 6);
            // 
            // logNewRemoteToolStripMenuItem
            // 
            this.logNewRemoteToolStripMenuItem.Name = "logNewRemoteToolStripMenuItem";
            this.logNewRemoteToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.logNewRemoteToolStripMenuItem.Text = "Log New Remote...";
            this.logNewRemoteToolStripMenuItem.Click += new System.EventHandler(this.logNewRemoteToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 440);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "DBEditor";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tNToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem partialIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unLockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unLockTNToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unLockPartialIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iDLevelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem intronToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem labelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateIDNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem logNewRemoteToolStripMenuItem;
    }
}