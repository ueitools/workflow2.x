using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;

namespace DBEditor
{
    public partial class DeviceForm
    {
        /// <summary>
        /// Country
        /// </summary>
        /// <param name="countryList"></param>
        /// <returns></returns>
        private StringCollection GetRegionList(
            CountryCollection countryList)
        {
            StringCollection strList = new StringCollection();
            foreach (Country country in countryList)
            {
                if (strList.Contains(country.Region) != true)
                    strList.Add(country.Region);
            }

            return strList;
        }

        private CountryCollection GetCountryList(string region)
        {
            CountryCollection list = new CountryCollection();
            foreach (Country country in _countryList)
            {
                if (country.Region == region)
                {
                    list.Add(country);
                }
            }

            return list;
        }

        private void countryDataGridView_CellBeginEdit(
            object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == this.CountryName.Index)
            {
                DataGridViewComboBoxCell dgcb =
                    (DataGridViewComboBoxCell)countryDataGridView
                    [e.ColumnIndex, e.RowIndex];

                string region = (string)this.countryDataGridView
                    [e.ColumnIndex - 1, e.RowIndex].Value;

                dgcb.DataSource = GetCountryList(region);
            }
        }

        private void countryDataGridView_CellEndEdit(
            object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == this.CountryName.Index)
            {
                DataGridViewComboBoxCell dgcb =
                    (DataGridViewComboBoxCell)countryDataGridView
                    [e.ColumnIndex, e.RowIndex];

                dgcb.DataSource = _countryList;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opInfoList"></param>
        /// <returns></returns>
        private StringCollection GetOperationList(OpInfoCollection opInfoList)
        {
            StringCollection strList = new StringCollection();
            foreach (OpInfo opInfo in opInfoList)
            {
                if (strList.Contains(opInfo.Operation) != true)
                    strList.Add(opInfo.Operation);
            }

            return strList;
        }

        private OpInfoCollection GetInformationList(string operation)
        {
            OpInfoCollection list = new OpInfoCollection();
            foreach (OpInfo opInfo in _opInfoList)
            {
                if (opInfo.Operation == operation)
                {
                    list.Add(opInfo);
                }
            }

            return list;
        }

        private void opInfoDataGridView_CellBeginEdit(
            object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == this.Information.Index)
            {
                DataGridViewComboBoxCell dgcb =
                    (DataGridViewComboBoxCell)opInfoDataGridView
                    [e.ColumnIndex, e.RowIndex];

                string operation = (string)this.opInfoDataGridView
                    [e.ColumnIndex - 1, e.RowIndex].Value;

                dgcb.DataSource = GetInformationList(operation);
            }
        }

        private void opInfoDataGridView_CellEndEdit(
            object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == this.CountryName.Index)
            {
                DataGridViewComboBoxCell dgcb =
                    (DataGridViewComboBoxCell)opInfoDataGridView
                    [e.ColumnIndex, e.RowIndex];

                dgcb.DataSource = _opInfoList;
            }
        }

        ///
        /// PRINT
        ///
        #region print
        // for keeping track of lines printed
        private int LastLinePrinted;

        public void resetLastLinePrinted()
        {
            LastLinePrinted = 0;
        }

        /// <summary>
        /// crude report formatter (needs lots of work)
        /// </summary>
        /// <returns>a single formatted string from the screen data</returns>
        public string GetIDReportLine()
        {
            string data = null;
            string isTarget = "";
            int row = LastLinePrinted++;
            if (row >= 0 && row < deviceCollectionDataGridView.Rows.Count)
            {
                Device IDDeviceData = (Device)deviceCollectionDataGridView.
                    Rows[row].DataBoundItem;

                if (IDDeviceData == null)
                    return null;

                if (IDDeviceData.IsTarget == "Y")
                    isTarget = "T";
                else
                    isTarget = "R";

                data = string.Format(
                    "{0,-20} {1,-25}  {2,-5}  {3,-6}  {4,-6} " +
                    "{5,-8}  {6,-5}  {7,-6}",
                    IDDeviceData.Brand,
                    IDDeviceData.Model, IDDeviceData.Branch,
                    IDDeviceData.IsRetail, isTarget,
                    IDDeviceData.IsCodebook, IDDeviceData.IsPartNumber,
                    IDDeviceData.TN);
            }
            if (data == null)
                LastLinePrinted = 0;
            return data;
        }

        public string GetIDReportHeader(string ID)
        {
            string head = string.Format(
                "ID: {0} \r\n{1,-20} {2,-25}  {3,-5} {4,-6}  " +
                "{5,-6}  {6,-8}  {7,-5}  {8,-6}\r\n", ID, "Brand",
                "Model", "Branch", "Retail", "Target", "Codebook",
                "Part#", "TN") + "\r\n";
            return head;
        }

        public string GetTNReportLine()
        {
            string data = null;
            string isTarget = "";
            int row = LastLinePrinted++;
            if (row >= 0 && row < deviceCollectionDataGridView.Rows.Count)
            {
                Device IDDeviceData = (Device)deviceCollectionDataGridView.
                    Rows[row].DataBoundItem;
                if (IDDeviceData == null)
                    return null;

                if (IDDeviceData.IsTarget == "Y")
                    isTarget = "T";
                else
                    isTarget = "R";

                data = string.Format("{0,-20} {1,-25}  {2,-5}  " +
                    "{3,-6}  {4,-6}  {5,-8}  {6,-5}  {7,-6}",
                    IDDeviceData.Brand,
                    IDDeviceData.Model, IDDeviceData.Branch,
                    IDDeviceData.IsRetail, isTarget,
                    IDDeviceData.IsCodebook, IDDeviceData.IsPartNumber,
                    IDDeviceData.ID);
            }
            if (data == null)
                LastLinePrinted = 0;
            return data;
        }

        public string GetTNReportHeader(string TN)
        {
            string head = string.Format(
                "TN: {0} \r\n{1,-20} {2,-25}  {3,-5} {4,-6}  " +
                "{5,-6}  {6,-8}  {7,-5}  {8,-6}\r\n", TN, "Brand",
                "Model", "Branch", "Retail", "Target", "Codebook",
                "Part#", "ID") + "\r\n";
            return head;
        }
        #endregion
    }
}