using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;
using System.Text;
using BusinessObject;

namespace DBEditor
{
    public class TNHelper
    {
        public const string LogFileName = "ChangeLog.txt";
        public const string LogStartMessage = "Checked Out";
        /// <summary>
        /// Create the expected zip file name from a TN number
        /// </summary>
        /// <param name="tn"></param>
        /// <returns></returns>
        public static string TNZipName(int tn)
        {
            return String.Format("TN{0:D4}.ZIP", tn);
        }

        /// <summary>
        /// Create a directory name or the TN to unzip into
        /// </summary>
        /// <param name="tn"></param>
        /// <returns></returns>
        public static string TNUnzipDir(int tn)
        {
            return String.Format("TN{0:D4}\\", tn);
        }

        /// <summary>
        /// If the TN ZIP files does not alreay exist, 
        /// copy it from the CAPDIR and extract it to a TN working directory
        /// </summary>
        /// <param name="tn"></param>
        public static void RetrieveTN(int tn)
        {
            string workPath = Common.GetWorkPath();
            string CapPath = Common.GetConfig("CAPDIR");
            string SourceFile = TNZipName(tn);
            string UnzipDir = TNUnzipDir(tn);

            if (File.Exists(workPath + SourceFile))     // already have a local copy?
                return;

            if (!File.Exists(CapPath + SourceFile))     // does original TN exist?
            {
                CreateTN(tn);
                return;
            }
            File.Copy(CapPath + SourceFile, workPath + SourceFile);

            if (Directory.Exists(Common.GetWorkPath() + UnzipDir))
                Directory.Delete(Common.GetWorkPath() + UnzipDir, true); // contents as well

            Directory.CreateDirectory(workPath + UnzipDir);
            ZipHelper.ExtractAll(workPath + SourceFile, workPath + UnzipDir);
            Log(tn, LogStartMessage);
        }

        public static void WriteTnToNetwork(int tn)
        {
            string workPath = Common.GetWorkPath();
            string capPath = Common.GetConfig("CAPDIR");
            string sourceFile = TNZipName(tn);
            int backupNum = 0;
            string backupFile;

            if (!File.Exists(workPath + sourceFile))     // Don't have a local copy?
                return;

            StringCollection log = ReadLogFile(tn);
            if (log[log.Count - 1].StartsWith(LogStartMessage))
                return;

            do
            {
                backupNum++;
                backupFile = string.Format("TN{0:D4}-{1}.ZIP", tn, backupNum);
            }
            while (File.Exists(workPath + backupFile) && backupNum < 500);

            if (File.Exists(capPath + sourceFile))
                File.Move(capPath + sourceFile, workPath + backupFile);

            File.Copy(workPath + sourceFile, capPath + sourceFile);
        }

        /// <summary>
        /// If the TN or the TN working directory exists remove them completely
        /// </summary>
        /// <param name="tn"></param>
        public static void CleanupTN(int tn)
        {
            try
            {
                if (File.Exists(Common.GetWorkPath() + TNZipName(tn)))
                    File.Delete(Common.GetWorkPath() + TNZipName(tn));
                if (Directory.Exists(Common.GetWorkPath() + TNUnzipDir(tn)))
                    Directory.Delete(Common.GetWorkPath() + TNUnzipDir(tn), true); // contents as well
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "TN Access Error");
            }
        }

        public static void CreateTN(int tn)
        {
            string workPath = Common.GetWorkPath();

            CleanupTN(tn);                          // remove all trace if exists
            Directory.CreateDirectory(workPath + TNUnzipDir(tn));
            PrjFileHelper.CreatePrjFile(tn);
            Log(tn, "TN Created");

            StringCollection FileEntries = new StringCollection();
            FileEntries.Add(PrjFileHelper.PrjFileName(tn));
            FileEntries.Add(LogFileName);

            ZipHelper.CreateZipFile(workPath + TNZipName(tn), FileEntries,
                                    workPath + TNUnzipDir(tn));
        }

        /// <summary>
        /// Create/Append a message into the ChangeLog for a TN
        /// </summary>
        /// <param name="tn"></param>
        /// <param name="Message"></param>
        public static void Log(int tn, string Message)
        {
            string path = Common.GetWorkPath() + TNUnzipDir(tn);
            if (!Directory.Exists(path))
                return;

            string FileName = path + LogFileName;

            using (StreamWriter sw = File.AppendText(FileName))
            {
                sw.WriteLine(Message + 
                    "  [" + SystemInformation.UserName.ToLower() + 
                    " - "  + DateTime.Now.ToString() + "]");
                sw.Close();
            }
        }

        public static StringCollection ReadLogFile(int tn)
        {
            StringCollection LogContent = new StringCollection();
            string LogFile = LogFileName;
            string line;
            StreamReader logStreamIn = new StreamReader(Common.GetWorkPath() +
                                                        TNUnzipDir(tn) + LogFile);

            while ((line = logStreamIn.ReadLine()) != null)
                LogContent.Add(line);

            logStreamIn.Close();
            return LogContent;
        }

        public static Hashtable RenumberCaptureFiles(int tn, HashtableCollection TableEntries)
        {
            string prjFileName = PrjFileHelper.PrjFileName(tn);
            Hashtable RenameTable;
            RenameTable = PrjFileHelper.RenumberPrjFileEntries(tn, TableEntries);

            string SourceDir = Common.GetWorkPath() + TNUnzipDir(tn);
            string DestDir = Common.GetWorkPath() + Path.GetRandomFileName(); //"!!TNTemp\\";
            while (Directory.Exists(DestDir))
                DestDir = Common.GetWorkPath() + Path.GetRandomFileName();

            DestDir += "\\";
            Directory.CreateDirectory(DestDir);

            foreach (DictionaryEntry entry in RenameTable)
            {
                File.Move(SourceDir + entry.Key.ToString(),
                            DestDir + entry.Value.ToString());
            }

            File.Move(SourceDir + prjFileName, DestDir + prjFileName);
            File.Move(SourceDir + LogFileName, DestDir + LogFileName);
            Directory.Delete(SourceDir, true);
            Directory.Move(DestDir, SourceDir);
            return RenameTable;
        }

        /// <summary>
        /// Check for TN capture changes, if changed - Update the .ZIP file and save to the CAPDIR
        /// </summary>
        /// <param name="tn"></param>
        public static void UpdateTNZip(int tn, StringCollection ProjectFiles)
        {
            ProjectFiles.Add(PrjFileHelper.PrjFileName(tn));
            ProjectFiles.Add(LogFileName);
            ZipHelper.CreateZipFile(Common.GetWorkPath() + TNZipName(tn),
                                    ProjectFiles,
                                    Common.GetWorkPath() + TNUnzipDir(tn));
        }

    }
}
