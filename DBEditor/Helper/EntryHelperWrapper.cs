using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Configuration;
using Microsoft.Win32;
using BusinessObject;
using EntryHelper;

namespace DBEditor
{
    public class EntryHelperWrapper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string Map(int tn, string fileName, 
            string id, string workingDirectory)
        {
            string data = "";
            string tnZipArchive = GetTNZipArchive(tn, workingDirectory);
            string outFilePath = Path.GetTempPath();

            TnZipFileExistsOrCopy(tn, tnZipArchive);
            ZipHelper.ExtractFile(tnZipArchive, fileName, outFilePath);

            FunctionMapper functionMapper = new FunctionMapper();
            string error = functionMapper.MapFunction(outFilePath, 
                                                        fileName, id, out data);
            if (!string.IsNullOrEmpty(error))
            {
                DialogResult res = MessageBox.Show(error, "Continue?",
                                                    MessageBoxButtons.OKCancel,
                                                    MessageBoxIcon.Question);
                if (res == DialogResult.Cancel)
                    throw new Exception(error);
            }

            return data;
        }

        public static void RetrieveU1FromZipFile(int tn, string fileName, string workingdir)
        {
            string tnZipArchive = GetTNZipArchive(tn, workingdir);
            string outFilePath = workingdir;

            TnZipFileExistsOrCopy(tn, tnZipArchive);
            ZipHelper.ExtractFile(tnZipArchive, fileName, outFilePath);
        }

        private static void TnZipFileExistsOrCopy(int tn, string tnZipArchive)
        {
            if (File.Exists(tnZipArchive))
                return;

            string title = "File not found";
            string text = "The TN Zip file was not found in the working directory.\n ";
            text += "Do you want to copy it from the TN directory?";
            MessageBoxButtons buttons = MessageBoxButtons.OKCancel;

            if (MessageBox.Show(text, title, buttons, MessageBoxIcon.Question) == DialogResult.OK)
            {
                string path = Common.GetConfig("CAPDIR");
                string SourceFile = GetTNZipArchive(tn, path);
                File.Copy(SourceFile, tnZipArchive);
            }
            else
            {
                throw new Exception("The TN Zip file was not in the working directory.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string Import(int executor_code, 
            string file, string label)
        {
            TNDataImporter importer =
                new TNDataImporter(executor_code, file);

            return importer.GetDataForLabel(label);
        }

        public static HashtableCollection ImportAll(int executor_code, string file)
        {
            TNDataImporter importer =
                new TNDataImporter(executor_code, file);

            return importer.GetAllLabelsAndData();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Nullable<int> GetExecutor_Code(string id)
        {
            IDHeader header = DAOFactory.IDHeader().Select(id);
            if (header != null)
                return header.Executor_Code;
            else
                return null;
        }

        public static string GetIntron(string mode, string label)
        {
            IntronAssigner assigner = new IntronAssigner();
            return assigner.GetIntron(mode, label); 
        }

        /// <summary>
        /// 4 digit     400     --> TN0400.ZIP
        /// 5 digit   10100     --> TN10100.ZIP
        /// </summary>
        /// <param name="tn"></param>
        /// <returns></returns>
        private static string GetTNZipArchive(int tn, string workingDirectory)
        {
            string strTN = null;
            if (tn < 10000)
                strTN = String.Format("{0:D4}", tn);
            else
                strTN = String.Format("{0}", tn);

            string tnZipArchive = workingDirectory
                + "TN" + strTN + ".ZIP";
            return tnZipArchive;
        }
    }
}
