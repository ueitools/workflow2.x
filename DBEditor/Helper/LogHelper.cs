using System;
using System.Collections.Generic;
using System.Text;

using System.Security.Principal;
using BusinessObject;

namespace DBEditor
{
    public static class LogHelper
    {
        public static Log CreateLog(int tn, string contents)
        {
            Log log = new Log();
            log.TN = tn;
            log.Time = DateTime.Now;
            log.Content = contents;

            string name = WindowsIdentity.GetCurrent().Name;
            if (name.Contains("\\"))
            {
                log.Operator = name.Substring(name.LastIndexOf("\\") + 1);
            }

            return log;
        }

        public static Log CreateLog(string id, string contents)
        {
            Log log = new Log();
            log.ID = id;
            log.Time = DateTime.Now;
            log.Content = contents;

            string name = WindowsIdentity.GetCurrent().Name;
            if (name.Contains("\\"))
            {
                log.Operator = name.Substring(name.LastIndexOf("\\") + 1);
            }

            return log;
        }
    }
}
