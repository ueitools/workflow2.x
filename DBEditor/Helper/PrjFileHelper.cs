using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;
using BusinessObject;

namespace DBEditor
{
    public class PrjFileHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tn"></param>
        /// <returns></returns>
        public static string PrjFileName(int tn)
        {
            return string.Format("L{0:D4}.PRJ", tn);
        }

        public static Hashtable RenumberPrjFileEntries(int tn, HashtableCollection TableEntries)
        {
            int capNum = 0;
            string line;
            string file;
            string TNUnzipDir = TNHelper.TNUnzipDir(tn);
            string PrjFile = PrjFileName(tn);
            StringCollection prjContent = ReadPrjFile(tn);
            string SourceDir = Common.GetWorkPath() + TNUnzipDir;

            // write unchanging portion of PrjFile
            StreamWriter prjStreamOut = new StreamWriter(SourceDir + PrjFile);
            foreach (string item in prjContent)
            {
                prjStreamOut.WriteLine(item);
                if (item.StartsWith("CAPTURE LIST"))
                    break;
            }

            // collect a list of unique file names from the FileEntries list
            Hashtable RenameTable = new Hashtable();
            foreach (Hashtable item in TableEntries)
            {
                if (item["FileName"] == null)
                    continue;
                file = item["FileName"].ToString();
                if (!File.Exists(Common.GetWorkPath() + TNUnzipDir + file))
                    continue;
                if (!RenameTable.Contains(file))
                {
                    string captureExtension = EditFunctions.GetCaptureExtension(file);
                    RenameTable.Add(file, GetNewCapName(tn, capNum++, captureExtension));
                    line = RenameTable[file].ToString() + "   : " + FindLabel(item, prjContent);
                    prjStreamOut.WriteLine(line);
                }
            }
            prjStreamOut.Close();
            return RenameTable;
        }

        public static StringCollection UpdatePrjAndReportDiff(int tn, HashtableCollection TableEntries)
        {
            string prjFile = PrjFileName(tn);
            string TNUnzipDir = TNHelper.TNUnzipDir(tn);
            string SourceDir = Common.GetWorkPath() + TNUnzipDir;
            StringCollection prjContent = PrjFileHelper.ReadPrjFile(tn);
            StringCollection Difference = new StringCollection();

            StreamWriter prjStreamOut = new StreamWriter(SourceDir + prjFile);
            foreach (string item in prjContent)
            {
                if (!item.StartsWith("!"))
                    prjStreamOut.WriteLine(item);
                else
                {
                    if (!FileIsInTable(TableEntries, item))
                        Difference.Add("Deleted - " + item);
                }
            }
            foreach (Hashtable row in TableEntries)
            {
                if (string.IsNullOrEmpty(row["FileName"].ToString()))
                    continue;

                string line = row["FileName"].ToString() +
                    "   : " + row["Label"].ToString();
                prjStreamOut.WriteLine(line);

                if (!FileIsInPrj(prjContent, row))
                    Difference.Add("Added - " + line);
            }
            prjStreamOut.Close();
            return Difference;
        }

        private static bool FileIsInPrj(StringCollection prjContent, Hashtable row)
        {
            foreach (string item in prjContent)
            {
                if (item.StartsWith(row["FileName"].ToString()))
                    return true;
            }
            return false;
        }

        private static bool FileIsInTable(HashtableCollection TableEntries, string item)
        {
            foreach (Hashtable row in TableEntries)
            {
                if (string.IsNullOrEmpty(row["FileName"].ToString()))
                    continue;
                if (item.StartsWith(row["FileName"].ToString()))
                    return true;
            }
            return false;
        }

        private static string FindLabel(Hashtable entry, StringCollection prjContent)
        {
            foreach (string item in prjContent)
            {
                if (item.StartsWith(entry["FileName"].ToString()))
                {
                    return item.Substring(item.IndexOf(':') + 2);
                }
            }
            return entry["Label"].ToString();
        }

        public static StringCollection ReadPrjFile(int tn)
        {
            string line;
            string TNUnzipDir = TNHelper.TNUnzipDir(tn);
            StringCollection prjContent = new StringCollection();
            string PrjFile = PrjFileName(tn);
            StreamReader prjStreamIn = new StreamReader(Common.GetWorkPath() +
                                                        TNUnzipDir + PrjFile);

            while ((line = prjStreamIn.ReadLine()) != null)
                prjContent.Add(line);

            prjStreamIn.Close();
            return prjContent;
        }

        public static string GetNewCapName(int tn, int capNum)
        {
            string filename;
            if (tn > 9999)
                filename = string.Format("!{0:D5}{1:X2}.U1", tn, capNum);
            else
                filename = string.Format("!{0:D4}{1:D3}.U1", tn, capNum);
            return filename;
        }

        public static string GetNewCapName(int tn, int capNum, string extension)
        {
            if (string.IsNullOrEmpty(extension)) {
                return GetNewCapName(tn, capNum);
            }

            string filename;
            if (tn > 9999)
                filename = string.Format("!{0:D5}{1:X2}.{2}", tn, capNum, extension);
            else
                filename = string.Format("!{0:D4}{1:D3}.{2}", tn, capNum, extension);
            return filename;
        }

        public static void CreatePrjFile(int tn)
        {
            TNHeader header = DAOFactory.TNHeader().Select(tn);
            string path = Common.GetWorkPath() + TNHelper.TNUnzipDir(tn);
            if (!Directory.Exists(path))
                return;

            string FileName = path + PrjFileName(tn);

            using (StreamWriter sw = File.AppendText(FileName))
            {
                sw.WriteLine("REMOTE CONTROL CAPTURE ----------------------------------------");
                sw.WriteLine("Copyright 1989 Universal Electronics, Inc.  All Rights Reserved\r\n");
                sw.WriteLine("Tracking Number: " + tn.ToString());
                sw.WriteLine("Primary Number : ");
                sw.WriteLine("Box Number     : " + header.Box + "\r\n");
                sw.WriteLine("Executor       : ");
                sw.WriteLine("Id             : \r\n");
                sw.WriteLine("Brand Name     : " + header.LegacyBrand);
                sw.WriteLine("Device Type    : " + header.LegacyDeviceType);
                sw.WriteLine("Remote Model   : " + header.LegacyRemoteModel);
                sw.WriteLine("Target Model   : " + header.LegacyTargetModel);
                sw.WriteLine("Homer Model    : " + header.Homer + "\r\n");
                sw.WriteLine("CUSTOMER-------------------------------------------------------\r\n");
                sw.WriteLine("Customer Name  : " + header.Sender_Name);
                sw.WriteLine("Company Name   : " + header.Sender_Company);
                sw.WriteLine("Address        : " + header.Sender_Address);
                sw.WriteLine("Address        : ");
                sw.WriteLine("Address        : ");
                sw.WriteLine("Address        : ");
                sw.WriteLine("Phone          : " + header.Sender_Phone + "\r\n\r\n");
                sw.WriteLine("Markets        : " + header.LegacyMarketFlag + "\r\n");
                sw.WriteLine("TRACKING-------------------------------------------------------\r\n");
                sw.Write("Logged In      : ");
                sw.WriteLine(FormatDate(header.LogList[0].Time) + 
                                "\tBy: " + header.LogList[0].Operator);
                sw.WriteLine("Last Modified  : " + FormatDate(DateTime.Now) + 
                                "\tBy: " + SystemInformation.UserName.ToLower() + "\r\n");
                sw.WriteLine("CAPTURE LIST---------------------------------------------------");
                sw.Close();
            }
        }

        private static string FormatDate(DateTime dt)
        {
            string DayDate;
            DayDate = string.Format("{0:D2} {1:D2} {2} {3}",
                            dt.Day, dt.Month, dt.Year,
                            dt.ToShortTimeString());
            return DayDate;
        }
    }
}
