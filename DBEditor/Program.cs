using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DBEditor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                switch (args[0])                
                {
                    case "ID":
                        DBEditorWrapper.EditPartialID();
                        break;
                    case "TN":
                        DBEditorWrapper.EditTN();
                        break;

                    case "DATA":
                        DBEditorWrapper.EditData();
                        break;
                    case "INTRON":
                        DBEditorWrapper.EditIntron();
                        break;
                    case "LABEL":
                        DBEditorWrapper.EditLabel();
                        break;

                    case "UNLOCK_ID":
                        DBEditorWrapper.UnLockPartialID();
                        break;
                    case "UNLOCK_TN":
                        DBEditorWrapper.UnLockTN();
                        break;

                    case "UPDATE_ID":
                        DBEditorWrapper.RenameID();
                        break;

                    case "ADD_REMOTE":
                        DBEditorWrapper.AddNewRemote();
                        break;

                    // Get ID or TN number and pass it on to the editor
                    default:       
                        Application.EnableVisualStyles();
                        if (char.IsNumber(args[0][0]))
                        {
                            DBEditorWrapper.EditTN(
                                int.Parse(args[0]));
                        }
                        else
                        {
                            DBEditorWrapper.EditPartialID(
                                args[0].ToString());
                        }
                        break;
                }
                return;
            }

            Application.EnableVisualStyles();
            Application.Run(new MainForm());
        }
    }
}