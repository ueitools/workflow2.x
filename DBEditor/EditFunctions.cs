using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using EntryHelper;
using Microsoft.Win32;
using System.IO;
using BusinessObject;

namespace DBEditor
{
    public class EditFunctions
    {
        static public int _tnNumber = 0;

        public static ContextMenu CommonEditPopup(EventHandler Target)
        {
            ContextMenu ClickMenu = new ContextMenu();
            ClickMenu.MenuItems.Add("Cut", new EventHandler(Target));
            ClickMenu.MenuItems.Add("Copy", new EventHandler(Target));
            ClickMenu.MenuItems.Add("Paste", new EventHandler(Target));
            ClickMenu.MenuItems.Add("Paste Insert", new EventHandler(Target));
            ClickMenu.MenuItems.Add("Insert Blank Row", new EventHandler(Target));
            ClickMenu.MenuItems.Add("Delete", new EventHandler(Target));
            ClickMenu.MenuItems.Add(new MenuItem("-"));
            ClickMenu.MenuItems.Add("ViewFile", new EventHandler(Target));
            ClickMenu.MenuItems.Add("Capture", new EventHandler(Target));
            return ClickMenu;
        }

        public static void KeyCheck(object sender, KeyEventArgs e, DataGridView Grid, BindingSource Data)
        {
            switch (e.KeyCode)
            {
                case Keys.Delete:
                    EditMenuFunctionsHandler("Delete", Grid, Data);
                    break;
                case Keys.Insert:
                    if (e.Shift)
                        EditMenuFunctionsHandler("Paste", Grid, Data);
                    if (e.Control)
                        EditMenuFunctionsHandler("Copy", Grid, Data);
                    break;
                case Keys.C:
                    if (e.Control)
                        EditMenuFunctionsHandler("Copy", Grid, Data);
                    break;
                case Keys.V:
                    if (e.Control)
                        EditMenuFunctionsHandler("Paste", Grid, Data);
                    break;
                case Keys.X:
                    if (e.Control)
                        EditMenuFunctionsHandler("Cut", Grid, Data);
                    break;
            }
        }

        public static void EditMenuFunctionsHandler(string Selection, DataGridView Grid, BindingSource Data)
        {
            switch (Selection)
            {
                case "Cut":
                    CopySelectedGridToClipboard(Grid);
                    DeleteSelectedGridCells(Grid);
                    CaptureFunctions.UpdateChangesToTnFiles(Grid);
                    break;
                case "Copy":
                    CopySelectedGridToClipboard(Grid);
                    break;
                case "Paste":
                    PasteFromClipboardToDataGrid(Grid, Data, false);
                    CaptureFunctions.UpdateChangesToTnFiles(Grid);
                    break;
                case "Paste Insert":
                    IDataObject CbData = Clipboard.GetDataObject();
                    if (CbData.GetDataPresent(DataFormats.Text))            // text CbData is present?
                    {
                        PasteFromClipboardToDataGrid(Grid, Data, true);
                        CaptureFunctions.UpdateChangesToTnFiles(Grid);
                    }
                    break;
                case "Insert Blank Row":
                        InsertBlankLineToDataGrid(Grid, Data);
                    break;
                case "Delete":
                    DeleteSelectedGridCells(Grid);
                    CaptureFunctions.UpdateChangesToTnFiles(Grid);
                    break;
                case "ViewFile":
                    CaptureFunctions.ViewCaptureFileForSelectedGridCell(Grid);
                    break;
                case "Capture":
                    CaptureFunctions.CaptureNewU1FileForSelectedRows(Grid);
                    break;
            }            
        }

        private static void InsertBlankLineToDataGrid(DataGridView Grid, BindingSource Data)
        {
            DataGridViewSelectedCellCollection cells = Grid.SelectedCells;            
            if (cells.Count > 0)
                Data.Insert(cells[0].RowIndex, new Function());
        }

        private static void DeleteSelectedGridCells(DataGridView Grid)
        {
            DataGridViewSelectedRowCollection rows = Grid.SelectedRows;
            if (rows.Count > 0)
            {
                foreach (DataGridViewRow row in rows)
                {
                    if (row.Index >= Grid.RowCount - 1)         // don't try to delete the "new" row!
                        continue;
                    LogOperation(_tnNumber, "Deleted", row);
                    Grid.Rows.RemoveAt(row.Index);
                }
            }
            else
                foreach (DataGridViewCell cell in Grid.SelectedCells)
                {
                    if (cell.RowIndex >= Grid.RowCount - 1)     // don't try to delete the "new" row!
                        continue;
                    if (cell.ValueType.IsValueType)
                        cell.Value = 0;
                    else
                        cell.Value = "";
                }
        }

        public static void LogOperation(int tn, string Action, DataGridViewRow row)
        {
            string Msg = Action + " ";

            if (row != null)
            {
                Msg += string.Format("[row{0}] ", row.Index);
                if (row.Cells[2] != null && row.Cells[2].Value != null)
                    Msg += row.Cells[2].Value.ToString();
            }

            TNHelper.Log(tn, Msg);
        }

        private static void CopySelectedGridToClipboard(DataGridView Grid)
        {
            Clipboard.Clear();
            if (Grid.SelectedCells.Count > 0)
            {
                DataObject SelectedCellContent = Grid.GetClipboardContent();
                try
                {
                    Clipboard.SetText(SelectedCellContent.GetText());
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Copy clipboard data to a data grid.
        /// Multiple columns and rows are allowed.
        /// NOTE: hidden column fields may be in the clip board and cause trouble!
        /// </summary>
        /// <param name="Grid">The target DataGridView</param>
        public static void PasteFromClipboardToDataGrid(DataGridView Grid, BindingSource Data, bool Insert)
        {
            try
            {
                IDataObject CbData = Clipboard.GetDataObject();
                if (!CbData.GetDataPresent(DataFormats.Text))       // text data is present?
                    throw new Exception("The data on the clipboard is not in the correct format");

                Grid.ClearSelection();
                string CbText = CbData.GetData(DataFormats.Text).ToString();
                PasteRows(Grid, Data, Insert, CbText);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private static void PasteRows(DataGridView Grid, BindingSource Data, bool Insert, string CbText)
        {
            string[] delim = new string[] { "\r\n" };
            string[] CbLines = CbText.Split(delim, StringSplitOptions.None);
            FullOrPartialRow(Grid, CbLines[0]);                 // if pasting complete line select first col

            int row = Grid.CurrentCell.RowIndex;
            int col = Grid.CurrentCell.ColumnIndex;

            if (row + 1 >= Grid.RowCount && row > 0)
                Grid.CurrentCell = Grid[col, row - 1];

            string Operation = (Insert) ? "Insert " : "Paste ";

            foreach (string line in CbLines)
            {
                if (Insert || row + 1 >= Grid.RowCount)         // new line?
                {
                    Data.Insert(row, new Function());
                    Grid["TN", row].Value = _tnNumber;
                }

                col = PasteLineItems(Grid, row, col, line);
                col = Grid.CurrentCell.ColumnIndex;             // reset the column start point
                CheckDupFiles(Grid, row);
                LogOperation(_tnNumber, Operation, Grid.Rows[row]);
                row++;                                          // get ready for the next row
            }
            if (row <= Grid.Rows.Count)
                Grid.CurrentCell = Grid[col, row];              // at end, set next start point
        }

        private static void CheckDupFiles(DataGridView Grid, int row)
        {
            string CurrentName = (string)Grid["Filename", row].Value;
            foreach (DataGridViewRow checkRow in Grid.Rows)
            {
                if ((string)checkRow.Cells["Filename"].Value == CurrentName)
                {
                    CopyAndRename(Grid, row);
                    break;
                }
            }
        }

        private static void CopyAndRename(DataGridView Grid, int row)
        {
            string oldName = (string)Grid["Filename", row].Value;
            string newName = "#" + PrjFileHelper.GetNewCapName(_tnNumber, row, GetCaptureExtension(oldName));
            string zipdir = Common.GetWorkPath() + TNHelper.TNUnzipDir(_tnNumber);

            if (File.Exists(zipdir + oldName))
            {
                File.Copy(zipdir + oldName, zipdir + newName);

                Grid["Filename", row].Value = newName;
            }
            else
                Grid["Filename", row].Value = "";
        }

        public static string GetCaptureExtension(string oldName) {
            string captureExtension = string.Empty;
            if (string.IsNullOrEmpty(oldName) == false) {
                captureExtension = Path.GetExtension(oldName).Remove(0, 1);
            }
            return captureExtension;
        }

        private static void FullOrPartialRow(DataGridView Grid, string CbLine)
        {
            int row = Grid.CurrentCell.RowIndex;
            string[] delim = new string[] { "\t" };
            string[] items = CbLine.Split(delim, StringSplitOptions.None);
            if (items.Length == Grid.ColumnCount)               // are we pasting complete lines?
                Grid.CurrentCell = Grid[0, row];                // if so, set current cell to start of row.
        }

        private static int PasteLineItems(DataGridView Grid, int row, int col, string line)
        {
            string[] delim = new string[] { "\t" };
            string[] itemList = line.Split(delim, StringSplitOptions.None);
            bool firstItem = true;

            foreach (string item in itemList)
            {
                if (firstItem && string.IsNullOrEmpty(item))    // when pasting lines the first item 
                    continue;                                   // is empty and must be thrown out.
                firstItem = false;

                if (row < Grid.RowCount)                        // if it's in Grid range
                    Grid[col, row].Value = item;                // paste it in place.

                col++;                                          // get ready for the next column
                while (col < Grid.ColumnCount && !Grid.Columns[col].Visible)
                    col++;                                      // must skip the hidden (not copied) column

                if (col >= Grid.ColumnCount)                    // can't paste off the right edge!
                    break;
            }
            return col;
        }

        /// <summary>
        /// prove that a tn number is valid by attempting to load it.
        /// if an exception is thrown, display it and return false.
        /// </summary>
        /// <param name="tnNum">a TN number value</param>
        /// <returns>true if TN loads, false otherwise.</returns>
        public static bool validateTN(int tnNum)
        {
            TN tmp = new TN();
            try
            {
                tmp.Load(tnNum);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
            return true;
        }
    }
}
