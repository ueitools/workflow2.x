using System;
using System.Collections.Generic;
using System.Text;

namespace DBEditor
{
    class WindowsDomain
    {
        public static bool InHQDomain(string domain)
        {
            return domain == "UEIC" || domain == "AMER";
        }

        public static bool InHQDomain()
        {
            return InHQDomain(Environment.UserDomainName);
        }
    }
}
