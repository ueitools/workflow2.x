﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace UEI.ServiceProvider
{
    public class SubLocationEditor
    {
        #region Variables
        private DBOperations objDb = null;
        #endregion

        #region Properties
        public String ErrorMessage { get; set; }
        #endregion

        #region Constrcuctor
        public SubLocationEditor()
        {
            String connectionString = DBConnectionString.ServiceProvider;
            this.objDb = new DBOperations(connectionString);
        }
        #endregion

        #region Get Province, City and ZIP code based on Service Provider
        public SubLocationModel GetAvailableSubLocationsByServiceProvider(Int32 serviceproviderRID)
        {
            SubLocationModel m_SubLocations = null;
            DataTable dtResult = null;
            this.ErrorMessage = String.Empty;
            try
            {                
                IDbCommand objCmd = objDb.CreateCommand();              
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "[dbo].[SRVP_Sp_GetSubLocationInfoByServiceProvider]";
                objCmd.Parameters.Add(objDb.CreateParameter("@nServiceProvider_RID", DbType.String, ParameterDirection.Input, serviceproviderRID.ToString().Length, serviceproviderRID));
                dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult != null)
                {
                    foreach (DataRow item in dtResult.Rows)
                    {
                        SubLocation sublocation = new SubLocation();
                        if (!String.IsNullOrEmpty(item["SubLocation_RID"].ToString())) sublocation.SubLocationRID = Int64.Parse(item["SubLocation_RID"].ToString());
                        if (!String.IsNullOrEmpty(item["RegionCode"].ToString())) sublocation.RegionCode = item["RegionCode"].ToString();
                        if (!String.IsNullOrEmpty(item["Region1"].ToString())) sublocation.Region1 = item["Region1"].ToString();
                        if (!String.IsNullOrEmpty(item["Region2"].ToString())) sublocation.Region2 = item["Region2"].ToString();
                        if (!String.IsNullOrEmpty(item["Region3"].ToString())) sublocation.Region3 = item["Region3"].ToString();
                        if (!String.IsNullOrEmpty(item["Region4"].ToString())) sublocation.Region4 = item["Region4"].ToString();
                        if (!String.IsNullOrEmpty(item["Area1"].ToString())) sublocation.Area1 = item["Area1"].ToString();
                        if (!String.IsNullOrEmpty(item["Area2"].ToString())) sublocation.Area2 = item["Area2"].ToString();
                        if (!String.IsNullOrEmpty(item["City"].ToString())) sublocation.City = item["City"].ToString();
                        if (!String.IsNullOrEmpty(item["ZIP"].ToString())) sublocation.ZipCode = item["ZIP"].ToString();
                        if (!String.IsNullOrEmpty(item["TimeZone"].ToString())) sublocation.TimeZone = item["TimeZone"].ToString();
                        if (!String.IsNullOrEmpty(item["UTC_Offset"].ToString())) sublocation.UTCOffset = item["UTC_Offset"].ToString();
                        if (!String.IsNullOrEmpty(item["Latitude"].ToString())) sublocation.Latitude = Double.Parse(item["Latitude"].ToString());
                        if (!String.IsNullOrEmpty(item["Longitude"].ToString())) sublocation.Longitude = Double.Parse(item["Longitude"].ToString());
                        if (!String.IsNullOrEmpty(item["ProvinceId"].ToString())) sublocation.ProvinceID = item["ProvinceId"].ToString();
                        if (!String.IsNullOrEmpty(item["CityId"].ToString())) sublocation.CityID = item["CityId"].ToString();
                        if (m_SubLocations == null)
                            m_SubLocations = new SubLocationModel();
                        m_SubLocations.Add(sublocation);
                        sublocation = null;
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return m_SubLocations;
        }
        #endregion   
     
        #region Get Province, City and ZIP code based on Location
        public SubLocationModel GetAvailableSubLocationsByLocation(String region, String country, String subloc)
        {
            SubLocationModel m_SubLocations = null;
            DataTable dtResult = null;
            this.ErrorMessage = String.Empty;
            try
            {                
                IDbCommand objCmd = objDb.CreateCommand();              
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "[dbo].[SRVP_Sp_GetSubLocationByRegionCountry]";
                if (!String.IsNullOrEmpty(region))
                    objCmd.Parameters.Add(objDb.CreateParameter("@nRegion", DbType.String, ParameterDirection.Input, region.Length, region));
                if (!String.IsNullOrEmpty(country))
                    objCmd.Parameters.Add(objDb.CreateParameter("@nCountry", DbType.String, ParameterDirection.Input, country.Length, country));
                if (!String.IsNullOrEmpty(subloc))
                    objCmd.Parameters.Add(objDb.CreateParameter("@nSubLocation", DbType.String, ParameterDirection.Input, subloc.Length, subloc));
                dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult != null)
                {
                    foreach (DataRow item in dtResult.Rows)
                    {
                        SubLocation sublocation = new SubLocation();
                        if (!String.IsNullOrEmpty(item["SubLocation_RID"].ToString())) sublocation.SubLocationRID = Int64.Parse(item["SubLocation_RID"].ToString());
                        if (!String.IsNullOrEmpty(item["RegionCode"].ToString())) sublocation.RegionCode = item["RegionCode"].ToString();
                        if (!String.IsNullOrEmpty(item["Region1"].ToString())) sublocation.Region1 = item["Region1"].ToString();
                        if (!String.IsNullOrEmpty(item["Region2"].ToString())) sublocation.Region2 = item["Region2"].ToString();
                        if (!String.IsNullOrEmpty(item["Region3"].ToString())) sublocation.Region3 = item["Region3"].ToString();
                        if (!String.IsNullOrEmpty(item["Region4"].ToString())) sublocation.Region4 = item["Region4"].ToString();
                        if (!String.IsNullOrEmpty(item["Area1"].ToString())) sublocation.Area1 = item["Area1"].ToString();
                        if (!String.IsNullOrEmpty(item["Area2"].ToString())) sublocation.Area2 = item["Area2"].ToString();
                        if (!String.IsNullOrEmpty(item["City"].ToString())) sublocation.City = item["City"].ToString();
                        if (!String.IsNullOrEmpty(item["ZIP"].ToString())) sublocation.ZipCode = item["ZIP"].ToString();
                        if (!String.IsNullOrEmpty(item["TimeZone"].ToString())) sublocation.TimeZone = item["TimeZone"].ToString();
                        if (!String.IsNullOrEmpty(item["UTC_Offset"].ToString())) sublocation.UTCOffset = item["UTC_Offset"].ToString();
                        if (!String.IsNullOrEmpty(item["Latitude"].ToString())) sublocation.Latitude = Double.Parse(item["Latitude"].ToString());
                        if (!String.IsNullOrEmpty(item["Longitude"].ToString())) sublocation.Longitude = Double.Parse(item["Longitude"].ToString());
                        if (!String.IsNullOrEmpty(item["ProvinceId"].ToString())) sublocation.ProvinceID = item["ProvinceId"].ToString();
                        if (!String.IsNullOrEmpty(item["CityId"].ToString())) sublocation.CityID = item["CityId"].ToString();
                        if (m_SubLocations == null)
                            m_SubLocations = new SubLocationModel();
                        m_SubLocations.Add(sublocation);
                        sublocation = null;
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return m_SubLocations;
        }
        #endregion

        #region Attach Sub Location To Service Provider
        public void AttachSubLocation(String serviceproviderRID, String sublocationRID)
        {            
            DataTable dtResult = null;
            this.ErrorMessage = String.Empty;
            try
            {
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "[dbo].[SRVP_Sp_AttachSubLocationByServiceProvider]";
                objCmd.Parameters.Add(objDb.CreateParameter("@nServiceProvider_Rid", DbType.String, ParameterDirection.Input, serviceproviderRID.Length, serviceproviderRID));
                objCmd.Parameters.Add(objDb.CreateParameter("@nSubLocation_Rid", DbType.String, ParameterDirection.Input, sublocationRID.Length, sublocationRID));
                dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult != null)
                {
                    foreach (DataRow item in dtResult.Rows)
                    {
                        if (!String.IsNullOrEmpty(item["RESULT"].ToString()))
                        {
                            if (item["RESULT"].ToString() != "0")
                                this.ErrorMessage = item["RESULT"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }            
        }
        #endregion

        #region Detach Sub Location From Service Provider
        public void DetachSubLocation(String serviceproviderRID, String sublocationRID)
        {            
            DataTable dtResult = null;
            this.ErrorMessage = String.Empty;
            try
            {
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "[dbo].[SRVP_Sp_DetachSubLocationByServiceProvider]";
                objCmd.Parameters.Add(objDb.CreateParameter("@nServiceProvider_Rid", DbType.String, ParameterDirection.Input, serviceproviderRID.Length, serviceproviderRID));
                objCmd.Parameters.Add(objDb.CreateParameter("@nSubLocation_Rid", DbType.String, ParameterDirection.Input, sublocationRID.Length, sublocationRID));
                dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult != null)
                {
                    foreach (DataRow item in dtResult.Rows)
                    {
                        if (!String.IsNullOrEmpty(item["RESULT"].ToString()))
                        {
                            if (item["RESULT"].ToString() != "0")
                                this.ErrorMessage = item["RESULT"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }            
        }
        #endregion
    }
}