﻿using BusinessObject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace UEI.ServiceProvider
{
    public class ServiceProviderViewer
    {
        #region Variables
        private DBOperations objDb = null;
        #endregion

        #region Properties
        public String ErrorMessage { get; set; }
        public LocationModel ServiceProviderLocations { get; set; }
        public DeviceModel ServiceProviderDeviceTypes { get; set; }
        #endregion

        #region Constrcuctor
        public ServiceProviderViewer()
        {
            String connectionString = DBConnectionString.ServiceProvider;
            this.objDb = new DBOperations(connectionString);
        }
        #endregion   

        #region Methods
        public DeviceModel GetDeviceTypeByServiceProviderRID(String serviceproviderrid)
        {
            this.ErrorMessage = String.Empty;
            DeviceModel l_DeviceTypes = null;
            try
            {
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "[dbo].[SRVP_Sp_GetDeviceTypeServiceProvider]";
                objCmd.Parameters.Add(objDb.CreateParameter("@nServiceProvider_Rid", DbType.String, ParameterDirection.Input, serviceproviderrid.Length, serviceproviderrid));
                DataTable dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult != null)
                {                    
                    foreach (DataRow item in dtResult.Rows)
                    {
                        DeviceType devicetype = new DeviceType();
                        if (!String.IsNullOrEmpty(item["ServiceProvider_RID"].ToString())) devicetype.ServiceProviderRID = Int32.Parse(item["ServiceProvider_RID"].ToString());
                        if (!String.IsNullOrEmpty(item["MainDevice"].ToString())) devicetype.MainDeviceType = item["MainDevice"].ToString();
                        if (!String.IsNullOrEmpty(item["SubDevice"].ToString())) devicetype.SubDeviceType = item["SubDevice"].ToString();
                        if (!String.IsNullOrEmpty(item["SubLocations"].ToString())) devicetype.SubLocation = item["SubLocations"].ToString();
                        if (l_DeviceTypes == null)
                            l_DeviceTypes = new DeviceModel();
                        l_DeviceTypes.Add(devicetype);
                        devicetype = null;
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return l_DeviceTypes;
        }
        public LocationModel GetLocationByServiceProviderRID(String serviceproviderrid)
        {
            this.ErrorMessage = String.Empty;
            LocationModel l_Locations = null;
            try
            {
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "[dbo].[SRVP_Sp_GetLocationByServiceProvider]";
                objCmd.Parameters.Add(objDb.CreateParameter("@nServiceProvider_RID", DbType.String, ParameterDirection.Input, serviceproviderrid.Length, serviceproviderrid));
                DataTable dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult != null)
                {                    
                    foreach (DataRow item in dtResult.Rows)
                    {
                        Locations location = new Locations();
                        if (!String.IsNullOrEmpty(item["ServiceProvider_RID"].ToString())) location.ServiceProviderRID = Int32.Parse(item["ServiceProvider_RID"].ToString());
                        if (!String.IsNullOrEmpty(item["Location_RID"].ToString())) location.LocationRID = Int32.Parse(item["Location_RID"].ToString());
                        if (!String.IsNullOrEmpty(item["Region"].ToString())) location.Region = item["Region"].ToString();
                        if (!String.IsNullOrEmpty(item["Country"].ToString())) location.Country = item["Country"].ToString();
                        if (!String.IsNullOrEmpty(item["Region1"].ToString())) location.SubLocation = item["Region1"].ToString();  
                        if (l_Locations == null)
                            l_Locations = new LocationModel();
                        l_Locations.Add(location);
                        location = null;
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return l_Locations;
        }
        public void GetDeviceTypesforServiceProviders()
        {
            this.ErrorMessage = String.Empty;
            this.ServiceProviderDeviceTypes = null;
            try
            {                
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "[dbo].[SRVP_Sp_GetDeviceTypeServiceProvider]";
                DataTable dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult != null)
                {                    
                    foreach (DataRow item in dtResult.Rows)
                    {
                        DeviceType devicetype = new DeviceType();
                        if (!String.IsNullOrEmpty(item["ServiceProvider_RID"].ToString())) devicetype.ServiceProviderRID = Int32.Parse(item["ServiceProvider_RID"].ToString());
                        if (!String.IsNullOrEmpty(item["MainDevice"].ToString())) devicetype.MainDeviceType = item["MainDevice"].ToString();
                        if (!String.IsNullOrEmpty(item["SubDevice"].ToString())) devicetype.SubDeviceType = item["SubDevice"].ToString();
                        if (!String.IsNullOrEmpty(item["SubLocation"].ToString())) devicetype.SubLocation = item["SubLocation"].ToString();
                        if (this.ServiceProviderDeviceTypes == null)
                            this.ServiceProviderDeviceTypes = new DeviceModel();
                        this.ServiceProviderDeviceTypes.Add(devicetype);
                        devicetype = null;
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }            
        }
        public void GetLocationforServiceProviders()
        {
            this.ErrorMessage = String.Empty;
            this.ServiceProviderLocations = null;
            try
            {                
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "[dbo].[SRVP_Sp_GetLocationByServiceProvider]";              
                DataTable dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult != null)
                {                    
                    foreach (DataRow item in dtResult.Rows)
                    {
                        Locations location = new Locations();
                        if (!String.IsNullOrEmpty(item["ServiceProvider_RID"].ToString())) location.ServiceProviderRID = Int32.Parse(item["ServiceProvider_RID"].ToString());
                        if (!String.IsNullOrEmpty(item["Location_RID"].ToString())) location.LocationRID = Int32.Parse(item["Location_RID"].ToString());
                        if (!String.IsNullOrEmpty(item["Region"].ToString())) location.Region = item["Region"].ToString();
                        if (!String.IsNullOrEmpty(item["Country"].ToString())) location.Country = item["Country"].ToString();
                        if (!String.IsNullOrEmpty(item["Region1"].ToString())) location.SubLocation = item["Region1"].ToString();
                        if (this.ServiceProviderLocations == null)
                            this.ServiceProviderLocations = new LocationModel();
                        this.ServiceProviderLocations.Add(location);
                        location = null;
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }            
        }
        public ServiceProviderModel GetServiceProviders()
        {
            //Load Locations for Service Provider
            GetLocationforServiceProviders();
            //Load Device Types for Service Provider
            GetDeviceTypesforServiceProviders();

            this.ErrorMessage = String.Empty;
            ServiceProviderModel l_ServiceProviders = null;
            try
            {                
                IDbCommand objCmd = objDb.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "[dbo].[SRVP_Sp_GetServiceProviders]";               
                DataTable dtResult = objDb.ExecuteDataTable(objCmd);
                if (dtResult != null)
                {
                    l_ServiceProviders = new ServiceProviderModel();
                    foreach (DataRow item in dtResult.Rows)
                    {
                        ServiceProviders serviceprovider = new ServiceProviders();
                        if (!String.IsNullOrEmpty(item["ServiceProvider_RID"].ToString())) serviceprovider.ServiceProviderRID = Int32.Parse(item["ServiceProvider_RID"].ToString());
                        if (!String.IsNullOrEmpty(item["MSOSP"].ToString())) serviceprovider.MSOSP = item["MSOSP"].ToString();
                        if (!String.IsNullOrEmpty(item["ServiceProvider"].ToString())) serviceprovider.ServiceProvider = item["ServiceProvider"].ToString();
                        if (!String.IsNullOrEmpty(item["AliasServiceProvider"].ToString())) serviceprovider.AliasServiceProvider = item["AliasServiceProvider"].ToString();
                        if (!String.IsNullOrEmpty(item["ServiceProviderVariation"].ToString())) serviceprovider.ServiceProviderVariation = item["ServiceProviderVariation"].ToString();
                        if (!String.IsNullOrEmpty(item["STBBrand"].ToString())) serviceprovider.Brand = item["STBBrand"].ToString();
                        if (!String.IsNullOrEmpty(item["AliasBrand"].ToString())) serviceprovider.AliasBrand = item["AliasBrand"].ToString();
                        if (!String.IsNullOrEmpty(item["BrandVariation"].ToString())) serviceprovider.BrandVariation = item["BrandVariation"].ToString();
                        if (!String.IsNullOrEmpty(item["Model"].ToString())) serviceprovider.Model = item["Model"].ToString();
                        if (!String.IsNullOrEmpty(item["Id"].ToString())) serviceprovider.ID = item["Id"].ToString();
                        if (l_ServiceProviders == null)
                            l_ServiceProviders = new ServiceProviderModel();
                        if (this.ServiceProviderLocations != null)
                        {
                            LocationModel l_location = this.ServiceProviderLocations.GetLocationsByServiceProvider(Int32.Parse(item["ServiceProvider_RID"].ToString()));
                            serviceprovider.Location = l_location;
                        }
                        if (this.ServiceProviderDeviceTypes != null)
                        {
                            DeviceModel l_devicetype = this.ServiceProviderDeviceTypes.GetDeviceTypesByServiceProvider(Int32.Parse(item["ServiceProvider_RID"].ToString()));
                            serviceprovider.DeviceTypes = l_devicetype;
                        }                                            
                        l_ServiceProviders.Add(serviceprovider);
                        serviceprovider = null;
                    }
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return l_ServiceProviders;
        }
        #endregion
    }
}