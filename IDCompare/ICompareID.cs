using System;
using System.Collections.Generic;
using CommonForms;

namespace IDCompare
{
    public class ICompareID : IICompareID
    {
        double m_freqTolerance;
        int FREQ_ARRAY_SIZE;

        public ICompareID()
        {
            m_freqTolerance = 0.05;
            FREQ_ARRAY_SIZE = 10;
        }

        #region public functions

        public void DoCompare(out int DoCompResult, 
            string mRXCompareStr, string mTXCompareStr)
        {
            bool m_dataExec = false;
            if (mRXCompareStr.Contains("B"))
            {
                m_dataExec = true;
            }

            if (m_dataExec)
                DoCompResult = CompareDataInfo(mRXCompareStr,
                    mTXCompareStr);
            else
                DoCompResult = CompareFreqInfo(mRXCompareStr,
                    mTXCompareStr);
        }

        public void DoCompare(string OutFile1, out int DoCompResult,
            out string mRXCompareStr, string mTXCompareStr)
        {
            CFIInfo rxCFIInfo;

            DoCompare(OutFile1, out DoCompResult, out mRXCompareStr,
                mTXCompareStr, out rxCFIInfo);
        }

        public void DoCompare(string OutFile1, out int DoCompResult, 
            out string mRXCompareStr, string mTXCompareStr, 
            out CFIInfo rxCFIInfo)
        {
            CFIReader cfiReader = new CFIReader();
            mRXCompareStr = cfiReader.GetCFIDataString(OutFile1, 
                out rxCFIInfo);

            bool m_dataExec = false;
            if (mRXCompareStr.Contains("B"))
            {
                m_dataExec = true;
            }

            if (m_dataExec)
                DoCompResult = CompareDataInfo(mRXCompareStr,
                    mTXCompareStr);
            else
                DoCompResult = CompareFreqInfo(mRXCompareStr,
                    mTXCompareStr);
        }

        public void DoCompare(string OutFile2, out int DoCompResult, 
            string mRXCompareStr, out string mTXCompareStr)
        {
            CFIInfo txCFIInfo;

            DoCompare(OutFile2, out DoCompResult, mRXCompareStr,
                out mTXCompareStr, out txCFIInfo);
        }

        public void DoCompare(string OutFile2, out int DoCompResult, 
            string mRXCompareStr, out string mTXCompareStr, 
            out CFIInfo txCFIInfo)
        {
            CFIReader cfiReader = new CFIReader();
            mTXCompareStr = cfiReader.GetCFIDataString(OutFile2, 
                out txCFIInfo);

            bool m_dataExec = false;
            if (mRXCompareStr.Contains("B"))
            {
                m_dataExec = true;
            }

            if (m_dataExec)
                DoCompResult = CompareDataInfo(mRXCompareStr,
                    mTXCompareStr);
            else
                DoCompResult = CompareFreqInfo(mRXCompareStr,
                    mTXCompareStr);
        }

        public void DoCompare(string OutFile1, string OutFile2,
            out int DoCompResult, out string mRXCompareStr,
            out string mTXCompareStr)
        {
            CFIInfo rxCFIInfo, txCFIInfo;

            DoCompare(OutFile1, OutFile2, out DoCompResult,
                out mRXCompareStr, out mTXCompareStr, out rxCFIInfo,
                out txCFIInfo);
        }

        public void DoCompare(string OutFile1, string OutFile2,
            out int DoCompResult, out string mRXCompareStr,
            out string mTXCompareStr, out CFIInfo rxCFIInfo,
            out CFIInfo txCFIInfo)
        {
            DoCompare(OutFile1, OutFile2, out DoCompResult, out mRXCompareStr,
                out mTXCompareStr, out rxCFIInfo, out txCFIInfo, true, 0);
        }

        public void DoCompare(string OutFile1, string OutFile2, out int DoCompResult,
            out string mRXCompareStr, out string mTXCompareStr,
            out CFIInfo rxCFIInfo, out CFIInfo txCFIInfo, bool maskToggle, int iter)
        {
            string rxLabel, txLabel;
            DoCompare(OutFile1, OutFile2, out DoCompResult, out rxLabel, out txLabel, out mRXCompareStr, out mTXCompareStr,
                      out rxCFIInfo, out txCFIInfo, maskToggle, iter);
        }

        public void DoCompare(string OutFile1, string OutFile2, out int DoCompResult,
            out string rxLabel, out string txLabel, out string mRXCompareStr, out string mTXCompareStr,
            out CFIInfo rxCFIInfo, out CFIInfo txCFIInfo, bool maskToggle, int iter)
        {
            CFIReader cfiReader = new CFIReader();
            rxLabel = cfiReader.GetCfiLabel(OutFile1);
            mRXCompareStr = cfiReader.GetCFIDataString(OutFile1,
                out rxCFIInfo, maskToggle, -1);
            txLabel = cfiReader.GetCfiLabel(OutFile2);
            mTXCompareStr = cfiReader.GetCFIDataString(OutFile2,
                out txCFIInfo, maskToggle, iter);

            bool m_dataExec = false;
            if (mRXCompareStr.Contains("B"))
            {
                m_dataExec = true;
            }

            if (m_dataExec)
                DoCompResult = CompareDataInfo(mRXCompareStr,
                    mTXCompareStr);
            else
                DoCompResult = CompareFreqInfo(mRXCompareStr,
                    mTXCompareStr);

            cfiReader = null;
        }

        public void DoCompare(string OutFile1, string OutFile2,
            out int DoCompResult)
        {
            CFIInfo rxCFIInfo, txCFIInfo;

            DoCompare(OutFile1, OutFile2, out DoCompResult,
                out rxCFIInfo, out txCFIInfo);
        }

        public void DoCompare(string OutFile1, string OutFile2,
            out int DoCompResult, bool maskToggle)
        {
            string rxCompareStr, txCompareStr;
            CFIInfo rxCFIInfo, txCFIInfo;

            DoCompare(OutFile1, OutFile2, out DoCompResult,
                out rxCompareStr, out txCompareStr, out rxCFIInfo, out txCFIInfo,
                maskToggle, -1);
        }

        public void DoCompare(string OutFile1, string OutFile2,
            out int DoCompResult, out CFIInfo rxCFIInfo,
            out CFIInfo txCFIInfo)
        {
            string mRXCompareStr;
            string mTXCompareStr;

            DoCompare(OutFile1, OutFile2,out DoCompResult,
                out mRXCompareStr,out mTXCompareStr, 
                out rxCFIInfo, out txCFIInfo);
        }

        public List<string> DoReport(string OutFile1, string OutFile2,
            out int DoCompResult)
        {
            CFIInfo rxCFIInfo, txCFIInfo;
            List<string> errorMessages;

            DoCompare(OutFile1, OutFile2, out DoCompResult,
                out rxCFIInfo, out txCFIInfo);
            CompareCFIInfo(rxCFIInfo, txCFIInfo, out errorMessages);
            return errorMessages;
        }

        public bool CompareCFIInfo(CFIInfo rxCFIInfo,
            CFIInfo txCFIInfo, out List<string> errorMessages)
        {
            List<string> warnings;
            bool compareResult = CompareCFIInfo(rxCFIInfo, txCFIInfo, -1, out warnings, out errorMessages);
            errorMessages.InsertRange(0, warnings);
            return compareResult;
        }

        public bool CompareCFIInfo(CFIInfo rxCFIInfo,
            CFIInfo txCFIInfo, int exec, out List<string> warningMessages, out List<string> errorMessages)
        {
            bool matched = true;
            warningMessages = new List<string>();
            errorMessages = new List<string>();

            if ( rxCFIInfo.FrequencyDev(txCFIInfo) > 0 )
            {
                matched = false;
                errorMessages.Add( String.Format(
                    "Timing error: frequency not matched:  "
                    + "Frequency(Capture)= {0:00.00}, "
                    + "Frequency(DB)= {1:00.00}, dev = {2:0.00} "
                    + " > {3:0.00}", rxCFIInfo.Frequency, 
                    txCFIInfo.Frequency,  rxCFIInfo.FrequencyDev(txCFIInfo),
                    CFIInfo.FREQUENCY_TOLERANCE));
            }

            if (rxCFIInfo.NumFrame < txCFIInfo.NumFrame)
            {                
                if (CFIInfo.FRAME_COUNT)
                    matched = false;
                errorMessages.Add(String.Format(
                    "Timing error: frame number not matched:  "
                    + "Frame Number(Capture)= {0}, Frame Number(DB)= {1}",
                    rxCFIInfo.NumFrame, txCFIInfo.NumFrame));
            }

            int minFrames = Math.Min(rxCFIInfo.NumFrame, txCFIInfo.NumFrame);

            int i = 0;

            for (; i < minFrames - 1; i++)
            {
                if (rxCFIInfo.FrameData[i] != "Valid"
                    && txCFIInfo.FrameData[i] != "Valid")
                {
                    if (rxCFIInfo.FrameData[i] != txCFIInfo.FrameData[i])
                    {
                        matched = false;
                        warningMessages.Add(String.Format(
                            "Warning: frame {0} (Capture) {1}          "
                            + "frame {0} (DB) {2}",
                            i + 1, rxCFIInfo.FrameData[i],
                            txCFIInfo.FrameData[i]));
                    }
                }
                else if (rxCFIInfo.FrameData[i] != "Valid")
                {
                    matched = false;
                    errorMessages.Add(String.Format(
                        "Timing error: frame {0} (Capture) {1}",
                        i + 1, rxCFIInfo.FrameData[i]));
                }
                else if (txCFIInfo.FrameData[i] != "Valid")
                {
                    matched = false;
                    errorMessages.Add(String.Format(
                        "Timing error: frame {0} (DB) {1}",
                        i + 1, txCFIInfo.FrameData[i]));
                }
            }

            int j;

            if (txCFIInfo.NumFrame <= rxCFIInfo.NumFrame)
            {
                i = rxCFIInfo.NumFrame - 1;
                j = txCFIInfo.NumFrame - 1;

                if (rxCFIInfo.FrameData[i] != "Valid"
                    && txCFIInfo.FrameData[j] != "Valid")
                {
                    if (rxCFIInfo.FrameData[i] != txCFIInfo.FrameData[j])
                    {
                        matched = false;
                        warningMessages.Add(String.Format(
                            "Warning: frame {0} (Capture) {1}          f"
                            + "rame {2} (DB) {3}",
                            i + 1, rxCFIInfo.FrameData[i], j + 1,
                            txCFIInfo.FrameData[j]));
                    }
                }
                else if (rxCFIInfo.FrameData[i] != "Valid")
                {
                    matched = false;
                    errorMessages.Add(String.Format(
                        "Timing error: frame {0} (Capture) {1}",
                        i + 1, rxCFIInfo.FrameData[i]));
                }
                else if (txCFIInfo.FrameData[j] != "Valid")
                {
                    matched = false;
                    errorMessages.Add(String.Format(
                        "Timing error: frame {0} (DB) {1}",
                        j + 1, txCFIInfo.FrameData[j]));
                }
            }

            if ( rxCFIInfo.LineList == null 
                && txCFIInfo.LineList == null)
                return matched;

            if ( rxCFIInfo.LineList == null )
            {
                errorMessages.Add(String.Format(
                    "Timing error: empty capture file "));
                return false;
            }

            if (txCFIInfo.LineList == null )
            {
                errorMessages.Add(String.Format(
                    "Timing error: empty DB file "));
                return false;
            }

            i = 0;
            j = 0;

            int endFrameLineOffset = 0;
            if (exec == 364 || exec == 484 || exec == 485)
                endFrameLineOffset = 1;
            else if (exec == 402 || exec == 420)
            {
                int indexOfSecondToLastFrame = rxCFIInfo.FrameList.Count - 2;
                if(indexOfSecondToLastFrame >=0)
                endFrameLineOffset = rxCFIInfo.IndexOfLastFrames -
                                     rxCFIInfo.FrameList[indexOfSecondToLastFrame].LineIndex;
            }

            bool processingEndFrames = false;
            for (; i < rxCFIInfo.LineList.Count &&
                j < txCFIInfo.LineList.Count; i++, j++)
            {
                if (processingEndFrames == false && i >= txCFIInfo.LineList.Count)
                {
                    if (rxCFIInfo.NumFrame <= txCFIInfo.NumFrame)
                    {
                        matched = false;
                        errorMessages.Add(String.Format(
                            "Timing error: Capture line {0}: {1}"
                            + "          DB: empty frame",
                            rxCFIInfo.LineList[i].lineNo,
                            rxCFIInfo.LineList[i].Name));
                    }
                    break;
                }

                if (j == txCFIInfo.IndexOfLastFrames - endFrameLineOffset)
                {
                    i = rxCFIInfo.IndexOfLastFrames - endFrameLineOffset;
                    processingEndFrames = true;
                }

                if (rxCFIInfo.LineList[i].Name != txCFIInfo.LineList[j].Name)
                {
                    matched = false;
                    errorMessages.Add(String.Format(
                        "Timing error: Capture line {0}: {1}"
                        + "          DB: line {2}: {3}",
                        rxCFIInfo.LineList[i].lineNo,
                        rxCFIInfo.LineList[i].Name,
                        txCFIInfo.LineList[j].lineNo,
                        txCFIInfo.LineList[j].Name));
                    continue;
                }

                if (rxCFIInfo.LineList[i].Name == "Data")
                    continue;

                if (rxCFIInfo.LineList[i].Name == "CodeGap"
                    && i == rxCFIInfo.LineList.Count - 1)
                    continue;

                bool validPulseDelay = false;
                if (rxCFIInfo.LineList[i].Name == "Pulse"
                    && (rxCFIInfo.LineList[i].delay != 0 || txCFIInfo.LineList[j].delay != 0)
                    && rxCFIInfo.Frequency == 0F)
                {
                    bool lastRxFrameAndPulse = rxCFIInfo.LineList[i].LastPulse && i >= rxCFIInfo.IndexOfLastFrames;
                    bool lastTxFrameAndPulse = txCFIInfo.LineList[j].LastPulse && j >= txCFIInfo.IndexOfLastFrames;
                    if (lastRxFrameAndPulse == false && lastTxFrameAndPulse == false)
                    {
                        double dev = rxCFIInfo.LineList[i].PulseDelayDev(txCFIInfo.LineList[j]);
                        if (dev > 0)
                        {
                            matched = false;
                            errorMessages.Add(
                                String.Format("Timing error: Pulse/Delay not matched:"
                                              + "  Pulse/Delay(Capture) = {0}, Pulse/Delay(DB)"
                                              + " = {1}, dev = {2:0.00}  > {3:0.00}",
                                              rxCFIInfo.LineList[i].Value
                                              + rxCFIInfo.LineList[i].delay,
                                              txCFIInfo.LineList[j].Value
                                              + txCFIInfo.LineList[j].delay,
                                              dev, CFILineInfo.PULSE_DELAY_TOLERANCE));
                            continue;
                        }
                    }

                    validPulseDelay = true;
                }

                if (rxCFIInfo.LineList[i].ValueDiff(txCFIInfo.LineList[j]) > 0)
                {
                    string messageTitle = "Timing error";
                    double tolerance = 0;

                    if (rxCFIInfo.LineList[i].Name == "Pulse")
                    {
                        if (validPulseDelay)
                            continue;

                        tolerance = CFILineInfo.PULSE_TOLERANCE;
                    }
                    else if (rxCFIInfo.LineList[i].Name == "Delay")
                        tolerance = CFILineInfo.DELAY_TOLERANCE;
                    else if (rxCFIInfo.LineList[i].Name == "CodeGap")
                    {
                        tolerance = CFILineInfo.CODEGAP_TOLERANCE;
                        if (exec == 364 || exec == 484 || exec == 485)
                            messageTitle = "Warning";

                        if (exec == 402)
                        {
                            if (txCFIInfo.NumFrame == 2)//LEFT RIGHT keys
                            {
                                matched = true;
                                messageTitle = "Warning";
                            }
                            else
                            {
                    matched = false;
                                messageTitle = "Timing error";
                            }
                        }
                    }

                    
                    string message = String.Format(
                        "{0}: {1} not matched:  "
                        + "{2}(Capture)= {3}, {4}(DB)= {5}, dev = {6:0.00} "
                        + " > {7:0.00}",
                        messageTitle,
                        rxCFIInfo.LineList[i].Name,
                        rxCFIInfo.LineList[i].Name,
                        rxCFIInfo.LineList[i].Value,
                        txCFIInfo.LineList[j].Name,
                        txCFIInfo.LineList[j].Value,
                        rxCFIInfo.LineList[i].ValueDiff(txCFIInfo.LineList[j]),
                        tolerance);

                    if (messageTitle == "Warning")
                        warningMessages.Add(message);
                    else
                        errorMessages.Add(message);

                    continue;
                }
            }
            /*
            if (i < txCFIInfo.LineList.Count)
            {
                matched = false;    
                errorMessages.Add(String.Format(
                    "Timing error: Capture: empty frame"
                    + "          DB: line {0}: {1}",
                    txCFIInfo.LineList[i].lineNo,
                    txCFIInfo.LineList[i].Name));
            }
            */
            return matched;
        }

        #endregion

        #region Private functions

        private int CompareDataInfo(string mRXCompareStr,
            string mTXCompareStr)
        {
            if (String.IsNullOrEmpty(mRXCompareStr)
                || String.IsNullOrEmpty(mTXCompareStr))
                return 0;

            if (mRXCompareStr == "Corrupted data frame!")
                return 0;

            if (FingerPrintMatches(mRXCompareStr, mTXCompareStr))
                return 1;

            return 0;
        }

        private bool FingerPrintMatches(string mRxCompareStr, string mTxCompareStr) 
        {
            string[] separator = new string[] { "FD" };
            string[] txFrameData = mTxCompareStr.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            string[] rxFrameData = mRxCompareStr.Split(separator, StringSplitOptions.RemoveEmptyEntries);

            bool matching = false;
            int maxFrameCounter = 2;
            try
            {
                //Modified by: Kini
                //Date: 09/30/2011
                //Case 1: When rx frame length and tx frame length are equal
                if (rxFrameData.Length == txFrameData.Length)
                {
                    //Case 1a: When rx frame length and tx frame lengths are same and also greater than two frames
                    // In such case we are restricting the comparison to two frames
                    // This is only on an experimental basis and needs to be changed in future
                    // This modification was done to handle last frame CFI errors, but might impact genuine third or more frame differences
                    if (rxFrameData.Length > maxFrameCounter)
                    {
                        for (int frameDataCount = 0; frameDataCount < maxFrameCounter; frameDataCount++)
                        {
                            matching = (txFrameData[frameDataCount].Trim() == rxFrameData[frameDataCount].Trim());
                            if (matching == false)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        //When rx frame length and tx frame length = 1
                        for (int frameDataCount = 0; frameDataCount < rxFrameData.Length; frameDataCount++)
                        {
                            matching = (txFrameData[frameDataCount].Trim() == rxFrameData[frameDataCount].Trim());
                            if (matching == false)
                            {
                                break;
                            }
                        }
                    }
                }
                //Case 2: When rx frame length greater than tx frame length
                else if (rxFrameData.Length > txFrameData.Length)
                {
                    for (int frameDataCount = 0; frameDataCount < txFrameData.Length; frameDataCount++)
                    {
                        matching = (txFrameData[frameDataCount].Trim() == rxFrameData[frameDataCount].Trim());
                        if (matching == false)
                        {
                            break;
                        }
                    }
                }
                //Case 3: When tx frame length greater than rx frame length
                else if (rxFrameData.Length < txFrameData.Length)
                {
                    for (int frameDataCount = 0; frameDataCount < rxFrameData.Length; frameDataCount++)
                    {
                        matching = (txFrameData[frameDataCount].Trim() == rxFrameData[frameDataCount].Trim());
                        if (matching == false)
                        {
                            break;
                        }
                    }
                }



                return matching;
            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private int CompareFreqInfo(string mRXCompareStr,
            string mTXCompareStr)
        {
            if (String.IsNullOrEmpty(mRXCompareStr)
                || String.IsNullOrEmpty(mTXCompareStr))
                return 0;

            if (mRXCompareStr == "Corrupted data frame!")
                return 0;

            double[] RX_Freq = new double[FREQ_ARRAY_SIZE];
            double[] TX_Freq = new double[FREQ_ARRAY_SIZE];

            GetFreqDataIntoArray(mRXCompareStr, RX_Freq);
            GetFreqDataIntoArray(mTXCompareStr, TX_Freq);
            int j = 0;
            int i = 0;

            while (i < FREQ_ARRAY_SIZE && j < FREQ_ARRAY_SIZE)
            {
                if (RX_Freq[i] == -1.00 || TX_Freq[j] == -1.00)
                {
                    if (i == 0)
                        return 0;
                }

                if (TX_Freq[j] == 0.00)
                {
                    if (RX_Freq[i] - TX_Freq[j] > m_freqTolerance)
                    {
                        return 0;
                    }
                }
                else if ((fabs(RX_Freq[i] - TX_Freq[i]) / TX_Freq[i]) > m_freqTolerance)
                {
                    return 0;
                }

                if (RX_Freq[i] == -1.00 && TX_Freq[j] == -1.00)
                {
                    break;
                }

                if (RX_Freq[i] != -1.00)
                    i++;

                if (TX_Freq[j] != -1.00)
                    j++;
            }

            return 1;
        }

        private void GetFreqDataIntoArray(string TXRXStr,
            double[] TXRXFreq)
        {
            for (int i = 0; i < FREQ_ARRAY_SIZE; i++)
                TXRXFreq[i] = -1.00;

            string[] str = TXRXStr.Split(new string[] { "FD" },
                StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < str.Length && i < FREQ_ARRAY_SIZE; i++)
            {
                str[i] = str[i].Trim();
                TXRXFreq[i] = double.Parse(str[i]);
            }
        }

        private double fabs(double d)
        {
            return d > 0 ? d : -d;
        }

        #endregion
    }

    public interface IICompareID {

        void DoCompare(out int DoCompResult, string mRXCompareStr, string mTXCompareStr);
        void DoCompare(string OutFile1, out int DoCompResult, out string mRXCompareStr, string mTXCompareStr);
        void DoCompare(string OutFile1, out int DoCompResult, out string mRXCompareStr, string mTXCompareStr, 
                                out CFIInfo rxCFIInfo);
        void DoCompare(string OutFile2, out int DoCompResult, string mRXCompareStr, out string mTXCompareStr);
        void DoCompare(string OutFile2, out int DoCompResult, string mRXCompareStr, out string mTXCompareStr,
                                out CFIInfo txCFIInfo);
        void DoCompare(string OutFile1, string OutFile2, out int DoCompResult, out string mRXCompareStr,
                                out string mTXCompareStr);
        void DoCompare(string OutFile1, string OutFile2, out int DoCompResult, out string mRXCompareStr,
                                out string mTXCompareStr, out CFIInfo rxCFIInfo, out CFIInfo txCFIInfo);
        void DoCompare(string OutFile1, string OutFile2, out int DoCompResult, out string mRXCompareStr,
                                out string mTXCompareStr, out CFIInfo rxCFIInfo, out CFIInfo txCFIInfo,
                                bool maskToggle, int iter);
        void DoCompare(string OutFile1, string OutFile2, out int DoCompResult);
        void DoCompare(string OutFile1, string OutFile2, out int DoCompResult, 
                                out CFIInfo rxCFIInfo,out CFIInfo txCFIInfo);
        List<string> DoReport(string OutFile1, string OutFile2, out int DoCompResult);
    }
}
