using System;
using System.Windows.Forms;

namespace IRMaestro
{
    static class Program
    {
        private const string IDToFIKey = "ID_TO_FI";
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();

           
            //Application.SetCompatibleTextRenderingDefault(false);
            ToolType toolType = ToolType.None;
            if (args != null && args.Length > 0 && args[0] == "READ_BACK_VERIFICATION")
                toolType = ToolType.READ_BACK;
            else if (args != null && args.Length > 0 && args[0] == "READ_BACK_RESULT")
                toolType = ToolType.READ_BACK_RES;
            else if (args != null && args.Length > 0 && args[0] == "PICK_TO_CFI")
                toolType = ToolType.PICK_TO_CFI;
            else if (args != null && args.Length > 0 && args[0] == "DECODE_IR")
                toolType = ToolType.DECODE_IR;
            else if (args != null && args.Length > 0 && args[0] == "IR_VERIFICATION")
                toolType = ToolType.IR_VERIFICATION;
            else if (args != null && args.Length > 0 && args[0] == "MACRO_ANALYZER")
                toolType = ToolType.MACRO_ANALYZER;
            else if (args != null && args.Length > 0 && args[0] == "PICK_TO_FI")
                toolType = ToolType.PICK_TO_FI;
            else if (args != null && args.Length > 0 && args[0] == "ID_COMPARE")
                toolType = ToolType.ID_COMPARE;
            else if (args != null && args.Length > 0 && args[0] == "TIMING_INSP")
                toolType = ToolType.TIMING_INSP;
            else if (args != null && args.Length > 0 && args[0] == "TIMING_INS_REP")
                toolType = ToolType.TIMING_INS_REP;
            else if (args != null && args.Length > 0 && args[0] == IDToFIKey)//added by binil
                toolType = ToolType.ID_TO_FI;

            Application.Run(new MainForm(toolType));
        }
    }
}