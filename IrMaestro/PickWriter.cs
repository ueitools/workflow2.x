using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using CommonForms;
using BusinessObject;
using PickAccessLayer;
using PickData;
using Row = System.Collections.Generic.IDictionary<string, string>;
using UEI.Legacy;
using IRMaestro.Forms;
using ProjectPickUpdater.Forms;
using ProjectPickUpdater;
using CommonCode;
using SQLPickReader;

namespace IRMaestro
{
    public struct ICSettings
    {
        public static string DBString = DBConnectionString.LOCALPRODUCT;
    }
    public struct ProjectParam
    {
        public string dbPath;
        public string productName;
        public bool internalPick;
        public bool cfiOnly;
        public string cfiPath;
    }
    class PickWriter
    {
        #region Private Fields
        //private RichTextBox ReportTB;
        private PickToFIDlg PickOptionForm;
        private bool bExtFunc;
        private bool bSeparateToggleOutput;
        private bool bAllLabels;
    
        private CommonForms.IDSelectionForm IDSel;

        private SQLPickReader.SQLPickReader PickReader = null;
        private PrjWrapper PrjHelper;

        string _projectName;
               
        string workdir;

        #endregion

        public void Init()
        {
            int countFiles = 0;
            workdir = GetWorkPath();
            PickOptionForm = new PickToFIDlg();
            PrjHelper = new PrjWrapper();            

            DialogResult res1 = PickOptionForm.ShowDialog();
            if (res1 == DialogResult.OK)
            {
                bExtFunc = PickOptionForm.ExtFunc;
                bSeparateToggleOutput = PickOptionForm.SeparateToggle;
                bAllLabels = PickOptionForm.AllLabels;
                _projectName = PickOptionForm.PickDBDir;
            }
            else
                return;

            PickOptionForm.Close();
                       
                    
            PickReader = new SQLPickReader.SQLPickReader();
            PickReader.PickDbName = _projectName;
            
            // IDSelectionForm from CommonForms Project
            IDSel = new CommonForms.IDSelectionForm();
            DialogResult res2 = IDSel.ShowDialog();
            IDSel.Close();
            
            // bug fix: db_query.dbd load path workaround
            Directory.SetCurrentDirectory(
                Environment.CommandLine.Substring(1,
                Environment.CommandLine.LastIndexOf("\\")));
            
            if (res2 == DialogResult.OK)
            {            
                ProgressDlg Progress = new ProgressDlg();
                Progress.Show();
                countFiles = GenerateFiles(Progress);
                Progress.Close();
                string msg = String.Format(
                    "{0} Zip FI files generated in {1}",
                    countFiles,workdir);
                MessageBox.Show(msg);
            }

            IDSel.Dispose();
        }

        public DialogResult InitCFI()
        {
            List<string> idlist = new List<string>();
            ProjectParam param = new ProjectParam();
            PickToCFIDlg pickCFIDlg = new PickToCFIDlg();

            if (pickCFIDlg.ShowDialog() == DialogResult.OK)
            {
                IList<string> allIDList = new List<string>();
                string productDBConn = "";

                param.cfiPath = pickCFIDlg.GetCFIPath();
                param.productName = pickCFIDlg.GetProductName();

                if (pickCFIDlg.IsPickSource())
                {
                    PickReader = new SQLPickReader.SQLPickReader();
                    PickReader.PickDbName = _projectName;

                    allIDList = PickReader.GetAllIDList();
                    IList<string> idList = pickCFIDlg.GetIDList(allIDList);

                    return GenerateCFIFiles(idList, param);
                }
                else
                {
                    productDBConn = pickCFIDlg.GetProductDBConnectionString();
                    DAOFactory.ResetDBConnection(productDBConn);
                    bool isPick2 = false;
                    PickData.IProject pick2Project = PickRetrieveFunctions.GetPickConfigAsProject(param.productName, out isPick2);

                    if (!isPick2)
                    {
                        PickIDSnapshotCollection pickIDCollection =
                            ProductDBFunctions.GetPickIDSnapshotHeader(param.productName);
                        foreach (PickIDSnapshot pickSnapshot in pickIDCollection)
                            allIDList.Add(pickSnapshot.ID);
                        IList<string> idList = pickCFIDlg.GetIDList(allIDList);
                        return GenerateCFIFilesFromProductDB(idList, param, productDBConn, pickIDCollection);
                    }
                    else
                    {
                        Pick2DAO pick2Dao = new Pick2DAO();
                        int countFiles = 0;
                        IList<Hashtable> idVersionList = pick2Dao.GetPickIdList(param.productName);
                        List<string> allPickedIdList = new List<string>();

                        foreach (Hashtable idVersion in idVersionList)
                        {
                            string idName = (string) idVersion["FK_ID"];

                            allPickedIdList.Add(idName.Trim().ToUpper());
                        }
                        IList<string> idList = pickCFIDlg.GetIDList(allPickedIdList);

                        IList<Hashtable> filteredIdVersionList = new List<Hashtable>();

                        foreach (Hashtable idVersion in idVersionList)
                        {
                            string idName = (string)idVersion["FK_ID"];

                            if (idList.Contains(idName.Trim().ToUpper()))
                            {
                                filteredIdVersionList.Add(idVersion);
                            }
                        }
                        Dictionary<string,PickIDSnapshot> idFunctionLookup = new Dictionary<string, PickIDSnapshot>();
                        foreach (Hashtable idVersion in filteredIdVersionList)
                        {
                            string idName = (string)idVersion["FK_ID"];
                            int version = (int)idVersion["Version"];

                            PickIdHeader pickIdHeader = pick2Dao.GetPickIdHeader(param.productName, idName, version);

                            IList<PickPrefix> prefixes = pick2Dao.GetPickPrefixWithVersion(param.productName,
                                                                                           idName,
                                                                                           version);
                            IList<Hashtable> dbFunctions = pick2Dao.GetPickDataMapWithVersion(param.productName,
                                                                                               idName,
                                                                                               version);
                            idFunctionLookup.Add(idName, PickIDSnapshotAdapter.Adapt(pickIdHeader, prefixes, dbFunctions));
                        }

                        TransferProgressForm.TransferID getAndWrite = delegate(string id)
                        {
                            PickIDSnapshot pickID;

                            if (idFunctionLookup.TryGetValue(id, out pickID))
                            {
                                WriteCFI(pickID, param.cfiPath);
                                countFiles++;
                            }
                        };

                        return ShowAndTransfer(idList, getAndWrite, ref countFiles);
                    }
                }
            }
            else
                return DialogResult.Cancel;
        }

        private DialogResult GenerateCFIFiles(IList<string> IDList, ProjectParam userParam)
        {
            int countFiles = 0;
            PickTransfer pickTransfer = new PickTransfer(null, null);

            TransferProgressForm.TransferID getAndWrite = delegate(string id)
            {
                //PickIDSnapshot pickSnapshot = null;
                //bool tempFlag = false;
                //bool internalPick = true;

                //if (pickTransfer.GetAndSetPickIDSnapshot(
                //    userParam.productName, id, tempFlag, internalPick, out pickSnapshot, PickReader) &&
                //    WriteCFI(pickSnapshot, userParam.cfiPath))
                //    countFiles++;
            };
            return ShowAndTransfer(IDList, getAndWrite, ref countFiles);
        }

        private DialogResult GenerateCFIFilesFromProductDB(IList<string> IDList, ProjectParam userParam,
            string productDBConn, PickIDSnapshotCollection pickIDCollection)
        {
            int countFiles = 0;
            PickTransfer pickTransfer = new PickTransfer(null, productDBConn);

            TransferProgressForm.TransferID getAndWrite = delegate(string id)
            {
                PickIDSnapshot pickSnapshot = pickTransfer.FindPickIDSnapshot(id, pickIDCollection);
                if (pickSnapshot != null)
                {
                    WriteCFI(pickSnapshot, userParam.cfiPath);
                    countFiles++;
                }
            };
            return ShowAndTransfer(IDList, getAndWrite, ref countFiles);
        }

        private DialogResult ShowAndTransfer(
            IList<string> IDList, 
            TransferProgressForm.TransferID getAndWrite,
            ref int countFiles)
        {
            TransferProgressForm.ShowAndTransferID(IDList, getAndWrite);

            FinishedCFIForm finishForm =
                new FinishedCFIForm("Created " + countFiles + " cfi zip files.");

            return finishForm.ShowDialog();
        }

        private bool WriteCFI(PickIDSnapshot pickID, string path)
        {
            bool result = false;

            if (!path.EndsWith("\\"))
                path += "\\";

            if (pickID.IsExternalPrefix == "N")
            {
                List<string> cfiFiles =
                    FunctionList.CreateTxCfiFiles(PickIDSnapshot.AdaptToPickID(pickID), path);
                List<CaptureInfo> captureList = new List<CaptureInfo>();

                for (int i = 0; i < cfiFiles.Count; i++)
                {
                    File.Move(path + cfiFiles[i], path + cfiFiles[i].ToUpper());
                    cfiFiles[i] = cfiFiles[i].ToUpper();
                }
                foreach (string cfiName in cfiFiles)
                {
                    DataMap dataMap = FindDataMap(pickID, cfiName);

                    if (dataMap != null)
                    {
                        string label = dataMap.KeyLabel + " (" + dataMap.Outron + ")";

                        CaptureInfo captureInfo = new CaptureInfo(label, cfiName, dataMap.Outron);
                        captureList.Add(captureInfo);
                    }
                }
                string projXmlName = pickID.ID + "cfi.xml";

                CreateProjectFileXML(path + projXmlName, captureList, pickID.ID);
                cfiFiles.Add(projXmlName);
                CommonCode.ZipHelper.CreateZipFile(path + pickID.ID + "CFI.zip", cfiFiles, path);
                foreach (string filename in cfiFiles)
                    File.Delete(path + filename);
                result = true;
            }
            return result;
        }

        private DataMap FindDataMap(PickIDSnapshot pickID, string cfiFilename)
        {
            DataMap result = null;

            foreach (DataMap dataMap in pickID.DataMapList)
            {
                string path = FunctionList.MakeCFIFilename(pickID.ID, dataMap.Outron, "");

                if (string.Compare(path, cfiFilename, true) == 0)
                {
                    result = dataMap;
                    break;
                }
            }
            return result;
        }

        private void CreateProjectFileXML(string xmlProjectPath, List<CaptureInfo> captureList, string id)
        {
            ProjectInfo projectInfo = new ProjectInfo();
            projectInfo.FileList = captureList.ToArray();
            projectInfo.ID = id;
            projectInfo.ToXMLFile(xmlProjectPath, projectInfo);
        }

        private int GenerateFiles(ProgressDlg Progress)
        {
            int countFiles = 0;
            
            // Get the list of IDs that we're gonna process
            
            IList<string> allIDList = PickReader.GetAllIDList();

            
           
           List<string> IDList = IDSel.GetSelectedIDList(allIDList);
            

            Progress.SetupProgressBar(0, IDList.Count, 1);

            //Add by Satyadeep
            //Delete if Error Log File Exists
            String pictofierrorlogFile = workdir + "PickToFiError.log";
            try
            {                
                if(File.Exists(pictofierrorlogFile))
                    File.Delete(pictofierrorlogFile);
            }
            catch
            {
            }
            
            try
            {
                foreach (string ID in IDList)
                {
                    Progress.UpdateProgressBar();
                    Progress.Text = string.Format("Processing ID {0}", ID);
                    MakeFile(ID);
                    countFiles++;
                }
                //Added by Satyadeep
                //PIC to FI Error Log                               
                if(File.Exists(pictofierrorlogFile))
                {
                    StreamWriter pictofierrorLogStreamWriter = new StreamWriter(pictofierrorlogFile,true);
                    pictofierrorLogStreamWriter.WriteLine(DateTime.Now.ToString("MM/dd/yyy h:mm:ss.fff tt") + ">> |- =============================================================");
                    pictofierrorLogStreamWriter.Close();
                    MessageBox.Show("Please review PickToFiError.log file\nin your Working Directory", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return countFiles;
        }

        private void MakeFile(string ID)
        {
            PrjHelper.Init(ID, workdir);
            
            /* deal with PickReader to get the prefixes and
             * pick items
             */
            int version = PickReader.GetMaxVersion(ID);
            Row id_header = PickReader.GetPickID(ID, version);
            IList<Row> PickItemRec = PickReader.GetPickItem(ID, version);
            IList<Row> PrefixList= PickReader.GetPrefixItem(ID, version);
            List<int> iPrefixList = PickReader.ConvertPrefixList(PrefixList);
            if (id_header["IsExternalPrefix"] == "Y")
            {
                ProcessExternalPrefix(ID, iPrefixList);
            }

            // get a Data - Label map in case we need multiple labels
            HashtableCollection mapDataLabel = getMapForDataLabel(ID);

             // for each item in PickItemRec
            foreach (Row data in PickItemRec) 
            {
                String strFIname = getFIfileName(ID,data["Outron"]); 
                int iFilesCreated = 0;
                if (IsUnWantedData(data))
                    continue;
                    
                // Create Tx output files
                iFilesCreated = FunctionList.CreateTxFiFile(
                   id_header, data, iPrefixList, workdir + strFIname,
                   this.bSeparateToggleOutput);
                
                // get label list for the data
                List<string> label_list = getLabelListForData(data,
                    bAllLabels, mapDataLabel);
                
                // write filename,label-list and outron into prj
                writeToPrjFile(strFIname, label_list,data["Outron"], 
                    iFilesCreated); 
            }
            PrjHelper.Save();
        }

        private HashtableCollection getMapForDataLabel(string ID)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ID", ID);
            AdHocDAO adhoc_DAO = new AdHocDAO();
            HashtableCollection tableDataLabel = 
                adhoc_DAO.Select("AdHoc.GetIDDataLabel", paramList);
            return tableDataLabel;
        }

        private string getFIfileName(string ID, string outron)
        {
            string strFIname = ID + outron;
            strFIname = strFIname.Replace("+", "@");
            strFIname = strFIname.Replace('<', '&');
            strFIname = strFIname.Replace('>', '!');
            return strFIname;
        }

        private bool IsUnWantedData(Row data)
        {
            if (data["DATA"] == null || data["DATA"] == "")
                return true;

            if (data["Outron"][0] == 'X' &&
                Char.IsDigit(data["Outron"][1]) &&
                Char.IsDigit(data["Outron"][2]) &&
                bExtFunc == false)
                return true;

            return false;
        }

        /* get a Data - Label map in case we need multiple labels */
        private List<string> getLabelListForData(Row data, bool multi_label,
            HashtableCollection mapDataLabel)
        {
            List<string> label_list = new List<string>();
            label_list.Add(data["Label"]);
            if (multi_label) // include multiple labels for each data record
            {
                foreach (Hashtable item in mapDataLabel)
                {
                    //Added by Satyadeep
                    //To Handle null data
                    if(item["Data"] != null)
                    {
                        if((label_list.Count > 1) &&
                            (item["Data"].ToString() != data["DATA"]))
                            break;

                        if((item["Data"].ToString() == data["DATA"]) &&
                            (!label_list.Contains(item["Label"].ToString())))
                            label_list.Add(item["Label"].ToString());
                    }
                }
            }
            return label_list;
        }

        private void writeToPrjFile(string strFIname, List<string> label_list,
            string outron, int iFilesCreated)
        {
            PrjHelper.Add(strFIname, label_list, outron);

            // Add Toggle output files
            for (int i = 1; i < iFilesCreated; i++)
            {
                PrjHelper.Add(strFIname + "_" + i.ToString(),
                    label_list, outron);
            }
        }

        public string GetWorkPath()
        {
            string workpath = CommonForms.Configuration.GetWorkingDirectory();
            if (!string.IsNullOrEmpty(workpath) && !workpath.EndsWith("\\"))
                workpath += "\\";

            return workpath;
        }

        /*  xPfxBlock()
         *
         *  read external prefixes in xpfile into List<int> prefix
         *  note: in C# there's no functions similar to sscanf(),
         *  so we scan the values by looping through the token
         */
        private int xPfxBlock(StreamReader xpfile, List<int> prefix)
        {
            string strLine = "";
            int num_ext_prefix = 0;
            string str1 = "";
            int ul1 = 0, ul2 = 0; // unsgigned long prefix data byte

            while (!xpfile.EndOfStream)
            {
                ul1 = 0;
                ul2 = 0;
                strLine = xpfile.ReadLine();
                if (strLine.Contains("endif") || strLine.Contains("ENDIF"))
                    break;

                getRC16SectionData(strLine, ref str1, ref ul1, ref ul2);
                
                if (ul1 > 0)
                {
                    prefix.Add(ul1);
                    num_ext_prefix++;
                }

                if (ul2 > 0)
                {
                    prefix.Add(ul2);
                    num_ext_prefix++;
                }
            }// while

            while (!xpfile.EndOfStream)
            {
                strLine = xpfile.ReadLine();
                if (strLine.Contains("DB"))
                {
                    str1 = getDBlineData(strLine);
                    ul1 = ((str1[0] == '1') ? 1 : 0);
                    for (int cnt = 1; cnt < 8; cnt++)
                    {
                        ul1 = ul1 << 1;
                        ul1 = ul1 + ((str1[cnt] == '1') ? 1 : 0);
                    }
                    prefix.Add(ul1);
                    num_ext_prefix++;
                }
            }// while
            return num_ext_prefix;
        }
        
        private void getRC16SectionData(string strLine, ref string str1,
            ref int ul1, ref int ul2)
        { 
            string[] Tokens = strLine.Split(new Char[] { ' ', ',' });
            int index_in_tokens = 0;

             for (; index_in_tokens < Tokens.Length;
               index_in_tokens++)
             {
                   if (Tokens[index_in_tokens] != "")
                   {
                       str1 = Tokens[index_in_tokens].Trim();
                       break;
                   }
             }

             for (index_in_tokens++; index_in_tokens < Tokens.Length;
               index_in_tokens++)
             {
               if (Tokens[index_in_tokens] != "")
               {
                   ul1 = Int32.Parse(Tokens[index_in_tokens].Trim());
                   break;
               }
             }

             if (str1 == "Burst" || str1 =="Freq")
             {
                 for (index_in_tokens++; index_in_tokens < Tokens.Length; 
                   index_in_tokens++)
                 {
                   if (Tokens[index_in_tokens] != "")
                   {
                       ul2 = Int32.Parse(Tokens[index_in_tokens].Trim());
                       break;
                   }
                 }
             }
        }

        private string getDBlineData(string strDBline)
        {
            string strData = "";
            string[] Tokens = strDBline.Split(' ');
            int index_in_tokens = 0;

            for (; index_in_tokens < Tokens.Length; index_in_tokens++)
            {
                if (Tokens[index_in_tokens] != "" &&
                    Tokens[index_in_tokens] == "DB")
                {
                    string dump = Tokens[index_in_tokens].Trim();
                    break;
                }
            }

            for (index_in_tokens++; index_in_tokens < Tokens.Length;
                index_in_tokens++)
            {
                if (Tokens[index_in_tokens] != "")
                {
                    strData = Tokens[index_in_tokens].Trim();
                    break;
                }
            }
            return strData;
        }

        /*  scanXprefix()
         * 
         *  scans the external prefix file and
         *  calls  xPfxBlock to do the parsing
         */
        private int scanXprefix(String dbDir, string id, List<int> prefix)
        {
            int len = dbDir.Length;
            if (dbDir[len-1] == '\\')
                dbDir.Remove(len, 1);
            if (Int32.Parse(id.Substring(1)) < 1000)
            {
                id = id.Remove(1, 1);
            }

            string file_path = String.Format("{0}\\xprefix\\{1}.exp", 
                dbDir, id);

            StreamReader xpfile = new StreamReader(file_path);
            string strLine = "";

            if (xpfile == null)
            {
                return 0;
            }

            // go through the file until we find the HC05RC16 section
            do
            {
                strLine = xpfile.ReadLine();
                if ((strLine.Contains(" IF ") || strLine.Contains(" if ")) && 
                    strLine.Contains(" HC05RC16"))
                    break;
            } while (!xpfile.EndOfStream);

            // safe exit when failed to find match in the external prefix file
            if (xpfile.EndOfStream)
            {
                xpfile.Close();
                xpfile = null;
                return 0;
            }

            int ret = xPfxBlock(xpfile, prefix);
            xpfile.Close();
            xpfile = null;
            return ret;
        }

        /* external prefix processing */
        private void ProcessExternalPrefix(string ID, List<int> iPrefixList)
        {
            string xpfx = CommonForms.WorkflowConfig.GetCfg("Xprefix");
            iPrefixList.Clear(); // this line might be changed later
            if (scanXprefix(xpfx, ID, iPrefixList) == 0)
            {
                MessageBox.Show("Error when scanning external prefix!");
            }
        }
    }

    class PickIDSnapshotAdapter
    {
        /// <summary>
        /// 1/21/2015 currently this is minimum for cfi generation, not complete
        /// </summary>
        /// <param name="prefixes"></param>
        /// <param name="functions"></param>
        /// <returns></returns>
        public static PickIDSnapshot Adapt(PickIdHeader pickIdHeader, IList<PickPrefix> prefixes, IList<Hashtable> functions)
        {
            PickIDSnapshot pickIdSnapshot = new PickIDSnapshot();

            pickIdSnapshot.Executor_Code = pickIdHeader.ExecutorCode;
            pickIdSnapshot.ID = pickIdHeader.ID;
            pickIdSnapshot.Name = pickIdHeader.ID;
            if (string.IsNullOrEmpty(pickIdHeader.ParentID))
                pickIdSnapshot.PickFromID = pickIdHeader.ID;
            else
                pickIdSnapshot.PickFromID = pickIdHeader.ParentID;
            pickIdSnapshot.IsInversedData = (pickIdHeader.IsInversedData ? "Y" : "N");
            pickIdSnapshot.IsHexFormat = (pickIdHeader.IsHexFormat ? "Y" : "N");
            pickIdSnapshot.IsInversedPrefix = (pickIdHeader.IsInversedPrefix ? "Y" : "N");
            pickIdSnapshot.IsExternalPrefix = (pickIdHeader.IsExternalPrefix ? "Y" : "N");
            pickIdSnapshot.IsFrequencyData = (pickIdHeader.IsFrequencyData ? "Y" : "N");

            pickIdSnapshot.PrefixList = new PrefixCollection();

            foreach (PickPrefix prefix in prefixes)
            {
                Prefix newPrefix = new Prefix();

                newPrefix.Data = prefix.Data;
                newPrefix.ID = prefix.ID;
                newPrefix.Description = prefix.Description;
                pickIdSnapshot.PrefixList.Add(newPrefix);
            }

            pickIdSnapshot.DataMapList = new DataMapCollection();

            foreach (Hashtable function in functions)
            {
                DataMap dataMap = new DataMap();

                dataMap.Data = (string) function["Data"];
                dataMap.Intron = ((string)function["FirmwareLabel"]).Trim(); 
                dataMap.KeyLabel = ((string) function["IDLabel"]).Trim();
                // outron must match firmware label for T0702 to repeat power key properly.
                dataMap.Outron = ((string)function["FirmwareLabel"]).Trim();
                dataMap.IntronPriority = ((string) function["IntronPriority"]).Trim();

                pickIdSnapshot.DataMapList.Add(dataMap);
            }
            return pickIdSnapshot;
        }
    }
}
