//********************************************************************************
//REVISION HISTORY
//--------------------------------------------------------------------------------
//03/15/2010
//--------------------------------------------------------------------------------
//- Modifications made in StarIrVerificationForm and StartMacroAnalyzerForm to support
//  both UEI Db and Pick Db
//
//03/24/2010
//---------------------------------------------------------------------------------
//- Added GetSelectedDBType method to get the previously selected DB Type from registry.
//03/25/2010
//----------------------------------------------------------------------------------
//- Removed GetSelectedDbType as this functionality was moved to presenter of IDV and MIDV
//
//*********************************************************************************
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using BusinessObject;
using CommonForms;
using IRMaestro.Forms;
namespace IRMaestro {
    public partial class MainForm : Form {
        public abstract class ChildFormId {
            public const string IR_VERIFICATION = "IrVerification";
            public const string MACRO_ANALYZER = "MacroAnalyzer";
            public const string ID_COMPARE = "IdCompare";
        }

        private Dictionary<string, Form> _childForms;
        private ToolType Tool;

        

        public MainForm(ToolType Type) {
            InitializeComponent();
            
            InitializeForm();
            Tool = Type;
            _childForms = new Dictionary<string, Form>();
        }

        private void InitializeForm() {
            Text = "IR Maestro";
            WindowState = FormWindowState.Maximized;

            ReportTB.Visible = false;
        }

        private void MainForm_Load(object sender, EventArgs e) {
           switch (Tool) {
                case ToolType.PICK_TO_FI:
                    RunPickToFi();
                    break;
                case ToolType.READ_BACK:
                    RunReadBackVerification();
                    break;
                case ToolType.READ_BACK_RES:
                    RunReadBackResults();

                    break;
                case ToolType.PICK_TO_CFI: {
                    RunPickToCFI();
                    break;
                }
                case ToolType.DECODE_IR: {
                    RunDecodeIr();
                    Close();
                    break;
                }
                case ToolType.IR_VERIFICATION: {
                    StartIrVerificationForm();
                    break;
                }
                case ToolType.MACRO_ANALYZER: {
                    StartMacroAnalyzerForm();
                    break;
                }
            case ToolType.ID_COMPARE: {
                StartIDCompareApp();
                   break;
                }
                case ToolType.TIMING_INSP: {
                    StartTimingInspector();
                        break;
                }
                case ToolType.TIMING_INS_REP: {
                    StartTIResults();
                    break;
                }
                case ToolType.ID_TO_FI:
                    GenerateFIForAllKeysInID();
                    break;


                default:
                    break;
            }
        }

        private void GenerateFIForAllKeysInID()
        {
            using (IDSelectionForm idSelectionForm = new IDSelectionForm())
            {
                if (idSelectionForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    IList<string> idList = idSelectionForm.SelectedIDList;

                    if (idList != null)
                    {
                        //IDToFI fiGenerator = new IDToFI(IDToFiCallBack);
                        //fiGenerator.GenerateFI(idList, CommonForms.Configuration.GetWorkingDirectory());
                        IDTOFIForm idToFIForm = new IDTOFIForm(idList, CommonForms.Configuration.GetWorkingDirectory());
                        idToFIForm.ShowDialog();
                    }
                }
                else
                {
                    DialogResult r = idSelectionForm.DialogResult;
                }
            }
        }

        private void IDToFiCallBack(IDToFI.IDToFIStatus status)
        {

        }
        private void RunPickToFi() {
            PickWriter PTF = new PickWriter();
            PTF.Init();
        }

        private void RunPickToCFI()
        {
            PickWriter writer = new PickWriter();
            if (writer.InitCFI() == DialogResult.Abort)
                Close();
        }

        private void RunReadBackVerification() {
            ReadBackDlg readBackDlg = new ReadBackDlg();
            readBackDlg.ShowDialog(this);
        }

        private void RunReadBackResults()
        {
            CFIListForm.bShowTimingReport = false;
            OpenReadbackResults();
        }

        private void OpenReadbackResults()
        {
            BringToFront();
            string WORKDIR = Configuration.GetWorkingDirectory();
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Report files (*.rpt)|*.rpt|All files (*.*)|*.*";

            if (File.Exists(WORKDIR + "results.rpt")) {
                dlg.FileName = WORKDIR + "results.rpt";
            }

            if (dlg.ShowDialog(this) == DialogResult.OK) {
                string file = Path.GetFileName(dlg.FileName);
                string path = dlg.FileName.Substring(0, dlg.FileName.LastIndexOf("\\") + 1);

                CompareReportForm reportForm = new CompareReportForm(path, file);
                reportForm.ShowDialog(this);
            }
        }

        private void RunDecodeIr() {
            DecodeIrSetupForm decodeIrSetupForm = new DecodeIrSetupForm();
            if (decodeIrSetupForm.ShowDialog(this) == DialogResult.Cancel) {
                return;
            }

            using (IrDecoder irDecoder = new IrDecoder(decodeIrSetupForm.FilePath)) {
                irDecoder.IdentifyCapture = decodeIrSetupForm.IdentifyFiles;
                irDecoder.DefinedExec = decodeIrSetupForm.Executor;
                irDecoder.GenericExecConfigPath = decodeIrSetupForm.GenericFilePath;
                irDecoder.KeepCfiFiles = decodeIrSetupForm.KeepCfiFiles;

                string summaryFile;
                if (irDecoder.StartProcess(this) &&
                    string.IsNullOrEmpty(summaryFile = irDecoder.GenerateSummaryFile()) == false) {
                    try {
                        System.Diagnostics.Process.Start(summaryFile);
                    } catch {
                        System.Diagnostics.Process.Start("NotePad", summaryFile);
                    }
                } else {
                    MessageBox.Show(this, irDecoder.LastErrorMessage, "Error decoding", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
            }
        }

        private void UpdateChildFormRecords() {
            List<string> removeList = new List<string>();
            foreach (KeyValuePair<string, Form> keyValuePair in _childForms) {
                if (keyValuePair.Value.IsDisposed) {
                    removeList.Add(keyValuePair.Key);
                }
            }

            foreach (string childId in removeList) {
                _childForms.Remove(childId);
            }
        }

        private Form GetChildForm(string childFormId) {
            UpdateChildFormRecords();

            Form childForm = null;
            if (_childForms.ContainsKey(childFormId)) {
                childForm = _childForms[childFormId];
            }

            if (childForm == null) {
                if (childFormId == ChildFormId.IR_VERIFICATION) {
                    childForm = new IdVerificationForm();
                } else if (childFormId == ChildFormId.MACRO_ANALYZER) {
                    childForm = new MacroAnalyzerForm();
                }
                else if (childFormId == ChildFormId.ID_COMPARE)
                {
                    childForm = new IDCompareForm();
                }
                else{
                    return null;
                }
                childForm.WindowState = FormWindowState.Minimized;
                _childForms.Add(childFormId, childForm);
            }

            childForm.MdiParent = this;
            childForm.ShowInTaskbar = false;

            return childForm;
        }

        private void StartIrVerificationForm() 
        {
            DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);

            Form childForm = GetChildForm(ChildFormId.IR_VERIFICATION);

            IdVerificationPresenter idVerificationPresenter = new IdVerificationPresenter(new DbFunctionsWrapper());
            idVerificationPresenter.SetView((IdVerificationForm)childForm);

            childForm.Show();
            childForm.WindowState = FormWindowState.Maximized;
        
        }

        private void StartMacroAnalyzerForm() {
                        
            DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);

            Form childForm = GetChildForm(ChildFormId.MACRO_ANALYZER);

            MacroAnalyzerPresenter macroAnalyzerPresenter = new MacroAnalyzerPresenter();
            macroAnalyzerPresenter.SetView((MacroAnalyzerForm)childForm);

            childForm.Show();
            childForm.WindowState = FormWindowState.Maximized;

        }

        private void pickToFIToolStripMenuItem_Click(object sender, EventArgs e) {
            RunPickToFi();
        }

        private void readBackVerificationToolStripMenuItem_Click(object sender, EventArgs e) {
            RunReadBackVerification();
        }

        private void readBackResultsToolStripMenuItem_Click(object sender, EventArgs e) {
            RunReadBackResults();
        }

        private void decoderToolStripMenuItem_Click(object sender, EventArgs e) {
            RunDecodeIr();
        }

        private void iDVerificationToolStripMenuItem_Click(object sender, EventArgs e) {
            StartIrVerificationForm();
        }

        private void macroAnalyzerToolStripMenuItem_Click(object sender, EventArgs e) {
            StartMacroAnalyzerForm();
        }

        
        private void uSBeeSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            USBeeSettings usbeepodsetting = new USBeeSettings();
            usbeepodsetting.Show();
        }

        private void iDCompareAppToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StartIDCompareApp();
        }

        private void StartIDCompareApp()
        {
            Form childForm = GetChildForm(ChildFormId.ID_COMPARE);
            childForm.Show();
            childForm.WindowState = FormWindowState.Maximized;        
        }
        
        private void timingInspectorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StartTimingInspector();
        }

        private void timingInspectorResultsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StartTIResults();
        }

        private void StartTimingInspector()
        {
            DoRBComparisionForm rbCmpForm = new DoRBComparisionForm();
            rbCmpForm.ShowDialog();
        }

        private void StartTIResults()
        {
            CFIListForm.bShowTimingReport = true;
            OpenReadbackResults();        
        }

        private void iDToFIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GenerateFIForAllKeysInID();
        }

        private void pickToCFIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RunPickToCFI();
        }

       
    }
    public enum ToolType
    {
        None,
        PICK_TO_CAPTURE,
        PICK_TO_FI,
        READ_BACK,
        READ_BACK_RES,
        DECODE_IR,
        IR_VERIFICATION,
        MACRO_ANALYZER,
        PICK_TO_CFI,
        ID_COMPARE,
        TIMING_INSP,
        TIMING_INS_REP,
        ID_TO_FI//Added by binil
    }
}