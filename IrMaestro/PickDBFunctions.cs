//using System;
//using System.Collections.Generic;
//using System.Collections;
//using System.IO;
//using System.Text;
//using BusinessObject;
//using CommonForms;
//using IDCompare;
//using UEI.Legacy;
//using System.Windows.Forms;
//namespace IRMaestro
//{
//    public class pickcfifiles
//    {
//        public List<string> cfiFilesFromPickID;
//        public Hashtable pickIntronTable;
//        public Hashtable pickLabelTable;
//        public Hashtable pickIntronPriorityTable;
//        public Hashtable dataTable;
//    }
//    public class MultiIDPickComparer
//    {
//        public List<PickIDComparer> pickcomparer;
//    }
//    public class PickIDComparer
//    {
//        public PickID id;
//        public string strID;
//        public PickDBObject pkObject;
//        public pickcfifiles cfifiles;
//        public string workingpath;
//    }
//    /// <summary>
//    /// Pick DB function class used to perform operations on Pick Object
//    /// </summary>
//    public class PickDBFunctions : PickDBObject
//    {
//        public static Dictionary<string, IDFunction> IDfunctionlist;
//        public static PickDBObject pickobject;
//        public PickDBFunctions()
//        {
//        }
//        private int matchcount = 0;
//        private string pickPath = string.Empty;
//        IMultiIdFunctionCompareHelper multiIdFunctionCompareHelper;
//        /// <summary>
//        /// Used in ID Verification
//        /// </summary>
//        /// <param name="id"></param>
//        /// <param name="signalpath"></param>
//        /// <returns></returns>
//        public MatchedIdFunctionGroup GetMatchedFunction(string id, string signalpath)
//        {
//            MatchedIdFunctionGroup matchedfuncs = CompareRdBack(PickDBObject.pickid, signalpath);
//            return matchedfuncs;
//        }
//        private IDFunction GetIDFunction(string matchedlabel)
//        {
//            IDFunction functioninfo = new IDFunction();
//            foreach (IDFunction fn in PickDBObject.id.FunctionList)
//            {
//                if (fn.Label == matchedlabel)
//                {
//                    functioninfo = fn;
//                    break;
//                }
//            }
//            return functioninfo;
//        }
//        /// <summary>
//        /// Does the following operations
//        /// 1. Checks whether the CFI files are generated for the selected ID
//        /// 2. If not present then generates the CFI files of all the keys in the selected ID
//        /// 3. Generates the CFI file of the captured signal
//        /// 4. Compares the captured signal with all the CFI and returns the key labels that matches with the captured data
//        /// 
//        /// </summary>
//        /// <param name="pickID"></param>
//        /// <param name="signalpath"></param>
//        /// <returns></returns>
//        private MatchedIdFunctionGroup CompareRdBack(PickID pickID, string signalpath)
//        {
//            string id = DBSettings.selectedId;
//            matchcount = 0;
//            //Gets the CFI files for all keys in the PickDb for the selected ID
//            if ((PickDBObject.id_cficollection == null))
//            {
//                PickDBObject.id_cficollection = new pickcfifiles();
//                IDfunctionlist = new Dictionary<string, IDFunction>();
//                PickDBObject.id_cficollection.cfiFilesFromPickID = new List<string>();
//                PickDBObject.id_cficollection.pickIntronPriorityTable = new Hashtable();
//                PickDBObject.id_cficollection.pickIntronTable = new Hashtable();
//                PickDBObject.id_cficollection.pickLabelTable = new Hashtable();
//                PickDBObject.id_cficollection.dataTable = new Hashtable();
//                PickDBObject.id_cficollection.cfiFilesFromPickID = FunctionList.CreateTxCfiFiles(pickID, DBSettings.pickIDWorkingPath,
//                        out PickDBObject.id_cficollection.pickIntronTable, out PickDBObject.id_cficollection.pickLabelTable, out PickDBObject.id_cficollection.pickIntronPriorityTable, out PickDBObject.id_cficollection.dataTable);
//            }
//            string capturepath = Path.GetDirectoryName(signalpath) + Path.DirectorySeparatorChar;
//            Hashtable rbLabelList = new Hashtable();
//            Hashtable rbOutronList = null;
//            string capturecfipath = capturepath + Path.GetFileNameWithoutExtension(signalpath) + ".cfi";
//            FunctionList.ConvertCaptureToCfiFile(capturecfipath, signalpath, pickID.OID.Header.Executor_Code);
//            ICompareID CfiCmp = new ICompareID();
//            CompareRes res = new CompareRes();
//            res.cfiFileList = new List<string>();
//            IPickReader pickReader = new PickReader(pickPath, "PICKDB02");
//            MatchedIdFunctionGroup fnsCollections = new MatchedIdFunctionGroup();
//            foreach (string pickIDCfiFile in PickDBObject.id_cficollection.cfiFilesFromPickID)
//            {
//                string data = (string)PickDBObject.id_cficollection.dataTable[pickIDCfiFile];
//                string pickcfipath = DBSettings.pickIDWorkingPath + pickIDCfiFile;
//                if (File.Exists(pickcfipath))
//                {
//                    int DoCompResult = 0;
//                    res.cfiFileList.Add(capturecfipath);
//                    CfiCmp.DoCompare(capturecfipath, pickcfipath, out DoCompResult);
//                    string tempdata = string.Empty;
//                    if (DoCompResult > 0)
//                    {
//                        matchcount++;
//                        IDFunction fnsmatches = new IDFunction();
//                        StringCollection keydata = new StringCollection();
//                        keydata = (StringCollection)PickDBObject.id_cficollection.pickLabelTable[data];
//                        fnsmatches = GetIDFunction(keydata[0]);
//                        fnsCollections.AddFunction(fnsmatches);
//                    }
//                }
//            }
//            return fnsCollections;
//        }
//        /// <summary>
//        /// Used in Macro Analyzer
//        /// </summary>
//        /// <param name="captureFiles"></param>
//        /// <returns></returns>
//        public MultiIdFunctionSearchResults SearchForMatch(params string[] captureFiles)
//        {
//            MultiIdFunctionSearchResults results = new MultiIdFunctionSearchResults();
//            foreach (string capturePath in captureFiles)
//            {
//                MultiIdFunctionSearchResults.Item resultItem = new MultiIdFunctionSearchResults.Item(capturePath);
//                int matchedExec = 0;
//                MatchedIdFunctionGroup matchedfuncs = new MatchedIdFunctionGroup();
//                matchedfuncs = CompareRdBack(PickDBObject.pickid, capturePath);
//                if (matchcount > 0)
//                {
//                    resultItem.AddSearchResult(matchcount, DBSettings.selectedId, matchedfuncs);
//                    matchedExec = PickDBObject.pickid.OID.Header.Executor_Code;
//                }
//                results.Add(resultItem);
//            }
//            return results;
//        }
//        public MultiIdFunctionSearchResults SearchForIDMatch(MultiIDPickComparer multiIdPickComparer, params string[] captureFiles)
//        {
//            // TODO: MultiSearch for match (untested)
//            MultiIdFunctionSearchResults results = new MultiIdFunctionSearchResults();
//            foreach (string capturePath in captureFiles)
//            {
//                MultiIdFunctionSearchResults.Item resultItem = new MultiIdFunctionSearchResults.Item(capturePath);
//                int matchedExec = 0;
//                foreach (PickIDComparer functionCompareHelper in multiIdPickComparer.pickcomparer)
//                {
//                    MatchedIdFunctionGroup matchedfuncs = new MatchedIdFunctionGroup();
//                    DBSettings.pickIDWorkingPath = functionCompareHelper.workingpath;
//                    DBSettings.selectedId = functionCompareHelper.strID;
//                    PickDBObject.id_cficollection = functionCompareHelper.cfifiles;
//                    matchedfuncs = CompareRdBack(functionCompareHelper.id, capturePath);
//                    if (matchcount > 0)
//                    {
//                        resultItem.AddSearchResult(matchcount, functionCompareHelper.strID, matchedfuncs);
//                        matchedExec = PickDBObject.pickid.OID.Header.Executor_Code;
//                    }
//                    else if (matchcount <= 0)
//                    {
//                        continue;
//                    }
//                }
//                results.Add(resultItem);
//            }
//            return results;
//        }
//    }
//    /// <summary>
//    /// Pick Db Object class to store the info of the selected Pick ID
//    /// </summary>
//    public class PickDBObject
//    {
//        #region Variables
//        private List<string> _pickKeyLabels;
//        public List<string> pickKeyLabels
//        {
//            get
//            {
//                return _pickKeyLabels;
//            }
//        }
//        private int idMaxVersion;
//        private IDictionary<string, string> pickIDReader;
//        private IList<IDictionary<string, string>> pickPrefixList;
//        private IList<IDictionary<string, string>> pickItemReader;
//        private static IPickReader pickreader;
//        public static string pickDBpath;
//        public static string selectedPickID;
//        public static PickID pickid;
//        public static ID id;
//        public static pickcfifiles id_cficollection;
//        #endregion
//        #region Methods
//        public PickDBObject(string pickdbpath, string dbname)
//        {
//            Environment.CurrentDirectory = Path.GetDirectoryName(Application.ExecutablePath);
//            PickDBObject.pickDBpath = pickdbpath;
//            PickDBObject.pickreader = new PickReader(PickDBObject.pickDBpath, dbname);
//        }
//        public PickDBObject()
//        {
//        }
//        public void LoadPickObjects(string selectedId)
//        {
//            try
//            {
//                PickDBObject.selectedPickID = selectedId;
//                idMaxVersion = pickreader.GetMaxVersion(PickDBObject.selectedPickID);
//                GetPickIDInfo();
//                FillPickIDInfo();
//                GetPickPrefixListInfo();
//                FillPrefixListInfo();
//                GetPickItemInfo();
//                FillPickItemInfo();
//                GetPickFunctionListAndHeaderInfo();
//            }
//            catch { throw new Exception(); }
//        }
//        private void FillPickIDInfo()
//        {
//            pickid = new PickID();
//            pickid.OID.Header = new IDHeader();
//            pickid.OID.Header.ID = PickDBObject.selectedPickID;
//            pickid.OID.Header.Executor_Code = int.Parse(pickIDReader[PickReader.PickIdFields.Executor_Code]);
//            pickid.OID.Header.IsExternalPrefix = pickIDReader[PickReader.PickIdFields.IsExternalPrefix];
//            pickid.OID.Header.IsInversedData = pickIDReader[PickReader.PickIdFields.IsInversedData];
//        }
//        private void FillPrefixListInfo()
//        {
//            pickid.OID.Header.PrefixList = new PrefixCollection();
//            foreach (IDictionary<string, string> dictionary in pickPrefixList)
//            {
//                string data = dictionary[PickReader.PrefixItemFields.DATA];
//                if (string.IsNullOrEmpty(data) == false)
//                {
//                    Prefix prefix = new Prefix();
//                    prefix.Data = data;
//                    pickid.OID.Header.PrefixList.Add(prefix);
//                }
//            }
//        }
//        private void FillPickItemInfo()
//        {
//            foreach (IDictionary<string, string> pickItemData in pickItemReader)
//            {
//                string outron = pickItemData[PickReader.PickItemFields.Outron];
//                if (string.IsNullOrEmpty(outron) == false && outron[0] == 'X' && char.IsDigit(outron[1]) && char.IsDigit(outron[2]))
//                {
//                    continue;
//                }
//                DataMap dataMap = new DataMap();
//                string data = pickItemData[PickReader.PickItemFields.DATA];
//                dataMap.Data = data;
//                dataMap.LabelList = new StringCollection();
//                dataMap.LabelList.Add(pickItemData[PickReader.PickItemFields.Label]);
//                dataMap.Outron = pickItemData[PickReader.PickItemFields.Outron];
//                dataMap.Intron = pickItemData[PickReader.PickItemFields.Intron];
//                pickid.DataMapList.Add(dataMap);
//            }
//        }
//        public IList<string> GetAllIDs()
//        {
//            if (PickDBObject.pickreader != null)
//            {
//                return PickDBObject.pickreader.GetAllIDList();
//            }
//            else
//            {
//                return null;
//            }
//        }
//        private void GetPickIDInfo()
//        {
//            pickIDReader = PickDBObject.pickreader.GetPickID(PickDBObject.selectedPickID, idMaxVersion);
//        }
//        private void GetPickItemInfo()
//        {
//            pickItemReader = PickDBObject.pickreader.GetPickItem(PickDBObject.selectedPickID, idMaxVersion);
//        }
//        private void GetPickPrefixListInfo()
//        {
//            pickPrefixList = PickDBObject.pickreader.GetPrefixItem(PickDBObject.selectedPickID, idMaxVersion);
//        }
//        private void GetPickFunctionListAndHeaderInfo()
//        {
//            id = new ID();
//            IDHeader header = new IDHeader();
//            id.FunctionList = new IDFunctionCollection();
//            IDFunction pkfunction;
//            _pickKeyLabels = new List<string>();
//            foreach (Dictionary<string, string> iddata in pickItemReader)
//            {
//                pkfunction = new IDFunction();
//                pkfunction.Data = iddata["DATA"];
//                pkfunction.Intron = iddata["Intron"];
//                pkfunction.Label = iddata["Label"];
//                //Get the non empty key labels to be populated in the UI
//                if ((pkfunction.Label != "") || (pkfunction.Label != string.Empty))
//                {
//                    _pickKeyLabels.Add(pkfunction.Label);
//                }
//                id.FunctionList.Add(pkfunction);
//            }
//            header.Executor_Code = Convert.ToInt16(pickIDReader["Executor_Code"]);
//            header.IsExternalPrefix = pickIDReader["IsExternalPrefix"];
//            header.IsFrequencyData = pickIDReader["IsFrequencyData"];
//            header.IsInversedPrefix = pickIDReader["IsInversedPrefix"];
//            header.IsRestricted = pickIDReader["IsRestricted"];
//            header.Text = pickIDReader["Text"];
//            id.Header = header;
//        }
//        #endregion
//    }

//}