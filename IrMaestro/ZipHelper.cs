using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Checksums;
using BusinessObject;

namespace IRMaestro
{
    public class ZipHelper
    {
        public static void CreateZipFromDirectory(string ZipArchive, string SourcePath)
        {
            CreateZipFromDirectory(ZipArchive, SourcePath, false);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ZipArchive">Full path(including name) of the zip file to be generated.</param>
        /// <param name="SourcePath">Folder containing the files to zip.</param>
        /// <param name="deleteDirectoryAfterZipping">Pass True or false to delete the source path after zipping.</param>
        public static void CreateZipFromDirectory(string ZipArchive, string SourcePath,bool deleteDirectoryAfterZipping)
        {
            StringCollection WorkFiles = new StringCollection();
            string[] FileList = Directory.GetFiles(SourcePath);
            foreach (string file in FileList)
                WorkFiles.Add(Path.GetFileName(file));

            CreateZipFile(ZipArchive, WorkFiles, SourcePath);


            if (deleteDirectoryAfterZipping)
            {
                try { Directory.Delete(SourcePath, true); }
                catch { }
            }
        }

        public static void CreateZipFile(string zipArchive, StringCollection workFiles, 
                                        string workPath)
        {
            if (string.IsNullOrEmpty(zipArchive))
                return;
            if (workFiles == null || workFiles.Count < 1)
                return;

            string filePath;

            if (!string.IsNullOrEmpty(workPath) && !workPath.EndsWith("\\"))
                workPath += "\\";

            Crc32 crc = new Crc32();
            ZipOutputStream s = new ZipOutputStream(File.Create(zipArchive));

            s.SetLevel(6); // 0 - store only to 9 - means best compression

            foreach (string file in workFiles)
            {
                if (string.IsNullOrEmpty(file))
                    continue;
                filePath = workPath + file;
                if (!File.Exists(filePath))
                    continue;
                FileStream fs = File.OpenRead(filePath);
                byte[] buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                ZipEntry entry = new ZipEntry(file);
                entry.DateTime = File.GetLastWriteTime(filePath);
                // set Size and the crc
                entry.Size = fs.Length;
                fs.Close();
                crc.Reset();
                crc.Update(buffer);
                entry.Crc = crc.Value;
                s.PutNextEntry(entry);
                s.Write(buffer, 0, buffer.Length);
            }

            s.Finish();
            s.Close();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="zipArchive"></param>
        /// <param name="filePath"></param>
        public static StringCollection ExtractAll(string zipArchive, string filePath)
        {
            StringCollection Files = new StringCollection();
            ZipInputStream zipInputStream =
                new ZipInputStream(File.OpenRead(zipArchive));

            ZipEntry entry = null;
            string curFileName = null;
            FileStream streamWriter = null;

            if (!filePath.EndsWith("\\"))
                filePath += "\\";

            while ((entry = zipInputStream.GetNextEntry()) != null)
            {
                Files.Add(entry.Name);
                curFileName = Path.GetFileName(entry.Name);
                streamWriter = File.Create(filePath + curFileName);
                StreamCopy(streamWriter, zipInputStream);
                streamWriter.Close();
                File.SetCreationTime(filePath + curFileName, entry.DateTime);
                File.SetLastWriteTime(filePath + curFileName, entry.DateTime);
            }
            return Files;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zipArchive"></param>
        /// <param name="filename"></param>
        public static void ExtractFile(string zipArchive, string fileName,
            string filePath)
        {
            ZipInputStream zipInputStream =
                new ZipInputStream(File.OpenRead(zipArchive));

            ZipEntry entry = null;
            string curFileName = null;
            while ((entry = zipInputStream.GetNextEntry()) != null)
            {
                curFileName = Path.GetFileName(entry.Name);
                if (fileName == curFileName)
                {
                    FileStream streamWriter =
                        File.Create(filePath + fileName);

                    StreamCopy(streamWriter, zipInputStream);
                    streamWriter.Close();
                    File.SetCreationTime(filePath + fileName, entry.DateTime);
                    File.SetLastWriteTime(filePath + fileName, entry.DateTime);
                }
            }
        }

        /// <summary>
        /// buffered write
        /// </summary>
        /// <param name="zipInputStream"></param>
        /// <param name="streamWriter"></param>
        private static void StreamCopy(Stream destStream,
            Stream srcStream)
        {
            int size = 2048;
            byte[] data = new byte[2048];
            while (true)
            {
                size = srcStream.Read(data, 0, data.Length);
                if (size > 0)
                {
                    destStream.Write(data, 0, size);
                }
                else
                {
                    break;
                }
            }
        }
    }
}
