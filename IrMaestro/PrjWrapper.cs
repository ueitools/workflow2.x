using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using CommonForms;
//using Row = System.Collections.Generic.IDictionary<string, string>;

namespace IRMaestro
{
    struct FileLabelOutron
    {
        public string file_name;
        public List<string> label_list;
        public string outron;
    }

    class PrjWrapper
    {
       
        private List<FileLabelOutron> FIFileLabelOutronList = 
            new List<FileLabelOutron>();
        private string ID;
        private string workpath;
        private string OutputZipFilePath;
        private string PrjFilePath;
        public void Init(string id, string work_path)
        {
            workpath = work_path;
            ID = id;
            OutputZipFilePath = workpath + ID + "FI.zip";
            PrjFilePath = workpath + ID + "fi.prj";

            // delete old files
            File.Delete(OutputZipFilePath);
            File.Delete(PrjFilePath);
            foreach (FileLabelOutron item in FIFileLabelOutronList)
                File.Delete(workpath+item.file_name);

            using (StreamWriter sw = File.AppendText(PrjFilePath))
            {
                sw.WriteLine("Id    :    {0}", ID);
                sw.Write(sw.NewLine);
                sw.Close();
            }
        }

        // Add FI filepath and label list into internal structure 
        // of PrjWrapper, also write the FI/label info into
        // the prj file
        public void Add(string filename, List<string> labellist, string outron)
        {
            FileLabelOutron entry;
            entry.file_name =  filename + ".fi";
            entry.label_list = labellist;
            entry.outron = outron;
            FIFileLabelOutronList.Add(entry);

            // write content into prj file
            using (StreamWriter sw = File.AppendText(PrjFilePath))
            {
                string lineToWrite = filename + ".FI :    ";
                foreach (string label in labellist)
                {
                    lineToWrite += String.Format("{0},", label);
                }
                lineToWrite = lineToWrite.Remove(lineToWrite.Length - 1, 1);
                lineToWrite += String.Format(" ({0})",outron);

                sw.WriteLine(lineToWrite);
                sw.Close();
            }
        }

        // Zip the FI files and prj file into a *.zip 
        public void Save()
        {
            Boolean flagError = false;
            if (FIFileLabelOutronList.Count == 0)
            {
                return;
            }
            
            List<string> file_list_zip = new List<string>();
            
            // add Prj file
            file_list_zip.Add(ID + "fi.prj");

            // add FI files
            foreach (FileLabelOutron item in FIFileLabelOutronList)
            {
                file_list_zip.Add(item.file_name);

                //Added By Satyadeep
                //PIC To FI Error Log File
                if(File.Exists(workpath+item.file_name))
                {
                    StreamReader readgeneratedfifile = new StreamReader(workpath + item.file_name);
                    String ficontent = readgeneratedfifile.ReadLine();
                    while(ficontent != null)
                    {
                        if(ficontent.Contains("No valid Exec was specified"))
                        {
                            String pictofierrorlogFile = workpath + "PickToFiError.log";
                            StreamWriter pictofierrorLogStreamWriter = null;
                            if(!File.Exists(pictofierrorlogFile))
                            {
                                pictofierrorLogStreamWriter = new StreamWriter(pictofierrorlogFile);
                                pictofierrorLogStreamWriter.WriteLine(DateTime.Now.ToString("MM/dd/yyy h:mm:ss.fff tt") + ">> |- =============================================================");
                                pictofierrorLogStreamWriter.WriteLine(DateTime.Now.ToString("MM/dd/yyy h:mm:ss.fff tt") + ">> |- == PickToFiError.Log");
                                pictofierrorLogStreamWriter.WriteLine(DateTime.Now.ToString("MM/dd/yyy h:mm:ss.fff tt") + ">> |- =============================================================");
                            }
                            else
                            {
                                pictofierrorLogStreamWriter = new StreamWriter(pictofierrorlogFile,true);
                            }
                            if(flagError == false)
                            {
                                pictofierrorLogStreamWriter.WriteLine(DateTime.Now.ToString("MM/dd/yyy h:mm:ss.fff tt") + ">> |- == "+ ID +" : " + ficontent);
                                flagError = true;
                            }
                            pictofierrorLogStreamWriter.Close();
                        }
                        ficontent = readgeneratedfifile.ReadLine();
                    }
                    readgeneratedfifile.Close();                    
                }
            }

            CommonForms.ZipHelper.CreateZipFile(OutputZipFilePath, file_list_zip,
                                        workpath);

            foreach (string file in file_list_zip)
                File.Delete(workpath+file);
        }
        public string GetWorkPath()
        {
            string workpath = CommonForms.Configuration.GetWorkingDirectory();
            if (!string.IsNullOrEmpty(workpath) && !workpath.EndsWith("\\"))
                workpath += "\\";

            return workpath;
        }
    }
}
