using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommonForms;

namespace IRMaestro
{
    public partial class SetOptionsForm : Form
    {
        private int devPulse = 6;
        private int devDelay = 6;
        private int devDelayPulse = 6;
        private int devFreq = 10;
        private int devCodeGap = 6;

        public SetOptionsForm()
        {
            InitializeComponent();
        }

        private void SetOptionsForm_Load(object sender, EventArgs e)
        {

            devFreq = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.FREQ_DEV, 10);
            devPulse = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.PULSE_DEV, 6);
            devDelay = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.DELAY_DEV, 6);
            devCodeGap = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CODEGAP_DEV, 6);
            devDelayPulse = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.DELAYPULSE_DEV, 6);
            MaskToggleBitsCheckBox.Checked = Convert.ToBoolean(UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.MASK_TOGGLE_BITS, 0));
            CompFrameNoCheckBox.Checked = Convert.ToBoolean(UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CMPFRAMENUM, 0));
            keepCfiCheckbox.Checked = Convert.ToBoolean(UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.KEEPCFI, 0));

            //devPulse = (int) ( CFILineInfo.PULSE_TOLERANCE * 100 );
            //devDelay = (int)(CFILineInfo.DELAY_TOLERANCE * 100);
            //devDelayPulse = (int)(CFILineInfo.PULSE_DELAY_TOLERANCE * 100);
            //devFreq = (int)(CFIInfo.FREQUENCY_TOLERANCE* 100);
            //devCodeGap = (int)(CFILineInfo.CODEGAP_TOLERANCE * 100);
            //MaskToggleBitsCheckBox.Checked = CFIInfo.MASK_TOGGLE_BITS;
            //CompFrameNoCheckBox.Checked = CFIInfo.FRAME_COUNT;

            CFIInfo.SET_TOLERANCE(devFreq, devPulse, devDelay,
               devCodeGap, devDelayPulse, CompFrameNoCheckBox.Checked, MaskToggleBitsCheckBox.Checked);

            FrequencyTextBox.Text = "" + devFreq;
            PulseDurationTextBox.Text = "" + devPulse;
            DelayDurationTextBox.Text = "" + devDelay;
            PulseDelayDurationTextBox.Text = "" + devDelayPulse;
            CodeGapTextBox.Text = "" + devCodeGap;
        }

        private void Cancelutton_Click(object sender, EventArgs e)
        {
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.FREQ_DEV, devFreq.ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.PULSE_DEV, devPulse.ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.DELAY_DEV, devDelay.ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CODEGAP_DEV, devCodeGap.ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.DELAYPULSE_DEV, devDelayPulse.ToString());

            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CMPFRAMENUM, Convert.ToInt16(CompFrameNoCheckBox.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.MASK_TOGGLE_BITS, Convert.ToInt16(MaskToggleBitsCheckBox.Checked).ToString());

            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CMPPULSECHKBOX, Convert.ToInt16(PulseDuration.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CMPDELAYCHKBOX, Convert.ToInt16(DelayDuration.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CMPFREQCHKBOX, Convert.ToInt16(Frequency.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CMPCODEGAP, Convert.ToInt16(CodeGap.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CMPDELAYPUSLE, Convert.ToInt16(PulseDelayDuration.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.KEEPCFI, Convert.ToInt16(keepCfiCheckbox.Checked).ToString());

            CFIInfo.SET_TOLERANCE(devFreq, devPulse, devDelay,
                devCodeGap, devDelayPulse, CompFrameNoCheckBox.Checked, MaskToggleBitsCheckBox.Checked);

            this.Close();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            //Saving to Registry
            //Configuration.SetConfigItem("CFIInfoTolarance", "devFreq", devFreq);
            //Configuration.SetConfigItem("CFIInfoTolarance", "devPulse", devPulse);
            //Configuration.SetConfigItem("CFIInfoTolarance", "devDelay", devDelay);
            //Configuration.SetConfigItem("CFIInfoTolarance", "devCodeGap", devCodeGap);
            //Configuration.SetConfigItem("CFIInfoTolarance", "devDelayPulse", devDelayPulse);
            //Configuration.SetConfigItem("CFIInfoTolarance", "bCompFrameNo", CompFrameNoCheckBox.Checked);
            //Configuration.SetConfigItem("CFIInfoTolarance", "bMaskToggleBits", MaskToggleBitsCheckBox.Checked);
            ////


            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.FREQ_DEV, devFreq.ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.PULSE_DEV, devPulse.ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.DELAY_DEV, devDelay.ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CODEGAP_DEV, devCodeGap.ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.DELAYPULSE_DEV, devDelayPulse.ToString());

            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CMPFRAMENUM, Convert.ToInt16(CompFrameNoCheckBox.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.MASK_TOGGLE_BITS, Convert.ToInt16(MaskToggleBitsCheckBox.Checked).ToString());

            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CMPPULSECHKBOX, Convert.ToInt16(PulseDuration.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CMPDELAYCHKBOX, Convert.ToInt16(DelayDuration.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CMPFREQCHKBOX, Convert.ToInt16(Frequency.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CMPCODEGAP, Convert.ToInt16(CodeGap.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CMPDELAYPUSLE, Convert.ToInt16(PulseDelayDuration.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.KEEPCFI, Convert.ToInt16(keepCfiCheckbox.Checked).ToString());

            CFIInfo.SET_TOLERANCE(devFreq, devPulse, devDelay,
                devCodeGap, devDelayPulse, CompFrameNoCheckBox.Checked, MaskToggleBitsCheckBox.Checked);

            this.Close();
        }

        #region Validating functions

        private void FrequencyTextBox_Validating(object sender,
            CancelEventArgs e)
        {
            if (!Frequency.Checked)
                return;

            string s = FrequencyTextBox.Text;
            s = s.Trim();

            int freq = 0;

            try
            {
                freq = int.Parse(s);
                if (freq > 100 || freq < 1)
                    throw new Exception();
                devFreq = freq;
            }
            catch
            {
                MessageBox.Show(
                    "Frequency dev should be a number bewteen 1 and 100");
                FrequencyTextBox.Text = "" + devFreq;
            }
        }

        private void PulseDurationTextBox_Validating(object sender,
            CancelEventArgs e)
        {
            if (!PulseDuration.Checked)
                return;

            string s = PulseDurationTextBox.Text;
            s = s.Trim();

            int pulse = 0;

            try
            {
                pulse = int.Parse(s);
                if (pulse > 100 || pulse < 1)
                    throw new Exception();
                devPulse = pulse;
            }
            catch
            {
                MessageBox.Show(
                    "Pulse duration dev should be a number bewteen 1 and 100");
                PulseDurationTextBox.Text = "" + devPulse;
            }
        }

        private void DelayDurationTextBox_Validating(object sender,
            CancelEventArgs e)
        {
            if (!DelayDuration.Checked)
                return;

            string s = DelayDurationTextBox.Text;
            s = s.Trim();

            int delay = 0;

            try
            {
                delay = int.Parse(s);
                if (delay > 100 || delay < 1)
                    throw new Exception();
                devDelay = delay;
            }
            catch
            {
                MessageBox.Show(
                    "Delay duration dev should be a number bewteen 1 and 100");
                DelayDurationTextBox.Text = "" + devDelay;
            }
        }

        private void PulseDelayDurationTextBox_Validating(object sender,
            CancelEventArgs e)
        {
            if (!PulseDelayDuration.Checked)
                return;

            string s = PulseDelayDurationTextBox.Text;
            s = s.Trim();

            int delayPulse = 0;

            try
            {
                delayPulse = int.Parse(s);
                if (delayPulse > 100 || delayPulse < 1)
                    throw new Exception();
                devDelayPulse = delayPulse;
            }
            catch
            {
                MessageBox.Show(
                    "Pulse/Delay duration dev should be a number bewteen 1 and 100");
                PulseDelayDurationTextBox.Text = "" + devDelayPulse;
            }
        }

        private void CodeGapTextBox_Validating(object sender,
            CancelEventArgs e)
        {
            if (!CodeGap.Checked)
                return;

            string s = CodeGapTextBox.Text;
            s = s.Trim();

            int codeGap = 0;

            try
            {
                codeGap = int.Parse(s);
                if (codeGap > 100 || codeGap < 1)
                    throw new Exception();
                devCodeGap = codeGap;
            }
            catch
            {
                MessageBox.Show(
                    "Code gap dev should be a number bewteen 1 and 100");
                CodeGapTextBox.Text = "" + devCodeGap;
            }
        }

        private void PulseDuration_CheckedChanged(object sender, EventArgs e)
        {
            if (PulseDuration.Checked)
                PulseDurationTextBox_Validating(null, null);
        }

        private void DelayDuration_CheckedChanged(object sender, EventArgs e)
        {
            if (DelayDuration.Checked)
                DelayDurationTextBox_Validating(null, null);
        }

        private void PulseDelayDuration_CheckedChanged(object sender,
            EventArgs e)
        {
            if (PulseDelayDuration.Checked)
                PulseDelayDurationTextBox_Validating(null, null);
        }

        private void Frequency_CheckedChanged(object sender, EventArgs e)
        {
            if (Frequency.Checked)
                FrequencyTextBox_Validating(null, null);
        }

        private void CodeGap_CheckedChanged(object sender, EventArgs e)
        {
            if (CodeGap.Checked)
                CodeGapTextBox_Validating(null, null);
        }

        #endregion    

    }
}