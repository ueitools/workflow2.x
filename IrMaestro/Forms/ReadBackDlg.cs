using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using IRMaestro.Lib;
using Microsoft.Win32;
using BusinessObject;
using CommonForms;
using IRMaestro.Forms;
//Start Add - PAL 
using PickAccessLayer;
using PickData;
using System.Collections;
//End Add - PAL

namespace IRMaestro
{
    public partial class ReadBackDlg : Form
    {
        public ReadBackDlg()
        {
            InitializeComponent();
			CFIInfo.MASK_TOGGLE_BITS = false;
            //XMLRadio.Checked = true;

            //Start Add - PAL 
            PickAccessLayer.Pick2DAO pickdb = new Pick2DAO();
            IList<Hashtable> ProjNames = null;

            CFIListForm.bShowTimingReport = true;

            ProjNames = PickAccessLayer.PickRetrieveFunctions.GetPickProjectNames();
            PickProjcomboBox.Items.Clear();
            foreach (Hashtable Projs in ProjNames)
            {
                PickProjcomboBox.Items.Add(Projs["Name"]);
            }
            if (PickProjcomboBox.Items != null)
                PickProjcomboBox.SelectedIndex = 0;
            //End Add - PAL 

            databaseRadiobutton.Checked = true;
        }

        private void ReadBackDlg_Shown(object sender, EventArgs e)
        {
            WorkDirText.Text = Configuration.GetWorkingDirectory();
            if (!Directory.Exists(WorkDirText.Text))
                Configuration.SetWorkingDirectory();

            string strPickType = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                        UserSettings.ConfigKeys.PICK_SOURCE);

            if (strPickType == "XML File")
            {
                xmlFileRadiobutton.Checked = true;
                xmlDirTextbox.Text = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                          UserSettings.ConfigKeys.PICK_DIR);
            }
            else if (strPickType == "Database")
            {
                databaseRadiobutton.Checked = true;
                string pickProject = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                         UserSettings.ConfigKeys.PICK_DIR);
                PickProjcomboBox.SelectedItem = pickProject;

            }
            ReadBackDirText.Text = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                        UserSettings.ConfigKeys.READBACK_DIR);
        }

        private void WorkDirBrBttn_Click(object sender, EventArgs e)
        {
            Configuration.SetWorkingDirectory();
            WorkDirText.Text = Configuration.GetWorkingDirectory();
        }

        private void ReadBackBrBttn_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.SelectedPath = ReadBackDirText.Text;

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                ReadBackDirText.Text = dlg.SelectedPath;
            }
        }

        private void OptBttn_Click(object sender, EventArgs e)
        {
            SetOptionsForm setOptionsForm = new SetOptionsForm();
            setOptionsForm.ShowDialog();
        }

        private void OkBttn_Click(object sender, EventArgs e)
        {
            //Sreedhar Added
            if (!Directory.Exists(WorkDirText.Text))
            {
                MessageBox.Show("Directory does not exists");
                return;
            }
             string _WORKINGDIR = "WorkingDirectory";
             string _UEITOOLSROOT = "HKEY_CURRENT_USER\\Software\\UEITools\\";
             Registry.SetValue(_UEITOOLSROOT, _WORKINGDIR,
                                 WorkDirText.Text, RegistryValueKind.String);
            //Sreedhar Added
            
            if (String.IsNullOrEmpty(WorkDirText.Text))
            {
                MessageBox.Show("Please enter the working directory");
                return;
            }
            if (File.Exists(WorkDirText.Text + "\\results.rpt"))
            {
                MergeResultsForm mergeForm = new MergeResultsForm();
                DialogResult dlgRes = mergeForm.ShowDialog();

                if (dlgRes == DialogResult.Yes)
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                              UserSettings.ConfigKeys.MERGE_REPORT,
                                              "1");
                   
                }
                else
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                              UserSettings.ConfigKeys.MERGE_REPORT,
                                              "0");
                   UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                              UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                              "0");
                    return;
                }  
            }
            else
            {
                UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                          UserSettings.ConfigKeys.MERGE_REPORT,
                                          "0");
               UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                          UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                          "0");
            }
            
            if (String.IsNullOrEmpty(ReadBackDirText.Text))
            {
                MessageBox.Show("Please enter the readback directory");
                return;
            }
            else
            {
                UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                              UserSettings.ConfigKeys.READBACK_DIR,
                                              ReadBackDirText.Text);
            }


            if (databaseRadiobutton.Checked)
            {
                UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                            UserSettings.ConfigKeys.PICK_SOURCE,
                                            databaseRadiobutton.Text);

                if (String.IsNullOrEmpty((string)PickProjcomboBox.SelectedItem))
                {
                    MessageBox.Show("Please Select a project");
                    return;
                }
                else
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                  UserSettings.ConfigKeys.PICK_DIR,
                                                  (string)PickProjcomboBox.SelectedItem);
                }

            }
            else if (xmlFileRadiobutton.Checked)
            {
                UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                             UserSettings.ConfigKeys.PICK_SOURCE,
                                             xmlFileRadiobutton.Text);

                if (String.IsNullOrEmpty(xmlDirTextbox.Text))
                {
                    MessageBox.Show("Please enter valid xml directory");
                    return;
                }
                else
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                  UserSettings.ConfigKeys.PICK_DIR,
                                                  xmlDirTextbox.Text);
                }
            }


            IDForm idForm;


            if (PickProjcomboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a product");
                return;
            }

            if (databaseRadiobutton.Checked)
            {
                idForm = new IDForm(ReadBackDirText.Text, WorkDirText.Text, IRMaestro.Lib.Readback.PickSource.StaticDB, (string)PickProjcomboBox.SelectedItem);
                idForm.ShowDialog();
            }
            else if(xmlFileRadiobutton.Checked)
            {
                idForm = new IDForm(xmlDirTextbox.Text, ReadBackDirText.Text, WorkDirText.Text,
                                   xmlFileRadiobutton.Checked ? Readback.PickSource.StaticXML : Readback.PickSource.StaticDB);
                idForm.ShowDialog();
            }
           
        }

        private void CancelBttn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ReadBackDlg_FormClosing(object sender, 
            FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void WorkDirText_Validating(object sender, CancelEventArgs e)
        {
            if (WorkDirText.Text == null || WorkDirText.Text.Trim() == "")
            {
                WorkDirText.Text = "";
                return;
            }

            WorkDirText.Text = WorkDirText.Text.Trim();

            if ( !Directory.Exists(WorkDirText.Text))
            {
                MessageBox.Show("Directory " + WorkDirText.Text
                    + " does not exist");
                WorkDirText.Text = "";
                return;
            }
        }


        private void ReadBackDirText_Validating(object sender,
            CancelEventArgs e)
        {
            if (ReadBackDirText.Text == null 
                || ReadBackDirText.Text.Trim() == "")
            {
                ReadBackDirText.Text = "";
                return;
            }

            ReadBackDirText.Text = ReadBackDirText.Text.Trim();

            if (!Directory.Exists(ReadBackDirText.Text))
            {
                MessageBox.Show("Directory " + ReadBackDirText.Text
                    + " does not exist");
                ReadBackDirText.Text = "";
                return;
            }
        }

        private void xmlFileRadiobutton_CheckedChanged(object sender, EventArgs e)
        {
            //if (databaseRadiobutton.Checked)
            //{
            //    PickProjcomboBox.Enabled = true;
            //    xmlDirTextbox.Enabled = false;
            //    pickxmlBrwBtn.Enabled = false;

            //}
            //else if (xmlFileRadiobutton.Checked)
            //{
            //    PickProjcomboBox.Enabled = false;
            //    xmlDirTextbox.Enabled = true;
            //    pickxmlBrwBtn.Enabled = true;
            //}
        }

        private void databaseRadiobutton_CheckedChanged(object sender, EventArgs e)
        {
            if (databaseRadiobutton.Checked)
            {
                PickProjcomboBox.Enabled = true;
                xmlDirTextbox.Enabled = false;
                pickxmlBrwBtn.Enabled = false;

            }
            else if (xmlFileRadiobutton.Checked)
            {
                PickProjcomboBox.Enabled = false;
                xmlDirTextbox.Enabled = true;
                pickxmlBrwBtn.Enabled = true;
            }

        }

        private void pickxmlBrwBtn_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.SelectedPath = xmlDirTextbox.Text;

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                xmlDirTextbox.Text = dlg.SelectedPath;
            }

        }

    }
}