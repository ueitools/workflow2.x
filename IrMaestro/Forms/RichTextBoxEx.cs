using System;
using System.Windows.Forms;

namespace IRMaestro.Forms
{
    public partial class RichTextBoxEx : UserControl
    {
        public RichTextBoxEx()
        {
            InitializeComponent();
        }

        public override string Text
        {
            get { return rtbMain.Text; }
            set { rtbMain.Text = value; }
        }

        public string[] Lines
        {
            get { return rtbMain.Lines; }
            set { rtbMain.Lines = value; }
        }

        public bool ReadOnly
        {
            get { return rtbMain.ReadOnly; }
            set { rtbMain.ReadOnly = value; }
        }

        public BorderStyle BorderStyle
        {
            get { return base.BorderStyle; }
            set { base.BorderStyle = value; }
        }
    }
}
