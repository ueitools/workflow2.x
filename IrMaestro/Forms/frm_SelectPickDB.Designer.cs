namespace IRMaestro.Forms
{
    partial class frm_SelectPickDB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBrowsePickDB = new System.Windows.Forms.Button();
            this.txtSelectedPickDB = new System.Windows.Forms.TextBox();
            this.lblSelectedPickDB = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnBrowsePickDB
            // 
            this.btnBrowsePickDB.Location = new System.Drawing.Point(326, 12);
            this.btnBrowsePickDB.Name = "btnBrowsePickDB";
            this.btnBrowsePickDB.Size = new System.Drawing.Size(75, 23);
            this.btnBrowsePickDB.TabIndex = 0;
            this.btnBrowsePickDB.Text = "Browse...";
            this.btnBrowsePickDB.UseVisualStyleBackColor = true;
            this.btnBrowsePickDB.Click += new System.EventHandler(this.btnBrowsePickDB_Click);
            // 
            // txtSelectedPickDB
            // 
            this.txtSelectedPickDB.Location = new System.Drawing.Point(78, 15);
            this.txtSelectedPickDB.Name = "txtSelectedPickDB";
            this.txtSelectedPickDB.Size = new System.Drawing.Size(242, 20);
            this.txtSelectedPickDB.TabIndex = 1;
            // 
            // lblSelectedPickDB
            // 
            this.lblSelectedPickDB.AutoSize = true;
            this.lblSelectedPickDB.Location = new System.Drawing.Point(12, 22);
            this.lblSelectedPickDB.Name = "lblSelectedPickDB";
            this.lblSelectedPickDB.Size = new System.Drawing.Size(46, 13);
            this.lblSelectedPickDB.TabIndex = 2;
            this.lblSelectedPickDB.Text = "Pick DB";
            // 
            // frm_SelectPickDB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 65);
            this.Controls.Add(this.lblSelectedPickDB);
            this.Controls.Add(this.txtSelectedPickDB);
            this.Controls.Add(this.btnBrowsePickDB);
            this.Name = "frm_SelectPickDB";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select PickDB";
            this.Load += new System.EventHandler(this.frm_SelectPickDB_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBrowsePickDB;
        private System.Windows.Forms.TextBox txtSelectedPickDB;
        private System.Windows.Forms.Label lblSelectedPickDB;
    }
}