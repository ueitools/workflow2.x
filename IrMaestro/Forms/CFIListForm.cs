using System;
using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;

namespace IRMaestro
{
    public partial class CFIListForm : Form
    {
        private CompareRes res;
        private string workDir;
        private string id;

        public static bool bShowTimingReport = false;

        public CFIListForm(string workDir, string ID)
        {
            InitializeComponent();

            this.workDir = workDir;
            this.id = ID;
            res = new CompareRes();
        }

        public CFIListForm(CompareRes res)
        {
            InitializeComponent();
            workDir = null;
            this.res = res;
        }

        private void CFIListForm_Shown(object sender, EventArgs e)
        {
            string compFileName;
            string rbkFile;

            if (res.cfiFileList == null)
            {
                if (workDir == null)
                    return;

                //string id = Path.GetFileName(workDir);
                compFileName = "RB!" + id + "_"
                                + id + ".RBK";
               
                //Start Add to show report Timing inspector
                if (bShowTimingReport == true)
                {
                    compFileName = "RB!" + id + "_"
                                    + id + ".FBK";
                }
                //End Add to show report as RBK format for Timing inspector

                res.ID = id;

                if (!workDir.EndsWith("\\"))
                    workDir += "\\";

                //DirectoryInfo di = new DirectoryInfo(workDir);
                //DirectoryInfo parentDir = di.Parent;
                //string rbkPath = parentDir.FullName;
                //if (!rbkPath.EndsWith("\\"))
                //    rbkPath += "\\";
                rbkFile = workDir + compFileName;

                if (!File.Exists(rbkFile))
                    return;

                string text = File.ReadAllText(rbkFile);
                CompareTextBox.Text = text;

                //Start Add to show report as RBK format for Timing inspector
                string[] fileList = null;

                if (!Directory.Exists(workDir + id))
                {                    
                    CFIListBox.Text = "CFI file(s) doesnot exists for ID " + id +
                        "\r\nCFI files might have deleted in the last run";
                    return;
                }
                
                if (bShowTimingReport == true)
                {
                     fileList = Directory.GetFiles(workDir + id,
                        "*_*#.FI", SearchOption.TopDirectoryOnly);
                }
                else //End Add to show report as RBK format for Timing inspector
                {
                     fileList = Directory.GetFiles(workDir + id,
                    "*_*#.CFI", SearchOption.TopDirectoryOnly);
                }

                if (fileList == null)
                    return;

                res.cfiFileList = new List<string>();

                foreach (string file in fileList)
                {
                    CFIListBox.Items.Add(Path.GetFileName(file));
                    res.cfiFileList.Add(file);
                }
            }
            else
            {
                compFileName = "RB!" + res.ID + "_"
                                + res.ID + ".RBK";

                rbkFile = res.cfiFileList[0].Replace(
                    Path.GetFileName(res.cfiFileList[0]), compFileName);

                string text = File.ReadAllText(rbkFile);
                CompareTextBox.Text = text;

                foreach (string file in res.cfiFileList)
                {
                    CFIListBox.Items.Add(Path.GetFileName(file));
                }
            }
        }

        private void CFIListBox_SelectedIndexChanged(object sender, 
            EventArgs e)
        {
            if (CFIListBox.SelectedItem == null)
                return;

            if (res.cfiFileList == null || res.cfiFileList.Count == 0)
                return;

            string path = res.cfiFileList[0].Replace(
                Path.GetFileName(res.cfiFileList[0]), "");

            string readBackCFI = path + (string)CFIListBox.SelectedItem;
            string pickCFI = readBackCFI.Replace("#", "");

            if(!File.Exists(readBackCFI))
            {
                MessageBox.Show("Cannot find file " + readBackCFI + " in ReadBack");
                return;
            }
            if (!File.Exists(pickCFI))
            {
                MessageBox.Show("Cannot find file " + pickCFI + " in PickDB");
                return;
            }

            CFICompareForm cfiCompareForm = new CFICompareForm(readBackCFI,
                pickCFI);

            cfiCompareForm.ShowDialog();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void printRptTSButton_Click(object sender, EventArgs e)
        {
            CompareReportForm.PrintReport(CompareTextBox.Text);
        }

        private void PageSetupTSButton_Click(object sender, EventArgs e)
        {
            CompareReportForm.PageSetup();
        }
    }
}