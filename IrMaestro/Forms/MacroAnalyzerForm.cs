//********************************************************************************
//REVISION HISTORY
//--------------------------------------------------------------------------------
//03/24/2010
//---------------------------------------------------------------------------------
//- Added OnSettings event handler to support the settings button.
//- Added ResetStatusBar method that could display the DB type selected.
//- Added ClearCaptureStations method that could clear the Capture pod combo box
//  when switched between different db's
//*********************************************************************************

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace IRMaestro {
    public partial class MacroAnalyzerForm : Form, IMacroAnalyzerView {
        private const string FORM_TITLE = "Macro Analyzer";
        
        public MacroAnalyzerForm() {
            InitializeComponent();

            InitializeForm();
        }

        private void InitializeForm() {
            Text = FORM_TITLE;
        }

        public event EventHandler OnSelectIds;
        public event EventHandler OnResetIds;
        public event EventHandler OnIdListChanged;
        public event EventHandler OnStartCapture;
        public event EventHandler OnUsb1MHzCaptureModeChanged;
        public event CapturePodChangedDelegate OnCapturePodChanged;
        public event FormClosingEventHandler OnFormClose;
        public event EventHandler OnSettings;

        private void RaiseIdListChanged() {
            if (OnIdListChanged != null) {
                OnIdListChanged(this, EventArgs.Empty);
            }
        }

        public bool Usb1MHzCaptureMode {
            get { return Use1MHzCheckBox.Checked; }
            set { Use1MHzCheckBox.Checked = value; }
        }

        public string GetIdListText() {
            return IdListTextBox.Text;
        }

        public void SetCaptureButtonEnabled(bool enable) {
            CaptureButton.Enabled = enable;
        }

        public void ResetStatusbar()
        {
            string db = DBSettings.GetDBType(DBSettings.macroregpath, "SelectedDBType");
            DBType dbtype = DBType.UEIDB;

            if (db == "UEIDB")
                dbtype = DBType.UEIDB;
            else
                dbtype = DBType.PickDB;

            switch (dbtype)
            {
                case DBType.UEIDB:
                    {
                        toolStripDBType.Text = "UEI DB Selected";
                        toolStripPickDBPath.Text = "";
                        break;
                    }
                case DBType.PickDB:
                    {
                        toolStripDBType.Text = "Selected Pick Project";
                        toolStripPickDBPath.Text = ": " + DBSettings.GetDBPath(DBSettings.macroregpath, "SelectedPickProject");
                        break;
                    }
            }
            
        }

        public void SetIdListText(string listText) {
            IdListTextBox.Text = listText;
            IdListTextBox.SelectAll();
            IndicateSelectionMade();
        }

        public void EnableUsbCapture1MHz(bool enable)
        {
            Use1MHzCheckBox.Enabled = enable;
        }

        public void EnableCapturePodSelection(bool enable)
        {
            CapturePodComboBox.Enabled = enable;
        }

        public void ClearCaptureStations()
        {
            CapturePodComboBox.Items.Clear();
        }

        public void ClearIDSelection()
        {
            SearchResultsTreeView.Nodes.Clear();
            IdListTextBox.Text = "";
            Text = FORM_TITLE + " [" + IdListTextBox.Text + "]";
        }
        private void IndicateSelectionMade() {
            if (IdListTextBox.Text != string.Empty) {
                Text = FORM_TITLE + " [" + IdListTextBox.Text + "]";
            }
        }

        public void Show1MHzCapture(bool show) {
            Use1MHzCheckBox.Visible = show;
        }

        public void SetAvailableCaptureStations(IList<string> availableCapturePodList)
        {
            foreach (string avaliableCapturePod in availableCapturePodList) {
                CapturePodComboBox.Items.Add(avaliableCapturePod);
            }
        }

        public void SetCurrentCapturePod(string capturePod)
        {
            CapturePodComboBox.SelectedItem = capturePod;
            DBSettings.PodType = capturePod;
        }

        private void CapturePodComboBox_SelectedIndexChanged(object sender, EventArgs e) {
            string selectedItem = CapturePodComboBox.SelectedItem.ToString();
            if (OnCapturePodChanged != null) {
                OnCapturePodChanged(selectedItem);
            }
        }

        public void SetResultData(MvpDataContainers.HierarchicalData resultData) {
            SearchResultsTreeView.Nodes.Clear();

            if (resultData != null) {
                foreach (MvpDataContainers.HierarchicalData.Node resultNode in resultData) {
                    TreeNode treeNode = new TreeNode(resultNode.Label);
                    SearchResultsTreeView.Nodes.Add(treeNode);
                    AddNodes(resultNode.Children, treeNode);
                }
            }
            SearchResultsTreeView.ExpandAll();
        }

        private void AddNodes(IEnumerable<MvpDataContainers.HierarchicalData.Node> resultNode, TreeNode parent) {
            if (resultNode == null) {
                return;
            }

            foreach (MvpDataContainers.HierarchicalData.Node child in resultNode) {
                TreeNode treeNode = new TreeNode(child.Label);
                parent.Nodes.Add(treeNode);
                AddNodes(child.Children, treeNode);
            }
        }

        private void IdListTextBox_Leave(object sender, EventArgs e) {
            RaiseIdListChanged();
        }

        private void SelectIdButton_Click(object sender, EventArgs e) {
            if (OnSelectIds != null) {
                OnSelectIds(this, EventArgs.Empty);
            }
        }

        private void CaptureButton_Click(object sender, EventArgs e) {
            if (OnStartCapture != null) {
                OnStartCapture(this, EventArgs.Empty);
            }
        }

        private void IdListTextBox_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                RaiseIdListChanged();
            }
        }

        private void Use1MHzCheckBox_CheckedChanged(object sender, EventArgs e) {
            if (OnUsb1MHzCaptureModeChanged != null) {
                OnUsb1MHzCaptureModeChanged(this, EventArgs.Empty);
            }
        }

        private void SetButton_Click(object sender, EventArgs e) {
            RaiseIdListChanged();
        }

        private void ResetButton_Click(object sender, EventArgs e) {
            if (OnResetIds != null) {
                OnResetIds(this, EventArgs.Empty);
            }
        }

        private void CaptureSettingsButton_Click(object sender, EventArgs e)
        {
            if (OnSettings != null)
            {
                OnSettings(this, EventArgs.Empty);
            }
            
        }

               

        private void MacroAnalyzerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            OnFormClose(this, null);
        }

        



        
    }
}