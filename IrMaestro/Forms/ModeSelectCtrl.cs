using System;
using System.Windows.Forms;

namespace CommonUI {
    public partial class ModeSelectCtrl : UserControl {
        private string _modeList;

        public event EventHandler ModeSelectionChanged;

        public string ModeList {
            get { return _modeList; }
            set {
                _modeList = value;
                foreach (Control item in Controls) {
                    if ((string)item.Tag == "All") {
                        continue;
                    }
                    CheckBox box = (CheckBox)item;
                    CheckNoNotify(box, _modeList.Contains((string)box.Tag));
                }
                CheckNoNotify(cb_All, AllChecked());
                RaiseModeSelectionChanged();
            }
        }

        public ModeSelectCtrl() {
            InitializeComponent();
        }

        private void ModeSelectCtrl_Load(object sender, EventArgs e) {
            _modeList = "";
            cb_All.Checked = false;
        }

        private void CheckNoNotify(CheckBox checkBox, bool check) {
            checkBox.CheckedChanged -= cb_CheckChanged;
            checkBox.Checked = check;
            checkBox.CheckedChanged += cb_CheckChanged;
        }

        private void cb_CheckChanged(object sender, EventArgs e) {
            CheckBox checkBox = (CheckBox)sender;

            if ((string)checkBox.Tag != "All") {
                CheckNoNotify(cb_All, AllChecked());
            } else {
                SetAll(checkBox.Checked);
            }

            RaiseModeSelectionChanged();
        }

        private void RaiseModeSelectionChanged() {
            if (ModeSelectionChanged != null) {
                ModeSelectionChanged(this, EventArgs.Empty);
            }
        }

        private bool AllChecked() {
            bool result = true;
            _modeList = "";
            foreach (Control item in Controls) {
                if ((string)item.Tag == "All") {
                    continue;
                }
                CheckBox box = (CheckBox)item;
                if (!box.Checked) {
                    result = false;
                } else {
                    _modeList += (string)box.Tag;
                }
            }
            return result;
        }

        private void SetAll(bool state) {
            _modeList = "";
            foreach (Control item in Controls) {
                CheckBox cb = (CheckBox)item;
                CheckNoNotify(cb, state);
                if (!cb.Tag.Equals("All")) {
                    _modeList += (string)cb.Tag;
                }
            }
        }
    }
}