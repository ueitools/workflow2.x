using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using Microsoft.Win32;
using CommonForms.Forms;

namespace IRMaestro.Forms
{
    public partial class ProjectNameForm : Form
    {
        const string _UEITOOLSROOT =
        "HKEY_CURRENT_USER\\Software\\UEITools\\";
        const string _PICKDBDIR = "IRMaestroPickDBDirectory";
        const string _DEFAULT_PICKDBDIR = "";
        public const string _PROJ_ROOT =
            "HKEY_CURRENT_USER\\Software\\UEITools\\IRMaestro";
        const string _SELECTEDDB = "IRMaestroSelectedDB";
        string name;
        bool internalPick;
        private string DBString = DBConnectionString.LOCALPRODUCT;

        public ProjectNameForm()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (idSelectionControl1.ValidateAndGetData() == false)
            {
                return;
            }
            name = productComboBox.Text;
            internalPick = internalCheckBox.Checked;
            Registry.SetValue(_UEITOOLSROOT, _PICKDBDIR,
                    pathTextBox.Text, RegistryValueKind.String);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        public string GetName()
        {
            return name;
        }

        public string GetPath()
        {
            return (string)Registry.GetValue(
                _UEITOOLSROOT, _PICKDBDIR, _DEFAULT_PICKDBDIR);
        }

        public bool GetInternalPick()
        {
            return internalPick;
        }

        public string GetDBString()
        {
            return DBString;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        public List<string> GetSelectedIDList(IList<string> allIDList)
        {
            return idSelectionControl1.GetSelectedIDList(allIDList);
        }

        private void ProjectNameForm_Load(object sender, EventArgs e)
        {
            pathTextBox.Text = GetPath();

            dbSelectorComboBox.Init(new DBSelectorComboBoxSettings(),
                _PROJ_ROOT, _SELECTEDDB, UpdateProjectList);
        }

        private void UpdateProjectList(DBSelectorComboBoxSettings.DB db)
        {
            switch (db)
            {
                case DBSelectorComboBoxSettings.DB.Working:
                    DBString = DBConnectionString.WORKPRODUCT;
                    break;
                case DBSelectorComboBoxSettings.DB.Public:
                    DBString = DBConnectionString.PRODUCT;
                    break;
                case DBSelectorComboBoxSettings.DB.Local:
                    DBString = DBConnectionString.LOCALPRODUCT;
                    break;
                default:
                    throw new Exception("Unhandled database");
            }
            DAOFactory.ResetDBConnection(DBString);

            ProjectHeaderCollection projectList;

            try
            {
                productComboBox.Items.Clear();

                projectList = ProductDBFunctions.GetAllProjects();

                foreach (ProjectHeader project in projectList)
                {
                    productComboBox.Items.Add(project.Name);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();

            dlg.SelectedPath = pathTextBox.Text;
            DialogResult res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                pathTextBox.Text = dlg.SelectedPath;
            }
        }
    }
}