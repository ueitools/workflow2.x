using System;
using System.Collections.Generic;
using System.Windows.Forms;
using CommonForms;

namespace IRMaestro {
    public partial class DecodeIrSetupForm : Form {
        private const int GENERIC_EXEC = 998;

        private abstract class RegistryKeys {
            public const string SubKey = "DecodeIr";
//            public const string FilePath = "FilePath";
            public const string Executor = "Exec";
            public const string GenericPath = "GenericPath";
            public const string IdentifyFiles = "IdentifyFiles";
            public const string KeepCfiFiles = "KeepCfiFiles";
        }

        private string _genericFilePath;
        private bool _processDropDownClosedEvent;

        public DecodeIrSetupForm() {
            InitializeComponent();

            InitializeForm();

            _genericFilePath = string.Empty;
        }

        public string FilePath {
            get { return SourceFileInput.FilePath; }
        }

        public int Executor {
            get {
                if (UseGenericExecCheckBox.Checked) {
                    return GENERIC_EXEC;
                }

                int executor = -1;
                if (ExecutorComboList.SelectedItem != null) {
                    executor = (int)ExecutorComboList.SelectedItem;
                }
                return executor;
            }
        }

        public string GenericFilePath {
            get { return _genericFilePath;  }
            set {
                _genericFilePath = value;
                UseGenericExecCheckBox.Checked = string.IsNullOrEmpty(_genericFilePath) == false;
            }
        }

        public bool IdentifyFiles {
            get { return IdFilesCheckBox.Checked; }
        }

        public bool KeepCfiFiles {
            get { return KeepCfiFilesCheckBox.Checked; }
        }

        private void InitializeForm() {
            ExecutorComboList.MouseWheel += ExecutorComboList_MouseWheel;

            Text = "Decode IR";
            SourceFileInput.Filter = "Capture Set (*.zip)|*.zip|Capture File (*.u1;*.u2)|*.u1;*.u2|All Files (*.*)|*.*";

            ExecutorComboList.Items.Clear();
            List<int> executorList = FunctionList.GetAllExecutorList();
            foreach (int executor in executorList) {
                ExecutorComboList.Items.Add(executor);                
            }
            
//            SourceFileInput.FilePath = Configuration.GetConfigItem(RegistryKeys.SubKey, RegistryKeys.FilePath,
//                                                                   Configuration.GetWorkingDirectory());
            ExecutorComboList.SelectedItem = Configuration.GetConfigItem(RegistryKeys.SubKey, RegistryKeys.Executor, -1);
            GenericFilePath = Configuration.GetConfigItem(RegistryKeys.SubKey, RegistryKeys.Executor);
            IdFilesCheckBox.Checked = Configuration.GetConfigItem(RegistryKeys.SubKey, RegistryKeys.IdentifyFiles, false);
            KeepCfiFilesCheckBox.Checked = Configuration.GetConfigItem(RegistryKeys.SubKey, RegistryKeys.KeepCfiFiles, false);
        }

        private void UseGenericExecCheckBox_CheckedChanged(object sender, System.EventArgs e) {
            if (!UseGenericExecCheckBox.Checked) {
                return;
            }

            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.InitialDirectory = Configuration.GetWorkingDirectory();
            fileDialog.Filter = "Generic Configurations (*.cfg)|*.cfg";
            if (fileDialog.ShowDialog(this) == DialogResult.OK) {
                _genericFilePath = fileDialog.FileName;
            }
        }

        private void OkButton_Click(object sender, System.EventArgs e) {
//            if (string.IsNullOrEmpty(SourceFileInput.FilePath) == false) {
//                Configuration.SetConfigItem(RegistryKeys.SubKey, RegistryKeys.FilePath, SourceFileInput.FilePath);
//            }
            if (ExecutorComboList.SelectedItem != null) {
                Configuration.SetConfigItem(RegistryKeys.SubKey, RegistryKeys.Executor, (int)ExecutorComboList.SelectedItem);
            }
            if (string.IsNullOrEmpty(_genericFilePath)) {
                Configuration.SetConfigItem(RegistryKeys.SubKey, RegistryKeys.GenericPath, _genericFilePath);
            }
            Configuration.SetConfigItem(RegistryKeys.SubKey, RegistryKeys.IdentifyFiles, IdFilesCheckBox.Checked);
            Configuration.SetConfigItem(RegistryKeys.SubKey, RegistryKeys.KeepCfiFiles, KeepCfiFilesCheckBox.Checked);
        }

        private void ExecutorComboList_DropDownClosed(object sender, System.EventArgs e) {
            if (_processDropDownClosedEvent == false)
                return;

            int index = ExecutorComboList.FindStringExact(ExecutorComboList.Text);
            if (index < 0) {
                ExecutorComboList.Text = string.Empty;
            }
            ExecutorComboList.SelectedIndex = index;

            _processDropDownClosedEvent = false;
        }

        private void ExecutorComboList_KeyPress(object sender, KeyPressEventArgs e) {
            ExecutorComboList.DroppedDown = true;
            _processDropDownClosedEvent = true;
        }

        private void ExecutorComboList_MouseWheel(object sender, MouseEventArgs e) {
            ExecutorComboList.DroppedDown = true;
        }
    }
}