namespace IRMaestro
{
    partial class DoRBComparisionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DoRBComparisionForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.PickProjComboBox = new System.Windows.Forms.ComboBox();
            this.rbdirBrwBtn = new System.Windows.Forms.Button();
            this.workdirBrwbtn = new System.Windows.Forms.Button();
            this.rbdirTextBox = new System.Windows.Forms.TextBox();
            this.workdirTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.OutronFileTextBox = new System.Windows.Forms.TextBox();
            this.OutronfileBrwBtn = new System.Windows.Forms.Button();
            this.setoptionsBtn = new System.Windows.Forms.Button();
            this.OKBtn = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.PickProjComboBox);
            this.groupBox1.Controls.Add(this.rbdirBrwBtn);
            this.groupBox1.Controls.Add(this.workdirBrwbtn);
            this.groupBox1.Controls.Add(this.rbdirTextBox);
            this.groupBox1.Controls.Add(this.workdirTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(16, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(548, 142);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Directory Setting";
            // 
            // PickProjComboBox
            // 
            this.PickProjComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.PickProjComboBox.FormattingEnabled = true;
            this.PickProjComboBox.Location = new System.Drawing.Point(163, 59);
            this.PickProjComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PickProjComboBox.Name = "PickProjComboBox";
            this.PickProjComboBox.Size = new System.Drawing.Size(324, 24);
            this.PickProjComboBox.TabIndex = 6;
            // 
            // rbdirBrwBtn
            // 
            this.rbdirBrwBtn.Image = ((System.Drawing.Image)(resources.GetObject("rbdirBrwBtn.Image")));
            this.rbdirBrwBtn.Location = new System.Drawing.Point(496, 95);
            this.rbdirBrwBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbdirBrwBtn.Name = "rbdirBrwBtn";
            this.rbdirBrwBtn.Size = new System.Drawing.Size(33, 30);
            this.rbdirBrwBtn.TabIndex = 5;
            this.rbdirBrwBtn.UseVisualStyleBackColor = true;
            this.rbdirBrwBtn.Click += new System.EventHandler(this.rbdirBrwBtn_Click);
            // 
            // workdirBrwbtn
            // 
            this.workdirBrwbtn.Image = ((System.Drawing.Image)(resources.GetObject("workdirBrwbtn.Image")));
            this.workdirBrwbtn.Location = new System.Drawing.Point(496, 25);
            this.workdirBrwbtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.workdirBrwbtn.Name = "workdirBrwbtn";
            this.workdirBrwbtn.Size = new System.Drawing.Size(33, 30);
            this.workdirBrwbtn.TabIndex = 1;
            this.workdirBrwbtn.UseVisualStyleBackColor = true;
            this.workdirBrwbtn.Click += new System.EventHandler(this.workdirBrwbtn_Click);
            // 
            // rbdirTextBox
            // 
            this.rbdirTextBox.Location = new System.Drawing.Point(163, 97);
            this.rbdirTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbdirTextBox.Name = "rbdirTextBox";
            this.rbdirTextBox.Size = new System.Drawing.Size(324, 22);
            this.rbdirTextBox.TabIndex = 4;
            // 
            // workdirTextBox
            // 
            this.workdirTextBox.Location = new System.Drawing.Point(163, 27);
            this.workdirTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.workdirTextBox.Name = "workdirTextBox";
            this.workdirTextBox.Size = new System.Drawing.Size(324, 22);
            this.workdirTextBox.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 101);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "ReadBack Directory:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "PickDB Directory:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Working Directory:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButton2);
            this.groupBox2.Controls.Add(this.radioButton1);
            this.groupBox2.Location = new System.Drawing.Point(16, 164);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(431, 82);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ReadBack Summary Option";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(11, 49);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(161, 21);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "Cover All ReadBacks";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(11, 23);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(251, 21);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Only Cover Unmatched ReadBacks";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 262);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Outron File:";
            // 
            // OutronFileTextBox
            // 
            this.OutronFileTextBox.Enabled = false;
            this.OutronFileTextBox.Location = new System.Drawing.Point(127, 258);
            this.OutronFileTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OutronFileTextBox.Name = "OutronFileTextBox";
            this.OutronFileTextBox.Size = new System.Drawing.Size(219, 22);
            this.OutronFileTextBox.TabIndex = 0;
            // 
            // OutronfileBrwBtn
            // 
            this.OutronfileBrwBtn.Enabled = false;
            this.OutronfileBrwBtn.Image = ((System.Drawing.Image)(resources.GetObject("OutronfileBrwBtn.Image")));
            this.OutronfileBrwBtn.Location = new System.Drawing.Point(355, 255);
            this.OutronfileBrwBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OutronfileBrwBtn.Name = "OutronfileBrwBtn";
            this.OutronfileBrwBtn.Size = new System.Drawing.Size(33, 30);
            this.OutronfileBrwBtn.TabIndex = 1;
            this.OutronfileBrwBtn.UseVisualStyleBackColor = true;
            this.OutronfileBrwBtn.Click += new System.EventHandler(this.OutronfileBrwBtn_Click);
            // 
            // setoptionsBtn
            // 
            this.setoptionsBtn.Location = new System.Drawing.Point(41, 295);
            this.setoptionsBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.setoptionsBtn.Name = "setoptionsBtn";
            this.setoptionsBtn.Size = new System.Drawing.Size(115, 28);
            this.setoptionsBtn.TabIndex = 2;
            this.setoptionsBtn.Text = "Setup Option";
            this.setoptionsBtn.UseVisualStyleBackColor = true;
            this.setoptionsBtn.Click += new System.EventHandler(this.setoptionsBtn_Click);
            // 
            // OKBtn
            // 
            this.OKBtn.Location = new System.Drawing.Point(179, 295);
            this.OKBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OKBtn.Name = "OKBtn";
            this.OKBtn.Size = new System.Drawing.Size(92, 28);
            this.OKBtn.TabIndex = 3;
            this.OKBtn.Text = "OK";
            this.OKBtn.UseVisualStyleBackColor = true;
            this.OKBtn.Click += new System.EventHandler(this.OKBtn_Click);
            // 
            // CancelBtn
            // 
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.Location = new System.Drawing.Point(292, 295);
            this.CancelBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(100, 28);
            this.CancelBtn.TabIndex = 4;
            this.CancelBtn.Text = "Cancel";
            this.CancelBtn.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // DoRBComparisionForm
            // 
            this.AcceptButton = this.OKBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.CancelBtn;
            this.ClientSize = new System.Drawing.Size(580, 338);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.OKBtn);
            this.Controls.Add(this.setoptionsBtn);
            this.Controls.Add(this.OutronfileBrwBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.OutronFileTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DoRBComparisionForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Do FI Comparision";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox rbdirTextBox;
        private System.Windows.Forms.TextBox workdirTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button workdirBrwbtn;
        private System.Windows.Forms.Button rbdirBrwBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox OutronFileTextBox;
        private System.Windows.Forms.Button OutronfileBrwBtn;
        private System.Windows.Forms.Button setoptionsBtn;
        private System.Windows.Forms.Button OKBtn;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ComboBox PickProjComboBox;
    }
}