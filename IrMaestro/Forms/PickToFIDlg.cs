using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

using PickAccessLayer;
using PickData;
using System.Collections;
using System.Collections.Generic;

namespace IRMaestro
{
    public partial class PickToFIDlg : Form
    {
        #region Private Fields
        private bool _extFunc;
        private bool _separateToggle;
        private bool _allLabels;
        private string _pickDBDir;
        #endregion

        #region Public access
        public bool ExtFunc { get { return _extFunc; } }
        public bool SeparateToggle { get { return _separateToggle; } }
        public bool AllLabels { get { return _allLabels; }}
        public string PickDBDir { get { return _pickDBDir; } }
        #endregion
        const string _UEITOOLSROOT =
        "HKEY_CURRENT_USER\\Software\\UEITools\\";
        const string _PICKDBDIR = "IRMaestroPickDBDirectory";
        const string _DEFAULT_PICKDBDIR = "";

        //Start Add - PAL 
        PickAccessLayer.Pick2DAO pickdb = new Pick2DAO();
        IList<Hashtable> ProjNames = null;
        //End Add - PAL 

        public PickToFIDlg()
        {
            InitializeComponent();            
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty((String)ProjectcomboBox.SelectedItem))
            {
                MessageBox.Show("Please Select a project");
                return;
            }
            if (ProjectcomboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a product");
                return;
            }
            _extFunc = this.chkboxExtFunc.Checked;
            _separateToggle = this.chkboxSeparateToggle.Checked;
            _allLabels = this.chkboxAllLabels.Checked;
            _pickDBDir = (String)ProjectcomboBox.SelectedItem;//ProjectcomboBox.Text;
            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

            
        private string GetPickDBDir()
        { 
            return (string)Registry.GetValue(
                _UEITOOLSROOT, _PICKDBDIR, _DEFAULT_PICKDBDIR);
        }

        private void PickToFIDlg_Load(object sender, EventArgs e)
        {            
            ProjNames = PickAccessLayer.PickRetrieveFunctions.GetPickProjectNames();
            ProjectcomboBox.Items.Clear();
            foreach (Hashtable Projs in ProjNames)
            {
                ProjectcomboBox.Items.Add(Projs["Name"]);
            }
            if (ProjectcomboBox.Items != null)
                ProjectcomboBox.Text = GetPickDBDir();
                //ProjectcomboBox.SelectedIndex = 0;            
        }
    }
}