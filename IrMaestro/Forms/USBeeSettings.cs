using System;
using System.ComponentModel;
using System.Windows.Forms;
using CommonCode;

namespace IRMaestro
{
    public partial class USBeeSettings : Form
    {
        public static int PodNo;
        bool conStatus = false;

        USBeeCapture _usbee = new USBeeCapture();
        
        public USBeeSettings()
        {
            InitializeComponent();
        }

        protected override void OnShown(EventArgs e)
        {
            if (_usbee.IsAvailable() == false)
            {
                MessageBox.Show("Error: USBee pod drivers not installed.");
                Close();
            }
        }

        private void txtPodNo_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                PodNo = Convert.ToInt32(txtPodNo.Text);

                if ((PodNo > 32767) || (PodNo < 0))
                    MessageBox.Show("Enter values between 1 and 32767");
                
            }
            catch
            {
                MessageBox.Show("Enter numeric values between 1 and 32767");
            }
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            _usbee.SetPodNo(Convert.ToInt32(txtPodNo.Text));
            conStatus = _usbee.IsPodConnected();
            
            if (conStatus != true)
                MessageBox.Show("Invalid Pod Number");

            if(conStatus==true)
                this.Close();
        }
        private void USBeeSettings_Load(object sender, EventArgs e)
        {
            txtPodNo.Text = _usbee.GetPodNo().ToString();
        }

        
        

        }
}