namespace IRMaestro.Forms
{
    partial class PickToCFIDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.idSelectionControl1 = new CommonForms.Forms.IDSelectionControl();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.productDBRadioButton = new System.Windows.Forms.RadioButton();
            this.pickDBRadioButton = new System.Windows.Forms.RadioButton();
            this.databaseSelectorComboBox1 = new CommonForms.Forms.DatabaseSelectorComboBox();
            this.productComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pickInput = new CommonUI.Controls.DirectoryInput();
            this.directoryInput1 = new CommonUI.Controls.DirectoryInput();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.idSelectionControl1);
            this.groupBox1.Location = new System.Drawing.Point(15, 198);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(410, 284);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ID Selection";
            // 
            // idSelectionControl1
            // 
            this.idSelectionControl1.Location = new System.Drawing.Point(5, 19);
            this.idSelectionControl1.Name = "idSelectionControl1";
            this.idSelectionControl1.Size = new System.Drawing.Size(390, 255);
            this.idSelectionControl1.TabIndex = 14;
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(15, 537);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 20;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(180, 537);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 21;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 485);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Directory for CFI Output";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.productDBRadioButton);
            this.groupBox2.Controls.Add(this.pickDBRadioButton);
            this.groupBox2.Controls.Add(this.pickInput);
            this.groupBox2.Location = new System.Drawing.Point(12, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(413, 189);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Source";
            // 
            // productDBRadioButton
            // 
            this.productDBRadioButton.AutoSize = true;
            this.productDBRadioButton.Location = new System.Drawing.Point(19, 79);
            this.productDBRadioButton.Name = "productDBRadioButton";
            this.productDBRadioButton.Size = new System.Drawing.Size(137, 17);
            this.productDBRadioButton.TabIndex = 28;
            this.productDBRadioButton.Text = "From product database:";
            this.productDBRadioButton.UseVisualStyleBackColor = true;
            // 
            // pickDBRadioButton
            // 
            this.pickDBRadioButton.AutoSize = true;
            this.pickDBRadioButton.Checked = true;
            this.pickDBRadioButton.Location = new System.Drawing.Point(19, 19);
            this.pickDBRadioButton.Name = "pickDBRadioButton";
            this.pickDBRadioButton.Size = new System.Drawing.Size(138, 17);
            this.pickDBRadioButton.TabIndex = 27;
            this.pickDBRadioButton.TabStop = true;
            this.pickDBRadioButton.Text = "From Pick DB Directory:";
            this.pickDBRadioButton.UseVisualStyleBackColor = true;
            // 
            // databaseSelectorComboBox1
            // 
            this.databaseSelectorComboBox1.Location = new System.Drawing.Point(13, 19);
            this.databaseSelectorComboBox1.Name = "databaseSelectorComboBox1";
            this.databaseSelectorComboBox1.Size = new System.Drawing.Size(275, 28);
            this.databaseSelectorComboBox1.TabIndex = 25;
            this.databaseSelectorComboBox1.Enter += new System.EventHandler(this.databaseSelectorComboBox1_Enter);
            // 
            // productComboBox
            // 
            this.productComboBox.FormattingEnabled = true;
            this.productComboBox.Location = new System.Drawing.Point(13, 53);
            this.productComboBox.Name = "productComboBox";
            this.productComboBox.Size = new System.Drawing.Size(275, 21);
            this.productComboBox.Sorted = true;
            this.productComboBox.TabIndex = 29;
            this.productComboBox.DropDown += new System.EventHandler(this.productComboBox_DropDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.databaseSelectorComboBox1);
            this.groupBox3.Controls.Add(this.productComboBox);
            this.groupBox3.Location = new System.Drawing.Point(41, 101);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(308, 81);
            this.groupBox3.TabIndex = 30;
            this.groupBox3.TabStop = false;
            // 
            // pickInput
            // 
            this.pickInput.DirPath = "C:\\Working";
            this.pickInput.Location = new System.Drawing.Point(40, 42);
            this.pickInput.Name = "pickInput";
            this.pickInput.Size = new System.Drawing.Size(358, 26);
            this.pickInput.TabIndex = 24;
            this.pickInput.Enter += new System.EventHandler(this.pickInput_Enter);
            // 
            // directoryInput1
            // 
            this.directoryInput1.DirPath = "C:\\Working";
            this.directoryInput1.Location = new System.Drawing.Point(15, 501);
            this.directoryInput1.Name = "directoryInput1";
            this.directoryInput1.Size = new System.Drawing.Size(410, 26);
            this.directoryInput1.TabIndex = 22;
            // 
            // PickToCFIDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(443, 573);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.directoryInput1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.groupBox1);
            this.Name = "PickToCFIDlg";
            this.Text = "PickToCFIDlg";
            this.Load += new System.EventHandler(this.PickToCFIDlg_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CommonForms.Forms.IDSelectionControl idSelectionControl1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private CommonUI.Controls.DirectoryInput directoryInput1;
        private System.Windows.Forms.Label label1;
        private CommonUI.Controls.DirectoryInput pickInput;
        private System.Windows.Forms.GroupBox groupBox2;
        private CommonForms.Forms.DatabaseSelectorComboBox databaseSelectorComboBox1;
        private System.Windows.Forms.RadioButton productDBRadioButton;
        private System.Windows.Forms.RadioButton pickDBRadioButton;
        private System.Windows.Forms.ComboBox productComboBox;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}