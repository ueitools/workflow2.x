using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IRMaestro.Forms
{
    public partial class FIReadbackOption : Form
    {
        public FIReadbackOption()
        {
            InitializeComponent();
        }

        private void FIReadbackOption_Load(object sender, EventArgs e)
        {
            chkPulseDuration.Checked = true;
            chkDelayDuration.Checked = true;
            chkFrequency.Checked = true;
            chkCodeGap.Checked = true;
            chkDutyCycle.Checked = false;
            chkKeepFI.Checked = true;
            txtDutyCycle.Enabled = false;

            txtPulseDuration.Text = "6";
            txtDelayDuration.Text = "6";
            txtFrequency.Text = "6";
            txtCodeGap.Text = "6";
            txtDutyCycle.Text = "60";

            txtCombinePulseDelay.Text = "6";
            txtNPONotCombinePulseDurationTolerance.Enabled = false;
            txtNPONotCombinePulseDurationTolerance.Text = "40";
            txtNPODelayTolerance.Enabled = false;
            txtNPODelayTolerance.Text = "10";


        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}