namespace IRMaestro.Forms
{
    partial class DoReadbackComparison
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpDirectorySetting = new System.Windows.Forms.GroupBox();
            this.btnBrowseReadbackDir = new System.Windows.Forms.Button();
            this.btnBrowsePickDB = new System.Windows.Forms.Button();
            this.btnBrowseWorkingDirectory = new System.Windows.Forms.Button();
            this.txtReadbackDirectory = new System.Windows.Forms.TextBox();
            this.txtPickDBDirectory = new System.Windows.Forms.TextBox();
            this.txtWorkingDirectory = new System.Windows.Forms.TextBox();
            this.lblReadbackDirectory = new System.Windows.Forms.Label();
            this.lblPickDBDirectory = new System.Windows.Forms.Label();
            this.lblWorkingDirectory = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbCoveraAllReadback = new System.Windows.Forms.RadioButton();
            this.rbCoverUnmatchedReadback = new System.Windows.Forms.RadioButton();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.lblOutronFile = new System.Windows.Forms.Label();
            this.btnSetup = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.grpDirectorySetting.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDirectorySetting
            // 
            this.grpDirectorySetting.AccessibleRole = System.Windows.Forms.AccessibleRole.Cursor;
            this.grpDirectorySetting.Controls.Add(this.btnBrowseReadbackDir);
            this.grpDirectorySetting.Controls.Add(this.btnBrowsePickDB);
            this.grpDirectorySetting.Controls.Add(this.btnBrowseWorkingDirectory);
            this.grpDirectorySetting.Controls.Add(this.txtReadbackDirectory);
            this.grpDirectorySetting.Controls.Add(this.txtPickDBDirectory);
            this.grpDirectorySetting.Controls.Add(this.txtWorkingDirectory);
            this.grpDirectorySetting.Controls.Add(this.lblReadbackDirectory);
            this.grpDirectorySetting.Controls.Add(this.lblPickDBDirectory);
            this.grpDirectorySetting.Controls.Add(this.lblWorkingDirectory);
            this.grpDirectorySetting.Location = new System.Drawing.Point(12, 12);
            this.grpDirectorySetting.Name = "grpDirectorySetting";
            this.grpDirectorySetting.Size = new System.Drawing.Size(378, 122);
            this.grpDirectorySetting.TabIndex = 0;
            this.grpDirectorySetting.TabStop = false;
            this.grpDirectorySetting.Text = "Directory Setting";
            // 
            // btnBrowseReadbackDir
            // 
            this.btnBrowseReadbackDir.Location = new System.Drawing.Point(295, 74);
            this.btnBrowseReadbackDir.Name = "btnBrowseReadbackDir";
            this.btnBrowseReadbackDir.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseReadbackDir.TabIndex = 12;
            this.btnBrowseReadbackDir.Text = "Browse...";
            this.btnBrowseReadbackDir.UseVisualStyleBackColor = true;
            this.btnBrowseReadbackDir.Click += new System.EventHandler(this.btnBrowseReadbackDir_Click);
            // 
            // btnBrowsePickDB
            // 
            this.btnBrowsePickDB.Location = new System.Drawing.Point(295, 45);
            this.btnBrowsePickDB.Name = "btnBrowsePickDB";
            this.btnBrowsePickDB.Size = new System.Drawing.Size(75, 23);
            this.btnBrowsePickDB.TabIndex = 11;
            this.btnBrowsePickDB.Text = "Browse...";
            this.btnBrowsePickDB.UseVisualStyleBackColor = true;
            this.btnBrowsePickDB.Click += new System.EventHandler(this.btnBrowsePickDB_Click);
            // 
            // btnBrowseWorkingDirectory
            // 
            this.btnBrowseWorkingDirectory.Location = new System.Drawing.Point(295, 14);
            this.btnBrowseWorkingDirectory.Name = "btnBrowseWorkingDirectory";
            this.btnBrowseWorkingDirectory.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseWorkingDirectory.TabIndex = 10;
            this.btnBrowseWorkingDirectory.Text = "Browse...";
            this.btnBrowseWorkingDirectory.UseVisualStyleBackColor = true;
            this.btnBrowseWorkingDirectory.Click += new System.EventHandler(this.btnBrowseWorkingDirectory_Click);
            // 
            // txtReadbackDirectory
            // 
            this.txtReadbackDirectory.Location = new System.Drawing.Point(115, 77);
            this.txtReadbackDirectory.Name = "txtReadbackDirectory";
            this.txtReadbackDirectory.Size = new System.Drawing.Size(174, 20);
            this.txtReadbackDirectory.TabIndex = 5;
            // 
            // txtPickDBDirectory
            // 
            this.txtPickDBDirectory.Location = new System.Drawing.Point(115, 48);
            this.txtPickDBDirectory.Name = "txtPickDBDirectory";
            this.txtPickDBDirectory.Size = new System.Drawing.Size(174, 20);
            this.txtPickDBDirectory.TabIndex = 4;
            // 
            // txtWorkingDirectory
            // 
            this.txtWorkingDirectory.Location = new System.Drawing.Point(115, 17);
            this.txtWorkingDirectory.Name = "txtWorkingDirectory";
            this.txtWorkingDirectory.Size = new System.Drawing.Size(174, 20);
            this.txtWorkingDirectory.TabIndex = 3;
            // 
            // lblReadbackDirectory
            // 
            this.lblReadbackDirectory.AutoSize = true;
            this.lblReadbackDirectory.Location = new System.Drawing.Point(7, 84);
            this.lblReadbackDirectory.Name = "lblReadbackDirectory";
            this.lblReadbackDirectory.Size = new System.Drawing.Size(102, 13);
            this.lblReadbackDirectory.TabIndex = 2;
            this.lblReadbackDirectory.Text = "Readback Directory";
            // 
            // lblPickDBDirectory
            // 
            this.lblPickDBDirectory.AutoSize = true;
            this.lblPickDBDirectory.Location = new System.Drawing.Point(7, 51);
            this.lblPickDBDirectory.Name = "lblPickDBDirectory";
            this.lblPickDBDirectory.Size = new System.Drawing.Size(88, 13);
            this.lblPickDBDirectory.TabIndex = 1;
            this.lblPickDBDirectory.Text = "PickDB Directory";
            // 
            // lblWorkingDirectory
            // 
            this.lblWorkingDirectory.AutoSize = true;
            this.lblWorkingDirectory.Location = new System.Drawing.Point(7, 20);
            this.lblWorkingDirectory.Name = "lblWorkingDirectory";
            this.lblWorkingDirectory.Size = new System.Drawing.Size(92, 13);
            this.lblWorkingDirectory.TabIndex = 0;
            this.lblWorkingDirectory.Text = "Working Directory";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbCoveraAllReadback);
            this.groupBox2.Controls.Add(this.rbCoverUnmatchedReadback);
            this.groupBox2.Location = new System.Drawing.Point(12, 140);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(378, 64);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Readback Summary Option";
            // 
            // rbCoveraAllReadback
            // 
            this.rbCoveraAllReadback.AutoSize = true;
            this.rbCoveraAllReadback.Location = new System.Drawing.Point(25, 41);
            this.rbCoveraAllReadback.Name = "rbCoveraAllReadback";
            this.rbCoveraAllReadback.Size = new System.Drawing.Size(125, 17);
            this.rbCoveraAllReadback.TabIndex = 1;
            this.rbCoveraAllReadback.TabStop = true;
            this.rbCoveraAllReadback.Text = "Cover All Readbacks";
            this.rbCoveraAllReadback.UseVisualStyleBackColor = true;
            // 
            // rbCoverUnmatchedReadback
            // 
            this.rbCoverUnmatchedReadback.AutoSize = true;
            this.rbCoverUnmatchedReadback.Checked = true;
            this.rbCoverUnmatchedReadback.Location = new System.Drawing.Point(25, 18);
            this.rbCoverUnmatchedReadback.Name = "rbCoverUnmatchedReadback";
            this.rbCoverUnmatchedReadback.Size = new System.Drawing.Size(193, 17);
            this.rbCoverUnmatchedReadback.TabIndex = 0;
            this.rbCoverUnmatchedReadback.TabStop = true;
            this.rbCoverUnmatchedReadback.Text = "Only Cover Unmatched Readbacks";
            this.rbCoverUnmatchedReadback.UseVisualStyleBackColor = true;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(127, 214);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(255, 20);
            this.textBox4.TabIndex = 6;
            // 
            // lblOutronFile
            // 
            this.lblOutronFile.AutoSize = true;
            this.lblOutronFile.Location = new System.Drawing.Point(19, 217);
            this.lblOutronFile.Name = "lblOutronFile";
            this.lblOutronFile.Size = new System.Drawing.Size(61, 13);
            this.lblOutronFile.TabIndex = 6;
            this.lblOutronFile.Text = "Outron File:";
            // 
            // btnSetup
            // 
            this.btnSetup.Location = new System.Drawing.Point(22, 245);
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.Size = new System.Drawing.Size(89, 23);
            this.btnSetup.TabIndex = 7;
            this.btnSetup.Text = "Setup Option";
            this.btnSetup.UseVisualStyleBackColor = true;
            this.btnSetup.Click += new System.EventHandler(this.btnSetup_Click);
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(172, 245);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 8;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(315, 245);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // DoReadbackComparison
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 285);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnSetup);
            this.Controls.Add(this.lblOutronFile);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.grpDirectorySetting);
            this.KeyPreview = true;
            this.Name = "DoReadbackComparison";
            this.ShowInTaskbar = false;
            this.Text = "Do Readback Comparison";
            this.Load += new System.EventHandler(this.DoReadbackComparison_Load);
            //this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DoReadbackComparison_KeyPress);
            this.grpDirectorySetting.ResumeLayout(false);
            this.grpDirectorySetting.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDirectorySetting;
        private System.Windows.Forms.TextBox txtReadbackDirectory;
        private System.Windows.Forms.TextBox txtPickDBDirectory;
        private System.Windows.Forms.TextBox txtWorkingDirectory;
        private System.Windows.Forms.Label lblReadbackDirectory;
        private System.Windows.Forms.Label lblPickDBDirectory;
        private System.Windows.Forms.Label lblWorkingDirectory;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbCoveraAllReadback;
        private System.Windows.Forms.RadioButton rbCoverUnmatchedReadback;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label lblOutronFile;
        private System.Windows.Forms.Button btnSetup;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnBrowsePickDB;
        private System.Windows.Forms.Button btnBrowseWorkingDirectory;
        private System.Windows.Forms.Button btnBrowseReadbackDir;
    }
}