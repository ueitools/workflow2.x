using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommonForms;
using CommonCode;

namespace IRMaestro.Forms
{
    public partial class DoReadbackComparison : Form
    {
        public DoReadbackComparison()
        {
            InitializeComponent();
        }

        private void btnSetup_Click(object sender, EventArgs e)
        {
            FIReadbackOption FISetup = new FIReadbackOption();
            FISetup.ShowDialog();
        }

        private void DoReadbackComparison_Load(object sender, EventArgs e)
        {
            txtWorkingDirectory.Text=CommonForms.Configuration.GetWorkingDirectory();
            txtPickDBDirectory.Text = CommonForms.Configuration.GetPickDirectory();
            txtReadbackDirectory.Text = CommonForms.Configuration.GetReadbackDirectory();
        }

        private void btnBrowseWorkingDirectory_Click(object sender, EventArgs e)
        {
            CommonForms.Configuration.SetWorkingDirectory();
            txtWorkingDirectory.Text = CommonForms.Configuration.GetWorkingDirectory();
        }

        private void btnBrowsePickDB_Click(object sender, EventArgs e)
        {
            CommonForms.Configuration.SetPickDirectory();
            txtPickDBDirectory.Text = CommonForms.Configuration.GetPickDirectory();
        }

        private void btnBrowseReadbackDir_Click(object sender, EventArgs e)
        {
            CommonForms.Configuration.SetReadbackDirectory();
            txtReadbackDirectory.Text = CommonForms.Configuration.GetReadbackDirectory();
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            IDForm idform = new IDForm(txtPickDBDirectory.Text, txtReadbackDirectory.Text, txtWorkingDirectory.Text, IRMaestro.IDForm.PickSource.Live);
            idform.Show();
        }

        //private void DoReadbackComparison_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    int keyval=e.KeyChar;
        //    switch (keyval)
        //    {
        //        case 27:
        //            {
        //                btnCancel_Click(null, null);
        //                break;
        //            }
        //        case 13:
        //            {
        //                btnOk_Click(null, null);
        //                break;
        //            }
        //    }

            

        }
        

        
    }
