using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.GZip;
using CommonForms;
using Microsoft.Win32;
using IRMaestro.Forms;
//Start Add - PAL 
using PickAccessLayer;
using PickData;
using System.Collections;
//End Add - PAL


namespace IRMaestro
{
    public partial class DoRBComparisionForm : Form
    {
        public DoRBComparisionForm()
        {
            InitializeComponent();
            CFIListForm.bShowTimingReport = true;
            workdirTextBox.Text = Configuration.GetWorkingDirectory();
            //string pickProject = Configuration.GetConfigItem("FICompare", "PickProject", "");
            //rbdirTextBox.Text = Configuration.GetConfigItem("FICompare", "RdBkPath", "");

            string pickProject = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                        UserSettings.ConfigKeys.PICK_DIR);

            rbdirTextBox.Text = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                        UserSettings.ConfigKeys.READBACK_DIR);


            //Start Add - PAL 
            PickAccessLayer.Pick2DAO pickdb = new Pick2DAO();
            IList<Hashtable> ProjNames = null;

            ProjNames = PickAccessLayer.PickRetrieveFunctions.GetPickProjectNames();
            PickProjComboBox.Items.Clear();
            foreach (Hashtable Projs in ProjNames)
            {
                PickProjComboBox.Items.Add(Projs["Name"]);
            }
            if (PickProjComboBox.Items != null)
                PickProjComboBox.Text = pickProject;
            //End Add - PAL 

        }

        private void workdirBrwbtn_Click(object sender, EventArgs e)
        {
            Configuration.SetWorkingDirectory();
            workdirTextBox.Text = Configuration.GetWorkingDirectory();

            //folderBrowserDialog1.SelectedPath = workdirTextBox.Text;
            //DialogResult dlgres = folderBrowserDialog1.ShowDialog();
            //if (dlgres == DialogResult.OK)
            //{
            //    workdirTextBox.Text = folderBrowserDialog1.SelectedPath;
            //}
        }

   
        private void rbdirBrwBtn_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = rbdirTextBox.Text;
            DialogResult dlgres = folderBrowserDialog1.ShowDialog();
            if (dlgres == DialogResult.OK)
            {
                rbdirTextBox.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void OutronfileBrwBtn_Click(object sender, EventArgs e)
        {    
            DialogResult dlgres = openFileDialog1.ShowDialog();
            if (dlgres == DialogResult.OK)
            {
                OutronFileTextBox.Text = openFileDialog1.FileName;
            }         
        }

        private void setoptionsBtn_Click(object sender, EventArgs e)
        {
            FIRBOptions options = new FIRBOptions();
            options.ShowDialog();         
           
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(workdirTextBox.Text))
            {
                MessageBox.Show("Select a Valid Working directory");
                return;
            }
            else
            {
                Registry.SetValue("HKEY_CURRENT_USER\\Software\\UEITools\\", "WorkingDirectory",
                                   workdirTextBox.Text, RegistryValueKind.String);               

            }
            if (File.Exists(workdirTextBox.Text + "\\results.rpt"))
            {
                MergeResultsForm mergeForm = new MergeResultsForm();
                DialogResult dlgRes = mergeForm.ShowDialog();

                if (dlgRes == DialogResult.Yes)
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                              UserSettings.ConfigKeys.MERGE_REPORT,
                                              "1");

                }
                else
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                              UserSettings.ConfigKeys.MERGE_REPORT,
                                              "0");
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                               UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                               "0");
                    return;
                }
            }
            else
            {
                UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                          UserSettings.ConfigKeys.MERGE_REPORT,
                                          "0");
                UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                           UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                           "0");
            }
            

            if ((PickProjComboBox.Text == "") || (String.IsNullOrEmpty((string)PickProjComboBox.SelectedItem)))
            {
                 MessageBox.Show("Select a Valid Pick Project");
                return;
            }
            else
            {
                //CommonForms.Configuration.SetConfigItem("FICompare", "PickProject", PickProjComboBox.Text);

                UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                            UserSettings.ConfigKeys.PICK_DIR,
                                            PickProjComboBox.Text);

             
            }

            if (!Directory.Exists(rbdirTextBox.Text))
            {
                MessageBox.Show("Select a Valid Readback directory");
                return;
            }
            else
            {
                ////CommonForms.Configuration.SetConfigItem("FICompare", "RdBkPath", rbdirTextBox.Text);


                UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                            UserSettings.ConfigKeys.READBACK_DIR,
                                            rbdirTextBox.Text);
            }



            //IDForm idform = new IDForm(PickProjComboBox.Text, rbdirTextBox.Text, workdirTextBox.Text, IDForm.PickSource.StaticDB);
            IDForm idForm = new IDForm(rbdirTextBox.Text, workdirTextBox.Text, IRMaestro.Lib.Readback.PickSource.StaticDB, (string)PickProjComboBox.SelectedItem);
            idForm.SetRbFiComparision(true);
            idForm.ShowDialog();
        }
    }
}