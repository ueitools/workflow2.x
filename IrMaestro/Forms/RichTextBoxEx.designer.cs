namespace IRMaestro.Forms
{
    partial class RichTextBoxEx
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lnbMain = new TextBoxEx.RichTextBoxLineNumberBox();
            this.rtbMain = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // lnbMain
            // 
            this.lnbMain._SeeThroughMode_ = false;
            this.lnbMain.AutoSizing = true;
            this.lnbMain.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lnbMain.BackgroundGradient_AlphaColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lnbMain.BackgroundGradient_BetaColor = System.Drawing.Color.LightSteelBlue;
            this.lnbMain.BackgroundGradient_Direction = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.lnbMain.BorderLines_Color = System.Drawing.Color.SlateGray;
            this.lnbMain.BorderLines_Style = System.Drawing.Drawing2D.DashStyle.Dot;
            this.lnbMain.BorderLines_Thickness = 1F;
            this.lnbMain.DockSide = TextBoxEx.RichTextBoxLineNumberBox.LineNumberDockSide.Left;
            this.lnbMain.GridLines_Color = System.Drawing.Color.SlateGray;
            this.lnbMain.GridLines_Style = System.Drawing.Drawing2D.DashStyle.Dot;
            this.lnbMain.GridLines_Thickness = 1F;
            this.lnbMain.LineNrs_Alignment = System.Drawing.ContentAlignment.TopRight;
            this.lnbMain.LineNrs_AntiAlias = false;
            this.lnbMain.LineNrs_AsHexadecimal = false;
            this.lnbMain.LineNrs_ClippedByItemRectangle = true;
            this.lnbMain.LineNrs_LeadingZeroes = false;
            this.lnbMain.LineNrs_Offset = new System.Drawing.Size(0, 0);
            this.lnbMain.Location = new System.Drawing.Point(7, 3);
            this.lnbMain.Margin = new System.Windows.Forms.Padding(0);
            this.lnbMain.MarginLines_Color = System.Drawing.Color.SlateGray;
            this.lnbMain.MarginLines_Side = TextBoxEx.RichTextBoxLineNumberBox.LineNumberDockSide.Right;
            this.lnbMain.MarginLines_Style = System.Drawing.Drawing2D.DashStyle.Solid;
            this.lnbMain.MarginLines_Thickness = 1F;
            this.lnbMain.Name = "lnbMain";
            this.lnbMain.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.lnbMain.ParentRichTextBox = this.rtbMain;
            this.lnbMain.Show_BackgroundGradient = false;
            this.lnbMain.Show_BorderLines = false;
            this.lnbMain.Show_GridLines = false;
            this.lnbMain.Show_LineNrs = true;
            this.lnbMain.Show_MarginLines = false;
            this.lnbMain.Size = new System.Drawing.Size(17, 278);
            this.lnbMain.TabIndex = 0;
            // 
            // rtbMain
            // 
            this.rtbMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbMain.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbMain.Location = new System.Drawing.Point(25, 3);
            this.rtbMain.Name = "rtbMain";
            this.rtbMain.Size = new System.Drawing.Size(426, 278);
            this.rtbMain.TabIndex = 1;
            this.rtbMain.Text = "";
            // 
            // RichTextBoxEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.rtbMain);
            this.Controls.Add(this.lnbMain);
            this.Name = "RichTextBoxEx";
            this.Size = new System.Drawing.Size(454, 284);
            this.ResumeLayout(false);

        }

        #endregion

        private TextBoxEx.RichTextBoxLineNumberBox lnbMain;
        private System.Windows.Forms.RichTextBox rtbMain;
    }
}
