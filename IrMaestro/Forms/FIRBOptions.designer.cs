namespace IRMaestro
{
    partial class FIRBOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FIRBOptions));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dutycycletb = new System.Windows.Forms.TextBox();
            this.codegaptb = new System.Windows.Forms.TextBox();
            this.frequencytb = new System.Windows.Forms.TextBox();
            this.delaydurationtb = new System.Windows.Forms.TextBox();
            this.PulseDurationtb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DutyCycleCBox = new System.Windows.Forms.CheckBox();
            this.CodeGapCBox = new System.Windows.Forms.CheckBox();
            this.FrequencyCBox = new System.Windows.Forms.CheckBox();
            this.DelayDurationCBox = new System.Windows.Forms.CheckBox();
            this.PulseDurationCBox = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.keepFIcheckBox = new System.Windows.Forms.CheckBox();
            this.okBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.CmpFramesChkBox = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dutycycletb);
            this.groupBox1.Controls.Add(this.codegaptb);
            this.groupBox1.Controls.Add(this.frequencytb);
            this.groupBox1.Controls.Add(this.delaydurationtb);
            this.groupBox1.Controls.Add(this.PulseDurationtb);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.DutyCycleCBox);
            this.groupBox1.Controls.Add(this.CodeGapCBox);
            this.groupBox1.Controls.Add(this.FrequencyCBox);
            this.groupBox1.Controls.Add(this.DelayDurationCBox);
            this.groupBox1.Controls.Add(this.PulseDurationCBox);
            this.groupBox1.Location = new System.Drawing.Point(16, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(340, 160);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // dutycycletb
            // 
            this.dutycycletb.Location = new System.Drawing.Point(265, 126);
            this.dutycycletb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dutycycletb.Name = "dutycycletb";
            this.dutycycletb.Size = new System.Drawing.Size(49, 22);
            this.dutycycletb.TabIndex = 9;
            this.dutycycletb.Text = "60";
            this.dutycycletb.Validating += new System.ComponentModel.CancelEventHandler(this.dutycycletb_Validating);
            // 
            // codegaptb
            // 
            this.codegaptb.Location = new System.Drawing.Point(265, 98);
            this.codegaptb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.codegaptb.Name = "codegaptb";
            this.codegaptb.Size = new System.Drawing.Size(49, 22);
            this.codegaptb.TabIndex = 7;
            this.codegaptb.Text = "6";
            this.codegaptb.Validating += new System.ComponentModel.CancelEventHandler(this.codegaptb_Validating);
            // 
            // frequencytb
            // 
            this.frequencytb.Location = new System.Drawing.Point(265, 71);
            this.frequencytb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.frequencytb.Name = "frequencytb";
            this.frequencytb.Size = new System.Drawing.Size(49, 22);
            this.frequencytb.TabIndex = 5;
            this.frequencytb.Text = "6";
            this.frequencytb.Validating += new System.ComponentModel.CancelEventHandler(this.frequencytb_Validating);
            // 
            // delaydurationtb
            // 
            this.delaydurationtb.Location = new System.Drawing.Point(265, 44);
            this.delaydurationtb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.delaydurationtb.Name = "delaydurationtb";
            this.delaydurationtb.Size = new System.Drawing.Size(49, 22);
            this.delaydurationtb.TabIndex = 3;
            this.delaydurationtb.Text = "6";
            this.delaydurationtb.Validating += new System.ComponentModel.CancelEventHandler(this.delaydurationtb_Validating);
            // 
            // PulseDurationtb
            // 
            this.PulseDurationtb.Location = new System.Drawing.Point(265, 17);
            this.PulseDurationtb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PulseDurationtb.Name = "PulseDurationtb";
            this.PulseDurationtb.Size = new System.Drawing.Size(49, 22);
            this.PulseDurationtb.TabIndex = 1;
            this.PulseDurationtb.Text = "6";
            this.PulseDurationtb.Validating += new System.ComponentModel.CancelEventHandler(this.PulseDurationtb_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(203, 128);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Dev(%)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(203, 101);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Dev(%)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(203, 74);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Dev(%)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(203, 47);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Dev(%)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(203, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dev(%)";
            // 
            // DutyCycleCBox
            // 
            this.DutyCycleCBox.AutoSize = true;
            this.DutyCycleCBox.Checked = true;
            this.DutyCycleCBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DutyCycleCBox.Location = new System.Drawing.Point(8, 126);
            this.DutyCycleCBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DutyCycleCBox.Name = "DutyCycleCBox";
            this.DutyCycleCBox.Size = new System.Drawing.Size(97, 21);
            this.DutyCycleCBox.TabIndex = 8;
            this.DutyCycleCBox.Text = "Duty Cycle";
            this.DutyCycleCBox.UseVisualStyleBackColor = true;
            // 
            // CodeGapCBox
            // 
            this.CodeGapCBox.AutoSize = true;
            this.CodeGapCBox.Checked = true;
            this.CodeGapCBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CodeGapCBox.Location = new System.Drawing.Point(8, 98);
            this.CodeGapCBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CodeGapCBox.Name = "CodeGapCBox";
            this.CodeGapCBox.Size = new System.Drawing.Size(94, 21);
            this.CodeGapCBox.TabIndex = 6;
            this.CodeGapCBox.Text = "Code Gap";
            this.CodeGapCBox.UseVisualStyleBackColor = true;
            // 
            // FrequencyCBox
            // 
            this.FrequencyCBox.AutoSize = true;
            this.FrequencyCBox.Checked = true;
            this.FrequencyCBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.FrequencyCBox.Location = new System.Drawing.Point(8, 71);
            this.FrequencyCBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FrequencyCBox.Name = "FrequencyCBox";
            this.FrequencyCBox.Size = new System.Drawing.Size(97, 21);
            this.FrequencyCBox.TabIndex = 4;
            this.FrequencyCBox.Text = "Frequency";
            this.FrequencyCBox.UseVisualStyleBackColor = true;
            // 
            // DelayDurationCBox
            // 
            this.DelayDurationCBox.AutoSize = true;
            this.DelayDurationCBox.Checked = true;
            this.DelayDurationCBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DelayDurationCBox.Location = new System.Drawing.Point(8, 44);
            this.DelayDurationCBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DelayDurationCBox.Name = "DelayDurationCBox";
            this.DelayDurationCBox.Size = new System.Drawing.Size(124, 21);
            this.DelayDurationCBox.TabIndex = 2;
            this.DelayDurationCBox.Text = "Delay Duration";
            this.DelayDurationCBox.UseVisualStyleBackColor = true;
            // 
            // PulseDurationCBox
            // 
            this.PulseDurationCBox.AutoSize = true;
            this.PulseDurationCBox.Checked = true;
            this.PulseDurationCBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PulseDurationCBox.Location = new System.Drawing.Point(8, 17);
            this.PulseDurationCBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PulseDurationCBox.Name = "PulseDurationCBox";
            this.PulseDurationCBox.Size = new System.Drawing.Size(123, 21);
            this.PulseDurationCBox.TabIndex = 0;
            this.PulseDurationCBox.Text = "Pulse Duration";
            this.PulseDurationCBox.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox6);
            this.groupBox2.Controls.Add(this.radioButton2);
            this.groupBox2.Controls.Add(this.radioButton1);
            this.groupBox2.Controls.Add(this.textBox8);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.textBox7);
            this.groupBox2.Controls.Add(this.checkBox1);
            this.groupBox2.Controls.Add(this.checkBox2);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Enabled = false;
            this.groupBox2.Location = new System.Drawing.Point(16, 182);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(340, 133);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "For Nacked Pulse Only";
            // 
            // textBox6
            // 
            this.textBox6.Enabled = false;
            this.textBox6.Location = new System.Drawing.Point(265, 21);
            this.textBox6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(49, 22);
            this.textBox6.TabIndex = 1;
            this.textBox6.Text = "6";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Enabled = false;
            this.radioButton2.Location = new System.Drawing.Point(8, 48);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(215, 21);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.Text = "Do Not Combine Pulse/Delay ";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Enabled = false;
            this.radioButton1.Location = new System.Drawing.Point(8, 23);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(167, 21);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Combine Pulse/Delay ";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // textBox8
            // 
            this.textBox8.Enabled = false;
            this.textBox8.Location = new System.Drawing.Point(265, 105);
            this.textBox8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(49, 22);
            this.textBox8.TabIndex = 8;
            this.textBox8.Text = "10";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Enabled = false;
            this.label6.Location = new System.Drawing.Point(203, 25);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "Dev(%)";
            // 
            // textBox7
            // 
            this.textBox7.Enabled = false;
            this.textBox7.Location = new System.Drawing.Point(265, 74);
            this.textBox7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(49, 22);
            this.textBox7.TabIndex = 5;
            this.textBox7.Text = "40";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.Location = new System.Drawing.Point(41, 76);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(123, 21);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.Text = "Pulse Duration";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Enabled = false;
            this.checkBox2.Location = new System.Drawing.Point(41, 103);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(124, 21);
            this.checkBox2.TabIndex = 6;
            this.checkBox2.Text = "Delay Duration";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(203, 76);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 17);
            this.label7.TabIndex = 4;
            this.label7.Text = "Dev(%)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(203, 107);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Dev(%)";
            // 
            // keepFIcheckBox
            // 
            this.keepFIcheckBox.AutoSize = true;
            this.keepFIcheckBox.Checked = true;
            this.keepFIcheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.keepFIcheckBox.Location = new System.Drawing.Point(16, 324);
            this.keepFIcheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.keepFIcheckBox.Name = "keepFIcheckBox";
            this.keepFIcheckBox.Size = new System.Drawing.Size(130, 21);
            this.keepFIcheckBox.TabIndex = 0;
            this.keepFIcheckBox.Text = "Keep All FI Files";
            this.keepFIcheckBox.UseVisualStyleBackColor = true;
            // 
            // okBtn
            // 
            this.okBtn.Location = new System.Drawing.Point(15, 358);
            this.okBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(100, 28);
            this.okBtn.TabIndex = 1;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(249, 358);
            this.cancelBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(100, 28);
            this.cancelBtn.TabIndex = 2;
            this.cancelBtn.Text = "CANCEL";
            this.cancelBtn.UseVisualStyleBackColor = true;
            // 
            // CmpFramesChkBox
            // 
            this.CmpFramesChkBox.AutoSize = true;
            this.CmpFramesChkBox.Location = new System.Drawing.Point(212, 324);
            this.CmpFramesChkBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CmpFramesChkBox.Name = "CmpFramesChkBox";
            this.CmpFramesChkBox.Size = new System.Drawing.Size(138, 21);
            this.CmpFramesChkBox.TabIndex = 3;
            this.CmpFramesChkBox.Text = "Compare Frames";
            this.CmpFramesChkBox.UseVisualStyleBackColor = true;
            // 
            // FIRBOptions
            // 
            this.AcceptButton = this.okBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.cancelBtn;
            this.ClientSize = new System.Drawing.Size(365, 396);
            this.Controls.Add(this.CmpFramesChkBox);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.keepFIcheckBox);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FIRBOptions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FI ReadBack Option";
            this.Load += new System.EventHandler(this.FIRBOptions_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox DutyCycleCBox;
        private System.Windows.Forms.CheckBox CodeGapCBox;
        private System.Windows.Forms.CheckBox FrequencyCBox;
        private System.Windows.Forms.CheckBox DelayDurationCBox;
        private System.Windows.Forms.CheckBox PulseDurationCBox;
        private System.Windows.Forms.TextBox dutycycletb;
        private System.Windows.Forms.TextBox codegaptb;
        private System.Windows.Forms.TextBox frequencytb;
        private System.Windows.Forms.TextBox delaydurationtb;
        private System.Windows.Forms.TextBox PulseDurationtb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox keepFIcheckBox;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.CheckBox CmpFramesChkBox;
    }
}