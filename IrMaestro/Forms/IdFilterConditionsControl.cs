// requires IdFilterConditions.cs in CommonCode
// requires Id.cs in CommonCode

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ToolsData;

namespace CommonUI.Controls {
    public partial class IdFilterConditionsControl : UserControl, IdSelectionFilterControlPresenter.IIdSelectionFilterControlView {
        private IdSelectionFilterControlPresenter _selectionFilterControlPresenter;

        public IdFilterConditionsControl() {
            InitializeComponent();

            _selectionFilterControlPresenter = new IdSelectionFilterControlPresenter();
            _selectionFilterControlPresenter.SetView(this);
        }

        public bool EnableRangeSelector{
            set{
                ByRangeRadioButton.Enabled = value;
                groupBox1.Enabled = value;
            }
        }

        public bool EnableModeSelector{
            set{
                ByModeRadioButton.Enabled = value;
                groupBox2.Enabled = value;
            }
        }

        public bool EnableFileSelector{
            set{
                ByFileRadioButton.Enabled = value;
                groupBox3.Enabled = value;
            }
        }

        public IdFilterConditions FilterConditions {
            get { return _selectionFilterControlPresenter.FilterConditions; }
        }

        public event EventHandler Closed;
        public event FormClosingEventHandler FormClosing;
        public event ControlCreatedDelegate ControlCreated;

        public void CreateControls() {}

        public void NotifyError(string errorMessage) {
            MessageBox.Show(this, errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public event EventHandler FilterByRangeSelected;
        public event EventHandler FilterByModeSelected;
        public event EventHandler FilterByFileSelected;
        public event EventHandler FilterRangeFromChanged;
        public event EventHandler FilterRangeToChanged;
        public event EventHandler FilterModeChanged;
        public event EventHandler FilterFilenameChanged;

        public string GetFilterRangeFrom() {
            return IdFromTextBox.Text;
        }

        public string GetFilterRangeTo() {
            return IdToTextBox.Text;
        }

        public string GetFilterModes() {
            return ModeSelectControl.ModeList;
        }

        public string GetFilterFilename() {
            return IdFileInputControl.FilePath;
        }

        public bool GetFilterFilenameVerificationFlag() {
            return IdFileInputControl.PathVerified;
        }

        public void EnableRangeSelection(bool enable) {
            IdFromLabel.Enabled = enable;
            IdFromTextBox.Enabled = enable;
            IdToLabel.Enabled = enable;
            IdToTextBox.Enabled = enable;
        }

        public void EnableModeSelection(bool enable) {
            ModeSelectControl.Enabled = enable;
        }

        public void EnableFileSelection(bool enable) {
            IdFileInputControl.Enabled = enable;
        }

        public void SetFilterByRange(bool flag) {
            ByRangeRadioButton.Checked = flag;
        }

        public void SetFilterByMode(bool flag) {
            ByModeRadioButton.Checked = flag;
        }

        public void SetFilterByFile(bool flag) {
            ByFileRadioButton.Checked = flag;
        }

        public void SetFilterRangeFrom(string fromId) {
            IdFromTextBox.Text = fromId;
        }

        public void SetFilterRangeTo(string toId) {
            IdToTextBox.Text = toId;
        }

        public void SetFilterModeList(string modes) {
            ModeSelectControl.ModeList = modes;
        }

        public void SetFilterFilterIdFilename(string filename) {
            IdFileInputControl.FilePath = filename;
        }

        private void ByRangeRadioButton_CheckedChanged(object sender, EventArgs e) {
            if (FilterByRangeSelected != null) {
                FilterByRangeSelected(this, EventArgs.Empty);
            }
        }

        private void ByModeRadioButton_CheckedChanged(object sender, EventArgs e) {
            if (FilterByModeSelected != null) {
                FilterByModeSelected(this, EventArgs.Empty);
            }
        }

        private void ByFileRadioButton_CheckedChanged(object sender, EventArgs e) {
            if (FilterByFileSelected != null) {
                FilterByFileSelected(this, EventArgs.Empty);
            }
        }

        private void IdFromTextBox_Leave(object sender, EventArgs e) {
            if (FilterRangeFromChanged != null) {
                FilterRangeFromChanged(this, EventArgs.Empty);
            }
        }

        private void IdToTextBox_Leave(object sender, EventArgs e) {
            if (FilterRangeToChanged != null) {
                FilterRangeToChanged(this, EventArgs.Empty);
            }
        }

        private void ModeSelectControl_ModeSelectionChanged(object sender, EventArgs e) {
            if (FilterModeChanged != null) {
                FilterModeChanged(this, EventArgs.Empty);
            }
        }

        private void IdFileInputControl_FilePathChanged(object sender, EventArgs e) {
            if (FilterFilenameChanged != null) {
                FilterFilenameChanged(this, EventArgs.Empty);
            }
        }

        public bool AreConditionsVerified() {
            return _selectionFilterControlPresenter.IsVerified();
        }

        public void SetFilterControl(IdFilterConditions filterConditions) {
            _selectionFilterControlPresenter.FilterConditions = filterConditions;
        }
    }

    public class IdSelectionFilterControlPresenter : PresenterBase {
        private IdFilterConditions _filterConditions;

        public IdSelectionFilterControlPresenter() {
            _filterConditions = new IdFilterConditions();
        }

        public IdFilterConditions FilterConditions {
            get { return _filterConditions; }
            set {
                _filterConditions = value;

                IdFilterConditions.FilterByType filterType = _filterConditions.FilterType;
                View.SetFilterByRange(filterType == IdFilterConditions.FilterByType.Range);
                View.SetFilterByMode(filterType == IdFilterConditions.FilterByType.Mode);
                View.SetFilterByFile(filterType == IdFilterConditions.FilterByType.File);

                UpdateUI();

                Id fromId = _filterConditions.FilterRange.From;
                View.SetFilterRangeFrom((fromId == null) ? string.Empty : (string)fromId);

                Id toId = _filterConditions.FilterRange.To;
                View.SetFilterRangeTo((toId == null) ? string.Empty : (string)toId);

                string modes = "";
                foreach (char filterMode in _filterConditions.FilterModes) {
                    modes += filterMode;
                }
                View.SetFilterModeList(modes);

                View.SetFilterFilterIdFilename(_filterConditions.FilterIdsFilename);
            }
        }

        private IIdSelectionFilterControlView View {
            get { return (IIdSelectionFilterControlView)_view; }
        }

        public bool IsVerified() {
            if (_filterConditions.FilterType == IdFilterConditions.FilterByType.None) {
                return false;
            }

            if (_filterConditions.FilterType == IdFilterConditions.FilterByType.Range) {
                return (_filterConditions.FilterRange.From != null);
            }

            if (_filterConditions.FilterType == IdFilterConditions.FilterByType.Mode) {
                return _filterConditions.FilterModes.Count != 0;
            }

            if (_filterConditions.FilterType == IdFilterConditions.FilterByType.File) {
                bool isPathVerified = View.GetFilterFilenameVerificationFlag();
                if (isPathVerified == false) {
                    try {
                        _filterConditions.LoadFile(View.GetFilterFilename());
                    } catch (Exception) {
                        return false;
                    }
                }
            }
            return true;
        }

        public override void SetView(IView view) {
            base.SetView(view);

            View.FilterByRangeSelected += View_FilterByRangeSelectedEventHandler;
            View.FilterByModeSelected += View_FilterByModeSelectedEventHandler;
            View.FilterByFileSelected += View_FilterByFileSelectedEventHandler;
            View.FilterRangeFromChanged += View_FilterRangeFromChangedEventHandler;
            View.FilterRangeToChanged += View_FilterRangeToChangedEventHandler;
            View.FilterModeChanged += View_FilterModeChangedEventHandler;
            View.FilterFilenameChanged += View_FilterFilenameChangedEventHandler;

            IdFilterConditions.FilterByType filterType = _filterConditions.FilterType;
            View.SetFilterByRange(filterType == IdFilterConditions.FilterByType.Range);
            View.SetFilterByMode(filterType == IdFilterConditions.FilterByType.Mode);
            View.SetFilterByFile(filterType == IdFilterConditions.FilterByType.File);

            Id fromId = _filterConditions.FilterRange.From;
            View.SetFilterRangeFrom((fromId == null) ? string.Empty : (string)fromId);

            Id toId = _filterConditions.FilterRange.To;
            View.SetFilterRangeTo((toId == null) ? string.Empty : (string)toId);

            UpdateUI();
        }

        private void UpdateUI() {
            IdFilterConditions.FilterByType filterType = _filterConditions.FilterType;
            View.EnableRangeSelection(filterType == IdFilterConditions.FilterByType.Range);
            View.EnableModeSelection(filterType == IdFilterConditions.FilterByType.Mode);
            View.EnableFileSelection(filterType == IdFilterConditions.FilterByType.File);
        }

        private void View_FilterByRangeSelectedEventHandler(object sender, EventArgs e) {
            _filterConditions.FilterType = IdFilterConditions.FilterByType.Range;
            UpdateUI();
        }

        private void View_FilterByModeSelectedEventHandler(object sender, EventArgs e) {
            _filterConditions.FilterType = IdFilterConditions.FilterByType.Mode;
            UpdateUI();
        }

        private void View_FilterByFileSelectedEventHandler(object sender, EventArgs e) {
            _filterConditions.FilterType = IdFilterConditions.FilterByType.File;
            UpdateUI();
        }

        private void View_FilterRangeFromChangedEventHandler(object sender, EventArgs e) {
            string filterRangeFrom = View.GetFilterRangeFrom();

            if (filterRangeFrom.Trim() != string.Empty) {
                try {
                    Id id = new Id(filterRangeFrom);
                    _filterConditions.SetFilterRangeFrom((string)id);
                    View.SetFilterRangeFrom((string)id);
                } catch (Exception x) {
                    View.NotifyError(x.Message);
                }
            }
        }

        private void View_FilterRangeToChangedEventHandler(object sender, EventArgs e) {
            string filterRangeTo = View.GetFilterRangeTo();
            if (filterRangeTo.Trim() != string.Empty) {
                try {
                    Id id = new Id(filterRangeTo);
                    _filterConditions.SetFilterRangeTo((string)id);
                    View.SetFilterRangeTo((string)id);
                } catch (Exception x) {
                    View.NotifyError(x.Message);
                }
            }
        }

        private void View_FilterModeChangedEventHandler(object sender, EventArgs e) {
            _filterConditions.FilterModes.Clear();
            string filterModes = View.GetFilterModes();
            foreach (char filterMode in filterModes) {
                _filterConditions.FilterModes.Add(filterMode);
            }
        }

        private void View_FilterFilenameChangedEventHandler(object sender, EventArgs e) {
            string filename = View.GetFilterFilename();

            try {
                _filterConditions.LoadFile(filename);
            } catch (Exception x) {
                View.NotifyError(x.Message);
            }
        }

        public interface IIdSelectionFilterControlView : IView {
            event EventHandler FilterByRangeSelected;
            event EventHandler FilterByModeSelected;
            event EventHandler FilterByFileSelected;
            event EventHandler FilterRangeFromChanged;
            event EventHandler FilterRangeToChanged;
            event EventHandler FilterModeChanged;
            event EventHandler FilterFilenameChanged;

            string GetFilterRangeFrom();
            string GetFilterRangeTo();
            string GetFilterModes();
            string GetFilterFilename();
            bool GetFilterFilenameVerificationFlag();

            void EnableRangeSelection(bool enable);
            void EnableModeSelection(bool enable);
            void EnableFileSelection(bool enable);

            void SetFilterByRange(bool flag);
            void SetFilterByMode(bool flag);
            void SetFilterByFile(bool flag);
            void SetFilterRangeFrom(string fromId);
            void SetFilterRangeTo(string toId);
            void SetFilterModeList(string modes);
            void SetFilterFilterIdFilename(string filename);
        }
    }

    public abstract class PresenterBase {
        protected IView _view;

        public virtual void SetView(IView view) {
            _view = view;
        }
    }

    public delegate void ControlCreatedDelegate(Control control);

    public interface IView {
        event EventHandler Closed;
        event FormClosingEventHandler FormClosing;
        event ControlCreatedDelegate ControlCreated;

        Point Location { get; }
        Size Size { get; }

        void CreateControls();
        void NotifyError(string errorMessage);
    }

    public abstract class MvpDataContainers {
        public class ListData : List<string> {
            public ListData() {}
            public ListData(int capacity) : base(capacity) {}
            public ListData(IEnumerable<string> collection) : base(collection) {}
        }

        public class TableData : List<TableData.RowData> {
            public TableData() {}
            public TableData(int capacity) : base(capacity) {}
            public TableData(IEnumerable<RowData> collection) : base(collection) {}

            public class RowData : List<string> {
                public RowData() {}
                public RowData(int capacity) : base(capacity) {}
                public RowData(IEnumerable<string> collection) : base(collection) {}
            }
        }

        public class HierarchicalData : List<HierarchicalData.Node> {
            public HierarchicalData() {}
            public HierarchicalData(int capacity) : base(capacity) {}
            public HierarchicalData(IEnumerable<Node> collection) : base(collection) {}

            public class Node {
                private string _label;
                private List<Node> _children;

                public Node(string label) {
                    _label = label;
                    _children = new List<Node>();
                }

                public string Label {
                    get { return _label; }
                }

                public List<Node> Children {
                    get { return _children; }
                }
            }
        }
    }
}