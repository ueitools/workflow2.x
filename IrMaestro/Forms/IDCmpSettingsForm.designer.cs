namespace IRMaestro
{
    partial class IDCmpSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IDCmpSettingsForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.capDirBrwbutton = new System.Windows.Forms.Button();
            this.capsigpathtextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ProjectcomboBox = new System.Windows.Forms.ComboBox();
            this.UDIDbSelCombobox = new System.Windows.Forms.ComboBox();
            this.ueidbradioButton = new System.Windows.Forms.RadioButton();
            this.pickdbradiobtn = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.outputPathBrwBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.outputpathTxtbox = new System.Windows.Forms.TextBox();
            this.projectnameTxtBox = new System.Windows.Forms.TextBox();
            this.ExcelRepoRadioBtn = new System.Windows.Forms.RadioButton();
            this.TxtRepoRadioBtn = new System.Windows.Forms.RadioButton();
            this.cancelButton = new System.Windows.Forms.Button();
            this.Ok = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.optionsButton = new System.Windows.Forms.Button();
            this.WorkDirText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.wDirBrwbutton = new System.Windows.Forms.Button();
            this.KeepCfischeckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.capDirBrwbutton);
            this.groupBox1.Controls.Add(this.capsigpathtextBox);
            this.groupBox1.Location = new System.Drawing.Point(23, 75);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(424, 81);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Capture File Directory";
            // 
            // capDirBrwbutton
            // 
            this.capDirBrwbutton.Location = new System.Drawing.Point(328, 33);
            this.capDirBrwbutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.capDirBrwbutton.Name = "capDirBrwbutton";
            this.capDirBrwbutton.Size = new System.Drawing.Size(77, 26);
            this.capDirBrwbutton.TabIndex = 3;
            this.capDirBrwbutton.Text = "Browse";
            this.capDirBrwbutton.UseVisualStyleBackColor = true;
            this.capDirBrwbutton.Click += new System.EventHandler(this.capDirBrwbutton_Click);
            // 
            // capsigpathtextBox
            // 
            this.capsigpathtextBox.Location = new System.Drawing.Point(27, 34);
            this.capsigpathtextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.capsigpathtextBox.Name = "capsigpathtextBox";
            this.capsigpathtextBox.Size = new System.Drawing.Size(288, 22);
            this.capsigpathtextBox.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ProjectcomboBox);
            this.groupBox2.Controls.Add(this.UDIDbSelCombobox);
            this.groupBox2.Controls.Add(this.ueidbradioButton);
            this.groupBox2.Controls.Add(this.pickdbradiobtn);
            this.groupBox2.Location = new System.Drawing.Point(23, 178);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(424, 114);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Database";
            // 
            // ProjectcomboBox
            // 
            this.ProjectcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ProjectcomboBox.FormattingEnabled = true;
            this.ProjectcomboBox.Location = new System.Drawing.Point(119, 68);
            this.ProjectcomboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ProjectcomboBox.Name = "ProjectcomboBox";
            this.ProjectcomboBox.Size = new System.Drawing.Size(280, 24);
            this.ProjectcomboBox.TabIndex = 6;
            this.ProjectcomboBox.SelectedIndexChanged += new System.EventHandler(this.ProjectcomboBox_SelectedIndexChanged);
            // 
            // UDIDbSelCombobox
            // 
            this.UDIDbSelCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.UDIDbSelCombobox.FormattingEnabled = true;
            this.UDIDbSelCombobox.Location = new System.Drawing.Point(119, 26);
            this.UDIDbSelCombobox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.UDIDbSelCombobox.Name = "UDIDbSelCombobox";
            this.UDIDbSelCombobox.Size = new System.Drawing.Size(280, 24);
            this.UDIDbSelCombobox.TabIndex = 2;
            // 
            // ueidbradioButton
            // 
            this.ueidbradioButton.AutoSize = true;
            this.ueidbradioButton.Checked = true;
            this.ueidbradioButton.Location = new System.Drawing.Point(29, 27);
            this.ueidbradioButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ueidbradioButton.Name = "ueidbradioButton";
            this.ueidbradioButton.Size = new System.Drawing.Size(74, 21);
            this.ueidbradioButton.TabIndex = 0;
            this.ueidbradioButton.TabStop = true;
            this.ueidbradioButton.Text = "UEI DB";
            this.ueidbradioButton.UseVisualStyleBackColor = true;
            this.ueidbradioButton.CheckedChanged += new System.EventHandler(this.ueidbradioButton_CheckedChanged);
            // 
            // pickdbradiobtn
            // 
            this.pickdbradiobtn.AutoSize = true;
            this.pickdbradiobtn.Location = new System.Drawing.Point(29, 68);
            this.pickdbradiobtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pickdbradiobtn.Name = "pickdbradiobtn";
            this.pickdbradiobtn.Size = new System.Drawing.Size(74, 21);
            this.pickdbradiobtn.TabIndex = 1;
            this.pickdbradiobtn.Text = "PickDB";
            this.pickdbradiobtn.UseVisualStyleBackColor = true;
            this.pickdbradiobtn.CheckedChanged += new System.EventHandler(this.pickdbradiobtn_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.outputPathBrwBtn);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.outputpathTxtbox);
            this.groupBox3.Controls.Add(this.projectnameTxtBox);
            this.groupBox3.Controls.Add(this.ExcelRepoRadioBtn);
            this.groupBox3.Controls.Add(this.TxtRepoRadioBtn);
            this.groupBox3.Location = new System.Drawing.Point(23, 300);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(424, 114);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Report";
            // 
            // outputPathBrwBtn
            // 
            this.outputPathBrwBtn.Location = new System.Drawing.Point(340, 52);
            this.outputPathBrwBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.outputPathBrwBtn.Name = "outputPathBrwBtn";
            this.outputPathBrwBtn.Size = new System.Drawing.Size(80, 28);
            this.outputPathBrwBtn.TabIndex = 5;
            this.outputPathBrwBtn.Text = "Browse";
            this.outputPathBrwBtn.UseVisualStyleBackColor = true;
            this.outputPathBrwBtn.Click += new System.EventHandler(this.button6_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 85);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Project Name (Optional)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 55);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Output Path (Optional)";
            // 
            // outputpathTxtbox
            // 
            this.outputpathTxtbox.Location = new System.Drawing.Point(172, 52);
            this.outputpathTxtbox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.outputpathTxtbox.Name = "outputpathTxtbox";
            this.outputpathTxtbox.Size = new System.Drawing.Size(157, 22);
            this.outputpathTxtbox.TabIndex = 3;
            // 
            // projectnameTxtBox
            // 
            this.projectnameTxtBox.Location = new System.Drawing.Point(172, 82);
            this.projectnameTxtBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.projectnameTxtBox.Name = "projectnameTxtBox";
            this.projectnameTxtBox.Size = new System.Drawing.Size(157, 22);
            this.projectnameTxtBox.TabIndex = 2;
            this.projectnameTxtBox.Text = "Untitled";
            // 
            // ExcelRepoRadioBtn
            // 
            this.ExcelRepoRadioBtn.AutoSize = true;
            this.ExcelRepoRadioBtn.Enabled = false;
            this.ExcelRepoRadioBtn.Location = new System.Drawing.Point(217, 21);
            this.ExcelRepoRadioBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ExcelRepoRadioBtn.Name = "ExcelRepoRadioBtn";
            this.ExcelRepoRadioBtn.Size = new System.Drawing.Size(62, 21);
            this.ExcelRepoRadioBtn.TabIndex = 1;
            this.ExcelRepoRadioBtn.Text = "Excel";
            this.ExcelRepoRadioBtn.UseVisualStyleBackColor = true;
            // 
            // TxtRepoRadioBtn
            // 
            this.TxtRepoRadioBtn.AutoSize = true;
            this.TxtRepoRadioBtn.Checked = true;
            this.TxtRepoRadioBtn.Location = new System.Drawing.Point(67, 21);
            this.TxtRepoRadioBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtRepoRadioBtn.Name = "TxtRepoRadioBtn";
            this.TxtRepoRadioBtn.Size = new System.Drawing.Size(56, 21);
            this.TxtRepoRadioBtn.TabIndex = 0;
            this.TxtRepoRadioBtn.TabStop = true;
            this.TxtRepoRadioBtn.Text = "Text";
            this.TxtRepoRadioBtn.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(353, 480);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(100, 28);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // Ok
            // 
            this.Ok.Location = new System.Drawing.Point(245, 480);
            this.Ok.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Ok.Name = "Ok";
            this.Ok.Size = new System.Drawing.Size(100, 28);
            this.Ok.TabIndex = 2;
            this.Ok.Text = "Ok";
            this.Ok.UseVisualStyleBackColor = true;
            this.Ok.Click += new System.EventHandler(this.Ok_Click);
            // 
            // optionsButton
            // 
            this.optionsButton.Location = new System.Drawing.Point(291, 436);
            this.optionsButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.optionsButton.Name = "optionsButton";
            this.optionsButton.Size = new System.Drawing.Size(156, 30);
            this.optionsButton.TabIndex = 3;
            this.optionsButton.Text = "&Options";
            this.optionsButton.UseVisualStyleBackColor = true;
            this.optionsButton.Click += new System.EventHandler(this.button4_Click);
            // 
            // WorkDirText
            // 
            this.WorkDirText.Location = new System.Drawing.Point(155, 31);
            this.WorkDirText.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.WorkDirText.Name = "WorkDirText";
            this.WorkDirText.Size = new System.Drawing.Size(180, 22);
            this.WorkDirText.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 34);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Working Directory";
            // 
            // wDirBrwbutton
            // 
            this.wDirBrwbutton.Location = new System.Drawing.Point(345, 31);
            this.wDirBrwbutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.wDirBrwbutton.Name = "wDirBrwbutton";
            this.wDirBrwbutton.Size = new System.Drawing.Size(77, 26);
            this.wDirBrwbutton.TabIndex = 6;
            this.wDirBrwbutton.Text = "Browse";
            this.wDirBrwbutton.UseVisualStyleBackColor = true;
            this.wDirBrwbutton.Click += new System.EventHandler(this.wDirBrwbutton_Click);
            // 
            // KeepCfischeckBox
            // 
            this.KeepCfischeckBox.AutoSize = true;
            this.KeepCfischeckBox.Checked = true;
            this.KeepCfischeckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.KeepCfischeckBox.Location = new System.Drawing.Point(43, 444);
            this.KeepCfischeckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.KeepCfischeckBox.Name = "KeepCfischeckBox";
            this.KeepCfischeckBox.Size = new System.Drawing.Size(116, 21);
            this.KeepCfischeckBox.TabIndex = 7;
            this.KeepCfischeckBox.Text = "Keep CFI files";
            this.KeepCfischeckBox.UseVisualStyleBackColor = true;
            // 
            // IDCmpSettingsForm
            // 
            this.AcceptButton = this.Ok;
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(472, 523);
            this.Controls.Add(this.KeepCfischeckBox);
            this.Controls.Add(this.wDirBrwbutton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.WorkDirText);
            this.Controls.Add(this.optionsButton);
            this.Controls.Add(this.Ok);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "IDCmpSettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ID Compare Settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button capDirBrwbutton;
        private System.Windows.Forms.TextBox capsigpathtextBox;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button Ok;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ComboBox UDIDbSelCombobox;
        private System.Windows.Forms.RadioButton ueidbradioButton;
        private System.Windows.Forms.RadioButton pickdbradiobtn;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox outputpathTxtbox;
        private System.Windows.Forms.TextBox projectnameTxtBox;
        private System.Windows.Forms.RadioButton ExcelRepoRadioBtn;
        private System.Windows.Forms.RadioButton TxtRepoRadioBtn;
        private System.Windows.Forms.Button optionsButton;
        private System.Windows.Forms.TextBox WorkDirText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button wDirBrwbutton;
        private System.Windows.Forms.Button outputPathBrwBtn;
        private System.Windows.Forms.CheckBox KeepCfischeckBox;
        private System.Windows.Forms.ComboBox ProjectcomboBox;
    }
}