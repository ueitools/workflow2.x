using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using BusinessObject;
using CommonForms;
using IRMaestro.Lib;
using ToolsData;
using UEI.Legacy;

namespace IRMaestro
{
    public partial class IDForm : Form
    {
        private readonly string _pickPath;
        private readonly string _projectName;
        private readonly string _readBackPath;
        private readonly Readback.PickSource _staticPickSource;
        private readonly string _workingPath;
        private bool _rbFiComparision;

        public IDForm(string pickPath, string readBackPath,
                      string workingPath, Readback.PickSource staticPickSource)
        {
            InitializeComponent();

            _pickPath = pickPath;
            _readBackPath = readBackPath;
            _workingPath = workingPath;

            if (!_pickPath.EndsWith("\\"))
                _pickPath += "\\";

            if (!_readBackPath.EndsWith("\\"))
                _readBackPath += "\\";

            if (!_workingPath.EndsWith("\\"))
                _workingPath += "\\";

            _staticPickSource = staticPickSource;
            _projectName = "";

            FunctionList.InitIntronTable();
        }

        public IDForm(string readBackPath, string workingPath,
                      Readback.PickSource staticPickSource, string projectName)
        {
            InitializeComponent();

            _workingPath = workingPath;
            _readBackPath = readBackPath;

            if (!_readBackPath.EndsWith("\\"))
                _readBackPath += "\\";

            if (!_workingPath.EndsWith("\\"))
                _workingPath += "\\";

            _pickPath = "";
            _staticPickSource = staticPickSource;
            _projectName = projectName;

            FunctionList.InitIntronTable();
        }

        public void SetRbFiComparision(bool bVal)
        {
            _rbFiComparision = bVal;
        }

        private void CancelBttn_Click(object sender, EventArgs e)
        {  
            this.Cursor = Cursors.WaitCursor;           
            ReadBackBGWorker.CancelAsync();
            Close();
        }

        private void OkBttn_Click(object sender, EventArgs e)
        {
            if (ReadBackBGWorker.IsBusy)
                return;

            OkBttn.Focus();
            
            Environment.CurrentDirectory = Application.StartupPath;

            Id.List idList = GetIdList();
            if (idList.Count > 0)
            {
                idFilterConditionsControl.Enabled = false;    
               ReadBackBGWorker.RunWorkerAsync();
            }
            else
            {
                MessageBox.Show("Filter settings resulted in no IDs to run.  Please adjust settings.");
            }
        }

        private Id.List GetIdList()
        {
            Id.List idList = new Id.List();
            switch (_staticPickSource)
            {
                case Readback.PickSource.StaticXML:
                    string[] xmlFiles = Directory.GetFiles(_pickPath, "*.XML");
                    foreach (string xmlFile in xmlFiles)
                    {
                        string idName = Path.GetFileNameWithoutExtension(xmlFile);
                        if (Id.ValidateIdFormat(idName) && idFilterConditionsControl.FilterConditions.IsInRange(idName))
                        {
                            idList.Add(new Id(idName));
                        }
                    }
                    break;
                case Readback.PickSource.StaticDB:
                    SQLPickReader.SQLPickReader sqlPickReader = new SQLPickReader.SQLPickReader();
                    sqlPickReader.PickDbName = _projectName;
                    idList = sqlPickReader.GetAllIDList(idFilterConditionsControl.FilterConditions);
                    break;
                case Readback.PickSource.Live:
                    string currentConnection = DAOFactory.GetDBConnectionString();
                    DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);

                    List<string> idNames = new List<string>(DBFunctions.GetAllIDList());
                    foreach (string idName in idNames)
                    {
                        if (Id.ValidateIdFormat(idName) && idFilterConditionsControl.FilterConditions.IsInRange(idName))
                        {
                            idList.Add(new Id(idName));
                        }
                    }

                    DAOFactory.ResetDBConnection(currentConnection);
                    break;
            }

                return idList;
        }

        private void ReadBackBGWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            Id.List idList = GetIdList();

            Readback readback = new Readback(_readBackPath, _workingPath, _staticPickSource,
                                             _pickPath, _projectName, _rbFiComparision);
            bool errorLogged;
            List<CompareRes> results;
            readback.RunReadback(idList, out results, out errorLogged);

            if (errorLogged)
            {
                MessageBox.Show(
                    "Errors have occured during run, please check the error log for more information");
            }
            
            CompareReportForm resultsForm = new CompareReportForm(_workingPath, results);
            resultsForm.ShowDialog();

        }

        private void ReadBackBGWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        private void ReadBackBGWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            idFilterConditionsControl.Enabled = true;
            bool bKeepCFI = Convert.ToBoolean(UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.KEEPCFI, 0));
            if (_rbFiComparision)
                bKeepCFI = Convert.ToBoolean(UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.KEEPFI, 0));
            if (!bKeepCFI)
            {
                string strRBorTI = string.Empty;
                if (_rbFiComparision)
                    strRBorTI = "FI";
                else
                    strRBorTI = "CFI";
                DialogResult dlgres = MessageBox.Show("Do you want to delete " + strRBorTI + " files?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlgres == DialogResult.Yes)
                {
                    Id.List idList = GetIdList();
                    foreach (string idString in idList)
                    {
                        try
                        {
                            Directory.Delete(_workingPath + idString, true);
                        }
                        catch
                        { }
                    }
                }
            }
            Close();
        }
    }
}