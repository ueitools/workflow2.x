namespace IRMaestro.Forms
{
    partial class frm_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Settings));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ProjectcomboBox = new System.Windows.Forms.ComboBox();
            this.cmbUEIDB = new System.Windows.Forms.ComboBox();
            this.lblUEIDb = new System.Windows.Forms.Label();
            this.rbPickDB = new System.Windows.Forms.RadioButton();
            this.rbUEIdb = new System.Windows.Forms.RadioButton();
            this.btnCancelDB = new System.Windows.Forms.Button();
            this.btnSetDb = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtPodNo = new System.Windows.Forms.TextBox();
            this.lblPodNo = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.USBFieldCaptureDevicesComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.ProjectcomboBox);
            this.groupBox1.Controls.Add(this.cmbUEIDB);
            this.groupBox1.Controls.Add(this.lblUEIDb);
            this.groupBox1.Controls.Add(this.rbPickDB);
            this.groupBox1.Controls.Add(this.rbUEIdb);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(27, 21);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(557, 181);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DB Settings";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 121);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Pick Project";
            // 
            // ProjectcomboBox
            // 
            this.ProjectcomboBox.FormattingEnabled = true;
            this.ProjectcomboBox.Location = new System.Drawing.Point(145, 117);
            this.ProjectcomboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ProjectcomboBox.Name = "ProjectcomboBox";
            this.ProjectcomboBox.Size = new System.Drawing.Size(349, 25);
            this.ProjectcomboBox.TabIndex = 5;
            this.ProjectcomboBox.SelectedIndexChanged += new System.EventHandler(this.ProjectcomboBox_SelectedIndexChanged);
            // 
            // cmbUEIDB
            // 
            this.cmbUEIDB.Enabled = false;
            this.cmbUEIDB.FormattingEnabled = true;
            this.cmbUEIDB.Location = new System.Drawing.Point(111, 71);
            this.cmbUEIDB.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbUEIDB.Name = "cmbUEIDB";
            this.cmbUEIDB.Size = new System.Drawing.Size(384, 25);
            this.cmbUEIDB.TabIndex = 3;
            // 
            // lblUEIDb
            // 
            this.lblUEIDb.AutoSize = true;
            this.lblUEIDb.Location = new System.Drawing.Point(23, 71);
            this.lblUEIDb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUEIDb.Name = "lblUEIDb";
            this.lblUEIDb.Size = new System.Drawing.Size(53, 17);
            this.lblUEIDb.TabIndex = 2;
            this.lblUEIDb.Text = "UEI DB";
            // 
            // rbPickDB
            // 
            this.rbPickDB.AutoSize = true;
            this.rbPickDB.Location = new System.Drawing.Point(348, 23);
            this.rbPickDB.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbPickDB.Name = "rbPickDB";
            this.rbPickDB.Size = new System.Drawing.Size(78, 21);
            this.rbPickDB.TabIndex = 1;
            this.rbPickDB.Text = "Pick DB";
            this.rbPickDB.UseVisualStyleBackColor = true;
            this.rbPickDB.CheckedChanged += new System.EventHandler(this.rbPickDB_CheckedChanged);
            // 
            // rbUEIdb
            // 
            this.rbUEIdb.AutoSize = true;
            this.rbUEIdb.Checked = true;
            this.rbUEIdb.Location = new System.Drawing.Point(111, 23);
            this.rbUEIdb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbUEIdb.Name = "rbUEIdb";
            this.rbUEIdb.Size = new System.Drawing.Size(74, 21);
            this.rbUEIdb.TabIndex = 0;
            this.rbUEIdb.TabStop = true;
            this.rbUEIdb.Text = "UEI DB";
            this.rbUEIdb.UseVisualStyleBackColor = true;
            this.rbUEIdb.CheckedChanged += new System.EventHandler(this.rbUEIdb_CheckedChanged);
            // 
            // btnCancelDB
            // 
            this.btnCancelDB.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelDB.Location = new System.Drawing.Point(317, 415);
            this.btnCancelDB.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancelDB.Name = "btnCancelDB";
            this.btnCancelDB.Size = new System.Drawing.Size(100, 28);
            this.btnCancelDB.TabIndex = 4;
            this.btnCancelDB.Text = "Cancel";
            this.btnCancelDB.UseVisualStyleBackColor = true;
            this.btnCancelDB.Click += new System.EventHandler(this.btnCancelDB_Click);
            // 
            // btnSetDb
            // 
            this.btnSetDb.Location = new System.Drawing.Point(185, 415);
            this.btnSetDb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSetDb.Name = "btnSetDb";
            this.btnSetDb.Size = new System.Drawing.Size(100, 28);
            this.btnSetDb.TabIndex = 3;
            this.btnSetDb.Text = "Set";
            this.btnSetDb.UseVisualStyleBackColor = true;
            this.btnSetDb.Click += new System.EventHandler(this.btnSetDb_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtPodNo);
            this.groupBox2.Controls.Add(this.lblPodNo);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(27, 217);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(557, 100);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "USBee Pod Settings";
            // 
            // txtPodNo
            // 
            this.txtPodNo.Location = new System.Drawing.Point(173, 39);
            this.txtPodNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPodNo.Name = "txtPodNo";
            this.txtPodNo.Size = new System.Drawing.Size(145, 23);
            this.txtPodNo.TabIndex = 1;
            // 
            // lblPodNo
            // 
            this.lblPodNo.AutoSize = true;
            this.lblPodNo.Location = new System.Drawing.Point(23, 43);
            this.lblPodNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPodNo.Name = "lblPodNo";
            this.lblPodNo.Size = new System.Drawing.Size(87, 17);
            this.lblPodNo.TabIndex = 0;
            this.lblPodNo.Text = "Pod Number";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.USBFieldCaptureDevicesComboBox);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(29, 331);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(557, 68);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "USB Field Capture Settings";
            // 
            // USBFieldCaptureDevicesComboBox
            // 
            this.USBFieldCaptureDevicesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.USBFieldCaptureDevicesComboBox.FormattingEnabled = true;
            this.USBFieldCaptureDevicesComboBox.Location = new System.Drawing.Point(227, 26);
            this.USBFieldCaptureDevicesComboBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.USBFieldCaptureDevicesComboBox.Name = "USBFieldCaptureDevicesComboBox";
            this.USBFieldCaptureDevicesComboBox.Size = new System.Drawing.Size(313, 24);
            this.USBFieldCaptureDevicesComboBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(214, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Select USB Field Capture Device";
            // 
            // frm_Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(621, 460);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnCancelDB);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnSetDb);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frm_Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.frm_SelectPickDB_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbUEIDB;
        private System.Windows.Forms.Label lblUEIDb;
        private System.Windows.Forms.RadioButton rbPickDB;
        private System.Windows.Forms.RadioButton rbUEIdb;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCancelDB;
        private System.Windows.Forms.Button btnSetDb;
        private System.Windows.Forms.TextBox txtPodNo;
        private System.Windows.Forms.Label lblPodNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ProjectcomboBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox USBFieldCaptureDevicesComboBox;
        private System.Windows.Forms.Label label2;
    }
}