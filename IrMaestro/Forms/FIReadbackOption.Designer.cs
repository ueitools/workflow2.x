namespace IRMaestro.Forms
{
    partial class FIReadbackOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblDutyCycle = new System.Windows.Forms.Label();
            this.lblCodeGap = new System.Windows.Forms.Label();
            this.lblFrequency = new System.Windows.Forms.Label();
            this.lblDelayTolerance = new System.Windows.Forms.Label();
            this.lblPulseTolerance = new System.Windows.Forms.Label();
            this.txtDutyCycle = new System.Windows.Forms.TextBox();
            this.txtCodeGap = new System.Windows.Forms.TextBox();
            this.txtFrequency = new System.Windows.Forms.TextBox();
            this.txtDelayDuration = new System.Windows.Forms.TextBox();
            this.txtPulseDuration = new System.Windows.Forms.TextBox();
            this.chkDutyCycle = new System.Windows.Forms.CheckBox();
            this.chkCodeGap = new System.Windows.Forms.CheckBox();
            this.chkFrequency = new System.Windows.Forms.CheckBox();
            this.chkDelayDuration = new System.Windows.Forms.CheckBox();
            this.chkPulseDuration = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblNPODelayDuration = new System.Windows.Forms.Label();
            this.txtNPODelayTolerance = new System.Windows.Forms.TextBox();
            this.lblNPOPulseDurationTolerance = new System.Windows.Forms.Label();
            this.txtNPONotCombinePulseDurationTolerance = new System.Windows.Forms.TextBox();
            this.lblCombinePulseDelay = new System.Windows.Forms.Label();
            this.chkNPO_DelayDuration = new System.Windows.Forms.CheckBox();
            this.txtCombinePulseDelay = new System.Windows.Forms.TextBox();
            this.chkNPOPulseDuration = new System.Windows.Forms.CheckBox();
            this.rbNPO_NotcombineDelay = new System.Windows.Forms.RadioButton();
            this.rbNPO_CombinePulseDelay = new System.Windows.Forms.RadioButton();
            this.chkKeepFI = new System.Windows.Forms.CheckBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.groupBox1.Controls.Add(this.lblDutyCycle);
            this.groupBox1.Controls.Add(this.lblCodeGap);
            this.groupBox1.Controls.Add(this.lblFrequency);
            this.groupBox1.Controls.Add(this.lblDelayTolerance);
            this.groupBox1.Controls.Add(this.lblPulseTolerance);
            this.groupBox1.Controls.Add(this.txtDutyCycle);
            this.groupBox1.Controls.Add(this.txtCodeGap);
            this.groupBox1.Controls.Add(this.txtFrequency);
            this.groupBox1.Controls.Add(this.txtDelayDuration);
            this.groupBox1.Controls.Add(this.txtPulseDuration);
            this.groupBox1.Controls.Add(this.chkDutyCycle);
            this.groupBox1.Controls.Add(this.chkCodeGap);
            this.groupBox1.Controls.Add(this.chkFrequency);
            this.groupBox1.Controls.Add(this.chkDelayDuration);
            this.groupBox1.Controls.Add(this.chkPulseDuration);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(252, 150);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // lblDutyCycle
            // 
            this.lblDutyCycle.AutoSize = true;
            this.lblDutyCycle.Location = new System.Drawing.Point(154, 130);
            this.lblDutyCycle.Name = "lblDutyCycle";
            this.lblDutyCycle.Size = new System.Drawing.Size(39, 13);
            this.lblDutyCycle.TabIndex = 14;
            this.lblDutyCycle.Text = "dev(%)";
            // 
            // lblCodeGap
            // 
            this.lblCodeGap.AutoSize = true;
            this.lblCodeGap.Location = new System.Drawing.Point(154, 104);
            this.lblCodeGap.Name = "lblCodeGap";
            this.lblCodeGap.Size = new System.Drawing.Size(39, 13);
            this.lblCodeGap.TabIndex = 13;
            this.lblCodeGap.Text = "dev(%)";
            // 
            // lblFrequency
            // 
            this.lblFrequency.AutoSize = true;
            this.lblFrequency.Location = new System.Drawing.Point(154, 78);
            this.lblFrequency.Name = "lblFrequency";
            this.lblFrequency.Size = new System.Drawing.Size(39, 13);
            this.lblFrequency.TabIndex = 12;
            this.lblFrequency.Text = "dev(%)";
            // 
            // lblDelayTolerance
            // 
            this.lblDelayTolerance.AutoSize = true;
            this.lblDelayTolerance.Location = new System.Drawing.Point(154, 52);
            this.lblDelayTolerance.Name = "lblDelayTolerance";
            this.lblDelayTolerance.Size = new System.Drawing.Size(39, 13);
            this.lblDelayTolerance.TabIndex = 11;
            this.lblDelayTolerance.Text = "dev(%)";
            // 
            // lblPulseTolerance
            // 
            this.lblPulseTolerance.AutoSize = true;
            this.lblPulseTolerance.Location = new System.Drawing.Point(154, 25);
            this.lblPulseTolerance.Name = "lblPulseTolerance";
            this.lblPulseTolerance.Size = new System.Drawing.Size(39, 13);
            this.lblPulseTolerance.TabIndex = 10;
            this.lblPulseTolerance.Text = "dev(%)";
            // 
            // txtDutyCycle
            // 
            this.txtDutyCycle.Location = new System.Drawing.Point(204, 123);
            this.txtDutyCycle.Name = "txtDutyCycle";
            this.txtDutyCycle.Size = new System.Drawing.Size(33, 20);
            this.txtDutyCycle.TabIndex = 9;
            // 
            // txtCodeGap
            // 
            this.txtCodeGap.Location = new System.Drawing.Point(204, 97);
            this.txtCodeGap.Name = "txtCodeGap";
            this.txtCodeGap.Size = new System.Drawing.Size(33, 20);
            this.txtCodeGap.TabIndex = 8;
            // 
            // txtFrequency
            // 
            this.txtFrequency.Location = new System.Drawing.Point(204, 71);
            this.txtFrequency.Name = "txtFrequency";
            this.txtFrequency.Size = new System.Drawing.Size(33, 20);
            this.txtFrequency.TabIndex = 7;
            // 
            // txtDelayDuration
            // 
            this.txtDelayDuration.Location = new System.Drawing.Point(204, 45);
            this.txtDelayDuration.Name = "txtDelayDuration";
            this.txtDelayDuration.Size = new System.Drawing.Size(33, 20);
            this.txtDelayDuration.TabIndex = 6;
            // 
            // txtPulseDuration
            // 
            this.txtPulseDuration.Location = new System.Drawing.Point(204, 19);
            this.txtPulseDuration.Name = "txtPulseDuration";
            this.txtPulseDuration.Size = new System.Drawing.Size(33, 20);
            this.txtPulseDuration.TabIndex = 5;
            // 
            // chkDutyCycle
            // 
            this.chkDutyCycle.AutoSize = true;
            this.chkDutyCycle.Location = new System.Drawing.Point(6, 123);
            this.chkDutyCycle.Name = "chkDutyCycle";
            this.chkDutyCycle.Size = new System.Drawing.Size(77, 17);
            this.chkDutyCycle.TabIndex = 4;
            this.chkDutyCycle.Text = "Duty Cycle";
            this.chkDutyCycle.UseVisualStyleBackColor = true;
            // 
            // chkCodeGap
            // 
            this.chkCodeGap.AutoSize = true;
            this.chkCodeGap.Location = new System.Drawing.Point(7, 97);
            this.chkCodeGap.Name = "chkCodeGap";
            this.chkCodeGap.Size = new System.Drawing.Size(74, 17);
            this.chkCodeGap.TabIndex = 3;
            this.chkCodeGap.Text = "Code Gap";
            this.chkCodeGap.UseVisualStyleBackColor = true;
            // 
            // chkFrequency
            // 
            this.chkFrequency.AutoSize = true;
            this.chkFrequency.Location = new System.Drawing.Point(7, 71);
            this.chkFrequency.Name = "chkFrequency";
            this.chkFrequency.Size = new System.Drawing.Size(76, 17);
            this.chkFrequency.TabIndex = 2;
            this.chkFrequency.Text = "Frequency";
            this.chkFrequency.UseVisualStyleBackColor = true;
            // 
            // chkDelayDuration
            // 
            this.chkDelayDuration.AutoSize = true;
            this.chkDelayDuration.Location = new System.Drawing.Point(7, 45);
            this.chkDelayDuration.Name = "chkDelayDuration";
            this.chkDelayDuration.Size = new System.Drawing.Size(96, 17);
            this.chkDelayDuration.TabIndex = 1;
            this.chkDelayDuration.Text = "Delay Duration";
            this.chkDelayDuration.UseVisualStyleBackColor = true;
            // 
            // chkPulseDuration
            // 
            this.chkPulseDuration.AutoSize = true;
            this.chkPulseDuration.Location = new System.Drawing.Point(7, 19);
            this.chkPulseDuration.Name = "chkPulseDuration";
            this.chkPulseDuration.Size = new System.Drawing.Size(95, 17);
            this.chkPulseDuration.TabIndex = 0;
            this.chkPulseDuration.Text = "Pulse Duration";
            this.chkPulseDuration.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblNPODelayDuration);
            this.groupBox2.Controls.Add(this.txtNPODelayTolerance);
            this.groupBox2.Controls.Add(this.lblNPOPulseDurationTolerance);
            this.groupBox2.Controls.Add(this.txtNPONotCombinePulseDurationTolerance);
            this.groupBox2.Controls.Add(this.lblCombinePulseDelay);
            this.groupBox2.Controls.Add(this.chkNPO_DelayDuration);
            this.groupBox2.Controls.Add(this.txtCombinePulseDelay);
            this.groupBox2.Controls.Add(this.chkNPOPulseDuration);
            this.groupBox2.Controls.Add(this.rbNPO_NotcombineDelay);
            this.groupBox2.Controls.Add(this.rbNPO_CombinePulseDelay);
            this.groupBox2.Location = new System.Drawing.Point(13, 169);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(252, 119);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "For Naked Pulse Only";
            // 
            // lblNPODelayDuration
            // 
            this.lblNPODelayDuration.AutoSize = true;
            this.lblNPODelayDuration.Location = new System.Drawing.Point(154, 95);
            this.lblNPODelayDuration.Name = "lblNPODelayDuration";
            this.lblNPODelayDuration.Size = new System.Drawing.Size(39, 13);
            this.lblNPODelayDuration.TabIndex = 20;
            this.lblNPODelayDuration.Text = "dev(%)";
            // 
            // txtNPODelayTolerance
            // 
            this.txtNPODelayTolerance.Location = new System.Drawing.Point(204, 89);
            this.txtNPODelayTolerance.Name = "txtNPODelayTolerance";
            this.txtNPODelayTolerance.Size = new System.Drawing.Size(33, 20);
            this.txtNPODelayTolerance.TabIndex = 19;
            // 
            // lblNPOPulseDurationTolerance
            // 
            this.lblNPOPulseDurationTolerance.AutoSize = true;
            this.lblNPOPulseDurationTolerance.Location = new System.Drawing.Point(154, 69);
            this.lblNPOPulseDurationTolerance.Name = "lblNPOPulseDurationTolerance";
            this.lblNPOPulseDurationTolerance.Size = new System.Drawing.Size(39, 13);
            this.lblNPOPulseDurationTolerance.TabIndex = 18;
            this.lblNPOPulseDurationTolerance.Text = "dev(%)";
            // 
            // txtNPONotCombinePulseDurationTolerance
            // 
            this.txtNPONotCombinePulseDurationTolerance.Location = new System.Drawing.Point(204, 63);
            this.txtNPONotCombinePulseDurationTolerance.Name = "txtNPONotCombinePulseDurationTolerance";
            this.txtNPONotCombinePulseDurationTolerance.Size = new System.Drawing.Size(33, 20);
            this.txtNPONotCombinePulseDurationTolerance.TabIndex = 17;
            // 
            // lblCombinePulseDelay
            // 
            this.lblCombinePulseDelay.AutoSize = true;
            this.lblCombinePulseDelay.Location = new System.Drawing.Point(154, 22);
            this.lblCombinePulseDelay.Name = "lblCombinePulseDelay";
            this.lblCombinePulseDelay.Size = new System.Drawing.Size(39, 13);
            this.lblCombinePulseDelay.TabIndex = 16;
            this.lblCombinePulseDelay.Text = "dev(%)";
            // 
            // chkNPO_DelayDuration
            // 
            this.chkNPO_DelayDuration.AutoSize = true;
            this.chkNPO_DelayDuration.Location = new System.Drawing.Point(25, 92);
            this.chkNPO_DelayDuration.Name = "chkNPO_DelayDuration";
            this.chkNPO_DelayDuration.Size = new System.Drawing.Size(96, 17);
            this.chkNPO_DelayDuration.TabIndex = 6;
            this.chkNPO_DelayDuration.Text = "Delay Duration";
            this.chkNPO_DelayDuration.UseVisualStyleBackColor = true;
            // 
            // txtCombinePulseDelay
            // 
            this.txtCombinePulseDelay.Location = new System.Drawing.Point(204, 17);
            this.txtCombinePulseDelay.Name = "txtCombinePulseDelay";
            this.txtCombinePulseDelay.Size = new System.Drawing.Size(33, 20);
            this.txtCombinePulseDelay.TabIndex = 15;
            // 
            // chkNPOPulseDuration
            // 
            this.chkNPOPulseDuration.AutoSize = true;
            this.chkNPOPulseDuration.Location = new System.Drawing.Point(25, 68);
            this.chkNPOPulseDuration.Name = "chkNPOPulseDuration";
            this.chkNPOPulseDuration.Size = new System.Drawing.Size(95, 17);
            this.chkNPOPulseDuration.TabIndex = 5;
            this.chkNPOPulseDuration.Text = "Pulse Duration";
            this.chkNPOPulseDuration.UseVisualStyleBackColor = true;
            // 
            // rbNPO_NotcombineDelay
            // 
            this.rbNPO_NotcombineDelay.AutoSize = true;
            this.rbNPO_NotcombineDelay.Location = new System.Drawing.Point(6, 43);
            this.rbNPO_NotcombineDelay.Name = "rbNPO_NotcombineDelay";
            this.rbNPO_NotcombineDelay.Size = new System.Drawing.Size(164, 17);
            this.rbNPO_NotcombineDelay.TabIndex = 1;
            this.rbNPO_NotcombineDelay.TabStop = true;
            this.rbNPO_NotcombineDelay.Text = "Do Not Combine Pulse/Delay";
            this.rbNPO_NotcombineDelay.UseVisualStyleBackColor = true;
            // 
            // rbNPO_CombinePulseDelay
            // 
            this.rbNPO_CombinePulseDelay.AutoSize = true;
            this.rbNPO_CombinePulseDelay.Location = new System.Drawing.Point(7, 20);
            this.rbNPO_CombinePulseDelay.Name = "rbNPO_CombinePulseDelay";
            this.rbNPO_CombinePulseDelay.Size = new System.Drawing.Size(127, 17);
            this.rbNPO_CombinePulseDelay.TabIndex = 0;
            this.rbNPO_CombinePulseDelay.TabStop = true;
            this.rbNPO_CombinePulseDelay.Text = "Combine Pulse/Delay";
            this.rbNPO_CombinePulseDelay.UseVisualStyleBackColor = true;
            // 
            // chkKeepFI
            // 
            this.chkKeepFI.AutoSize = true;
            this.chkKeepFI.Location = new System.Drawing.Point(19, 294);
            this.chkKeepFI.Name = "chkKeepFI";
            this.chkKeepFI.Size = new System.Drawing.Size(101, 17);
            this.chkKeepFI.TabIndex = 15;
            this.chkKeepFI.Text = "Keep All FI Files";
            this.chkKeepFI.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(20, 330);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 16;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(188, 330);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.TabStop = false;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FIReadbackOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 365);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.chkKeepFI);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FIReadbackOption";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "FI Readback Option";
            this.Load += new System.EventHandler(this.FIReadbackOption_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkKeepFI;
        private System.Windows.Forms.Label lblDutyCycle;
        private System.Windows.Forms.Label lblCodeGap;
        private System.Windows.Forms.Label lblFrequency;
        private System.Windows.Forms.Label lblDelayTolerance;
        private System.Windows.Forms.Label lblPulseTolerance;
        private System.Windows.Forms.TextBox txtDutyCycle;
        private System.Windows.Forms.TextBox txtCodeGap;
        private System.Windows.Forms.TextBox txtFrequency;
        private System.Windows.Forms.TextBox txtDelayDuration;
        private System.Windows.Forms.TextBox txtPulseDuration;
        private System.Windows.Forms.CheckBox chkDutyCycle;
        private System.Windows.Forms.CheckBox chkCodeGap;
        private System.Windows.Forms.CheckBox chkFrequency;
        private System.Windows.Forms.CheckBox chkDelayDuration;
        private System.Windows.Forms.CheckBox chkPulseDuration;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblNPODelayDuration;
        private System.Windows.Forms.TextBox txtNPODelayTolerance;
        private System.Windows.Forms.Label lblNPOPulseDurationTolerance;
        private System.Windows.Forms.TextBox txtNPONotCombinePulseDurationTolerance;
        private System.Windows.Forms.Label lblCombinePulseDelay;
        private System.Windows.Forms.CheckBox chkNPO_DelayDuration;
        private System.Windows.Forms.TextBox txtCombinePulseDelay;
        private System.Windows.Forms.CheckBox chkNPOPulseDuration;
        private System.Windows.Forms.RadioButton rbNPO_NotcombineDelay;
        private System.Windows.Forms.RadioButton rbNPO_CombinePulseDelay;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
    }
}