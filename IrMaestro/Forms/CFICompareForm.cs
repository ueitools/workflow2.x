using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IRMaestro
{
    public partial class CFICompareForm : Form
    {
        private string readBackPath;
        private string pickPath;

        public CFICompareForm(string readBackPath,
            string pickPath)
        {
            InitializeComponent();

            this.readBackPath = readBackPath;
            this.pickPath = pickPath;
        }

        private void CFICompareForm_Shown(object sender, EventArgs e)
        {
            lblReadback.Text = readBackPath;
            if (File.Exists(readBackPath))
            {
                ReadBackText.Text = File.ReadAllText(readBackPath);
            }
            else
                ReadBackText.Text = "No file found";


            lblPick.Text = pickPath;
            if (File.Exists(pickPath))
            {
                pickText.Text = File.ReadAllText(pickPath);
            }
            else
                pickText.Text = "No file found";


            lblReadback.Text += "     [Capture]";
            lblPick.Text += "     [Database]";
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CFICompareForm_Resize(object sender, EventArgs e)
        {
            Size size = this.Size;

            int x = (size.Width - 20) / 2;

            ReadBackText.Size = new Size(x, ReadBackText.Size.Height);
            lblReadback.Size = new Size(x, lblReadback.Size.Height);
            pickText.Size = new Size(x, pickText.Size.Height);
            lblPick.Size = new Size(x, lblPick.Size.Height);

            pickText.Location = new Point(x + 10, pickText.Location.Y);
            lblPick.Location = new Point(x + 10, lblPick.Location.Y);
            OkButton.Location = new Point(x - 28, OkButton.Location.Y);
        }

        private void printRptTSButton_Click(object sender, EventArgs e)
        {
            CompareReportForm.PrintReport(ReadBackText.Text);
            CompareReportForm.PrintReport(pickText.Text);
        }

        private void PageSetupTSButton_Click(object sender, EventArgs e)
        {
            CompareReportForm.PageSetup();
        }
    }
}