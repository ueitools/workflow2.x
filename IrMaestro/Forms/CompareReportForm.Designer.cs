namespace IRMaestro
{
    partial class CompareReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CompareReportForm));
            this.reportDataGrid = new System.Windows.Forms.DataGridView();
            this.Exec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Matched = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unmatched = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Warnings = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WarningMessages = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OkButton = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.printRptTSButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.PageSetupTSButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ShowOnlyUMButton = new System.Windows.Forms.ToolStripButton();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataGrid)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // reportDataGrid
            // 
            this.reportDataGrid.AllowUserToAddRows = false;
            this.reportDataGrid.AllowUserToDeleteRows = false;
            this.reportDataGrid.AllowUserToResizeColumns = false;
            this.reportDataGrid.AllowUserToResizeRows = false;
            this.reportDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reportDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.reportDataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.reportDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reportDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.reportDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Exec,
            this.ID,
            this.Matched,
            this.Unmatched,
            this.Warnings,
            this.WarningMessages});
            this.reportDataGrid.Location = new System.Drawing.Point(0, 31);
            this.reportDataGrid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.reportDataGrid.MultiSelect = false;
            this.reportDataGrid.Name = "reportDataGrid";
            this.reportDataGrid.ReadOnly = true;
            this.reportDataGrid.RowHeadersVisible = false;
            this.reportDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.reportDataGrid.Size = new System.Drawing.Size(907, 359);
            this.reportDataGrid.TabIndex = 0;
            this.reportDataGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.report_CellDoubleClick);
            this.reportDataGrid.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.reportDataGrid_ColumnHeaderMouseClick);
            // 
            // Exec
            // 
            this.Exec.HeaderText = "Exec";
            this.Exec.MinimumWidth = 40;
            this.Exec.Name = "Exec";
            this.Exec.ReadOnly = true;
            this.Exec.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 40;
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Matched
            // 
            this.Matched.HeaderText = "Matched Functions";
            this.Matched.MinimumWidth = 40;
            this.Matched.Name = "Matched";
            this.Matched.ReadOnly = true;
            this.Matched.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Unmatched
            // 
            this.Unmatched.HeaderText = "Unmatched Functions";
            this.Unmatched.MinimumWidth = 40;
            this.Unmatched.Name = "Unmatched";
            this.Unmatched.ReadOnly = true;
            this.Unmatched.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Warnings
            // 
            this.Warnings.HeaderText = "Warnings";
            this.Warnings.MinimumWidth = 40;
            this.Warnings.Name = "Warnings";
            this.Warnings.ReadOnly = true;
            this.Warnings.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // WarningMessages
            // 
            this.WarningMessages.HeaderText = "Warning Messages";
            this.WarningMessages.MinimumWidth = 300;
            this.WarningMessages.Name = "WarningMessages";
            this.WarningMessages.ReadOnly = true;
            this.WarningMessages.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // OkButton
            // 
            this.OkButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.OkButton.Location = new System.Drawing.Point(392, 410);
            this.OkButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(100, 28);
            this.OkButton.TabIndex = 1;
            this.OkButton.Text = "Ok";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printRptTSButton,
            this.toolStripSeparator5,
            this.PageSetupTSButton,
            this.toolStripSeparator1,
            this.ShowOnlyUMButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(904, 27);
            this.toolStrip1.TabIndex = 67;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // printRptTSButton
            // 
            this.printRptTSButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printRptTSButton.Image = ((System.Drawing.Image)(resources.GetObject("printRptTSButton.Image")));
            this.printRptTSButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printRptTSButton.Name = "printRptTSButton";
            this.printRptTSButton.Size = new System.Drawing.Size(23, 24);
            this.printRptTSButton.Text = "Print";
            this.printRptTSButton.Click += new System.EventHandler(this.printRptTSButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Margin = new System.Windows.Forms.Padding(11, 0, 11, 0);
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 27);
            // 
            // PageSetupTSButton
            // 
            this.PageSetupTSButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PageSetupTSButton.Image = ((System.Drawing.Image)(resources.GetObject("PageSetupTSButton.Image")));
            this.PageSetupTSButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PageSetupTSButton.Name = "PageSetupTSButton";
            this.PageSetupTSButton.Size = new System.Drawing.Size(23, 24);
            this.PageSetupTSButton.Text = "Page Setup";
            this.PageSetupTSButton.Click += new System.EventHandler(this.PageSetupTSButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.MergeIndex = 5;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // ShowOnlyUMButton
            // 
            this.ShowOnlyUMButton.BackColor = System.Drawing.SystemColors.Control;
            this.ShowOnlyUMButton.CheckOnClick = true;
            this.ShowOnlyUMButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ShowOnlyUMButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ShowOnlyUMButton.Image = ((System.Drawing.Image)(resources.GetObject("ShowOnlyUMButton.Image")));
            this.ShowOnlyUMButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ShowOnlyUMButton.Name = "ShowOnlyUMButton";
            this.ShowOnlyUMButton.Size = new System.Drawing.Size(163, 24);
            this.ShowOnlyUMButton.Text = "Show Only Unmatched";
            this.ShowOnlyUMButton.Click += new System.EventHandler(this.ShowOnlyUMButton_Click);
            // 
            // CompareReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.OkButton;
            this.ClientSize = new System.Drawing.Size(904, 468);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.reportDataGrid);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "CompareReportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Report";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CompareReportForm_FormClosing);
            this.Shown += new System.EventHandler(this.CompareReportForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.reportDataGrid)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView reportDataGrid;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton printRptTSButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton PageSetupTSButton;
        private System.Windows.Forms.ToolStripButton ShowOnlyUMButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Exec;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Matched;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unmatched;
        private System.Windows.Forms.DataGridViewTextBoxColumn Warnings;
        private System.Windows.Forms.DataGridViewTextBoxColumn WarningMessages;
    }
}