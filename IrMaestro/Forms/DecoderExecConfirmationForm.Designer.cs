namespace IRMaestro {
    partial class DecoderExecConfirmationForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DecoderExecConfirmationForm));
            this.CaptionLabel = new System.Windows.Forms.Label();
            this.UseCurrentExecRadioButton = new System.Windows.Forms.RadioButton();
            this.ChangeExecRadioButton = new System.Windows.Forms.RadioButton();
            this.ContinueButton = new System.Windows.Forms.Button();
            this.StopButton = new System.Windows.Forms.Button();
            this.ExecListComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // CaptionLabel
            // 
            this.CaptionLabel.AutoSize = true;
            this.CaptionLabel.Location = new System.Drawing.Point(16, 11);
            this.CaptionLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.CaptionLabel.Name = "CaptionLabel";
            this.CaptionLabel.Size = new System.Drawing.Size(227, 17);
            this.CaptionLabel.TabIndex = 0;
            this.CaptionLabel.Text = "Input cannot be identified by E###.";
            // 
            // UseCurrentExecRadioButton
            // 
            this.UseCurrentExecRadioButton.AutoSize = true;
            this.UseCurrentExecRadioButton.Location = new System.Drawing.Point(40, 54);
            this.UseCurrentExecRadioButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.UseCurrentExecRadioButton.Name = "UseCurrentExecRadioButton";
            this.UseCurrentExecRadioButton.Size = new System.Drawing.Size(142, 21);
            this.UseCurrentExecRadioButton.TabIndex = 1;
            this.UseCurrentExecRadioButton.TabStop = true;
            this.UseCurrentExecRadioButton.Text = "Use E### anyway";
            this.UseCurrentExecRadioButton.UseVisualStyleBackColor = true;
            this.UseCurrentExecRadioButton.CheckedChanged += new System.EventHandler(this.UseCurrentExecRadioButton_CheckedChanged);
            // 
            // ChangeExecRadioButton
            // 
            this.ChangeExecRadioButton.AutoSize = true;
            this.ChangeExecRadioButton.Location = new System.Drawing.Point(40, 87);
            this.ChangeExecRadioButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ChangeExecRadioButton.Name = "ChangeExecRadioButton";
            this.ChangeExecRadioButton.Size = new System.Drawing.Size(161, 21);
            this.ChangeExecRadioButton.TabIndex = 2;
            this.ChangeExecRadioButton.TabStop = true;
            this.ChangeExecRadioButton.Text = "Change Executor to :";
            this.ChangeExecRadioButton.UseVisualStyleBackColor = true;
            this.ChangeExecRadioButton.CheckedChanged += new System.EventHandler(this.ChangeExecRadioButton_CheckedChanged);
            // 
            // ContinueButton
            // 
            this.ContinueButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ContinueButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ContinueButton.Location = new System.Drawing.Point(84, 138);
            this.ContinueButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ContinueButton.Name = "ContinueButton";
            this.ContinueButton.Size = new System.Drawing.Size(100, 28);
            this.ContinueButton.TabIndex = 4;
            this.ContinueButton.Text = "Continue";
            this.ContinueButton.UseVisualStyleBackColor = true;
            this.ContinueButton.Click += new System.EventHandler(this.ContinueButton_Click);
            // 
            // StopButton
            // 
            this.StopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.StopButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.StopButton.Location = new System.Drawing.Point(192, 138);
            this.StopButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.StopButton.Name = "StopButton";
            this.StopButton.Size = new System.Drawing.Size(100, 28);
            this.StopButton.TabIndex = 5;
            this.StopButton.Text = "Stop";
            this.StopButton.UseVisualStyleBackColor = true;
            // 
            // ExecListComboBox
            // 
            this.ExecListComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ExecListComboBox.FormattingEnabled = true;
            this.ExecListComboBox.Location = new System.Drawing.Point(216, 87);
            this.ExecListComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ExecListComboBox.Name = "ExecListComboBox";
            this.ExecListComboBox.Size = new System.Drawing.Size(75, 24);
            this.ExecListComboBox.TabIndex = 3;
            // 
            // DecoderExecConfirmationForm
            // 
            this.AcceptButton = this.ContinueButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.StopButton;
            this.ClientSize = new System.Drawing.Size(308, 181);
            this.Controls.Add(this.ExecListComboBox);
            this.Controls.Add(this.StopButton);
            this.Controls.Add(this.ContinueButton);
            this.Controls.Add(this.ChangeExecRadioButton);
            this.Controls.Add(this.UseCurrentExecRadioButton);
            this.Controls.Add(this.CaptionLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "DecoderExecConfirmationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Executor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CaptionLabel;
        private System.Windows.Forms.RadioButton UseCurrentExecRadioButton;
        private System.Windows.Forms.RadioButton ChangeExecRadioButton;
        private System.Windows.Forms.Button ContinueButton;
        private System.Windows.Forms.Button StopButton;
        private System.Windows.Forms.ComboBox ExecListComboBox;
    }
}