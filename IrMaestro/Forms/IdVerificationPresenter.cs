//********************************************************************************
//REVISION HISTORY
//--------------------------------------------------------------------------------
//03/15/2010
//---------------------------------------------------------------------------------
//- Added pickdbfn, matchedfunctionlist, pickobject to support pickdb
//- modified OnIrCapturedEventHandler,View_OnIdSelectedEventHandler,
//  LoadId,PopulateAvailableFunctionList,GetIdList,PopulateMatchedFunctionList,
//  PopulateIdList(Modified from private to public) to support Pick DB
//
//03/24/2010
//---------------------------------------------------------------------------------
//- Added ResetStatusBar method in SetView, To switch the contents displayed in the 
//  status bar when the selected DB are switched
//- Added View_OnSettings method to handle the settings button
//- Added ResetEventhandlers to reset similar events when switched between 
//  different databases
//03/25/2010
//---------------------------------------------------------------------------------
//- Added SetDb Class that contains the DBType and Pick db settings specific to 
//  IDVerification
//*********************************************************************************
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using BusinessObject;
using CommonCode;
using CommonUI.Controls;
using UEI.Legacy;
using System.Collections;
using PAL = PickAccessLayer;
using SQLPickReader;

namespace IRMaestro
{
    public class IdVerificationPresenter : PresenterBase, ITestIDVerificationPresenter
    {
        private const string REG_SUBDIR = "ID Verify";
        private const string WORKING_SUBDIR = "IDV";
        //Added to support pick db
        PickDBFunctions pickdbfn;
        MatchedIdFunctionGroup matchedfunctionlist;
        PALObject palObject;
        SetDB setDb = new SetDB();
        private abstract class RegistryKey
        {
            public const string OneMHzCapture = "OneMHzCapture";
        }
        private IDbFunctionsWrapper _dbWrapper;
        private IIrCaptureHelper _irCaptureHelper;
        private IIdFunctionCompareHelper _idFunctionCompareHelper;
        private IFormFactory _formFactory;
        private ITempFilePath _tempFilePath;
        private IIrCaptureSet _irVerificationProject;
        public IdVerificationPresenter(IDbFunctionsWrapper dbWrapper)
        {
            DBSettings.idregpath = @"HKEY_CURRENT_USER\Software\UEITools\ID Verify";
            setDb.GetDBType(DBSettings.idregpath);
            setDb.GetPickDBPath(DBSettings.idregpath);
            if (setDb.selectedDBType == DBType.UEIDB)
                _dbWrapper = dbWrapper;
            _tempFilePath = new TempFilePath();
            _tempFilePath.SetSubDirectory(WORKING_SUBDIR);
            _irCaptureHelper = new IrCaptureHelper();
            _irCaptureHelper.UseStandardCapture();
            _irCaptureHelper.SetFilePathManager(_tempFilePath);
            _irCaptureHelper.OnIrCaptured += OnIrCapturedEventHandler;
            _idFunctionCompareHelper = new IdFunctionCompareHelper();
            _idFunctionCompareHelper.SetFilePathManager(_tempFilePath);
            _formFactory = FormFactory.GetInstance();
            _irVerificationProject = new IrCaptureSet();
            _irVerificationProject.IrCaptureSetType = IrCaptureSetType.VR;
            //Added to handle result functions from pickDB
            matchedfunctionlist = new MatchedIdFunctionGroup();
        }
        
        private IIdVerificationView View
        {
            get { return (IIdVerificationView)_view; }
        }
        public override void SetView(IView view)
        {
            base.SetView(view);
                        
            if (setDb.selectedDBType == DBType.UEIDB)
                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
                        
            View.OnIdSelected += View_OnIdSelectedEventHandler;
            View.OnStartCapture += View_OnStartCaptureEventHandler;
            View.OnResetId += View_OnResetIdEventHandler;
            View.OnUsbCapture1MHzChanged += View_OnUsbCapture1MHzChangedEventHandler;
            View.OnSave += View_OnSaveEventHandler;
            //View.Closed += View_OnClosedEventHandler;
            //View.FormClosing += View_OnClosingEventHandler;
            View.OnFormClose += View_OnFormCloseEventhandler;
            View.OnCapturePodChanged += View_OnCapturePodChanged;
            
            View.OnSettings += View_OnSettingsEventHandler;
            ResetCaptureStations();
            View.ResetStatusbar();
            PopulateIdList();
        }
        private void ResetView()
        {
            setDb.GetDBType(DBSettings.idregpath);
            setDb.GetPickDBPath(DBSettings.idregpath);
            
            DialogResult dialogResult = SaveModified(false);

            if (setDb.selectedDBType == DBType.UEIDB)
                _dbWrapper = new DbFunctionsWrapper();

            _irVerificationProject = new IrCaptureSet();
            _irVerificationProject.IrCaptureSetType = IrCaptureSetType.VR;
            _tempFilePath.SetSubDirectory(WORKING_SUBDIR);
            View.EnableCapture(false);
            View.ResetStatusbar();
            View.ClearComboSelection();
            ResetCaptureStations();
            PopulateIdList();
        }
        private void ResetCaptureStations()
        {
            bool isUsBeeConnected = _irCaptureHelper.IsUSBeeConnected();
            bool hasUSBCaptureDevice = _irCaptureHelper.FindUSBCaptureDevice();
            bool bIsUSBFCDeviceConnected = _irCaptureHelper.FindUSBFieldCaptureDevice();

            _irCaptureHelper.CapturePod = IrCaptureHelper.CapturePodType.NonUSB;
            if (hasUSBCaptureDevice)
            {
                _irCaptureHelper.CapturePod = IrCaptureHelper.CapturePodType.NiUSB;
            }
            else if (isUsBeeConnected)
            {
                _irCaptureHelper.CapturePod = IrCaptureHelper.CapturePodType.USBee;
            }
            else if (bIsUSBFCDeviceConnected)
            {
                _irCaptureHelper.CapturePod = IrCaptureHelper.CapturePodType.FcUSB; //Field Capture Device
            }

            View.Usb1MHzCaptureMode = Configuration.GetConfigItem(REG_SUBDIR, RegistryKey.OneMHzCapture, false);
            View.Show1MHzCapture(hasUSBCaptureDevice);
            View.ClearCaptureStations();
            StringCollection availableCaptureStations = GetAvailableCaptureStations(hasUSBCaptureDevice, isUsBeeConnected, bIsUSBFCDeviceConnected);
            View.SetAvailableCaptureStations(availableCaptureStations);
            View.EnableCapturePodSelection(availableCaptureStations.Count > 1);
            View.SetCurrentCapturePod(_irCaptureHelper.CapturePod.Name);
        }

        private StringCollection GetAvailableCaptureStations(bool hasUSBCaptureDevice, bool isUsBeeConnected, bool bIsUSBFCDeviceConnected)
        {
            StringCollection availableCaptureStations = new StringCollection();
            foreach (IrCaptureHelper.CapturePodType podType in IrCaptureHelper.CapturePodType.Members)
            {
                if (podType == IrCaptureHelper.CapturePodType.USBee && isUsBeeConnected == false)
                {
                    continue;
                }
                if (podType == IrCaptureHelper.CapturePodType.NiUSB && hasUSBCaptureDevice == false)
                {
                    continue;
                }
                if (podType == IrCaptureHelper.CapturePodType.FcUSB && bIsUSBFCDeviceConnected == false)
                {
                    continue;
                }
                availableCaptureStations.Add(podType.Name);
            }
            return availableCaptureStations;
        }

        private void View_OnCapturePodChanged(string capturePod)
        {
            IrCaptureHelper.CapturePodType currentCapturePodType = _irCaptureHelper.CapturePod;
            IrCaptureHelper.CapturePodType capturePodType = IrCaptureHelper.CapturePodType.Members.Find(
                delegate(IrCaptureHelper.CapturePodType podType) { return podType.Name == capturePod; });
            bool errorSettingCapturePod = false;
            if (capturePodType == IrCaptureHelper.CapturePodType.USBee && _irCaptureHelper.IsUSBeeConnected() == false)
            {
                FormFactory.GetInstance().MessageBox(null, "USBee pod not connected.", "Error", MessageBoxButtons.OK);
                errorSettingCapturePod = true;
            }
            if (capturePodType == IrCaptureHelper.CapturePodType.NiUSB && _irCaptureHelper.FindUSBCaptureDevice() == false)
            {
                FormFactory.GetInstance().MessageBox(null, "NI USB capture station not available", "Error", MessageBoxButtons.OK);
                errorSettingCapturePod = true;
            }
            if (capturePodType == IrCaptureHelper.CapturePodType.FcUSB && _irCaptureHelper.FindUSBFieldCaptureDevice() == false)
            {
                FormFactory.GetInstance().MessageBox(null, "Filed Capture Device not connected", "Error", MessageBoxButtons.OK);
                errorSettingCapturePod = true;
            }

            if (errorSettingCapturePod)
            {
                View.SetCurrentCapturePod(currentCapturePodType.Name);
            }
            else
            {
                View.SetCurrentCapturePod(capturePod);
                //For NI USB capture && Field Capture USB 1MHZ capture should be enabled
                View.EnableUsbCapture1MHz((capturePodType == IrCaptureHelper.CapturePodType.NiUSB) || (capturePodType == IrCaptureHelper.CapturePodType.FcUSB));
                _irCaptureHelper.CapturePod = capturePodType;
            }
        }
        private void View_OnSettingsEventHandler(object sender, EventArgs e)
        {
            Forms.frm_Settings settings = new IRMaestro.Forms.frm_Settings(WORKING_SUBDIR);
                
            if(settings.ShowDialog()==DialogResult.OK)
                ResetView();
        }
        private void OnIrCapturedEventHandler(string capturePath)
        {
            switch (setDb.selectedDBType)
            {
                case DBType.UEIDB:
                    {
                        int compareResults = _idFunctionCompareHelper.SearchForMatchingFunction(capturePath);
                        if (compareResults > 0)
                        {
                            _irVerificationProject.AddFunctionCapture(_idFunctionCompareHelper.LastMatchedIdFunctionGroup.Label, capturePath, "");
                            PopulateAvailableFunctionList();
                            PopulateMatchedFunctionList();
                        }
                        break;
                    }
                case DBType.PickDB:
                    {
                         pickdbfn = new PickDBFunctions();
                        _idFunctionCompareHelper.LastMatchedIdFunctionGroup = pickdbfn.GetMatchedFunction(DBSettings.selectedId, capturePath);
                        if (_idFunctionCompareHelper.LastMatchedIdFunctionGroup != null)
                        {
                            _irVerificationProject.AddFunctionCapture(_idFunctionCompareHelper.LastMatchedIdFunctionGroup.Label, capturePath, "");
                                                        
                            PopulateAvailableFunctionList();
                            PopulateMatchedFunctionList();
                        }
                        break;
                    }
            }
        }
        
        //private void View_OnClosedEventHandler(object sender, EventArgs e)
        //{
        //    _tempFilePath.ClearFiles();
        //}
        //private void View_OnClosingEventHandler(object sender, FormClosingEventArgs e)
        //{
        //    if (SaveModified(false) == DialogResult.Cancel)
        //    {
        //        e.Cancel = true;
        //    }
        //}

        private void View_OnFormCloseEventhandler(Object sender, FormClosingEventArgs e)
        {
            if (SaveModified(false) == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
            else
            {
                _tempFilePath.ClearFiles();
                _tempFilePath.Dispose();
                _irCaptureHelper.Dispose();
                GC.SuppressFinalize(this);
            }
        }
        
        private void View_OnIdSelectedEventHandler(object sender, EventArgs e)
        {
            
            DialogResult dialogResult = SaveModified(false);
            switch (setDb.selectedDBType)
            {
                case DBType.UEIDB:
                    {
                        
                        if (dialogResult == DialogResult.Yes || dialogResult == DialogResult.No)
                        {
                            string selectedId = View.SelectedId;
                            if ((selectedId != string.Empty) ||(selectedId!=null) || (selectedId!=""))
                            {
                                LoadId(selectedId);
                                _irCaptureHelper.SetId(selectedId);
                            }
                        }
                        else
                        {
                            View.SelectedId = _idFunctionCompareHelper.Id;
                        }
                        break;
                    }
                case DBType.PickDB:
                    {
                        
                        if (dialogResult == DialogResult.Yes || dialogResult == DialogResult.No)
                        {
                            string selectedId = View.SelectedId;
                            DBSettings.selectedId = selectedId;
                            if (DBSettings.selectedId != "" || DBSettings.selectedId != string.Empty)
                            {
                                DBSettings.CreatePickWorkingPath();
                                //PickDBObject.id_cficollection = null;
                                PALObject.id_cficollection = null;
                               
                                palObject.LoadPickObjects(DBSettings.selectedId);
                                LoadId(selectedId);
                                _irCaptureHelper.SetId(selectedId);
                            }
                            
                        }
                        else
                        {
                            
                            View.SelectedId = _idFunctionCompareHelper.Id;
                        }
                        break;
                    }
            }
        }
        private void View_OnStartCaptureEventHandler(object sender, EventArgs e)
        {
            if (_idFunctionCompareHelper.IsReady())
            {
                if (_irCaptureHelper.CapturePod == IrCaptureHelper.CapturePodType.FcUSB)
                {
                    string[] strDeviceNames = _irCaptureHelper.GetUSBFieldCaptureDeviceList();

                    if (strDeviceNames.Length > 0)
                    {
                        _irCaptureHelper.UsbFcDevicename = strDeviceNames[0]; //Default Device
                        Properties.Settings.Default._SelectedUSBFieldCaptureDevice = _irCaptureHelper.UsbFcDevicename;

                        string strSelectedFCDevice = Properties.Settings.Default._SelectedUSBFieldCaptureDevice;
                        if (strSelectedFCDevice.Trim() != string.Empty)
                        {

                            bool bFound = false;
                            foreach (string strDeviceName in strDeviceNames)
                            {
                                if (strDeviceName == strSelectedFCDevice)
                                {
                                    _irCaptureHelper.UsbFcDevicename = strSelectedFCDevice;
                                    Properties.Settings.Default._SelectedUSBFieldCaptureDevice = _irCaptureHelper.UsbFcDevicename;

                                    bFound = true;
                                    break;
                                }
                            }

                        }
                    }
                    else
                    {
                        _irCaptureHelper.UsbFcDevicename = "";
                        Properties.Settings.Default._SelectedUSBFieldCaptureDevice = "";
                    }
                }

                string cdw = Environment.CurrentDirectory;
                Environment.CurrentDirectory = _tempFilePath.CurrentWorkingPath;
                View.EnableCapture(false);
                _irCaptureHelper.SetLocation(new Point(100, 100));
                _irCaptureHelper.CaptureContinuous("Capture");
                View.EnableCapture(true);
                Environment.CurrentDirectory = cdw;
                if (string.IsNullOrEmpty(_irCaptureHelper.LastErrorMessage) == false)
                {
                    _formFactory.MessageBox(null, _irCaptureHelper.LastErrorMessage, "Capture Error", MessageBoxButtons.OK);
                }
            }
        }

        private void View_OnResetIdEventHandler(object sender, EventArgs e)
        {
            DialogResult dialogResult = SaveModified(false);
            if (dialogResult == DialogResult.Yes || dialogResult == DialogResult.No)
            {
                LoadId(View.SelectedId);
            }
        }
        private void View_OnUsbCapture1MHzChangedEventHandler(object sender, EventArgs e)
        {
            bool capture1MHz = View.Usb1MHzCaptureMode;
            _irCaptureHelper.UsbCapture1MHz = capture1MHz;
            Configuration.SetConfigItem(REG_SUBDIR, RegistryKey.OneMHzCapture, capture1MHz);
        }
        private void View_OnSaveEventHandler(object sender, EventArgs e)
        {
            SaveModified(true);
        }
        private DialogResult SaveModified(bool forceSave)
        {
            DialogResult dialogResult = DialogResult.Yes;
            if (_irVerificationProject.IsModified)
            {
                if (forceSave == false)
                {
                    dialogResult = _formFactory.MessageBox(null, MessageStrings.SaveProjectRequest_Text,
                                                           MessageStrings.SaveProjectRequest_Caption,
                                                           MessageBoxButtons.YesNoCancel);
                }
                if (dialogResult == DialogResult.Yes)
                {
                    try
                    {
                        _irVerificationProject.SaveAsAt(Configuration.GetWorkingDirectory());
                        string message = string.Format("File save at {0}", _irVerificationProject.LastSavedLocation);
                        _formFactory.MessageBox(null, message, "Saved", MessageBoxButtons.OK);
                    }
                    catch (IOException e)
                    {
                        string message = string.Format("Error occured attempting to save.\n\n{0}", e.Message);
                        _formFactory.MessageBox(null, message, "Error saving...", MessageBoxButtons.OK);
                    }
                }
            }
            return dialogResult;
        }
        
        private void LoadId(string id)
        {
            View.EnableCapture(false);
            _idFunctionCompareHelper.LoadId(id, setDb.selectedDBType);
            _irVerificationProject.LoadId(id);
            PopulateAvailableFunctionList();
            PopulateMatchedFunctionList();
            View.EnableCapture(_idFunctionCompareHelper.IsReady());
         }
        private void PopulateAvailableFunctionList()
        {
            switch (setDb.selectedDBType)
            {
                case DBType.UEIDB:
                    {
                        if (_idFunctionCompareHelper.FunctionList == null)
                        {
                            return;
                        }
                        List<string> functionLabels = new List<string>();
                        foreach (IDFunction function in _idFunctionCompareHelper.FunctionList)
                        {
                            functionLabels.Add(function.Label);
                        }
                        View.SetAvailableFunctionList(new MvpDataContainers.ListData(functionLabels));
                        break;
                    }
                case DBType.PickDB:
                    {
                        
                        List<string> functionLabels = new List<string>();
                        if (((DBSettings.selectedId.CompareTo(""))!=0) || (DBSettings.selectedId != null) || (DBSettings.selectedId != string.Empty))
                        {
                            if (palObject != null)
                                functionLabels = palObject.pickKeyLabels;
                            if (functionLabels!=null)
                                if(functionLabels.Count>0)
                                    View.SetAvailableFunctionList(new MvpDataContainers.ListData(functionLabels));
                        }
                        break;
                    }
                }
        }
            
        private void PopulateMatchedFunctionList()
        {
            string[] functionLabelList = _irVerificationProject.FunctionLabelList;
            if (functionLabelList == null || functionLabelList.Length == 0)
            {
                View.SetMatchedFunctionList(new MvpDataContainers.ListData());
                return;
            }
            List<string> functionLabels = new List<string>();
            foreach (string label in functionLabelList)
            {
                functionLabels.Add(label);
            }
            View.SetMatchedFunctionList(new MvpDataContainers.ListData(functionLabels));
            View.SetMatchedFunctionSelectedIndex(functionLabelList.Length - 1);
                        
        }
        
        public void PopulateIdList()
        {
            StringCollection idList = GetIdList();
            List<string> stringList = new List<string>();
            foreach (string id in idList)
            {
                stringList.Add(id);
            }
            View.SetIdList(new MvpDataContainers.ListData(stringList));
            View.SelectedId = "";
        }
                
        private StringCollection GetIdList()
        {
            StringCollection stringCollection = new StringCollection();
            switch (setDb.selectedDBType)
            {
                case DBType.UEIDB:
                    {
                        if (_dbWrapper != null)
                        {
                            stringCollection = _dbWrapper.GetAllIDList();
                        }
                        break;
                    }
                case DBType.PickDB:
                    {
                        palObject = new PALObject(DBSettings.GetDBPath(DBSettings.idregpath, "SelectedPickProject"));
                        IList<string> idlist = palObject.GetAllIDs();
                            
                            StringCollection idcollection = new StringCollection();
                            foreach (string id in idlist)
                            {
                                idcollection.Add(id);
                            }
                            stringCollection = idcollection;
                        break;
                    }
            }
            return stringCollection;
        }
        void ITestIDVerificationPresenter.SetIrCaptureHelper(IIrCaptureHelper irCaptureHelper)
        {
            _irCaptureHelper = irCaptureHelper;
            _irCaptureHelper.OnIrCaptured += OnIrCapturedEventHandler;
        }
        void ITestIDVerificationPresenter.SetFunctionCompareHelper(IIdFunctionCompareHelper idFunctionCompareHelper)
        {
            _idFunctionCompareHelper = idFunctionCompareHelper;
        }
        void ITestIDVerificationPresenter.SetFormsLib(IFormFactory formFactory)
        {
            _formFactory = formFactory;
        }
        void ITestIDVerificationPresenter.SetFilePathManager(ITempFilePath filePathManager)
        {
            _tempFilePath = filePathManager;
            _irCaptureHelper.SetFilePathManager(filePathManager);
        }
        void ITestIDVerificationPresenter.SetIrVerificationProject(IIrCaptureSet irVerificationProject)
        {
            _irVerificationProject = irVerificationProject;
        }
        public abstract class MessageStrings
        {
            public const string SelectNewIdWarning_Text = "Do you want to discard current results?";
            public const string SelectNewIdWarning_Caption = "Select New ID...";
            public const string ResetIdWarning_Text = "Resetting will clear any previous results.";
            public const string ResetIdWarning_Caption = "Reset ID Results";
            public const string SaveProjectRequest_Text = "Would you like to save the capture results?";
            public const string SaveProjectRequest_Caption = "Save Capture...";
        }
    }
    public interface ITestIDVerificationPresenter
    {
        void SetIrCaptureHelper(IIrCaptureHelper irCaptureHelper);
        void SetFunctionCompareHelper(IIdFunctionCompareHelper idFunctionCompareHelper);
        void SetFormsLib(IFormFactory formFactory);
        void SetFilePathManager(ITempFilePath filePathManager);
        void SetIrVerificationProject(IIrCaptureSet irVerificationProject);
    }
    public delegate void CapturePodChangedDelegate(string capturePod);
    public interface IIdVerificationView : IView
    {
        event EventHandler OnIdSelected;
        event EventHandler OnStartCapture;
        event EventHandler OnResetId;
        event EventHandler OnUsbCapture1MHzChanged;
        event EventHandler OnSave;
        event CapturePodChangedDelegate OnCapturePodChanged;
        event EventHandler OnSettings;
        event FormClosingEventHandler OnFormClose;
       
        string SelectedId { get; set; }
        bool Usb1MHzCaptureMode { get; set; }
        void SetIdList(MvpDataContainers.ListData idList);
        void SetAvailableFunctionList(MvpDataContainers.ListData functionLabels);
        void EnableCapture(bool enabled);
        void EnableUsbCapture1MHz(bool enable);
        void EnableCapturePodSelection(bool enable);
        void SetMatchedFunctionList(MvpDataContainers.ListData functionLabels);
        void SetMatchedFunctionSelectedIndex(int index);
        void Show1MHzCapture(bool show);
        void ClearCaptureStations();
        void SetAvailableCaptureStations(IList<string> availableCapturePodList);
        void SetCurrentCapturePod(string capturePod);
        void ResetStatusbar();
        void ClearComboSelection();
    }
}