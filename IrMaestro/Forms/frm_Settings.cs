//********************************************************************************
//REVISION HISTORY
//--------------------------------------------------------------------------------
//03/15/2010
// Added this new class to set the pick DB Path
// 
//---------------------------------------------------------------------------------
//03/24/2010
//---------------------------------------------------------------------------------
//- Modified the name from frm_SelectPickDB to frm_Settings
//- Added options to switch between UEIDb and Pick DB
//- When Pick DB is selected activates the pick db text box
//- Also contains USBee pod settings, is activated only when USBee is selected
//*********************************************************************************
using System;
using System.ComponentModel;
using System.Windows.Forms;
using CommonCode;
//Start Add - PAL 
using PickAccessLayer;
using PickData;
using System.Collections;
using System.Collections.Generic;
//End Add - PAL

namespace IRMaestro.Forms
{
    public partial class frm_Settings : Form
    {
        //Start Add - PAL 
        PickAccessLayer.Pick2DAO pickdb = new Pick2DAO();
        IList<Hashtable> ProjNames = null;
        //End Add - PAL 

        public static int PodNo;
        bool conStatus = false;
        USBeeCapture _usbee = new USBeeCapture();
        private string regpath;
        DBType selecteddbtype;
        string pickdbpath;
        public frm_Settings()
        {
            InitializeComponent();
        }
        public frm_Settings(string toolname)
        {
            if (toolname == "IDV")
            {
                regpath = @"HKEY_CURRENT_USER\Software\UEITools\ID Verify";
            }
            else if(toolname=="MIDV")
            {
                regpath = @"HKEY_CURRENT_USER\Software\UEITools\Macro Analyzer";
            }
            InitializeComponent();
        }

        private void frmSettingsOnLoad()
        {
            string dbtype = DBSettings.GetDBType(regpath, "SelectedDBType");
            if(dbtype=="UEIDB")
                selecteddbtype = DBType.UEIDB;
            else
                selecteddbtype=DBType.PickDB;

            if (selecteddbtype == DBType.UEIDB)
            {
                rbPickDB.Checked = false;
                rbUEIdb.Checked = true;
                rbUEIdb_CheckedChanged(null, null);
            }
            else
            {
                rbPickDB.Checked = true;
                rbUEIdb.Checked = false;
                rbPickDB_CheckedChanged(null, null);
            }
            //Start Add - PAL 
            ProjectcomboBox.Text = DBSettings.GetDBPath(regpath, "SelectedPickProject");
            //End Add - PAL 
        }
        private void USBeeSettingsOnLoad()
        {
            txtPodNo.Text = _usbee.GetPodNo().ToString();
            lblPodNo.Enabled = true;
            txtPodNo.Enabled = true;
        }

        private void USBFieldCaptureSettingsOnLoad()
        {
            IIrCaptureHelper helper = new IrCaptureHelper();
            string[] strDeviceNames = helper.GetUSBFieldCaptureDeviceList();

            if (strDeviceNames.Length > 0)
            {
                USBFieldCaptureDevicesComboBox.Items.AddRange(strDeviceNames);

                string strSelectedFCDevice = Properties.Settings.Default._SelectedUSBFieldCaptureDevice;
                int iIndex = USBFieldCaptureDevicesComboBox.FindStringExact(strSelectedFCDevice);
                if (iIndex != -1)
                {
                    USBFieldCaptureDevicesComboBox.SelectedIndex = iIndex;
                }
                else
                {
                    USBFieldCaptureDevicesComboBox.SelectedIndex = 0; //First Device will be default;
                }
            }
        }

        private void frm_SelectPickDB_Load(object sender, EventArgs e)
        {
            USBFieldCaptureSettingsOnLoad();
            USBeeSettingsOnLoad();
            frmSettingsOnLoad();
         }
        private void btnCancelDB_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void rbPickDB_CheckedChanged(object sender, EventArgs e)
        {
            selecteddbtype = DBType.PickDB;
            ProjectcomboBox.Enabled = true;
                        
            //Start Add - PAL 
            ProjNames = PickAccessLayer.PickRetrieveFunctions.GetPickProjectNames();
            ProjectcomboBox.Items.Clear();
            foreach (Hashtable Projs in ProjNames)
            {
                ProjectcomboBox.Items.Add(Projs["Name"]);
            }
            if (ProjectcomboBox.Items != null)
                ProjectcomboBox.SelectedIndex = 0;
            //End Add - PAL 

        }
        private void ProjectcomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void rbUEIdb_CheckedChanged(object sender, EventArgs e)
        {
            selecteddbtype = DBType.UEIDB;
            ProjectcomboBox.Enabled = false;
          
        }
        private void btnSetDb_Click(object sender, EventArgs e)
        {
            bool pickDBNotEmpty = true;
            bool validPodNumber = true;
            if (selecteddbtype == DBType.UEIDB)
            {
                DBSettings.SetRegistry(regpath ,"SelectedDBType", "UEIDB"); 
            }
            else
            {
                DBSettings.SetRegistry(regpath, "SelectedDBType", "PICKDB");
                if (ProjectcomboBox.Text == "")
                {
                    pickDBNotEmpty = false;
                    MessageBox.Show("Pick DB path cannot be empty");
                }
                else
                {
                    //pickdbpath = txtSelectedPickDB.Text;
                    //DBSettings.SetRegistry(regpath, "SelectedPickDB", pickdbpath);
                    //Start Add - PAL 
                    DBSettings.SetRegistry(regpath, "SelectedPickProject", ProjectcomboBox.Text);
                    //End Add - PAL 
                }
                                
            }
            validPodNumber = SetUSBeePodNo();

            bool canClose = false;
            if(selecteddbtype==DBType.PickDB && DBSettings.PodType=="USBee" && pickDBNotEmpty==true && validPodNumber==true)
                canClose = true;
            else if (selecteddbtype == DBType.PickDB && DBSettings.PodType != "USBee" && pickDBNotEmpty == true)
                canClose = true;

            if (selecteddbtype == DBType.UEIDB && DBSettings.PodType == "USBee" && validPodNumber == true)
                canClose = true;
            else if (selecteddbtype == DBType.UEIDB && DBSettings.PodType != "USBee" )
                canClose = true;


            //USB Field Capture Device settings
            if (USBFieldCaptureDevicesComboBox.Items.Count > 0)
                Properties.Settings.Default._SelectedUSBFieldCaptureDevice = USBFieldCaptureDevicesComboBox.Text;

            if (canClose)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();                
            }
        }
        private bool SetUSBeePodNo()
        {
            if (txtPodNo.Text == "" || txtPodNo.Text == string.Empty)
                conStatus = false;
            else
            {
                _usbee.SetPodNo(Convert.ToInt32(txtPodNo.Text));
                conStatus = _usbee.IsPodConnected();
            }
            if (conStatus != true && DBSettings.PodType=="USBee")
                MessageBox.Show("Invalid Pod Number or Pod not connected");
            return conStatus;
        }
        private void txtPodNo_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                PodNo = Convert.ToInt32(txtPodNo.Text);
                if ((PodNo > 32767) || (PodNo < 0))
                    MessageBox.Show("Enter values between 1 and 32767");
            }
            catch
            {
                MessageBox.Show("Enter numeric values between 1 and 32767");
            }
        }

       
        
    }
}