namespace IRMaestro
{
    partial class CFICompareForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CFICompareForm));
            this.OkButton = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.printRptTSButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.PageSetupTSButton = new System.Windows.Forms.ToolStripButton();
            this.lblReadback = new System.Windows.Forms.Label();
            this.lblPick = new System.Windows.Forms.Label();
            this.pickText = new IRMaestro.Forms.RichTextBoxEx();
            this.ReadBackText = new IRMaestro.Forms.RichTextBoxEx();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // OkButton
            // 
            this.OkButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.OkButton.Location = new System.Drawing.Point(230, 470);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(80, 22);
            this.OkButton.TabIndex = 4;
            this.OkButton.Text = "Ok";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printRptTSButton,
            this.toolStripSeparator5,
            this.PageSetupTSButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(543, 25);
            this.toolStrip1.TabIndex = 68;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // printRptTSButton
            // 
            this.printRptTSButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printRptTSButton.Image = ((System.Drawing.Image)(resources.GetObject("printRptTSButton.Image")));
            this.printRptTSButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printRptTSButton.Name = "printRptTSButton";
            this.printRptTSButton.Size = new System.Drawing.Size(23, 22);
            this.printRptTSButton.Text = "Print";
            this.printRptTSButton.Click += new System.EventHandler(this.printRptTSButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Margin = new System.Windows.Forms.Padding(11, 0, 11, 0);
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // PageSetupTSButton
            // 
            this.PageSetupTSButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PageSetupTSButton.Image = ((System.Drawing.Image)(resources.GetObject("PageSetupTSButton.Image")));
            this.PageSetupTSButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PageSetupTSButton.Name = "PageSetupTSButton";
            this.PageSetupTSButton.Size = new System.Drawing.Size(23, 22);
            this.PageSetupTSButton.Text = "Page Setup";
            this.PageSetupTSButton.Click += new System.EventHandler(this.PageSetupTSButton_Click);
            // 
            // lblReadback
            // 
            this.lblReadback.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblReadback.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReadback.Location = new System.Drawing.Point(0, 25);
            this.lblReadback.Name = "lblReadback";
            this.lblReadback.Size = new System.Drawing.Size(256, 30);
            this.lblReadback.TabIndex = 69;
            this.lblReadback.Text = "label1";
            this.lblReadback.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPick
            // 
            this.lblPick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPick.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPick.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPick.Location = new System.Drawing.Point(290, 25);
            this.lblPick.Name = "lblPick";
            this.lblPick.Size = new System.Drawing.Size(253, 30);
            this.lblPick.TabIndex = 70;
            this.lblPick.Text = "label2";
            this.lblPick.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pickText
            // 
            this.pickText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pickText.AutoScroll = true;
            this.pickText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pickText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pickText.Lines = new string[] {
        "This",
        "is",
        "a",
        "test"};
            this.pickText.Location = new System.Drawing.Point(290, 60);
            this.pickText.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pickText.Name = "pickText";
            this.pickText.ReadOnly = true;
            this.pickText.Size = new System.Drawing.Size(256, 395);
            this.pickText.TabIndex = 1;
            // 
            // ReadBackText
            // 
            this.ReadBackText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ReadBackText.AutoScroll = true;
            this.ReadBackText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ReadBackText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReadBackText.Lines = new string[] {
        "This",
        "is",
        "a",
        "test"};
            this.ReadBackText.Location = new System.Drawing.Point(0, 60);
            this.ReadBackText.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ReadBackText.Name = "ReadBackText";
            this.ReadBackText.ReadOnly = true;
            this.ReadBackText.Size = new System.Drawing.Size(256, 395);
            this.ReadBackText.TabIndex = 0;
            // 
            // CFICompareForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.OkButton;
            this.ClientSize = new System.Drawing.Size(543, 514);
            this.Controls.Add(this.lblPick);
            this.Controls.Add(this.lblReadback);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.pickText);
            this.Controls.Add(this.ReadBackText);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CFICompareForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.CFICompareForm_Shown);
            this.Resize += new System.EventHandler(this.CFICompareForm_Resize);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Forms.RichTextBoxEx ReadBackText;
        private Forms.RichTextBoxEx pickText;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton printRptTSButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton PageSetupTSButton;
        private System.Windows.Forms.Label lblReadback;
        private System.Windows.Forms.Label lblPick;
    }
}