//********************************************************************************
//REVISION HISTORY
//--------------------------------------------------------------------------------
//03/15/2010
//---------------------------------------------------------------------------------
//- Added pickdbfn, pickobject to support pickdb
//- modified View_OnIdListChangedEventHandler
//  
//
//03/24/2010
//---------------------------------------------------------------------------------
//- Added ResetStatusBar method in SetView, To switch the contents displayed in the 
//  status bar when the selected DB are switched
//- Added View_OnSettings method to handle the settings button
//- Added ResetEventhandlers to reset similar events when switched between 
//  different databases
//03/25/2010
//---------------------------------------------------------------------------------
//- Added SetDb Class that contains the DbType and pickdb path specific to macroAnalyzer
//*********************************************************************************
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BusinessObject;
using CommonCode;
using CommonUI.Controls;
using SplitCap;
namespace IRMaestro
{
    public class MacroAnalyzerPresenter : PresenterBase, ITestMacroAnalyzerPresenter
    {
        private const string REG_SUBDIR = "Macro Analyzer";
        private const string WORKING_SUBDIR = "MIDV";
        private abstract class RegistryKey
        {
            public const string OneMHzCapture = "OneMHzCapture";
        }
        SetDB setDb = new SetDB();
        private IFormFactory _formFactory;
        private IIrCaptureHelper _irCaptureHelper;
        private ITempFilePath _tempFilePath;
        private IMultiIdFunctionCompareHelper _multiIdFunctionCompareHelper;
        private MultiIDPickComparer _multiIdPickComparer;
        private MvpDataContainers.HierarchicalData _resultData;
        PickDBFunctions pickdbfn;
        PALObject palObject;
        public MacroAnalyzerPresenter()
        {
            DBSettings.macroregpath = @"HKEY_CURRENT_USER\Software\UEITools\Macro Analyzer";
            setDb.GetDBType(DBSettings.macroregpath);
            setDb.GetPickDBPath(DBSettings.macroregpath);
            _formFactory = FormFactory.GetInstance();
            _tempFilePath = new TempFilePath();
            _tempFilePath.SetSubDirectory(WORKING_SUBDIR);
            _irCaptureHelper = new IrCaptureHelper();
            _irCaptureHelper.UseLongCapture();
            _irCaptureHelper.SetFilePathManager(_tempFilePath);
            _irCaptureHelper.OnIrCaptured += OnIrCapturedEventHandler;
            _multiIdFunctionCompareHelper = new MultiIdFunctionCompareHelper();
            _multiIdPickComparer = new MultiIDPickComparer();
        }
        //~MacroAnalyzerPresenter()
        //{
        //    Dispose();
        //}
        public void Dispose()
        {
            _tempFilePath.Dispose();
            _irCaptureHelper.Dispose();
            GC.SuppressFinalize(this);
        }
        private IMacroAnalyzerView View
        {
            get { return (IMacroAnalyzerView)_view; }
        }
        private MvpDataContainers.HierarchicalData ResultData
        {
            get
            {
                if (_resultData == null)
                {
                    _resultData = new MvpDataContainers.HierarchicalData();
                }
                return _resultData;
            }
        }
        public override void SetView(IView view)
        {
            base.SetView(view);
            View.OnResetIds += View_OnResetIdsEventHandler;
            View.OnIdListChanged += View_OnIdListChangedEventHandler;
            View.OnStartCapture += View_OnStartCaptureEventHandler;
            View.OnUsb1MHzCaptureModeChanged += View_OnUsb1MHzCaptureModeChangedEventHandler;
            View.OnCapturePodChanged += View_OnCapturePodChanged;
            View.OnSettings += View_OnSettingsEventHandler;
            View.OnFormClose += View_OnFormCloseEventhandler;
            ResetCaptureStations();
            View.ResetStatusbar();
            View.SetCaptureButtonEnabled(false);
            View.SetIdListText("");
        }
        private void ResetView()
        {
            setDb.GetDBType(DBSettings.macroregpath);
            setDb.GetPickDBPath(DBSettings.macroregpath);
            _tempFilePath.SetSubDirectory(WORKING_SUBDIR);
            //_irCaptureHelper.SetFilePathManager(_tempFilePath);
            View.SetCaptureButtonEnabled(false);
            View.ClearIDSelection();
            ResetCaptureStations();
            View.ResetStatusbar();
        }
        private void ResetCaptureStations()
        {
            bool isUsBeeConnected = IsUSBeeConnected();
            bool hasUSBCaptureDevice = _irCaptureHelper.FindUSBCaptureDevice();
            bool bIsUSBFCDeviceConnected = _irCaptureHelper.FindUSBFieldCaptureDevice();

            _irCaptureHelper.CapturePod = IrCaptureHelper.CapturePodType.NonUSB;
            if (hasUSBCaptureDevice)
            {
                _irCaptureHelper.CapturePod = IrCaptureHelper.CapturePodType.NiUSB;
            }
            else if (isUsBeeConnected)
            {
                _irCaptureHelper.CapturePod = IrCaptureHelper.CapturePodType.USBee;
            }
            else if (bIsUSBFCDeviceConnected)
            {
                _irCaptureHelper.CapturePod = IrCaptureHelper.CapturePodType.FcUSB; //Field Capture Device
            }

            View.Usb1MHzCaptureMode = Configuration.GetConfigItem(REG_SUBDIR, RegistryKey.OneMHzCapture, false);
            View.Show1MHzCapture(hasUSBCaptureDevice);
            View.ClearCaptureStations();
            StringCollection availableCaptureStations = GetAvailableCaptureStations(hasUSBCaptureDevice, isUsBeeConnected, bIsUSBFCDeviceConnected);
            View.SetAvailableCaptureStations(availableCaptureStations);
            View.EnableCapturePodSelection(availableCaptureStations.Count > 1);
            View.SetCurrentCapturePod(_irCaptureHelper.CapturePod.Name);
        }
        private StringCollection GetAvailableCaptureStations(bool hasUSBCaptureDevice, bool isUsBeeConnected, bool bIsUSBFCDeviceConnected)
        {
            StringCollection availableCaptureStations = new StringCollection();
            foreach (IrCaptureHelper.CapturePodType podType in IrCaptureHelper.CapturePodType.Members)
            {
                if (podType == IrCaptureHelper.CapturePodType.USBee && isUsBeeConnected == false)
                {
                    continue;
                }
                if (podType == IrCaptureHelper.CapturePodType.NiUSB && hasUSBCaptureDevice == false)
                {
                    continue;
                }
                if (podType == IrCaptureHelper.CapturePodType.FcUSB && bIsUSBFCDeviceConnected == false)
                {
                    continue;
                }
                availableCaptureStations.Add(podType.Name);
            }
            return availableCaptureStations;
        }

        private void View_OnFormCloseEventhandler(object sender, FormClosingEventArgs e)
        {
            Dispose();
        }
        private void View_OnSettingsEventHandler(object sender, EventArgs e)
        {
            Forms.frm_Settings settings = new IRMaestro.Forms.frm_Settings(WORKING_SUBDIR);
            if (settings.ShowDialog() == DialogResult.OK)
                ResetView();
        }

        private void View_OnCapturePodChanged(string capturePod)
        {
            IrCaptureHelper.CapturePodType currentCapturePodType = _irCaptureHelper.CapturePod;
            IrCaptureHelper.CapturePodType capturePodType = IrCaptureHelper.CapturePodType.Members.Find(
                delegate(IrCaptureHelper.CapturePodType podType) { return podType.Name == capturePod; });
            bool errorSettingCapturePod = false;
            if (capturePodType == IrCaptureHelper.CapturePodType.USBee && _irCaptureHelper.IsUSBeeConnected() == false)
            {
                FormFactory.GetInstance().MessageBox(null, "USBee pod not connected.", "Error", MessageBoxButtons.OK);
                errorSettingCapturePod = true;
            }
            if (capturePodType == IrCaptureHelper.CapturePodType.NiUSB && _irCaptureHelper.FindUSBCaptureDevice() == false)
            {
                FormFactory.GetInstance().MessageBox(null, "NI USB capture station not available", "Error", MessageBoxButtons.OK);
                errorSettingCapturePod = true;
            }
            if (capturePodType == IrCaptureHelper.CapturePodType.FcUSB && _irCaptureHelper.FindUSBFieldCaptureDevice() == false)
            {
                FormFactory.GetInstance().MessageBox(null, "Filed Capture Device not connected", "Error", MessageBoxButtons.OK);
                errorSettingCapturePod = true;
            }
            if (errorSettingCapturePod)
            {
                View.SetCurrentCapturePod(currentCapturePodType.Name);
            }
            else
            {
                View.SetCurrentCapturePod(capturePod);
                //For NI USB capture && Field Capture USB 1MHZ capture should be enabled
                View.EnableUsbCapture1MHz((capturePodType == IrCaptureHelper.CapturePodType.NiUSB) || (capturePodType == IrCaptureHelper.CapturePodType.FcUSB));

                _irCaptureHelper.CapturePod = capturePodType;
            }
        }
        private bool IsUSBeeConnected()
        {
            bool status = false;
            IrCaptureHelper usbee = new IrCaptureHelper();
            status = usbee.IsUSBeeConnected();
            return status;
        }
        private void OnIrCapturedEventHandler(string filePath)
        {
            string macroLabel = string.Format("Macro {0}", ResultData.Count + 1);
            string macroCaptureFilename = macroLabel + ".zip";
            /////////////////////////////////////////////////////////////////////
            // TODO: Split capture (untested)
            BreakFile macroSpliter = new BreakFile();
            string macroSplitFilename = macroSpliter.Breakitapart(filePath, _tempFilePath.GetWorkingDirectory());
            string macroSplitProjectPath = _tempFilePath.GetWorkingDirectory() + macroSplitFilename;
            string macroCapturePath = _tempFilePath.CopyFileFrom(macroSplitProjectPath, macroCaptureFilename);
            _tempFilePath.DeleteFile(macroSplitProjectPath);
            using (IIrCaptureSet irCaptureSet = new IrCaptureSet())
            {
                irCaptureSet.SetCaptureSet(macroCapturePath);
                string[] files = irCaptureSet.CaptureFileList;
                /////////////////////////////////////////////////////////////////////
                MultiIdFunctionSearchResults multiIdFunctionSearchResults = null;
                switch (setDb.selectedDBType)
                {
                    case DBType.UEIDB:
                        {
                            multiIdFunctionSearchResults = _multiIdFunctionCompareHelper.SearchForMatch(files);
                            break;
                        }
                    case DBType.PickDB:
                        {
                            multiIdFunctionSearchResults = pickdbfn.SearchForIDMatch(_multiIdPickComparer, files);
                            break;
                        }
                }
                /////////////////////////////////////////////////////////////////////
                // TODO: process compare results and set view with results (untested)
                ProcessResultDataForView(multiIdFunctionSearchResults, macroLabel);
                View.SetResultData(_resultData);
                /////////////////////////////////////////////////////////////////////
            }
        }
        private void View_OnResetIdsEventHandler(object sender, EventArgs e)
        {
            View.SetResultData(null);
            _resultData = null;
            _tempFilePath.ClearFiles();
        }
        private void ProcessResultDataForView(MultiIdFunctionSearchResults multiIdFunctionSearchResults, string macroLabel)
        {
            MvpDataContainers.HierarchicalData data = ResultData;
            MvpDataContainers.HierarchicalData.Node root = new MvpDataContainers.HierarchicalData.Node(macroLabel);
            data.Add(root);
            int functionCount = 1;
            foreach (MultiIdFunctionSearchResults.Item idFunctionSearchResult in multiIdFunctionSearchResults)
            {
                MvpDataContainers.HierarchicalData.Node functionNode =
                    new MvpDataContainers.HierarchicalData.Node(string.Format("Key {0}", functionCount));
                root.Children.Add(functionNode);
                if (idFunctionSearchResult.SearchResults.Count > 0)
                {
                    foreach (MultiIdFunctionSearchResults.Results result in idFunctionSearchResult.SearchResults)
                    {
                        string label = string.Format("[{0}] {1}", result.MatchId, result.MatchedFunctions.Label);
                        functionNode.Children.Add(new MvpDataContainers.HierarchicalData.Node(label));
                    }
                }
                else
                {
                    functionNode.Children.Add(new MvpDataContainers.HierarchicalData.Node("No match found"));
                }
                functionCount++;
            }
        }
        public void View_OnIdListChangedEventHandler(object sender, EventArgs e)
        {
            _multiIdFunctionCompareHelper.Clear();
            bool atLeastOneIdIsValid = false;
            string idListText = View.GetIdListText();
            string[] ids = idListText.Split(',');
            string selectedIdListText = string.Empty;
            switch (setDb.selectedDBType)
            {
                case DBType.UEIDB:
                    {
                        foreach (string id in ids)
                        {
                            string listedId = id.Trim(' ', '[', ']', '!');
                            bool succeeded = _multiIdFunctionCompareHelper.AddId(listedId, setDb.selectedDBType);
                            if (succeeded)
                            {
                                if (listedId == "" || listedId == string.Empty)
                                    atLeastOneIdIsValid = false;
                                else
                                    atLeastOneIdIsValid = true;
                                selectedIdListText += listedId + ", ";
                            }
                            else
                            {
                                selectedIdListText += "[" + listedId + "], ";
                            }
                        }
                        selectedIdListText = selectedIdListText.Trim(' ', ',');
                        break;
                    }
                case DBType.PickDB:
                    {
                        palObject = new PALObject(DBSettings.GetDBPath(DBSettings.macroregpath, "SelectedPickProject"));
                        
                        pickdbfn = new PickDBFunctions();
                        _multiIdPickComparer.pickcomparer = new List<PickIDComparer>();
                        foreach (string id in ids)
                        {
                            if (id.Trim().CompareTo("") != 0)
                            {
                                string listId = id.Trim(' ', '[', ']', '!');
                                PickIDComparer _pickIdComparer = new PickIDComparer();
                                DBSettings.selectedId = listId.ToUpper();
                                DBSettings.CreatePickWorkingPath();
                                try
                                {
                                    palObject.LoadPickObjects(DBSettings.selectedId);
                                    if (listId == "" || listId == string.Empty)
                                        atLeastOneIdIsValid = false;
                                    else
                                    {
                                        atLeastOneIdIsValid = true;
                                        _pickIdComparer.id = PALObject.pickid;
                                        _pickIdComparer.strID = DBSettings.selectedId;
                                        _pickIdComparer.workingpath = DBSettings.pickIDWorkingPath;
                                        _pickIdComparer.cfifiles = null;
                                        _pickIdComparer.pkObject = palObject;
                                    }
                                    selectedIdListText += listId + ", ";
                                    _multiIdPickComparer.pickcomparer.Add(_pickIdComparer);
                                }
                                catch
                                {
                                    selectedIdListText += "[" + listId + "], ";
                                }
                            }
                        }
                        selectedIdListText = selectedIdListText.Trim(' ', ',');
                        break;
                    }
            }
            View.SetIdListText(selectedIdListText.ToUpper());
            View.SetCaptureButtonEnabled(atLeastOneIdIsValid);
            View.SetResultData(null);
            _resultData = null;
            _tempFilePath.ClearFiles();
            if (selectedIdListText.Contains("["))
            {
                _formFactory.MessageBox(null,
                                        "One or more IDs entered was not found.  IDs between '[]' are unidentified and will not be used.",
                                        "Invalide IDs",
                                        MessageBoxButtons.OK);
            }
        }
        public void View_OnStartCaptureEventHandler(object sender, EventArgs e)
        {
            if (_irCaptureHelper.CapturePod == IrCaptureHelper.CapturePodType.FcUSB)
            {
                string[] strDeviceNames = _irCaptureHelper.GetUSBFieldCaptureDeviceList();
                if (strDeviceNames.Length > 0)
                {
                    _irCaptureHelper.UsbFcDevicename = strDeviceNames[0]; //Default Device
                    Properties.Settings.Default._SelectedUSBFieldCaptureDevice = _irCaptureHelper.UsbFcDevicename;

                    string strSelectedFCDevice = Properties.Settings.Default._SelectedUSBFieldCaptureDevice;
                    if (strSelectedFCDevice.Trim() != string.Empty)
                    {

                        bool bFound = false;
                        foreach (string strDeviceName in strDeviceNames)
                        {
                            if (strDeviceName == strSelectedFCDevice)
                            {
                                _irCaptureHelper.UsbFcDevicename = strSelectedFCDevice;
                                Properties.Settings.Default._SelectedUSBFieldCaptureDevice = _irCaptureHelper.UsbFcDevicename;

                                bFound = true;
                                break;
                            }
                        }

                    }
                }
                else
                {
                    _irCaptureHelper.UsbFcDevicename = "";
                    Properties.Settings.Default._SelectedUSBFieldCaptureDevice = "";
                }
            }
            string cdw = Environment.CurrentDirectory;
            Environment.CurrentDirectory = _tempFilePath.CurrentWorkingPath;
            string captureName = _tempFilePath.GetNewCaptureName("", ResultData.Count, _irCaptureHelper.WillCaptureAt1MHz());
            string capturePath = _tempFilePath.GetWorkingDirectory() + captureName;
            bool captureOne = _irCaptureHelper.CaptureOne("", capturePath);
            Environment.CurrentDirectory = cdw;
            if (captureOne == false && string.IsNullOrEmpty(_irCaptureHelper.LastErrorMessage) == false)
            {
                _formFactory.MessageBox(null, _irCaptureHelper.LastErrorMessage, "Capture Error", MessageBoxButtons.OK);
            }
        }
        private void View_OnUsb1MHzCaptureModeChangedEventHandler(object sender, EventArgs e)
        {
            bool capture1MHz = View.Usb1MHzCaptureMode;
            _irCaptureHelper.UsbCapture1MHz = capture1MHz;
            Configuration.SetConfigItem(REG_SUBDIR, RegistryKey.OneMHzCapture, capture1MHz);
        }
        void ITestMacroAnalyzerPresenter.SetFormsLib(IFormFactory formFactory)
        {
            _formFactory = formFactory;
        }
        void ITestMacroAnalyzerPresenter.SetIrCaptureHelper(IIrCaptureHelper irCaptureHelper)
        {
            _irCaptureHelper = irCaptureHelper;
            _irCaptureHelper.OnIrCaptured += OnIrCapturedEventHandler;
        }
        void ITestMacroAnalyzerPresenter.SetFilePathManager(ITempFilePath filePathManager)
        {
            _tempFilePath = filePathManager;
        }
        void ITestMacroAnalyzerPresenter.SetMultiIdFunctionCompareHelper(IMultiIdFunctionCompareHelper multiIdFunctionCompareHelper)
        {
            _multiIdFunctionCompareHelper = multiIdFunctionCompareHelper;
        }
    }
    public interface ITestMacroAnalyzerPresenter : IDisposable
    {
        void SetFormsLib(IFormFactory formFactory);
        void SetIrCaptureHelper(IIrCaptureHelper irCaptureHelper);
        void SetFilePathManager(ITempFilePath filePathManager);
        void SetMultiIdFunctionCompareHelper(IMultiIdFunctionCompareHelper multiIdFunctionCompareHelper);
    }
    public interface IMacroAnalyzerView : IView
    {
        event EventHandler OnSelectIds;
        event EventHandler OnResetIds;
        event EventHandler OnIdListChanged;
        event EventHandler OnStartCapture;
        event EventHandler OnUsb1MHzCaptureModeChanged;
        event CapturePodChangedDelegate OnCapturePodChanged;
        event EventHandler OnSettings;
        event FormClosingEventHandler OnFormClose;
        bool Usb1MHzCaptureMode { get; set; }
        string GetIdListText();
        void SetCaptureButtonEnabled(bool enable);
        void SetIdListText(string listText);
        void EnableUsbCapture1MHz(bool enable);
        void EnableCapturePodSelection(bool enable);
        void ClearCaptureStations();
        void Show1MHzCapture(bool show);
        void SetAvailableCaptureStations(IList<string> availableCapturePodList);
        void SetCurrentCapturePod(string capturePod);
        void SetResultData(MvpDataContainers.HierarchicalData resultData);
        void ResetStatusbar();
        void ClearIDSelection();
    }
}