namespace IRMaestro
{
    partial class ReadBackDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadBackDlg));
            this.WorkDirLabel = new System.Windows.Forms.Label();
            this.ReadBackDirLabel = new System.Windows.Forms.Label();
            this.WorkDirText = new System.Windows.Forms.TextBox();
            this.ReadBackDirText = new System.Windows.Forms.TextBox();
            this.OptBttn = new System.Windows.Forms.Button();
            this.OkBttn = new System.Windows.Forms.Button();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.WorkDirBrBttn = new System.Windows.Forms.Button();
            this.ReadBackBrBttn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.PickProjcomboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.xmlFileRadiobutton = new System.Windows.Forms.RadioButton();
            this.databaseRadiobutton = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pickxmlBrwBtn = new System.Windows.Forms.Button();
            this.xmlDirTextbox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // WorkDirLabel
            // 
            this.WorkDirLabel.AutoSize = true;
            this.WorkDirLabel.Location = new System.Drawing.Point(32, 41);
            this.WorkDirLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.WorkDirLabel.Name = "WorkDirLabel";
            this.WorkDirLabel.Size = new System.Drawing.Size(125, 17);
            this.WorkDirLabel.TabIndex = 0;
            this.WorkDirLabel.Text = "Working Directory:";
            // 
            // ReadBackDirLabel
            // 
            this.ReadBackDirLabel.AutoSize = true;
            this.ReadBackDirLabel.Location = new System.Drawing.Point(32, 286);
            this.ReadBackDirLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ReadBackDirLabel.Name = "ReadBackDirLabel";
            this.ReadBackDirLabel.Size = new System.Drawing.Size(138, 17);
            this.ReadBackDirLabel.TabIndex = 2;
            this.ReadBackDirLabel.Text = "ReadBack Directory:";
            // 
            // WorkDirText
            // 
            this.WorkDirText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WorkDirText.Location = new System.Drawing.Point(197, 41);
            this.WorkDirText.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.WorkDirText.Name = "WorkDirText";
            this.WorkDirText.Size = new System.Drawing.Size(415, 22);
            this.WorkDirText.TabIndex = 3;
            this.WorkDirText.Validating += new System.ComponentModel.CancelEventHandler(this.WorkDirText_Validating);
            // 
            // ReadBackDirText
            // 
            this.ReadBackDirText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReadBackDirText.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::IRMaestro.Properties.Settings.Default, "_ReadbackPath", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ReadBackDirText.Location = new System.Drawing.Point(197, 282);
            this.ReadBackDirText.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ReadBackDirText.Name = "ReadBackDirText";
            this.ReadBackDirText.Size = new System.Drawing.Size(415, 22);
            this.ReadBackDirText.TabIndex = 5;
            this.ReadBackDirText.Text = global::IRMaestro.Properties.Settings.Default._ReadbackPath;
            this.ReadBackDirText.Validating += new System.ComponentModel.CancelEventHandler(this.ReadBackDirText_Validating);
            // 
            // OptBttn
            // 
            this.OptBttn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.OptBttn.Location = new System.Drawing.Point(248, 343);
            this.OptBttn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OptBttn.Name = "OptBttn";
            this.OptBttn.Size = new System.Drawing.Size(113, 28);
            this.OptBttn.TabIndex = 6;
            this.OptBttn.Text = "Setup Option";
            this.OptBttn.UseVisualStyleBackColor = true;
            this.OptBttn.Click += new System.EventHandler(this.OptBttn_Click);
            // 
            // OkBttn
            // 
            this.OkBttn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.OkBttn.Location = new System.Drawing.Point(408, 343);
            this.OkBttn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OkBttn.Name = "OkBttn";
            this.OkBttn.Size = new System.Drawing.Size(113, 28);
            this.OkBttn.TabIndex = 7;
            this.OkBttn.Text = "Ok";
            this.OkBttn.UseVisualStyleBackColor = true;
            this.OkBttn.Click += new System.EventHandler(this.OkBttn_Click);
            // 
            // CancelBttn
            // 
            this.CancelBttn.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CancelBttn.Location = new System.Drawing.Point(565, 343);
            this.CancelBttn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CancelBttn.Name = "CancelBttn";
            this.CancelBttn.Size = new System.Drawing.Size(113, 28);
            this.CancelBttn.TabIndex = 8;
            this.CancelBttn.Text = "Cancel";
            this.CancelBttn.UseVisualStyleBackColor = true;
            this.CancelBttn.Click += new System.EventHandler(this.CancelBttn_Click);
            // 
            // WorkDirBrBttn
            // 
            this.WorkDirBrBttn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.WorkDirBrBttn.Location = new System.Drawing.Point(620, 37);
            this.WorkDirBrBttn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.WorkDirBrBttn.Name = "WorkDirBrBttn";
            this.WorkDirBrBttn.Size = new System.Drawing.Size(100, 28);
            this.WorkDirBrBttn.TabIndex = 9;
            this.WorkDirBrBttn.Text = "Browse";
            this.WorkDirBrBttn.UseVisualStyleBackColor = true;
            this.WorkDirBrBttn.Click += new System.EventHandler(this.WorkDirBrBttn_Click);
            // 
            // ReadBackBrBttn
            // 
            this.ReadBackBrBttn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ReadBackBrBttn.Location = new System.Drawing.Point(621, 279);
            this.ReadBackBrBttn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ReadBackBrBttn.Name = "ReadBackBrBttn";
            this.ReadBackBrBttn.Size = new System.Drawing.Size(100, 28);
            this.ReadBackBrBttn.TabIndex = 11;
            this.ReadBackBrBttn.Text = "Browse";
            this.ReadBackBrBttn.UseVisualStyleBackColor = true;
            this.ReadBackBrBttn.Click += new System.EventHandler(this.ReadBackBrBttn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 167);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 19;
            this.label1.Text = "Pick Project";
            // 
            // PickProjcomboBox
            // 
            this.PickProjcomboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.PickProjcomboBox.FormattingEnabled = true;
            this.PickProjcomboBox.Location = new System.Drawing.Point(197, 164);
            this.PickProjcomboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PickProjcomboBox.Name = "PickProjcomboBox";
            this.PickProjcomboBox.Size = new System.Drawing.Size(313, 24);
            this.PickProjcomboBox.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 215);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 17);
            this.label2.TabIndex = 19;
            this.label2.Text = "Pick XML Directory:";
            // 
            // xmlFileRadiobutton
            // 
            this.xmlFileRadiobutton.AutoSize = true;
            this.xmlFileRadiobutton.Location = new System.Drawing.Point(161, 34);
            this.xmlFileRadiobutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xmlFileRadiobutton.Name = "xmlFileRadiobutton";
            this.xmlFileRadiobutton.Size = new System.Drawing.Size(83, 21);
            this.xmlFileRadiobutton.TabIndex = 21;
            this.xmlFileRadiobutton.TabStop = true;
            this.xmlFileRadiobutton.Text = "XML File";
            this.xmlFileRadiobutton.UseVisualStyleBackColor = true;
            this.xmlFileRadiobutton.CheckedChanged += new System.EventHandler(this.xmlFileRadiobutton_CheckedChanged);
            // 
            // databaseRadiobutton
            // 
            this.databaseRadiobutton.AutoSize = true;
            this.databaseRadiobutton.Location = new System.Drawing.Point(329, 34);
            this.databaseRadiobutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.databaseRadiobutton.Name = "databaseRadiobutton";
            this.databaseRadiobutton.Size = new System.Drawing.Size(90, 21);
            this.databaseRadiobutton.TabIndex = 22;
            this.databaseRadiobutton.TabStop = true;
            this.databaseRadiobutton.Text = "Database";
            this.databaseRadiobutton.UseVisualStyleBackColor = true;
            this.databaseRadiobutton.CheckedChanged += new System.EventHandler(this.databaseRadiobutton_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.databaseRadiobutton);
            this.groupBox1.Controls.Add(this.pickxmlBrwBtn);
            this.groupBox1.Controls.Add(this.xmlFileRadiobutton);
            this.groupBox1.Controls.Add(this.xmlDirTextbox);
            this.groupBox1.Location = new System.Drawing.Point(36, 86);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(699, 174);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pick Source";
            // 
            // pickxmlBrwBtn
            // 
            this.pickxmlBrwBtn.Location = new System.Drawing.Point(584, 123);
            this.pickxmlBrwBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pickxmlBrwBtn.Name = "pickxmlBrwBtn";
            this.pickxmlBrwBtn.Size = new System.Drawing.Size(100, 28);
            this.pickxmlBrwBtn.TabIndex = 1;
            this.pickxmlBrwBtn.Text = "Browse";
            this.pickxmlBrwBtn.UseVisualStyleBackColor = true;
            this.pickxmlBrwBtn.Click += new System.EventHandler(this.pickxmlBrwBtn_Click);
            // 
            // xmlDirTextbox
            // 
            this.xmlDirTextbox.Location = new System.Drawing.Point(161, 127);
            this.xmlDirTextbox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xmlDirTextbox.Name = "xmlDirTextbox";
            this.xmlDirTextbox.Size = new System.Drawing.Size(415, 22);
            this.xmlDirTextbox.TabIndex = 0;
            // 
            // ReadBackDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(777, 410);
            this.Controls.Add(this.PickProjcomboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ReadBackBrBttn);
            this.Controls.Add(this.WorkDirBrBttn);
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.OkBttn);
            this.Controls.Add(this.OptBttn);
            this.Controls.Add(this.ReadBackDirText);
            this.Controls.Add(this.WorkDirText);
            this.Controls.Add(this.ReadBackDirLabel);
            this.Controls.Add(this.WorkDirLabel);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "ReadBackDlg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Do ReadBack Comparison";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReadBackDlg_FormClosing);
            this.Shown += new System.EventHandler(this.ReadBackDlg_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label WorkDirLabel;
        private System.Windows.Forms.Label ReadBackDirLabel;
        private System.Windows.Forms.TextBox WorkDirText;
        private System.Windows.Forms.TextBox ReadBackDirText;
        private System.Windows.Forms.Button OptBttn;
        private System.Windows.Forms.Button OkBttn;
        private System.Windows.Forms.Button CancelBttn;
        private System.Windows.Forms.Button WorkDirBrBttn;
        private System.Windows.Forms.Button ReadBackBrBttn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox PickProjcomboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton xmlFileRadiobutton;
        private System.Windows.Forms.RadioButton databaseRadiobutton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button pickxmlBrwBtn;
        private System.Windows.Forms.TextBox xmlDirTextbox;
    }
}