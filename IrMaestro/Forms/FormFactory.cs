using System.Collections.Generic;
using System.Windows.Forms;

namespace CommonUI.Controls {
    public class FormFactory : IFormFactory {
        private static FormFactory __FormFactory;
        private Dictionary<string, CreateFormDelegate> _formCreationDelegates;
        
        public static IFormFactory GetInstance() {
            if (__FormFactory == null) {
                __FormFactory = new FormFactory();
            }
            return __FormFactory;
        }

        private FormFactory() {
            _formCreationDelegates = new Dictionary<string, CreateFormDelegate>();
        }

        public DialogResult MessageBox(IWin32Window owner, string text, string caption, MessageBoxButtons buttons) {
            return System.Windows.Forms.MessageBox.Show(owner, text, caption, buttons);
        }

        public IProcessingForm GetProcessingForm() {
            return new ProcessingForm();
        }

        public void RegisterForm(string formId, CreateFormDelegate createFormDelegate) {
            if (_formCreationDelegates.ContainsKey(formId) == false) {
                _formCreationDelegates.Add(formId, createFormDelegate);
            }
        }

        public IFormFactoryForm GetForm(string formId) {
            IFormFactoryForm form = null;
            if (_formCreationDelegates.ContainsKey(formId)) {
                form = _formCreationDelegates[formId]();
            }

            return form;
        }
    }

    public interface IFormFactoryForm {
        string Text { get; set; }

        void Show();
        void Show(IWin32Window owner);
        DialogResult ShowDialog();
        DialogResult ShowDialog(IWin32Window owner);

        void Close();
    }

    public delegate IFormFactoryForm CreateFormDelegate();
    public interface IFormFactory {
        DialogResult MessageBox(IWin32Window owner, string text, string caption, MessageBoxButtons buttons);
        IProcessingForm GetProcessingForm();

        IFormFactoryForm GetForm(string formId);
        void RegisterForm(string formId, CreateFormDelegate createFormDelegate);
    }
}
