using System.Windows.Forms;
using CommonUI.Controls;

namespace IRMaestro {
    public partial class DecoderExecConfirmationForm : Form, IFormFactoryForm, IDecoderExecConfirmationForm {
        private int _executor;

        public DecoderExecConfirmationForm() {
            InitializeComponent();
        }

        public int Executor {
            get { return _executor; }
            set {
                _executor = value;
                CaptionLabel.Text = string.Format("Input file can't be identified by E{0:000}.", _executor);
                UseCurrentExecRadioButton.Text = string.Format("Use E{0:000} anyway.", _executor);
            }
        }

        public void SetExecList(ExecutorList list) {
            ExecListComboBox.Items.Clear();
            foreach (int executor in list) {
                ExecListComboBox.Items.Add(executor);                
            }
        }

        private void ChangeExecRadioButton_CheckedChanged(object sender, System.EventArgs e) {
            UpdateUI();
        }

        private void UseCurrentExecRadioButton_CheckedChanged(object sender, System.EventArgs e) {
            UpdateUI();
        }

        private void UpdateUI() {
            ExecListComboBox.Enabled = ChangeExecRadioButton.Checked;
        }

        private void ContinueButton_Click(object sender, System.EventArgs e) {
            if (ChangeExecRadioButton.Checked) {
                _executor = (int)ExecListComboBox.SelectedItem;
            }
        }
    }

    public interface IDecoderExecConfirmationForm {
        int Executor { get; set; }
        void SetExecList(ExecutorList list);
    }
}