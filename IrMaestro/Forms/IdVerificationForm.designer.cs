namespace IRMaestro
{
    partial class IdVerificationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IdVerificationForm));
            this.label1 = new System.Windows.Forms.Label();
            this.IdListComboBox = new System.Windows.Forms.ComboBox();
            this.ResetIdButton = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label2 = new System.Windows.Forms.Label();
            this.AvailableFunctionsListBox = new System.Windows.Forms.ListView();
            this.label3 = new System.Windows.Forms.Label();
            this.MatchedFunctionsListView = new System.Windows.Forms.ListView();
            this.SaveButton = new System.Windows.Forms.Button();
            this.SetIdButton = new System.Windows.Forms.Button();
            this.Use1MHzCheckBox = new System.Windows.Forms.CheckBox();
            this.CaptureButton = new System.Windows.Forms.Button();
            this.CaptureSettingsButton = new System.Windows.Forms.Button();
            this.CapturePodComboBox = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripDbStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripPodStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripPickDBPath = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(14, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Id";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // IdListComboBox
            // 
            this.IdListComboBox.FormattingEnabled = true;
            this.IdListComboBox.Location = new System.Drawing.Point(38, 13);
            this.IdListComboBox.Name = "IdListComboBox";
            this.IdListComboBox.Size = new System.Drawing.Size(129, 21);
            this.IdListComboBox.TabIndex = 1;
            this.IdListComboBox.DropDownClosed += new System.EventHandler(this.TnListComboBox_DropDownClosed);
            this.IdListComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TnListComboBox_KeyPress);
            // 
            // ResetIdButton
            // 
            this.ResetIdButton.Location = new System.Drawing.Point(260, 11);
            this.ResetIdButton.Name = "ResetIdButton";
            this.ResetIdButton.Size = new System.Drawing.Size(80, 22);
            this.ResetIdButton.TabIndex = 2;
            this.ResetIdButton.Text = "Reset";
            this.ResetIdButton.UseVisualStyleBackColor = true;
            this.ResetIdButton.Click += new System.EventHandler(this.ResetIdButton_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Location = new System.Drawing.Point(2, 39);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.AvailableFunctionsListBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.MatchedFunctionsListView);
            this.splitContainer1.Size = new System.Drawing.Size(895, 629);
            this.splitContainer1.SplitterDistance = 435;
            this.splitContainer1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Available Functions";
            // 
            // AvailableFunctionsListBox
            // 
            this.AvailableFunctionsListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AvailableFunctionsListBox.Location = new System.Drawing.Point(6, 25);
            this.AvailableFunctionsListBox.Name = "AvailableFunctionsListBox";
            this.AvailableFunctionsListBox.Size = new System.Drawing.Size(421, 498);
            this.AvailableFunctionsListBox.TabIndex = 0;
            this.AvailableFunctionsListBox.UseCompatibleStateImageBehavior = false;
            this.AvailableFunctionsListBox.View = System.Windows.Forms.View.List;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Matched Functions";
            // 
            // MatchedFunctionsListView
            // 
            this.MatchedFunctionsListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MatchedFunctionsListView.HideSelection = false;
            this.MatchedFunctionsListView.Location = new System.Drawing.Point(6, 25);
            this.MatchedFunctionsListView.Name = "MatchedFunctionsListView";
            this.MatchedFunctionsListView.Size = new System.Drawing.Size(442, 498);
            this.MatchedFunctionsListView.TabIndex = 2;
            this.MatchedFunctionsListView.UseCompatibleStateImageBehavior = false;
            this.MatchedFunctionsListView.View = System.Windows.Forms.View.List;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(346, 11);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(80, 22);
            this.SaveButton.TabIndex = 6;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // SetIdButton
            // 
            this.SetIdButton.Location = new System.Drawing.Point(174, 11);
            this.SetIdButton.Name = "SetIdButton";
            this.SetIdButton.Size = new System.Drawing.Size(80, 22);
            this.SetIdButton.TabIndex = 7;
            this.SetIdButton.Text = "Set";
            this.SetIdButton.UseVisualStyleBackColor = true;
            this.SetIdButton.Click += new System.EventHandler(this.SetIdButton_Click);
            // 
            // Use1MHzCheckBox
            // 
            this.Use1MHzCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Use1MHzCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.Use1MHzCheckBox.AutoSize = true;
            this.Use1MHzCheckBox.Location = new System.Drawing.Point(780, 11);
            this.Use1MHzCheckBox.Name = "Use1MHzCheckBox";
            this.Use1MHzCheckBox.Size = new System.Drawing.Size(45, 23);
            this.Use1MHzCheckBox.TabIndex = 8;
            this.Use1MHzCheckBox.Text = "1MHz";
            this.Use1MHzCheckBox.UseVisualStyleBackColor = true;
            this.Use1MHzCheckBox.CheckedChanged += new System.EventHandler(this.Use1MHzCheckBox_CheckedChanged);
            // 
            // CaptureButton
            // 
            this.CaptureButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CaptureButton.Location = new System.Drawing.Point(605, 11);
            this.CaptureButton.Name = "CaptureButton";
            this.CaptureButton.Size = new System.Drawing.Size(80, 22);
            this.CaptureButton.TabIndex = 4;
            this.CaptureButton.Text = "Capture";
            this.CaptureButton.UseVisualStyleBackColor = true;
            this.CaptureButton.Click += new System.EventHandler(this.CaptureButton_Click);
            // 
            // CaptureSettingsButton
            // 
            this.CaptureSettingsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CaptureSettingsButton.Location = new System.Drawing.Point(831, 11);
            this.CaptureSettingsButton.Name = "CaptureSettingsButton";
            this.CaptureSettingsButton.Size = new System.Drawing.Size(66, 22);
            this.CaptureSettingsButton.TabIndex = 12;
            this.CaptureSettingsButton.Text = "Settings...";
            this.CaptureSettingsButton.UseVisualStyleBackColor = true;
            this.CaptureSettingsButton.Click += new System.EventHandler(this.CaptureSettingsButton_Click);
            // 
            // CapturePodComboBox
            // 
            this.CapturePodComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CapturePodComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CapturePodComboBox.FormattingEnabled = true;
            this.CapturePodComboBox.Location = new System.Drawing.Point(691, 12);
            this.CapturePodComboBox.Name = "CapturePodComboBox";
            this.CapturePodComboBox.Size = new System.Drawing.Size(78, 21);
            this.CapturePodComboBox.TabIndex = 13;
            this.CapturePodComboBox.SelectedIndexChanged += new System.EventHandler(this.CapturePodComboBox_SelectedIndexChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDbStatus,
            this.toolStripPodStatus,
            this.toolStripPickDBPath});
            this.statusStrip1.Location = new System.Drawing.Point(0, 574);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 15, 0);
            this.statusStrip1.Size = new System.Drawing.Size(902, 22);
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripDbStatus
            // 
            this.toolStripDbStatus.Name = "toolStripDbStatus";
            this.toolStripDbStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripPodStatus
            // 
            this.toolStripPodStatus.Name = "toolStripPodStatus";
            this.toolStripPodStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripPickDBPath
            // 
            this.toolStripPickDBPath.Name = "toolStripPickDBPath";
            this.toolStripPickDBPath.Size = new System.Drawing.Size(0, 17);
            // 
            // IdVerificationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(902, 596);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.CapturePodComboBox);
            this.Controls.Add(this.CaptureSettingsButton);
            this.Controls.Add(this.Use1MHzCheckBox);
            this.Controls.Add(this.SetIdButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.CaptureButton);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.ResetIdButton);
            this.Controls.Add(this.IdListComboBox);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "IdVerificationForm";
            this.ShowInTaskbar = false;
            this.Text = "Verify ID";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.IdVerificationForm_FormClosing);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox IdListComboBox;
        private System.Windows.Forms.Button ResetIdButton;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView AvailableFunctionsListBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView MatchedFunctionsListView;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button SetIdButton;
        private System.Windows.Forms.CheckBox Use1MHzCheckBox;
        private System.Windows.Forms.Button CaptureButton;
        private System.Windows.Forms.Button CaptureSettingsButton;
        private System.Windows.Forms.ComboBox CapturePodComboBox;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripDbStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripPodStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripPickDBPath;
    }
}

