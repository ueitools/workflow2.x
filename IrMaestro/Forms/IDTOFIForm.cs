﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace IRMaestro.Forms
{
    public partial class IDTOFIForm : Form
    {
        private IList<string> listOfIds;
        private string outputDirectory = String.Empty;
        private bool convertOutputToZip = false;
        

        public IDTOFIForm():this(new List<string>(),String.Empty)
        {
            
        }

        public IDTOFIForm(IList<string> idList,string fiOutput)
        {
            InitializeComponent();

            this.listOfIds = idList;
            this.outputDirectory = fiOutput;
        }

        private void IDTOFIForm_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                txtBxFiOutput.Text = this.outputDirectory;

                lblIdCount.Text = this.listOfIds.Count.ToString();

                lblIdCount.BorderStyle = BorderStyle.None;
                lblProgressMessage.BorderStyle = BorderStyle.None;
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            progressBar.Value = 0;
            lblProgressMessage.Text = String.Empty;

            if (this.listOfIds!=null && this.listOfIds.Count > 0 && !String.IsNullOrEmpty(txtBxFiOutput.Text) && Directory.Exists(txtBxFiOutput.Text))
            {
                lblProgressMessage.Text = "Execution started..";

                btnGenerate.Enabled = false;    
                this.outputDirectory = txtBxFiOutput.Text.Trim();
                this.convertOutputToZip = chkBxZipOutput.Checked;

                if (!backgroundWorker.IsBusy)
                {
                    backgroundWorker.RunWorkerAsync();
                }
            }
            //set the progress bar.
        }

        #region BackgroundWorker

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
           
          IDToFI idToFiHelper = new IDToFI();
          int completedIdCount = 0;

          FIGenerationResult result = new FIGenerationResult();
          result.NumberOfIds = this.listOfIds.Count;
          result.FailedIdsWithReason = new Dictionary<string, string>();
          result.StartTime = DateTime.Now;

          Stopwatch stopWatch = new Stopwatch();
          stopWatch.Start();

          foreach (var id in this.listOfIds)
          {
             // FailedIdWithReason failedId = new FailedIdWithReason();

              string idFailureMessage =  String.Empty;
              if (!backgroundWorker.CancellationPending)
              {
                  if (idToFiHelper.GenerateFI(id, this.outputDirectory,this.convertOutputToZip, out idFailureMessage))
                  {
                      //successful
                      
                  }
                  else
                  {
                      //failed
                      result.FailedIdsWithReason.Add(id, idFailureMessage);
                  }

                  completedIdCount++;

                  double percent = Math.Floor((double)(completedIdCount * 100) / this.listOfIds.Count);
                  backgroundWorker.ReportProgress(Convert.ToInt32(percent),id);
              }
              else
              {
                  e.Cancel = true;
                  break;
              }

              
          }
          stopWatch.Stop();
          result.ExecutionTime = stopWatch.Elapsed;

          e.Result = result;   
          
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
            
            lblProgressMessage.Text = e.ProgressPercentage + "%. Current id : " + e.UserState.ToString();
          
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnGenerate.Enabled = true;
            string executionStatusMessage = String.Empty;
            if (e.Cancelled)
            {
                string message =String.Format("User cancelled on {0}", DateTime.Now.ToString());
                lblProgressMessage.Text = message;
                executionStatusMessage = message;

                MessageBox.Show(message);
            }
            else if (e.Error != null)
            {
                executionStatusMessage = String.Format("Exception on {0}. Message : {1}", DateTime.Now.ToString(),e.Error.Message);
            }
            else
            {
                executionStatusMessage = "FI Generation completed.";
                lblProgressMessage.Text = executionStatusMessage;
                
               
                //Log the result.
                if (e.Result != null)
                {
                    FIGenerationResult result = (FIGenerationResult)e.Result;
                    MessageBox.Show(executionStatusMessage + "Failed Ids : " + result.FailedIdsWithReason.Count);
                    //create a log.
                    StringBuilder logBuilder = new StringBuilder();
                    logBuilder.AppendLine(String.Format("---FI Generation ---Started on  {0}--", result.StartTime.ToString()));
                    logBuilder.AppendLine(String.Format("Execution Status   : {0}", executionStatusMessage));
                    logBuilder.AppendLine(String.Format("Execution Time     : {0}", result.ExecutionTime.ToString()));
                    logBuilder.AppendLine(String.Format("Number Of Ids      : {0}", result.NumberOfIds));
                    logBuilder.AppendLine(String.Format("Ids failed         : {0}", result.FailedIdsWithReason.Keys.Count));
                    if (result.FailedIdsWithReason.Keys.Count > 0)
                        logBuilder.AppendLine(String.Format("{0,-10}{1}", "ID", "Description"));
                    foreach (var id in result.FailedIdsWithReason.Keys)
                    {
                        logBuilder.AppendLine(String.Format("{0,-10}{1}", id, result.FailedIdsWithReason[id]));

                    }

                    File.AppendAllText(Path.Combine(this.outputDirectory,"IDToFILog.txt"),logBuilder.ToString());
                }

            }
        }

        #endregion

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (backgroundWorker.WorkerSupportsCancellation)
            {
                if(backgroundWorker.IsBusy)
                 backgroundWorker.CancelAsync();
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog browseFolder = new FolderBrowserDialog())
            {
                if (browseFolder.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                   txtBxFiOutput.Text = browseFolder.SelectedPath;
                }
            }
        }

        private void IDTOFIForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (backgroundWorker.IsBusy)
                {
                    string message = "FI file generation is currently running. Do you want to close the form and stop the process?";
                    if (MessageBox.Show(message, "Stop FI Generation.", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                    {
                        backgroundWorker.CancelAsync();
                        e.Cancel = false;
                    }
                    else
                        e.Cancel = true;
                }
            }
        }


        #region NestedItems

        
        private class FIGenerationResult
        {
            public int NumberOfIds { get; set; }
            public DateTime StartTime { get; set; }
            public TimeSpan ExecutionTime { get; set; }
            public Dictionary<string, string> FailedIdsWithReason { get; set; }
        }
        #endregion
    }
}
