using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using BusinessObject;
using CommonForms;
using Microsoft.Win32;
//Start Add - PAL 
using PickAccessLayer;
using PickData;
using System.Collections;
using System.Collections.Generic;
//End Add - PAL
namespace IRMaestro
{
    public partial class IDCmpSettingsForm : Form
    {
        //Start Add - PAL 
        PickAccessLayer.Pick2DAO pickdb = new Pick2DAO();
        IList<Hashtable> ProjNames = null;
        //End Add - PAL 

        public static bool bInitStatus = false;
        public static string strdb = "";
        public IDCmpSettingsForm()
        {
            InitializeComponent();
            InitSettingsForm();
        }

        private void InitSettingsForm()
        {
            WorkDirText.Text = Configuration.GetWorkingDirectory();
            UDIDbSelCombobox.Items.Add(new TempDBComboBoxItem("UeiTemp", DBConnectionString.UEITEMP));
            string domain = SystemInformation.UserDomainName;
            if (domain == "UEIC")
            {
                UDIDbSelCombobox.Items.Add(new TempDBComboBoxItem("UeiTemp_India", DBConnectionString.UEITEMP_INDIA));
            }
            UDIDbSelCombobox.Items.Add(new TempDBComboBoxItem("UEI_PUBLIC", DBConnectionString.UEIPUBLIC));
            UDIDbSelCombobox.SelectedIndex = 0;
            if (Configuration.GetConfigItem("bKeepCfi") == "True")
                KeepCfischeckBox.Checked = true;
            else
                KeepCfischeckBox.Checked = false;
            if (Configuration.GetConfigItem("dbType") == "UEIDB")
            {
                ueidbradioButton.Checked = true;
                UDIDbSelCombobox.Text = Configuration.GetConfigItem("strDBName");
            }
            else if (Configuration.GetConfigItem("dbType") == "PickDB")
            {
                ProjectcomboBox.Text =Configuration.GetConfigItem("strDBName");
                pickdbradiobtn.Checked = true;                
            }
            else
            {
                ueidbradioButton.Checked = true;
            }
            if (Configuration.GetConfigItem("iRepoType") == "0")
            {
                TxtRepoRadioBtn.Checked = true;
                ExcelRepoRadioBtn.Checked = false;
            }
            else if (Configuration.GetConfigItem("iRepoType") == "1")
            {
                TxtRepoRadioBtn.Checked = false;
                ExcelRepoRadioBtn.Checked = true;
            }
            else
            {
                TxtRepoRadioBtn.Checked = true;
            }
            capsigpathtextBox.Text = Configuration.GetConfigItem("strCapFolder");
            outputpathTxtbox.Text = Configuration.GetConfigItem("strOutputPath");
            projectnameTxtBox.Text = Configuration.GetConfigItem("strProjName");
        }
        private void capDirBrwbutton_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = capsigpathtextBox.Text;
            DialogResult dlgres = folderBrowserDialog1.ShowDialog();
            if (dlgres == DialogResult.OK)
            {
                capsigpathtextBox.Text = folderBrowserDialog1.SelectedPath;
            }

        }
        
        private void Ok_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (ueidbradioButton.Checked == true)
                strdb = "UEI DB";
            if (pickdbradiobtn.Checked == true)
                strdb = "Pick DB";
            string _connectionString = ((TempDBComboBoxItem)UDIDbSelCombobox.SelectedItem).ConnectionString;
            BusinessObject.DAOFactory.ResetDBConnection(_connectionString);

            if (KeepCfischeckBox.Checked == true)
            {
                IDCompareForm.IDCmpSettings.bKeepCfi = true;
            }
            else
            {
                IDCompareForm.IDCmpSettings.bKeepCfi = false;
            }

            if (!Directory.Exists(capsigpathtextBox.Text))
            {
                MessageBox.Show("Select a Valid Captured Signals folder");
                return;
            }
            if (outputpathTxtbox.Text != "")
            {
                if (!Directory.Exists(outputpathTxtbox.Text))
                {
                    MessageBox.Show("Folder does not exists.\r\nSelect a Valid Output path");
                    return;
                }
            }

            if (WorkDirText.Text == Configuration.GetWorkingDirectory())
            {
                IDCompareForm.IDCmpSettings.bIsWorkDirChanged = false;
            }
            else
            {
                IDCompareForm.IDCmpSettings.bIsWorkDirChanged = true;
            }

            if (!Directory.Exists(WorkDirText.Text))
            {
                MessageBox.Show("Select a Valid Working directory");
                return;
            }
            else
            {
                Registry.SetValue("HKEY_CURRENT_USER\\Software\\UEITools\\", "WorkingDirectory",
                                   WorkDirText.Text, RegistryValueKind.String);

            }

            //This is to reset the previous selected IDs
            if (UDIDbSelCombobox.Text != Configuration.GetConfigItem("strDBName"))
            {
                Configuration.SetConfigItem("IdFilterRes", "");
                Configuration.SetConfigItem("IdFilterSel", "");
            }

            IDCompareForm.IDCmpSettings.strWorkDir = WorkDirText.Text;
            IDCompareForm.IDCmpSettings.strCapSigFolder = capsigpathtextBox.Text;
            if (ueidbradioButton.Checked == true)
            {
                IDCompareForm.IDCmpSettings.dbType = IDCompareForm.IDCmpSettings.DBType.UEIDB;
                IDCompareForm.IDCmpSettings.strDBName = UDIDbSelCombobox.SelectedItem.ToString();
            }
            else if (pickdbradiobtn.Checked == true)
            {
                IDCompareForm.IDCmpSettings.dbType = IDCompareForm.IDCmpSettings.DBType.PickDB;
                IDCompareForm.IDCmpSettings.strDBName = ProjectcomboBox.Text;
            }
            if (TxtRepoRadioBtn.Checked == true)
                IDCompareForm.IDCmpSettings.iRepoType = 0;
            else if (ExcelRepoRadioBtn.Checked == true)
                IDCompareForm.IDCmpSettings.iRepoType = 1;
            IDCompareForm.IDCmpSettings.strOutputPath = outputpathTxtbox.Text;
            IDCompareForm.IDCmpSettings.strProjName = projectnameTxtBox.Text;
            Configuration.SetConfigItem("bKeepCfi", IDCompareForm.IDCmpSettings.bKeepCfi.ToString());
            Configuration.SetConfigItem("dbType", IDCompareForm.IDCmpSettings.dbType.ToString());
            Configuration.SetConfigItem("iRepoType", IDCompareForm.IDCmpSettings.iRepoType.ToString());
            Configuration.SetConfigItem("strCapFolder", IDCompareForm.IDCmpSettings.strCapSigFolder);
            Configuration.SetConfigItem("strDBName", IDCompareForm.IDCmpSettings.strDBName);
            Configuration.SetConfigItem("strOutputPath", IDCompareForm.IDCmpSettings.strOutputPath);
            Configuration.SetConfigItem("strProjName", IDCompareForm.IDCmpSettings.strProjName);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            SetOptionsForm setoptform = new SetOptionsForm();
            setoptform.ShowDialog();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void pickdbradiobtn_CheckedChanged(object sender, EventArgs e)
        {
            if (pickdbradiobtn.Checked == true)
            {
                strdb = "Pick DB";
                //Start Add - PAL 
                ProjectcomboBox.Enabled = true;
                ProjNames = PickAccessLayer.PickRetrieveFunctions.GetPickProjectNames();
                ProjectcomboBox.Items.Clear();
                foreach (Hashtable Projs in ProjNames)
                {
                    ProjectcomboBox.Items.Add(Projs["Name"]);
                }
                if (ProjectcomboBox.Items != null)
                    ProjectcomboBox.Text = Configuration.GetConfigItem("strDBName");//ProjectcomboBox.SelectedIndex = 0;
                //End Add - PAL 
            }
            else if (pickdbradiobtn.Checked == false)
            {
                ProjectcomboBox.Enabled = false;
            }

        }
        private void ueidbradioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (ueidbradioButton.Checked == true)
                strdb = UDIDbSelCombobox.SelectedItem.ToString();
        }
        private void wDirBrwbutton_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = WorkDirText.Text;
            DialogResult dlgres = folderBrowserDialog1.ShowDialog();
            if (dlgres == DialogResult.OK)
            {
                WorkDirText.Text = folderBrowserDialog1.SelectedPath;
            }
        }
        private class TempDBComboBoxItem
        {
            private string _caption;
            private string _connectionString;
            public TempDBComboBoxItem(string caption, string connectionString)
            {
                _caption = caption;
                _connectionString = connectionString;
            }
            public string ConnectionString
            {
                get { return _connectionString; }
            }
            public override string ToString()
            {
                return _caption;
            }
        }
        private void button6_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = outputpathTxtbox.Text;
            DialogResult dlgres = folderBrowserDialog1.ShowDialog();
            if (dlgres == DialogResult.OK)
            {
                outputpathTxtbox.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void ProjectcomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}