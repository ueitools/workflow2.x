namespace IRMaestro {
    partial class MacroAnalyzerForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MacroAnalyzerForm));
            this.IdListTextBox = new System.Windows.Forms.TextBox();
            this.SearchResultsTreeView = new System.Windows.Forms.TreeView();
            this.SelectIdButton = new System.Windows.Forms.Button();
            this.CaptureButton = new System.Windows.Forms.Button();
            this.ResetButton = new System.Windows.Forms.Button();
            this.SetButton = new System.Windows.Forms.Button();
            this.Use1MHzCheckBox = new System.Windows.Forms.CheckBox();
            this.CaptureSettingsButton = new System.Windows.Forms.Button();
            this.CapturePodComboBox = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripDBType = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripPickDBPath = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripPodNo = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // IdListTextBox
            // 
            this.IdListTextBox.Location = new System.Drawing.Point(17, 16);
            this.IdListTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IdListTextBox.Name = "IdListTextBox";
            this.IdListTextBox.Size = new System.Drawing.Size(239, 22);
            this.IdListTextBox.TabIndex = 0;
            this.IdListTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IdListTextBox_KeyDown);
            this.IdListTextBox.Leave += new System.EventHandler(this.IdListTextBox_Leave);
            // 
            // SearchResultsTreeView
            // 
            this.SearchResultsTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchResultsTreeView.Location = new System.Drawing.Point(17, 49);
            this.SearchResultsTreeView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.SearchResultsTreeView.Name = "SearchResultsTreeView";
            this.SearchResultsTreeView.Size = new System.Drawing.Size(1099, 666);
            this.SearchResultsTreeView.TabIndex = 1;
            // 
            // SelectIdButton
            // 
            this.SelectIdButton.Location = new System.Drawing.Point(441, 12);
            this.SelectIdButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.SelectIdButton.Name = "SelectIdButton";
            this.SelectIdButton.Size = new System.Drawing.Size(100, 28);
            this.SelectIdButton.TabIndex = 2;
            this.SelectIdButton.Text = "Select IDs";
            this.SelectIdButton.UseVisualStyleBackColor = true;
            this.SelectIdButton.Visible = false;
            this.SelectIdButton.Click += new System.EventHandler(this.SelectIdButton_Click);
            // 
            // CaptureButton
            // 
            this.CaptureButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CaptureButton.Location = new System.Drawing.Point(755, 12);
            this.CaptureButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CaptureButton.Name = "CaptureButton";
            this.CaptureButton.Size = new System.Drawing.Size(100, 28);
            this.CaptureButton.TabIndex = 3;
            this.CaptureButton.Text = "Capture";
            this.CaptureButton.UseVisualStyleBackColor = true;
            this.CaptureButton.Click += new System.EventHandler(this.CaptureButton_Click);
            // 
            // ResetButton
            // 
            this.ResetButton.Location = new System.Drawing.Point(353, 14);
            this.ResetButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(80, 28);
            this.ResetButton.TabIndex = 5;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // SetButton
            // 
            this.SetButton.Location = new System.Drawing.Point(265, 14);
            this.SetButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.SetButton.Name = "SetButton";
            this.SetButton.Size = new System.Drawing.Size(80, 28);
            this.SetButton.TabIndex = 6;
            this.SetButton.Text = "Set";
            this.SetButton.UseVisualStyleBackColor = true;
            this.SetButton.Click += new System.EventHandler(this.SetButton_Click);
            // 
            // Use1MHzCheckBox
            // 
            this.Use1MHzCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Use1MHzCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.Use1MHzCheckBox.AutoSize = true;
            this.Use1MHzCheckBox.Location = new System.Drawing.Point(977, 12);
            this.Use1MHzCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Use1MHzCheckBox.Name = "Use1MHzCheckBox";
            this.Use1MHzCheckBox.Size = new System.Drawing.Size(54, 27);
            this.Use1MHzCheckBox.TabIndex = 7;
            this.Use1MHzCheckBox.Text = "1MHz";
            this.Use1MHzCheckBox.UseVisualStyleBackColor = true;
            this.Use1MHzCheckBox.Visible = false;
            this.Use1MHzCheckBox.CheckedChanged += new System.EventHandler(this.Use1MHzCheckBox_CheckedChanged);
            // 
            // CaptureSettingsButton
            // 
            this.CaptureSettingsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CaptureSettingsButton.Location = new System.Drawing.Point(1039, 12);
            this.CaptureSettingsButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CaptureSettingsButton.Name = "CaptureSettingsButton";
            this.CaptureSettingsButton.Size = new System.Drawing.Size(79, 28);
            this.CaptureSettingsButton.TabIndex = 9;
            this.CaptureSettingsButton.Text = "Setting...";
            this.CaptureSettingsButton.UseVisualStyleBackColor = true;
            this.CaptureSettingsButton.Click += new System.EventHandler(this.CaptureSettingsButton_Click);
            // 
            // CapturePodComboBox
            // 
            this.CapturePodComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CapturePodComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CapturePodComboBox.FormattingEnabled = true;
            this.CapturePodComboBox.Location = new System.Drawing.Point(863, 14);
            this.CapturePodComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CapturePodComboBox.Name = "CapturePodComboBox";
            this.CapturePodComboBox.Size = new System.Drawing.Size(99, 24);
            this.CapturePodComboBox.TabIndex = 13;
            this.CapturePodComboBox.SelectedIndexChanged += new System.EventHandler(this.CapturePodComboBox_SelectedIndexChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDBType,
            this.toolStripPickDBPath,
            this.toolStripPodNo});
            this.statusStrip1.Location = new System.Drawing.Point(0, 723);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1133, 22);
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripDBType
            // 
            this.toolStripDBType.Name = "toolStripDBType";
            this.toolStripDBType.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripPickDBPath
            // 
            this.toolStripPickDBPath.Name = "toolStripPickDBPath";
            this.toolStripPickDBPath.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripPodNo
            // 
            this.toolStripPodNo.Name = "toolStripPodNo";
            this.toolStripPodNo.Size = new System.Drawing.Size(0, 17);
            // 
            // MacroAnalyzerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1133, 745);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.CapturePodComboBox);
            this.Controls.Add(this.CaptureSettingsButton);
            this.Controls.Add(this.Use1MHzCheckBox);
            this.Controls.Add(this.SelectIdButton);
            this.Controls.Add(this.SetButton);
            this.Controls.Add(this.SearchResultsTreeView);
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.CaptureButton);
            this.Controls.Add(this.IdListTextBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimumSize = new System.Drawing.Size(666, 236);
            this.Name = "MacroAnalyzerForm";
            this.Text = "Macro Analyzer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MacroAnalyzerForm_FormClosing);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox IdListTextBox;
        private System.Windows.Forms.TreeView SearchResultsTreeView;
        private System.Windows.Forms.Button SelectIdButton;
        private System.Windows.Forms.Button CaptureButton;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.Button SetButton;
        private System.Windows.Forms.CheckBox Use1MHzCheckBox;
        private System.Windows.Forms.Button CaptureSettingsButton;
        private System.Windows.Forms.ComboBox CapturePodComboBox;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripDBType;
        private System.Windows.Forms.ToolStripStatusLabel toolStripPickDBPath;
        private System.Windows.Forms.ToolStripStatusLabel toolStripPodNo;
    }
}