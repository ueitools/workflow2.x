using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;
using Microsoft.Win32;
using CommonForms;
using System.ComponentModel;

namespace IRMaestro
{
    public partial class CompareReportForm : Form
    {
        public const string PROJ_ROOT =
            "HKEY_CURRENT_USER\\Software\\UEITools\\ReadBack";
        private const string ROW_FORMAT = "{0,-10}{1,-12}{2,-13}{3,-13}{4,-8}{5}\r\n";
        private const string HEADER = "                    Matched     Unmatched\r\n" +
                                      "Exec       ID      Functions    Functions    Warnings   Warning Messages\r\n" +
                                      "-------   -----   -----------  -----------  ----------  -----------------------\r\n";

        private static PrinterSettings printerSettings = new PrinterSettings();
        private static PageSettings pageSettings = new PageSettings();
        private static StringReader streamToPrint;
        private static Font printFont;

        private List<CompareRes> results;
        private string workPath;
        private string reportFile = "results.rpt";

        public CompareReportForm(string workPath)
        {
            InitializeComponent();
            this.workPath = workPath;
        }

        public CompareReportForm(string workPath, string fileName)
            : this(workPath)
        {
            this.reportFile = fileName;
        }

        public CompareReportForm(string workPath,
            List<CompareRes> results)
            : this(workPath)
        {
            this.results = results;            
        }

        public static void PageSetup()
        {
            PageSetupDialog psd = new PageSetupDialog();

            psd.PageSettings = pageSettings;
            psd.PrinterSettings = printerSettings;
            psd.ShowDialog();
            Registry.SetValue(PROJ_ROOT, "PageSettings", pageSettings);
        }

        public static void PrintReport(string ReportText)
        {
            streamToPrint = new System.IO.StringReader(ReportText);
            printFont = new System.Drawing.Font("Courier New", 10,
                System.Drawing.FontStyle.Regular);

            PrintDocument doc = new PrintDocument();
            doc.PrintPage += new PrintPageEventHandler(document_PrintPage);
            doc.PrinterSettings = printerSettings;
            doc.DefaultPageSettings = pageSettings;

            PrintDialog dlg = new PrintDialog();
            dlg.Document = doc;
            dlg.PrinterSettings = printerSettings;

            DialogResult result = dlg.ShowDialog();

            if (result != DialogResult.Cancel)
            {
                doc.Print();
            }
        }

        private static void document_PrintPage(object sender,
            PrintPageEventArgs ev)
        {
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            string line = null;

            // Calculate the number of lines per page.
            linesPerPage = ev.MarginBounds.Height /
               printFont.GetHeight(ev.Graphics);

            // Print each line of the file.
            while (count < linesPerPage &&
               ((line = streamToPrint.ReadLine()) != null))
            {
                yPos = topMargin + (count *
                   printFont.GetHeight(ev.Graphics));
                ev.Graphics.DrawString(line, printFont, Brushes.Black,
                   leftMargin, yPos, new StringFormat());
                count++;
            }

            // If more lines exist, print another page.
            if (line != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false;
        }

        private void OK_Click(object sender, EventArgs e)
        {
            string contents = HEADER;
            for (int i = 0; i < reportDataGrid.RowCount; i++)
            {
                contents += String.Format(ROW_FORMAT,
                    reportDataGrid["Exec", i].Value,
                    reportDataGrid["ID", i].Value,
                    reportDataGrid["Matched", i].Value,
                    reportDataGrid["Unmatched", i].Value,
                    reportDataGrid["Warnings", i].Value,
                    reportDataGrid["WarningMessages", i].Value);
            }

            File.WriteAllText(workPath + reportFile, contents);

            this.Close();
        }

        private void CompareReportForm_Shown(object sender, EventArgs e)
        {
            this.Update();

            if (results == null)
            {
                if (!File.Exists(workPath + reportFile))
                    return;

                StreamReader sw = new StreamReader(workPath + reportFile);
                results = new List<CompareRes>();

                string buf;

                while ((buf = sw.ReadLine()) != null)
                {
                    if (buf.Trim() == "")
                        continue;
                    if (buf.Contains("Matched")
                        || buf.Contains("Functions")
                        || buf.Contains("--------------"))
                        continue;

                    string[] strs = buf.Split(new char[] { ' ' }, 6,
                        StringSplitOptions.RemoveEmptyEntries);

                    if (strs.Length < 4)
                        continue;

                    CompareRes res = new CompareRes();
                    res.Exec = strs[0];
                    res.ID = strs[1];

                    try
                    {
                        res.matchedFunctions = int.Parse(strs[2]);
                        res.unMatchedFunctions = int.Parse(strs[3]);
                        if (strs.Length == 6)
                            res.SetWarnings(int.Parse(strs[4]), strs[5]);
                    }
                    catch (Exception)
                    {
                        continue;
                    }

                    reportDataGrid.Rows.Add(res.Exec, res.ID,
                        res.matchedFunctions,
                        res.unMatchedFunctions,
                        res.Warnings, res.GetWarningMessages());
                    results.Add(res);
                }

                sw.Close();
            }
            else
            {
                string contents = HEADER;
                foreach (CompareRes res in results)
                {
                    reportDataGrid.Rows.Add(res.Exec, res.ID,
                        res.matchedFunctions,
                        res.unMatchedFunctions,
                        res.Warnings, res.GetWarningMessages());
                    contents += String.Format(ROW_FORMAT,
                        res.Exec, res.ID, res.matchedFunctions, res.unMatchedFunctions, res.Warnings, res.GetWarningMessages());
                }

                File.WriteAllText(workPath + reportFile, contents);

                //Begin Merge results module
                string strMergeRptOpt = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                         UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL);

                if (CFIListForm.bShowTimingReport)
                {
                    strMergeRptOpt = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                           UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL);
                }
                if (strMergeRptOpt == "1")
                {
                    string prevIDlist = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                             UserSettings.ConfigKeys.MERGE_REPORT_APPEND);

                    if (CFIListForm.bShowTimingReport)
                    {
                        prevIDlist = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                               UserSettings.ConfigKeys.MERGE_REPORT_APPEND);
                    }


                    if (prevIDlist == null)
                        return;

                    prevIDlist = prevIDlist.TrimEnd(',');
                    List<String> skippedIDlist = new List<String>(prevIDlist.Split(','));                     
                    ShowOnlyNewRunIDs(skippedIDlist);
                }
                //End Merge results module             
            }
            DoSorting();
        }

        private void ShowOnlyNewRunIDs(List<String> skippedIDlist)
        {
            reportDataGrid.Rows.Clear();
            if (results != null)
            {
                if (!File.Exists(workPath + reportFile))
                    return;
                StreamReader sw = new StreamReader(workPath + reportFile);
                results = new List<CompareRes>();
                string buf;

                while ((buf = sw.ReadLine()) != null)
                {
                    if (buf.Trim() == "")
                        continue;
                    if (buf.Contains("Matched")
                        || buf.Contains("Functions")
                        || buf.Contains("--------------"))
                        continue;
                    string[] strs = buf.Split(new char[] { ' ' }, 6,
                        StringSplitOptions.RemoveEmptyEntries);
                    if (strs.Length < 4)
                        continue;
                    
                    if(skippedIDlist.Contains(strs[1]))
                        continue;

                    CompareRes res = new CompareRes();
                    res.Exec = strs[0];
                    res.ID = strs[1];


                    try
                    {
                        res.matchedFunctions = int.Parse(strs[2]);
                        res.unMatchedFunctions = int.Parse(strs[3]);
                        if (strs.Length == 6)
                            res.SetWarnings(int.Parse(strs[4]), strs[5]);
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                    reportDataGrid.Rows.Add(res.Exec, res.ID,
                        res.matchedFunctions,
                        res.unMatchedFunctions,
                        res.Warnings, res.GetWarningMessages());
                    results.Add(res);
                }
                sw.Close();
            }
        }

        private void report_CellDoubleClick(object sender,
            DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0
                || e.RowIndex >= reportDataGrid.RowCount)
                return;
            CFIListForm cfiListForm = new CFIListForm(workPath,
                 (string)reportDataGrid["ID", e.RowIndex].Value);
            cfiListForm.ShowDialog();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            //If Merged repot Do nothing.
            string strMergeRpt = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                        UserSettings.ConfigKeys.MERGE_REPORT);
            if (strMergeRpt == "0")
            {
                string contents = HEADER;
                for (int i = 0; i < reportDataGrid.RowCount; i++)
                {
                    contents += String.Format(ROW_FORMAT,
                        reportDataGrid["Exec", i].Value,
                        reportDataGrid["ID", i].Value,
                        reportDataGrid["Matched", i].Value,
                        reportDataGrid["Unmatched", i].Value,
                        reportDataGrid["Warnings", i].Value,
                        reportDataGrid["WarningMessages", i].Value);
                }

                if (ShowOnlyUMButton.Checked)
                {
                    DialogResult res;
                    res = MessageBox.Show("Do you want to save the Report?", "Message", MessageBoxButtons.YesNo);
                    if (res == DialogResult.No)
                        this.Close();
                    string strFileExt = Path.GetExtension(reportFile);
                    string strFileExt1 = "(*" + strFileExt + ") |*" + strFileExt;
                    saveFileDialog1.Filter = strFileExt1;
                    saveFileDialog1.Filter += "|All File Types (*.*)|*.*";
                    saveFileDialog1.DefaultExt = strFileExt;
                    saveFileDialog1.OverwritePrompt = true;
                    res = saveFileDialog1.ShowDialog();
                    if (res == DialogResult.OK)
                    {
                        string sFilePath = saveFileDialog1.FileName;
                        File.WriteAllText(sFilePath, contents);
                    }
                }
                else
                {
                    File.WriteAllText(workPath + reportFile, contents);
                }
            }
            this.Close();
        }

        private void printRptTSButton_Click(object sender, EventArgs e)
        {
            string contents = HEADER;
            for (int i = 0; i < reportDataGrid.RowCount; i++)
            {
                contents += String.Format(ROW_FORMAT,
                    reportDataGrid["Exec", i].Value,
                    reportDataGrid["ID", i].Value,
                    reportDataGrid["Matched", i].Value,
                    reportDataGrid["Unmatched", i].Value,
                    reportDataGrid["Warnings", i].Value,
                    reportDataGrid["WarningMessages", i].Value);
            }

            PrintReport(contents);
        }

        private void PageSetupTSButton_Click(object sender, EventArgs e)
        {
            PageSetup();
        }

        private void CompareReportForm_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (CFIListForm.bShowTimingReport == false)
            {
                UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.RPT_SORT_COLOUMN, reportDataGrid.SortedColumn.Index.ToString());
                UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.RPT_SORT_ORDER, reportDataGrid.SortOrder.ToString());
            }
            else
            {
                UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.RPT_SORT_COLOUMN, reportDataGrid.SortedColumn.Index.ToString());
                UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.RPT_SORT_ORDER, reportDataGrid.SortOrder.ToString());
            }

        }

        private void DoSorting()
        {
            int iColoumnIndex = 1;
            ListSortDirection direction;
            string SortedOrder = "Ascending";
            direction = ListSortDirection.Ascending;
            if (CFIListForm.bShowTimingReport == false)
            {
                iColoumnIndex = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.RPT_SORT_COLOUMN, 1);
                SortedOrder = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.RPT_SORT_ORDER);
            }
            else
            {
                iColoumnIndex = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.RPT_SORT_COLOUMN, 1);
                SortedOrder = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.RPT_SORT_ORDER);
            }
            if (SortedOrder == "Ascending")
                direction = ListSortDirection.Ascending;
            else if (SortedOrder == "Descending")
                direction = ListSortDirection.Descending;
            DataGridViewColumn newColumn = reportDataGrid.Columns[iColoumnIndex];
            if (newColumn != null)
            {
                reportDataGrid.Sort(newColumn, direction);
                newColumn.HeaderCell.SortGlyphDirection =
                    direction == ListSortDirection.Ascending ?
                    SortOrder.Ascending : SortOrder.Descending;
            }
        }

        private void ShowOnlyUMButton_Click(object sender, EventArgs e)
        {
            
            //this.Update();
            if (ShowOnlyUMButton.Checked)
            {
               reportDataGrid.Rows.Clear();
                if (results != null)
                {
                    if (!File.Exists(workPath + reportFile))
                        return;
                    StreamReader sw = new StreamReader(workPath + reportFile);
                    results = new List<CompareRes>();
                    string buf;
                    while ((buf = sw.ReadLine()) != null)
                    {
                        if (buf.Trim() == "")
                            continue;
                        if (buf.Contains("Matched")
                            || buf.Contains("Functions")
                            || buf.Contains("--------------"))
                            continue;
                        string[] strs = buf.Split(new char[] { ' ' }, 6,
                            StringSplitOptions.RemoveEmptyEntries);
                        if (strs.Length < 4)
                            continue;
                        if (strs[3] == "0")
                            continue;
                        CompareRes res = new CompareRes();
                        res.Exec = strs[0];
                        res.ID = strs[1];
                        try
                        {
                            res.matchedFunctions = int.Parse(strs[2]);
                            res.unMatchedFunctions = int.Parse(strs[3]);
                            if (strs.Length == 6)
                                res.SetWarnings(int.Parse(strs[4]), strs[5]);
                        }
                        catch (Exception)
                        {
                            continue;
                        }
                        reportDataGrid.Rows.Add(res.Exec, res.ID,
                            res.matchedFunctions,
                            res.unMatchedFunctions,
                            res.Warnings, res.GetWarningMessages());
                        results.Add(res);
                    }
                    sw.Close();
                }
                DoSorting();
            }
            else
            {
                //this.Update();
                reportDataGrid.Rows.Clear();

                if (!File.Exists(workPath + reportFile))
                    return;

                StreamReader sw = new StreamReader(workPath + reportFile);
                results = new List<CompareRes>();

                string buf;

                while ((buf = sw.ReadLine()) != null)
                {
                    if (buf.Trim() == "")
                        continue;
                    if (buf.Contains("Matched")
                        || buf.Contains("Functions")
                        || buf.Contains("--------------"))
                        continue;

                    string[] strs = buf.Split(new char[] { ' ' }, 6,
                        StringSplitOptions.RemoveEmptyEntries);

                    if (strs.Length < 4)
                        continue;

                    //Begin Merge results module
                    string strMergeRptOpt = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                             UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL);

                    if (CFIListForm.bShowTimingReport)
                    {
                        strMergeRptOpt = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                               UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL);
                    }

                    if (strMergeRptOpt == "1")
                    {
                        string prevIDlist = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                                 UserSettings.ConfigKeys.MERGE_REPORT_APPEND);

                        if (CFIListForm.bShowTimingReport)
                        {
                            prevIDlist = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                                   UserSettings.ConfigKeys.MERGE_REPORT_APPEND);
                        }
                        if (prevIDlist == null)
                            return;

                        prevIDlist = prevIDlist.TrimEnd(',');
                        List<String> skippedIDlist = new List<String>(prevIDlist.Split(','));
                        
                        if(skippedIDlist.Contains(strs[1]))
                            continue;
                    }
                    //End Merge results module 

                    CompareRes res = new CompareRes();
                    res.Exec = strs[0];
                    res.ID = strs[1];
                    
                    try
                    {
                        res.matchedFunctions = int.Parse(strs[2]);
                        res.unMatchedFunctions = int.Parse(strs[3]);
                        if (strs.Length == 6)
                            res.SetWarnings(int.Parse(strs[4]), strs[5]);
                    }
                    catch (Exception)
                    {
                        continue;
                    }

                    reportDataGrid.Rows.Add(res.Exec, res.ID,
                        res.matchedFunctions,
                        res.unMatchedFunctions,
                        res.Warnings, res.GetWarningMessages());
                    results.Add(res);
                }
                sw.Close();
                DoSorting();
            }
        }

        private void reportDataGrid_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (CFIListForm.bShowTimingReport == false)
            {
                UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.RPT_SORT_COLOUMN, reportDataGrid.SortedColumn.Index.ToString());
                UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.RPT_SORT_ORDER, reportDataGrid.SortOrder.ToString());
            }
            else
            {
                UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.RPT_SORT_COLOUMN, reportDataGrid.SortedColumn.Index.ToString());
                UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.RPT_SORT_ORDER, reportDataGrid.SortOrder.ToString());
            }
        }
    
    }

    #region Data Type Definitions

    public class CompareRes {
        private const string NON_FUNCTIONAL_MESSAGE = "'Non-functional' functions";
        private const string HIGH_SPEED_CAPTURE_MESSAGE = "High frequency signal";
        private const string NO_READBACK_MESSAGE = "No Readback file found";
        private const string NO_EXECUTOR_MESSAGE = "Executor not implemented";

        public enum WarningType {
            General = 0x01,
            NonFunctional = 0x02,
            HighSpeedCapture = 0x04,
            NoReadback = 0x08,
            NoExecutor = 0x16,
        }

        private int _warnings;
        private byte _warningsTypes;
        private List<string> _warningMessages;

        public string Exec;
        public string ID;
        public int matchedFunctions;
        public int unMatchedFunctions;
        public List<string> cfiFileList;

        public CompareRes() {
            _warnings = 0;
            _warningsTypes = 0;
            _warningMessages = new List<string>();
        }

        public int Warnings
        {
            get { return _warnings; }
        }

        public void AddWarning(WarningType warningType) {
            _warnings++;
            if ((_warningsTypes & (byte)warningType) == 0) {
                switch (warningType) {
                    case WarningType.NonFunctional:
                        if (_warningMessages.Contains(NON_FUNCTIONAL_MESSAGE) == false)
                            _warningMessages.Add(NON_FUNCTIONAL_MESSAGE);
                        break;
                    case WarningType.HighSpeedCapture:
                        if (_warningMessages.Contains(HIGH_SPEED_CAPTURE_MESSAGE) == false)
                            _warningMessages.Add(HIGH_SPEED_CAPTURE_MESSAGE);
                        break;
                    case WarningType.NoReadback:
                        if (_warningMessages.Contains(NO_READBACK_MESSAGE) == false)
                            _warningMessages.Add(NO_READBACK_MESSAGE);
                        break;
                    case WarningType.NoExecutor:
                        if (_warningMessages.Contains(NO_EXECUTOR_MESSAGE) == false)
                            _warningMessages.Add(NO_EXECUTOR_MESSAGE);
                        break;
                }
            }
        }

        public string GetWarningMessages()
        {
            return string.Join(", ", _warningMessages.ToArray());
        }

        public void SetWarnings(int warnings, string warningMessages)
        {
            _warnings = warnings;
            string[] strs = warningMessages.Split(new char[] { ',' }, 6, StringSplitOptions.RemoveEmptyEntries);
            if (strs.Length > 0)
            {
                _warningMessages.Clear();
                foreach (string message in strs)
                {
                    _warningMessages.Add(message.Trim());
                }
            }

        }
    }

    #endregion
}