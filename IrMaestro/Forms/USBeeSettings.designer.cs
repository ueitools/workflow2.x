namespace IRMaestro
{
    partial class USBeeSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(USBeeSettings));
            this.lblPodNo = new System.Windows.Forms.Label();
            this.txtPodNo = new System.Windows.Forms.TextBox();
            this.btnSet = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblPodNo
            // 
            this.lblPodNo.AutoSize = true;
            this.lblPodNo.Location = new System.Drawing.Point(16, 31);
            this.lblPodNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPodNo.Name = "lblPodNo";
            this.lblPodNo.Size = new System.Drawing.Size(87, 17);
            this.lblPodNo.TabIndex = 0;
            this.lblPodNo.Text = "Pod Number";
            // 
            // txtPodNo
            // 
            this.txtPodNo.Location = new System.Drawing.Point(113, 31);
            this.txtPodNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPodNo.Name = "txtPodNo";
            this.txtPodNo.Size = new System.Drawing.Size(239, 22);
            this.txtPodNo.TabIndex = 1;
            this.txtPodNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtPodNo_Validating);
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(361, 28);
            this.btnSet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(80, 28);
            this.btnSet.TabIndex = 2;
            this.btnSet.Text = "Set";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // USBeeSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(457, 89);
            this.Controls.Add(this.btnSet);
            this.Controls.Add(this.txtPodNo);
            this.Controls.Add(this.lblPodNo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "USBeeSettings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.USBeeSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPodNo;
        private System.Windows.Forms.TextBox txtPodNo;
        private System.Windows.Forms.Button btnSet;
    }
}