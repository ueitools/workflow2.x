namespace IRMaestro.Forms
{
    partial class ProjectNameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.productComboBox = new System.Windows.Forms.ComboBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pathTextBox = new System.Windows.Forms.TextBox();
            this.internalCheckBox = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.idSelectionControl1 = new CommonForms.Forms.IDSelectionControl();
            this.dbGroupBox = new System.Windows.Forms.GroupBox();
            this.dbSelectorComboBox = new CommonForms.Forms.DatabaseSelectorComboBox();
            this.groupBox1.SuspendLayout();
            this.dbGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "To Project:";
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(31, 243);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 2;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(134, 243);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // productComboBox
            // 
            this.productComboBox.FormattingEnabled = true;
            this.productComboBox.Location = new System.Drawing.Point(11, 142);
            this.productComboBox.Name = "productComboBox";
            this.productComboBox.Size = new System.Drawing.Size(354, 21);
            this.productComboBox.Sorted = true;
            this.productComboBox.TabIndex = 7;
            // 
            // browseButton
            // 
            this.browseButton.Location = new System.Drawing.Point(290, 42);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(75, 23);
            this.browseButton.TabIndex = 8;
            this.browseButton.Text = "Browse";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "From Pick DB Directory:";
            // 
            // pathTextBox
            // 
            this.pathTextBox.Location = new System.Drawing.Point(11, 43);
            this.pathTextBox.Name = "pathTextBox";
            this.pathTextBox.Size = new System.Drawing.Size(273, 20);
            this.pathTextBox.TabIndex = 10;
            // 
            // internalCheckBox
            // 
            this.internalCheckBox.AutoSize = true;
            this.internalCheckBox.Checked = true;
            this.internalCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.internalCheckBox.Location = new System.Drawing.Point(34, 208);
            this.internalCheckBox.Name = "internalCheckBox";
            this.internalCheckBox.Size = new System.Drawing.Size(101, 17);
            this.internalCheckBox.TabIndex = 11;
            this.internalCheckBox.Text = "Mark as internal";
            this.internalCheckBox.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "To Database:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.idSelectionControl1);
            this.groupBox1.Location = new System.Drawing.Point(394, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(410, 284);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ID Selection";
            // 
            // idSelectionControl1
            // 
            this.idSelectionControl1.Location = new System.Drawing.Point(5, 19);
            this.idSelectionControl1.Name = "idSelectionControl1";
            this.idSelectionControl1.Size = new System.Drawing.Size(390, 255);
            this.idSelectionControl1.TabIndex = 14;
            // 
            // dbGroupBox
            // 
            this.dbGroupBox.Controls.Add(this.dbSelectorComboBox);
            this.dbGroupBox.Controls.Add(this.label2);
            this.dbGroupBox.Controls.Add(this.label1);
            this.dbGroupBox.Controls.Add(this.productComboBox);
            this.dbGroupBox.Controls.Add(this.label3);
            this.dbGroupBox.Controls.Add(this.browseButton);
            this.dbGroupBox.Controls.Add(this.pathTextBox);
            this.dbGroupBox.Location = new System.Drawing.Point(12, 5);
            this.dbGroupBox.Name = "dbGroupBox";
            this.dbGroupBox.Size = new System.Drawing.Size(376, 189);
            this.dbGroupBox.TabIndex = 16;
            this.dbGroupBox.TabStop = false;
            this.dbGroupBox.Text = "Database";
            // 
            // dbSelectorComboBox
            // 
            this.dbSelectorComboBox.Location = new System.Drawing.Point(11, 91);
            this.dbSelectorComboBox.Name = "dbSelectorComboBox";
            this.dbSelectorComboBox.Size = new System.Drawing.Size(354, 28);
            this.dbSelectorComboBox.TabIndex = 13;
            // 
            // ProjectNameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 309);
            this.Controls.Add(this.dbGroupBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.internalCheckBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Name = "ProjectNameForm";
            this.Text = "Transfer Pick Database";
            this.Load += new System.EventHandler(this.ProjectNameForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.dbGroupBox.ResumeLayout(false);
            this.dbGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ComboBox productComboBox;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox pathTextBox;
        private System.Windows.Forms.CheckBox internalCheckBox;
        private System.Windows.Forms.Label label3;
        private CommonForms.Forms.IDSelectionControl idSelectionControl1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox dbGroupBox;
        private CommonForms.Forms.DatabaseSelectorComboBox dbSelectorComboBox;
    }
}