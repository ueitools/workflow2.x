namespace IRMaestro
{
    partial class CFIListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CFIListForm));
            this.CFIListBox = new System.Windows.Forms.ListBox();
            this.CompareTextBox = new System.Windows.Forms.RichTextBox();
            this.OkButton = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.printRptTSButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.PageSetupTSButton = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CFIListBox
            // 
            this.CFIListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.CFIListBox.FormattingEnabled = true;
            this.CFIListBox.ItemHeight = 16;
            this.CFIListBox.Location = new System.Drawing.Point(0, 31);
            this.CFIListBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CFIListBox.Name = "CFIListBox";
            this.CFIListBox.Size = new System.Drawing.Size(159, 500);
            this.CFIListBox.TabIndex = 0;
            this.CFIListBox.SelectedIndexChanged += new System.EventHandler(this.CFIListBox_SelectedIndexChanged);
            // 
            // CompareTextBox
            // 
            this.CompareTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CompareTextBox.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CompareTextBox.Location = new System.Drawing.Point(164, 31);
            this.CompareTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CompareTextBox.Name = "CompareTextBox";
            this.CompareTextBox.ReadOnly = true;
            this.CompareTextBox.Size = new System.Drawing.Size(416, 500);
            this.CompareTextBox.TabIndex = 1;
            this.CompareTextBox.Text = "";
            // 
            // OkButton
            // 
            this.OkButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.OkButton.Location = new System.Drawing.Point(217, 559);
            this.OkButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(100, 28);
            this.OkButton.TabIndex = 2;
            this.OkButton.Text = "Ok";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printRptTSButton,
            this.toolStripSeparator5,
            this.PageSetupTSButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(581, 25);
            this.toolStrip1.TabIndex = 68;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // printRptTSButton
            // 
            this.printRptTSButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printRptTSButton.Image = ((System.Drawing.Image)(resources.GetObject("printRptTSButton.Image")));
            this.printRptTSButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printRptTSButton.Name = "printRptTSButton";
            this.printRptTSButton.Size = new System.Drawing.Size(23, 22);
            this.printRptTSButton.Text = "Print";
            this.printRptTSButton.Click += new System.EventHandler(this.printRptTSButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Margin = new System.Windows.Forms.Padding(11, 0, 11, 0);
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // PageSetupTSButton
            // 
            this.PageSetupTSButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PageSetupTSButton.Image = ((System.Drawing.Image)(resources.GetObject("PageSetupTSButton.Image")));
            this.PageSetupTSButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PageSetupTSButton.Name = "PageSetupTSButton";
            this.PageSetupTSButton.Size = new System.Drawing.Size(23, 22);
            this.PageSetupTSButton.Text = "Page Setup";
            this.PageSetupTSButton.Click += new System.EventHandler(this.PageSetupTSButton_Click);
            // 
            // CFIListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.OkButton;
            this.ClientSize = new System.Drawing.Size(581, 608);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.CompareTextBox);
            this.Controls.Add(this.CFIListBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "CFIListForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CFI List";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.CFIListForm_Shown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox CFIListBox;
        private System.Windows.Forms.RichTextBox CompareTextBox;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton printRptTSButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton PageSetupTSButton;
    }
}