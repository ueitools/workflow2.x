namespace IRMaestro
{
    partial class SetOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetOptionsForm));
            this.TimingGroupBox = new System.Windows.Forms.GroupBox();
            this.MaskToggleBitsCheckBox = new System.Windows.Forms.CheckBox();
            this.CompFrameNoCheckBox = new System.Windows.Forms.CheckBox();
            this.CodeGapTextBox = new System.Windows.Forms.TextBox();
            this.FrequencyTextBox = new System.Windows.Forms.TextBox();
            this.PulseDelayDurationTextBox = new System.Windows.Forms.TextBox();
            this.DelayDurationTextBox = new System.Windows.Forms.TextBox();
            this.PulseDurationTextBox = new System.Windows.Forms.TextBox();
            this.CodeGap = new System.Windows.Forms.CheckBox();
            this.Frequency = new System.Windows.Forms.CheckBox();
            this.PulseDelayDuration = new System.Windows.Forms.CheckBox();
            this.DelayDuration = new System.Windows.Forms.CheckBox();
            this.PulseDuration = new System.Windows.Forms.CheckBox();
            this.OkButton = new System.Windows.Forms.Button();
            this.Cancelutton = new System.Windows.Forms.Button();
            this.keepCfiCheckbox = new System.Windows.Forms.CheckBox();
            this.TimingGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // TimingGroupBox
            // 
            this.TimingGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TimingGroupBox.Controls.Add(this.MaskToggleBitsCheckBox);
            this.TimingGroupBox.Controls.Add(this.CompFrameNoCheckBox);
            this.TimingGroupBox.Controls.Add(this.CodeGapTextBox);
            this.TimingGroupBox.Controls.Add(this.FrequencyTextBox);
            this.TimingGroupBox.Controls.Add(this.PulseDelayDurationTextBox);
            this.TimingGroupBox.Controls.Add(this.DelayDurationTextBox);
            this.TimingGroupBox.Controls.Add(this.PulseDurationTextBox);
            this.TimingGroupBox.Controls.Add(this.CodeGap);
            this.TimingGroupBox.Controls.Add(this.Frequency);
            this.TimingGroupBox.Controls.Add(this.PulseDelayDuration);
            this.TimingGroupBox.Controls.Add(this.DelayDuration);
            this.TimingGroupBox.Controls.Add(this.PulseDuration);
            this.TimingGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimingGroupBox.Location = new System.Drawing.Point(29, 26);
            this.TimingGroupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TimingGroupBox.Name = "TimingGroupBox";
            this.TimingGroupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TimingGroupBox.Size = new System.Drawing.Size(371, 299);
            this.TimingGroupBox.TabIndex = 4;
            this.TimingGroupBox.TabStop = false;
            this.TimingGroupBox.Text = "Timing";
            // 
            // MaskToggleBitsCheckBox
            // 
            this.MaskToggleBitsCheckBox.AutoSize = true;
            this.MaskToggleBitsCheckBox.Location = new System.Drawing.Point(28, 249);
            this.MaskToggleBitsCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaskToggleBitsCheckBox.Name = "MaskToggleBitsCheckBox";
            this.MaskToggleBitsCheckBox.Size = new System.Drawing.Size(138, 21);
            this.MaskToggleBitsCheckBox.TabIndex = 11;
            this.MaskToggleBitsCheckBox.Text = "Mask Toggle Bits";
            this.MaskToggleBitsCheckBox.UseVisualStyleBackColor = true;
            // 
            // CompFrameNoCheckBox
            // 
            this.CompFrameNoCheckBox.AutoSize = true;
            this.CompFrameNoCheckBox.Location = new System.Drawing.Point(28, 213);
            this.CompFrameNoCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CompFrameNoCheckBox.Name = "CompFrameNoCheckBox";
            this.CompFrameNoCheckBox.Size = new System.Drawing.Size(185, 21);
            this.CompFrameNoCheckBox.TabIndex = 10;
            this.CompFrameNoCheckBox.Text = "Compare Frame Number";
            this.CompFrameNoCheckBox.UseVisualStyleBackColor = true;
            // 
            // CodeGapTextBox
            // 
            this.CodeGapTextBox.Location = new System.Drawing.Point(299, 176);
            this.CodeGapTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CodeGapTextBox.Name = "CodeGapTextBox";
            this.CodeGapTextBox.Size = new System.Drawing.Size(39, 23);
            this.CodeGapTextBox.TabIndex = 7;
            this.CodeGapTextBox.Text = "10";
            this.CodeGapTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.CodeGapTextBox_Validating);
            // 
            // FrequencyTextBox
            // 
            this.FrequencyTextBox.Location = new System.Drawing.Point(299, 142);
            this.FrequencyTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FrequencyTextBox.Name = "FrequencyTextBox";
            this.FrequencyTextBox.Size = new System.Drawing.Size(39, 23);
            this.FrequencyTextBox.TabIndex = 9;
            this.FrequencyTextBox.Text = "10";
            this.FrequencyTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.FrequencyTextBox_Validating);
            // 
            // PulseDelayDurationTextBox
            // 
            this.PulseDelayDurationTextBox.Location = new System.Drawing.Point(299, 107);
            this.PulseDelayDurationTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PulseDelayDurationTextBox.Name = "PulseDelayDurationTextBox";
            this.PulseDelayDurationTextBox.Size = new System.Drawing.Size(39, 23);
            this.PulseDelayDurationTextBox.TabIndex = 8;
            this.PulseDelayDurationTextBox.Text = "10";
            this.PulseDelayDurationTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.PulseDelayDurationTextBox_Validating);
            // 
            // DelayDurationTextBox
            // 
            this.DelayDurationTextBox.Location = new System.Drawing.Point(299, 73);
            this.DelayDurationTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DelayDurationTextBox.Name = "DelayDurationTextBox";
            this.DelayDurationTextBox.Size = new System.Drawing.Size(39, 23);
            this.DelayDurationTextBox.TabIndex = 7;
            this.DelayDurationTextBox.Text = "10";
            this.DelayDurationTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.DelayDurationTextBox_Validating);
            // 
            // PulseDurationTextBox
            // 
            this.PulseDurationTextBox.Location = new System.Drawing.Point(299, 38);
            this.PulseDurationTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PulseDurationTextBox.Name = "PulseDurationTextBox";
            this.PulseDurationTextBox.Size = new System.Drawing.Size(39, 23);
            this.PulseDurationTextBox.TabIndex = 6;
            this.PulseDurationTextBox.Text = "10";
            this.PulseDurationTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.PulseDurationTextBox_Validating);
            // 
            // CodeGap
            // 
            this.CodeGap.AutoSize = true;
            this.CodeGap.Checked = true;
            this.CodeGap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CodeGap.Location = new System.Drawing.Point(28, 178);
            this.CodeGap.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CodeGap.Name = "CodeGap";
            this.CodeGap.Size = new System.Drawing.Size(223, 21);
            this.CodeGap.TabIndex = 5;
            this.CodeGap.Text = "Code Gap                     dev(%)";
            this.CodeGap.UseVisualStyleBackColor = true;
            this.CodeGap.CheckedChanged += new System.EventHandler(this.CodeGap_CheckedChanged);
            // 
            // Frequency
            // 
            this.Frequency.AutoSize = true;
            this.Frequency.Checked = true;
            this.Frequency.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Frequency.Location = new System.Drawing.Point(28, 144);
            this.Frequency.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Frequency.Name = "Frequency";
            this.Frequency.Size = new System.Drawing.Size(226, 21);
            this.Frequency.TabIndex = 4;
            this.Frequency.Text = "Frequencey                   dev(%)";
            this.Frequency.UseVisualStyleBackColor = true;
            this.Frequency.CheckedChanged += new System.EventHandler(this.Frequency_CheckedChanged);
            // 
            // PulseDelayDuration
            // 
            this.PulseDelayDuration.AutoSize = true;
            this.PulseDelayDuration.Checked = true;
            this.PulseDelayDuration.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PulseDelayDuration.Location = new System.Drawing.Point(28, 111);
            this.PulseDelayDuration.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PulseDelayDuration.Name = "PulseDelayDuration";
            this.PulseDelayDuration.Size = new System.Drawing.Size(224, 21);
            this.PulseDelayDuration.TabIndex = 3;
            this.PulseDelayDuration.Text = "Pulse/Delay Duration    dev(%)";
            this.PulseDelayDuration.UseVisualStyleBackColor = true;
            this.PulseDelayDuration.CheckedChanged += new System.EventHandler(this.PulseDelayDuration_CheckedChanged);
            // 
            // DelayDuration
            // 
            this.DelayDuration.AutoSize = true;
            this.DelayDuration.Checked = true;
            this.DelayDuration.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DelayDuration.Location = new System.Drawing.Point(28, 76);
            this.DelayDuration.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DelayDuration.Name = "DelayDuration";
            this.DelayDuration.Size = new System.Drawing.Size(225, 21);
            this.DelayDuration.TabIndex = 2;
            this.DelayDuration.Text = "Delay Duration              dev(%)";
            this.DelayDuration.UseVisualStyleBackColor = true;
            this.DelayDuration.CheckedChanged += new System.EventHandler(this.DelayDuration_CheckedChanged);
            // 
            // PulseDuration
            // 
            this.PulseDuration.AutoSize = true;
            this.PulseDuration.Checked = true;
            this.PulseDuration.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PulseDuration.Location = new System.Drawing.Point(28, 42);
            this.PulseDuration.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PulseDuration.Name = "PulseDuration";
            this.PulseDuration.Size = new System.Drawing.Size(224, 21);
            this.PulseDuration.TabIndex = 1;
            this.PulseDuration.Text = "Pulse Duration              dev(%)";
            this.PulseDuration.UseVisualStyleBackColor = true;
            this.PulseDuration.CheckedChanged += new System.EventHandler(this.PulseDuration_CheckedChanged);
            // 
            // OkButton
            // 
            this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.OkButton.Location = new System.Drawing.Point(73, 378);
            this.OkButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(100, 28);
            this.OkButton.TabIndex = 5;
            this.OkButton.Text = "Ok";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // Cancelutton
            // 
            this.Cancelutton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Cancelutton.Location = new System.Drawing.Point(232, 378);
            this.Cancelutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Cancelutton.Name = "Cancelutton";
            this.Cancelutton.Size = new System.Drawing.Size(100, 28);
            this.Cancelutton.TabIndex = 6;
            this.Cancelutton.Text = "Cancel";
            this.Cancelutton.UseVisualStyleBackColor = true;
            this.Cancelutton.Click += new System.EventHandler(this.Cancelutton_Click);
            // 
            // keepCfiCheckbox
            // 
            this.keepCfiCheckbox.AutoSize = true;
            this.keepCfiCheckbox.Location = new System.Drawing.Point(57, 335);
            this.keepCfiCheckbox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.keepCfiCheckbox.Name = "keepCfiCheckbox";
            this.keepCfiCheckbox.Size = new System.Drawing.Size(120, 21);
            this.keepCfiCheckbox.TabIndex = 12;
            this.keepCfiCheckbox.Text = "Keep CFI Files";
            this.keepCfiCheckbox.UseVisualStyleBackColor = true;
            // 
            // SetOptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(437, 421);
            this.Controls.Add(this.keepCfiCheckbox);
            this.Controls.Add(this.Cancelutton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.TimingGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "SetOptionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Set Up Options";
            this.Load += new System.EventHandler(this.SetOptionsForm_Load);
            this.TimingGroupBox.ResumeLayout(false);
            this.TimingGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox TimingGroupBox;
        private System.Windows.Forms.TextBox CodeGapTextBox;
        private System.Windows.Forms.TextBox FrequencyTextBox;
        private System.Windows.Forms.TextBox PulseDelayDurationTextBox;
        private System.Windows.Forms.TextBox DelayDurationTextBox;
        private System.Windows.Forms.TextBox PulseDurationTextBox;
        private System.Windows.Forms.CheckBox CodeGap;
        private System.Windows.Forms.CheckBox Frequency;
        private System.Windows.Forms.CheckBox PulseDelayDuration;
        private System.Windows.Forms.CheckBox DelayDuration;
        private System.Windows.Forms.CheckBox PulseDuration;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button Cancelutton;
        private System.Windows.Forms.CheckBox CompFrameNoCheckBox;
        private System.Windows.Forms.CheckBox MaskToggleBitsCheckBox;
        private System.Windows.Forms.CheckBox keepCfiCheckbox;
    }
}