namespace CommonUI.Controls {
    partial class IdFilterConditionsControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ByRangeRadioButton = new System.Windows.Forms.RadioButton();
            this.ByFileRadioButton = new System.Windows.Forms.RadioButton();
            this.ByModeRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.IdToLabel = new System.Windows.Forms.Label();
            this.IdFromLabel = new System.Windows.Forms.Label();
            this.IdToTextBox = new System.Windows.Forms.TextBox();
            this.IdFromTextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.IdFileInputControl = new CommonUI.Controls.FileInput();
            this.ModeSelectControl = new CommonUI.ModeSelectCtrl();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ByRangeRadioButton
            // 
            this.ByRangeRadioButton.AutoSize = true;
            this.ByRangeRadioButton.Location = new System.Drawing.Point(12, 0);
            this.ByRangeRadioButton.Name = "ByRangeRadioButton";
            this.ByRangeRadioButton.Size = new System.Drawing.Size(72, 17);
            this.ByRangeRadioButton.TabIndex = 1;
            this.ByRangeRadioButton.Text = "By Range";
            this.ByRangeRadioButton.CheckedChanged += new System.EventHandler(this.ByRangeRadioButton_CheckedChanged);
            // 
            // ByFileRadioButton
            // 
            this.ByFileRadioButton.AutoSize = true;
            this.ByFileRadioButton.Location = new System.Drawing.Point(12, 174);
            this.ByFileRadioButton.Name = "ByFileRadioButton";
            this.ByFileRadioButton.Size = new System.Drawing.Size(56, 17);
            this.ByFileRadioButton.TabIndex = 5;
            this.ByFileRadioButton.Text = "By File";
            this.ByFileRadioButton.UseVisualStyleBackColor = true;
            this.ByFileRadioButton.CheckedChanged += new System.EventHandler(this.ByFileRadioButton_CheckedChanged);
            // 
            // ByModeRadioButton
            // 
            this.ByModeRadioButton.AutoSize = true;
            this.ByModeRadioButton.Location = new System.Drawing.Point(12, 50);
            this.ByModeRadioButton.Name = "ByModeRadioButton";
            this.ByModeRadioButton.Size = new System.Drawing.Size(67, 17);
            this.ByModeRadioButton.TabIndex = 3;
            this.ByModeRadioButton.Text = "By Mode";
            this.ByModeRadioButton.UseVisualStyleBackColor = true;
            this.ByModeRadioButton.CheckedChanged += new System.EventHandler(this.ByModeRadioButton_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.IdFileInputControl);
            this.groupBox3.Location = new System.Drawing.Point(3, 175);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(387, 54);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.IdToLabel);
            this.groupBox1.Controls.Add(this.IdFromLabel);
            this.groupBox1.Controls.Add(this.IdToTextBox);
            this.groupBox1.Controls.Add(this.IdFromTextBox);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(387, 43);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // IdToLabel
            // 
            this.IdToLabel.AutoSize = true;
            this.IdToLabel.Location = new System.Drawing.Point(232, 20);
            this.IdToLabel.Name = "IdToLabel";
            this.IdToLabel.Size = new System.Drawing.Size(20, 13);
            this.IdToLabel.TabIndex = 3;
            this.IdToLabel.Text = "To";
            // 
            // IdFromLabel
            // 
            this.IdFromLabel.AutoSize = true;
            this.IdFromLabel.Location = new System.Drawing.Point(95, 20);
            this.IdFromLabel.Name = "IdFromLabel";
            this.IdFromLabel.Size = new System.Drawing.Size(30, 13);
            this.IdFromLabel.TabIndex = 2;
            this.IdFromLabel.Text = "From";
            // 
            // IdToTextBox
            // 
            this.IdToTextBox.Location = new System.Drawing.Point(258, 17);
            this.IdToTextBox.Name = "IdToTextBox";
            this.IdToTextBox.Size = new System.Drawing.Size(66, 20);
            this.IdToTextBox.TabIndex = 1;
            this.IdToTextBox.Leave += new System.EventHandler(this.IdToTextBox_Leave);
            // 
            // IdFromTextBox
            // 
            this.IdFromTextBox.Location = new System.Drawing.Point(131, 17);
            this.IdFromTextBox.Name = "IdFromTextBox";
            this.IdFromTextBox.Size = new System.Drawing.Size(66, 20);
            this.IdFromTextBox.TabIndex = 0;
            this.IdFromTextBox.Leave += new System.EventHandler(this.IdFromTextBox_Leave);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.ModeSelectControl);
            this.groupBox2.Location = new System.Drawing.Point(3, 52);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(387, 117);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // IdFileInputControl
            // 
            this.IdFileInputControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IdFileInputControl.FilePath = "";
            this.IdFileInputControl.Filter = "Text Files (*.TXT)|*.TXT|(*.LST)|*.LST|All Files (*.*)|*.*";
            this.IdFileInputControl.Location = new System.Drawing.Point(9, 24);
            this.IdFileInputControl.Name = "IdFileInputControl";
            this.IdFileInputControl.Size = new System.Drawing.Size(372, 26);
            this.IdFileInputControl.TabIndex = 0;
            this.IdFileInputControl.Title = "Open";
            this.IdFileInputControl.UseSaveDialog = false;
            this.IdFileInputControl.FilePathChanged += new System.EventHandler(this.IdFileInputControl_FilePathChanged);
            // 
            // ModeSelectControl
            // 
            this.ModeSelectControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ModeSelectControl.Location = new System.Drawing.Point(9, 22);
            this.ModeSelectControl.Margin = new System.Windows.Forms.Padding(0);
            this.ModeSelectControl.ModeList = "";
            this.ModeSelectControl.Name = "ModeSelectControl";
            this.ModeSelectControl.Size = new System.Drawing.Size(372, 83);
            this.ModeSelectControl.TabIndex = 0;
            this.ModeSelectControl.ModeSelectionChanged += new System.EventHandler(this.ModeSelectControl_ModeSelectionChanged);
            // 
            // IdFilterConditionsControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.ByRangeRadioButton);
            this.Controls.Add(this.ByFileRadioButton);
            this.Controls.Add(this.ByModeRadioButton);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "IdFilterConditionsControl";
            this.Size = new System.Drawing.Size(391, 232);
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton ByRangeRadioButton;
        private System.Windows.Forms.RadioButton ByFileRadioButton;
        private System.Windows.Forms.RadioButton ByModeRadioButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private CommonUI.Controls.FileInput IdFileInputControl;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label IdToLabel;
        private System.Windows.Forms.Label IdFromLabel;
        private System.Windows.Forms.TextBox IdToTextBox;
        private System.Windows.Forms.TextBox IdFromTextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private CommonUI.ModeSelectCtrl ModeSelectControl;


    }
}
