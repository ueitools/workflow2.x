//********************************************************************************
//REVISION HISTORY
//--------------------------------------------------------------------------------
//03/24/2010
//---------------------------------------------------------------------------------
//- Added OnSettings event handler to support the settings button.
//- Added ResetStatusBar method that could display the DB type selected.
//- Added ClearCaptureStations method that could clear the Capture pod combo box
//  when switched between different db's
//*********************************************************************************
using System;
using System.Collections.Generic;
using System.Windows.Forms;
namespace IRMaestro
{
    public partial class IdVerificationForm : Form, IIdVerificationView
    {
        private const string FORM_TITLE = "Verify IR";
        public IdVerificationForm()
        {
            InitializeComponent();
            InitializeForm();
        }
        private void InitializeForm()
        {
            Text = FORM_TITLE;
        }
        public event EventHandler OnIdSelected;
        public event EventHandler OnStartCapture;
        public event EventHandler OnCapturePodChangeRequested;
        public event CapturePodChangedDelegate OnCapturePodChanged;
        public event EventHandler OnResetId;
        public event EventHandler OnUsbCapture1MHzChanged;
        public event EventHandler OnSave;
        //Added to support the pick DB settings and Pod settings
        public event EventHandler OnSettings;
        public event FormClosingEventHandler OnFormClose;
        public string SelectedId
        {
            get
            {
                string tn = string.Empty;
                if (IdListComboBox.SelectedItem != null)
                {
                    return IdListComboBox.SelectedItem.ToString();
                }
                return tn;
            }
            set { IdListComboBox.SelectedItem = value; }
        }
        public bool Usb1MHzCaptureMode
        {
            get { return Use1MHzCheckBox.Checked; }
            set { Use1MHzCheckBox.Checked = value; }
        }
        public void SetIdList(MvpDataContainers.ListData idList)
        {
            IdListComboBox.Items.Clear();
            foreach (string id in idList)
            {
                IdListComboBox.Items.Add(id);
            }
        }
        public void SetAvailableFunctionList(MvpDataContainers.ListData functionLabels)
        {
            AvailableFunctionsListBox.Items.Clear();
            foreach (string functionLabel in functionLabels)
            {
                AvailableFunctionsListBox.Items.Add(functionLabel);
            }
            AvailableFunctionsListBox.Refresh();
        }
        public void EnableCapture(bool enabled)
        {
            CaptureButton.Enabled = enabled;
        }
        public void EnableUsbCapture1MHz(bool enable)
        {
            Use1MHzCheckBox.Enabled = enable;
        }
        public void EnableCapturePodSelection(bool enable)
        {
            CapturePodComboBox.Enabled = enable;
        }
        public void SetMatchedFunctionList(MvpDataContainers.ListData functionLabels)
        {
            MatchedFunctionsListView.Items.Clear();
            foreach (string functionLabel in functionLabels)
            {
                MatchedFunctionsListView.Items.Add(functionLabel);
            }
            MatchedFunctionsListView.Refresh();
        }
        public void SetMatchedFunctionSelectedIndex(int index)
        {
            MatchedFunctionsListView.SelectedIndices.Clear();
            MatchedFunctionsListView.SelectedIndices.Add(index);
        }
        public void Show1MHzCapture(bool show)
        {
            Use1MHzCheckBox.Enabled = show;
        }
        public void ClearCaptureStations()
        {
            CapturePodComboBox.Items.Clear();
        }
        public void SetAvailableCaptureStations(IList<string> availableCapturePodList)
        {
            foreach (string avaliableCapturePod in availableCapturePodList) {
                CapturePodComboBox.Items.Add(avaliableCapturePod);
            }
        }
        public void ResetStatusbar()
        {
            string db = DBSettings.GetDBType(DBSettings.idregpath, "SelectedDBType");
            DBType dbtype = DBType.UEIDB;
            
            if (db == "UEIDB")
                dbtype = DBType.UEIDB;
            else
                dbtype = DBType.PickDB;
            switch (dbtype)
            {
                case DBType.UEIDB:
                    {
                        toolStripDbStatus.Text = "UEI DB Selected";
                        toolStripPickDBPath.Text = "";
                        AvailableFunctionsListBox.Sorting = SortOrder.Ascending;
                        break;
                    }
                case DBType.PickDB:
                    {
                        toolStripDbStatus.Text = "Selected Pick Project ";
                        toolStripPickDBPath.Text = ": " + DBSettings.GetDBPath(DBSettings.idregpath, "SelectedPickProject");
                        AvailableFunctionsListBox.Sorting = SortOrder.None;
                        break;
                    }
            }
        }
        public void ClearComboSelection()
        {
            IdListComboBox.Text = "";
            IdListComboBox.Items.Clear();
            AvailableFunctionsListBox.Clear();
            MatchedFunctionsListView.Clear();
            DBSettings.selectedId = "";
            SelectedId = "";
            IdListComboBox.SelectedIndex = -1;
            
            Text = FORM_TITLE + " [" + SelectedId + "]";
            
        }
        public void SetCurrentCapturePod(string capturePod)
        {
            CapturePodComboBox.SelectedItem = capturePod;
            DBSettings.PodType = capturePod;
            
        }
        private void CapturePodComboBox_SelectedIndexChanged(object sender, EventArgs e) {
            string selectedItem = CapturePodComboBox.SelectedItem.ToString();
            if (OnCapturePodChanged != null) {
                OnCapturePodChanged(selectedItem);
            }
        }
        private void SetIdButton_Click(object sender, EventArgs e)
        {
            IndicateSelectionMade();
        }
        private void ResetIdButton_Click(object sender, EventArgs e)
        {
            if (OnResetId != null)
            {
                OnResetId(this, EventArgs.Empty);
            }
        }
        private void CaptureButton_Click(object sender, EventArgs e)
        {
            if (OnStartCapture != null)
            {
                OnStartCapture(this, EventArgs.Empty);
            }
        }
        private void Use1MHzCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OnUsbCapture1MHzChanged != null)
            {
                OnUsbCapture1MHzChanged(this, EventArgs.Empty);
            }
        }
        private void TnListComboBox_DropDownClosed(object sender, EventArgs e)
        {
           IndicateSelectionMade();
        }
        private void IndicateSelectionMade()
        {
            int index = IdListComboBox.SelectedIndex;
            if (index < 0)
            {
                index = IdListComboBox.FindStringExact(IdListComboBox.Text);
                if (index < 0)
                {
                    IdListComboBox.Text = string.Empty;
                    MessageBox.Show("Please select an ID");
                }
            }
            IdListComboBox.SelectedIndex = index;
            Text = FORM_TITLE + " [" + SelectedId + "]";
            if (OnIdSelected != null)
            {
                OnIdSelected(this, EventArgs.Empty);
            }
        }
        private void TnListComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            IdListComboBox.DroppedDown = true;
        }
        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (OnSave != null)
            {
                OnSave(this, EventArgs.Empty);
            }
        }
        private void CaptureSettingsButton_Click(object sender, EventArgs e)
        {
           if (OnSettings != null)
            {
                OnSettings(this, EventArgs.Empty);
            }
        }

        private void IdVerificationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            OnFormClose(this, null);
        }
        
    }
}