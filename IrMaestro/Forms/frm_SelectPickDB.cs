using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IRMaestro.Forms
{
    public partial class frm_SelectPickDB : Form
    {
        public frm_SelectPickDB()
        {
            InitializeComponent();
        }

        private void btnBrowsePickDB_Click(object sender, EventArgs e)
        {
            DBSettings.pickDBPath=DBSettings.BrowseFile("Pick DB", "*.DBD");
            txtSelectedPickDB.Text = DBSettings.pickDBPath;
            if (DBSettings.pickDBPath != string.Empty)
            {
                this.Close();
            }
            else
            {
                MessageBox.Show("No Pick DB Selected");
            }
        }

        private void frm_SelectPickDB_Load(object sender, EventArgs e)
        {
            txtSelectedPickDB.Text = DBSettings.pickDBPath;
        }

    }
}