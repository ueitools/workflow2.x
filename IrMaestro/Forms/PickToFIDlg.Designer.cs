namespace IRMaestro
{
    partial class PickToFIDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PickToFIDlg));
            this.chkboxAllLabels = new System.Windows.Forms.CheckBox();
            this.chkboxSeparateToggle = new System.Windows.Forms.CheckBox();
            this.chkboxExtFunc = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ProjectcomboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // chkboxAllLabels
            // 
            this.chkboxAllLabels.AutoSize = true;
            this.chkboxAllLabels.Location = new System.Drawing.Point(20, 148);
            this.chkboxAllLabels.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkboxAllLabels.Name = "chkboxAllLabels";
            this.chkboxAllLabels.Size = new System.Drawing.Size(140, 21);
            this.chkboxAllLabels.TabIndex = 2;
            this.chkboxAllLabels.Text = "Include All Labels";
            this.chkboxAllLabels.UseVisualStyleBackColor = true;
            // 
            // chkboxSeparateToggle
            // 
            this.chkboxSeparateToggle.AutoSize = true;
            this.chkboxSeparateToggle.Location = new System.Drawing.Point(20, 119);
            this.chkboxSeparateToggle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkboxSeparateToggle.Name = "chkboxSeparateToggle";
            this.chkboxSeparateToggle.Size = new System.Drawing.Size(210, 21);
            this.chkboxSeparateToggle.TabIndex = 1;
            this.chkboxSeparateToggle.Text = "Separate Toggle Bit Outputs";
            this.chkboxSeparateToggle.UseVisualStyleBackColor = true;
            // 
            // chkboxExtFunc
            // 
            this.chkboxExtFunc.AutoSize = true;
            this.chkboxExtFunc.Location = new System.Drawing.Point(20, 91);
            this.chkboxExtFunc.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkboxExtFunc.Name = "chkboxExtFunc";
            this.chkboxExtFunc.Size = new System.Drawing.Size(203, 21);
            this.chkboxExtFunc.TabIndex = 0;
            this.chkboxExtFunc.Text = "Include Extended Functions";
            this.chkboxExtFunc.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(20, 208);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(156, 208);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Pick DB Directory";
            // 
            // ProjectcomboBox
            // 
            this.ProjectcomboBox.FormattingEnabled = true;
            this.ProjectcomboBox.Location = new System.Drawing.Point(20, 42);
            this.ProjectcomboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ProjectcomboBox.Name = "ProjectcomboBox";
            this.ProjectcomboBox.Size = new System.Drawing.Size(303, 24);
            this.ProjectcomboBox.TabIndex = 6;
            // 
            // PickToFIDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(389, 266);
            this.Controls.Add(this.ProjectcomboBox);
            this.Controls.Add(this.chkboxAllLabels);
            this.Controls.Add(this.chkboxSeparateToggle);
            this.Controls.Add(this.chkboxExtFunc);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "PickToFIDlg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PickToFI";
            this.Load += new System.EventHandler(this.PickToFIDlg_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkboxAllLabels;
        private System.Windows.Forms.CheckBox chkboxSeparateToggle;
        private System.Windows.Forms.CheckBox chkboxExtFunc;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ProjectcomboBox;
    }
}