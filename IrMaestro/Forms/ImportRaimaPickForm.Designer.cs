namespace IRMaestro.Forms
{
    partial class ImportRaimaPickForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportRaimaPickForm));
            this.label1 = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.productComboBox = new System.Windows.Forms.ComboBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pathTextBox = new System.Windows.Forms.TextBox();
            this.internalCheckBox = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.idSelectionControl1 = new CommonForms.Forms.IDSelectionControl();
            this.dbGroupBox = new System.Windows.Forms.GroupBox();
            this.dbSelectorComboBox = new CommonForms.Forms.DatabaseSelectorComboBox();
            this.cfiButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.dbGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 150);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "To Project:";
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(41, 299);
            this.okButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(100, 28);
            this.okButton.TabIndex = 2;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(179, 299);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(100, 28);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // productComboBox
            // 
            this.productComboBox.FormattingEnabled = true;
            this.productComboBox.Location = new System.Drawing.Point(15, 175);
            this.productComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.productComboBox.Name = "productComboBox";
            this.productComboBox.Size = new System.Drawing.Size(471, 24);
            this.productComboBox.Sorted = true;
            this.productComboBox.TabIndex = 7;
            this.productComboBox.SelectedIndexChanged += new System.EventHandler(this.productComboBox_SelectedIndexChanged);
            // 
            // browseButton
            // 
            this.browseButton.Location = new System.Drawing.Point(387, 52);
            this.browseButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(100, 28);
            this.browseButton.TabIndex = 8;
            this.browseButton.Text = "Browse";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "From Pick DB Directory:";
            // 
            // pathTextBox
            // 
            this.pathTextBox.Location = new System.Drawing.Point(15, 53);
            this.pathTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pathTextBox.Name = "pathTextBox";
            this.pathTextBox.Size = new System.Drawing.Size(363, 22);
            this.pathTextBox.TabIndex = 10;
            // 
            // internalCheckBox
            // 
            this.internalCheckBox.AutoSize = true;
            this.internalCheckBox.Checked = true;
            this.internalCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.internalCheckBox.Location = new System.Drawing.Point(45, 256);
            this.internalCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.internalCheckBox.Name = "internalCheckBox";
            this.internalCheckBox.Size = new System.Drawing.Size(131, 21);
            this.internalCheckBox.TabIndex = 11;
            this.internalCheckBox.Text = "Mark as internal";
            this.internalCheckBox.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 87);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "To Database:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.idSelectionControl1);
            this.groupBox1.Location = new System.Drawing.Point(525, 6);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(547, 350);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ID Selection";
            // 
            // idSelectionControl1
            // 
            this.idSelectionControl1.Location = new System.Drawing.Point(7, 23);
            this.idSelectionControl1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.idSelectionControl1.Name = "idSelectionControl1";
            this.idSelectionControl1.Size = new System.Drawing.Size(520, 314);
            this.idSelectionControl1.TabIndex = 14;
            // 
            // dbGroupBox
            // 
            this.dbGroupBox.Controls.Add(this.dbSelectorComboBox);
            this.dbGroupBox.Controls.Add(this.label2);
            this.dbGroupBox.Controls.Add(this.label1);
            this.dbGroupBox.Controls.Add(this.productComboBox);
            this.dbGroupBox.Controls.Add(this.label3);
            this.dbGroupBox.Controls.Add(this.browseButton);
            this.dbGroupBox.Controls.Add(this.pathTextBox);
            this.dbGroupBox.Location = new System.Drawing.Point(16, 6);
            this.dbGroupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dbGroupBox.Name = "dbGroupBox";
            this.dbGroupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dbGroupBox.Size = new System.Drawing.Size(501, 233);
            this.dbGroupBox.TabIndex = 16;
            this.dbGroupBox.TabStop = false;
            this.dbGroupBox.Text = "Database";
            // 
            // dbSelectorComboBox
            // 
            this.dbSelectorComboBox.Location = new System.Drawing.Point(15, 112);
            this.dbSelectorComboBox.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.dbSelectorComboBox.Name = "dbSelectorComboBox";
            this.dbSelectorComboBox.Size = new System.Drawing.Size(472, 34);
            this.dbSelectorComboBox.TabIndex = 13;
            // 
            // cfiButton
            // 
            this.cfiButton.Location = new System.Drawing.Point(317, 299);
            this.cfiButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cfiButton.Name = "cfiButton";
            this.cfiButton.Size = new System.Drawing.Size(117, 28);
            this.cfiButton.TabIndex = 17;
            this.cfiButton.Text = "Make CFI Only";
            this.cfiButton.UseVisualStyleBackColor = true;
            this.cfiButton.Click += new System.EventHandler(this.cfiButton_Click);
            // 
            // ImportRaimaPickForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1091, 380);
            this.Controls.Add(this.cfiButton);
            this.Controls.Add(this.dbGroupBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.internalCheckBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ImportRaimaPickForm";
            this.Text = "Transfer Pick Database";
            this.Load += new System.EventHandler(this.ProjectNameForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.dbGroupBox.ResumeLayout(false);
            this.dbGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ComboBox productComboBox;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox pathTextBox;
        private System.Windows.Forms.CheckBox internalCheckBox;
        private System.Windows.Forms.Label label3;
        private CommonForms.Forms.IDSelectionControl idSelectionControl1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox dbGroupBox;
        private CommonForms.Forms.DatabaseSelectorComboBox dbSelectorComboBox;
        private System.Windows.Forms.Button cfiButton;
    }
}