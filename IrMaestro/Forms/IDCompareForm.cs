using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;
using BusinessObject;
using CommonCode;
using CommonForms;
using IDCompare;
using IRMaestro;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.GZip;


namespace IRMaestro
{
    public partial class IDCompareForm : Form
    {
        private IIdFunctionCompareHelper _idFunctionCompareHelper;
        private ITempFilePath _tempFilePath;
        private IDbFunctionsWrapper _dbWrapper;
        private string strListBoxSearch = "";
		PALObject palObject;
        public static class IDCmpSettings
        {
            public enum DBType
            {
                UEIDB,
                PickDB
            };
            public static string strWorkDir;
            public static string strCapSigFolder;
            public static DBType dbType;
            public static string strDBName;
            public static Int16 iRepoType;
            public static string strOutputPath;
            public static string strProjName;
            public static bool bKeepCfi;
            public static bool bIsWorkDirChanged;
        }
        private static List<string> IDList;
        public IDCompareForm()
        {
            InitializeComponent();
            Init();
            this.Dock = DockStyle.Fill;
            SetModifiedSettings(true);
        }
        
        private void Init()
        {
            if (CommonForms.Configuration.GetConfigItem("strCapFolder") == "" ||
                    CommonForms.Configuration.GetConfigItem("dbType") == "" ||
                    CommonForms.Configuration.GetConfigItem("strDBName") == "" ||
                    CommonForms.Configuration.GetWorkingDirectory() == "")
            {
                settingsToolStripMenuItem_Click(null, null);
            }
            else
            {
                string strCapFdr = CommonForms.Configuration.GetConfigItem("strCapFolder");
                string strDb = CommonForms.Configuration.GetConfigItem("strDBName");
                if (strDb == "UEI_PUBLIC")
                    DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
                else if (strDb == "UeiTemp_India")
                    DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP_INDIA);
                else if (strDb == "UeiTemp")
                    DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);

                if (CommonForms.Configuration.GetConfigItem("dbType") == "PickDB")
                    IDCmpSettings.dbType = IDCmpSettings.DBType.PickDB;
                else
                    IDCmpSettings.dbType = IDCmpSettings.DBType.UEIDB;
                IDCmpSettings.strCapSigFolder = strCapFdr;
                IDCmpSettings.strDBName = strDb;
                IDCmpSettings.strWorkDir = CommonForms.Configuration.GetWorkingDirectory();
                FillSettings(strCapFdr, strDb);
            }

            //Reading tolerance values from the registry
            int devPulse = 6;
            int devDelay = 6;
            int devDelayPulse = 6;
            int devFreq = 10;
            int devCodeGap = 6;
            bool bCompFrameNo = false;
            bool bMaskToggleBits = false;
            devFreq = CommonForms.Configuration.GetConfigItem("CFIInfoTolarance", "devFreq", 10);
            devPulse = CommonForms.Configuration.GetConfigItem("CFIInfoTolarance", "devPulse", 6);
            devDelay = CommonForms.Configuration.GetConfigItem("CFIInfoTolarance", "devDelay", 6);
            devCodeGap = CommonForms.Configuration.GetConfigItem("CFIInfoTolarance", "devCodeGap", 6);
            devDelayPulse = CommonForms.Configuration.GetConfigItem("CFIInfoTolarance", "devDelayPulse", 6);
            bCompFrameNo = CommonForms.Configuration.GetConfigItem("CFIInfoTolarance", "bCompFrameNo", false);
            bMaskToggleBits = CommonForms.Configuration.GetConfigItem("CFIInfoTolarance", "bMaskToggleBits", false);

            CFIInfo.SET_TOLERANCE(devFreq, devPulse, devDelay,
                devCodeGap, devDelayPulse, bCompFrameNo, bMaskToggleBits);
        }

        
        private void SetModifiedSettings(bool bIsWorkDirChanged)
        {

            if (bIsWorkDirChanged == true)
            {
                FunctionList.InitIntronTable();
                _tempFilePath = new TempFilePath();
                _tempFilePath.SetSubDirectory("\\NIDV\\");
                _idFunctionCompareHelper = new IdFunctionCompareHelper();
                _idFunctionCompareHelper.SetFilePathManager(_tempFilePath);
            }
            
            LoadPrevSelIDs();
        }

        private void IDcomboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (multipleListBox.Items.Count != 0)
                multipleListBox.SelectedIndex = 0;

        }
        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IDCmpSettingsForm sttForm = new IDCmpSettingsForm();
            DialogResult dlgres = sttForm.ShowDialog();
            if (dlgres == DialogResult.OK)
            {
                FillSettings(IDCmpSettings.strCapSigFolder, IDCmpSettings.strDBName);
                SetModifiedSettings(IDCmpSettings.bIsWorkDirChanged);
            }
        }

        private void FillSettings(string strCapFolder,string strDb)
        {
            multipleListBox.Items.Clear();
            IDcomboBox.Items.Clear();
            try
            {
                string[] strcapfiles = Directory.GetFiles(strCapFolder, "*.zip");
                foreach (string str in strcapfiles)
                {
                    string str1 = Path.GetFileNameWithoutExtension(str);
                    IDcomboBox.Items.Add(str1);
                }
                toolStripStatusLabel1.Text = strDb + " in Use";
                if (IDcomboBox.Items.Count > 1)
                {
                    IDcomboBox.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            { }
            SetModifiedSettings(IDCmpSettings.bIsWorkDirChanged);
        }

        private void multipleListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string strItem;
            if ((multipleListBox.SelectedItem != null) && (IDcomboBox.SelectedItem != null))
            {
                this.Cursor = Cursors.WaitCursor;
                try
                {
                    if (multipleListBox.Text == "--   " || multipleListBox.Text == "     ")
                    {
                        this.Cursor = Cursors.Default;
                        return;
                    }
                    strItem = multipleListBox.SelectedItem.ToString();
                    string strDestID = IDcomboBox.SelectedItem.ToString();
                    string strCapSigPath = IDCmpSettings.strCapSigFolder + "\\" + strDestID + ".zip";

                    if (IDCmpSettings.DBType.PickDB == IDCmpSettings.dbType)
                    {
                        DBSettings.selectedId = strItem;
                        PALObject.id_cficollection = null;
                        palObject.LoadPickObjects(DBSettings.selectedId);
                        CompareWithPickDB(PALObject.pickid, strCapSigPath);
                    }
                    else
                    {
                    _idFunctionCompareHelper.LoadId(strItem, (IRMaestro.DBType)IDCmpSettings.dbType);
                    CompareCaptureFile(strItem, strCapSigPath);
                }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Comparison Failed " + ex.Message + "\r\n" + ex.Source);
                }
                this.Cursor = Cursors.Default;
            }
        }
        private void CompareCaptureFile(string ID, string rdBackZip)
        {
            String strUnZipFolder = IDCmpSettings.strWorkDir + "\\UZSignal\\" + Path.GetFileNameWithoutExtension(rdBackZip);
            FastZip fz = new FastZip();
            fz.CreateEmptyDirectories = true;
            fz.ExtractZip(rdBackZip, strUnZipFolder, "");
            string[] strExtSignalFiles = Directory.GetFiles(strUnZipFolder, "*.*", SearchOption.AllDirectories);
            string strFileExt = "";
            string strSelDB = "";
            if (IDCmpSettings.DBType.PickDB == IDCmpSettings.dbType)
            {
                strFileExt = ".pk";
                strSelDB += "PickDB";
            }
            else
            {
                if (IDCmpSettings.strDBName == "UeiTemp")
                { strFileExt = ".QA"; }
                else if (IDCmpSettings.strDBName == "UeiTemp_India")
                { strFileExt = ".IQA"; }
                else if (IDCmpSettings.strDBName == "UEI_PUBLIC")
                { strFileExt = ".UEI"; }
                strSelDB = strFileExt.Replace(".", "");
                strSelDB += "DB";
            }
            Hashtable labelList = new Hashtable();
            foreach (string strPrjFile in strExtSignalFiles)
            {
                if (strPrjFile.Contains(".prj") || strPrjFile.Contains(".PRJ"))
                {
                    labelList = FunctionList.GetLabels(strPrjFile);
                }
            }
            string comparisonFilePath = "";
            comparisonFilePath = IDCmpSettings.strWorkDir + "\\" + Path.GetFileNameWithoutExtension(rdBackZip);
            comparisonFilePath += "_" + ID;
            comparisonFilePath += strFileExt;
            StreamWriter comparisonFile = File.CreateText(comparisonFilePath);
            comparisonFile.WriteLine("Project Name = " + IDCmpSettings.strProjName);
            comparisonFile.WriteLine("Tracking Number = " + Path.GetFileName(rdBackZip));
            comparisonFile.WriteLine("Executor Number = " + _idFunctionCompareHelper.Exec.ToString());
            comparisonFile.WriteLine("Comparing Capture File: " + rdBackZip + " to ID: " + ID);
            comparisonFile.WriteLine("DataBase type: " + strSelDB);
            string strUnMatched = "---------------------------"
                   + "----------------------------------"
                  + "------------------------------\r\n\r\n"
                   + "Unmatched CFI files:\r\n\r\n";
            int nFailed = 0;
            foreach (string strSigFile in strExtSignalFiles)
            {
                string strSigFile1 = strSigFile.ToUpper();
                if (Path.GetExtension(strSigFile1) == ".U1" || Path.GetExtension(strSigFile1) == ".U2")
                {
                    string[] strResults = _idFunctionCompareHelper.IDCSearchForMatchingFunction(strSigFile, ID, labelList, strSelDB);
                    if (strResults[2] == "0")
                    {
                        nFailed++;
                        strUnMatched += strResults[1];
                    }
                    else
                    {
                        comparisonFile.Write(strResults[0]); 
                    }
                    if (!IDCmpSettings.bKeepCfi)
                    {
                        try
                        {
                            string cfiFilename = Path.GetFileNameWithoutExtension(strSigFile) + "_" + ID + ".cfi";
                            string captureDirectory = strSigFile.Substring(0, strSigFile.LastIndexOf(Path.DirectorySeparatorChar) + 1);
                            string cfiPath = "";
                            string tmpPath = captureDirectory.Replace("\\UZSignal", "");
                            cfiPath = tmpPath + cfiFilename;
                            File.Delete(cfiPath);
                        }
                        catch (Exception exception)
                        { }
                    }
                }
            }
            if (nFailed >= 0)
            {
                if (nFailed == 0)
                strUnMatched = strUnMatched.Replace("Unmatched CFI files:\r\n\r\n", "");
                strUnMatched += "Total " + nFailed + " unmatched CFI files.";
                comparisonFile.WriteLine(strUnMatched);
            }
            comparisonFile.Close();
            try
            {
                System.Diagnostics.Process process = System.Diagnostics.Process.Start(comparisonFilePath);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Process.Start("notepad.exe", comparisonFilePath);
            }
            string strPath = strUnZipFolder.Replace("\\UZSignal", "");
            if (IDCmpSettings.bKeepCfi)
            {
                string strScrFolder = IDCmpSettings.strWorkDir + "\\NIDV";
                string strDestFolder = "";                
                strDestFolder = strPath + "\\" + ID + "\\";
                
                if (!Directory.Exists(strDestFolder))
                {
                    Directory.CreateDirectory(strDestFolder);
                }
                else
                {
                    Directory.Delete(strDestFolder, true);
                    Directory.CreateDirectory(strDestFolder);
                }
                string[] strAllfiles = Directory.GetFiles(strScrFolder, "*.tmp", SearchOption.TopDirectoryOnly);
                foreach (string cfiFile in strAllfiles)
                {
                    string nwCfiFile = Path.GetFileNameWithoutExtension(cfiFile);
                    nwCfiFile = nwCfiFile.Replace("tmp", "_");
                    nwCfiFile += ".CFI";
                    string destFile = strDestFolder + nwCfiFile;
                    File.Copy(cfiFile, destFile, true);
                }
            }
            else
            {
                if (Directory.Exists(strPath))
                {
                    Directory.Delete(strPath, true);
                }
            }

            if (IDCmpSettings.strOutputPath != "")
            {
                if (IDCmpSettings.strOutputPath == IDCmpSettings.strWorkDir)
                    return;

                try
                {
                    string nwPath = IDCmpSettings.strOutputPath + "\\" + Path.GetFileName(comparisonFilePath);
                    File.Copy(comparisonFilePath, nwPath, true);
                    if (IDCmpSettings.bKeepCfi)
                    {
                        string strCurFolder = IDCmpSettings.strWorkDir + "\\" + Path.GetFileNameWithoutExtension(rdBackZip);
                        string strNwDirpath = IDCmpSettings.strOutputPath + "\\" + Path.GetFileNameWithoutExtension(rdBackZip);
                        DirectoryInfo diSource = new DirectoryInfo(strCurFolder);
                        DirectoryInfo diTarget = new DirectoryInfo(strNwDirpath);
                        CopyCfiFolders(diSource, diTarget);
                        Directory.Delete(strCurFolder, true);
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }

        private void CopyCfiFolders(DirectoryInfo source, DirectoryInfo target)
        {
            if (Directory.Exists(target.FullName) == false)
            {
                Directory.CreateDirectory(target.FullName);
            }
            foreach (FileInfo fi in source.GetFiles())
            {
                fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            }
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyCfiFolders(diSourceSubDir, nextTargetSubDir);
            }
        }
        private void multipleListBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                multipleListBox_MouseDoubleClick(null, null);
            }
            if ((e.KeyChar == (char)Keys.Back) || (e.KeyChar == (char)Keys.Escape) ||
                (e.KeyChar == (char)Keys.Down) || (e.KeyChar == (char)Keys.Up) ||
                (e.KeyChar == (char)Keys.Left) || (e.KeyChar == (char)Keys.Right))
            {
                strListBoxSearch = "";
                return;
            }
            strListBoxSearch += e.KeyChar.ToString();
            multipleListBox.SelectedIndex = multipleListBox.FindString(strListBoxSearch);
            if (multipleListBox.SelectedIndex == -1)
            {
                strListBoxSearch = "";
            }
        }
        private void RefreshBtn_Click(object sender, EventArgs e)
        {
            IDcomboBox.Items.Clear();
            if (!Directory.Exists(IDCmpSettings.strCapSigFolder))
            {
                MessageBox.Show("Select a Valid Capture Signals Folder");
                return;
            }
            string[] strcapfiles = Directory.GetFiles(IDCmpSettings.strCapSigFolder, "*.zip");
            if (strcapfiles.Length == 0)
            {
                MessageBox.Show("Select a Valid Capture Signals Folder");
                return;
            }
            this.Cursor = Cursors.WaitCursor;
            foreach (string str in strcapfiles)
            {
                string str1 = Path.GetFileNameWithoutExtension(str);
                IDcomboBox.Items.Add(str1);
            }
            if (IDcomboBox.Items.Count > 0)
            {
                IDcomboBox.SelectedIndex = 0;
            }
            this.Cursor = Cursors.Default;
        }
        private void IDFilterBtn_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            StringCollection stringCollection = new StringCollection();

            if (IDCmpSettings.DBType.PickDB == IDCmpSettings.dbType)
            {
                if (IDCmpSettings.strDBName != string.Empty)
                {
                    palObject = new PALObject(DBSettings.GetDBPath(DBSettings.idregpath, "SelectedPickProject"));
                    IList<string> idlist = palObject.GetAllIDs();

                    StringCollection idcollection = new StringCollection();
                    foreach (string id in idlist)
                    {
                        idcollection.Add(id);
                    }
                    stringCollection = idcollection;
                }
            }
            else
            {
                _dbWrapper = new DbFunctionsWrapper();
            stringCollection = _dbWrapper.GetAllIDList();
            }

            IDSelectionForm idselform = new IDSelectionForm();
            DialogResult dlgres = idselform.ShowDialog();
            idselform.Close();
            if (dlgres == DialogResult.OK)
            {
                multipleListBox.Items.Clear();
                IDList = idselform.GetSelectedIDList(stringCollection);
                foreach (string ID in IDList)
                {
                    multipleListBox.Items.Add(ID);
                }
                multipleListBox.Focus();
            }
            this.Cursor = Cursors.Default;
        }
        private void multipleListBox_Leave(object sender, EventArgs e)
        {
            strListBoxSearch = "";
        }
        private void multipleListBox_Enter(object sender, EventArgs e)
        {
            strListBoxSearch = "";
        }

        private void LoadPrevSelIDs()
        {
            this.Cursor = Cursors.WaitCursor;
            StringCollection stringCollection = new StringCollection();

            if (IDCmpSettings.DBType.PickDB == IDCmpSettings.dbType)
            {
                if (IDCmpSettings.strDBName != string.Empty)
                {
                    palObject = new PALObject(IDCmpSettings.strDBName);
                    IList<string> idlist = palObject.GetAllIDs();

                    StringCollection idcollection = new StringCollection();
                    foreach (string id in idlist)
                    {
                        idcollection.Add(id);
                    }
                    stringCollection = idcollection;
                }
            }
            else
            {
                _dbWrapper = new DbFunctionsWrapper();
            stringCollection = _dbWrapper.GetAllIDList();
            }



            IDSelectionForm idselform = new IDSelectionForm();
            SelectType prevSel = new SelectType();
            string strResult = CommonForms.Configuration.GetConfigItem("IdFilterRes");
            string strSelType = CommonForms.Configuration.GetConfigItem("IdFilterSel");
            if (strSelType == "FILE" && !File.Exists(strResult))
            {
                strResult = "";
                strSelType = "";
            }
            if (strResult != "" && strSelType != "")
            {
                prevSel.Result = strResult;
                if (strSelType == "RANGE")
                    prevSel.SelectionType = SelType.RANGE;
                else if (strSelType == "MODE")
                    prevSel.SelectionType = SelType.MODE;
                else if (strSelType == "FILE")
                    prevSel.SelectionType = SelType.FILE;

                IDList = idselform.GetSelectedIDList(stringCollection, prevSel);
                multipleListBox.Items.Clear();
                foreach (string Ids in IDList)
                {
                    multipleListBox.Items.Add(Ids);
                }
            }
            else
            {
                multipleListBox.Items.Clear();
                foreach (string Id in stringCollection)
                {
                    multipleListBox.Items.Add(Id);
                }
            }
            multipleListBox.Focus();
            this.Cursor = Cursors.Default;
        }
        private void IDCompareApp_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Directory.Delete(IDCmpSettings.strWorkDir + "\\NIDV", true);
                Directory.Delete(IDCmpSettings.strWorkDir + "\\UZSignal", true);
            }
            catch (Exception ex) { }
        }


        private void CompareWithPickDB(PickID pickID, string rdBackZip)
        {
            string id = pickID.OID.Header.ID;
            string strExec = pickID.OID.Header.Executor_Code.ToString();
            string pickIDWorkingPath = IDCmpSettings.strWorkDir + "\\NIDV\\" + id + "\\";
            if (!Directory.Exists(pickIDWorkingPath))
                Directory.CreateDirectory(pickIDWorkingPath);

            String strUnZipFolder = IDCmpSettings.strWorkDir + "\\UZSignal\\" + Path.GetFileNameWithoutExtension(rdBackZip);
            FastZip fz = new FastZip();
            fz.CreateEmptyDirectories = true;
            fz.ExtractZip(rdBackZip, strUnZipFolder, "");
            string[] strExtSignalFiles = Directory.GetFiles(strUnZipFolder, "*.*", SearchOption.AllDirectories);
            string strFileExt = "";
            string strSelDB = "";
            if (IDCmpSettings.DBType.PickDB == IDCmpSettings.dbType)
            {
                strFileExt = ".pk";
                strSelDB += "PickDB";
            }
            Hashtable labelList = new Hashtable();
            foreach (string strPrjFile in strExtSignalFiles)
            {
                if (strPrjFile.Contains(".prj") || strPrjFile.Contains(".PRJ"))
                {
                    labelList = FunctionList.GetLabels(strPrjFile);
                }
            }
            string comparisonFilePath = "";
            comparisonFilePath = IDCmpSettings.strWorkDir + "\\" + Path.GetFileNameWithoutExtension(rdBackZip);
            comparisonFilePath += "_" + id;
            comparisonFilePath += strFileExt;
            StreamWriter comparisonFile = File.CreateText(comparisonFilePath);
            comparisonFile.WriteLine("Project Name = " + IDCmpSettings.strProjName);
            comparisonFile.WriteLine("Tracking Number = " + Path.GetFileName(rdBackZip));
            comparisonFile.WriteLine("Executor Number = " + strExec);
            comparisonFile.WriteLine("Comparing Capture File: " + rdBackZip + " to ID: " + id);
            comparisonFile.WriteLine("DataBase type: " + IDCmpSettings.strDBName);
            string strUnMatched = "---------------------------"
                   + "----------------------------------"
                  + "------------------------------\r\n\r\n"
                   + "Unmatched CFI files:\r\n\r\n";
            int nFailed = 0;
            foreach (string strSigFile in strExtSignalFiles)
            {
                string[] strRowResultArr = new string[3];
                string strSigFile1 = strSigFile.ToUpper();
                if (Path.GetExtension(strSigFile1) == ".U1" || Path.GetExtension(strSigFile1) == ".U2")
                {
                    int dPassed = 0, dFailed = 0;
                    string strCfiFilename = Path.GetFileNameWithoutExtension(strSigFile) + "_" + id + ".cfi";
                    string captureDirectory = strSigFile.Substring(0, strSigFile.LastIndexOf(Path.DirectorySeparatorChar) + 1);
                    string strCfiPath = "";
                    string strProjName = "Project Name: " + IDCompareForm.IDCmpSettings.strProjName + "\r\n";
                    string strCfiDir = captureDirectory.Replace("\\UZSignal", "");
                    if (!Directory.Exists(strCfiDir))
                    {
                        Directory.CreateDirectory(strCfiDir);
                    }
                    List<string> errorMessages = null;
                    strCfiPath = strCfiDir + strCfiFilename;
                    CommonForms.FunctionList.ConvertCaptureToCfiFile(strCfiPath, strSigFile, pickID.OID.Header.Executor_Code);

                    ICompareID cfiCmp = new ICompareID();
                    string strFailedData = "";
                    string strPassedData = "";
                    string strExecnum = "";
                    string rbLabel, pickLabel;
                    bool bCfiMatched = true;

                    Hashtable pickIntronTable, pickLabelTable, pickIntronPriorityTable, dataTable;
                    List<string> cfiFilesFromPickID = FunctionList.CreateTxCfiFiles(
                 pickID, pickIDWorkingPath, CFIInfo.MASK_TOGGLE_BITS == false,
                 out pickIntronTable, out pickLabelTable, out pickIntronPriorityTable, out dataTable);


                    strPassedData += "--------------------------"
                     + "----------------------------------"
                      + "------------------------------\r\n\r\n";

                    foreach (string pickIDCfiFile in cfiFilesFromPickID)
                    {
                        bool bFound = false;
                        List<string> warningMessages = null;
                        string strRXData = "", strTXData = "";
                        bool bIsNonFunctional = false;
                        bool bIsHiFrequency = false;
                        string strOutputFilename = pickIDWorkingPath + pickIDCfiFile;
                        int nCompareResults;
                        CommonForms.CFIInfo RXCFIInfo = null, TXCFIInfo = null;
                        cfiCmp.DoCompare(strCfiPath, strOutputFilename, out nCompareResults,
                                             out rbLabel, out pickLabel,
                                             out strRXData, out strTXData, out RXCFIInfo, out TXCFIInfo,
                                             CommonForms.CFIInfo.MASK_TOGGLE_BITS, -1);
                        if (strRXData == null)
                        {
                            cfiCmp.DoCompare(strCfiPath, out nCompareResults,
                                out strRXData, strTXData, out RXCFIInfo);
                        }
                        else if (strTXData == null)
                        {
                            cfiCmp.DoCompare(strCfiPath, out nCompareResults,
                              strRXData, out strTXData, out TXCFIInfo);
                        }
                        else
                        {
                            cfiCmp.DoCompare(out nCompareResults, strRXData, strTXData);
                        }
                        bIsHiFrequency = TXCFIInfo.Frequency > 0F && TXCFIInfo.Frequency < 10F;
                        string rxData = strRXData.Replace("FD", "");
                        rxData = rxData.Replace("B", "").Trim();
                        string txData = strTXData.Replace("FD", "");
                        txData = txData.Replace("B", "").Trim();
                        string fileName = Path.GetFileName(strSigFile);
                        strExecnum = "Executor Number = " + strExec + "\r\n";
                        string strDbCfiname = Path.GetFileNameWithoutExtension(strOutputFilename);
                        strDbCfiname = strDbCfiname.Replace("tmp", "_");
                        if (nCompareResults > 0)
                        {
                            dPassed = dPassed + 1;
                            bFound = true;
                        }
                        if (!bFound)
                        {
                            dFailed = dFailed + 1;
                            string strU1File = Path.GetFileNameWithoutExtension(strSigFile);
                            strU1File += ".cfi";
                            strFailedData = String.Format("{0, -21}{1}       {2}\r\n\r\n",
                                strU1File, rxData, (string)labelList[fileName]);

                        }
                        else
                        {
                            strPassedData += String.Format("{0,-15}",
                                fileName);
                            strPassedData += String.Format("{0,-2}", ":");
                            strPassedData += rxData;
                            strPassedData += "              ";
                            strPassedData += String.Format("{0,-30}",
                                (string)labelList[fileName]);

                            if (labelList[fileName] != null)
                            {
                                string label = (string)labelList[fileName];
                                if (!label.Contains("|"))
                                    strPassedData += CommonForms.FunctionList.GetIntronData(
                                       id.Substring(0, 1), label);
                                else
                                {
                                    string[] strs = label.Split(new string[] { "|" },
                                        StringSplitOptions.RemoveEmptyEntries);
                                    if (strs != null && strs.Length > 0)
                                    {
                                        for (int k = 0; k < strs.Length; k++)
                                        {
                                            string s = CommonForms.FunctionList.GetIntronData(
                                                id.Substring(0, 1), strs[k].Trim());
                                            if (s != null)
                                            {
                                                strPassedData += s;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            strPassedData += "\r\n";
                            strPassedData += String.Format("{0,-15}", strSelDB);
                            strPassedData += String.Format("{0,-2}", ":");
                            strPassedData += txData;
                            strPassedData += "              ";
                            string pickDbdata = dataTable[pickIDCfiFile].ToString();


                            StringCollection introns = (StringCollection)pickIntronTable[pickDbdata];
                            StringCollection intronPrioritys = (StringCollection)pickIntronPriorityTable[pickDbdata];
                            StringCollection labels = (StringCollection)pickLabelTable[pickDbdata];

                            strPassedData += String.Format("{0,-30}", labels[0]);
                            strPassedData += introns[0];
                            if (intronPrioritys[0] != null)
                                if (intronPrioritys[0] != "") //add intron priority if available.
                                    strPassedData += "(p" + intronPrioritys[0] + ")";
                            strPassedData += "\r\n\r\n";
                            if (!cfiCmp.CompareCFIInfo(RXCFIInfo, TXCFIInfo, out errorMessages))
                                bCfiMatched = false;
                        }
                        if (warningMessages != null && warningMessages.Count > 0)
                        {
                            errorMessages.InsertRange(0, warningMessages);
                        }
                        if (errorMessages != null && errorMessages.Count > 0)
                        {
                            if (bIsNonFunctional)
                            {
                                errorMessages.Insert(0, "Warning: Compare found errors but function is 'Non-functional'");
                            }
                            if (bIsHiFrequency)
                            {
                                errorMessages.Insert(0, "Warning: Hi-Frequency signal");
                            }
                        }
                    }

                    if (errorMessages != null && errorMessages.Count > 0 && !bCfiMatched)
                    {
                        foreach (string message in errorMessages)
                        {
                            strPassedData += message;
                            strPassedData += "\r\n";
                        }
                        strPassedData += "\r\n";
                    }
                    strRowResultArr[0] = strPassedData;
                    strRowResultArr[1] = strFailedData;
                    strRowResultArr[2] = dPassed.ToString();

                }



                if (strRowResultArr[2] == "0")
                {
                    nFailed++;
                    strUnMatched += strRowResultArr[1];
                }
                else
                {
                    comparisonFile.Write(strRowResultArr[0]);
                }
                if (!IDCmpSettings.bKeepCfi)
                {
                    try
                    {
                        string cfiFilename = Path.GetFileNameWithoutExtension(strSigFile) + "_" + id + ".cfi";
                        string captureDirectory = strSigFile.Substring(0, strSigFile.LastIndexOf(Path.DirectorySeparatorChar) + 1);
                        string cfiPath = "";
                        string tmpPath = captureDirectory.Replace("\\UZSignal", "");
                        cfiPath = tmpPath + cfiFilename;
                        File.Delete(cfiPath);
                    }
                    catch (Exception exception)
                    { }
                }
            }
            if (nFailed >= 0)
            {
                if (nFailed == 0)
                    strUnMatched = strUnMatched.Replace("Unmatched CFI files:\r\n\r\n", "");
                strUnMatched += "Total " + nFailed + " unmatched CFI files.";
                comparisonFile.WriteLine(strUnMatched);
            }
            comparisonFile.Close();
            try
            {
                System.Diagnostics.Process process = System.Diagnostics.Process.Start(comparisonFilePath);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Process.Start("notepad.exe", comparisonFilePath);
            }
            string strPath = strUnZipFolder.Replace("\\UZSignal", "");
            if (IDCmpSettings.bKeepCfi)
            {
                string strScrFolder = IDCmpSettings.strWorkDir + "\\NIDV";
                string strDestFolder = "";
                strDestFolder = strPath + "\\" + id + "\\";

                if (!Directory.Exists(strDestFolder))
                {
                    Directory.CreateDirectory(strDestFolder);
                }
                else
                {
                    Directory.Delete(strDestFolder, true);
                    Directory.CreateDirectory(strDestFolder);
                }
                string[] strAllfiles = Directory.GetFiles(strScrFolder, "*.tmp", SearchOption.TopDirectoryOnly);
                foreach (string cfiFile in strAllfiles)
                {
                    string nwCfiFile = Path.GetFileNameWithoutExtension(cfiFile);
                    nwCfiFile = nwCfiFile.Replace("tmp", "_");
                    nwCfiFile += ".CFI";
                    string destFile = strDestFolder + nwCfiFile;
                    File.Copy(cfiFile, destFile, true);
                }
            }
            else
            {
                if (Directory.Exists(strPath))
                {
                    Directory.Delete(strPath, true);
                }
            }

            if (IDCmpSettings.strOutputPath != "")
            {
                if (IDCmpSettings.strOutputPath == IDCmpSettings.strWorkDir)
                    return;

                try
                {
                    string nwPath = IDCmpSettings.strOutputPath + "\\" + Path.GetFileName(comparisonFilePath);
                    File.Copy(comparisonFilePath, nwPath, true);
                    if (IDCmpSettings.bKeepCfi)
                    {
                        string strCurFolder = IDCmpSettings.strWorkDir + "\\" + Path.GetFileNameWithoutExtension(rdBackZip);
                        string strNwDirpath = IDCmpSettings.strOutputPath + "\\" + Path.GetFileNameWithoutExtension(rdBackZip);
                        DirectoryInfo diSource = new DirectoryInfo(strCurFolder);
                        DirectoryInfo diTarget = new DirectoryInfo(strNwDirpath);
                        CopyCfiFolders(diSource, diTarget);
                        Directory.Delete(strCurFolder, true);
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }


    }
}