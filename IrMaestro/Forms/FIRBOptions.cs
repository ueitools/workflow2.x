using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using FiCompareLib;
using CommonForms;

namespace IRMaestro
{
    public partial class FIRBOptions : Form
    {
        private int devPulse = 6;
        private int devDelay = 6;
        private int devDelayPulse = 6;
        private int devFreq = 10;
        private int devCodeGap = 6;
        private int devDutyCycle = 60;

        public FIRBOptions()
        {
            InitializeComponent();
        }

        private void FIRBOptions_Load(object sender, EventArgs e)
        {

            devFreq = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,UserSettings.ConfigKeys.FREQ_DEV,10);
            devPulse = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.PULSE_DEV, 6);
            devDelay = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.DELAY_DEV, 6);
            devCodeGap = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CODEGAP_DEV, 6);
            devDelayPulse = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.DELAYPULSE_DEV, 6);
            devDutyCycle = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.DUTYCYCLE_DEV, 60);
            keepFIcheckBox.Checked = Convert.ToBoolean(UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.KEEPFI,0));

            PulseDurationCBox.Checked = Convert.ToBoolean(UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CMPPULSECHKBOX, 1));
            DelayDurationCBox.Checked = Convert.ToBoolean(UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CMPDELAYCHKBOX,1));
            FrequencyCBox.Checked = Convert.ToBoolean(UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CMPFREQCHKBOX,1));
            CodeGapCBox.Checked = Convert.ToBoolean(UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CMPCODEGAP,1));
            DutyCycleCBox.Checked = Convert.ToBoolean(UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CMPDUTYCHKBOX,0));
            CmpFramesChkBox.Checked = Convert.ToBoolean(UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.TICMPFRAMES, 0));

            frequencytb.Text = "" + devFreq;
            PulseDurationtb.Text = "" + devPulse;
            delaydurationtb.Text = "" + devDelay;
            dutycycletb.Text = "" + devDutyCycle;
            codegaptb.Text = "" + devCodeGap;
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            SetUserOptions();
            this.Close();
        }

        private void PulseDurationtb_Validating(object sender, CancelEventArgs e)
        {
            if (!PulseDurationCBox.Checked)
                return;

            string s = PulseDurationtb.Text;
            s = s.Trim();

            int pulse = 0;

            try
            {
                pulse = int.Parse(s);
                if (pulse > 100 || pulse < 1)
                    throw new Exception();
                devPulse = pulse;
            }
            catch
            {
                MessageBox.Show(
                    "Pulse duration dev should be a number bewteen 1 and 100");
                PulseDurationtb.Text = "" + devPulse;
            }

        }

        private void delaydurationtb_Validating(object sender, CancelEventArgs e)
        {
            if (!DelayDurationCBox.Checked)
                return;

            string s = delaydurationtb.Text;
            s = s.Trim();

            int delay = 0;

            try
            {
                delay = int.Parse(s);
                if (delay > 100 || delay < 1)
                    throw new Exception();
                devDelay = delay;
            }
            catch
            {
                MessageBox.Show(
                    "Delay duration dev should be a number bewteen 1 and 100");
                delaydurationtb.Text = "" + devDelay;
            }
        }

        private void frequencytb_Validating(object sender, CancelEventArgs e)
        {
            if (!FrequencyCBox.Checked)
                return;

            string s = frequencytb.Text;
            s = s.Trim();

            int freq = 0;

            try
            {
                freq = int.Parse(s);
                if (freq > 100 || freq < 1)
                    throw new Exception();
                devFreq = freq;
            }
            catch
            {
                MessageBox.Show(
                    "Frequency dev should be a number bewteen 1 and 100");
                frequencytb.Text = "" + devFreq;
            }
        }

        private void codegaptb_Validating(object sender, CancelEventArgs e)
        {
            if (!CodeGapCBox.Checked)
                return;

            string s = codegaptb.Text;
            s = s.Trim();

            int codeGap = 0;

            try
            {
                codeGap = int.Parse(s);
                if (codeGap > 100 || codeGap < 1)
                    throw new Exception();
                devCodeGap = codeGap;
            }
            catch
            {
                MessageBox.Show(
                    "Code gap dev should be a number bewteen 1 and 100");
                codegaptb.Text = "" + devCodeGap;
            }
        }

        private void dutycycletb_Validating(object sender, CancelEventArgs e)
        {

            if (!DutyCycleCBox.Checked)
                return;

            string s = dutycycletb.Text;
            s = s.Trim();

            int dutycycle = 0;

            try
            {
                dutycycle = int.Parse(s);
                if (dutycycle > 100 || dutycycle < 1)
                    throw new Exception();
                devDutyCycle = dutycycle;
            }
            catch
            {
                MessageBox.Show(
                    "Duty Cycle dev should be a number bewteen 1 and 100");
                dutycycletb.Text = "" + devDutyCycle;
            }
        }

        public void SetUserOptions()
        {
            UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.FREQ_DEV, devFreq.ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.PULSE_DEV, devPulse.ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.DELAY_DEV, devDelay.ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CODEGAP_DEV, devCodeGap.ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.DELAYPULSE_DEV, devDelayPulse.ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.DUTYCYCLE_DEV, devDutyCycle.ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.KEEPFI, Convert.ToInt16(keepFIcheckBox.Checked).ToString());

            UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CMPPULSECHKBOX, Convert.ToInt16(PulseDurationCBox.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CMPDELAYCHKBOX, Convert.ToInt16(DelayDurationCBox.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CMPFREQCHKBOX, Convert.ToInt16(FrequencyCBox.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CMPCODEGAP, Convert.ToInt16(CodeGapCBox.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CMPDUTYCHKBOX, Convert.ToInt16(DutyCycleCBox.Checked).ToString());
            UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.TICMPFRAMES, Convert.ToInt16(CmpFramesChkBox.Checked).ToString());

        }
    
    }
}