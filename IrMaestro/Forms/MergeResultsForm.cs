using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommonForms;

namespace IRMaestro.Forms
{
    public partial class MergeResultsForm : Form
    {
        public MergeResultsForm()
        {
            InitializeComponent();
            label1.Text = "Results.RPT already exists in the working folder.\r\n" +
                          "Do you want to merge the results?\r\n\r\n" +
                          "Select 'Yes' to merge results.\r\n" +
                          "Select 'No' to choose another working folder.";

            label2.Text = "New IDs will be appended.\r\n" +
                          "Previous run IDs will be updated.\r\n";


                          
        }

        private void yesButton_Click(object sender, EventArgs e)
        {
            if (CFIListForm.bShowTimingReport == false)
            {
                if (shownewrunRbtn.Checked)
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                 UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                                 "1");
                }
                else
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                    UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                                    "0");
                }
            }
            else
            {
                if (shownewrunRbtn.Checked)
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                 UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                                 "1");
                }
                else
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                    UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                                    "0");
                }
            
            }
            this.DialogResult = DialogResult.Yes;
        }

        private void noButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
        }

        private void shownewrunRbtn_CheckedChanged(object sender, EventArgs e)
        {
            if (CFIListForm.bShowTimingReport == false)
            {
                if (shownewrunRbtn.Checked)
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                 UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                                 "1");
                }
                else
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                    UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                                    "0");
                }
            }
            else
            {
                if (shownewrunRbtn.Checked)
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                 UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                                 "1");
                }
                else
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                    UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                                    "0");
                }
            }

        }

        private void showAllRbtn_CheckedChanged(object sender, EventArgs e)
        {
            if (CFIListForm.bShowTimingReport == false)
            {
                if (showAllRbtn.Checked)
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                 UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                                 "0");
                }
                else
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                    UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                                    "1");
                }
            }
            else
            {
                if (showAllRbtn.Checked)
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                 UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                                 "0");
                }
                else
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                    UserSettings.ConfigKeys.MERGE_REPORT_SHOWALL,
                                                    "1");
                }
            }

        }
    }
}