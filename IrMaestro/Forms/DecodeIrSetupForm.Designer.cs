namespace IRMaestro {
    partial class DecodeIrSetupForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DecodeIrSetupForm));
            this.UseGenericExecCheckBox = new System.Windows.Forms.CheckBox();
            this.IdFilesCheckBox = new System.Windows.Forms.CheckBox();
            this.KeepCfiFilesCheckBox = new System.Windows.Forms.CheckBox();
            this.SourceFileInput = new CommonUI.Controls.FileInput();
            this.ExecutorComboList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.OkButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // UseGenericExecCheckBox
            // 
            this.UseGenericExecCheckBox.AutoSize = true;
            this.UseGenericExecCheckBox.Location = new System.Drawing.Point(90, 84);
            this.UseGenericExecCheckBox.Name = "UseGenericExecCheckBox";
            this.UseGenericExecCheckBox.Size = new System.Drawing.Size(135, 17);
            this.UseGenericExecCheckBox.TabIndex = 0;
            this.UseGenericExecCheckBox.Text = "Load Generic Executor";
            this.UseGenericExecCheckBox.UseVisualStyleBackColor = true;
            this.UseGenericExecCheckBox.CheckedChanged += new System.EventHandler(this.UseGenericExecCheckBox_CheckedChanged);
            // 
            // IdFilesCheckBox
            // 
            this.IdFilesCheckBox.AutoSize = true;
            this.IdFilesCheckBox.Location = new System.Drawing.Point(90, 106);
            this.IdFilesCheckBox.Name = "IdFilesCheckBox";
            this.IdFilesCheckBox.Size = new System.Drawing.Size(172, 17);
            this.IdFilesCheckBox.TabIndex = 1;
            this.IdFilesCheckBox.Text = "Identify File(s) before Decoding";
            this.IdFilesCheckBox.UseVisualStyleBackColor = true;
            // 
            // KeepCfiFilesCheckBox
            // 
            this.KeepCfiFilesCheckBox.AutoSize = true;
            this.KeepCfiFilesCheckBox.Location = new System.Drawing.Point(90, 129);
            this.KeepCfiFilesCheckBox.Name = "KeepCfiFilesCheckBox";
            this.KeepCfiFilesCheckBox.Size = new System.Drawing.Size(173, 17);
            this.KeepCfiFilesCheckBox.TabIndex = 2;
            this.KeepCfiFilesCheckBox.Text = "Keep CFI File(s) after Decoding";
            this.KeepCfiFilesCheckBox.UseVisualStyleBackColor = true;
            // 
            // SourceFileInput
            // 
            this.SourceFileInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SourceFileInput.FilePath = "";
            this.SourceFileInput.Filter = "Text Files (*.TXT)|*.TXT|(*.LST)|*.LST|All Files (*.*)|*.*";
            this.SourceFileInput.Location = new System.Drawing.Point(44, 12);
            this.SourceFileInput.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.SourceFileInput.Name = "SourceFileInput";
            this.SourceFileInput.Size = new System.Drawing.Size(302, 26);
            this.SourceFileInput.TabIndex = 3;
            this.SourceFileInput.Title = "Open";
            this.SourceFileInput.UseSaveDialog = false;
            // 
            // ExecutorComboList
            // 
            this.ExecutorComboList.FormattingEnabled = true;
            this.ExecutorComboList.Location = new System.Drawing.Point(150, 57);
            this.ExecutorComboList.Name = "ExecutorComboList";
            this.ExecutorComboList.Size = new System.Drawing.Size(72, 21);
            this.ExecutorComboList.TabIndex = 4;
            this.ExecutorComboList.DropDownClosed += new System.EventHandler(this.ExecutorComboList_DropDownClosed);
            this.ExecutorComboList.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ExecutorComboList_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(87, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Executor";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "File";
            // 
            // OkButton
            // 
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(96, 170);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(80, 22);
            this.OkButton.TabIndex = 7;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CloseButton.Location = new System.Drawing.Point(182, 170);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(80, 22);
            this.CloseButton.TabIndex = 8;
            this.CloseButton.Text = "Cancel";
            this.CloseButton.UseVisualStyleBackColor = true;
            // 
            // DecodeIrSetupForm
            // 
            this.AcceptButton = this.OkButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.CloseButton;
            this.ClientSize = new System.Drawing.Size(358, 205);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ExecutorComboList);
            this.Controls.Add(this.SourceFileInput);
            this.Controls.Add(this.KeepCfiFilesCheckBox);
            this.Controls.Add(this.IdFilesCheckBox);
            this.Controls.Add(this.UseGenericExecCheckBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DecodeIrSetupForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DecodeIrSetupForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox UseGenericExecCheckBox;
        private System.Windows.Forms.CheckBox IdFilesCheckBox;
        private System.Windows.Forms.CheckBox KeepCfiFilesCheckBox;
        private CommonUI.Controls.FileInput SourceFileInput;
        private System.Windows.Forms.ComboBox ExecutorComboList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button CloseButton;
    }
}