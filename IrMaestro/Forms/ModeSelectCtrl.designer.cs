namespace CommonUI
{
    partial class ModeSelectCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cb_Z = new System.Windows.Forms.CheckBox();
            this.cb_Y = new System.Windows.Forms.CheckBox();
            this.cb_X = new System.Windows.Forms.CheckBox();
            this.cb_W = new System.Windows.Forms.CheckBox();
            this.cb_V = new System.Windows.Forms.CheckBox();
            this.cb_U = new System.Windows.Forms.CheckBox();
            this.cb_T = new System.Windows.Forms.CheckBox();
            this.cb_S = new System.Windows.Forms.CheckBox();
            this.cb_R = new System.Windows.Forms.CheckBox();
            this.cb_Q = new System.Windows.Forms.CheckBox();
            this.cb_P = new System.Windows.Forms.CheckBox();
            this.cb_O = new System.Windows.Forms.CheckBox();
            this.cb_N = new System.Windows.Forms.CheckBox();
            this.cb_M = new System.Windows.Forms.CheckBox();
            this.cb_L = new System.Windows.Forms.CheckBox();
            this.cb_K = new System.Windows.Forms.CheckBox();
            this.cb_J = new System.Windows.Forms.CheckBox();
            this.cb_I = new System.Windows.Forms.CheckBox();
            this.cb_H = new System.Windows.Forms.CheckBox();
            this.cb_G = new System.Windows.Forms.CheckBox();
            this.cb_F = new System.Windows.Forms.CheckBox();
            this.cb_E = new System.Windows.Forms.CheckBox();
            this.cb_D = new System.Windows.Forms.CheckBox();
            this.cb_C = new System.Windows.Forms.CheckBox();
            this.cb_B = new System.Windows.Forms.CheckBox();
            this.cb_A = new System.Windows.Forms.CheckBox();
            this.cb_All = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // cb_Z
            // 
            this.cb_Z.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_Z.AutoSize = true;
            this.cb_Z.Location = new System.Drawing.Point(257, 53);
            this.cb_Z.Margin = new System.Windows.Forms.Padding(0);
            this.cb_Z.Name = "cb_Z";
            this.cb_Z.Size = new System.Drawing.Size(33, 17);
            this.cb_Z.TabIndex = 26;
            this.cb_Z.TabStop = false;
            this.cb_Z.Tag = "Z";
            this.cb_Z.Text = "Z";
            this.cb_Z.UseVisualStyleBackColor = true;
            this.cb_Z.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_Y
            // 
            this.cb_Y.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_Y.AutoSize = true;
            this.cb_Y.Location = new System.Drawing.Point(226, 53);
            this.cb_Y.Margin = new System.Windows.Forms.Padding(0);
            this.cb_Y.Name = "cb_Y";
            this.cb_Y.Size = new System.Drawing.Size(33, 17);
            this.cb_Y.TabIndex = 25;
            this.cb_Y.TabStop = false;
            this.cb_Y.Tag = "Y";
            this.cb_Y.Text = "Y";
            this.cb_Y.UseVisualStyleBackColor = true;
            this.cb_Y.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_X
            // 
            this.cb_X.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_X.AutoSize = true;
            this.cb_X.Location = new System.Drawing.Point(195, 53);
            this.cb_X.Margin = new System.Windows.Forms.Padding(0);
            this.cb_X.Name = "cb_X";
            this.cb_X.Size = new System.Drawing.Size(33, 17);
            this.cb_X.TabIndex = 24;
            this.cb_X.TabStop = false;
            this.cb_X.Tag = "X";
            this.cb_X.Text = "X";
            this.cb_X.UseVisualStyleBackColor = true;
            this.cb_X.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_W
            // 
            this.cb_W.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_W.AutoSize = true;
            this.cb_W.Location = new System.Drawing.Point(164, 53);
            this.cb_W.Margin = new System.Windows.Forms.Padding(0);
            this.cb_W.Name = "cb_W";
            this.cb_W.Size = new System.Drawing.Size(37, 17);
            this.cb_W.TabIndex = 23;
            this.cb_W.TabStop = false;
            this.cb_W.Tag = "W";
            this.cb_W.Text = "W";
            this.cb_W.UseVisualStyleBackColor = true;
            this.cb_W.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_V
            // 
            this.cb_V.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_V.AutoSize = true;
            this.cb_V.Location = new System.Drawing.Point(134, 53);
            this.cb_V.Margin = new System.Windows.Forms.Padding(0);
            this.cb_V.Name = "cb_V";
            this.cb_V.Size = new System.Drawing.Size(33, 17);
            this.cb_V.TabIndex = 22;
            this.cb_V.TabStop = false;
            this.cb_V.Tag = "V";
            this.cb_V.Text = "V";
            this.cb_V.UseVisualStyleBackColor = true;
            this.cb_V.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_U
            // 
            this.cb_U.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_U.AutoSize = true;
            this.cb_U.Location = new System.Drawing.Point(103, 53);
            this.cb_U.Margin = new System.Windows.Forms.Padding(0);
            this.cb_U.Name = "cb_U";
            this.cb_U.Size = new System.Drawing.Size(34, 17);
            this.cb_U.TabIndex = 21;
            this.cb_U.TabStop = false;
            this.cb_U.Tag = "U";
            this.cb_U.Text = "U";
            this.cb_U.UseVisualStyleBackColor = true;
            this.cb_U.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_T
            // 
            this.cb_T.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_T.AutoSize = true;
            this.cb_T.Location = new System.Drawing.Point(73, 53);
            this.cb_T.Margin = new System.Windows.Forms.Padding(0);
            this.cb_T.Name = "cb_T";
            this.cb_T.Size = new System.Drawing.Size(33, 17);
            this.cb_T.TabIndex = 20;
            this.cb_T.TabStop = false;
            this.cb_T.Tag = "T";
            this.cb_T.Text = "T";
            this.cb_T.UseVisualStyleBackColor = true;
            this.cb_T.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_S
            // 
            this.cb_S.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_S.AutoSize = true;
            this.cb_S.Location = new System.Drawing.Point(42, 53);
            this.cb_S.Margin = new System.Windows.Forms.Padding(0);
            this.cb_S.Name = "cb_S";
            this.cb_S.Size = new System.Drawing.Size(33, 17);
            this.cb_S.TabIndex = 19;
            this.cb_S.TabStop = false;
            this.cb_S.Tag = "S";
            this.cb_S.Text = "S";
            this.cb_S.UseVisualStyleBackColor = true;
            this.cb_S.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_R
            // 
            this.cb_R.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_R.AutoSize = true;
            this.cb_R.Location = new System.Drawing.Point(288, 26);
            this.cb_R.Margin = new System.Windows.Forms.Padding(0);
            this.cb_R.Name = "cb_R";
            this.cb_R.Size = new System.Drawing.Size(34, 17);
            this.cb_R.TabIndex = 18;
            this.cb_R.TabStop = false;
            this.cb_R.Tag = "R";
            this.cb_R.Text = "R";
            this.cb_R.UseVisualStyleBackColor = true;
            this.cb_R.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_Q
            // 
            this.cb_Q.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_Q.AutoSize = true;
            this.cb_Q.Location = new System.Drawing.Point(257, 26);
            this.cb_Q.Margin = new System.Windows.Forms.Padding(0);
            this.cb_Q.Name = "cb_Q";
            this.cb_Q.Size = new System.Drawing.Size(34, 17);
            this.cb_Q.TabIndex = 17;
            this.cb_Q.TabStop = false;
            this.cb_Q.Tag = "Q";
            this.cb_Q.Text = "Q";
            this.cb_Q.UseVisualStyleBackColor = true;
            this.cb_Q.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_P
            // 
            this.cb_P.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_P.AutoSize = true;
            this.cb_P.Location = new System.Drawing.Point(226, 26);
            this.cb_P.Margin = new System.Windows.Forms.Padding(0);
            this.cb_P.Name = "cb_P";
            this.cb_P.Size = new System.Drawing.Size(33, 17);
            this.cb_P.TabIndex = 16;
            this.cb_P.TabStop = false;
            this.cb_P.Tag = "P";
            this.cb_P.Text = "P";
            this.cb_P.UseVisualStyleBackColor = true;
            this.cb_P.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_O
            // 
            this.cb_O.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_O.AutoSize = true;
            this.cb_O.Location = new System.Drawing.Point(195, 26);
            this.cb_O.Margin = new System.Windows.Forms.Padding(0);
            this.cb_O.Name = "cb_O";
            this.cb_O.Size = new System.Drawing.Size(34, 17);
            this.cb_O.TabIndex = 15;
            this.cb_O.TabStop = false;
            this.cb_O.Tag = "O";
            this.cb_O.Text = "O";
            this.cb_O.UseVisualStyleBackColor = true;
            this.cb_O.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_N
            // 
            this.cb_N.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_N.AutoSize = true;
            this.cb_N.Location = new System.Drawing.Point(164, 26);
            this.cb_N.Margin = new System.Windows.Forms.Padding(0);
            this.cb_N.Name = "cb_N";
            this.cb_N.Size = new System.Drawing.Size(34, 17);
            this.cb_N.TabIndex = 14;
            this.cb_N.TabStop = false;
            this.cb_N.Tag = "N";
            this.cb_N.Text = "N";
            this.cb_N.UseVisualStyleBackColor = true;
            this.cb_N.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_M
            // 
            this.cb_M.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_M.AutoSize = true;
            this.cb_M.Location = new System.Drawing.Point(134, 26);
            this.cb_M.Margin = new System.Windows.Forms.Padding(0);
            this.cb_M.Name = "cb_M";
            this.cb_M.Size = new System.Drawing.Size(35, 17);
            this.cb_M.TabIndex = 13;
            this.cb_M.TabStop = false;
            this.cb_M.Tag = "M";
            this.cb_M.Text = "M";
            this.cb_M.UseVisualStyleBackColor = true;
            this.cb_M.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_L
            // 
            this.cb_L.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_L.AutoSize = true;
            this.cb_L.Location = new System.Drawing.Point(103, 26);
            this.cb_L.Margin = new System.Windows.Forms.Padding(0);
            this.cb_L.Name = "cb_L";
            this.cb_L.Size = new System.Drawing.Size(32, 17);
            this.cb_L.TabIndex = 12;
            this.cb_L.TabStop = false;
            this.cb_L.Tag = "L";
            this.cb_L.Text = "L";
            this.cb_L.UseVisualStyleBackColor = true;
            this.cb_L.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_K
            // 
            this.cb_K.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_K.AutoSize = true;
            this.cb_K.Location = new System.Drawing.Point(73, 26);
            this.cb_K.Margin = new System.Windows.Forms.Padding(0);
            this.cb_K.Name = "cb_K";
            this.cb_K.Size = new System.Drawing.Size(33, 17);
            this.cb_K.TabIndex = 11;
            this.cb_K.TabStop = false;
            this.cb_K.Tag = "K";
            this.cb_K.Text = "K";
            this.cb_K.UseVisualStyleBackColor = true;
            this.cb_K.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_J
            // 
            this.cb_J.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_J.AutoSize = true;
            this.cb_J.Location = new System.Drawing.Point(42, 26);
            this.cb_J.Margin = new System.Windows.Forms.Padding(0);
            this.cb_J.Name = "cb_J";
            this.cb_J.Size = new System.Drawing.Size(31, 17);
            this.cb_J.TabIndex = 10;
            this.cb_J.TabStop = false;
            this.cb_J.Tag = "J";
            this.cb_J.Text = "J";
            this.cb_J.UseVisualStyleBackColor = true;
            this.cb_J.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_I
            // 
            this.cb_I.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_I.AutoSize = true;
            this.cb_I.Location = new System.Drawing.Point(288, 0);
            this.cb_I.Margin = new System.Windows.Forms.Padding(0);
            this.cb_I.Name = "cb_I";
            this.cb_I.Size = new System.Drawing.Size(29, 17);
            this.cb_I.TabIndex = 9;
            this.cb_I.TabStop = false;
            this.cb_I.Tag = "I";
            this.cb_I.Text = "I";
            this.cb_I.UseVisualStyleBackColor = true;
            this.cb_I.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_H
            // 
            this.cb_H.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_H.AutoSize = true;
            this.cb_H.Location = new System.Drawing.Point(257, 0);
            this.cb_H.Margin = new System.Windows.Forms.Padding(0);
            this.cb_H.Name = "cb_H";
            this.cb_H.Size = new System.Drawing.Size(34, 17);
            this.cb_H.TabIndex = 8;
            this.cb_H.TabStop = false;
            this.cb_H.Tag = "H";
            this.cb_H.Text = "H";
            this.cb_H.UseVisualStyleBackColor = true;
            this.cb_H.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_G
            // 
            this.cb_G.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_G.AutoSize = true;
            this.cb_G.Location = new System.Drawing.Point(226, 0);
            this.cb_G.Margin = new System.Windows.Forms.Padding(0);
            this.cb_G.Name = "cb_G";
            this.cb_G.Size = new System.Drawing.Size(34, 17);
            this.cb_G.TabIndex = 7;
            this.cb_G.TabStop = false;
            this.cb_G.Tag = "G";
            this.cb_G.Text = "G";
            this.cb_G.UseVisualStyleBackColor = true;
            this.cb_G.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_F
            // 
            this.cb_F.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_F.AutoSize = true;
            this.cb_F.Location = new System.Drawing.Point(195, 0);
            this.cb_F.Margin = new System.Windows.Forms.Padding(0);
            this.cb_F.Name = "cb_F";
            this.cb_F.Size = new System.Drawing.Size(32, 17);
            this.cb_F.TabIndex = 6;
            this.cb_F.TabStop = false;
            this.cb_F.Tag = "F";
            this.cb_F.Text = "F";
            this.cb_F.UseVisualStyleBackColor = true;
            this.cb_F.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_E
            // 
            this.cb_E.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_E.AutoSize = true;
            this.cb_E.Location = new System.Drawing.Point(164, 0);
            this.cb_E.Margin = new System.Windows.Forms.Padding(0);
            this.cb_E.Name = "cb_E";
            this.cb_E.Size = new System.Drawing.Size(33, 17);
            this.cb_E.TabIndex = 5;
            this.cb_E.TabStop = false;
            this.cb_E.Tag = "E";
            this.cb_E.Text = "E";
            this.cb_E.UseVisualStyleBackColor = true;
            this.cb_E.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_D
            // 
            this.cb_D.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_D.AutoSize = true;
            this.cb_D.Location = new System.Drawing.Point(134, 0);
            this.cb_D.Margin = new System.Windows.Forms.Padding(0);
            this.cb_D.Name = "cb_D";
            this.cb_D.Size = new System.Drawing.Size(34, 17);
            this.cb_D.TabIndex = 4;
            this.cb_D.TabStop = false;
            this.cb_D.Tag = "D";
            this.cb_D.Text = "D";
            this.cb_D.UseVisualStyleBackColor = true;
            this.cb_D.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_C
            // 
            this.cb_C.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_C.AutoSize = true;
            this.cb_C.Location = new System.Drawing.Point(103, 0);
            this.cb_C.Margin = new System.Windows.Forms.Padding(0);
            this.cb_C.Name = "cb_C";
            this.cb_C.Size = new System.Drawing.Size(33, 17);
            this.cb_C.TabIndex = 3;
            this.cb_C.TabStop = false;
            this.cb_C.Tag = "C";
            this.cb_C.Text = "C";
            this.cb_C.UseVisualStyleBackColor = true;
            this.cb_C.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_B
            // 
            this.cb_B.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_B.AutoSize = true;
            this.cb_B.Location = new System.Drawing.Point(73, 0);
            this.cb_B.Margin = new System.Windows.Forms.Padding(0);
            this.cb_B.Name = "cb_B";
            this.cb_B.Size = new System.Drawing.Size(33, 17);
            this.cb_B.TabIndex = 2;
            this.cb_B.TabStop = false;
            this.cb_B.Tag = "B";
            this.cb_B.Text = "B";
            this.cb_B.UseVisualStyleBackColor = true;
            this.cb_B.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_A
            // 
            this.cb_A.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_A.AutoSize = true;
            this.cb_A.Location = new System.Drawing.Point(42, 0);
            this.cb_A.Margin = new System.Windows.Forms.Padding(0);
            this.cb_A.Name = "cb_A";
            this.cb_A.Size = new System.Drawing.Size(33, 17);
            this.cb_A.TabIndex = 1;
            this.cb_A.TabStop = false;
            this.cb_A.Tag = "A";
            this.cb_A.Text = "A";
            this.cb_A.UseVisualStyleBackColor = true;
            this.cb_A.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // cb_All
            // 
            this.cb_All.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cb_All.AutoSize = true;
            this.cb_All.Location = new System.Drawing.Point(1, 0);
            this.cb_All.Margin = new System.Windows.Forms.Padding(0);
            this.cb_All.Name = "cb_All";
            this.cb_All.Size = new System.Drawing.Size(37, 17);
            this.cb_All.TabIndex = 0;
            this.cb_All.Tag = "All";
            this.cb_All.Text = "All";
            this.cb_All.UseVisualStyleBackColor = true;
            this.cb_All.CheckedChanged += new System.EventHandler(this.cb_CheckChanged);
            // 
            // ModeSelectCtrl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.cb_Z);
            this.Controls.Add(this.cb_Y);
            this.Controls.Add(this.cb_X);
            this.Controls.Add(this.cb_W);
            this.Controls.Add(this.cb_V);
            this.Controls.Add(this.cb_U);
            this.Controls.Add(this.cb_T);
            this.Controls.Add(this.cb_S);
            this.Controls.Add(this.cb_R);
            this.Controls.Add(this.cb_Q);
            this.Controls.Add(this.cb_P);
            this.Controls.Add(this.cb_O);
            this.Controls.Add(this.cb_N);
            this.Controls.Add(this.cb_M);
            this.Controls.Add(this.cb_L);
            this.Controls.Add(this.cb_K);
            this.Controls.Add(this.cb_J);
            this.Controls.Add(this.cb_I);
            this.Controls.Add(this.cb_H);
            this.Controls.Add(this.cb_G);
            this.Controls.Add(this.cb_F);
            this.Controls.Add(this.cb_E);
            this.Controls.Add(this.cb_D);
            this.Controls.Add(this.cb_C);
            this.Controls.Add(this.cb_B);
            this.Controls.Add(this.cb_A);
            this.Controls.Add(this.cb_All);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ModeSelectCtrl";
            this.Size = new System.Drawing.Size(314, 65);
            this.Load += new System.EventHandler(this.ModeSelectCtrl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cb_Z;
        private System.Windows.Forms.CheckBox cb_Y;
        private System.Windows.Forms.CheckBox cb_X;
        private System.Windows.Forms.CheckBox cb_W;
        private System.Windows.Forms.CheckBox cb_V;
        private System.Windows.Forms.CheckBox cb_U;
        private System.Windows.Forms.CheckBox cb_T;
        private System.Windows.Forms.CheckBox cb_S;
        private System.Windows.Forms.CheckBox cb_R;
        private System.Windows.Forms.CheckBox cb_Q;
        private System.Windows.Forms.CheckBox cb_P;
        private System.Windows.Forms.CheckBox cb_O;
        private System.Windows.Forms.CheckBox cb_N;
        private System.Windows.Forms.CheckBox cb_M;
        private System.Windows.Forms.CheckBox cb_L;
        private System.Windows.Forms.CheckBox cb_K;
        private System.Windows.Forms.CheckBox cb_J;
        private System.Windows.Forms.CheckBox cb_I;
        private System.Windows.Forms.CheckBox cb_H;
        private System.Windows.Forms.CheckBox cb_G;
        private System.Windows.Forms.CheckBox cb_F;
        private System.Windows.Forms.CheckBox cb_E;
        private System.Windows.Forms.CheckBox cb_D;
        private System.Windows.Forms.CheckBox cb_C;
        private System.Windows.Forms.CheckBox cb_B;
        private System.Windows.Forms.CheckBox cb_A;
        private System.Windows.Forms.CheckBox cb_All;

    }
}
