namespace IRMaestro
{
    partial class IDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OkBttn = new System.Windows.Forms.Button();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            ReadBackBGWorker = new System.ComponentModel.BackgroundWorker();
            this.idFilterConditionsControl = new CommonUI.Controls.IdFilterConditionsControl();
            this.SuspendLayout();
            // 
            // OkBttn
            // 
            this.OkBttn.Location = new System.Drawing.Point(112, 310);
            this.OkBttn.Name = "OkBttn";
            this.OkBttn.Size = new System.Drawing.Size(75, 23);
            this.OkBttn.TabIndex = 56;
            this.OkBttn.Text = "Ok";
            this.OkBttn.UseVisualStyleBackColor = true;
            this.OkBttn.Click += new System.EventHandler(this.OkBttn_Click);
            // 
            // CancelBttn
            // 
            this.CancelBttn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBttn.Location = new System.Drawing.Point(234, 310);
            this.CancelBttn.Name = "CancelBttn";
            this.CancelBttn.Size = new System.Drawing.Size(75, 23);
            this.CancelBttn.TabIndex = 57;
            this.CancelBttn.Text = "Cancel";
            this.CancelBttn.UseVisualStyleBackColor = true;
            this.CancelBttn.Click += new System.EventHandler(this.CancelBttn_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(36, 267);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(349, 23);
            this.progressBar.TabIndex = 58;
            // 
            // ReadBackBGWorker
            // 
            ReadBackBGWorker.WorkerReportsProgress = true;
            ReadBackBGWorker.WorkerSupportsCancellation = true;
            ReadBackBGWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ReadBackBGWorker_DoWork);
            ReadBackBGWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.ReadBackBGWorker_ProgressChanged);
            ReadBackBGWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ReadBackBGWorker_RunWorkerCompleted);
            // 
            // idFilterConditionsControl
            // 
            this.idFilterConditionsControl.Location = new System.Drawing.Point(12, 12);
            this.idFilterConditionsControl.Name = "idFilterConditionsControl";
            this.idFilterConditionsControl.Size = new System.Drawing.Size(391, 232);
            this.idFilterConditionsControl.TabIndex = 60;
            // 
            // IDForm
            // 
            this.AcceptButton = this.OkBttn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.CancelButton = this.CancelBttn;
            this.ClientSize = new System.Drawing.Size(420, 349);
            this.Controls.Add(this.idFilterConditionsControl);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.OkBttn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "IDForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ID Selection";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button OkBttn;
        private System.Windows.Forms.Button CancelBttn;
        private System.Windows.Forms.ProgressBar progressBar;
        private CommonUI.Controls.IdFilterConditionsControl idFilterConditionsControl;
        public static System.ComponentModel.BackgroundWorker ReadBackBGWorker;
    }
}