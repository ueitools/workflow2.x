using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using CommonForms.Forms;
using BusinessObject;

namespace IRMaestro.Forms
{
    public partial class PickToCFIDlg : Form
    {
        string pickPath;
        string cfiPath;
        const string _UEITOOLSROOT =
        "HKEY_CURRENT_USER\\Software\\UEITools\\";
        const string _PICKDBDIR = "IRMaestroPickDBDirectory";
        const string _CFIDIR = "IRMaestroCFIDirectory";
        const string _DEFAULT_PICKDBDIR = "";
        const string _PICKCFIDB = "IRMaestroCFISource";
        const string _PRODUCTNAME = "IRMaestroProductName";
        string userConnectionString = "";
        bool sourceIsPickOrDBFlag = true;

        public PickToCFIDlg()
        {
            InitializeComponent();
        }

        public string GetPickPath()
        {
            return pickPath;
        }

        public string GetProductDBConnectionString()
        {
            return userConnectionString;
        }

        public bool IsPickSource()
        {
            return sourceIsPickOrDBFlag;
        }

        public string GetCFIPath()
        {
            return cfiPath;
        }

        public IList<string> GetIDList(IList<string> allIDList)
        {
            return idSelectionControl1.GetSelectedIDList(allIDList);
        }

        public string GetProductName()
        {
            string defaultValue = "";

            return (string)Registry.GetValue(_UEITOOLSROOT, _PRODUCTNAME, defaultValue);
        }

        private void SetProductName(string name)
        {
            Registry.SetValue(_UEITOOLSROOT, _PRODUCTNAME,
                name, RegistryValueKind.String);
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (idSelectionControl1.ValidateAndGetData() == false)
            {                
                return;
            }
            cfiPath = directoryInput1.DirPath;
            pickPath = pickInput.DirPath;            
            SetPickDBDir(pickPath);
            SetCFIDir(cfiPath);
            SetProductName(productComboBox.Text);
            sourceIsPickOrDBFlag = pickDBRadioButton.Checked;                
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void PickToCFIDlg_Load(object sender, EventArgs e)
        {
            pickInput.DirPath = GetPickDBDir();
            directoryInput1.DirPath = GetCFIDir();
            DBSelectorComboBoxSettings dbSettings = new DBSelectorComboBoxSettings();
            DatabaseSelectorComboBox.SelectedValueChanged callback =  HandleDatabaseConnectionString;
            databaseSelectorComboBox1.Init(dbSettings, _UEITOOLSROOT, _PICKCFIDB, callback);
            idSelectionControl1.AvailableModes = new List<string>(new string[] {"T", "Y", "A", "R", "M"});
        }

        private void HandleDatabaseConnectionString(DBSelectorComboBoxSettings.DB choice)
        {
            switch (choice)
            {
                case DBSelectorComboBoxSettings.DB.Public:
                    userConnectionString = DBConnectionString.PRODUCT;
                    break;
                case DBSelectorComboBoxSettings.DB.Working:
                    userConnectionString = DBConnectionString.WORKPRODUCT;
                    break;
                case DBSelectorComboBoxSettings.DB.Local :
                    userConnectionString = DBConnectionString.LOCALPRODUCT;
                    break;
            }
            DAOFactory.ResetDBConnection(userConnectionString);
            ProjectHeaderCollection projectList;
            try
            {
                productComboBox.Items.Clear();
                projectList = ProductDBFunctions.GetAllProjects();

                foreach (ProjectHeader project in projectList)
                {
                    productComboBox.Items.Add(project.Name);
                }
                string oldName = GetProductName();
                foreach (string name in productComboBox.Items)
                {
                    if (name == oldName)
                        productComboBox.SelectedItem = name;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private string GetPickDBDir()
        {
            return (string)Registry.GetValue(
                _UEITOOLSROOT, _PICKDBDIR, _DEFAULT_PICKDBDIR);
        }

        private void SetPickDBDir(string selectedPath)
        {
            Registry.SetValue(_UEITOOLSROOT, _PICKDBDIR,
                selectedPath, RegistryValueKind.String);
        }

        private string GetCFIDir()
        {
            return (string)Registry.GetValue(
                _UEITOOLSROOT, _CFIDIR, _DEFAULT_PICKDBDIR);
        }

        private void SetCFIDir(string selectedPath)
        {
            Registry.SetValue(_UEITOOLSROOT, _CFIDIR,
                selectedPath, RegistryValueKind.String);
        }

        private void productComboBox_DropDown(object sender, EventArgs e)
        {
            pickDBRadioButton.Checked = false;
            productDBRadioButton.Checked = true;
        }

        private void databaseSelectorComboBox1_Enter(object sender, EventArgs e)
        {
            pickDBRadioButton.Checked = false;
            productDBRadioButton.Checked = true;
        }

        private void pickInput_Enter(object sender, EventArgs e)
        {
            pickDBRadioButton.Checked = true;
            productDBRadioButton.Checked = false;
        }

        public void Test()
        {
            cfiPath = "c:\\alternatecode\\";
            SetProductName("LIMTEST07Oct-1");
            userConnectionString = DBConnectionString.LOCALPRODUCT;
            sourceIsPickOrDBFlag = false;
        }
    }
}