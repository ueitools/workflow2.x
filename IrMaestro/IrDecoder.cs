using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using CommonForms;
using CommonUI.Controls;
using CommonCode;

namespace IRMaestro {
    public class IrDecoder : IDisposable {
        private ITempFilePath _cfiOutputPathManager;
        private IProcessingForm _processingForm;

        private string _lastErrorMessage;
        private bool _singleCaptureFile;

        private string _filePath;
        private bool _identifyCapture;
        private int _definedExec;
        private string _genericExecConfigPath;
        private bool _keepCfiFiles;

        private List<CaptureInfo> _captureSetFiles;
        private List<string> _cfiFileList;
        private IWin32Window _ownerWindow;

        public IrDecoder(string filePath) {
            _filePath = filePath.ToUpper();

            _cfiOutputPathManager = new TempFilePath(true);
            _ownerWindow = null;

            _lastErrorMessage = string.Empty;
            _identifyCapture = false;
            _definedExec = 0;

            _processingForm = FormFactory.GetInstance().GetProcessingForm();
            _processingForm.ShowProgress = false;
        }

        public bool IdentifyCapture {
            get { return _identifyCapture; }
            set { _identifyCapture = value; }
        }

        public int DefinedExec {
            get { return _definedExec; }
            set { _definedExec = value; }
        }

        public string GenericExecConfigPath {
            get { return _genericExecConfigPath; }
            set { _genericExecConfigPath = value; }
        }

        public string LastErrorMessage {
            get { return _lastErrorMessage; }
        }

        public bool KeepCfiFiles {
            get { return _keepCfiFiles; }
            set { _keepCfiFiles = value; }
        }

        private bool InitDecoder(IWin32Window ownerWindow) {
            bool result = true;

            if (_filePath.Contains("ZIP")) {
                _singleCaptureFile = false;
            } else if (_filePath.Contains("U1") || _filePath.Contains("U2") || _filePath.Contains("FI")) {
                _singleCaptureFile = true;
            } else {
                _lastErrorMessage = "Input file format is invalid!!";
                result = false;
                _singleCaptureFile = false;
            }
            _ownerWindow = ownerWindow;

            return result;
        }

        public void Dispose() {
            _cfiOutputPathManager.Dispose();
               
            GC.SuppressFinalize(this);
        }

        public bool StartProcess(IWin32Window owner) {
            if (InitDecoder(owner) == false) {
                return false;
            }
            ((IFormFactoryForm)_processingForm).Show(owner);

            bool result = false;
            try {
                if (!_singleCaptureFile) {
                    result = ProcessCaptureSet();
                } else {
                    result = ProcessCaptureFile();
                }
                if (result && (_keepCfiFiles || _singleCaptureFile)) {
                    _cfiOutputPathManager.CopyToWorking("*.cfi");
                }
            } catch (UserCancelledException) {
                _lastErrorMessage = "User cancelled the process.";
                return false;
            } finally {
                ((IFormFactoryForm)_processingForm).Close();
            }
            return result;
        }

        private bool ProcessCaptureSet() {
            if (_processingForm.Cancelled) {
                throw new UserCancelledException();
            }

            bool cfiCreationResults = false;
            _processingForm.SetMessage("Extracting capture set...\n\n{0}", _filePath);
            using (IIrCaptureSet irCaptureSet = new IrCaptureSet()) {
                if (irCaptureSet.SetCaptureSet(_filePath) == false) {
                    _lastErrorMessage = "Unable to handle file!!";
                    return false;
                }

                _captureSetFiles = irCaptureSet.GetCaptureInfoFullPath();
                if (_identifyCapture) {
                    _processingForm.SetMessage("Identifying capture set...\n\n{0}", _filePath);

                    ExecutorList possibleExecutors = GetPossibleExecutors(_captureSetFiles);
                    _definedExec = ValidateOrRequestExecutor(possibleExecutors, _definedExec);
                }

                _processingForm.SetMessage("Making CFI Files from Capture set...\n\n{0}", _filePath);
                _cfiFileList = new List<string>();
                cfiCreationResults = CreateCfiFromCaptureSet();
            }
            return cfiCreationResults;
        }

        private bool ProcessCaptureFile() {
            if (_identifyCapture) {
                _processingForm.SetMessage("Identifying capture file...\n\n{0}", _filePath);

                ExecutorList possibleExecutors = GetPossibleExecutors(_filePath);
                _definedExec = ValidateOrRequestExecutor(possibleExecutors, _definedExec);
            }
            _processingForm.SetMessage("Making CFI File from Capture...\n\n{0}", _filePath);
            _cfiFileList = new List<string>();
            return CreateCFI(" ", _filePath);
        }

        private ExecutorList GetPossibleExecutors(string captureFile) {
            ExecutorList executorList = new ExecutorList(FunctionList.GetExecutorListFromU1File(captureFile));
            executorList.Sort();
            return executorList;
        }

        private ExecutorList GetPossibleExecutors(IEnumerable<CaptureInfo> captureInfoList) {
            ExecutorList executorList = new ExecutorList();

            foreach (CaptureInfo captureInfo in captureInfoList) {
                if (_processingForm.Cancelled) {
                    throw new UserCancelledException();
                }

                string captureFilename = captureInfo.FileName;
                List<int> nExec = FunctionList.GetExecutorListFromU1File(captureFilename);
                foreach (int exec in nExec) {
                    if (executorList.Contains(exec)) {
                        continue;
                    }
                    executorList.Add(exec);
                }
            }
            executorList.Sort();
            return executorList;
        }

        private int ValidateOrRequestExecutor(ExecutorList possibleExecutors, int definedExecutor) {
            if (_processingForm.Cancelled) {
                throw new UserCancelledException();
            }
            
            int validExecutor = definedExecutor;
            if (possibleExecutors.Contains(definedExecutor) == false) {
                if (possibleExecutors.Count == 0) {
                    string message = string.Format(
                        "Input file can't be identified by\nany executor!!\nContinue to decode using E{0:000}?", _definedExec);
                    if (FormFactory.GetInstance().MessageBox(_ownerWindow, message, "No Possible Executor to Decode",
                                                             MessageBoxButtons.YesNo) == DialogResult.No) {
                        throw new UserCancelledException();
                    }
                } else {
                    IDecoderExecConfirmationForm decoderExecConfirmationForm = new DecoderExecConfirmationForm();
                    decoderExecConfirmationForm.Executor = _definedExec;
                    decoderExecConfirmationForm.SetExecList(possibleExecutors);
                    if (((IFormFactoryForm)decoderExecConfirmationForm).ShowDialog((IWin32Window)_processingForm) == DialogResult.Cancel) {
                        throw new UserCancelledException();
                    }
                    validExecutor = decoderExecConfirmationForm.Executor; // switch to different Exec

//                    // update the Registry and Decoder's option setting
//                    gDecoderOpt.executor = _definedExec;
//                    theApp.WriteProfileInt("Settings", "Executor", _definedExec);
                }
            }
            return validExecutor;
        }

        private bool CreateCfiFromCaptureSet() {
            bool rslt = true;

            for (int i = 0; i < _captureSetFiles.Count; i++) {
                if (_processingForm.Cancelled) {
                    throw new UserCancelledException();
                }

                CaptureInfo captureInfo = _captureSetFiles[i];
                if (!CreateCFI(captureInfo.KeyLabel, captureInfo.FileName)) {
                    _lastErrorMessage = string.Format("Cannot Create CFI from {0}\nAbort!!", captureInfo.FileName);
                    rslt = false;
                    break;
                }
            }
            return rslt;
        }

        private bool CreateCFI(string Label, string captureFile) {
            bool rslt = true;
            string name = Path.GetFileNameWithoutExtension(captureFile);
            string filePath = _cfiOutputPathManager.MakeFullPath(_cfiOutputPathManager.GenerateTempFilename(name + ".cfi", "", ""));

            if (_cfiFileList.Contains(filePath)) {
                _lastErrorMessage = "Error occurred processing duplicate capture files";
                return false;
            }
            _cfiFileList.Add(filePath);

            try {
                bool useGenExec = string.IsNullOrEmpty(_genericExecConfigPath) == false;
                if (useGenExec) {
                    FunctionList.ConvertCaptureToCfiFile(filePath, captureFile, _definedExec, _genericExecConfigPath, Label);
                } else {
                    FunctionList.ConvertCaptureToCfiFile(filePath, captureFile, _definedExec, Label);
                }
            } catch (Exception pe) {
                _lastErrorMessage = pe.Message;
                rslt = false;
            }

            return rslt;
        }

        public string GenerateSummaryFile() {
            if (_singleCaptureFile) {
                return _cfiOutputPathManager.GetWorkingDirectory() + Path.GetFileName(_cfiFileList[0]);
            }
            string summaryFilename = _cfiOutputPathManager.GetWorkingDirectory() + Path.GetFileName(_filePath);
            summaryFilename = Path.ChangeExtension(summaryFilename, ".$$$");

            TextWriter summaryFileStream;
            try {
                summaryFileStream = new StreamWriter(summaryFilename);
            } catch {
                _lastErrorMessage = string.Format("Unable to create file: {0}", summaryFilename);
                return null;
            }

            if (_cfiFileList == null || _cfiFileList.Count == 0) {
                summaryFileStream.WriteLine("Errors when making CFI files.");
                summaryFileStream.Close();
                return null;
            }
            summaryFileStream.WriteLine("Filename: {0}", summaryFilename);
            summaryFileStream.WriteLine("Executor Number = {0}", _definedExec);
            summaryFileStream.WriteLine();

            string buf = string.Empty;
            for (int i = 0; i < _cfiFileList.Count; i++) {
                string cp = _cfiFileList[i];
                if (string.IsNullOrEmpty(cp)) {
                    continue;
                }

                TextReader cfiFileStream;
                try {
                    cfiFileStream = new StreamReader(cp);
                } catch {
                    continue;
                }

                string label = string.Empty;
                buf = string.Empty;
                bool firstFrame = true;
                bool foundData = false;
                string temp = cfiFileStream.ReadLine();
                while (temp != null) {
                    if (temp.Length == 0) {
                        temp = cfiFileStream.ReadLine();
                        continue;
                    }
                    if (temp.Contains("Key Label")) {
                        label = string.Empty;
                        string[] keyValue = temp.Split(new char[] {'='}, 2);
                        if (keyValue.Length >= 2) {
                            label = keyValue[1].Trim();
                        }
                    } else if (temp.Contains("Data")) {
                        //check for data
                        string[] keyValue = temp.Split('=');
                        if (keyValue.Length >= 2) {
                            buf += keyValue[1].Trim() + "   ";
                            foundData = true;
                        }
                    } else if (temp.Contains("Code Gap Duration")) {
                        if (buf.Length > 0) {
                            if (firstFrame) {
                                buf += "   " + label;
                                firstFrame = false;
                            }
                            summaryFileStream.WriteLine(buf);
                            buf = string.Empty;
                        }
                    }
                    temp = cfiFileStream.ReadLine();
                }
                cfiFileStream.Close();
                if (foundData == false) {
                    buf += "(Failed to Decode)    " + label;
                    summaryFileStream.Write(buf);
                    buf = string.Empty;
                }
                summaryFileStream.WriteLine();
            }
            summaryFileStream.Close();
            return summaryFilename;
        }

        public class UserCancelledException : Exception {
            public UserCancelledException() { }
            public UserCancelledException(string message) : base(message) { }
        }
    }

    public class ExecutorList : List<int> {
        public ExecutorList() {}
        public ExecutorList(IEnumerable<int> collection) : base(collection) {}
    }
}