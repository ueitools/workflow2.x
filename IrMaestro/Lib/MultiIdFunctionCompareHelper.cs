using System.Collections.Generic;
using CommonCode;

namespace IRMaestro {
    // TODO: MultiIdFunctionCompareHelper (untested)
    public class MultiIdFunctionCompareHelper : IMultiIdFunctionCompareHelper {
        public List<IIdFunctionCompareHelper> _compareHelpers;

        public MultiIdFunctionCompareHelper() {
            _compareHelpers = new List<IIdFunctionCompareHelper>();
        }

        public bool AddId(string id,DBType dbtype) {
            IIdFunctionCompareHelper functionCompareHelper = new IdFunctionCompareHelper();

            try {
                functionCompareHelper.LoadId(id,dbtype);
                functionCompareHelper.SetFilePathManager(new TempFilePath(true));
            } catch {
                return false;
            }

            _compareHelpers.Add(functionCompareHelper);
            
            return true;
        }

        public MultiIdFunctionSearchResults SearchForMatch(params string[] captureFiles) {
            // TODO: MultiSearch for match (untested)
            MultiIdFunctionSearchResults results = new MultiIdFunctionSearchResults();
            foreach (string capturePath in captureFiles) {
                MultiIdFunctionSearchResults.Item resultItem = new MultiIdFunctionSearchResults.Item(capturePath);

                int matchedExec = 0;
                foreach (IIdFunctionCompareHelper functionCompareHelper in _compareHelpers) {
                    bool notAMatchingExec = matchedExec != functionCompareHelper.Exec;
                    bool matchedExecSet = matchedExec != 0;
                    if (notAMatchingExec && matchedExecSet) {
                        continue;
                    }

                    int matchCount = functionCompareHelper.SearchForMatchingFunction(capturePath);
                    if (matchCount <= 0) {
                        continue;
                    }

                    resultItem.AddSearchResult(matchCount, functionCompareHelper.Id,
                                               functionCompareHelper.LastMatchedIdFunctionGroup);
                    matchedExec = functionCompareHelper.Exec;
                }

                results.Add(resultItem);
            }

            return results;
        }

        public void Clear() {
            _compareHelpers = new List<IIdFunctionCompareHelper>();
        }
    }

    public interface IMultiIdFunctionCompareHelper {
        bool AddId(string id, DBType dbtype);
        MultiIdFunctionSearchResults SearchForMatch(params string[] captureFiles);
        void Clear();
    }

    public class MultiIdFunctionSearchResults : List<MultiIdFunctionSearchResults.Item> {
        public class Item {
            private string _cfiPath;
            private List<Results> _searchResults;

            public Item(string cfiPath) {
                _cfiPath = cfiPath;
                _searchResults = new List<Results>();
            }

            public string CfiPath {
                get { return _cfiPath; }
            }

            public List<Results> SearchResults {
                get { return _searchResults; }
            }

            public void AddSearchResult(int matchedCount, string id, MatchedIdFunctionGroup matchedFunctions) {
                _searchResults.Add(new Results(matchedCount, id, matchedFunctions));
            }
        }

        public class Results {
            private int _matchCount;
            private string _matchId;
            private MatchedIdFunctionGroup _matchedFunctions;

            public Results(int matchCount, string matchId, MatchedIdFunctionGroup matchedFunctions) {
                _matchCount = matchCount;
                _matchId = matchId;
                _matchedFunctions = matchedFunctions;
            }

            public int MatchCount {
                get { return _matchCount; }
            }

            public string MatchId {
                get { return _matchId; }
            }

            public MatchedIdFunctionGroup MatchedFunctions {
                get { return _matchedFunctions; }
            }
        }
    }
}
