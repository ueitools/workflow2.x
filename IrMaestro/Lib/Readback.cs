using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using BusinessObject;
using CommonForms;
using FiCompareLib;
using IDCompare;
using ToolsData;
using PickAccessLayer;
//using UEI.Legacy;
using Row = System.Collections.Generic.IDictionary<string, string>;
using WorkflowConfig = CommonForms.WorkflowConfig;

namespace IRMaestro.Lib
{
    public class Readback
    {
        #region PickSource enum
        public enum PickSource
        {
            StaticXML,
            StaticDB,
            Live
        }
        #endregion

        private readonly string _pickPath;
        private readonly string _projectName;
        private readonly bool _rbFiComparision;
        private readonly string _readBackPath;
        private readonly PickSource _staticPickSource;
        private readonly string _workingPath;

        public Readback(string readBackPath, string workingPath, PickSource staticPickSource, string pickPath,
                        string projectName, bool rbFiComparision)
        {
            _rbFiComparision = rbFiComparision;
            _pickPath = pickPath;
            _readBackPath = readBackPath;
            _workingPath = workingPath;

            if (!_pickPath.EndsWith("\\"))
                _pickPath += "\\";

            if (!_readBackPath.EndsWith("\\"))
                _readBackPath += "\\";

            if (!_workingPath.EndsWith("\\"))
                _workingPath += "\\";

            _staticPickSource = staticPickSource;
            _projectName = projectName;

            FunctionList.InitIntronTable();
        }

        public void RunReadback(Id.List idList, out List<CompareRes> results, out bool errorLogged)
        {
            results = new List<CompareRes>();

            //Begin Merge Report
            //Tasks to do
            //
            //  1.  If previous results; Read previous results and load into the results object
            //  2.  Delete results IDs from results object w.r.t idList
            //  3.  Run Readback and fill results object
            //  4.  If show only new run IDs, make a copy of result object
            //
            string strMergeRpt = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                       UserSettings.ConfigKeys.MERGE_REPORT);

            if (_rbFiComparision)
            {
                strMergeRpt = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                          UserSettings.ConfigKeys.MERGE_REPORT);
            }


            if (strMergeRpt == "1")
            {
                LoadPreviousResults(ref results, idList);
            }
            //End

            StreamWriter errorLogSW = new StreamWriter(_workingPath + "error.log");
            errorLogged = false;
            int idCount = 0;
            foreach (string idString in idList)
            {
                //Start Add for RbFiCompare
                if (_rbFiComparision)
                {
                    try
                    {
                        CompareRbAndFi(results, idString);
                        CFIListForm.bShowTimingReport = true;
                    }
                    catch (Exception ex)
                    {
                        errorLogSW.WriteLine(idString + " :   [DB]" + ex.Message + " => " + ex.StackTrace + "\n");
                        errorLogged = true;
                    }
                } //End Add for RbFiCompare
                else
                {
                    CFIListForm.bShowTimingReport = false;
                    try
                    {
                        VerifyReadBack(results, idString);
                    }
                    catch (Exception ex)
                    {
                        errorLogSW.WriteLine(idString + " :  [DB]" + ex.Message + " => " + ex.StackTrace + "\n");
                        errorLogged = true;
                    }
                }
                int iPercent = (100 * (++idCount)) / idList.Count;
                IDForm.ReadBackBGWorker.ReportProgress(iPercent);
                if (IDForm.ReadBackBGWorker.CancellationPending)
                {
                    errorLogSW.Close();
                    return;
                }
            }
            errorLogSW.Close();
        }


        //Function to load Previous Results to merge
        private void LoadPreviousResults(ref List<CompareRes> results,Id.List idList)
        {
            List<string> idTemp = new List<string>();
            foreach (string id in idList)
                idTemp.Add(id);

            if (results != null)
            {
                string strPrevIDs = string.Empty;
                if (!File.Exists(_workingPath + "results.RPT"))
                    return;
                StreamReader sw = new StreamReader(_workingPath + "results.RPT");
                results = new List<CompareRes>();
                string buf;
                while ((buf = sw.ReadLine()) != null)
                {
                    if (buf.Trim() == "")
                        continue;
                    if (buf.Contains("Matched")
                        || buf.Contains("Functions")
                        || buf.Contains("--------------"))
                        continue;
                    string[] strs = buf.Split(new char[] { ' ' }, 6,
                        StringSplitOptions.RemoveEmptyEntries);
                    if (strs.Length < 4)
                        continue;

                    if (idTemp.Contains(strs[1]))
                        continue;

                    CompareRes res = new CompareRes();
                    res.Exec = strs[0];
                    res.ID = strs[1];
                    try
                    {
                        res.matchedFunctions = int.Parse(strs[2]);
                        res.unMatchedFunctions = int.Parse(strs[3]);
                        if (strs.Length == 6)
                            res.SetWarnings(int.Parse(strs[4]), strs[5]);
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                    strPrevIDs += res.ID + ",";
                    results.Add(res);
                }
                sw.Close();

                if (_rbFiComparision)
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                UserSettings.ConfigKeys.MERGE_REPORT_APPEND,
                                                strPrevIDs);
                }
                else
                {
                    UserSettings.SetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                UserSettings.ConfigKeys.MERGE_REPORT_APPEND,
                                                strPrevIDs);
                }
            }
        }
        //



        private void CompareRbAndFi(List<CompareRes> results, string strId)
        {
            try
            {
                PickID pickID = new PickID();
                LoadIdFromPickDB(strId, pickID);
                string rdBackZip = _readBackPath + "RB!" + strId + ".zip";
                CompareRbFi(pickID, rdBackZip, results);
            }
            catch (Exception ex)
            {
            }
        }

        private void CompareRbFi(PickID pickID, string rdBackZip, List<CompareRes> results)
        {
            try
            {
                string id = pickID.OID.Header.ID;
                string pickIDWorkingPath = _workingPath + id + "\\";

                try
                {
                    // clear the pick ID working path
                    if (Directory.Exists(pickIDWorkingPath))
                        Directory.Delete(pickIDWorkingPath, true);
                    Directory.CreateDirectory(pickIDWorkingPath);
                }
                catch
                {
                }

                List<string> txFiles = new List<string>();
                #region TX file generation
                {
                    SQLPickReader.SQLPickReader pickReader = new SQLPickReader.SQLPickReader();
                    pickReader.PickDbName = _projectName;

                    int version = pickReader.GetMaxVersion(id);
                    Row idHeader = pickReader.GetPickID(id, version);
                    IList<Row> pickItemRec = pickReader.GetPickItem(id, version);
                    IList<Row> prefixList = pickReader.GetPrefixItem(id, version);
                    List<int> iPrefixList = pickReader.ConvertPrefixList(prefixList);


                    if (idHeader["IsExternalPrefix"] == "Y")
                    {
                        ProcessExternalPrefix(id, iPrefixList);
                    }
                    foreach (Row data in pickItemRec)
                    {
                        int iFilesCreated;
                        if (string.IsNullOrEmpty(data["DATA"]))
                            continue;

                        if (data["Outron"][0] == 'X' &&
                            Char.IsDigit(data["Outron"][1]) &&
                            Char.IsDigit(data["Outron"][2]))
                            continue;

                        string strFIname = id + "_" + data["Outron"];
                        strFIname = strFIname.Replace("+", "@");
                        strFIname = strFIname.Replace('<', '&');
                        strFIname = strFIname.Replace('>', '!');
                        strFIname = pickIDWorkingPath + strFIname;

                        iFilesCreated = FunctionList.CreateTxFiFile(idHeader, data, iPrefixList, strFIname, true);
                        txFiles.Add(strFIname + ".FI");

                        //for handling toggle files
                        for (int nFicnt = 1; nFicnt < iFilesCreated; nFicnt++)
                        {
                            txFiles.Add(strFIname + "_" + nFicnt + ".FI");
                        }
                    }

                #endregion
                }

                Hashtable rbLabelList = new Hashtable();
                string rbWorkingPath = _workingPath + id + "\\";
                List<string> rxFiles = new List<string>();
                #region RX file generation
                {
                    string unZipedPath = FunctionList.UnzipTNZipFile(rbWorkingPath, rdBackZip);
                    string[] prjFiles = Directory.GetFiles(unZipedPath, "*.prj");
                    Hashtable rbOutronList = null;
                    if (prjFiles.Length > 0)
                    {
                        string[] rbCfiFileList;
                        rbLabelList = FunctionList.GetLabels(prjFiles[0], id, out rbOutronList, out rbCfiFileList);
                    }

                    string[] strSigFiles = Directory.GetFiles(unZipedPath);
                    foreach (string strFile in strSigFiles)
                    {
                        string strFile1 = strFile.ToUpper();
                        if (Path.GetExtension(strFile1) == ".U1" || Path.GetExtension(strFile1) == ".U2")
                        {
                            string strOutron = (string)rbOutronList[Path.GetFileName(strFile)];
                            strOutron = strOutron.Replace("+", "@");
                            strOutron = strOutron.Replace('<', '&');
                            strOutron = strOutron.Replace('>', '!');
                            string strFIpath = rbWorkingPath + id + "_" + strOutron + "#.FI";
                            FunctionList.ConvertCaptureToFI(strFIpath, strFile, pickID.OID.Header.Executor_Code);
                            rxFiles.Add(strFIpath);
                            //Delete the unziped signal. 
                            try
                            {
                                File.Delete(strFile1);
                            }
                            catch (Exception ex1)
                            {
                            }
                        }
                    }
                }
                #endregion

                IToggleFileHandler toggleFileHandler = new FiToggleFileHandler();
                toggleFileHandler.ProcessToggleFiles(pickIDWorkingPath, rbWorkingPath, rxFiles.ToArray());

                #region Report Generation

                int nPassed = 0;
                int nFailed = 0;
                string failedData = "\r\n";
                string passedData = "";

                string strMatchedU1File =
                    "\r\n\r\n------------------------------------------------------------------------------------------\r\nMatched U1 file:\r\n";

                CompareRes res = new CompareRes();
                res.cfiFileList = new List<string>();

                passedData +=
                    "------------------------------------------------------------------------------------------\r\n";

                foreach (string rxFile in rxFiles)
                {
                    List<string> errorMessages = new List<string>();
                    string txFile = string.Format("{0}\\{1}.FI", Path.GetDirectoryName(rxFile),
                                                  Path.GetFileNameWithoutExtension(rxFile).TrimEnd('#'));

                    bool matched = false;

                    string strKey = Path.GetFileName(txFile);
                    if (strKey.Contains("#.FI"))
                    {
                        strKey = strKey.Replace("#.FI", "#.CFI");
                    }
                    else
                    {
                        strKey = Path.GetFileNameWithoutExtension(txFile);
                        for (int iTf = 1; iTf < 10; iTf++)
                        {
                            string strTok = "#_" + iTf;
                            strKey = strKey.Replace(strTok, "#.CFI");
                        }
                    }
                    string strlbl = (string)rbLabelList[strKey + "#.CFI"];
                    string stroutron = string.Empty;
                    strKey = strKey.Replace("#.CFI", "");

                    string strRemId = id + "_";
                    stroutron = strKey.Replace(strRemId, "");

                    stroutron = stroutron.Replace("@", "+");
                    stroutron = stroutron.Replace('&', '<');
                    stroutron = stroutron.Replace('!', '>');

                    if (File.Exists(rxFile) == false)
                        errorMessages.Add("No Readback file found.");
                    else if (File.Exists(txFile) == false)
                        errorMessages.Add("No pick found.");
                    else
                    {
                        FileInfo rxFileInfo = new FileInfo(rxFile);
                        FileInfo txFileInfo = new FileInfo(txFile);
                        if (rxFileInfo.Length > 0 && txFileInfo.Length > 0)
                        {
                            res.cfiFileList.Add(rxFile);

                            FiParser fiTxcomparehelper = new FiParser();
                            fiTxcomparehelper.GetProcessedFiInfoWithFrames(txFile);

                            FiParser fiRxcomparehelper = new FiParser();
                            fiRxcomparehelper.GetProcessedFiInfoWithFrames(rxFile);

                            FiCompareLib.FiCompareLib comparehelper = new FiCompareLib.FiCompareLib();
                            List<string> warningMessages;
                            matched = comparehelper.DoCompare(fiTxcomparehelper, fiRxcomparehelper,
                                                              out warningMessages,
                                                              out errorMessages);
                        }
                        else
                        {
                            errorMessages.Add("No data to compare with.");
                        }
                    }

                    if (matched)
                    {
                        if (errorMessages.Count == 0)
                        {
                            passedData += String.Format("Label: {0, -15} {1, 26} {2}\r\n", strlbl, "Firmware Label: ",
                                                        stroutron);
                        }
                    }

                    if (errorMessages != null && errorMessages.Count > 0)
                    {
                        nFailed++;
                        passedData += String.Format("Label: {0, -15} {1, 26} {2}\r\n", strlbl, "Firmware Label: ", stroutron);

                        foreach (string errorMesage in errorMessages)
                        {
                            passedData += "\t" + errorMesage + "\r\n";
                        }
                    }
                    else
                    {
                        strMatchedU1File += String.Format("\r\n{0, -15}{1, 35}", strlbl, stroutron);
                        passedData += "\r\n";
                        nPassed++;
                    }
                }

                string comparisonFilePath = _workingPath + "RB!" + id;
                comparisonFilePath += "_" + id + ".FBK";
                StreamWriter comparisonFile = File.CreateText(comparisonFilePath);
                comparisonFile.WriteLine("Tracking Number = " + Path.GetFileNameWithoutExtension(rdBackZip));
                comparisonFile.WriteLine(string.Format("Executor Number = {0}", pickID.OID.Header.Executor_Code));
                comparisonFile.WriteLine("Comparing Capture File: " + Path.GetFileNameWithoutExtension(rdBackZip) +
                                         " to ID: " + id);

                //Saving Setting in .FBK file
                int devFreq = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.FREQ_DEV, 10);
                int devPulse = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.PULSE_DEV, 6);
                int devDelay = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.DELAY_DEV, 6);
                int devCodeGap = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.CODEGAP_DEV, 6);
                int devDelayPulse = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.DELAYPULSE_DEV, 6);
                int devDutyCycle = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR, UserSettings.ConfigKeys.DUTYCYCLE_DEV, 60);

                string strTolerance = "\r\nTolerance Settings: " +
                                      "Frequency Dev = " + devFreq.ToString() +
                                      "%\t| Pulse Dev = " + devPulse.ToString() +
                                      "%\t| Delay Dev = " + devDelay.ToString() +
                                      "%\t| Codegap Dev = " + devCodeGap.ToString() +
                                      "%\t| Duty Dev = " + devDutyCycle.ToString() + "%\r\n";
                comparisonFile.WriteLine(strTolerance);

                string pkPrj = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                     UserSettings.ConfigKeys.PICK_DIR);

                string rbDir = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                            UserSettings.ConfigKeys.READBACK_DIR);

                string pkDbType = UserSettings.GetUserConfig(UserSettings.CurrentApp.TIMING_INSPECTOR,
                                                           UserSettings.ConfigKeys.DBSEL);

                
                comparisonFile.WriteLine("Database Type: " + pkDbType);
                comparisonFile.WriteLine("PickDB Path: " + pkPrj);
                comparisonFile.WriteLine("Readback Directory: " + rbDir);
                //End Save

                if (nFailed > 0)
                    failedData =
                        "-------------------------------------------------------------------------------------------\r\n";

                else
                    failedData =
                        "-------------------------------------------------------------------------------------------\r\n\r\n";

                failedData += nFailed + " Functions Unmatched\r\n"
                              + nPassed + " Functions Matched.";

                comparisonFile.Write("\r\n" + passedData);
                comparisonFile.Write(failedData);
                comparisonFile.Write(strMatchedU1File);

                res.Exec = String.Format("Exec{0:000}", pickID.OID.Header.Executor_Code);

                res.ID = id;
                res.matchedFunctions = nPassed;
                res.unMatchedFunctions = nFailed;
                results.Add(res);
                comparisonFile.Close();

                #endregion

                return;
            }
            catch (Exception exp)
            {
                return;
            }
        }

        private void VerifyReadBack(List<CompareRes> results, string id)
        {
            PickID pickID = new PickID();

            switch (_staticPickSource)
            {
                case PickSource.StaticXML:
                    //New
                    LoadPickIDFromXML(id, pickID, _pickPath + id + ".xml");
                    //Old
                    // pickID.LoadFromXML(_pickPath + id + ".xml");
                    break;
                case PickSource.StaticDB:
                    LoadIdFromPickDB(id, pickID);
                    break;
                case PickSource.Live:
                    string currentConnection = DAOFactory.GetDBConnectionString();
                    DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);

                    pickID.NopData = ProductDBFunctions.GetNopData(id);
                    //pickID.OID = ProductDBFunctions.GetID(id);
                    pickID.OID = ProductDBFunctions.GetSimpleID(id);
                    pickID.ProjectName = _projectName;

                    string mode = id.Substring(0, 1);
                    DAOFactory.ResetDBConnection(currentConnection);
                    pickID.DataMapList = ProductDBFunctions.GetDataMapList(_projectName, mode);

                    IPickService pickService = pickID;
                    pickService.Pick();
                    break;
            }

            string rdBackZip = _readBackPath + "RB!" + id + ".zip";

            CompareRdBack(pickID, rdBackZip, results);
        }

        private void LoadIdFromPickDB(string id, PickID pickID)
        {
            SQLPickReader.SQLPickReader pickReader = new SQLPickReader.SQLPickReader();
            pickReader.PickDbName = _projectName;
            int maxVersion = pickReader.GetMaxVersion(id);
            IDictionary<string, string> readerPickId = pickReader.GetPickID(id, maxVersion);

            pickID.OID.Header = new IDHeader();
            pickID.OID.Header.ID = id;

            pickID.OID.Header.Executor_Code = int.Parse(readerPickId[SQLPickReader.PickIdFields.Executor_Code]);
            pickID.OID.Header.IsExternalPrefix = readerPickId[SQLPickReader.PickIdFields.IsExternalPrefix];
            pickID.OID.Header.IsInversedData = readerPickId[SQLPickReader.PickIdFields.IsInversedData];
            string isInversedData = pickID.OID.Header.IsInversedData;

            IList<IDictionary<string, string>> prefixList = pickReader.GetPrefixItem(id, maxVersion);
            pickID.OID.Header.PrefixList = new PrefixCollection();
            foreach (IDictionary<string, string> dictionary in prefixList)
            {
                string data = dictionary[SQLPickReader.PrefixItemFields.DATA];
                if (string.IsNullOrEmpty(data) == false)
                {
                    Prefix prefix = new Prefix();
                    prefix.Data = data;
                    pickID.OID.Header.PrefixList.Add(prefix);
                }
            }

            IList<IDictionary<string, string>> readerPickItem = pickReader.GetPickItem(id, maxVersion);
            foreach (IDictionary<string, string> pickItemData in readerPickItem)
            {
                string outron = pickItemData[SQLPickReader.PickItemFields.Outron];
                if (string.IsNullOrEmpty(outron) == false && outron[0] == 'X' && char.IsDigit(outron[1]) &&
                    char.IsDigit(outron[2]))
                {
                    continue;
                }

                DataMap dataMap = new DataMap();
                string data = pickItemData[SQLPickReader.PickItemFields.DATA];
                dataMap.Data = data;
                dataMap.LabelList = new StringCollection();
                dataMap.LabelList.Add(pickItemData[SQLPickReader.PickItemFields.Label]);
                dataMap.Outron = pickItemData[SQLPickReader.PickItemFields.Outron];
                dataMap.Intron = pickItemData[SQLPickReader.PickItemFields.Intron];
                //                dataMap.IntronPriority = pickItemData[PickReader.PickItemFields.Intron];
                pickID.DataMapList.Add(dataMap);
            }
        }

        private void LoadPickIDFromXML(string id, PickID pickID,string xmlFilePath)
        {
            try
            {
                PickResultIdConverter idConverted = PickResultIdConverter.FromXml(xmlFilePath);

                pickID.OID.Header = new IDHeader();
                pickID.OID.Header.ID = id;

                pickID.OID.Header.Executor_Code = idConverted.PickId.Exec;
                pickID.OID.Header.IsExternalPrefix = idConverted.PickId.IsExternalPrefix.ToString();
                pickID.OID.Header.IsInversedData = idConverted.PickId.IsInverseData.ToString();
                string isInversedData = pickID.OID.Header.IsInversedData;

                foreach (PickAccessLayer.DataAccessObjects.PickPrefixDAO prefixDao in idConverted.PickId.PrefixList)
                {
                    if (!String.IsNullOrEmpty(prefixDao.Data))
                    {
                        Prefix prefix = new Prefix();
                        prefix.Data = prefixDao.Data;
                        pickID.OID.Header.PrefixList.Add(prefix);
                    }
                }

                foreach (PickAccessLayer.DataAccessObjects.PickDataMapDAO dataDao in idConverted.PickId.DataMap)
                {
                    DataMap dataMap = new DataMap();
                    string data = dataDao.Data;
                    dataMap.Data = data;
                    dataMap.LabelList = new StringCollection();
                    dataMap.LabelList.Add(dataDao.IdLabel);
                    dataMap.Outron = dataDao.FirmwareLabel;
                    dataMap.Intron = dataDao.Intron;
                    dataMap.IntronPriority = dataDao.IntronPriority;
                    pickID.DataMapList.Add(dataMap);
                }
            }
            catch (Exception ex)
            {  
            }
        }

        private void CompareRdBack(PickID pickID, string rdBackZip, List<CompareRes> results)
        {
            string id = pickID.OID.Header.ID;
            string pickIDWorkingPath = _workingPath + id + "\\";

            Hashtable pickIntronTable, pickLabelTable, pickIntronPriorityTable, dataTable;
            try
            {
                // clear the pick ID working path
                if (Directory.Exists(pickIDWorkingPath))
                    Directory.Delete(pickIDWorkingPath, true);
                Directory.CreateDirectory(pickIDWorkingPath);
            }
            catch
            {
            }

            List<string> cfiFilesFromPickID = FunctionList.CreateTxCfiFiles(
                pickID, pickIDWorkingPath, CFIInfo.MASK_TOGGLE_BITS == false,
                out pickIntronTable, out pickLabelTable, out pickIntronPriorityTable, out dataTable);

            CompareRes res = new CompareRes();
            res.cfiFileList = new List<string>();

            string rbWorkingPath = _workingPath + id + "\\";
            if (File.Exists(rdBackZip) == false)
            {
                res.ID = id;
                res.Exec = String.Format("Exec{0:000}", pickID.OID.Header.Executor_Code);

                res.AddWarning(CompareRes.WarningType.NoReadback);
                res.matchedFunctions = 0;
                res.unMatchedFunctions = cfiFilesFromPickID.Count;

                results.Add(res);

                string compareFilePath = _workingPath + "RB!" + id;
                compareFilePath += "_" + id + ".RBK";

                StreamWriter compareFile = File.CreateText(compareFilePath);
                compareFile.WriteLine("No Readback file found.");
                compareFile.Close();

                return;
            }
            string unZipedPath;
            string[] prjFiles;
            //Add Warning if .zip file does not contain u1 or U2
            //
            try
            {
                 unZipedPath = FunctionList.UnzipTNZipFile(rbWorkingPath, rdBackZip);
                 prjFiles = Directory.GetFiles(unZipedPath, "*.prj");
            }
            catch {

                res.ID = id;
                res.Exec = String.Format("Exec{0:000}", pickID.OID.Header.Executor_Code);

                res.AddWarning(CompareRes.WarningType.NoReadback);
                res.matchedFunctions = 0;
                res.unMatchedFunctions = cfiFilesFromPickID.Count;

                results.Add(res);

                string compareFilePath = _workingPath + "RB!" + id;
                compareFilePath += "_" + id + ".RBK";

                StreamWriter compareFile = File.CreateText(compareFilePath);
                compareFile.WriteLine(".Prj file not found in Readback .zip");
                compareFile.Close();
                return;
            }
            string[] u1Files = Directory.GetFiles(unZipedPath, "*.u1");
            string[] U2Files = Directory.GetFiles(unZipedPath, "*.U2");
            if ((u1Files.Length == 0) && (U2Files.Length == 0))
            {
                res.ID = id;
                res.Exec = String.Format("Exec{0:000}", pickID.OID.Header.Executor_Code);

                res.AddWarning(CompareRes.WarningType.NoReadback);
                res.matchedFunctions = 0;
                res.unMatchedFunctions = cfiFilesFromPickID.Count;

                results.Add(res);

                string compareFilePath = _workingPath + "RB!" + id;
                compareFilePath += "_" + id + ".RBK";

                StreamWriter compareFile = File.CreateText(compareFilePath);
                compareFile.WriteLine("No .u1 or .U2 in Readback file.");
                compareFile.Close();

                return;
            }
            //

            Hashtable rbOutronList = null;
            string[] rbCfiFileList = null;
            Hashtable rbLabelList = new Hashtable();
            if (prjFiles.Length > 0)
            {
                rbLabelList = FunctionList.GetLabels(prjFiles[0], id, out rbOutronList,
                                                     out rbCfiFileList);
            }

            string[] unZipedFiles = Directory.GetFiles(unZipedPath);
            //Start Add 20120621 Warning for Exec Not Implemented
            int iExecStatus;
            //End Add 20120621
            FunctionList.GetCfiFiles(unZipedFiles, pickID.OID.Header.Executor_Code,
                                     rbWorkingPath,
                                     id, rbLabelList, rbOutronList, out iExecStatus);
            Hashtable rbIntronList = FunctionList.GetIntronList(rbLabelList, id);
            Directory.Delete(unZipedPath, true);

            IToggleFileHandler toggleFileHandler = new CfiToggleFileHandler();
            toggleFileHandler.ProcessToggleFiles(pickIDWorkingPath, rbWorkingPath, rbCfiFileList);

            ICompareID cfiCmp = new ICompareID();
            double passed = 0.0, failed = 0.0, warning = 0.0, intronPassed = 0.0, intronFailed = 0.0;
            string failedData = "";
            string passedData = "";
            foreach (string pickIDCfiFile in cfiFilesFromPickID)
            {
                //CC
                bool bccode = false;
                string strDBcc = string.Empty, strCPcc = string.Empty;
                //
                bool matched = false;
                string intron = null;
                List<string> warningMessages = null;
                List<string> errorMessages;
                string data = (string)dataTable[pickIDCfiFile];
                string rbCfiFile = pickIDCfiFile.Replace(".CFI", "#.CFI");
                string RXData = "", TXData = "";
                bool isNonFunctional = false;
                bool isHiFrequency = false;

                if (File.Exists(rbWorkingPath + rbCfiFile))
                {
                    CFIInfo rxCfiInfo, txCfiInfo;
                    string rbLabel, pickLabel;
                    int doCompResult;

                    res.cfiFileList.Add(rbWorkingPath + rbCfiFile);

                    cfiCmp.DoCompare(rbWorkingPath + rbCfiFile,
                                     pickIDWorkingPath + pickIDCfiFile, out doCompResult,
                                     out rbLabel, out pickLabel,
                                     out RXData, out TXData, out rxCfiInfo, out txCfiInfo,
                                     CFIInfo.MASK_TOGGLE_BITS, -1);

                    //CC                    
                     bccode = HasCustomCode(pickIDWorkingPath + pickIDCfiFile, rbWorkingPath + rbCfiFile, out strDBcc, out strCPcc);
                    //

                    isNonFunctional = IsLabelNonFunctional(rbLabel) || IsLabelNonFunctional(pickLabel);
                    isHiFrequency = txCfiInfo.Frequency >= 0F && txCfiInfo.Frequency < 10F;
                    if (doCompResult > 0)
                    {
                        if (cfiCmp.CompareCFIInfo(rxCfiInfo, txCfiInfo, pickID.OID.Header.Executor_Code,
                                                  out warningMessages, out errorMessages))
                        {
                            matched = true;
                            passed = passed + 1.0;

                            if (rbIntronList.Count > 0)
                            {
                                intron = FunctionList.IntronMatch(rbCfiFile, data, rbIntronList, rbLabelList,
                                                                  pickIntronTable);

                                if (intron != null)
                                    intronPassed = intronPassed + 1.0;
                                else
                                    intronFailed = intronFailed + 1.0;
                            }
                        }
                    }
                    else
                    {
                        errorMessages = new List<string>();
                        errorMessages.Add("Error: Data not matched");
                    }

                    if (warningMessages != null && warningMessages.Count > 0)
                    {
                        res.AddWarning(CompareRes.WarningType.General);
                        errorMessages.InsertRange(0, warningMessages);
                    }

                    if (errorMessages != null && errorMessages.Count > 0)
                    {
                        if (isNonFunctional)
                        {
                            res.AddWarning(CompareRes.WarningType.NonFunctional);
                            errorMessages.Insert(0, "Warning: Compare found errors but function is 'Non-functional'");
                        }

                        if (isHiFrequency && iExecStatus == 1)
                        {
                            res.AddWarning(CompareRes.WarningType.HighSpeedCapture);
                            errorMessages.Insert(0, "Warning: Hi-Frequency signal");
                        }
                        //Start Add 20120621 Warning for Exec Not Implemented
                        if (iExecStatus == 0)
                        {
                            res.AddWarning(CompareRes.WarningType.NoExecutor);
                            errorMessages.Insert(0, "Warning: Executor Not Implemented");                            
                        }
                        //End Add 20120621

                    }
                }
                else
                {
                    errorMessages = new List<string>();
                    errorMessages.Add("Error: No readback file found");
                }


                string rxData = RXData.Replace("FD", "");
                rxData = rxData.Replace("B", "").Trim();
                string txData = TXData.Replace("FD", "");
                txData = txData.Replace("B", "").Trim();

                StringCollection introns = (StringCollection)pickIntronTable[data];
                StringCollection intronPrioritys = (StringCollection)pickIntronPriorityTable[data];
                StringCollection labels = (StringCollection)pickLabelTable[data];

                if (!matched)
                {
                    if (isNonFunctional || isHiFrequency || (warningMessages != null && warningMessages.Count > 0))
                        warning += 1;
                    else
                        failed += 1;
                    intronFailed = intronFailed + 1.0;

                    failedData += "----------------------------------------\r\n\r\n";

                    if (labels != null && labels.Count > 0)
                    {
                        string labelString = labels[0];

                        for (int j = 1; j < labels.Count; j++)
                        {
                            labelString += " | " + labels[j];
                        }

                        failedData += String.Format("{0, -15}{1, -2}{2}              {3, -30}\r\n",
                                                    pickIDCfiFile, "  [DB] : ", txData, labelString);
                    }
                    else
                    {
                        failedData += String.Format("{0, -15}{1, -2}{2}\r\n", pickIDCfiFile, "  [DB] :", txData);
                    }

                    if (rbLabelList[rbCfiFile] != null)
                    {
                        failedData += String.Format("{0, -15}{1, -2}{2}              {3, -30}\r\n\r\n",
                                                    rbCfiFile, " [CP] : ", rxData, rbLabelList[rbCfiFile]);
                    }
                    else
                    {
                        failedData += String.Format("{0, -15}{1, -2}{2}\r\n\r\n", rbCfiFile, " [CP] : ", rxData);
                    }

                    if (errorMessages != null && errorMessages.Count > 0)
                    {
                        foreach (string errorMesage in errorMessages)
                        {
                            failedData += errorMesage + "\r\n";
                        }
                        failedData += "\r\n";
                    }
                }
                else
                {
                    passedData +=
                        "------------------------------------------------------------------------------------------\r\n\r\n";
                    if (introns == null || labels == null)
                        continue;

                    for (int j = 0; j < labels.Count; j++)
                    {
                        passedData += String.Format("{0,-15}", rbCfiFile);
                        passedData += String.Format("{0,-2}", " [CP] : ");
                        passedData += rxData;
                        passedData += "              ";
                        passedData += String.Format("{0,-30}", rbLabelList[rbCfiFile]);
                        if (intron != null)
                        {
                            passedData += intron;
                        }
                        else if (rbLabelList[rbCfiFile] != null)
                        {
                            string label = (string)rbLabelList[rbCfiFile];

                            if (!label.Contains("|"))
                                passedData += (string)rbIntronList[label];
                            else
                            {
                                string[] strs = label.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                                if (strs.Length > 0)
                                {
                                    for (int k = 0; k < strs.Length; k++)
                                    {
                                        string s = (string)rbIntronList[strs[k].Trim()];
                                        if (s != null)
                                        {
                                            passedData += s;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        passedData += "\r\n";

                        passedData += String.Format("{0,-15}", pickIDCfiFile);
                        passedData += String.Format("{0,-2}", "  [DB] : ");
                        passedData += txData;
                        passedData += "              ";
                        passedData += String.Format("{0,-30}", labels[j]);
                        if (j < introns.Count)
                            passedData += introns[j];
                        if (j < intronPrioritys.Count && intronPrioritys[j] != null && intronPrioritys[j].Trim() != "")
                            passedData += "(p" + intronPrioritys[j].Trim() + ")";
                        passedData += "\r\n";
                    }
                    //CC
                    if (bccode)
                    {                        
                        passedData += "\r\n[Capture] : " + strCPcc;
                        passedData += "\r\n     [DB] : " + strDBcc;
                        passedData += "\r\n";
                    }
                    //
                    passedData += "\r\n";

                    if (errorMessages != null)
                    {
                        foreach (string message in errorMessages)
                        {
                            passedData += message;
                            passedData += "\r\n";
                        }
                        passedData += "\r\n";
                    }
                }
            }

            string comparisonFilePath = _workingPath + "RB!" + id;
            comparisonFilePath += "_" + id + ".RBK";

            StreamWriter comparisonFile = File.CreateText(comparisonFilePath);

            if (failed > 0 || warning > 0)
                failedData = "-------------------------------------------------------------------------------------------\r\n\r\n"
                             + "Unmatched CFI files:\r\n\r\n" + failedData;
            else
                failedData =
                    "-------------------------------------------------------------------------------------------\r\n\r\n";

            failedData += string.Format("Total {0} unmatched CFI files.", failed + warning);

            comparisonFile.WriteLine(string.Format("Executor Number = {0}", pickID.OID.Header.Executor_Code));
            comparisonFile.WriteLine(string.Format("Comparing Capture File: {0}RB!{1}.ZIP to pickID in {2}{3}.XML",
                                                   _readBackPath, id, _pickPath, id));

            //Saving Setting in .RBK file
            int devFreq = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.FREQ_DEV, 10);
            int devPulse = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.PULSE_DEV, 6);
            int devDelay = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.DELAY_DEV, 6);
            int devCodeGap = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.CODEGAP_DEV, 6);
            int devDelayPulse = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.DELAYPULSE_DEV, 6);
            int devDutyCycle = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY, UserSettings.ConfigKeys.DUTYCYCLE_DEV, 60);

            string strTolerance = "\r\nTolerance Settings: " +
                                  "Frequency Dev = " + devFreq.ToString() +
                                  "%\t| Pulse Dev = " + devPulse.ToString() +
                                  "%\t| Delay Dev = " + devDelay.ToString() +
                                  "%\t| Codegap Dev = " + devCodeGap.ToString() +
                                  "%\t| Duty Dev = " + devDutyCycle.ToString() + "%\r\n";
            comparisonFile.WriteLine(strTolerance);

            string pkPrj = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                 UserSettings.ConfigKeys.PICK_DIR);

            string rbDir = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                        UserSettings.ConfigKeys.READBACK_DIR);

            string pkDbType = UserSettings.GetUserConfig(UserSettings.CurrentApp.READBACK_VERIFY,
                                                       UserSettings.ConfigKeys.DBSEL);


            comparisonFile.WriteLine("Database Type: " + pkDbType);
            comparisonFile.WriteLine("PickDB Path: " + pkPrj);
            comparisonFile.WriteLine("Readback Directory: " + rbDir);
            //End Save

            comparisonFile.Write("\r\n" + passedData);
            comparisonFile.Write(failedData);
            comparisonFile.Close();

            res.Exec = String.Format("Exec{0:000}", pickID.OID.Header.Executor_Code);

            res.ID = id;
            res.matchedFunctions = (int)passed;
            res.unMatchedFunctions = (int)failed;

            results.Add(res);

            return;
        }

        //CC
        public bool HasCustomCode(string DbFile, string CpFile, out string strDBcc, out string strCPcc)
        {
            bool bRet = false;
            strDBcc = string.Empty;
            strCPcc = string.Empty;
            StreamReader sw = new StreamReader(DbFile);
            string buf;

            while ((buf = sw.ReadLine()) != null)
            {
                if (buf.Trim() == "")
                    continue;
                if (buf.Contains("Custom Code:"))
                {
                    strDBcc = buf;
                    bRet = true;
                    break;
                }
            }
            sw.Close();

            sw = new StreamReader(CpFile);
            buf = string.Empty;
            while ((buf = sw.ReadLine()) != null)
            {
                if (buf.Trim() == "")
                    continue;
                if (buf.Contains("Custom Code:"))
                {
                    strCPcc = buf;
                    bRet = true;
                    break;
                }
            }
            sw.Close();
            return bRet;
        }
        //CC
        

        private bool IsLabelNonFunctional(string label)
        {
            string lowerLabel = label.ToLower();
            return lowerLabel.Contains("non") && lowerLabel.Contains("functional");
        }

        #region External Prefix processing

        private void ProcessExternalPrefix(string ID, List<int> iPrefixList)
        {
            string xpfx = WorkflowConfig.GetCfg("Xprefix");
            iPrefixList.Clear(); // this line might be changed later
            if (scanXprefix(xpfx, ID, iPrefixList) == 0)
            {
                //                MessageBox.Show("Error when scanning external prefix!");
            }
        }

        private int scanXprefix(String dbDir, string id, List<int> prefix)
        {
            int len = dbDir.Length;
            if (dbDir[len - 1] == '\\')
                dbDir.Remove(len, 1);
            if (Int32.Parse(id.Substring(1)) < 1000)
            {
                id = id.Remove(1, 1);
            }

            string file_path = String.Format("{0}\\xprefix\\{1}.exp",
                                             dbDir, id);

            StreamReader xpfile = new StreamReader(file_path);
            string strLine = "";

            if (xpfile == null)
            {
                return 0;
            }

            // go through the file until we find the HC05RC16 section
            do
            {
                strLine = xpfile.ReadLine();
                if ((strLine.Contains(" IF ") || strLine.Contains(" if ")) &&
                    strLine.Contains(" HC05RC16"))
                    break;
            } while (!xpfile.EndOfStream);

            // safe exit when failed to find match in the external prefix file
            if (xpfile.EndOfStream)
            {
                xpfile.Close();
                xpfile = null;
                return 0;
            }

            int ret = xPfxBlock(xpfile, prefix);
            xpfile.Close();
            xpfile = null;
            return ret;
        }

        private int xPfxBlock(StreamReader xpfile, List<int> prefix)
        {
            string strLine = "";
            int num_ext_prefix = 0;
            string str1 = "";
            int ul1 = 0, ul2 = 0; // unsgigned long prefix data byte

            while (!xpfile.EndOfStream)
            {
                ul1 = 0;
                ul2 = 0;
                strLine = xpfile.ReadLine();
                if (strLine.Contains("endif") || strLine.Contains("ENDIF"))
                    break;
                getRC16SectionData(strLine, ref str1, ref ul1, ref ul2);
                if (ul1 > 0)
                {
                    prefix.Add(ul1);
                    num_ext_prefix++;
                }
                if (ul2 > 0)
                {
                    prefix.Add(ul2);
                    num_ext_prefix++;
                }
            } // while
            while (!xpfile.EndOfStream)
            {
                strLine = xpfile.ReadLine();
                if (strLine.Contains("DB"))
                {
                    str1 = getDBlineData(strLine);
                    ul1 = ((str1[0] == '1') ? 1 : 0);
                    for (int cnt = 1; cnt < 8; cnt++)
                    {
                        ul1 = ul1 << 1;
                        ul1 = ul1 + ((str1[cnt] == '1') ? 1 : 0);
                    }
                    prefix.Add(ul1);
                    num_ext_prefix++;
                }
            } // while
            return num_ext_prefix;
        }

        private void getRC16SectionData(string strLine, ref string str1,
                                        ref int ul1, ref int ul2)
        {
            string[] Tokens = strLine.Split(new Char[] { ' ', ',' });
            int index_in_tokens = 0;

            for (;
                index_in_tokens < Tokens.Length;
                index_in_tokens++)
            {
                if (Tokens[index_in_tokens] != "")
                {
                    str1 = Tokens[index_in_tokens].Trim();
                    break;
                }
            }
            for (index_in_tokens++;
                 index_in_tokens < Tokens.Length;
                 index_in_tokens++)
            {
                if (Tokens[index_in_tokens] != "")
                {
                    ul1 = Int32.Parse(Tokens[index_in_tokens].Trim());
                    break;
                }
            }
            if (str1 == "Burst" || str1 == "Freq")
            {
                for (index_in_tokens++;
                     index_in_tokens < Tokens.Length;
                     index_in_tokens++)
                {
                    if (Tokens[index_in_tokens] != "")
                    {
                        ul2 = Int32.Parse(Tokens[index_in_tokens].Trim());
                        break;
                    }
                }
            }
        }

        private string getDBlineData(string strDBline)
        {
            string strData = "";
            string[] Tokens = strDBline.Split(' ');
            int index_in_tokens = 0;

            for (; index_in_tokens < Tokens.Length; index_in_tokens++)
            {
                if (Tokens[index_in_tokens] != "" &&
                    Tokens[index_in_tokens] == "DB")
                {
                    string dump = Tokens[index_in_tokens].Trim();
                    break;
                }
            }

            for (index_in_tokens++;
                 index_in_tokens < Tokens.Length;
                 index_in_tokens++)
            {
                if (Tokens[index_in_tokens] != "")
                {
                    strData = Tokens[index_in_tokens].Trim();
                    break;
                }
            }
            return strData;
        }

        #endregion
    }
}