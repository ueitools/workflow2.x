using System.Collections.Generic;
using System.IO;
using CommonForms;
using IDCompare;

namespace IRMaestro.Lib
{
    class CfiToggleFileHandler : IToggleFileHandler
    {
        public void ProcessToggleFiles(string pickIDWorkingPath, string rbWorkingPath, string[] rbFileList)
        {
            if (rbFileList != null && CFIInfo.MASK_TOGGLE_BITS == false)
            {
                string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(rbFileList[0]);
                fileNameWithoutExtension = fileNameWithoutExtension.Remove(fileNameWithoutExtension.Length - 1);
                int toggleFileCount = GetToggleFileCount(fileNameWithoutExtension, pickIDWorkingPath);

                int fileIndex = 0;
                ICompareID cfiCmp = new ICompareID();
                for (int index = 0; index < toggleFileCount; index++)
                {
                    string pickFile = MakeToggleFileName(fileNameWithoutExtension, index);

                    int compareResults;
                    string rbFile = rbFileList[0];
                    cfiCmp.DoCompare(rbWorkingPath + rbFile, pickIDWorkingPath + pickFile, out compareResults, false);
                    if (compareResults > 0)
                    {
                        fileIndex = index;
                        break;
                    }
                }

                CleanupToggleFiles(rbFileList, pickIDWorkingPath, toggleFileCount, rbWorkingPath, fileIndex);
            }
        }

        private void CleanupToggleFiles(IEnumerable<string> rbCfiFileList, string pickIDWorkingPath, int toggleFileCount,
                                        string rbWorkingPath, int fileIndex)
        {
            string fileNameWithoutExtension;
            foreach (string rbCfiFile in rbCfiFileList)
            {
                fileNameWithoutExtension = Path.GetFileNameWithoutExtension(rbCfiFile);
                fileNameWithoutExtension = fileNameWithoutExtension.Remove(fileNameWithoutExtension.Length - 1);

                if (fileIndex != 0)
                {
                    string sourceFileName = pickIDWorkingPath + MakeToggleFileName(fileNameWithoutExtension, fileIndex);
                    if (File.Exists(sourceFileName))
                    {
                        string destFileName = pickIDWorkingPath + MakeToggleFileName(fileNameWithoutExtension, 0);
                        File.Copy(sourceFileName, destFileName, true);
                    }
                }

                for (int index = 1; index < toggleFileCount; index++)
                {
                    string toggleFile = rbWorkingPath + MakeToggleFileName(fileNameWithoutExtension, index);
                    if (File.Exists(toggleFile))
                        File.Delete(toggleFile);
                }

                if (++fileIndex >= toggleFileCount)
                    fileIndex = 0;
            }
        }

        private string MakeToggleFileName(string fileName, int index)
        {
            string rbFile = string.Format("{0}.cfi", fileName);
            if (index > 0)
                rbFile = string.Format("{0}_{1}.cfi", fileName, index);
            return rbFile;
        }

        private int GetToggleFileCount(string fileNameWithoutExtension, string pickIDWorkingPath)
        {
            List<string> files = new List<string>();
            files.AddRange(Directory.GetFiles(pickIDWorkingPath, fileNameWithoutExtension + "_*.cfi"));
            int toggleFileCount = files.Count + 1;
            return toggleFileCount;
        }
    }
}
