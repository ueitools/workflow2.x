using System.Collections.Generic;
using System.IO;
using FiCompareLib;

namespace IRMaestro.Lib
{
    class FiToggleFileHandler : IToggleFileHandler
    {
        public void ProcessToggleFiles(string pickIDWorkingPath, string rbWorkingPath, string[] rbFileList)
        {
            if (rbFileList != null)
            {
                string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(rbFileList[0]);
                fileNameWithoutExtension = fileNameWithoutExtension.Remove(fileNameWithoutExtension.Length - 1);
                int toggleFileCount = GetToggleFileCount(fileNameWithoutExtension, pickIDWorkingPath);

                int fileIndex = 0;
                for (int index = 0; index < toggleFileCount; index++)
                {
                    string pickFile = MakeToggleFileName(fileNameWithoutExtension, index);

                    FiParser fiTxcomparehelper = new FiParser();
                    fiTxcomparehelper.GetProcessedFiInfoWithFrames(pickIDWorkingPath + pickFile);

                    FiParser fiRxcomparehelper = new FiParser();
                    string rbFile = rbFileList[0];
                    fiRxcomparehelper.GetProcessedFiInfoWithFrames(rbFile);

                    FiCompareLib.FiCompareLib comparehelper = new FiCompareLib.FiCompareLib();

                    List<string> warningMessages;
                    List<string> errorMessages;
                    bool matched = comparehelper.DoCompare(fiTxcomparehelper, fiRxcomparehelper, out warningMessages,
                                                           out errorMessages);
                    if (matched && (errorMessages == null || errorMessages.Count == 0))
                    {
                        fileIndex = index;
                        break;
                    }
                }

                CleanupToggleFiles(rbFileList, pickIDWorkingPath, toggleFileCount, rbWorkingPath, fileIndex);
            }
        }

        private void CleanupToggleFiles(IEnumerable<string> rbCfiFileList, string pickIDWorkingPath, int toggleFileCount,
                                        string rbWorkingPath, int fileIndex)
        {
            string fileNameWithoutExtension;
            foreach (string rbCfiFile in rbCfiFileList)
            {
                fileNameWithoutExtension = Path.GetFileNameWithoutExtension(rbCfiFile);
                fileNameWithoutExtension = fileNameWithoutExtension.Remove(fileNameWithoutExtension.Length - 1);

                if (fileIndex != 0)
                {
                    string sourceFileName = pickIDWorkingPath + MakeToggleFileName(fileNameWithoutExtension, fileIndex);
                    if (File.Exists(sourceFileName))
                    {
                        string destFileName = pickIDWorkingPath + MakeToggleFileName(fileNameWithoutExtension, 0);
                        File.Copy(sourceFileName, destFileName, true);
                    }
                }

                for (int index = 1; index < toggleFileCount; index++)
                {
                    string toggleFile = rbWorkingPath + MakeToggleFileName(fileNameWithoutExtension, index);
                    if (File.Exists(toggleFile))
                        File.Delete(toggleFile);
                }

                if (++fileIndex >= toggleFileCount)
                    fileIndex = 0;
            }
        }

        private string MakeToggleFileName(string fileName, int index)
        {
            string rbFile = string.Format("{0}.fi", fileName);
            if (index > 0)
                rbFile = string.Format("{0}_{1}.fi", fileName, index);
            return rbFile;
        }

        private int GetToggleFileCount(string fileNameWithoutExtension, string pickIDWorkingPath)
        {
            List<string> files = new List<string>();
            files.AddRange(Directory.GetFiles(pickIDWorkingPath, fileNameWithoutExtension + "_*.fi"));
            int toggleFileCount = files.Count + 1;
            return toggleFileCount;
        }
    }
}
