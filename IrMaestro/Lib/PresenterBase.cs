using System.Collections.Generic;

namespace IRMaestro {
    public abstract class PresenterBase {
        protected IView _view;

        public virtual void SetView(IView view) {
            _view = view;
        }
                
    }

    public abstract class MvpDataContainers {
        public class ListData : List<string> {
            public ListData() {}
            public ListData(int capacity) : base(capacity) {}
            public ListData(IEnumerable<string> collection) : base(collection) {}
        }

        public class TableData : List<TableData.RowData> {
            public TableData() {}
            public TableData(int capacity) : base(capacity) {}
            public TableData(IEnumerable<RowData> collection) : base(collection) {}

            public class RowData : List<string> {
                public RowData() {}
                public RowData(int capacity) : base(capacity) {}
                public RowData(IEnumerable<string> collection) : base(collection) {}
            }
        }

        public class HierarchicalData : List<HierarchicalData.Node> {
            public HierarchicalData() {}
            public HierarchicalData(int capacity) : base(capacity) { }
            public HierarchicalData(IEnumerable<Node> collection) : base(collection) { }

            public class Node {
                private string _label;
                private List<Node> _children;

                public Node(string label) {
                    _label = label;
                    _children = new List<Node>();
                }

                public string Label {
                    get { return _label; }
                }

                public List<Node> Children {
                    get { return _children; }
                }
            }
        }
    }
}
