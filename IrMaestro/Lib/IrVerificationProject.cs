using System.Collections.Generic;
using System.IO;

namespace IRMaestro {
//    public class IrVerificationProject : IIrVerificationProject, ITestIrVerificationProject  {
//        private bool _isModified;
//        private List<CaptureInfo> _functionCaptureList;
//        private string _id;
//        private string _lastSavedLocation;
//
//        public IrVerificationProject() {
//            Initialize();
//
//            _lastSavedLocation = string.Empty;
//        }
//
//        private void Initialize() {
//            _id = string.Empty;
//            _isModified = false;
//            _functionCaptureList = new List<CaptureInfo>();
//        }
//
//        public bool IsModified {
//            get { return _isModified; }
//        }
//
//        public string[] FunctionLabelList {
//            get {
//                List<string> labelList = new List<string>();
//                foreach (CaptureInfo captureInfo in _functionCaptureList) {
//                    labelList.Add(captureInfo.KeyLabel);
//                }
//                return labelList.ToArray();
//            }
//        }
//
//        public string LastSavedLocation {
//            get { return _lastSavedLocation; }
//        }
//
//        public int SaveAsAt(string targetDirectory) {
//            if (string.IsNullOrEmpty(targetDirectory)) {
//                return 0;
//            }
//
//            if (targetDirectory.EndsWith("\\") == false) {
//                targetDirectory += "\\";
//            }
//
//            return SaveProject(targetDirectory) ? 1 : 0;
//        }
//
//        public void AddFunctionCapture(string label, string cfiPath) {
//            if (string.IsNullOrEmpty(label) || string.IsNullOrEmpty(cfiPath)) {
//                return;
//            }
//
//            _isModified = true;
//            _functionCaptureList.Add(new CaptureInfo(label, cfiPath));
//        }
//
//        public void LoadId(string id) {
//            Initialize();
//            _id = id;
//        }
//
//        private bool SaveProject(string targetDirectory) {
//            ITempFilePath tempFilePath = new TempFilePath(true);
//
//            // Copy capture files to temporary directory for zipping up using generated filenames (filePathManager's CWD)
//            List<CaptureInfo> captureList = new List<CaptureInfo>();
//            for (int index = 0; index < _functionCaptureList.Count; index++) {
//                CaptureInfo captureInfo = _functionCaptureList[index];
//                string label = captureInfo.KeyLabel;
//                string fileName = tempFilePath.GetNewCaptureName(_id, index);
//                
//                string filePath = tempFilePath.CopyFileFrom(captureInfo.FileName, fileName);
//                captureList.Add(new CaptureInfo(label, filePath));
//            }
//
//            string projectPath = tempFilePath.MakeFullPath(tempFilePath.GenerateTempFilename(_id + ".prj", "L!", ""));
//            if (!CreateProjectFile(projectPath, captureList))
//                return false;
//
//            ProjectInfo projectInfo = new ProjectInfo();
//            projectInfo.FileList = new CaptureInfo[captureList.Count];
//            for (int index = 0; index < captureList.Count; index++) {
//                string label = captureList[index].KeyLabel;
//                string filename = captureList[index].FileName;
//                projectInfo.FileList[index] = new CaptureInfo(label, filename);
//            }
//            string xmlProjectPath = Path.ChangeExtension(projectPath, ".xml");
//            projectInfo.ToXMLFile(xmlProjectPath, projectInfo);
//
//            string savePath = targetDirectory + string.Format("VR!{0}.zip", _id);
//            tempFilePath.ZipTo(savePath);
//            _isModified = false;
//
//            _lastSavedLocation = savePath;
//            return true;
//        }
//
//        private bool CreateProjectFile(string filePath, IList<CaptureInfo> captureInfoList) {
//            List<string> projectFileLines = new List<string>(CreatePrjHeader(_id));
//            projectFileLines.AddRange(GetCaptureList(captureInfoList));
//
//            foreach (string text in projectFileLines) {
//                File.AppendAllText(filePath, text);
//            }
//
//            return true;
//        }
//
//        public IEnumerable<string> CreatePrjHeader(string ID) {
//            IList<string> report = new List<string>();
//
//            report.Add("REMOTE CONTROL CAPTURE ----------------------------------------\r\n\r\n");
//
//            report.Add("Copyright 2007 Universal Electronics, Inc.  All Rights Reserved\r\n\r\n");
//
//            report.Add("Tracking Number: 0000\r\n");
//            report.Add("Primary Number : 0\r\n\r\n");
//
//            report.Add("Id             : " + ID + "\r\n\r\n");
//
//            report.Add("CUSTOMER-------------------------------------------------------\r\n\r\n");
//
//            report.Add("Customer Name  : IrVerification -UEI\r\n\r\n");
//
//            report.Add("CAPTURE LIST---------------------------------------------------\r\n\r\n");
//
//            return report;
//        }
//
//        private IEnumerable<string> GetCaptureList(IList<CaptureInfo> captureInfoList) {
//            List<string> lines = new List<string>();
//
//            for (int index = 0; index < captureInfoList.Count; index++) {
//                string captureLabel = captureInfoList[index].KeyLabel;
//                string capturePath = captureInfoList[index].FileName;
//
//                if (string.IsNullOrEmpty(capturePath)) {
//                    continue;
//                }
//
//                string captureFilename = Path.GetFileName(capturePath);
//                lines.Add(string.Format("{0}   : {1}\r\n", captureFilename, captureLabel));
//            }
//            return lines;
//        }
//
//        List<CaptureInfo> ITestIrVerificationProject.GetFunctionCaptureList() {
//            return _functionCaptureList;
//        }
//    }
//
//    public interface IIrVerificationProject {
//        bool IsModified { get; }
//        string[] FunctionLabelList { get; }
//        string LastSavedLocation { get; }
//        int SaveAsAt(string targetDirectory);
//        void AddFunctionCapture(string label, string cfiPath);
//        void LoadId(string id);
//    }
//
//    public interface ITestIrVerificationProject {
//        List<CaptureInfo> GetFunctionCaptureList();
//    }
}
