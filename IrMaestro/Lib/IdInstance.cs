using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using BusinessObject;
using UEI.Legacy;

namespace IRMaestro {
    public class IdInstance : IIdHelper {
        private ID _id;

        public IdInstance() {
            _id = new ID();
        }

        public IDHeader Header {
            get { return _id.Header; }
        }

        public IDFunctionCollection FunctionList {
            get { return _id.FunctionList; }
            set { _id.FunctionList = value; }
        }

        public DeviceCollection DeviceList {
            get { return _id.DeviceList; }
            set { _id.DeviceList = value; }
        }

        

        public void Load(string id,DBType dbtype)
        {

            if (dbtype == DBType.UEIDB)
            {
                if (id == string.Empty)
                {
                    _id = new ID();
                }
                else
                {
                    //_id = ProductDBFunctions.GetID(id);
                    _id = ProductDBFunctions.GetSimpleID(id);
                }
            }
            else if (dbtype == DBType.PickDB)
            {
                
                if ((DBSettings.selectedId != string.Empty) ||(DBSettings.selectedId!=""))
                {
                    //_id = PickDBObject.id;
                    _id = PALObject.id;                   
                }
            }
        }
    }

    public interface IIdHelper {
        IDHeader Header { get; }
        IDFunctionCollection FunctionList { get; set; }
        DeviceCollection DeviceList { get; set; }
        void Load(string id, DBType dbtype);
    }
}
