//
// Taken from IDVerify
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using CommonForms;

namespace IRMaestro {
    public class FilePathManager : IFilePathManager
    {
        private string _currentWorkingPath;
        private IDictionary<string, string> _keyToFilename;

        public FilePathManager()
        {
            _keyToFilename = new Dictionary<string, string>();
            _currentWorkingPath = string.Empty;
        }

        public FilePathManager(bool createSubDirectory)
        {
            _keyToFilename = new Dictionary<string, string>();
            if (createSubDirectory)
            {
                SetSubDirectory("");
            }
        }

        ~FilePathManager()
        {
            if (string.IsNullOrEmpty(_currentWorkingPath))
            {
                return;
            }

            if (Directory.Exists(_currentWorkingPath))
            {
                if (CanDelete(_currentWorkingPath))
                {
                    try
                    {
                        Directory.Delete(_currentWorkingPath, true);
                    }
                    catch (Exception x)
                    {
                        MessageBox.Show(string.Format("Error removing {0}\n\n{1}", _currentWorkingPath, x.Message));
                    }
                }
                _currentWorkingPath = string.Empty;
            }
        }

        public string CurrentWorkingPath
        {
            get
            {
                if (string.IsNullOrEmpty(_currentWorkingPath))
                {
                    SetSubDirectory(null);
                }
                return _currentWorkingPath;
            }
        }

        public void CopyFileCache(string sourcePath, string destinationFilename)
        {
            string destinationPath = CurrentWorkingPath + destinationFilename;
            CopyFile(sourcePath, destinationPath);
            _keyToFilename.Add(destinationFilename, destinationPath);
        }

        public void DeleteFile(string file)
        {
            if (File.Exists(file))
            {
                File.Delete(file);
            }
        }

        public void CopyCacheToWorking()
        {
            foreach (string fileName in _keyToFilename.Values)
            {
                File.Copy(fileName, GetWorkingDirectory() + Path.GetFileName(fileName), true);
            }
        }

        public void SetSubDirectory(string subDirectory)
        {
            if (string.IsNullOrEmpty(_currentWorkingPath))
            {
                string tempFilepath = GetWorkingDirectory();
                if (subDirectory != null)
                {
                    if (subDirectory == string.Empty)
                    {
                        tempFilepath += Path.GetRandomFileName() + "\\";
                    }
                    else
                    {
                        tempFilepath += subDirectory + "\\";
                    }
                }
                _currentWorkingPath = tempFilepath;
            }

            if (CanDelete(_currentWorkingPath))
            {
                Directory.Delete(_currentWorkingPath, true);
            }
            Directory.CreateDirectory(_currentWorkingPath);
        }

        private bool CanDelete(string cdw)
        {
            return Directory.Exists(cdw) && cdw != GetWorkingDirectory();
        }

        public string GenerateTempFilename(string filename, string prefix, string suffix)
        {
            if (string.IsNullOrEmpty(filename))
            {
                filename = Path.GetFileName(Path.GetTempFileName());
            }
            string baseFilename = Path.GetFileName(filename);
            string baseExtension = Path.GetExtension(filename);
            string tempFilename = string.Format("{0}{1}{2}{3}", prefix, Path.GetFileNameWithoutExtension(baseFilename), suffix,
                                                (baseExtension != string.Empty) ? baseExtension : ".tmp");

            tempFilename = EnsureValidFilename(tempFilename);

            return CurrentWorkingPath + tempFilename;
        }

        public string GetNewCaptureName(int tn, int capNum)
        {
            string filename;
            if (tn > 9999)
            {
                filename = string.Format("!{0:D5}{1:X2}.U1", tn, capNum);
            }
            else
            {
                filename = string.Format("!{0:D4}{1:D3}.U1", tn, capNum);
            }
            return filename;
        }

        public string GetNewCaptureName(string id, int capNum)
        {
            return string.Format("!{0}{1:X2}.U1", id, capNum);
        }

        public string GetNewCapturePath(string id, int capNum)
        {
            return CurrentWorkingPath + GetNewCaptureName(id, capNum);
        }

        public string GenerateTempFilenameCached(string filename, string prefix, string suffix, string cacheKey)
        {
            string tempFilename = GenerateTempFilename(filename, prefix, suffix);
            CacheFilepath(cacheKey, tempFilename);

            return tempFilename;
        }

        private void ClearCachedFiles()
        {
            foreach (KeyValuePair<string, string> entry in _keyToFilename)
            {
                string filename = entry.Value;
                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }
            }
            _keyToFilename.Clear();
        }

        private void CacheFilepath(string cacheKey, string filename)
        {
            _keyToFilename[cacheKey] = filename;
        }

        public string GetCachedFilepath(string cacheKey)
        {
            string filepath = string.Empty;
            if (_keyToFilename.ContainsKey(cacheKey))
            {
                filepath = _keyToFilename[cacheKey];
            }
            return filepath;
        }

        public void CopyFile(string sourcePath, string destinationPath)
        {
            File.Copy(sourcePath, destinationPath);
            //_keyToFilename.Add(cacheKey, destinationPath);
        }

        public void ZipTo(string targetPath)
        {
            ZipHelper.CreateZipFromDirectory(targetPath, CurrentWorkingPath);
        }

        private string EnsureValidFilename(string tempFilename)
        {
            foreach (char invalidFileNameChar in Path.GetInvalidFileNameChars())
            {
                tempFilename = tempFilename.Replace(invalidFileNameChar, '_');
            }
            return tempFilename;
        }

        public bool ClearFiles()
        {
            if (string.IsNullOrEmpty(_currentWorkingPath))
            {
                return true;
            }

            if (Directory.Exists(_currentWorkingPath) == false)
            {
                return true;
            }

            ClearCachedFiles();
            string[] files = Directory.GetFiles(_currentWorkingPath);
            foreach (string file in files)
            {
                File.Delete(file);
            }

            return true;
        }

        public string GetWorkingDirectory()
        {
            string workpath = Configuration.GetWorkingDirectory();
            if (!string.IsNullOrEmpty(workpath) && !workpath.EndsWith("\\"))
            {
                workpath += "\\";
            }

            return workpath;
        }

        public bool Unzip(string zipPath)
        {
            if (string.IsNullOrEmpty(zipPath))
            {
                return false;
            }

            SetSubDirectory(Path.GetFileNameWithoutExtension(zipPath));
            ZipHelper.ExtractAll(zipPath, CurrentWorkingPath);

            return true;
        }

        public string[] GetFiles(string searchPattern)
        {
            return Directory.GetFiles(CurrentWorkingPath, searchPattern);
        }
    }

    public interface IFilePathManager
    {
        void SetSubDirectory(string subDirectory);
        string GenerateTempFilename(string filename, string prefix, string suffix);
        bool ClearFiles();
        string GetNewCaptureName(int tn, int capNum);
        string GetNewCaptureName(string id, int capNum);
        string GetNewCapturePath(string id, int capNum);
        string GenerateTempFilenameCached(string filename, string prefix, string suffix, string cacheKey);
        string GetCachedFilepath(string cacheKey);
        void CopyFile(string sourcePath, string destinationPath);
        void ZipTo(string targetPath);
        bool Unzip(string zipPath);
        string[] GetFiles(string searchPattern);
        string GetWorkingDirectory();
        string CurrentWorkingPath { get; }
        void CopyFileCache(string sourcePath, string destinationFilename);
        void DeleteFile(string file);
        void CopyCacheToWorking();
    }
}
