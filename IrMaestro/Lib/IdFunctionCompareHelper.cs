using System;
using System.Text;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using BusinessObject;
using CommonCode;
using IDCompare;
namespace IRMaestro
{
    public class IdFunctionCompareHelper : IIdFunctionCompareHelper, ITestFunctionCompareHelper
    {
        public const int NO_TN = -1;
        private IIdHelper _idHelper;
        private IICompareID _compareIdHelper;
        private ITempFilePath _tempFilePath;
        private MatchedIdFunctionGroup _lastMatchedIdFunctionGroup;
        private List<int> _prefixes;
        private HashtableCollection _idData;
        private Dictionary<IDFunction, string> _functionToCfi;
        private bool bIsIDCompare = false;
        public IdFunctionCompareHelper()
        {
            _idHelper = new IdInstance();
            _compareIdHelper = new ICompareID();
            _tempFilePath = new TempFilePath();
            _lastMatchedIdFunctionGroup = null;
            _prefixes = null;
            _idData = null;
            _functionToCfi = new Dictionary<IDFunction, string>();
        }
        public string Id
        {
            get
            {
                string id = string.Empty;
                if (GetIdHeader() != null)
                {
                    id = GetIdHeader().ID;
                }
                return id;
            }
        }
        public int Exec
        {
            get { return GetIdHeader().Executor_Code; }
        }
        public MatchedIdFunctionGroup LastMatchedIdFunctionGroup
        {
            get { return _lastMatchedIdFunctionGroup; }
            //Added to handle the pickdb support
            set { _lastMatchedIdFunctionGroup = value; }
        }
        public void LoadId(string id, DBType dbtype)
        {
            _tempFilePath.ClearFiles();


            _idHelper.Load(id, dbtype);

            InitializeIdData();

        }
        private void InitializeIdData()
        {
            _prefixes = null;
            _idData = null;
        }
        private IDHeader GetIdHeader()
        {
            return _idHelper.Header;
        }
        private List<int> GetPrefixes()
        {

            if (_prefixes == null)
            {
                //                _prefixes = new List<int>();
                //                PrefixCollection prefixList = GetIdHeader().PrefixList;
                //                foreach (Prefix prefix in prefixList) {
                //                    _prefixes.Add(Convert.ToInt32(prefix.Data, 2));                    
                //                }

                HashtableCollection idHeaderData = CommonForms.FunctionList.GetDBdata(Id, "AdHoc.GetIDHeader");
                _prefixes = CommonForms.FunctionList.CollectPrefixes(Id, idHeaderData);
            }
            return _prefixes;
        }
        private HashtableCollection GetIdData()
        {
            if (_idData == null)
            {
                _idData = CommonForms.FunctionList.GetDBdata(Id, "AdHoc.GetIDData");
            }
            return _idData;
        }
        public bool IsReady()
        {
            return Id != string.Empty;
        }
        public IDFunctionCollection FunctionList
        {
            get
            {
                if (_idHelper.FunctionList == null)
                {
                    return new IDFunctionCollection();
                }

                IDFunctionCollection idFunctionCollection = new IDFunctionCollection();
                foreach (IDFunction idFunction in _idHelper.FunctionList)
                {
                    idFunctionCollection.Add(idFunction);
                }
                return idFunctionCollection;
            }
        }
        public void SetFilePathManager(ITempFilePath filePathManager)
        {
            _tempFilePath = filePathManager;
        }
        private void CreateCfiFromFunctions(IEnumerable<IDFunction> functionCollection)
        {
            foreach (IDFunction availableFunction in functionCollection)
            {
                if (_functionToCfi.ContainsKey(availableFunction) == false)
                {
                    CreateCfiFile(availableFunction, Id);
                }
            }
        }
        private string CreateCfiFile(IDFunction function, string functionId)
        {
            string suffix = "_" + function.Intron;
            string outputFilepath = _tempFilePath.MakeFullPath(_tempFilePath.GenerateTempFilename("", functionId, suffix));
            _functionToCfi.Add(function, outputFilepath);
            Hashtable idData = null;
            //Start Modify
            //Old code
            //if (bIsIDCompare == true)
            //{
            //    idData = GetFunctionDataForIntron(function.Intron, function.Data);
            //}
            //else
            //{
            //    idData = GetFunctionDataForIntron(function.Intron);
            //}
            //if (idData == null)
            //{
            //    idData = GetFunctionDataForIntron(function.Intron);
            //}
            //New Code
            idData = new Hashtable();
            idData["Label"] = function.Label;
            idData["Data"] = function.Data;
            idData["ID"] = functionId;
            idData["Intron"] = function.Intron;
            //End Modify        
            CommonForms.FunctionList.CreateTxCfiFile(GetIdHeader(), GetPrefixes(), idData, outputFilepath);
            return outputFilepath;
        }
        public int SearchForMatchingFunction(string capturePath)
        {
            string cfiFilename = Path.GetFileNameWithoutExtension(capturePath) + ".cfi";
            string captureDirectory = capturePath.Substring(0, capturePath.LastIndexOf(Path.DirectorySeparatorChar) + 1);
            string cfiPath = captureDirectory + cfiFilename;
            CommonForms.FunctionList.ConvertCaptureToCfiFile(cfiPath, capturePath, Exec);
            CreateCfiFromFunctions(FunctionList);

            int matchCount = 0;
            MatchedIdFunctionGroup matchedFunctionGroup = null;
            foreach (IDFunction availableFunction in FunctionList)
            {
                if (_functionToCfi.ContainsKey(availableFunction) == false)
                {
                    CreateCfiFile(availableFunction, Id);
                }
                string outputFilename = _functionToCfi[availableFunction];

                int compareResults;
                _compareIdHelper.DoCompare(cfiPath, outputFilename, out compareResults);
                if (compareResults == 1)
                {
                    if (matchedFunctionGroup == null)
                    {
                        matchedFunctionGroup = new MatchedIdFunctionGroup();
                    }
                    matchedFunctionGroup.AddFunction(availableFunction);
                    matchCount++;
                }
            }
            if (matchedFunctionGroup != null)
            {
                _lastMatchedIdFunctionGroup = matchedFunctionGroup;
            }
            return matchCount;
        }

        //Added to support ID Compare functionality
         public string[] IDCSearchForMatchingFunction(string strCapturePath, string strID, Hashtable labelList, string strDBName)
         {
             int dPassed = 0, dFailed = 0;
             string strCfiFilename = Path.GetFileNameWithoutExtension(strCapturePath) + "_" + strID + ".cfi";
             string captureDirectory = strCapturePath.Substring(0, strCapturePath.LastIndexOf(Path.DirectorySeparatorChar) + 1);
             string strCfiPath = "";
             string strProjName = "Project Name: " + IDCompareForm.IDCmpSettings.strProjName + "\r\n";
             string strCfiDir = captureDirectory.Replace("\\UZSignal", "");
             if (!Directory.Exists(strCfiDir))
             {
                 Directory.CreateDirectory(strCfiDir);
             }
             strCfiPath = strCfiDir + strCfiFilename;
             CommonForms.FunctionList.ConvertCaptureToCfiFile(strCfiPath, strCapturePath, Exec);
             bIsIDCompare = true;
             CreateCfiFromFunctions(FunctionList);
             bIsIDCompare = false;
             ICompareID cfiCmp = new ICompareID();
             string strFailedData = "";
             string strPassedData = "";
             string strExecnum = "";
             string rbLabel, pickLabel;
             strPassedData += "--------------------------"
              + "----------------------------------"
               + "------------------------------\r\n\r\n";

             bool bCfiMatched = true;

             List<string> errorMessages = null;
             foreach (IDFunction availableFunction in FunctionList)
             {
                 bool bFound = false;
                 List<string> warningMessages = null;
                 string strRXData = "", strTXData = "";
                 bool bIsNonFunctional = false;
                 bool bIsHiFrequency = false;
                 string strOutputFilename = _functionToCfi[availableFunction];
                 int nCompareResults;
                 CommonForms.CFIInfo RXCFIInfo = null, TXCFIInfo = null;
                 cfiCmp.DoCompare(strCfiPath, strOutputFilename, out nCompareResults,
                                      out rbLabel, out pickLabel,
                                      out strRXData, out strTXData, out RXCFIInfo, out TXCFIInfo,
                                      CommonForms.CFIInfo.MASK_TOGGLE_BITS, -1);
                 if (strRXData == null)
                 {
                     cfiCmp.DoCompare(strCfiPath, out nCompareResults,
                         out strRXData, strTXData, out RXCFIInfo);
                 }
                 else if (strTXData == null)
                 {
                     cfiCmp.DoCompare(strCfiPath, out nCompareResults,
                       strRXData, out strTXData, out TXCFIInfo);
                 }
                 else
                 {
                     cfiCmp.DoCompare(out nCompareResults, strRXData, strTXData);
                 }
                 bIsHiFrequency = TXCFIInfo.Frequency > 0F && TXCFIInfo.Frequency < 10F;
                 string rxData = strRXData.Replace("FD", "");
                 rxData = rxData.Replace("B", "").Trim();
                 string txData = strTXData.Replace("FD", "");
                 txData = txData.Replace("B", "").Trim();
                 string fileName = Path.GetFileName(strCapturePath);
                 strExecnum = "Executor Number = " + Exec.ToString() + "\r\n";
                 string strDbCfiname = Path.GetFileNameWithoutExtension(strOutputFilename);
                 strDbCfiname = strDbCfiname.Replace("tmp", "_");
                 if (nCompareResults > 0)
                 {
                     dPassed = dPassed + 1;
                     bFound = true;
                 }
                 if (!bFound)
                 {
                     dFailed = dFailed + 1;
                     string strU1File = Path.GetFileNameWithoutExtension(strCapturePath);
                     strU1File += ".cfi";
                     strFailedData = String.Format("{0, -21}{1}       {2}\r\n\r\n",
                         strU1File, rxData, (string)labelList[fileName]);

                 }
                 else
                 {
                     strPassedData += String.Format("{0,-15}",
                         fileName);
                     strPassedData += String.Format("{0,-2}", ":");
                     strPassedData += rxData;
                     strPassedData += "              ";
                     strPassedData += String.Format("{0,-30}",
                         (string)labelList[fileName]);

                     if (labelList[fileName] != null)
                     {
                         string label = (string)labelList[fileName];
                         if (!label.Contains("|"))
                             strPassedData += CommonForms.FunctionList.GetIntronData(
                                strID.Substring(0, 1), label);
                         else
                         {
                             string[] strs = label.Split(new string[] { "|" },
                                 StringSplitOptions.RemoveEmptyEntries);
                             if (strs != null && strs.Length > 0)
                             {
                                 for (int k = 0; k < strs.Length; k++)
                                 {
                                     string s = CommonForms.FunctionList.GetIntronData(
                                         strID.Substring(0, 1), strs[k].Trim());
                                     if (s != null)
                                     {
                                         strPassedData += s;
                                         break;
                                     }
                                 }
                             }
                         }
                     }
                     strPassedData += "\r\n";
                     strPassedData += String.Format("{0,-15}", strDBName);
                     strPassedData += String.Format("{0,-2}", ":");
                     strPassedData += txData;
                     strPassedData += "              ";
                     strPassedData += String.Format("{0,-30}", availableFunction.Label);
                     strPassedData += availableFunction.Intron;
                     if (availableFunction.IntronPriority != null)
                         if (availableFunction.IntronPriority != "") //add intron priority if available.
                             strPassedData += "(p" + availableFunction.IntronPriority + ")";
                     strPassedData += "\r\n\r\n";
                     if (!cfiCmp.CompareCFIInfo(RXCFIInfo, TXCFIInfo, out errorMessages))
                         bCfiMatched = false;
                 }
                 if (warningMessages != null && warningMessages.Count > 0)
                 {
                     errorMessages.InsertRange(0, warningMessages);
                 }
                 if (errorMessages != null && errorMessages.Count > 0)
                 {
                     if (bIsNonFunctional)
                     {
                         errorMessages.Insert(0, "Warning: Compare found errors but function is 'Non-functional'");
                     }
                     if (bIsHiFrequency)
                     {
                         errorMessages.Insert(0, "Warning: Hi-Frequency signal");
                     }
                 }
             }
             if (errorMessages != null && errorMessages.Count > 0 && !bCfiMatched)
             {
                 foreach (string message in errorMessages)
                 {
                     strPassedData += message;
                     strPassedData += "\r\n";
                 }
                 strPassedData += "\r\n";
             }
             string[] strRowResultArr = new string[3];
             strRowResultArr[0] = strPassedData;
             strRowResultArr[1] = strFailedData;
             strRowResultArr[2] = dPassed.ToString();
             return strRowResultArr;
         }

        private string GetComplement(string data)
        {
            string s = "";
            for (int i = 0; i < data.Length; i++)
                if (data[i] == '0')
                    s = s + '1';
                else if (data[i] == '1')
                    s = s + '0';
            return s;
        }

        private Hashtable GetFunctionDataForIntron(string intron)
        {
            Hashtable idData = null;
            foreach (Hashtable data in GetIdData())
            {
                string idDataIntron = (string)data["Intron"];
                if (idDataIntron == intron)
                {   
                    idData = data;                    
                }
            }
            return idData;
        }

        private Hashtable GetFunctionDataForIntron(string intron, string strFunData)
        {
            Hashtable idData = null;
            foreach (Hashtable data in GetIdData())
            {
                string idDataIntron = (string)data["Intron"];
                if (idDataIntron == intron && (string)data["Data"] == strFunData)
                {
                    idData = data;
                }
            }
            return idData;
        }


        void ITestFunctionCompareHelper.SetIdHelper(IIdHelper idHelper)
        {
            _idHelper = idHelper;
        }
        void ITestFunctionCompareHelper.SetAvailableFunctionList(IDFunctionCollection functionCollection)
        {
            //            _availableFunctionList.AddRange(functionCollection);
        }
        void ITestFunctionCompareHelper.SetICompareID(IICompareID iCompareID)
        {
            _compareIdHelper = iCompareID;
        }
    }
    public class MatchedIdFunctionGroup
    {
        private List<IDFunction> _matchedFunctions;

        public MatchedIdFunctionGroup()
        {
            _matchedFunctions = new List<IDFunction>();
        }

        public string Label
        {
            get
            {
                string label = string.Empty;
                foreach (IDFunction idFunction in _matchedFunctions)
                {
                    label += idFunction.Label + " - ";
                }
                if (label.Trim() == "- -")
                    return "-";

                return label.Trim(' ', '-');
            }
        }

        public void AddFunction(IDFunction function)
        {
            _matchedFunctions.Add(function);
        }
    }
    public interface IIdFunctionCompareHelper
    {
        string Id { get; }
        int Exec { get; }
        void LoadId(string id, DBType dbtype);
        bool IsReady();
        IDFunctionCollection FunctionList { get; }
        void SetFilePathManager(ITempFilePath filePathManager);
        int SearchForMatchingFunction(string capturePath);
        //Modified to support pickdb
        MatchedIdFunctionGroup LastMatchedIdFunctionGroup { get; set;}
        //Start Add For IDCompareApp
        string[] IDCSearchForMatchingFunction(string capturePath, string ID, Hashtable labelList, string strFileExt);
        //End Add For IDCompareApp
    }
    public interface ITestFunctionCompareHelper
    {
        void SetIdHelper(IIdHelper idHelper);
        void SetAvailableFunctionList(IDFunctionCollection functionCollection);
        void SetICompareID(IICompareID iCompareID);
        void SetFilePathManager(ITempFilePath filePathManager);
    }
}
