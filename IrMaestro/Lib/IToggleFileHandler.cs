namespace IRMaestro.Lib
{
    interface IToggleFileHandler
    {
        void ProcessToggleFiles(string pickIDWorkingPath, string rbWorkingPath, string[] rbFileList);
    }
}
