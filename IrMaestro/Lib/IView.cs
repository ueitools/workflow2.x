using System;
using System.Drawing;
using System.Windows.Forms;

namespace IRMaestro {
    public interface IView {
        event EventHandler Closed;
        event FormClosingEventHandler FormClosing;
        
        Point Location { get; }
        Size Size { get; }
    }
}
