using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using CommonForms;
using BusinessObject;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.GZip;

namespace IRMaestro
{
    public class IDCompareSearchFunctions
    {
        const int QUERY_IDLIST_SIZE = 20;
        public string WORKDIR = "";
        public bool canceledByUser;
        public StringCollection IDListForDataQuery = new StringCollection();
        public HashtableCollection tempIDData;
        public string TNFiles;
        public string U1Label;
        public string Executor;
        public string GenericCfgPath;
        public string Brands;
        public bool KeepAllCFIFiles;
        public IDCompareSearchFunctions()
        {

        }

        public void SetCfiTollerance(int devFreq, int devPulse, int devDelay,
                                    int devCodeGap, int devDelayPulse)
        {
            CFIInfo.SET_TOLERANCE(devFreq, devPulse, devDelay,
                devCodeGap, devDelayPulse);
        }

        public List<CompareResults> IDCmpDBSearch(bool singleU1,
                                            string inputFile,
                                            string cmpID, ProcessingForm processingForm, string strDbType)
        {
            FunctionList.InitIntronTable();
            List<CompareResults> results = new List<CompareResults>();
            StringBuilder logFileContents = new StringBuilder();
            DateTime startTime = DateTime.Now;
            DateTime endTime;
            logFileContents.AppendFormat("Start time for ID:  {0}\r\n\r\n", startTime);
            string[] captureFiles;
            string tempFileDir;
            string capturePath;
            // Unzip TN file in to their individual U1 capture files
            logFileContents.AppendFormat("Unzip file: {0}\r\n", inputFile);
            capturePath = unzipTNZipFile(inputFile);
            logFileContents.AppendFormat("File: {0} successfully\r\n\r\n", inputFile);
            tempFileDir = capturePath.Substring(0, capturePath.LastIndexOf('\\'));
            captureFiles = Directory.GetFiles(capturePath, "*.*", SearchOption.AllDirectories);
            // If there are no files to process, then log that we're complete and end the search process
            if (captureFiles == null || captureFiles.Length == 0)
            {
                endTime = DateTime.Now;
                logFileContents.AppendFormat("Finish Comp. at:  {0}\r\n", endTime);
                logFileContents.AppendFormat("Total time used for Comp.: {0}\r\n",
                    FunctionList.GetTimeDifference(startTime, endTime));
                File.WriteAllText(WORKDIR + Path.GetFileName(inputFile).Replace(".ZIP", ".LOG"), logFileContents.ToString());
                RemoveTempFiles(tempFileDir);
                return results;
            }
            Hashtable labelList = new Hashtable();
            // Get capture labels from the PRJ file from the TN archive
            for (int i = 0; i < captureFiles.Length; i++)
            {
                if (captureFiles[i].Contains(".prj") || captureFiles[i].Contains(".PRJ"))
                {
                    labelList = FunctionList.GetLabels(captureFiles[i]);
                }
            }
            string logMessage = "Seaching in database:\r\n\r\n";
            // Get the list of the possible Execs we can use for this search
            List<int> execList = null;
            if (Executor != null)
            {
                execList = new List<int>();
                int parsedExec;
                if (int.TryParse(Executor.Trim(), out parsedExec))
                {
                    execList.Add(parsedExec);
                }
            }
            if (execList == null || execList.Count == 0)
            {
                execList = FunctionList.GetExecutorList(capturePath, singleU1, ref logMessage, labelList, processingForm);
            }
            logMessage += "Possible executors identified by U1 files:";
            if (execList.Count == 0)
                logMessage += "\r\n      No possible executor found!!";
            else
            {
                for (int i = 0; i < execList.Count; i++)
                {
                    if (i % 10 == 0)
                    {
                        logMessage += "\r\n  ";
                    }
                    logMessage += String.Format("    {0,-3}", execList[i]);
                }
            }
            logMessage += "\r\n\r\nExecutor/Id List:";
            string skipedFiles = "";
            StringCollection captureCfiFiles = null;
            if (execList.Contains(90))
                captureCfiFiles = FunctionList.GetCfiFiles(captureFiles, 90, labelList, ref logMessage, out skipedFiles);
            StringCollection prefixExcludedIDList = new StringCollection();
            StringCollection tmpIDList = new StringCollection();
            Hashtable ExecIDTable = new Hashtable();
            IDHeader IDHeaderData = null;
            string QAComplogMessage = string.Empty;
            string QAComplogMessage_India = string.Empty;
            string UEIComplogMessage = string.Empty;
            string ENDlogMessage = string.Empty;
            endTime = DateTime.Now;
            ENDlogMessage += String.Format("\r\n\r\n\r\nFinish ID at:  {0}\r\n", endTime);
            ENDlogMessage += String.Format("Total time used for ID: {0}\r\n\r\n", FunctionList.GetTimeDifference(startTime, endTime));
            startTime = DateTime.Now;
            ENDlogMessage += String.Format("Start time for Comp.:  {0}", startTime);
            int prevExecCode = -1;
            Hashtable RXDataTable = new Hashtable();
            Hashtable RXCFIInfoTable = new Hashtable();
            HashtableCollection IDheader = null;
            bool matched;
            HashtableCollection IDdata = null;
            int iter = 0;
            IDdata = FunctionList.GetGroupDBdata(IDListForDataQuery, "AdHoc.GetGroupIDData"); ;
            string tempFileName = Path.GetTempFileName();
            IDListForDataQuery.Clear();
            IDCompareHelper comp = new IDCompareHelper();
            comp.WORKDIR = WORKDIR;
            comp.KeepAllCFIFiles = KeepAllCFIFiles;
            IDheader = FunctionList.GetDBdata(cmpID, "AdHoc.GetIDHeader"); // retireve order is important
            int Executor_Code = 0;
            if (!string.IsNullOrEmpty(GenericCfgPath))
            {
                Executor_Code = ExecLib.ExecsInC.GENERIC_EXEC_NUM;
            }
            else if (IDheader.Count == 0)
            {
                IDHeaderData = DAOFactory.IDHeader().SelectHeaderOnly(cmpID);
                Executor_Code = IDHeaderData.Executor_Code;
            }
            else
            {
                IDHeaderData = null;
                Executor_Code = (int)IDheader[0]["Executor_Code"];
            }

            if (Executor_Code != prevExecCode)
            {
                captureCfiFiles = FunctionList.GetCfiFiles(captureFiles, Executor_Code, GenericCfgPath, labelList,
                                                    ref logMessage, out skipedFiles);

                RXDataTable.Clear();
                RXCFIInfoTable.Clear();
                prevExecCode = Executor_Code;
            }
            iter++;
            if (strDbType == "UeiTemp")
            {
                comp.DoCompare(IDHeaderData, IDdata, IDheader, results, cmpID, labelList,
                inputFile, captureCfiFiles, ref RXDataTable, ref RXCFIInfoTable, ListID.QA,
                tempFileName, ref QAComplogMessage, skipedFiles,
                out matched);
            }
            else if (strDbType == "UeiTemp_India")
            {
                comp.DoCompare(IDHeaderData, IDdata, IDheader, results, cmpID, labelList,
                inputFile, captureCfiFiles, ref RXDataTable, ref RXCFIInfoTable, ListID.QAI,
                tempFileName, ref QAComplogMessage, skipedFiles,
                out matched);
            }
            else if (strDbType == "UEI_PUBLIC")
            {
                comp.DoCompare(IDHeaderData, IDdata, IDheader, results, cmpID, labelList,
              inputFile, captureCfiFiles, ref RXDataTable, ref RXCFIInfoTable, ListID.UEI,
              tempFileName, ref QAComplogMessage, skipedFiles,
              out matched);
            }
            ProcessResults(inputFile, execList, logFileContents, startTime, tempFileDir,
                ref logMessage, QAComplogMessage, QAComplogMessage_India, UEIComplogMessage,
                ENDlogMessage, captureCfiFiles, tempFileName, prefixExcludedIDList, ExecIDTable);

            return results;
        }

        #region Private Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="execList"></param>
        /// <param name="logFileContents"></param>
        /// <param name="startTime"></param>
        /// <param name="tempFileDir"></param>
        /// <param name="logMessage"></param>
        /// <param name="QAComplogMessage"></param>
        /// <param name="UEIComplogMessage"></param>
        /// <param name="ENDlogMessage"></param>
        /// <param name="cfiFiles"></param>
        /// <param name="tempFileName"></param>
        /// <param name="prefixExculdedIDList"></param>
        /// <param name="ExecIDTable"></param>
        private void ProcessResults(string inputFile, List<int> execList, StringBuilder logFileContents,
            DateTime startTime, string tempFileDir, ref string logMessage,
            string QAComplogMessage, string QAComplogMessage_India, string UEIComplogMessage, string ENDlogMessage,
            StringCollection cfiFiles, string tempFileName, StringCollection prefixExculdedIDList, Hashtable ExecIDTable)
        {
            // remove all temporary files and old working files in the directories we will be using
            try
            {
                if (File.Exists(tempFileName))
                    File.Delete(tempFileName);
            }
            catch { }

            if (KeepAllCFIFiles && cfiFiles != null)
            {
                if (cfiFiles.Count > 0)
                {
                    foreach (string file in cfiFiles)
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch { }
                    }

                    string s = cfiFiles[0].Substring(0, cfiFiles[0].LastIndexOf('\\'));
                    string[] dirs = Directory.GetDirectories(s);

                    if (dirs == null || dirs.Length == 0)
                    {
                        try
                        {
                            Directory.Delete(s);
                        }
                        catch { }
                    }
                }
            }
            RemoveTempFiles(tempFileDir);
            // Write log of all messages that have been accumulated from the process
            logMessage += GetExecIDLogMessage(ExecIDTable, execList, prefixExculdedIDList);
            logFileContents.Append(logMessage + ENDlogMessage + QAComplogMessage + QAComplogMessage_India + UEIComplogMessage);

            DateTime endTime = DateTime.Now;
            logFileContents.AppendFormat("\r\n\r\nFinish Comp. at:  {0}\r\n", endTime);
            logFileContents.AppendFormat("Total time used for Comp.: {0}\r\n", FunctionList.GetTimeDifference(startTime, endTime));

            File.WriteAllText(WORKDIR + Path.GetFileNameWithoutExtension(inputFile) + ".LOG", logFileContents.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ExecIDTable"></param>
        /// <param name="execList"></param>
        /// <param name="prefixExculdedIDList"></param>
        private string GetExecIDLogMessage(Hashtable ExecIDTable, List<int> execList, StringCollection prefixExculdedIDList)
        {
            if (ExecIDTable.Count == 0)
            {
                return "      \r\nNone\r\n\r\n";
            }

            string logMessage = "\r\n";
            for (int i = 0; i < execList.Count; i++)
            {
                logMessage += String.Format("\r\nE{0:000}:", execList[i]);
                if (ExecIDTable.ContainsKey(execList[i]))
                {
                    StringCollection idList = (StringCollection)ExecIDTable[execList[i]];

                    for (int j = 0; j < idList.Count; j++)
                    {
                        if (j % 8 == 0)
                            logMessage += "\r\n ";
                        logMessage += "   " + idList[j];
                    }

                    if (execList[i] == 90 && prefixExculdedIDList.Count > 0)
                    {
                        logMessage += "\r\n\r\n    The following IDs are screened out by prefix comparisons:\r\n";
                        for (int j = 0; j < prefixExculdedIDList.Count; j++)
                        {
                            if (j % 8 == 0)
                                logMessage += "\r\n ";
                            logMessage += "   " + prefixExculdedIDList[j];
                        }
                    }
                }
                else
                    logMessage += "\r\n";
            }

            return logMessage;
        }


        private void Add(int executor, string id,
            Hashtable ExecIDTable, ListID listID)
        {
            if (ExecIDTable.ContainsKey(executor))
            {
                StringCollection idList
                    = (StringCollection)ExecIDTable[executor];
                AddToList(id, listID, idList);
                ExecIDTable[executor] = idList;
            }
            else
            {
                StringCollection idList = new StringCollection();
                idList.Add(listID.Prefix + id);
                ExecIDTable.Add(executor, idList);
            }
        }


        private void AddToList(string id, ListID listID, StringCollection idList)
        {
            int i = 0;

            for (; i < idList.Count; i++)
            {
                string[] strs = idList[i].Split(new char[] { ')' },
                     StringSplitOptions.RemoveEmptyEntries);
                string s = strs[1].Trim();

                if (String.Compare(id, s) < 0)
                    break;
            }

            idList.Insert(i, listID.Prefix + id);
        }


        private int GetPercentage(int num, int total)
        {
            int percentage = 1000 * num / total;

            int mod = percentage % 10;

            if (mod >= 5)
                percentage += 10;

            return (percentage - mod) / 10;
        }


        private string unzipTNZipFile(string zipFilePath)
        {
            string filePath = WORKDIR;

            int i = zipFilePath.Length - 1;
            int j;

            while (zipFilePath[i] != '\\' && i >= 0)
                i--;

            j = i + 1;

            while (zipFilePath[j] != '.' && j < zipFilePath.Length - 1)
                j++;

            filePath += zipFilePath.Substring(i + 1, j - i - 1) + "\\";

            if (Directory.Exists(filePath))
                Directory.Delete(filePath, true);
            Directory.CreateDirectory(filePath);

            //ZipHelper.ExtractAll(zipFilePath, filePath);
            FastZip fz = new FastZip();
            fz.CreateEmptyDirectories = true;
            fz.ExtractZip(zipFilePath, filePath, "");
            return filePath;
        }

        private void RemoveTempFiles(string tempFileDir)
        {
            if (tempFileDir == "")
                return;

            if (KeepAllCFIFiles)
            {
                string[] files = Directory.GetFiles(tempFileDir);

                if (files == null || files.Length == 0)
                    return;

                for (int i = 0; i < files.Length; i++)
                {
                    if (!files[i].Contains(".cfi")
                        && !files[i].Contains("._cfi"))
                    {
                        try
                        {
                            File.Delete(files[i]);
                        }
                        catch { }
                    }
                }
            }
            else
            {
                if (Directory.Exists(tempFileDir))
                {
                    try
                    {
                        Directory.Delete(tempFileDir, true);
                    }
                    catch { }
                }
            }
        }

        #endregion
        public class ListID
        {
            private readonly string _prefix;
            private readonly string _dbName;
            private readonly string _extension;
            public static readonly ListID QA = new ListID("(Q)", "QADB", ".QA");
            public static readonly ListID UEI = new ListID("(U)", "UEIDB", ".UEI");
            public static readonly ListID QAI = new ListID("(I)", "QAIDB", ".IQA");
            private ListID(string prefix, string dbName, string extension)
            {
                _prefix = prefix;
                _dbName = dbName;
                _extension = extension;
            }

            public string Prefix
            {
                get { return _prefix; }
            }

            public string DBName
            {
                get { return _dbName; }
            }

            public string Extension
            {
                get
                {
                    return _extension;
                }
            }
        }
    }
}
