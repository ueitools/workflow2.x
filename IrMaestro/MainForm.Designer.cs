namespace IRMaestro
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ReportTB = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pickToFIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readBackVerificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readBackResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decoderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iDVerificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.macroAnalyzerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iDCompareAppToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timingInspectorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timingInspectorResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iDToFIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pickToCFIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ReportTB
            // 
            this.ReportTB.BackColor = System.Drawing.SystemColors.Control;
            this.ReportTB.Location = new System.Drawing.Point(2, 27);
            this.ReportTB.Name = "ReportTB";
            this.ReportTB.Size = new System.Drawing.Size(607, 334);
            this.ReportTB.TabIndex = 0;
            this.ReportTB.Text = "";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(609, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pickToFIToolStripMenuItem,
            this.readBackVerificationToolStripMenuItem,
            this.readBackResultsToolStripMenuItem,
            this.decoderToolStripMenuItem,
            this.iDVerificationToolStripMenuItem,
            this.macroAnalyzerToolStripMenuItem,
            this.iDCompareAppToolStripMenuItem,
            this.timingInspectorToolStripMenuItem,
            this.timingInspectorResultsToolStripMenuItem,
            this.iDToFIToolStripMenuItem,
            this.pickToCFIToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.toolsToolStripMenuItem.Text = "IR Maestro";
            // 
            // pickToFIToolStripMenuItem
            // 
            this.pickToFIToolStripMenuItem.Name = "pickToFIToolStripMenuItem";
            this.pickToFIToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.pickToFIToolStripMenuItem.Text = "Pick to FI";
            this.pickToFIToolStripMenuItem.Click += new System.EventHandler(this.pickToFIToolStripMenuItem_Click);
            // 
            // readBackVerificationToolStripMenuItem
            // 
            this.readBackVerificationToolStripMenuItem.Name = "readBackVerificationToolStripMenuItem";
            this.readBackVerificationToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.readBackVerificationToolStripMenuItem.Text = "Read Back Verification";
            this.readBackVerificationToolStripMenuItem.Click += new System.EventHandler(this.readBackVerificationToolStripMenuItem_Click);
            // 
            // readBackResultsToolStripMenuItem
            // 
            this.readBackResultsToolStripMenuItem.Name = "readBackResultsToolStripMenuItem";
            this.readBackResultsToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.readBackResultsToolStripMenuItem.Text = "Read Back Results";
            this.readBackResultsToolStripMenuItem.Click += new System.EventHandler(this.readBackResultsToolStripMenuItem_Click);
            // 
            // decoderToolStripMenuItem
            // 
            this.decoderToolStripMenuItem.Name = "decoderToolStripMenuItem";
            this.decoderToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.decoderToolStripMenuItem.Text = "Decoder";
            this.decoderToolStripMenuItem.Click += new System.EventHandler(this.decoderToolStripMenuItem_Click);
            // 
            // iDVerificationToolStripMenuItem
            // 
            this.iDVerificationToolStripMenuItem.Name = "iDVerificationToolStripMenuItem";
            this.iDVerificationToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.iDVerificationToolStripMenuItem.Text = "ID Verification";
            this.iDVerificationToolStripMenuItem.Click += new System.EventHandler(this.iDVerificationToolStripMenuItem_Click);
            // 
            // macroAnalyzerToolStripMenuItem
            // 
            this.macroAnalyzerToolStripMenuItem.Name = "macroAnalyzerToolStripMenuItem";
            this.macroAnalyzerToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.macroAnalyzerToolStripMenuItem.Text = "Macro Analyzer";
            this.macroAnalyzerToolStripMenuItem.Click += new System.EventHandler(this.macroAnalyzerToolStripMenuItem_Click);
            // 
            // iDCompareAppToolStripMenuItem
            // 
            this.iDCompareAppToolStripMenuItem.Name = "iDCompareAppToolStripMenuItem";
            this.iDCompareAppToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.iDCompareAppToolStripMenuItem.Text = "ID Compare ";
            this.iDCompareAppToolStripMenuItem.ToolTipText = "ID Compare";
            this.iDCompareAppToolStripMenuItem.Click += new System.EventHandler(this.iDCompareAppToolStripMenuItem_Click);
            // 
            // timingInspectorToolStripMenuItem
            // 
            this.timingInspectorToolStripMenuItem.Name = "timingInspectorToolStripMenuItem";
            this.timingInspectorToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.timingInspectorToolStripMenuItem.Text = "Timing Inspector";
            this.timingInspectorToolStripMenuItem.Click += new System.EventHandler(this.timingInspectorToolStripMenuItem_Click);
            // 
            // timingInspectorResultsToolStripMenuItem
            // 
            this.timingInspectorResultsToolStripMenuItem.Name = "timingInspectorResultsToolStripMenuItem";
            this.timingInspectorResultsToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.timingInspectorResultsToolStripMenuItem.Text = "Timing Inspector Results";
            this.timingInspectorResultsToolStripMenuItem.Click += new System.EventHandler(this.timingInspectorResultsToolStripMenuItem_Click);
            // 
            // iDToFIToolStripMenuItem
            // 
            this.iDToFIToolStripMenuItem.Name = "iDToFIToolStripMenuItem";
            this.iDToFIToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.iDToFIToolStripMenuItem.Text = "ID To FI";
            this.iDToFIToolStripMenuItem.Click += new System.EventHandler(this.iDToFIToolStripMenuItem_Click);
            // 
            // pickToCFIToolStripMenuItem
            // 
            this.pickToCFIToolStripMenuItem.Name = "pickToCFIToolStripMenuItem";
            this.pickToCFIToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.pickToCFIToolStripMenuItem.Text = "Pick to CFI";
            this.pickToCFIToolStripMenuItem.Click += new System.EventHandler(this.pickToCFIToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 367);
            this.Controls.Add(this.ReportTB);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox ReportTB;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pickToFIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readBackVerificationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readBackResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem decoderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iDVerificationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem macroAnalyzerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iDCompareAppToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timingInspectorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timingInspectorResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iDToFIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pickToCFIToolStripMenuItem;
    }
}

