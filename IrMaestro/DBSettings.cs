using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;
 namespace IRMaestro
{
    /// <summary>
    /// enum value to switch the selection between UEIDB and Pick DB
    /// </summary>
     public enum DBType
    {
        UEIDB,
        PickDB
    };

     public class SetDB
     {
         public DBType selectedDBType;
         public string pickDBPath;

         public void GetDBType(string regpath)
         {
             string dbtype;
             dbtype = DBSettings.GetDBType(regpath, "SelectedDBType");
             if (dbtype == "UEIDB")
                 selectedDBType = DBType.UEIDB;
             else
                 selectedDBType = DBType.PickDB;
         }
         public void SetDBType(DBType db,string regpath)
         {
             if (db == DBType.UEIDB)
                 DBSettings.SetRegistry(regpath, "SelectedDBType", "UEIDB");
             else
                 DBSettings.SetRegistry(regpath, "SelectedDBType", "PickDB");
         }
         public void GetPickDBPath(string regpath)
         {
             //pickDBPath = DBSettings.GetDBPath(regpath, "SelectedPickDB");
             //if (!File.Exists(pickDBPath))
             //{
             //    SetDBType(DBType.UEIDB, regpath);
             //    selectedDBType = DBType.UEIDB;
             //}
             //Start Add - PAL 
             pickDBPath = DBSettings.GetDBPath(regpath, "SelectedPickProject");
             //End Add - PAL 

         }

     }

     public class DBSettings
    {
        const string _WORKINGDIR = "WorkingDirectory";
        const string _DEFAULT_WORKINGDIR = "C:\\Working";
        const string ueitools = "HKEY_CURRENT_USER\\Software\\UEITools";
        const string _DEFAULT_DBTYPE = "UEIDB";

         
        #region Static Variables
        public static string WorkingDir = _DEFAULT_WORKINGDIR;
        public static string PodType;
        
        
        public static string selectedId=string.Empty;
        public static string workingpath;
        public static string pickIDWorkingPath;
                
        public static string idregpath;
        public static string macroregpath;
        
        
        

    #endregion

        #region Methods
            public static string BrowseFolder()
            {
                FolderBrowserDialog browsefolder = new FolderBrowserDialog();
                if (browsefolder.ShowDialog() == DialogResult.OK)
                {
                    return browsefolder.SelectedPath;
                }
                else
                {
                    return string.Empty;
                }
            }

            public static string BrowseFile(string FileTypes, string pattern)
            {
                OpenFileDialog openFile = new OpenFileDialog();

                openFile.Filter = string.Format("{0}({1})|{1}", FileTypes, pattern);

                if (openFile.ShowDialog() == DialogResult.OK)
                    return openFile.FileName;
                else
                    return string.Empty;
            }

            public static string GetPathName(string FilePath)
            {
                if((FilePath.CompareTo(null)!=0 || FilePath!=string.Empty))
                {
                    FileInfo fi = new FileInfo(FilePath);
                    return fi.DirectoryName;
                }
                else
                {
                    return string.Empty;
                }
                
            }

            public static string GetFileNameWithoutExt(string FilePath)
            {
                FileInfo fi = new FileInfo(FilePath);
                string fname = fi.Name;
                fname = fname.Substring(0, fname.IndexOf("."));
                return fname;
            }

            public static void CreatePickWorkingPath()
            {
                workingpath = GetRegistry("WorkingDirectory") + Path.DirectorySeparatorChar;
                workingpath += selectedId;
                pickIDWorkingPath = workingpath + Path.DirectorySeparatorChar;
                try
                {
                    // clear and create the pick ID working path
                    if (Directory.Exists(pickIDWorkingPath))
                        Directory.Delete(pickIDWorkingPath, true);
                    Directory.CreateDirectory(pickIDWorkingPath);
                }
                catch { }

            }

            public static string GetRegistry(string RegistryName)
            {
                string result = (string)Registry.GetValue(ueitools, RegistryName, WorkingDir);

                if (string.IsNullOrEmpty(result))
                    result = WorkingDir;

                return result;
            }

            public static void SetRegistry(string regpath,string RegistryName, string value)
            {
                Registry.SetValue(regpath, RegistryName,
                    value, RegistryValueKind.String);
            }

            public static string GetDBType(string regpath,string RegistryName)
            {
                string dbtype = string.Empty;
                dbtype = (string)Registry.GetValue(regpath, RegistryName, _DEFAULT_DBTYPE);

                if (string.IsNullOrEmpty(dbtype))
                    dbtype = _DEFAULT_DBTYPE;
                            
                return dbtype;
            }

            public static string GetDBPath(string regpath,string RegistryName)
            {
                string dbpath = string.Empty;
                dbpath = (string)Registry.GetValue(regpath, RegistryName, string.Empty);

                if (string.IsNullOrEmpty(dbpath))
                    dbpath = string.Empty;

                return dbpath;

            }

        #endregion



    }
}
