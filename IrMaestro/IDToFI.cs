﻿using BusinessObject;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;


namespace IRMaestro
{
    public delegate void FIGenStatusDelegate(IDToFI.IDToFIStatus FiGenerationStatus);
    
    public class IDToFI
    {
        private FIGenStatusDelegate fiGenCallBack;
        private const string FIFolderSuffix = "FI";
        private const string PRJFileExtension = ".prj";
        public IDToFI()
        {
        }

        public IDToFI(FIGenStatusDelegate fiGenerationCallBack)
        {
            this.fiGenCallBack = fiGenerationCallBack;
        }

        public void GenerateFI(IList<string> idList, string fiPath)
        {

            if (idList!=null)
            {
                foreach (var id in idList)
                {
                    string fiOutputPath = CreateOutputFolder(id, fiPath);


                  IDToFIStatus fiStatus =  GenerateFIFromId(id,fiOutputPath);

                  if (this.fiGenCallBack != null)
                      this.fiGenCallBack(fiStatus);
                }
            }
        }
        /// <summary>
        /// Generates FI for all keys associated with the id in database.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fiPath"></param>
        /// <param name="convertToZip"></param>
        /// <param name="idFailureReason"></param>
        /// <remarks>Modified on May 8,2014: Added param 'bool convertToZip' </remarks>
        /// <returns></returns>
        public bool GenerateFI(string id, string fiPath,bool convertToZip,out string idFailureReason)
        {
            idFailureReason = String.Empty;

            if (!String.IsNullOrEmpty(id) && !String.IsNullOrEmpty(fiPath) && Directory.Exists(fiPath))
            {
                
               IDToFIStatus fiStatus = GenerateFIFromId(id, fiPath);

                    if (fiStatus.IDStatus == FIGenerationState.Successfull)
                    {
                        try
                        {
                            string fiFolder = Path.Combine(fiPath, id + FIFolderSuffix);

                            if (convertToZip)
                            {
                                //Zip the fi folder contents.
                                IRMaestro.ZipHelper.CreateZipFromDirectory(fiFolder + ".zip", fiFolder, true);
                            }
                        }
                        catch (Exception ex) {  /*Do nothing.*/ System.Diagnostics.Debug.WriteLine(ex.Message); }
                        return true;
                    }
                    else
                        idFailureReason = fiStatus.IDFailureReason.Message;
                

            }

            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">ID from which FI files are generated.</param>
        /// <param name="fiPath">Working directory</param>
        /// <returns></returns>
        private IDToFIStatus GenerateFIFromId(string id, string fiPath)
        {
            IDToFIStatus fiGenerationStatus = null;
            string fiOutputPath = String.Empty;
            string prjFileFullPath = String.Empty;
            string invalidFileNameLogPath = String.Empty;

            StringBuilder invalidKeyFileNames = new StringBuilder();

            StringBuilder prjFileContents = new StringBuilder(String.Format("Id : {0}",id));
            prjFileContents.AppendLine(Environment.NewLine);

            try
            {
                IList<Hashtable> functionList = DAOFactory.Function().GetIdData(id);

                if (functionList.Count > 0)
                {
                    //create FI folder.
                    fiOutputPath = CreateOutputFolder(id, fiPath);
                    //create InvalidFilName log file path
                    invalidFileNameLogPath = Path.Combine(fiOutputPath,"UnsupportedLabelNames.txt");
                    //create prj file path.
                    prjFileFullPath = Path.Combine(fiOutputPath, id + FIFolderSuffix + PRJFileExtension);
                    
                    IDHeader IDHeaderData = DAOFactory.IDHeader().Select(id);
                    int exec = IDHeaderData.Executor_Code;
                    List<int> _prefixes = new List<int>();
                    for (int i = 0; i < IDHeaderData.PrefixList.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(IDHeaderData.PrefixList[i].Data.ToString()))
                            _prefixes.Add(Convert.ToInt32(IDHeaderData.PrefixList[i].Data, 2));
                    }

                    bool _seperateToggleOutput = true;
                    string invertdata = IDHeaderData.IsInversedData;
                    bool _isinvert = false;
                    if (invertdata != null && invertdata.ToUpper() == "Y")
                        _isinvert = true;


                    int functionIndex = 0;
                    foreach (Hashtable functionTable in functionList)
                    {
                        string label =  functionTable["Label"].ToString();
                        string _data = functionTable["Data"].ToString();
                        //string filename = id + "k_" + functionIndex + ".fi";
                        string filename =  id + "_" + label + ".fi";
                        
                        if (IsFileNameValid(filename))
                        {
                            string fiFileFullPath = Path.Combine(fiOutputPath, filename);
                            //add filename and label in prj file
                            prjFileContents.AppendLine(String.Format("{0} : {1}", filename, label));

                            int filescount = CommonForms.FunctionList.CreateTxFiFile(exec, label, _data, _prefixes, _isinvert, fiFileFullPath, _seperateToggleOutput);

                            for (int i = 1; i < filescount; i++)
                            {
                                //eg: T0000k_2_1.fi
                                filename = id + "k_" + functionIndex + "_" + i + ".fi";
                                //add filename and label in prj file
                                prjFileContents.AppendLine(String.Format("{0} : {1}", filename, label));
                            }
                        }
                        else
                        {
                            invalidKeyFileNames.AppendLine(String.Format("{0} : {1}", filename, label));
                        }

                        functionIndex++;
                    }
                    //create .prj file

                    if (prjFileContents.Length > 0)
                    {
                        using (StreamWriter swriter = new StreamWriter(prjFileFullPath))
                        {
                            swriter.Write(prjFileContents.ToString());
                        }
                    }

                    //log
                    if (invalidKeyFileNames.Length > 0)
                    {
                        using (StreamWriter swriter = new StreamWriter(invalidFileNameLogPath))
                        {
                            swriter.Write(invalidKeyFileNames.ToString());
                        }
                    }

                    fiGenerationStatus = new IDToFIStatus(id, FIGenerationState.Successfull);

                    return fiGenerationStatus;
                }
                else
                {
                    fiGenerationStatus = new IDToFIStatus(id, FIGenerationState.Failure, new Exception("No functions in ID."));
                    return fiGenerationStatus;
                }
            }
            catch (Exception ex)
            {

                fiGenerationStatus = new IDToFIStatus(id, FIGenerationState.Failure,ex);
                return fiGenerationStatus;
            }
        }

        private string CreateOutputFolder(string id, string directory)
        {
            string fiOutputPath = Path.Combine(directory, id + FIFolderSuffix);

            if (Directory.Exists(fiOutputPath))
                Directory.Delete(fiOutputPath, true);

           DirectoryInfo dirInfo = Directory.CreateDirectory(fiOutputPath);

           if (dirInfo != null)
               return dirInfo.FullName;
           else
               return String.Empty;
           
        }

        private bool IsFileNameValid(string fileName)
        {
           char[] invalidFileNameChars =  Path.GetInvalidFileNameChars();
           System.Text.RegularExpressions.Regex containsInvalidChars = new System.Text.RegularExpressions.Regex("["+ 
               System.Text.RegularExpressions.Regex.Escape(new String(invalidFileNameChars))+"]");
           if (containsInvalidChars.IsMatch(fileName))
               return false;

            return true;
        }
       
        #region nestedclass

        public class IDToFIStatus
        {
            private string currentID;
            private FIGenerationState figenerationState = FIGenerationState.Failure;
            private Exception fiGenException = null;

            
            public IDToFIStatus(string id, FIGenerationState state):this(id,state,null){}

            public IDToFIStatus(string id, FIGenerationState state,Exception exception)
            {
                this.currentID = id;
                this.figenerationState = state;
                this.fiGenException = exception;
            }

            public string ID
            {
                get { return this.currentID; }
            }

            public FIGenerationState IDStatus
            {
                get { return this.figenerationState; }
            }

            public Exception IDFailureReason
            {
                get { return this.fiGenException; }
            }
        }
        #endregion
    }

    public enum FIGenerationState
    {
        Successfull,
        Failure
    }
}
