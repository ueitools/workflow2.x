﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace UEI.ServiceProvider
{
    public class DeviceType
    {
        private Int32 m_ServiceProviderRID = 0;
        public Int32 ServiceProviderRID
        {
            get { return m_ServiceProviderRID; }
            set { m_ServiceProviderRID = value; }
        }
        private String m_MainDeviceType = String.Empty;
        public String MainDeviceType
        {
            get { return m_MainDeviceType; }
            set { m_MainDeviceType = value; }
        }
        private String m_SubDeviceType;
        public String SubDeviceType
        {
            get { return m_SubDeviceType; }
            set { m_SubDeviceType = value; }
        }
        private String m_SubLocation = String.Empty;
        public String SubLocation
        {
            get { return m_SubLocation; }
            set { m_SubLocation = value; }
        }
        public DeviceType() { }
        public DeviceType(Int32 serviceproviderRID, String maindevice, String subdevice, String sublocation)
        {
            this.ServiceProviderRID = serviceproviderRID;
            this.MainDeviceType = maindevice;
            this.SubDeviceType = subdevice;
            this.SubLocation = sublocation;
        }
    }
    public class DeviceModel : Collection<DeviceType>
    {
        public Dictionary<String, Dictionary<String, List<String>>> GetAllDeviceTypes()
        {
            Dictionary<String, Dictionary<String, List<String>>> m_Result = null;
            if (this.Items != null)
            {
                foreach (DeviceType devicetype in this.Items)
                {
                    if (m_Result == null)
                        m_Result = new Dictionary<String, Dictionary<String, List<String>>>();
                    if (!String.IsNullOrEmpty(devicetype.MainDeviceType))
                    {
                        if (!m_Result.ContainsKey(devicetype.MainDeviceType))
                        {
                            m_Result.Add(devicetype.MainDeviceType, new Dictionary<String, List<String>>());
                        }
                    }
                    if (!String.IsNullOrEmpty(devicetype.SubDeviceType))
                    {
                        if (!m_Result[devicetype.MainDeviceType].ContainsKey(devicetype.SubDeviceType))
                        {
                            m_Result[devicetype.MainDeviceType].Add(devicetype.SubDeviceType, new List<String>());
                        }
                    }
                    if (!String.IsNullOrEmpty(devicetype.SubLocation))
                    {
                        if (!m_Result[devicetype.MainDeviceType][devicetype.SubDeviceType].Contains(devicetype.SubLocation))
                        {
                            m_Result[devicetype.MainDeviceType][devicetype.SubDeviceType].Add(devicetype.SubLocation);
                        }
                    }
                }
            }
            return m_Result;
        }
        public DeviceModel GetDeviceTypesByServiceProvider(Int32 serviceproviderrid)
        {
            DeviceModel m_Result = null;
            if (this.Items != null)
            {
                foreach (DeviceType devicetype in this.Items)
                {
                    if (devicetype.ServiceProviderRID == serviceproviderrid)
                    {
                        if (m_Result == null)
                            m_Result = new DeviceModel();
                        m_Result.Add(devicetype);
                    }
                }
            }
            return m_Result;
        }
    }
}