﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace UEI.ServiceProvider
{
    public class Locations
    {
        private Int32 m_LocationRID = 0;
        public Int32 LocationRID
        {
            get { return m_LocationRID; }
            set { m_LocationRID = value; }
        }
        private Int32 m_ServiceProviderRID = 0;
        public Int32 ServiceProviderRID
        {
            get { return m_ServiceProviderRID; }
            set { m_ServiceProviderRID = value; }
        }
        private String m_Region = String.Empty;
        public String Region
        {
            get { return m_Region; }
            set { m_Region = value; }
        }
        private String m_Country = String.Empty;
        public String Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }
        private String m_SubLocation = String.Empty;
        public String SubLocation
        {
            get { return m_SubLocation; }
            set { m_SubLocation = value; }
        }              
    }
    public class LocationModel : Collection<Locations>
    {
        public Dictionary<String, Dictionary<String, List<String>>> GetAllLocationByRegion(String strRegion)
        {
            Dictionary<String, Dictionary<String, List<String>>> m_Result = null;
            if (this.Items != null)
            {
                foreach (Locations location in this.Items)
                {
                    if (location.Region.Contains(strRegion))
                    {
                        if (m_Result == null)
                            m_Result = new Dictionary<String, Dictionary<String, List<String>>>();
                        if (!String.IsNullOrEmpty(location.Region))
                        {
                            if (!m_Result.ContainsKey(location.Region))
                            {
                                m_Result.Add(location.Region, new Dictionary<String, List<String>>());
                            }
                        }
                        if (!String.IsNullOrEmpty(location.Country))
                        {
                            if (!m_Result[location.Region].ContainsKey(location.Country))
                            {
                                m_Result[location.Region].Add(location.Country, new List<String>());
                            }
                        }
                        if (!String.IsNullOrEmpty(location.SubLocation))
                        {
                            if (!m_Result[location.Region][location.Country].Contains(location.SubLocation))
                            {
                                m_Result[location.Region][location.Country].Add(location.SubLocation);
                            }
                        }
                    }
                }
            }
            return m_Result;
        }
        public Dictionary<String, Dictionary<String, List<String>>> GetAllLocations()
        {
            Dictionary<String, Dictionary<String, List<String>>> m_Result = null;            
            if (this.Items != null)
            {
                foreach (Locations location in this.Items)
                {
                    if (m_Result == null)
                        m_Result = new Dictionary<String, Dictionary<String, List<String>>>();
                    if (!String.IsNullOrEmpty(location.Region))
                    {
                        if (!m_Result.ContainsKey(location.Region))
                        {
                            m_Result.Add(location.Region, new Dictionary<String, List<String>>());
                        }
                    }
                    if (!String.IsNullOrEmpty(location.Country))
                    {
                        if (!m_Result[location.Region].ContainsKey(location.Country))
                        {
                            m_Result[location.Region].Add(location.Country, new List<String>());
                        }
                    }
                    if (!String.IsNullOrEmpty(location.SubLocation))
                    {
                        if (!m_Result[location.Region][location.Country].Contains(location.SubLocation))
                        {
                            m_Result[location.Region][location.Country].Add(location.SubLocation);
                        }
                    }
                }
            }
            return m_Result;
        }        
        public LocationModel GetLocationsByServiceProvider(Int32 serviceproviderrid)
        {
            LocationModel m_Result = null;
            if (this.Items != null)
            {
                foreach (Locations location in this.Items)
                {
                    if (location.ServiceProviderRID == serviceproviderrid)
                    {
                        if (m_Result == null)
                            m_Result = new LocationModel();
                        m_Result.Add(location);
                    }                    
                }
            }
            return m_Result;
        }
    }
}