﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace UEI.ServiceProvider
{
    public class ServiceProviders
    {
        private LocationModel m_Location = new LocationModel();
        public LocationModel Location
        {
            get { return m_Location; }
            set { m_Location = value; }
        }
        private DeviceModel m_DeviceTypes = new DeviceModel();
        public DeviceModel DeviceTypes
        {
            get { return m_DeviceTypes; }
            set { m_DeviceTypes = value; }
        }
        private Int32 m_ServiceProviderRID = 0;
        public Int32 ServiceProviderRID
        {
            get { return m_ServiceProviderRID; }
            set { m_ServiceProviderRID = value; }
        }
        private String m_MSOSP = String.Empty;
        public String MSOSP
        {
            get { return m_MSOSP; }
            set { m_MSOSP = value; }
        }
        private String m_ServiceProvider = String.Empty;
        public String ServiceProvider
        {
            get { return m_ServiceProvider; }
            set { m_ServiceProvider = value; }
        }
        private String m_AliasServiceProvider = String.Empty;
        public String AliasServiceProvider
        {
            get { return m_AliasServiceProvider; }
            set { m_AliasServiceProvider = value; }
        }
        private String m_ServiceProviderVariation = String.Empty;
        public String ServiceProviderVariation
        {
            get { return m_ServiceProviderVariation; }
            set { m_ServiceProviderVariation = value; }
        }
        private String m_Brand = String.Empty;
        public String Brand
        {
            get { return m_Brand; }
            set { m_Brand = value; }
        }
        private String m_AliasBrand = String.Empty;
        public String AliasBrand
        {
            get { return m_AliasBrand; }
            set { m_AliasBrand = value; }
        }
        private String m_BrandVariation = String.Empty;
        public String BrandVariation
        {
            get { return m_BrandVariation; }
            set { m_BrandVariation = value; }
        }
        private String m_Model = String.Empty;
        public String Model
        {
            get { return m_Model; }
            set { m_Model = value; }
        }
        private String m_ID = String.Empty;
        public String ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }                       
        public Dictionary<String, List<String>> GetServiceProviderByRegion(String region)
        {
            Dictionary<String, List<String>> m_Result = null;
            if (this.Location != null)
            {
                foreach (Locations location in this.Location)
                {
                    if (location.Region.Contains(region))
                    {
                        if (m_Result == null)
                            m_Result = new Dictionary<String, List<String>>();
                        if (!m_Result.ContainsKey(location.Region))
                            m_Result.Add(location.Region, new List<string> { location.Country });
                        else
                            m_Result[location.Region].Add(location.Country);
                    }
                }
            }
            return m_Result;
        }
        public Dictionary<String, List<String>> GetServiceProviderByCountry(String region, String country)
        {
            Dictionary<String, List<String>> m_Result = null;
            if (this.Location != null)
            {
                foreach (Locations location in this.Location)
                {
                    if (!String.IsNullOrEmpty(location.Country))
                    {
                        if ((location.Country.Contains(country)) && (location.Region.Contains(region)))
                        {
                            if (m_Result == null)
                                m_Result = new Dictionary<String, List<String>>();
                            if (!m_Result.ContainsKey(location.Region))
                                m_Result.Add(location.Region, new List<string> { location.Country });
                            else
                                m_Result[location.Region].Add(location.Country);
                        }
                    }                    
                }
            }
            return m_Result;
        }
        public Dictionary<String, List<String>> GetServiceProviderBySubLocation(String region, String country, String sublocation)
        {
            Dictionary<String, List<String>> m_Result = null;
            if (this.Location != null)
            {
                foreach (Locations location in this.Location)
                {
                    if (!String.IsNullOrEmpty(location.SubLocation))
                    {
                        if ((location.SubLocation.Contains(sublocation)) && (location.Country.Contains(country)) && (location.Region.Contains(region)))
                        {
                            if (m_Result == null)
                                m_Result = new Dictionary<String, List<String>>();
                            if (!m_Result.ContainsKey(location.Region))
                                m_Result.Add(location.Region, new List<string> { location.Country });
                            else
                                m_Result[location.Region].Add(location.Country);
                        }
                    }                    
                }
            }
            return m_Result;
        }

        public Dictionary<String, List<String>> GetServiceProviderByMainDeviceType(String maindevice)
        {
            Dictionary<String, List<String>> m_Result = null;
            if (this.DeviceTypes != null)
            {
                foreach (DeviceType devicetype in this.DeviceTypes)
                {
                    if (devicetype.MainDeviceType.Contains(maindevice))
                    {
                        if (m_Result == null)
                            m_Result = new Dictionary<String, List<String>>();
                        if (!m_Result.ContainsKey(devicetype.MainDeviceType))
                            m_Result.Add(devicetype.MainDeviceType, new List<string> { devicetype.SubDeviceType });
                        else
                            m_Result[devicetype.MainDeviceType].Add(devicetype.SubDeviceType);
                    }
                }
            }
            return m_Result;
        }
        public Dictionary<String, List<String>> GetServiceProviderBySubDeviceType(String subdevice)
        {
            Dictionary<String, List<String>> m_Result = null;
            if (this.DeviceTypes != null)
            {
                foreach (DeviceType devicetype in this.DeviceTypes)
                {
                    if (!String.IsNullOrEmpty(devicetype.SubDeviceType))
                    {
                        if (devicetype.SubDeviceType.Contains(subdevice))
                        {
                            if (m_Result == null)
                                m_Result = new Dictionary<String, List<String>>();
                            if (!m_Result.ContainsKey(devicetype.MainDeviceType))
                                m_Result.Add(devicetype.MainDeviceType, new List<string> { devicetype.SubDeviceType });
                            else
                                m_Result[devicetype.MainDeviceType].Add(devicetype.SubDeviceType);
                        }
                    }
                }
            }
            return m_Result;
        }
        public Dictionary<String, List<String>> GetServiceProviderBySubLocation1(String sublocation)
        {
            Dictionary<String, List<String>> m_Result = null;
            if (this.DeviceTypes != null)
            {
                foreach (DeviceType devicetype in this.DeviceTypes)
                {
                    if (!String.IsNullOrEmpty(devicetype.SubLocation))
                    {
                        if (devicetype.SubLocation.Contains(sublocation))
                        {
                            if (m_Result == null)
                                m_Result = new Dictionary<String, List<String>>();
                            if (!m_Result.ContainsKey(devicetype.MainDeviceType))
                                m_Result.Add(devicetype.MainDeviceType, new List<string> { devicetype.SubDeviceType });
                            else
                                m_Result[devicetype.MainDeviceType].Add(devicetype.SubDeviceType);
                        }
                    }
                }
            }
            return m_Result;
        }
    }
    public class ServiceProviderModel : Collection<ServiceProviders>
    {
        
    }
}