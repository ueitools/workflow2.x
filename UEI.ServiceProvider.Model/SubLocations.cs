﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace UEI.ServiceProvider
{
    public class SubLocation
    {
        private Int64 m_SubLocationRID = 0;
        public Int64 SubLocationRID
        {
            get { return m_SubLocationRID; }
            set { m_SubLocationRID = value; }
        }
        private String m_RegionCode = String.Empty;
        public String RegionCode
        {
            get { return m_RegionCode; }
            set { m_RegionCode = value; }
        }
        private String m_Region1 = String.Empty;
        public String Region1
        {
            get { return m_Region1; }
            set { m_Region1 = value; }
        }
        private String m_Region2 = String.Empty;
        public String Region2
        {
            get { return m_Region2; }
            set { m_Region2 = value; }
        }
        private String m_Region3 = String.Empty;
        public String Region3
        {
            get { return m_Region3; }
            set { m_Region3 = value; }
        }
        private String m_Region4 = String.Empty;
        public String Region4
        {
            get { return m_Region4; }
            set { m_Region4 = value; }
        }
        private String m_City = String.Empty;
        public String City
        {
            get { return m_City; }
            set { m_City = value; }
        }
        private String m_Area1 = String.Empty;
        public String Area1
        {
            get { return m_Area1; }
            set { m_Area1 = value; }
        }
        private String m_Area2 = String.Empty;
        public String Area2
        {
            get { return m_Area2; }
            set { m_Area2 = value; }
        }
        private String m_ZipCode = String.Empty;
        public String ZipCode
        {
            get { return m_ZipCode; }
            set { m_ZipCode = value; }
        }
        private String m_TimeZone = String.Empty;
        public String TimeZone
        {
            get { return m_TimeZone; }
            set { m_TimeZone = value; }
        }
        private String m_UTCOffset = String.Empty;
        public String UTCOffset
        {
            get { return m_UTCOffset; }
            set { m_UTCOffset = value; }
        }
        private Double m_Latitude;
        public Double Latitude
        {
            get { return m_Latitude; }
            set { m_Latitude = value; }
        }
        private Double m_Longitude;
        public Double Longitude
        {
            get { return m_Longitude; }
            set { m_Longitude = value; }
        }
        private String m_ProvinceID = String.Empty;
        public String ProvinceID
        {
            get { return m_ProvinceID; }
            set { m_ProvinceID = value; }
        }
        private String m_CityID = String.Empty;
        public String CityID
        {
            get { return m_CityID; }
            set { m_CityID = value; }
        }
    }
    public class SubLocationModel : Collection<SubLocation>
    {
        public SubLocationModel GetSublocationByZipCode(String strZIPCode)
        {
            SubLocationModel m_Result = null;
            if ((this.Items != null) && (!String.IsNullOrEmpty(strZIPCode)))
            {
                foreach (SubLocation item in this.Items)
                {                    
                    if (item.ZipCode.Contains(strZIPCode))
                    {
                        if (m_Result == null)
                            m_Result = new SubLocationModel();
                        //int index = m_Result.IndexOf(item);
                        //if (index == -1)
                            m_Result.Add(item);
                    }
                }
            }
            return m_Result;
        }
    }
}