using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using BusinessObject;

namespace EntryHelper
{
    public class IntronAssigner
    {
        /// <summary>
        /// Search the database for an intron for any given Mode and Label 
        /// </summary>
        /// <param name="Mode"></param>
        /// <param name="Label"></param>
        /// <returns>Intron or null</returns>
        public string GetIntron(string Mode, string Label)
        {
            string Result = string.Empty;
            string SearchKey = null;

            // Only one mode allowed.
            if (Mode == null || Mode.Length != 1)
                return Result;

            // A label must be supplied.
            if (Label == null || Label.Length == 0)
                return Result;

            if (GetSearchKey(Mode, ref SearchKey))
            {
                HashtableCollection result = GetDBdata(SearchKey, Label);
                Result = CheckResult(SearchKey, result);
            }

            return Result;
        }

        /// <summary>
        /// There is an unknown number of result "rows" in the HashtableCollection
        /// The search result may be in alpha order "ADGKMPR"
        /// But we need to search in SearchKey order ex:"RADKPMG"
        /// so check result one at a time for correct mode and return 1st match only
        /// </summary>
        /// <param name="SearchKey"></param>
        /// <param name="result"></param>
        /// <returns>the "intron" of the first match of SearchKey to HashtableCollection</returns>
        private static string CheckResult(string SearchKey, HashtableCollection result)
        {
            string Intron = string.Empty;
            foreach (byte mode in SearchKey)        // Must search in SearchKey order ex:"RADKPMG"
            {                                       // but the result may be in any order "ADGKMPR"
                foreach (Hashtable row in result)   // so check result one at a time for correct mode.
                {
                    if (mode == ((string)row["Mode"])[0])
                    {
                        Intron = (string)row["Intron"];
                        break;
                    }
                }
                if (string.IsNullOrEmpty(Intron) != true)
                    break;
            }
            return Intron;
        }

        /// <summary>
        /// For any known given Mode there is a specific search system 
        /// </summary>
        /// <param name="Mode"></param>
        /// <param name="SearchKey"></param>
        /// <returns>a list of modes to search in for label to match an intron</returns>
        private static bool GetSearchKey(string Mode, ref string SearchKey)
        {
            bool result = true;
            switch (Mode.ToUpper())
            {
                case "T": SearchKey = "TG"; break;
                case "V": SearchKey = "VG"; break;
                case "K": SearchKey = "KRAMG"; break;
                case "L": SearchKey = "LDRAMG"; break;
                case "J": SearchKey = "JRAMG"; break;
                case "C": SearchKey = "CNG"; break;
                case "S": SearchKey = "SNG"; break;
                case "N": SearchKey = "NCG"; break;
                case "D": SearchKey = "DRAMG"; break;
                case "A": SearchKey = "ARMG"; break;
                case "R": SearchKey = "RADKPMG"; break;
                case "H": SearchKey = "HG"; break;
                case "M": SearchKey = "MARG"; break;
                case "P": SearchKey = "PRAMG"; break;
                case "Y": SearchKey = "YDRAMG"; break;
                case "B": SearchKey = "BNCG"; break;
                default: SearchKey = null; result = false;  break;
            }
            return result;
        }

        /// <summary>
        /// Retrieve the Intron(s) for given Modes and Label
        /// </summary>
        /// <param name="Modes"></param>
        /// <param name="Label"></param>
        /// <returns>a HashtableCollection of Modes, Labels and Introns</returns>
        private static HashtableCollection GetDBdata(string Modes, string Label)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("Modes", Modes);
            paramList.Add("Label", Label);
            HashtableCollection list = DAOFactory.AdHoc().Select("AdHoc.GetIntronData", paramList);
            return list;
        }
    }
}
