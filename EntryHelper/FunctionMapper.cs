using System;
using System.IO;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using ExecLib;
using IDCompare;
using BusinessObject;
using CommonForms;

namespace EntryHelper
{
    /// <summary>
    /// 
    /// </summary>
    public class FunctionMapper
    {
        private IExecsInC ExFuncs;

        /// <summary>
        /// Find a match between the TN Capture (RX) and all ID Data (TX), output in data.
        /// Returns null or an error string.
        /// </summary>
        /// <param name="Path">Full path to the capture file</param>
        /// <param name="File">Capture file for translation and comparison</param>
        /// <param name="ID">ID name for the comparison such as "T0000"</param>
        /// <param name="data">output of actual binary data</param>
        /// <returns>null on success, else an error message is returned</returns>
        /// <notes>
        /// check if path/file exists, if not throw execption
        /// Get ID basic info
        /// Setup ExecsInC with outfile name1, exec and report type
        /// Call Rx for path/file
        ///     Setup ExecsInC with outfile name2, exec and report type
        ///     Set data and Call Tx
        ///     loop till match or end of data
        /// return result 
        /// </notes>
        public string MapFunction(string fullPath, string sourceFile, 
                                    string id, out string data)
        {
            string RxFile = fullPath + "\\" + sourceFile;
            string OutFile1 = Path.GetTempFileName();
            string OutFile2 = Path.GetTempFileName();
            data = null;

            if (File.Exists(RxFile))
            {
                IDHeader IDHeaderData = DAOFactory.IDHeader().Select(id);
                HashtableCollection IDheader 
                    = FunctionList.GetDBdata(id, "AdHoc.GetIDHeader"); // retireve order is important
                HashtableCollection IDdata 
                    = FunctionList.GetDBdata(id, "AdHoc.GetIDData");

                if (IDdata.Count == 0)
                {
                    string msg = "ID {0} has no data or is not checked out.\n";
                    msg += "Try Importing Data or check out the ID.";
                    return string.Format(msg, id);
                }

                ICompareID CfiCmp = new ICompareID();  
                List<int> Prefixes = FunctionList.CollectPrefixes(id, IDheader);
                ExFuncs = new ExecsInC();
                int Exec = IDHeaderData.Executor_Code;
                int DoCompResult = 0;

                FunctionList.ConvertCaptureToCfiFile(OutFile1, RxFile, Exec);

                foreach (Hashtable DataRow in IDdata)
                {
                    if (string.IsNullOrEmpty((string)DataRow["Data"]))
                        continue;
                    FunctionList.CreateTxCfiFile(IDHeaderData, Prefixes, DataRow, OutFile2);
                    CfiCmp.DoCompare(OutFile1, OutFile2, out DoCompResult);
                    if (DoCompResult > 0)
                    {
                        data = (string)DataRow["Data"];
                        break;
                    }
                }
            }
            else
            {
                return string.Format("File {0} was not found in the TN", sourceFile);
            }
            System.IO.File.Delete(OutFile1);
            System.IO.File.Delete(OutFile2);
            return null;
        }
    } 
}
