using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.IO;
using BusinessObject;

namespace EntryHelper
{
	/// <summary>
    /// ImportTnData will search a given .$$$ file for specified label and 
    ///     return data indicated by the specified exec
	/// Use:
    /// Create an instance of ImportTnData giving the Exec and 
    ///     the full path and file name of the .$$$ file
    /// To retrieve data use ImportTnData.GetDataForLabel(string Label) it will
    ///     read the jcode file, find the label and return the needed bytes 
    ///     in string format
    /// 
    /// *** FirstByte and SecondByte should be positive 
    ///     if specified in spec table
    /// </summary>
	public class TNDataImporter
	{
        // Compiler claims that it will init all these to null or 0;
		private FileInfo  JcodeFile;   
		private int Exec;
		private int FirstByte;
		private int SecondByte;

		public TNDataImporter(int exec, string inputFile)
		{
			Exec = exec;
			GetExecByteInfo(ref FirstByte, ref SecondByte);
            JcodeFile = new FileInfo(inputFile);
		}

        public void OverrideExec(int byte1, int byte2)
        {
            FirstByte = byte1;
            SecondByte = byte2;
        }

		/// <summary>
		/// Read the jcode byte positions for the given exec
		/// </summary>
		private void GetExecByteInfo(ref int ByteOne, ref int ByteTwo)
		{
            Hashtable paramList = new Hashtable();

            paramList.Add("Executor_Code", Exec);
            HashtableCollection list = DAOFactory.AdHoc().Select(
                "AdHoc.GetExecSpec", paramList);

            int result;
            if (list.Count > 0)
            {
                Object objFirstByte = list[0]["First_Byte"];
                if(objFirstByte != null && Int32.TryParse(
                    objFirstByte.ToString(), out result) == true)
                {
                    ByteOne = result;
                }
                else
                    ByteOne = 0;

                Object objSecondByte = list[0]["Second_Byte"];
                if (objSecondByte != null && Int32.TryParse(
                    objSecondByte.ToString(), out result) == true)
                {
                    ByteTwo = result;
                }
                else
                    ByteTwo = 0;
            }
		}

		/// <summary>
		/// Read the jcode file, find the label and return the needed bytes
		/// </summary>
		public string GetDataForLabel(string label) 
		{
			string result = null;
			if (!string.IsNullOrEmpty(label))
			{
                StreamReader Jcodestream = JcodeFile.OpenText();

				string JcodeLine = Jcodestream.ReadLine();
				while (JcodeLine != null)
				{
					result = ParseLine(JcodeLine, label);
					if (!string.IsNullOrEmpty(result))
						break;
                    JcodeLine = Jcodestream.ReadLine();
				}

				Jcodestream.Close();
			}
			return result;
		}

        /// <summary>
        /// parse a line of text looking 
        /// for the label and return the wanted byte(s)
        /// 
        /// standardize the label and the line
        /// remove leading/trailing undesirable chars
        /// </summary>
        private string ParseLine(string JcodeLine, string Label)
        {
            string result = null;
            char[] trimchars = { ' ', '\r', '\n' };

            JcodeLine = JcodeLine.Trim(trimchars);

            if (JcodeLine.EndsWith(Label, StringComparison.CurrentCultureIgnoreCase))
            {
                // remove double spaces 
                // this is not really needed 
                // but it helps with the split function
                while (JcodeLine.IndexOf("  ") > 0)
                    JcodeLine = JcodeLine.Replace("  ", " ");

                // split the line into space delimited elements
                char[] delimStr = { ' ' };
                string[] elements = JcodeLine.Split(delimStr);

                if (CheckForMultiWordLabelMatch(elements, Label))
                {
                    // process data for execs that require special handling
                    ExecExceptionProcessing(ref elements);
                    result = RetrieveBytes(elements);
                }
            }

            return result;
        }

        /// <summary>
        /// Match multi word labels with the elements of the given line
        /// </summary>
        /// <param name="Line">The line read from the file broken into word elements</param>
        /// <param name="Label">The label to search for in the line</param>
        /// <returns></returns>
        private static bool CheckForMultiWordLabelMatch(string[] Line, string Label)
        {
            foreach (string element in Line)
            {
                // is it binary data?
                if ("01".Contains(element.Substring(0, 1)) &&
                    element.EndsWith("B", StringComparison.CurrentCultureIgnoreCase))
                    continue;
                // is it all or part of the label?
                if (!Label.Contains(element))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Get ALL labels and data from the .$$$ file and return them in a table
        /// </summary>
        /// <returns></returns>
        public HashtableCollection GetAllLabelsAndData()
        {
            HashtableCollection table = new HashtableCollection();
            string label = null;
            string data  = null;

            StreamReader Jcodestream = JcodeFile.OpenText();

            string JcodeLine = Jcodestream.ReadLine();
            while (JcodeLine != null)
            {
                label = null;
                data = null;
                ParseLine(JcodeLine, ref label, ref data);
                if (!string.IsNullOrEmpty(data) && !string.IsNullOrEmpty(label))
                {
                    Hashtable paramList = new Hashtable();
                    paramList.Add("Label", label);
                    paramList.Add("Data", data);
                    table.Add(paramList);
                }
                JcodeLine = Jcodestream.ReadLine();
            }
            Jcodestream.Close();

            return table;
        }

        /// <summary>
        /// Same as ParseLine but returns Label as well as Data
        /// </summary>
        /// <param name="JcodeLine"></param>
        /// <param name="Label"></param>
        /// <param name="Data"></param>
        void ParseLine(string JcodeLine, ref string Label, ref string Data)
        {
            char[] trimchars = { ' ', '\r', '\n' };
            if (string.IsNullOrEmpty(JcodeLine))
                return;

            JcodeLine = JcodeLine.Trim(trimchars);

            // remove double spaces 
            // this is not really needed 
            // but it helps with the split function
            while (JcodeLine.IndexOf("  ") > 0)
                JcodeLine = JcodeLine.Replace("  ", " ");

            // split the line into space delimited elements
            char[] delimStr = { ' ' };
            string[] elements = JcodeLine.Split(delimStr);

            // if the line starts with a binary string "bbbbbbbbB" it's a valid line
            if ("01".Contains(elements[0].Substring(0,1)) && 
                elements[0].EndsWith("B",StringComparison.CurrentCultureIgnoreCase))
            {
                foreach (string element in elements)
                {
                    // if the element isn't a binary string than it must be part of a label
                    if (!"01".Contains(element.Substring(0,1)) || 
                        !element.EndsWith("B", StringComparison.CurrentCultureIgnoreCase))
                        Label += element + " ";
                }
                // process data for execs that require special handling
                ExecExceptionProcessing(ref elements);
                Data = RetrieveBytes(elements);
            }
        }

        /// <summary>
        /// Get the element at the first byte postion
        /// if there is there a second byte position get that one too.
        /// </summary>
		private string RetrieveBytes(string[] elements)
		{
			string result = null;

            // Get the element at the first byte postion
            // the byte position is 1 based 
            // so subtract 1 for the 0 based array
			if (FirstByte > 0)
			{
				result = FormatByte(elements[FirstByte-1]);

				// is there a second byte position?  get that one.
				if (SecondByte > 0)
					result += FormatByte(elements[SecondByte-1]);
			}
			else
			{
				result = FormatByte(elements[0]);
			}
			return result;
		}

		/// <summary>
		/// Strip extra chars and make sure it's 8 bits long
        /// try to be sure we have a binary number
        /// remove all spaces and trailing 'B'
        /// make sure it's at least 8 bits long
		/// </summary>
		private static string FormatByte(string Source)
		{
            if (!string.IsNullOrEmpty(Source))
			{
				if (!Source.EndsWith("B", StringComparison.CurrentCultureIgnoreCase))			
					return null;

				char[] trimchars = {' ', 'B'};		
				Source = Source.Trim(trimchars);	
				Source = Source.PadRight(8, '0');	
			}
			return Source;
		}

		/// <summary>
		/// Process execs that require special handling
        /// special handling for 232
        ///     requires first 2 bits from byte 0 prefixed to 6 bits of byte 1
		/// </summary>
		private void ExecExceptionProcessing(ref string[] elements)
		{
			switch(Exec)
			{
				case 232:	
					if (elements[1].Length == 6)
						elements[1] = elements[0].Substring(0, 2) 
                            + elements[1];
					break;
				default:
					break;
			}
		}

	}
}
