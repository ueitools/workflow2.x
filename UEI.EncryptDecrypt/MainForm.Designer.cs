namespace UEI.EncryptDecrypt
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lbltoolStripStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkType3 = new System.Windows.Forms.CheckBox();
            this.chkType4 = new System.Windows.Forms.CheckBox();
            this.txtDestinationFile = new System.Windows.Forms.TextBox();
            this.txtSourceFile = new System.Windows.Forms.TextBox();
            this.txtBlockEndAddress = new System.Windows.Forms.TextBox();
            this.txtBlockStartAddress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnFileUnpackage = new System.Windows.Forms.Button();
            this.btnFilePackage = new System.Windows.Forms.Button();
            this.btnFileDecrypt = new System.Windows.Forms.Button();
            this.btnFileEncrypt = new System.Windows.Forms.Button();
            this.btnDecrypt = new System.Windows.Forms.Button();
            this.btnEncrypt = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtCypherText = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtPlainText = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtEncryptStatus = new System.Windows.Forms.TextBox();
            this.txtCypherKey = new System.Windows.Forms.TextBox();
            this.btnGenerateKEK = new System.Windows.Forms.Button();
            this.btnRandomKey = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbltoolStripStatus});
            this.statusStrip.Location = new System.Drawing.Point(0, 514);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip.Size = new System.Drawing.Size(807, 25);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "statusStrip1";
            // 
            // lbltoolStripStatus
            // 
            this.lbltoolStripStatus.Name = "lbltoolStripStatus";
            this.lbltoolStripStatus.Size = new System.Drawing.Size(45, 20);
            this.lbltoolStripStatus.Text = "Done";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(807, 514);
            this.panel1.TabIndex = 1;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chkType3);
            this.groupBox4.Controls.Add(this.chkType4);
            this.groupBox4.Controls.Add(this.txtDestinationFile);
            this.groupBox4.Controls.Add(this.txtSourceFile);
            this.groupBox4.Controls.Add(this.txtBlockEndAddress);
            this.groupBox4.Controls.Add(this.txtBlockStartAddress);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.btnFileUnpackage);
            this.groupBox4.Controls.Add(this.btnFilePackage);
            this.groupBox4.Controls.Add(this.btnFileDecrypt);
            this.groupBox4.Controls.Add(this.btnFileEncrypt);
            this.groupBox4.Controls.Add(this.btnDecrypt);
            this.groupBox4.Controls.Add(this.btnEncrypt);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 344);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Size = new System.Drawing.Size(807, 170);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            // 
            // chkType3
            // 
            this.chkType3.AutoSize = true;
            this.chkType3.Checked = true;
            this.chkType3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkType3.Location = new System.Drawing.Point(715, 78);
            this.chkType3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkType3.Name = "chkType3";
            this.chkType3.Size = new System.Drawing.Size(75, 21);
            this.chkType3.TabIndex = 15;
            this.chkType3.Text = "Type-3";
            this.chkType3.UseVisualStyleBackColor = true;
            // 
            // chkType4
            // 
            this.chkType4.AutoSize = true;
            this.chkType4.Location = new System.Drawing.Point(628, 78);
            this.chkType4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkType4.Name = "chkType4";
            this.chkType4.Size = new System.Drawing.Size(75, 21);
            this.chkType4.TabIndex = 14;
            this.chkType4.Text = "Type-4";
            this.chkType4.UseVisualStyleBackColor = true;
            // 
            // txtDestinationFile
            // 
            this.txtDestinationFile.BackColor = System.Drawing.Color.White;
            this.txtDestinationFile.Location = new System.Drawing.Point(21, 137);
            this.txtDestinationFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDestinationFile.Name = "txtDestinationFile";
            this.txtDestinationFile.ReadOnly = true;
            this.txtDestinationFile.Size = new System.Drawing.Size(772, 22);
            this.txtDestinationFile.TabIndex = 13;
            // 
            // txtSourceFile
            // 
            this.txtSourceFile.BackColor = System.Drawing.Color.White;
            this.txtSourceFile.Location = new System.Drawing.Point(21, 102);
            this.txtSourceFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSourceFile.Name = "txtSourceFile";
            this.txtSourceFile.ReadOnly = true;
            this.txtSourceFile.Size = new System.Drawing.Size(772, 22);
            this.txtSourceFile.TabIndex = 12;
            // 
            // txtBlockEndAddress
            // 
            this.txtBlockEndAddress.Location = new System.Drawing.Point(628, 44);
            this.txtBlockEndAddress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBlockEndAddress.Name = "txtBlockEndAddress";
            this.txtBlockEndAddress.Size = new System.Drawing.Size(161, 22);
            this.txtBlockEndAddress.TabIndex = 11;
            this.txtBlockEndAddress.Text = "0xFFFF";
            // 
            // txtBlockStartAddress
            // 
            this.txtBlockStartAddress.Location = new System.Drawing.Point(628, 15);
            this.txtBlockStartAddress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBlockStartAddress.Name = "txtBlockStartAddress";
            this.txtBlockStartAddress.Size = new System.Drawing.Size(161, 22);
            this.txtBlockStartAddress.TabIndex = 10;
            this.txtBlockStartAddress.Text = "0x1400";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(549, 53);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Block End:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(549, 23);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Block Start:";
            // 
            // btnFileUnpackage
            // 
            this.btnFileUnpackage.Location = new System.Drawing.Point(417, 53);
            this.btnFileUnpackage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFileUnpackage.Name = "btnFileUnpackage";
            this.btnFileUnpackage.Size = new System.Drawing.Size(127, 28);
            this.btnFileUnpackage.TabIndex = 5;
            this.btnFileUnpackage.Text = "Unpackage File";
            this.btnFileUnpackage.UseVisualStyleBackColor = true;
            // 
            // btnFilePackage
            // 
            this.btnFilePackage.Location = new System.Drawing.Point(289, 53);
            this.btnFilePackage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFilePackage.Name = "btnFilePackage";
            this.btnFilePackage.Size = new System.Drawing.Size(120, 28);
            this.btnFilePackage.TabIndex = 4;
            this.btnFilePackage.Text = "Package File";
            this.btnFilePackage.UseVisualStyleBackColor = true;
            // 
            // btnFileDecrypt
            // 
            this.btnFileDecrypt.Location = new System.Drawing.Point(131, 53);
            this.btnFileDecrypt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFileDecrypt.Name = "btnFileDecrypt";
            this.btnFileDecrypt.Size = new System.Drawing.Size(100, 28);
            this.btnFileDecrypt.TabIndex = 3;
            this.btnFileDecrypt.Text = "Decrypt File";
            this.btnFileDecrypt.UseVisualStyleBackColor = true;
            // 
            // btnFileEncrypt
            // 
            this.btnFileEncrypt.Location = new System.Drawing.Point(17, 53);
            this.btnFileEncrypt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFileEncrypt.Name = "btnFileEncrypt";
            this.btnFileEncrypt.Size = new System.Drawing.Size(100, 28);
            this.btnFileEncrypt.TabIndex = 2;
            this.btnFileEncrypt.Text = "Encrypt File";
            this.btnFileEncrypt.UseVisualStyleBackColor = true;
            // 
            // btnDecrypt
            // 
            this.btnDecrypt.Location = new System.Drawing.Point(131, 17);
            this.btnDecrypt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDecrypt.Name = "btnDecrypt";
            this.btnDecrypt.Size = new System.Drawing.Size(100, 28);
            this.btnDecrypt.TabIndex = 1;
            this.btnDecrypt.Text = "Decrypt";
            this.btnDecrypt.UseVisualStyleBackColor = true;
            // 
            // btnEncrypt
            // 
            this.btnEncrypt.Location = new System.Drawing.Point(17, 17);
            this.btnEncrypt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEncrypt.Name = "btnEncrypt";
            this.btnEncrypt.Size = new System.Drawing.Size(100, 28);
            this.btnEncrypt.TabIndex = 0;
            this.btnEncrypt.Text = "Encrypt";
            this.btnEncrypt.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtCypherText);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 196);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(807, 148);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Cypher Text";
            // 
            // txtCypherText
            // 
            this.txtCypherText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCypherText.Location = new System.Drawing.Point(4, 19);
            this.txtCypherText.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCypherText.Multiline = true;
            this.txtCypherText.Name = "txtCypherText";
            this.txtCypherText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtCypherText.Size = new System.Drawing.Size(799, 125);
            this.txtCypherText.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtPlainText);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 48);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(807, 148);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Plain Text";
            // 
            // txtPlainText
            // 
            this.txtPlainText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPlainText.Location = new System.Drawing.Point(4, 19);
            this.txtPlainText.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPlainText.Multiline = true;
            this.txtPlainText.Name = "txtPlainText";
            this.txtPlainText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtPlainText.Size = new System.Drawing.Size(799, 125);
            this.txtPlainText.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtEncryptStatus);
            this.groupBox1.Controls.Add(this.txtCypherKey);
            this.groupBox1.Controls.Add(this.btnGenerateKEK);
            this.groupBox1.Controls.Add(this.btnRandomKey);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(807, 48);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // txtEncryptStatus
            // 
            this.txtEncryptStatus.Location = new System.Drawing.Point(605, 16);
            this.txtEncryptStatus.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEncryptStatus.Name = "txtEncryptStatus";
            this.txtEncryptStatus.Size = new System.Drawing.Size(65, 22);
            this.txtEncryptStatus.TabIndex = 5;
            this.txtEncryptStatus.Text = "0x00";
            // 
            // txtCypherKey
            // 
            this.txtCypherKey.Location = new System.Drawing.Point(59, 14);
            this.txtCypherKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCypherKey.Name = "txtCypherKey";
            this.txtCypherKey.Size = new System.Drawing.Size(349, 22);
            this.txtCypherKey.TabIndex = 4;
            this.txtCypherKey.Text = "0+KxZC7GCsfbN4siS10BUQ==";
            // 
            // btnGenerateKEK
            // 
            this.btnGenerateKEK.Location = new System.Drawing.Point(680, 14);
            this.btnGenerateKEK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGenerateKEK.Name = "btnGenerateKEK";
            this.btnGenerateKEK.Size = new System.Drawing.Size(113, 28);
            this.btnGenerateKEK.TabIndex = 3;
            this.btnGenerateKEK.Text = "Generate KEK";
            this.btnGenerateKEK.UseVisualStyleBackColor = true;
            // 
            // btnRandomKey
            // 
            this.btnRandomKey.Location = new System.Drawing.Point(417, 12);
            this.btnRandomKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRandomKey.Name = "btnRandomKey";
            this.btnRandomKey.Size = new System.Drawing.Size(113, 28);
            this.btnRandomKey.TabIndex = 2;
            this.btnRandomKey.Text = "Randomize";
            this.btnRandomKey.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(548, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Status";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Key";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(807, 539);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Encrypt and Decrypt Utility";
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lbltoolStripStatus;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtEncryptStatus;
        private System.Windows.Forms.TextBox txtCypherKey;
        private System.Windows.Forms.Button btnGenerateKEK;
        private System.Windows.Forms.Button btnRandomKey;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtCypherText;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtPlainText;
        private System.Windows.Forms.TextBox txtBlockEndAddress;
        private System.Windows.Forms.TextBox txtBlockStartAddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnFileUnpackage;
        private System.Windows.Forms.Button btnFilePackage;
        private System.Windows.Forms.Button btnFileDecrypt;
        private System.Windows.Forms.Button btnFileEncrypt;
        private System.Windows.Forms.Button btnDecrypt;
        private System.Windows.Forms.Button btnEncrypt;
        private System.Windows.Forms.TextBox txtSourceFile;
        private System.Windows.Forms.TextBox txtDestinationFile;
        private System.Windows.Forms.CheckBox chkType3;
        private System.Windows.Forms.CheckBox chkType4;
    }
}