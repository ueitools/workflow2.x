﻿namespace UEI.EncryptDecrypt
{
    partial class AutomateEncryptDecrypt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutomateEncryptDecrypt));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtEncryptStatus = new System.Windows.Forms.TextBox();
            this.txtCypherKey = new System.Windows.Forms.TextBox();
            this.btnGenerateKEK = new System.Windows.Forms.Button();
            this.btnRandomKey = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnBrowseFilePath = new System.Windows.Forms.Button();
            this.txtOutputFileName = new System.Windows.Forms.TextBox();
            this.txtOutputFilePath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPageSize = new System.Windows.Forms.TextBox();
            this.txtHexFile = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.chkType3 = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chkType4 = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBlockEndAddress = new System.Windows.Forms.TextBox();
            this.txtBlockStartAddress = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtDestinationFile = new System.Windows.Forms.TextBox();
            this.btnFileUnpackage = new System.Windows.Forms.Button();
            this.btnFilePackage = new System.Windows.Forms.Button();
            this.etchedLine1 = new UEI.EncryptDecrypt.EtchedLine();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtEncryptStatus);
            this.groupBox1.Controls.Add(this.txtCypherKey);
            this.groupBox1.Controls.Add(this.btnGenerateKEK);
            this.groupBox1.Controls.Add(this.btnRandomKey);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(800, 65);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Encryption and Decryption Key";
            // 
            // txtEncryptStatus
            // 
            this.txtEncryptStatus.Location = new System.Drawing.Point(605, 25);
            this.txtEncryptStatus.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEncryptStatus.Name = "txtEncryptStatus";
            this.txtEncryptStatus.Size = new System.Drawing.Size(65, 22);
            this.txtEncryptStatus.TabIndex = 11;
            this.txtEncryptStatus.Text = "0x00";
            // 
            // txtCypherKey
            // 
            this.txtCypherKey.Location = new System.Drawing.Point(59, 22);
            this.txtCypherKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCypherKey.Name = "txtCypherKey";
            this.txtCypherKey.Size = new System.Drawing.Size(349, 22);
            this.txtCypherKey.TabIndex = 10;
            this.txtCypherKey.Text = "0+KxZC7GCsfbN4siS10BUQ==";
            // 
            // btnGenerateKEK
            // 
            this.btnGenerateKEK.Location = new System.Drawing.Point(680, 22);
            this.btnGenerateKEK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGenerateKEK.Name = "btnGenerateKEK";
            this.btnGenerateKEK.Size = new System.Drawing.Size(113, 28);
            this.btnGenerateKEK.TabIndex = 9;
            this.btnGenerateKEK.Text = "Generate KEK";
            this.btnGenerateKEK.UseVisualStyleBackColor = true;
            // 
            // btnRandomKey
            // 
            this.btnRandomKey.Location = new System.Drawing.Point(417, 21);
            this.btnRandomKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRandomKey.Name = "btnRandomKey";
            this.btnRandomKey.Size = new System.Drawing.Size(113, 28);
            this.btnRandomKey.TabIndex = 8;
            this.btnRandomKey.Text = "Randomize";
            this.btnRandomKey.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(548, 31);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 17);
            this.label11.TabIndex = 7;
            this.label11.Text = "Status";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 31);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 17);
            this.label12.TabIndex = 6;
            this.label12.Text = "Key";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnBrowseFilePath);
            this.groupBox2.Controls.Add(this.txtOutputFileName);
            this.groupBox2.Controls.Add(this.txtOutputFilePath);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtPageSize);
            this.groupBox2.Controls.Add(this.txtHexFile);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.btnBrowse);
            this.groupBox2.Controls.Add(this.chkType3);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.chkType4);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtBlockEndAddress);
            this.groupBox2.Controls.Add(this.txtBlockStartAddress);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 65);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(800, 204);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Input:";
            // 
            // btnBrowseFilePath
            // 
            this.btnBrowseFilePath.Location = new System.Drawing.Point(539, 46);
            this.btnBrowseFilePath.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBrowseFilePath.Name = "btnBrowseFilePath";
            this.btnBrowseFilePath.Size = new System.Drawing.Size(84, 28);
            this.btnBrowseFilePath.TabIndex = 28;
            this.btnBrowseFilePath.Text = "Browse";
            this.btnBrowseFilePath.UseVisualStyleBackColor = true;
            // 
            // txtOutputFileName
            // 
            this.txtOutputFileName.BackColor = System.Drawing.Color.White;
            this.txtOutputFileName.Location = new System.Drawing.Point(199, 16);
            this.txtOutputFileName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtOutputFileName.Name = "txtOutputFileName";
            this.txtOutputFileName.Size = new System.Drawing.Size(331, 22);
            this.txtOutputFileName.TabIndex = 27;
            // 
            // txtOutputFilePath
            // 
            this.txtOutputFilePath.BackColor = System.Drawing.Color.White;
            this.txtOutputFilePath.Location = new System.Drawing.Point(199, 48);
            this.txtOutputFilePath.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtOutputFilePath.Name = "txtOutputFilePath";
            this.txtOutputFilePath.ReadOnly = true;
            this.txtOutputFilePath.Size = new System.Drawing.Size(331, 22);
            this.txtOutputFilePath.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 25);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 17);
            this.label2.TabIndex = 25;
            this.label2.Text = "Enter Output File Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 57);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 17);
            this.label1.TabIndex = 24;
            this.label1.Text = "Select Output File Path:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(39, 89);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Select the Hex File: ";
            // 
            // txtPageSize
            // 
            this.txtPageSize.Location = new System.Drawing.Point(199, 156);
            this.txtPageSize.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPageSize.MaxLength = 10;
            this.txtPageSize.Name = "txtPageSize";
            this.txtPageSize.Size = new System.Drawing.Size(161, 22);
            this.txtPageSize.TabIndex = 23;
            this.txtPageSize.Text = "512";
            // 
            // txtHexFile
            // 
            this.txtHexFile.BackColor = System.Drawing.Color.White;
            this.txtHexFile.Location = new System.Drawing.Point(199, 80);
            this.txtHexFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtHexFile.Name = "txtHexFile";
            this.txtHexFile.ReadOnly = true;
            this.txtHexFile.Size = new System.Drawing.Size(331, 22);
            this.txtHexFile.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(93, 165);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 17);
            this.label10.TabIndex = 22;
            this.label10.Text = "Page Size: ";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(539, 78);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(84, 28);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            // 
            // chkType3
            // 
            this.chkType3.AutoSize = true;
            this.chkType3.Checked = true;
            this.chkType3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkType3.Location = new System.Drawing.Point(547, 165);
            this.chkType3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkType3.Name = "chkType3";
            this.chkType3.Size = new System.Drawing.Size(75, 21);
            this.chkType3.TabIndex = 21;
            this.chkType3.Text = "Type-3";
            this.chkType3.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(88, 124);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 17);
            this.label9.TabIndex = 16;
            this.label9.Text = "Block Start: ";
            // 
            // chkType4
            // 
            this.chkType4.AutoSize = true;
            this.chkType4.Location = new System.Drawing.Point(460, 165);
            this.chkType4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkType4.Name = "chkType4";
            this.chkType4.Size = new System.Drawing.Size(75, 21);
            this.chkType4.TabIndex = 20;
            this.chkType4.Text = "Type-4";
            this.chkType4.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(373, 124);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "Block End: ";
            // 
            // txtBlockEndAddress
            // 
            this.txtBlockEndAddress.Location = new System.Drawing.Point(460, 116);
            this.txtBlockEndAddress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBlockEndAddress.Name = "txtBlockEndAddress";
            this.txtBlockEndAddress.Size = new System.Drawing.Size(161, 22);
            this.txtBlockEndAddress.TabIndex = 19;
            this.txtBlockEndAddress.Text = "0xFFFF";
            // 
            // txtBlockStartAddress
            // 
            this.txtBlockStartAddress.Location = new System.Drawing.Point(199, 116);
            this.txtBlockStartAddress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBlockStartAddress.Name = "txtBlockStartAddress";
            this.txtBlockStartAddress.Size = new System.Drawing.Size(161, 22);
            this.txtBlockStartAddress.TabIndex = 18;
            this.txtBlockStartAddress.Text = "0x1400";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtDestinationFile);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 269);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(800, 75);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Output:";
            // 
            // txtDestinationFile
            // 
            this.txtDestinationFile.BackColor = System.Drawing.Color.White;
            this.txtDestinationFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDestinationFile.Location = new System.Drawing.Point(4, 19);
            this.txtDestinationFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDestinationFile.Multiline = true;
            this.txtDestinationFile.Name = "txtDestinationFile";
            this.txtDestinationFile.ReadOnly = true;
            this.txtDestinationFile.Size = new System.Drawing.Size(792, 52);
            this.txtDestinationFile.TabIndex = 14;
            // 
            // btnFileUnpackage
            // 
            this.btnFileUnpackage.Location = new System.Drawing.Point(539, 352);
            this.btnFileUnpackage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFileUnpackage.Name = "btnFileUnpackage";
            this.btnFileUnpackage.Size = new System.Drawing.Size(127, 28);
            this.btnFileUnpackage.TabIndex = 27;
            this.btnFileUnpackage.Text = "Unpackage File";
            this.btnFileUnpackage.UseVisualStyleBackColor = true;
            this.btnFileUnpackage.Visible = false;
            // 
            // btnFilePackage
            // 
            this.btnFilePackage.Location = new System.Drawing.Point(673, 352);
            this.btnFilePackage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFilePackage.Name = "btnFilePackage";
            this.btnFilePackage.Size = new System.Drawing.Size(120, 28);
            this.btnFilePackage.TabIndex = 28;
            this.btnFilePackage.Text = "Package File";
            this.btnFilePackage.UseVisualStyleBackColor = true;
            // 
            // etchedLine1
            // 
            this.etchedLine1.BackColor = System.Drawing.SystemColors.Control;
            this.etchedLine1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.etchedLine1.Edge = UEI.EncryptDecrypt.EtchEdge.Top;
            this.etchedLine1.Location = new System.Drawing.Point(0, 344);
            this.etchedLine1.Margin = new System.Windows.Forms.Padding(0);
            this.etchedLine1.Name = "etchedLine1";
            this.etchedLine1.Size = new System.Drawing.Size(800, 41);
            this.etchedLine1.TabIndex = 3;
            // 
            // AutomateEncryptDecrypt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(800, 385);
            this.Controls.Add(this.btnFileUnpackage);
            this.Controls.Add(this.btnFilePackage);
            this.Controls.Add(this.etchedLine1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "AutomateEncryptDecrypt";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Automate Encrypt and Decrypt";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtHexFile;
        private System.Windows.Forms.TextBox txtPageSize;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkType3;
        private System.Windows.Forms.CheckBox chkType4;
        private System.Windows.Forms.TextBox txtBlockEndAddress;
        private System.Windows.Forms.TextBox txtBlockStartAddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtEncryptStatus;
        private System.Windows.Forms.TextBox txtCypherKey;
        private System.Windows.Forms.Button btnGenerateKEK;
        private System.Windows.Forms.Button btnRandomKey;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtDestinationFile;
        private System.Windows.Forms.Button btnFilePackage;
        private System.Windows.Forms.Button btnFileUnpackage;
        private EtchedLine etchedLine1;
        private System.Windows.Forms.Button btnBrowseFilePath;
        private System.Windows.Forms.TextBox txtOutputFileName;
        private System.Windows.Forms.TextBox txtOutputFilePath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}