﻿using CommonForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace UEI.EncryptDecrypt
{
    public partial class AutomateEncryptDecrypt : Form
    {
        #region Variables
        private EncyptionDecryption m_EncryptDecrypt = null;
        private BackgroundWorker m_BackGroundWorker = null;
        private ProgressForm m_Progress = null;
        private EncryptDecryptTypes m_EncryptDecryptType = EncryptDecryptTypes.Encrypt;
        #endregion

        #region Properties
        public String FilePath { get; set; }
        public String FileName { get; set; }
        public String FileNameWithoutExt { get; set; }
        public String FileExtension { get; set; }
        #endregion

        #region Constructor
        public AutomateEncryptDecrypt()
        {
            InitializeComponent();
            m_BackGroundWorker = new BackgroundWorker();
            m_BackGroundWorker.DoWork += new DoWorkEventHandler(m_BackGroundWorker_DoWork);
            m_BackGroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(m_BackGroundWorker_RunWorkerCompleted);
            m_EncryptDecrypt = new EncyptionDecryption();
            this.btnRandomKey.Click += new EventHandler(btnRandomKey_Click);
            this.btnGenerateKEK.Click += new EventHandler(btnGenerateKEK_Click);
            this.Load += AutomateEncryptDecrypt_Load;
            this.btnBrowse.Click += btnBrowse_Click;
            this.btnFilePackage.Click += btnFilePackage_Click;
            this.btnFileUnpackage.Click += btnFileUnpackage_Click;
            this.txtPageSize.KeyPress += txtPageSize_KeyPress;
            this.chkType4.CheckedChanged += new EventHandler(chkType4_CheckedChanged);
            this.chkType3.CheckedChanged += new EventHandler(chkType3_CheckedChanged);
            //Updated New Features
            this.btnBrowseFilePath.Click += btnBrowseFilePath_Click;
            this.txtOutputFileName.Leave += txtOutputFileName_Leave;
        }                      
        void AutomateEncryptDecrypt_Load(object sender, EventArgs e)
        {
            CheckWorkingDirectory();
        }
        #endregion

        #region Generate Random Key
        void btnRandomKey_Click(object sender, EventArgs e)
        {
            this.m_EncryptDecrypt.GenerateRandomKey();
            txtCypherKey.Text = this.m_EncryptDecrypt.RandomKey;
        }
        #endregion

        #region Generate KEK
        void btnGenerateKEK_Click(object sender, EventArgs e)
        {
            this.m_EncryptDecrypt.GenerateKEK();
            if (this.m_EncryptDecrypt.SavedFile != String.Empty)
            {
                MessageBox.Show("Generate KEK Saved to Path\n" + this.m_EncryptDecrypt.SavedFile, "Alert!", MessageBoxButtons.OK);
            }
        }
        #endregion

        #region Check Box Handling
        void chkType3_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkType3.Checked)
            {
                this.chkType4.Checked = false;
            }
        }
        void chkType4_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkType4.Checked)
            {
                this.chkType3.Checked = false;
            }
        }
        #endregion

        #region Back Ground Worker
        void m_BackGroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_Progress.DialogResult = DialogResult.OK;
            m_Progress.Close();
            switch (m_EncryptDecryptType)
            {
                case EncryptDecryptTypes.Encrypt:
                    //this.txtCypherText.Text = this.m_EncryptDecrypt.CypherText;
                    break;
                case EncryptDecryptTypes.Decrypt:
                    //this.txtPlainText.Text = this.m_EncryptDecrypt.PlainText;
                    break;
                case EncryptDecryptTypes.EncryptFile:
                    break;
                case EncryptDecryptTypes.DecryptFile:
                    break;
                case EncryptDecryptTypes.PackageFile:
                    break;
                case EncryptDecryptTypes.UnPackageFile:
                    break;
                default:
                    break;
            }
        }
        void m_BackGroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (m_EncryptDecryptType)
            {
                case EncryptDecryptTypes.Encrypt:
                    this.m_EncryptDecrypt.Encrypt();
                    break;
                case EncryptDecryptTypes.Decrypt:
                    this.m_EncryptDecrypt.Decrypt();
                    break;
                case EncryptDecryptTypes.EncryptFile:
                    this.m_EncryptDecrypt.FileEncrypt();
                    break;
                case EncryptDecryptTypes.DecryptFile:
                    this.m_EncryptDecrypt.FileDecrypt();
                    break;
                case EncryptDecryptTypes.PackageFile:
                    this.m_EncryptDecrypt.FilePackage_Automate();
                    break;
                case EncryptDecryptTypes.UnPackageFile:
                    this.m_EncryptDecrypt.FileUnPackage_Automate();
                    break;
                default:
                    break;
            }
        }
        private void ResetTimer()
        {
            if (m_BackGroundWorker != null || m_BackGroundWorker.IsBusy == false)
                m_BackGroundWorker.RunWorkerAsync();

            if (m_Progress == null)
            {
                m_Progress = new ProgressForm();
                m_Progress.StartPosition = FormStartPosition.CenterParent;
            }
            m_Progress.SetMessage("Please Wait...");
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();
        }
        #endregion

        #region Methods
        void txtPageSize_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validate(e);
        }
        void Validate(KeyPressEventArgs E)
        {
            if (!char.IsNumber(E.KeyChar) & (Keys)E.KeyChar != Keys.Back & E.KeyChar != '.')
            {
                E.Handled = true;
            }
        }
        void btnFilePackage_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtCypherKey.Text))
            {
                if (!String.IsNullOrEmpty(txtHexFile.Text))
                {
                    if (!Directory.Exists(this.txtOutputFilePath.Text.Trim()))
                    {
                        MessageBox.Show("Seleced Output File Path doest not Exists", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.txtOutputFilePath.Focus();
                    }
                    else if (String.IsNullOrEmpty(txtOutputFileName.Text))
                    {
                        MessageBox.Show("Please enter output File Name", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.txtOutputFileName.Focus();
                    }
                    else if (String.IsNullOrEmpty(txtBlockStartAddress.Text))
                    {
                        MessageBox.Show("Please enter Block Start Address", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.txtBlockStartAddress.Focus();
                    }
                    else if (String.IsNullOrEmpty(txtBlockEndAddress.Text))
                    {
                        MessageBox.Show("Please enter Block End Address", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.txtBlockEndAddress.Focus();
                    }
                    else if (String.IsNullOrEmpty(txtPageSize.Text))
                    {
                        MessageBox.Show("Please enter Page Size", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.txtPageSize.Focus();
                    }                    
                    else
                    {
                        //Convert Hex to Bin using hex2bin.exe
                        CallHex2Bin();
                        String m_BinFileName = this.FilePath + this.FileNameWithoutExt + ".bin";
                        try
                        {
                            if (File.Exists(m_BinFileName))
                            {
                                Int32 m_Numberofpages = H2BUtility.ProcessBinFile(txtBlockStartAddress.Text, txtBlockEndAddress.Text, int.Parse(txtPageSize.Text), m_BinFileName);

                                //Encrypt bin File
                                if (chkType3.Checked)
                                {
                                    this.m_EncryptDecrypt.Type = PackageType.Type3;
                                    this.txtDestinationFile.Text = "";
                                    try
                                    {
                                        this.m_EncryptDecrypt.CypherKey = txtCypherKey.Text;
                                        this.m_EncryptDecrypt.BlockStartAddress = txtBlockStartAddress.Text;
                                        this.m_EncryptDecrypt.BlockEndAddress = txtBlockEndAddress.Text;
                                        this.m_EncryptDecrypt.SourceContent = File.ReadAllBytes(m_BinFileName);
                                        this.m_EncryptDecrypt.PageSize = Int32.Parse(txtPageSize.Text);
                                        this.m_EncryptDecryptType = EncryptDecryptTypes.PackageFile;
                                        ResetTimer();
                                        if (this.m_EncryptDecrypt.ErrorMessage != String.Empty)
                                        {
                                            MessageBox.Show(this.m_EncryptDecrypt.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                        else
                                        {
                                            //Insert two bytes of data at begin
                                            this.m_EncryptDecrypt.DestinationContent = H2BUtility.InsertSize(this.m_EncryptDecrypt.DestinationContent, m_Numberofpages);
                                            this.txtDestinationFile.Text = m_BinFileName + this.m_EncryptDecrypt.FileExtension;
                                            File.WriteAllBytes(this.txtDestinationFile.Text, this.m_EncryptDecrypt.DestinationContent);
                                            this.txtDestinationFile.Text = "Package File :" + this.txtDestinationFile.Text;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                                else if (chkType4.Checked)
                                {
                                    this.m_EncryptDecrypt.Type = PackageType.Type4;
                                    this.txtDestinationFile.Text = "";
                                    try
                                    {
                                        this.m_EncryptDecrypt.CypherKey = txtCypherKey.Text;
                                        this.m_EncryptDecrypt.SourceContent = File.ReadAllBytes(m_BinFileName);
                                        this.m_EncryptDecryptType = EncryptDecryptTypes.PackageFile;
                                        this.m_EncryptDecrypt.PageSize = Int32.Parse(txtPageSize.Text);
                                        ResetTimer();
                                        if (this.m_EncryptDecrypt.ErrorMessage != String.Empty)
                                        {
                                            MessageBox.Show(this.m_EncryptDecrypt.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                        else
                                        {
                                            //Insert two bytes of data at begin
                                            this.m_EncryptDecrypt.DestinationContent = H2BUtility.InsertSize(this.m_EncryptDecrypt.DestinationContent, m_Numberofpages);

                                            this.txtDestinationFile.Text = m_BinFileName + this.m_EncryptDecrypt.FileExtension;
                                            File.WriteAllBytes(this.txtDestinationFile.Text, this.m_EncryptDecrypt.DestinationContent);
                                            this.txtDestinationFile.Text = "Package File :" + this.txtDestinationFile.Text;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                                else
                                {
                                    this.m_EncryptDecrypt.Type = PackageType.Unkown;
                                    if (txtEncryptStatus.Text.Trim() == String.Empty)
                                    {
                                        MessageBox.Show("Status Address Can't be Empty", "Invalid", MessageBoxButtons.OK);
                                        txtEncryptStatus.Focus();
                                    }
                                    else
                                    {
                                        this.txtDestinationFile.Text = "";
                                        try
                                        {
                                            this.m_EncryptDecrypt.CypherKey = txtCypherKey.Text;
                                            this.m_EncryptDecrypt.EncryptStatusAddress = txtEncryptStatus.Text;
                                            this.m_EncryptDecrypt.SourceContent = File.ReadAllBytes(m_BinFileName);
                                            this.m_EncryptDecrypt.PageSize = Int32.Parse(txtPageSize.Text);
                                            this.m_EncryptDecryptType = EncryptDecryptTypes.PackageFile;
                                            ResetTimer();
                                            if (this.m_EncryptDecrypt.ErrorMessage != String.Empty)
                                            {
                                                MessageBox.Show(this.m_EncryptDecrypt.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            }
                                            else
                                            {
                                                //Insert two bytes of data at begin
                                                this.m_EncryptDecrypt.DestinationContent = H2BUtility.InsertSize(this.m_EncryptDecrypt.DestinationContent, m_Numberofpages);

                                                this.txtDestinationFile.Text = m_BinFileName + this.m_EncryptDecrypt.FileExtension;
                                                File.WriteAllBytes(this.txtDestinationFile.Text, this.m_EncryptDecrypt.DestinationContent);
                                                this.txtDestinationFile.Text = "Package File :" + this.txtDestinationFile.Text;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show("Fail to generate bin file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please Select a Hex File To Package", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Key Can't be Empty", "Invalid", MessageBoxButtons.OK);
            }
        }
        void btnFileUnpackage_Click(object sender, EventArgs e)
        {

        }
        void txtOutputFileName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(this.txtOutputFileName.Text.Trim()))
            {
                this.FileName = this.txtOutputFileName.Text.Trim() + this.FileExtension;
                this.FileNameWithoutExt = this.txtOutputFileName.Text.Trim();                                
            }
        }
        void btnBrowseFilePath_Click(object sender, EventArgs e)
        {           
            CommonForms.Configuration.SetWorkingDirectory();
            this.FilePath = Configuration.GetWorkingDirectory();            
            this.txtOutputFilePath.Text = this.FilePath;                                    
        }
        void btnBrowse_Click(object sender, EventArgs e)
        {
            this.FileName = String.Empty;
            this.FileNameWithoutExt = String.Empty;
            this.txtDestinationFile.Text = String.Empty;

            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "Select Hex File To Package";
            fileDialog.CheckFileExists = true;
            fileDialog.CheckPathExists = true;
            fileDialog.Multiselect = false;
            if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {                
                String fileExtension = System.IO.Path.GetExtension(fileDialog.FileName);
                this.FileExtension = fileExtension;
                if (String.IsNullOrEmpty(this.txtOutputFileName.Text.Trim()))
                {
                    this.FileName = fileDialog.SafeFileName;
                    this.FileNameWithoutExt = fileDialog.SafeFileName.Substring(0, fileDialog.SafeFileName.Length - fileExtension.Length);
                    this.txtOutputFileName.Text = this.FileNameWithoutExt;                    
                }
                else
                {
                    this.FileName = this.txtOutputFileName.Text.Trim() + this.FileExtension;
                    this.txtOutputFileName.Text = this.txtOutputFileName.Text.Trim();  
                }
                                               
                //RemoveFiles();
                //File.Copy(fileDialog.FileName, this.FilePath + this.FileName, true);

                this.txtHexFile.Text = fileDialog.FileName;
            }
        }
        private void CallHex2Bin()
        {
            if (!String.IsNullOrEmpty(this.txtHexFile.Text.Trim()))
            {
                RemoveFiles();
                File.Copy(this.txtHexFile.Text, this.FilePath + this.FileName, true);
            } 
            Process process = new Process();
            process.StartInfo.FileName = "hex2bin.exe";
            if (this.FilePath.Contains(" "))
                process.StartInfo.Arguments = @" """ + this.FilePath + this.FileName;
            else
                process.StartInfo.Arguments = this.FilePath + this.FileName;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.Start();
            process.WaitForExit();
            int ExitCode = process.ExitCode;            
        }
        private void RemoveFiles()
        {
            DirectoryInfo dir = new DirectoryInfo(FilePath);
            String fileName = this.FileNameWithoutExt + ".*";
            FileInfo[] fiArr = dir.GetFiles(fileName);
            try
            {
                foreach (FileInfo fi in fiArr)
                {
                    fi.Delete();
                }
            }
            catch { }
        }
        private void CheckWorkingDirectory()
        {
            this.FilePath = Configuration.GetWorkingDirectory();
            if (String.IsNullOrEmpty(this.FilePath))
            {
                CommonForms.Configuration.SetWorkingDirectory();
                this.FilePath = Configuration.GetWorkingDirectory();
            }
            this.txtOutputFilePath.Text = this.FilePath;
        }
        #endregion
    }
}