using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.EncryptDecrypt
{
    public partial class ProgressForm : Form
    {
        #region Progress Form
        public ProgressForm()
        {
            InitializeComponent();
            this.btnCancel.Click += new EventHandler(btnCancel_Click);
        }       
        #endregion

        #region Cancel
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        #region Set Message
        internal void SetMessage(String m_Message)
        {
            lblMsg.Text = m_Message;
            this.lblMsg.Visible = true;
            this.Process.Visible = true;
            this.btnCancel.Visible = false;
        }
        #endregion
    }
}