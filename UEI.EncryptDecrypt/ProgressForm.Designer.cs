namespace UEI.EncryptDecrypt
{
    partial class ProgressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progress1 = new UEI.EncryptDecrypt.Progress();
            this.lblMsg = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.Process = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // progress1
            // 
            this.progress1.AutoProgress = true;
            this.progress1.Location = new System.Drawing.Point(17, 16);
            this.progress1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.progress1.Name = "progress1";
            this.progress1.Position = 6;
            this.progress1.ShowBorder = false;
            this.progress1.Size = new System.Drawing.Size(333, 21);
            this.progress1.TabIndex = 0;
            // 
            // lblMsg
            // 
            this.lblMsg.Location = new System.Drawing.Point(88, 65);
            this.lblMsg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(256, 17);
            this.lblMsg.TabIndex = 16;
            this.lblMsg.Text = "label1";
            // 
            // btnCancel
            // 
            this.btnCancel.AutoSize = true;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(88, 108);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(67, 28);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // Process
            // 
            this.Process.AutoSize = true;
            this.Process.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Process.Location = new System.Drawing.Point(17, 66);
            this.Process.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Process.Name = "Process";
            this.Process.Size = new System.Drawing.Size(63, 17);
            this.Process.TabIndex = 14;
            this.Process.Text = "Process:";
            // 
            // ProgressForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(361, 153);
            this.ControlBox = false;
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.Process);
            this.Controls.Add(this.progress1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ProgressForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Progress";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Progress progress1;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label Process;
    }
}