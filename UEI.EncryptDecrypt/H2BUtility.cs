﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace UEI.EncryptDecrypt
{
    public class H2BUtility
    {
        #region Variables
        private static Dictionary<int, byte> m_Data = new Dictionary<int, byte>();
        //This is used for validation, once the code is stable this can be ignored
        private static Dictionary<int, byte> m_ModifiedData = new Dictionary<int, byte>();
        private static List<byte> m_NewByteStream = new List<byte>();
        private static Int32 m_Numberofpages = 0;
        #endregion       

        #region Methods
        public static int ProcessBinFile(string startaddress, string endaddress, int pagesize, string binfilepath)
        {
            ReadBinFile(binfilepath);            
            RemoveExtraBytes(startaddress, endaddress, pagesize);
            CreateNewBinFile(binfilepath);
            return m_Numberofpages;
        }
        private static void ReadBinFile(string binfilepath)
        {
            byte[] data = File.ReadAllBytes(binfilepath);
            m_Data = new Dictionary<int, byte>();
            int _addresscounter = 0;
            foreach (byte _item in data)
            {
                m_Data.Add(_addresscounter, _item);
                _addresscounter++;
            }
        }
        private static void RemoveExtraBytes(string startaddress, string endaddress, int pagesize)
        {
            Int32 _startaddress = Convert.ToInt32(startaddress, 16);//Int32.Parse(startaddress, System.Globalization.NumberStyles.HexNumber);
            Int32 _endaddress = Convert.ToInt32(endaddress, 16);//Int32.Parse(endaddress, System.Globalization.NumberStyles.HexNumber);
            m_ModifiedData = new Dictionary<int, byte>();
            m_NewByteStream = new List<byte>();
            foreach (KeyValuePair<int, byte> olddata in m_Data)
            {
                if (olddata.Key >= _startaddress && olddata.Key <= _endaddress)                
                {
                    m_ModifiedData.Add(olddata.Key, olddata.Value);
                    m_NewByteStream.Add(olddata.Value);
                }
            }
            CalculateTotalSize(_startaddress, _endaddress, pagesize);
        }       
        private static void CreateNewBinFile(string binfilepath)
        {
            File.WriteAllBytes(binfilepath, m_NewByteStream.ToArray());
        }
        private static void CalculateTotalSize(Int32 startaddress, Int32 endaddress, int pagesize)
        {
            m_Numberofpages = (endaddress - startaddress);
            m_Numberofpages++;
            m_Numberofpages = m_Numberofpages/pagesize;
        }
        public static void InsertSize(string binfilepath, int numberofpages)
        {
            byte[] _encdata = File.ReadAllBytes(binfilepath);
            byte[] _size = BitConverter.GetBytes((short)numberofpages);

            List<byte> _modifiedEncData = new List<byte>();
            if (_size.Length > 1)
            {
                for (int i = _size.Length - 1; i >= 0; i--)
                {
                    _modifiedEncData.Add(_size[i]);
                }
            }
            else if (_size.Length == 1)
            {
                _modifiedEncData.Add(0);
                _modifiedEncData.Add(_size[0]);
            }

            foreach (byte _endata in _encdata)
            {
                _modifiedEncData.Add(_endata);
            }

            File.WriteAllBytes(binfilepath, _modifiedEncData.ToArray());
        }
        public static byte[] InsertSize(byte[] binfile, int numberofpages)
        {
            byte[] _size = BitConverter.GetBytes((short)numberofpages);
            List<byte> _modifiedEncData = new List<byte>();
            if (_size.Length > 1)
            {
                for (int i = _size.Length - 1; i >= 0; i--)
                {
                    _modifiedEncData.Add(_size[i]);
                }
            }
            else if (_size.Length == 1)
            {
                _modifiedEncData.Add(0);
                _modifiedEncData.Add(_size[0]);
            }

            foreach (byte _encdata in binfile)
            {
                _modifiedEncData.Add(_encdata);
            }
            return _modifiedEncData.ToArray();
        }
        #endregion
    }
}