using System;
using System.Collections.Generic;
using System.Text;
using UEI.Security.Cryptography;
using System.IO;
using CommonForms;

namespace UEI.EncryptDecrypt
{
    public class EncyptionDecryption
    {
        #region Variables
        private const String SecretKeyFilename = "SecretTEA";
        private const String CSharpFileExtension = ".cs";
        private const String EncryptedFileExtension = ".tea";
        private const String PackagedFileExtension = ".enc";
        private CryptographyTEA m_Cryptography = null;
        #endregion

        #region Properties
        private String m_ErrorMessage = String.Empty;
        public String ErrorMessage
        {
            get { return m_ErrorMessage; }
            set { m_ErrorMessage = value; }
        }
        private String m_RankdomKey = String.Empty;
        public String RandomKey
        {
            get { return m_RankdomKey; }
            set { m_RankdomKey = value; }
        }
        private String m_SavedFile = String.Empty;
        public String SavedFile
        {
            get { return m_SavedFile; }
            set { m_SavedFile = value; }
        }
        private String m_FileExtension = String.Empty;
        public String FileExtension
        {
            get { return m_FileExtension; }
            set { m_FileExtension = value; }
        }
        private String m_CypherKey = String.Empty;
        public String CypherKey
        {
            get { return m_CypherKey; }
            set { m_CypherKey = value; }
        }
        private String m_PlainText = String.Empty;
        public String PlainText
        {
            get { return m_PlainText; }
            set { m_PlainText = value; }
        }
        private String m_CypherText = String.Empty;
        public String CypherText
        {
            get { return m_CypherText; }
            set { m_CypherText = value; }
        }
        private Byte[] m_SourceContect;
        public Byte[] SourceContent
        {
            get { return m_SourceContect; }
            set { m_SourceContect = value; }
        }
        private Byte[] m_DestinationContent;
        public Byte[] DestinationContent
        {
            get { return m_DestinationContent; }
            set { m_DestinationContent = value; }
        }
        private PackageType m_Type = PackageType.Type3;
        public PackageType Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        private String m_BlockStartAddress = String.Empty;
        public String BlockStartAddress
        {
            get { return m_BlockStartAddress; }
            set { m_BlockStartAddress = value; }
        }
        private String m_BlockEndAddress = String.Empty;
        public String BlockEndAddress
        {
            get { return m_BlockEndAddress; }
            set { m_BlockEndAddress = value; }
        }
        private String m_EncryptStatusAddress = String.Empty;
        public String EncryptStatusAddress
        {
            get { return m_EncryptStatusAddress; }
            set { m_EncryptStatusAddress = value; }
        }
        private Int32 m_PageSize = 512;
        public Int32 PageSize
        {
            get { return m_PageSize; }
            set { m_PageSize = value; }
        }
        #endregion

        #region Constructor
        public EncyptionDecryption()
        {
            this.m_Cryptography = new CryptographyTEA();
        }
        #endregion

        #region Generate Random Key
        public void GenerateRandomKey()
        {
            UInt32[] m_WorkingKey = m_Cryptography.GenerateWorkingKey();
            // Get the Base64 encoding of the Working Key.
            Byte[] m_WorkIngKeyBytes = new Byte[16];
            this.m_Cryptography.ConvertUInt32ArrayToBytes(m_WorkingKey, m_WorkIngKeyBytes);
            this.RandomKey = Convert.ToBase64String(m_WorkIngKeyBytes);
        }
        #endregion

        #region Generate KEK
        public void GenerateKEK()
        {
            UInt32[] m_KekKey = this.m_Cryptography.GenerateSecretKey();
            Byte[] m_KekBytes = new Byte[CryptographyTEA.KeySizeInBytes];
            this.m_Cryptography.ConvertUInt32ArrayToBytes(m_KekKey, m_KekBytes);
            StreamWriter m_StreamWriter = null;
            try
            {
                //
                // Export the C# version
                //
                String m_FileName = String.Empty;
                m_FileName = Configuration.GetWorkingDirectory();
                m_FileName += "\\" + SecretKeyFilename + CSharpFileExtension;

                m_StreamWriter = new StreamWriter(m_FileName, false);
                m_StreamWriter.WriteLine("using System;");
                m_StreamWriter.WriteLine();
                m_StreamWriter.WriteLine("namespace UEI.Security.Cryptography");
                m_StreamWriter.WriteLine("{");
                m_StreamWriter.WriteLine("\tpublic sealed partial class CryptographyTEA");
                m_StreamWriter.WriteLine("\t{");
                m_StreamWriter.WriteLine("\t\tprivate readonly byte[] SecretKey = new byte[16]");
                m_StreamWriter.WriteLine("\t\t{");
                m_StreamWriter.Write("\t\t\t");
                foreach(Byte m_Byte in m_KekBytes)
                {
                    m_StreamWriter.Write("0x");
                    m_StreamWriter.Write(Convert.ToString(m_Byte, 16));
                    m_StreamWriter.Write(",");
                }
                m_StreamWriter.WriteLine();

                m_StreamWriter.WriteLine("\t\t};");
                m_StreamWriter.WriteLine("\t}");
                m_StreamWriter.WriteLine("}");
                m_StreamWriter.WriteLine("");
                m_StreamWriter.Close();
                this.SavedFile = m_FileName;
            }
            catch(Exception ex)
            {
                m_StreamWriter.Close();                
            }           
        }
        #endregion

        #region Encrypt
        public void Encrypt()
        {
            ErrorMessage = String.Empty;
            //Get the Cypher Key
            UInt32[] m_Key = null;
            try
            {
                if(!String.IsNullOrEmpty(CypherKey))
                {
                    // Since the string isn't empty a key was specified.  
                    // Otherwise the default key encryption keys will be used when a null is passed for the key. 
                    Byte[] m_KeyBytes = Convert.FromBase64String(CypherKey);
                    m_Key = new UInt32[4];
                    this.m_Cryptography.ConvertBytesToUInt32Array(m_KeyBytes, m_Key);
                }

                // Get the Plain Text            
                if(!String.IsNullOrEmpty(PlainText))
                {
                    Char[] m_PlainChars = PlainText.ToCharArray();
                    Byte[] m_PlainBytes = Encoding.Unicode.GetBytes(m_PlainChars);
                    // Encrypt to cypher text..
                    Byte[] m_CypherBytes = this.m_Cryptography.EncryptData(m_PlainBytes, m_Key);
                    this.CypherText = Convert.ToBase64String(m_CypherBytes);
                }
            }
            catch(Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
        }
        #endregion

        #region Decrypt
        public void Decrypt()
        {
            ErrorMessage = String.Empty;
            // Get the Cypher Key
            UInt32[] m_Key = null;
            try
            {
                if(!String.IsNullOrEmpty(CypherKey))
                {
                    // Since the string isn't empty a key was specified.  
                    // Otherwise the default key encryption keys will be used when a null is passed for the key. 
                    Byte[] m_KeyBytes = Convert.FromBase64String(CypherKey);
                    m_Key = new UInt32[4];
                    this.m_Cryptography.ConvertBytesToUInt32Array(m_KeyBytes, m_Key);
                }

                // Get the Cypher Text            
                if(!String.IsNullOrEmpty(CypherText))
                {
                    Byte[] m_CypherBytes = Convert.FromBase64String(CypherText);
                    // Decrypt to plain text..
                    Byte[] m_PlainBytes = this.m_Cryptography.DecryptData(m_CypherBytes, m_Key);
                    Char[] m_PlainChars = Encoding.Unicode.GetChars(m_PlainBytes);
                    this.PlainText = new string(m_PlainChars);
                }
            }
            catch(Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
        }
        #endregion

        #region File Encrypt
        public void FileEncrypt()
        {
            ErrorMessage = String.Empty;
            // Get the Cypher Key
            UInt32[] m_Key = new UInt32[4];
            try
            {
                if(!String.IsNullOrEmpty(CypherKey))
                {
                    // Since the string isn't empty a key was specified.  
                    // Otherwise the default key encryption keys will be used when a null is passed for the key. 
                    Byte[] m_KeyBytes = Convert.FromBase64String(CypherKey);
                    this.m_Cryptography.ConvertBytesToUInt32Array(m_KeyBytes, m_Key);
                }
                // Encrypt to cypher text..
                DestinationContent = this.m_Cryptography.EncryptData(this.SourceContent, m_Key);

                // The encrypted file will have the same name with the addition of a standard encryption extension.
                this.FileExtension = EncryptedFileExtension;
            }
            catch
            {
                ErrorMessage = "Invalid source file.";
            }
        }
        #endregion

        #region File Decrypt
        public void FileDecrypt()
        {
            ErrorMessage = String.Empty;
            // Get the Cypher Key
            UInt32[] m_Key = new UInt32[4];
            try
            {
                if(!String.IsNullOrEmpty(CypherKey))
                {
                    // Since the string isn't empty a key was specified.  
                    // Otherwise the default key encryption keys will be used when a null is passed for the key. 
                    Byte[] m_KeyBytes = Convert.FromBase64String(CypherKey);
                    this.m_Cryptography.ConvertBytesToUInt32Array(m_KeyBytes, m_Key);
                }

                // Decrypt to plain text..
                DestinationContent = this.m_Cryptography.DecryptData(SourceContent, m_Key);

                // The decrypted filename will have the same name with the standard encryption extension removed.
                this.FileExtension = EncryptedFileExtension;
            }
            catch
            {
                ErrorMessage = "Invalid source file.";
            }
        }
        #endregion

        #region File Package
        public void FilePackage()
        {
            ErrorMessage = String.Empty;            
            // The packaged file will have the same name with the addition of a standard encryption extension.
            this.FileExtension = PackagedFileExtension;
            try
            {
                // Get the Cypher Key
                UInt32[] m_Key = new UInt32[4];
                Byte[] m_KeyBytes = Convert.FromBase64String(CypherKey);
                this.m_Cryptography.ConvertBytesToUInt32Array(m_KeyBytes, m_Key);

                // Encrypt to cypher text..            
                if(this.Type == PackageType.Type4)
                {
                    // Partial Encryption.
                    // The partial encryption of Type_4 encoding handles all of the necessary packaging.
                    DestinationContent = this.m_Cryptography.EncryptDataPartial(SourceContent, m_Key);
                }
                else if(this.Type == PackageType.Type3)
                {
                    // Sector Encryption. We will assume that it is divided into 3 parts: {System}{Application}{FDRA}. 
                    // Only the middle {Application} section will be encrypted.

                    // Get the {Application} block info. 
                    Int32 m_BlockStart = Convert.ToInt32(BlockStartAddress, 16);
                    Int32 m_BlockEnd = Convert.ToInt32(BlockEndAddress, 16);
                    Int32 m_BlockSize = (m_BlockEnd - m_BlockStart) + 1;

                    // Get the System Section.
                    Int32 m_SystemSize = m_BlockStart;
                    Byte[] m_SystemSection = new Byte[m_SystemSize];
                    for(Int32 i = 0; i < m_SystemSize; i++)
                    {
                        m_SystemSection[i] = SourceContent[i];
                    }

                    // Get the Application Section.
                    Int32 m_AppSize = m_BlockSize;
                    Byte[] m_AppSection = new Byte[m_AppSize];
                    for(Int32 i = 0; i < m_AppSize; i++)
                    {
                        m_AppSection[i] = SourceContent[m_SystemSize + i];
                    }

                    // Get the FDRA section.
                    Int32 m_FDRASize = SourceContent.Length - (m_SystemSize + m_AppSize);
                    Byte[] m_FDRASection = new Byte[m_FDRASize];
                    for(Int32 i = 0; i < m_FDRASize; i++)
                    {
                        m_FDRASection[i] = SourceContent[m_SystemSize + m_AppSize + i];
                    }

                    // Encrypt just the Application Section.
                    Byte[] m_dstBytes = this.m_Cryptography.EncryptDataSectors(m_AppSection, m_Key, 512);

                    // Build the entire file from all three sections.
                    DestinationContent = new Byte[m_SystemSize + m_dstBytes.Length + m_FDRASize];
                    Int32 p = 0;
                    foreach(Byte b in m_SystemSection)
                    {
                        DestinationContent[p++] = b;
                    }
                    foreach(Byte b in m_dstBytes)
                    {
                        // The encrypted application section.
                        DestinationContent[p++] = b;
                    }
                    foreach(Byte b in m_FDRASection)
                    {
                        DestinationContent[p++] = b;
                    }
                }
                else if(this.Type == PackageType.Unkown)
                {
                    // Normal Encryption
                    Byte[] m_dstBytes = this.m_Cryptography.EncryptData(SourceContent, m_Key);

                    // A standard packaged encrypted file.
                    QuicksetDistribution m_QSDistribution = new QuicksetDistribution();

                    // Get the checksum on the plain text.
                    Byte[] m_CheckSum = m_QSDistribution.CalculateChecksum(SourceContent);

                    // Get the ENC_STATUS
                    Byte m_EncryptStatus = Convert.ToByte(EncryptStatusAddress, 16);

                    // Package the encrypted data.
                    DestinationContent = m_QSDistribution.PackageEncryptedData(m_EncryptStatus, QuicksetDistribution.ENC_TYPE.Type_3, m_KeyBytes, m_dstBytes, m_CheckSum);
                }
            }
            catch
            {
                ErrorMessage = "Invalid source file.";
            }
        }
        public void FilePackage_Automate()
        {
            ErrorMessage = String.Empty;
            // The packaged file will have the same name with the addition of a standard encryption extension.
            this.FileExtension = PackagedFileExtension;
            try
            {
                // Get the Cypher Key
                UInt32[] m_Key = new UInt32[4];
                Byte[] m_KeyBytes = Convert.FromBase64String(CypherKey);
                this.m_Cryptography.ConvertBytesToUInt32Array(m_KeyBytes, m_Key);

                // Encrypt to cypher text..            
                if (this.Type == PackageType.Type4)
                {
                    // Partial Encryption.
                    // The partial encryption of Type_4 encoding handles all of the necessary packaging.
                    DestinationContent = this.m_Cryptography.EncryptDataPartial(SourceContent, m_Key);
                }
                else if (this.Type == PackageType.Type3)
                {
                    // Sector Encryption. It will consider entire file as {System}{Application}. 
                    // Only the middle {Application} section will be encrypted.                    

                    // Encrypt just the Application Section.
                    Byte[] m_dstBytes = this.m_Cryptography.EncryptDataSectors(SourceContent, m_Key, PageSize);

                    // Build the entire file from all three sections.
                    DestinationContent = new Byte[m_dstBytes.Length];
                    Int32 p = 0;                   
                    foreach (Byte b in m_dstBytes)
                    {
                        // The encrypted application section.
                        DestinationContent[p++] = b;
                    }                   
                }
                else if (this.Type == PackageType.Unkown)
                {
                    // Normal Encryption
                    Byte[] m_dstBytes = this.m_Cryptography.EncryptData(SourceContent, m_Key);

                    // A standard packaged encrypted file.
                    QuicksetDistribution m_QSDistribution = new QuicksetDistribution();

                    // Get the checksum on the plain text.
                    Byte[] m_CheckSum = m_QSDistribution.CalculateChecksum(SourceContent);

                    // Get the ENC_STATUS
                    Byte m_EncryptStatus = Convert.ToByte(EncryptStatusAddress, 16);

                    // Package the encrypted data.
                    DestinationContent = m_QSDistribution.PackageEncryptedData(m_EncryptStatus, QuicksetDistribution.ENC_TYPE.Type_3, m_KeyBytes, m_dstBytes, m_CheckSum);
                }
            }
            catch
            {
                ErrorMessage = "Invalid source file.";
            }
        }
        #endregion

        #region File Un Package
        public void FileUnPackage()
        {
            ErrorMessage = String.Empty;
            try
            {
                // The decrypted filename will have the same name with the standard encryption extension removed.
                this.FileExtension = PackagedFileExtension;
                if(this.Type == PackageType.Type4)
                {
                    // The Type_4 partial encryption handles all of the unpackaging.
                    DestinationContent = this.m_Cryptography.DecryptDataPartial(SourceContent);
                }
                else if(this.Type == PackageType.Type3)
                {
                    // Sector Encryption. We will assume that it is divided into 3 parts: {System}{Application}{FDRA}. 
                    // Only the middle {Application} section will be encrypted.
                    // Get the {Application} block info. 
                    Int32 m_BlockStart = Convert.ToInt32(BlockStartAddress, 16);
                    Int32 m_BlockEnd = Convert.ToInt32(BlockEndAddress, 16);
                    Int32 m_BlockSize = (m_BlockEnd - m_BlockStart) + 1;

                    // Get the System Section.
                    Int32 m_SystemSize = m_BlockStart;
                    Byte[] m_SystemSection = new Byte[m_SystemSize];
                    for(Int32 i = 0; i < m_SystemSize; i++)
                    {
                        m_SystemSection[i] = SourceContent[i];
                    }

                    // Get the Application Section.
                    Int32 m_AppSize = m_BlockSize;
                    Int32 m_EncAppSize = m_AppSize;
                    if(m_EncAppSize % CryptographyTEA.BlockSizeInBytes > 0)
                    {
                        m_EncAppSize += CryptographyTEA.BlockSizeInBytes - (m_EncAppSize % CryptographyTEA.BlockSizeInBytes);
                    }
                    Int32 m_EncSectorCount = m_EncAppSize / 512;
                    if(m_EncAppSize % 512 > 0)
                    {
                        m_EncSectorCount++;
                    }
                    m_EncAppSize += m_EncSectorCount * CryptographyTEA.EncryptedSectorWrapperSize;

                    Byte[] m_AppSection = new Byte[m_EncAppSize];
                    for(Int32 i = 0; i < m_EncAppSize; i++)
                    {
                        m_AppSection[i] = SourceContent[m_SystemSize + i];
                    }

                    // Get the FDRA section.
                    Int32 m_FDRASize = SourceContent.Length - (m_SystemSize + m_EncAppSize);
                    Byte[] m_FDRASection = new Byte[m_FDRASize];
                    for(Int32 i = 0; i < m_FDRASize; i++)
                    {
                        m_FDRASection[i] = SourceContent[m_SystemSize + m_EncAppSize + i];
                    }

                    // Decrypt just the Application Section.
                    Byte[] m_dstBytes = this.m_Cryptography.DecryptDataSectors(m_AppSection, 512);

                    // Build the entire file from all three sections.
                    DestinationContent = new Byte[m_SystemSize + m_AppSize + m_FDRASize];
                    Int32 p = 0;
                    foreach(Byte b in m_SystemSection)
                    {
                        DestinationContent[p++] = b;
                    }
                    for(Int32 i = 0; i < m_AppSize; i++)
                    {
                        DestinationContent[p++] = m_dstBytes[i];
                    }
                    foreach(Byte b in m_FDRASection)
                    {
                        DestinationContent[p++] = b;
                    }
                }
                else
                {
                    QuicksetDistribution m_QSDistribution = new QuicksetDistribution();

                    // Extract the needed data from the package.
                    Byte m_EncStatus;
                    QuicksetDistribution.ENC_TYPE m_DataType;
                    Byte[] m_KeyBytes;
                    Byte[] m_CypherBytes;
                    Byte[] m_CheckSum;

                    m_QSDistribution.UnpackageEncryptedData(SourceContent, out m_EncStatus, out m_DataType, out m_KeyBytes, out m_CypherBytes, out m_CheckSum);

                    // Confirm the correct encoding type.
                    if(m_DataType != QuicksetDistribution.ENC_TYPE.Type_3)
                    {
                        // Error.  Not Type-3.
                        ErrorMessage = "ERROR: Not ENC_TYPE = Type_3.";
                    }
                    else
                    {
                        // Extract the Cypher Key
                        UInt32[] m_Key = new UInt32[CryptographyTEA.KeySizeInUInt32];
                        this.m_Cryptography.ConvertBytesToUInt32Array(m_KeyBytes, m_Key);

                        // Decrypt to plain text..
                        byte[] dstBytes = null;
                        DestinationContent = this.m_Cryptography.DecryptData(m_CypherBytes, m_Key);

                        // Validate the checksum.
                        Byte[] m_PlainCheckSum = m_QSDistribution.CalculateChecksum(DestinationContent);
                        for(Int32 i = 0; i < m_PlainCheckSum.Length; i++)
                        {
                            if(m_PlainCheckSum[i] != m_CheckSum[i])
                            {
                                ErrorMessage = "ERROR: Checksum mismatch.";
                            }
                        }
                    }
                }
            }
            catch
            {
                ErrorMessage = "Invalid source file.";
            }
        }
        public void FileUnPackage_Automate()
        {
            ErrorMessage = String.Empty;
            try
            {
                // The decrypted filename will have the same name with the standard encryption extension removed.
                this.FileExtension = PackagedFileExtension;
                if (this.Type == PackageType.Type4)
                {
                    // The Type_4 partial encryption handles all of the unpackaging.
                    DestinationContent = this.m_Cryptography.DecryptDataPartial(SourceContent);
                }
                else if (this.Type == PackageType.Type3)
                {
                    // Sector Encryption.It will take : {System}{Application}. 
                    // Only the middle {Application} section will be encrypted.
                    // Get the {Application} block info. 
                   
                    // Decrypt just the Application Section.
                    Byte[] m_dstBytes = this.m_Cryptography.DecryptDataSectors(SourceContent, PageSize);
                    int m_AppSize = m_dstBytes.Length;
                    // Build the entire file from all three sections.
                    DestinationContent = new Byte[m_AppSize];
                    Int32 p = 0;                   
                    for (Int32 i = 0; i < m_AppSize; i++)
                    {
                        DestinationContent[p++] = m_dstBytes[i];
                    }                  
                }
                else
                {
                    QuicksetDistribution m_QSDistribution = new QuicksetDistribution();

                    // Extract the needed data from the package.
                    Byte m_EncStatus;
                    QuicksetDistribution.ENC_TYPE m_DataType;
                    Byte[] m_KeyBytes;
                    Byte[] m_CypherBytes;
                    Byte[] m_CheckSum;

                    m_QSDistribution.UnpackageEncryptedData(SourceContent, out m_EncStatus, out m_DataType, out m_KeyBytes, out m_CypherBytes, out m_CheckSum);

                    // Confirm the correct encoding type.
                    if (m_DataType != QuicksetDistribution.ENC_TYPE.Type_3)
                    {
                        // Error.  Not Type-3.
                        ErrorMessage = "ERROR: Not ENC_TYPE = Type_3.";
                    }
                    else
                    {
                        // Extract the Cypher Key
                        UInt32[] m_Key = new UInt32[CryptographyTEA.KeySizeInUInt32];
                        this.m_Cryptography.ConvertBytesToUInt32Array(m_KeyBytes, m_Key);

                        // Decrypt to plain text..
                        byte[] dstBytes = null;
                        DestinationContent = this.m_Cryptography.DecryptData(m_CypherBytes, m_Key);

                        // Validate the checksum.
                        Byte[] m_PlainCheckSum = m_QSDistribution.CalculateChecksum(DestinationContent);
                        for (Int32 i = 0; i < m_PlainCheckSum.Length; i++)
                        {
                            if (m_PlainCheckSum[i] != m_CheckSum[i])
                            {
                                ErrorMessage = "ERROR: Checksum mismatch.";
                            }
                        }
                    }
                }
            }
            catch
            {
                ErrorMessage = "Invalid source file.";
            }
        }
        #endregion        
    }
    public enum EncryptDecryptTypes
    {
        Encrypt,
        Decrypt,
        EncryptFile,
        DecryptFile,
        PackageFile,
        UnPackageFile
    }
    public enum PackageType
    {
        Type3,
        Type4,
        Unkown
    }
}