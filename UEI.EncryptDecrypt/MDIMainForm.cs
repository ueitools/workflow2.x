﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UEI.EncryptDecrypt
{
    public partial class MDIMainForm : Form
    {
        public MDIMainForm()
        {
            InitializeComponent();
        }        

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }       
        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }
        private void setWorkingDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CommonForms.Configuration.SetWorkingDirectory();
        }
        private void encryprAndDecryptOldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm m_EncDec = new MainForm();
            m_EncDec.ShowDialog();
        }

        private void encryptAndDecryptNewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AutomateEncryptDecrypt m_EncDec = new AutomateEncryptDecrypt();
            m_EncDec.ShowDialog();
        }              
    }
}
