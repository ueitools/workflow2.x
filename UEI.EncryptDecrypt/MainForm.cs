using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace UEI.EncryptDecrypt
{
    public partial class MainForm : Form
    {
        #region Variables
        private EncyptionDecryption m_EncryptDecrypt = null;
        private BackgroundWorker m_BackGroundWorker = null;
        private ProgressForm m_Progress = null;
        private EncryptDecryptTypes m_EncryptDecryptType = EncryptDecryptTypes.Encrypt;
        #endregion

        #region Page Load
        public MainForm()
        {
            InitializeComponent();
            m_BackGroundWorker = new BackgroundWorker();
            m_BackGroundWorker.DoWork += new DoWorkEventHandler(m_BackGroundWorker_DoWork);
            m_BackGroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(m_BackGroundWorker_RunWorkerCompleted);
            m_EncryptDecrypt = new EncyptionDecryption();
            this.btnRandomKey.Click += new EventHandler(btnRandomKey_Click);
            this.btnGenerateKEK.Click += new EventHandler(btnGenerateKEK_Click);
            this.btnEncrypt.Click += new EventHandler(btnEncrypt_Click);
            this.btnDecrypt.Click += new EventHandler(btnDecrypt_Click);
            this.btnFileEncrypt.Click += new EventHandler(btnFileEncrypt_Click);
            this.btnFileDecrypt.Click += new EventHandler(btnFileDecrypt_Click);
            this.btnFilePackage.Click += new EventHandler(btnFilePackage_Click);
            this.btnFileUnpackage.Click += new EventHandler(btnFileUnpackage_Click);
            this.chkType4.CheckedChanged += new EventHandler(chkType4_CheckedChanged);
            this.chkType3.CheckedChanged += new EventHandler(chkType3_CheckedChanged);
        }                                                     
        #endregion

        #region Back Ground Worker
        void m_BackGroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_Progress.DialogResult = DialogResult.OK;
            m_Progress.Close();
            switch(m_EncryptDecryptType)
            {
                case EncryptDecryptTypes.Encrypt:
                    this.txtCypherText.Text = this.m_EncryptDecrypt.CypherText;
                    break;
                case EncryptDecryptTypes.Decrypt:
                    this.txtPlainText.Text = this.m_EncryptDecrypt.PlainText;
                    break;
                case EncryptDecryptTypes.EncryptFile:                    
                    break;
                case EncryptDecryptTypes.DecryptFile:
                    break;
                case EncryptDecryptTypes.PackageFile:
                    break;
                case EncryptDecryptTypes.UnPackageFile:
                    break;
                default:
                    break;
            }            
        }
        void m_BackGroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            switch(m_EncryptDecryptType)
            {
                case EncryptDecryptTypes.Encrypt:
                    this.m_EncryptDecrypt.Encrypt();                    
                    break;
                case EncryptDecryptTypes.Decrypt:
                    this.m_EncryptDecrypt.Decrypt();                    
                    break;
                case EncryptDecryptTypes.EncryptFile:
                    this.m_EncryptDecrypt.FileEncrypt();
                    break;
                case EncryptDecryptTypes.DecryptFile:
                    this.m_EncryptDecrypt.FileDecrypt();
                    break;
                case EncryptDecryptTypes.PackageFile:
                    this.m_EncryptDecrypt.FilePackage();
                    break;
                case EncryptDecryptTypes.UnPackageFile:
                    this.m_EncryptDecrypt.FileUnPackage();
                    break;
                default:
                    break;
            }
        }        
        private void ResetTimer()
        {
            if(m_BackGroundWorker != null || m_BackGroundWorker.IsBusy == false)
                m_BackGroundWorker.RunWorkerAsync();

            if(m_Progress == null)
                m_Progress = new ProgressForm();
            m_Progress.SetMessage("Please Wait...");
            m_Progress.ShowInTaskbar = false;
            m_Progress.ShowDialog();            
        }
        #endregion

        #region Check Box Handling
        void chkType3_CheckedChanged(object sender, EventArgs e)
        {
            if(this.chkType3.Checked)
            {
                this.chkType4.Checked = false;
            }
        }
        void chkType4_CheckedChanged(object sender, EventArgs e)
        {
            if(this.chkType4.Checked)
            {
                this.chkType3.Checked = false;
            }
        }
        #endregion

        #region Package File
        void btnFilePackage_Click(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(txtCypherKey.Text))
            {
                OpenFileDialog m_OpenFileDialog = new OpenFileDialog();
                m_OpenFileDialog.Title = "Select File To Package";
                m_OpenFileDialog.CheckFileExists = true;
                m_OpenFileDialog.CheckPathExists = true;
                m_OpenFileDialog.Multiselect = false;
                if(chkType3.Checked)
                {
                    this.m_EncryptDecrypt.Type = PackageType.Type3;
                    if(txtBlockStartAddress.Text.Trim() == String.Empty)
                    {
                        MessageBox.Show("Block Start Address Can't be Empty", "Invalid", MessageBoxButtons.OK);
                        txtBlockStartAddress.Focus();
                    }
                    else if(txtBlockEndAddress.Text.Trim() == String.Empty)
                    {
                        MessageBox.Show("Block Start Address Can't be Empty", "Invalid", MessageBoxButtons.OK);
                        txtBlockEndAddress.Focus();
                    }                    
                    else
                    {
                        if(m_OpenFileDialog.ShowDialog() == DialogResult.OK)
                        {
                            this.txtDestinationFile.Text = "";
                            this.txtSourceFile.Text = "Source File :" + m_OpenFileDialog.FileName;
                            try
                            {
                                this.m_EncryptDecrypt.CypherKey = txtCypherKey.Text;
                                this.m_EncryptDecrypt.BlockStartAddress = txtBlockStartAddress.Text;
                                this.m_EncryptDecrypt.BlockEndAddress = txtBlockEndAddress.Text;
                                this.m_EncryptDecrypt.SourceContent = File.ReadAllBytes(m_OpenFileDialog.FileName);
                                this.m_EncryptDecryptType = EncryptDecryptTypes.PackageFile;
                                ResetTimer();
                                if(this.m_EncryptDecrypt.ErrorMessage != String.Empty)
                                {
                                    MessageBox.Show(this.m_EncryptDecrypt.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                else
                                {
                                    this.txtDestinationFile.Text = m_OpenFileDialog.FileName + this.m_EncryptDecrypt.FileExtension;
                                    File.WriteAllBytes(this.txtDestinationFile.Text, this.m_EncryptDecrypt.DestinationContent);
                                    this.txtDestinationFile.Text = "Package File :" + this.txtDestinationFile.Text;
                                }
                            }
                            catch(Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
                else if(chkType4.Checked)
                {
                    this.m_EncryptDecrypt.Type = PackageType.Type4;
                    if(m_OpenFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        this.txtDestinationFile.Text = "";
                        this.txtSourceFile.Text = "Source File :" + m_OpenFileDialog.FileName;
                        try
                        {
                            this.m_EncryptDecrypt.CypherKey = txtCypherKey.Text;
                            this.m_EncryptDecrypt.SourceContent = File.ReadAllBytes(m_OpenFileDialog.FileName);
                            this.m_EncryptDecryptType = EncryptDecryptTypes.PackageFile;
                            ResetTimer();
                            if(this.m_EncryptDecrypt.ErrorMessage != String.Empty)
                            {
                                MessageBox.Show(this.m_EncryptDecrypt.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else
                            {
                                this.txtDestinationFile.Text = m_OpenFileDialog.FileName + this.m_EncryptDecrypt.FileExtension;
                                File.WriteAllBytes(this.txtDestinationFile.Text, this.m_EncryptDecrypt.DestinationContent);
                                this.txtDestinationFile.Text = "Package File :" + this.txtDestinationFile.Text;
                            }
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    this.m_EncryptDecrypt.Type = PackageType.Unkown;
                    if(txtEncryptStatus.Text.Trim() == String.Empty)
                    {
                        MessageBox.Show("Status Address Can't be Empty", "Invalid", MessageBoxButtons.OK);
                        txtEncryptStatus.Focus();
                    }
                    else if(m_OpenFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        this.txtDestinationFile.Text = "";
                        this.txtSourceFile.Text = "Source File :" + m_OpenFileDialog.FileName;
                        try
                        {
                            this.m_EncryptDecrypt.CypherKey = txtCypherKey.Text;
                            this.m_EncryptDecrypt.EncryptStatusAddress = txtEncryptStatus.Text;
                            this.m_EncryptDecrypt.SourceContent = File.ReadAllBytes(m_OpenFileDialog.FileName);
                            this.m_EncryptDecryptType = EncryptDecryptTypes.PackageFile;
                            ResetTimer();
                            if(this.m_EncryptDecrypt.ErrorMessage != String.Empty)
                            {
                                MessageBox.Show(this.m_EncryptDecrypt.ErrorMessage, "Error", MessageBoxButtons.OK,MessageBoxIcon.Error);
                            }
                            else
                            {
                                this.txtDestinationFile.Text = m_OpenFileDialog.FileName + this.m_EncryptDecrypt.FileExtension;
                                File.WriteAllBytes(this.txtDestinationFile.Text, this.m_EncryptDecrypt.DestinationContent);
                                this.txtDestinationFile.Text = "Package File :" + this.txtDestinationFile.Text;
                            }
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK,MessageBoxIcon.Error);
                        }
                    }
                }                                
            }
            else
            {
                MessageBox.Show("Key Can't be Empty", "Invalid", MessageBoxButtons.OK);
            }
        }
        #endregion

        #region Un Package File
        void btnFileUnpackage_Click(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(txtCypherKey.Text))
            {
                OpenFileDialog m_OpenFileDialog = new OpenFileDialog();
                m_OpenFileDialog.Title = "Select File To UnPackage";
                m_OpenFileDialog.CheckFileExists = true;
                m_OpenFileDialog.CheckPathExists = true;
                m_OpenFileDialog.Multiselect = false;
                if(chkType3.Checked)
                {
                    this.m_EncryptDecrypt.Type = PackageType.Type3;
                    if(txtBlockStartAddress.Text.Trim() == String.Empty)
                    {
                        MessageBox.Show("Block Start Address Can't be Empty", "Invalid", MessageBoxButtons.OK);
                        txtBlockStartAddress.Focus();
                    }
                    else if(txtBlockEndAddress.Text.Trim() == String.Empty)
                    {
                        MessageBox.Show("Block Start Address Can't be Empty", "Invalid", MessageBoxButtons.OK);
                        txtBlockEndAddress.Focus();
                    }
                    else
                    {
                        if(m_OpenFileDialog.ShowDialog() == DialogResult.OK)
                        {
                            this.txtDestinationFile.Text = "";
                            this.txtSourceFile.Text = "Source File :" + m_OpenFileDialog.FileName;
                            try
                            {
                                this.m_EncryptDecrypt.CypherKey = txtCypherKey.Text;
                                this.m_EncryptDecrypt.BlockStartAddress = txtBlockStartAddress.Text;
                                this.m_EncryptDecrypt.BlockEndAddress = txtBlockEndAddress.Text;
                                this.m_EncryptDecrypt.SourceContent = File.ReadAllBytes(m_OpenFileDialog.FileName);
                                this.m_EncryptDecryptType = EncryptDecryptTypes.UnPackageFile;
                                ResetTimer();
                                if(this.m_EncryptDecrypt.ErrorMessage != String.Empty)
                                {
                                    MessageBox.Show(this.m_EncryptDecrypt.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                else
                                {
                                    if(m_OpenFileDialog.FileName.EndsWith(this.m_EncryptDecrypt.FileExtension))
                                        this.txtDestinationFile.Text = m_OpenFileDialog.FileName.Replace(this.m_EncryptDecrypt.FileExtension, "");
                                    else
                                        this.txtDestinationFile.Text = m_OpenFileDialog.FileName;
                                    File.WriteAllBytes(this.txtDestinationFile.Text, this.m_EncryptDecrypt.DestinationContent);
                                    this.txtDestinationFile.Text = "UnPackage File :" + this.txtDestinationFile.Text;
                                }
                            }
                            catch(Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
                else if(chkType4.Checked)
                {
                    this.m_EncryptDecrypt.Type = PackageType.Type4;
                    if(m_OpenFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        this.txtDestinationFile.Text = "";
                        this.txtSourceFile.Text = "Source File :" + m_OpenFileDialog.FileName;
                        try
                        {
                            this.m_EncryptDecrypt.CypherKey = txtCypherKey.Text;
                            this.m_EncryptDecrypt.SourceContent = File.ReadAllBytes(m_OpenFileDialog.FileName);
                            this.m_EncryptDecryptType = EncryptDecryptTypes.UnPackageFile;
                            ResetTimer();
                            if(this.m_EncryptDecrypt.ErrorMessage != String.Empty)
                            {
                                MessageBox.Show(this.m_EncryptDecrypt.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else
                            {
                                if(m_OpenFileDialog.FileName.EndsWith(this.m_EncryptDecrypt.FileExtension))
                                    this.txtDestinationFile.Text = m_OpenFileDialog.FileName.Replace(this.m_EncryptDecrypt.FileExtension, "");
                                else
                                    this.txtDestinationFile.Text = m_OpenFileDialog.FileName;
                                File.WriteAllBytes(this.txtDestinationFile.Text, this.m_EncryptDecrypt.DestinationContent);
                                this.txtDestinationFile.Text = "UnPackage File :" + this.txtDestinationFile.Text;
                            }
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    this.m_EncryptDecrypt.Type = PackageType.Unkown;
                    if(txtEncryptStatus.Text.Trim() == String.Empty)
                    {
                        MessageBox.Show("Status Address Can't be Empty", "Invalid", MessageBoxButtons.OK);
                        txtEncryptStatus.Focus();
                    }
                    else if(m_OpenFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        this.txtDestinationFile.Text = "";
                        this.txtSourceFile.Text = "Source File :" + m_OpenFileDialog.FileName;
                        try
                        {
                            this.m_EncryptDecrypt.CypherKey = txtCypherKey.Text;
                            this.m_EncryptDecrypt.EncryptStatusAddress = txtEncryptStatus.Text;
                            this.m_EncryptDecrypt.SourceContent = File.ReadAllBytes(m_OpenFileDialog.FileName);
                            this.m_EncryptDecryptType = EncryptDecryptTypes.UnPackageFile;
                            ResetTimer();
                            if(this.m_EncryptDecrypt.ErrorMessage != String.Empty)
                            {
                                MessageBox.Show(this.m_EncryptDecrypt.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else
                            {
                                if(m_OpenFileDialog.FileName.EndsWith(this.m_EncryptDecrypt.FileExtension))
                                    this.txtDestinationFile.Text = m_OpenFileDialog.FileName.Replace(this.m_EncryptDecrypt.FileExtension, "");
                                else
                                    this.txtDestinationFile.Text = m_OpenFileDialog.FileName;
                                File.WriteAllBytes(this.txtDestinationFile.Text, this.m_EncryptDecrypt.DestinationContent);
                                this.txtDestinationFile.Text = "UnPackage File :" + this.txtDestinationFile.Text;
                            }
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Key Can't be Empty", "Invalid", MessageBoxButtons.OK);
            }
        }
        #endregion

        #region File Decrypt
        void btnFileDecrypt_Click(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(txtCypherKey.Text))
            {
                OpenFileDialog m_OpenFileDialog = new OpenFileDialog();
                m_OpenFileDialog.Title = "Select File To Decrypt";
                m_OpenFileDialog.CheckFileExists = true;
                m_OpenFileDialog.CheckPathExists = true;
                m_OpenFileDialog.Multiselect = false;
                if(m_OpenFileDialog.ShowDialog() == DialogResult.OK)
                {
                    this.txtDestinationFile.Text = "";
                    this.txtSourceFile.Text = "Source File :" + m_OpenFileDialog.FileName;
                    try
                    {
                        this.m_EncryptDecrypt.CypherKey = txtCypherKey.Text;
                        this.m_EncryptDecrypt.SourceContent = File.ReadAllBytes(m_OpenFileDialog.FileName);
                        this.m_EncryptDecryptType = EncryptDecryptTypes.DecryptFile;
                        ResetTimer();
                        if(this.m_EncryptDecrypt.ErrorMessage != String.Empty)
                        {
                            MessageBox.Show(this.m_EncryptDecrypt.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            if(m_OpenFileDialog.FileName.EndsWith(this.m_EncryptDecrypt.FileExtension))
                                this.txtDestinationFile.Text = m_OpenFileDialog.FileName.Replace(this.m_EncryptDecrypt.FileExtension, "");
                            else
                                this.txtDestinationFile.Text = m_OpenFileDialog.FileName;
                            File.WriteAllBytes(this.txtDestinationFile.Text, this.m_EncryptDecrypt.DestinationContent);
                            this.txtDestinationFile.Text = "Decrypt File :" + this.txtDestinationFile.Text;
                        }
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Key Can't be Empty", "Invalid", MessageBoxButtons.OK);
            }
        } 
        #endregion

        #region File Encrypt
        void btnFileEncrypt_Click(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(txtCypherKey.Text))
            {
                OpenFileDialog m_OpenFileDialog = new OpenFileDialog();
                m_OpenFileDialog.Title = "Select File To Encrypt";
                m_OpenFileDialog.CheckFileExists = true;
                m_OpenFileDialog.CheckPathExists = true;
                m_OpenFileDialog.Multiselect = false;
                if(m_OpenFileDialog.ShowDialog() == DialogResult.OK)
                {
                    this.txtDestinationFile.Text = "";
                    this.txtSourceFile.Text = "Source File :" + m_OpenFileDialog.FileName;
                    try
                    {
                        this.m_EncryptDecrypt.CypherKey = txtCypherKey.Text;
                        this.m_EncryptDecrypt.SourceContent = File.ReadAllBytes(m_OpenFileDialog.FileName);
                        this.m_EncryptDecryptType = EncryptDecryptTypes.EncryptFile;
                        ResetTimer();
                        if(this.m_EncryptDecrypt.ErrorMessage != String.Empty)
                        {
                            MessageBox.Show(this.m_EncryptDecrypt.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            this.txtDestinationFile.Text = m_OpenFileDialog.FileName + this.m_EncryptDecrypt.FileExtension;
                            File.WriteAllBytes(this.txtDestinationFile.Text, this.m_EncryptDecrypt.DestinationContent);
                            this.txtDestinationFile.Text = "Encrypt File :" + this.txtDestinationFile.Text;
                        }
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Key Can't be Empty", "Invalid", MessageBoxButtons.OK);
            }
        } 
        #endregion

        #region Decrypt
        void btnDecrypt_Click(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(txtCypherKey.Text))
            {
                if(!String.IsNullOrEmpty(txtCypherText.Text))
                {
                    this.m_EncryptDecrypt.CypherKey = txtCypherKey.Text;
                    this.m_EncryptDecrypt.CypherText = txtCypherText.Text;
                    try
                    {
                        m_EncryptDecryptType = EncryptDecryptTypes.Decrypt;
                        ResetTimer();
                        if(this.m_EncryptDecrypt.ErrorMessage != String.Empty)
                        {
                            MessageBox.Show(this.m_EncryptDecrypt.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    MessageBox.Show("Cypher Text Can't be Empty", "Invalid", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("Key Can't be Empty", "Invalid", MessageBoxButtons.OK);
            }
        }
        #endregion

        #region Encrypt
        void btnEncrypt_Click(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(txtCypherKey.Text))
            {
                if(!String.IsNullOrEmpty(txtPlainText.Text))
                {
                    this.m_EncryptDecrypt.CypherKey = txtCypherKey.Text;
                    this.m_EncryptDecrypt.PlainText = txtPlainText.Text;
                    try
                    {
                        m_EncryptDecryptType = EncryptDecryptTypes.Encrypt;
                        ResetTimer();
                        if(this.m_EncryptDecrypt.ErrorMessage != String.Empty)
                        {
                            MessageBox.Show(this.m_EncryptDecrypt.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
                    }                    
                }
                else
                {
                    MessageBox.Show("Plain Text Can't be Empty", "Invalid", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("Key Can't be Empty", "Invalid", MessageBoxButtons.OK);
            }
        }
        #endregion

        #region Generate KEK
        void btnGenerateKEK_Click(object sender, EventArgs e)
        {
            this.m_EncryptDecrypt.GenerateKEK();
            if(this.m_EncryptDecrypt.SavedFile != String.Empty)
            {
                MessageBox.Show("Generate KEK Saved to Path\n"+this.m_EncryptDecrypt.SavedFile, "Alert!", MessageBoxButtons.OK);
            }
        }
        #endregion

        #region Generate Random Key
        void btnRandomKey_Click(object sender, EventArgs e)
        {
            this.m_EncryptDecrypt.GenerateRandomKey();
            txtCypherKey.Text = this.m_EncryptDecrypt.RandomKey;
        }
        #endregion
    }
}